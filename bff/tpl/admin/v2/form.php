<?php namespace bff\tpl\admin;
/**
 * Шаблон для констрктора таба настройки
 * @version 0.1
 * @modified 05.oct.2018
 * @var $this Form
 * @var $img FormImages
 * @var $fls FormFiles
 */

$u = rand(12345, 54321);

$tip = function($f) { if (empty($f['tip'])) return ''; $tip = $f['tip']; ob_start();ob_implicit_flush(false); ?>
    <a class="descr disabled j-popover" href="javascript:" data-placement="<?= $tip['placement'] ?>" data-trigger="<?= $tip['trigger'] ?>" data-content="<?= \HTML::escape($tip['content']) ?>"><i class="icon-question-sign"></i></a>
<?php $result = ob_get_clean();
    if (is_callable($tip['wrapper'], true)) { $result = call_user_func($tip['wrapper'], $result); }
    return $result;
};

# Генерация HTML для поля типа FIELD_TEXT
$text = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false);
    \HTML::attributeAdd($f['attr'], 'type', 'text');
    \HTML::attributeAdd($f['attr'], 'name', $name);
    \HTML::attributeAdd($f['attr'], 'value', \HTML::escape($f['value']));
    if ( ! isset($f['attr']['class'])) { \HTML::attributeAdd($f['attr'], 'class', 'stretch'); }
?>
    <tr>
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <input<?= \HTML::attributes($f['attr']) ?>/>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_TEXT_LANG
$textLang = function($f) use(& $name, & $tip) {
    $f['fullName'] = $name;
    \HTML::attributeAdd($f['attr'], 'type', 'text');
    if ( ! isset($f['attr']['class'])) { \HTML::attributeAdd($f['attr'], 'class', 'stretch'); }
    $f['tipHtml'] = $tip($f);
    return $this->locale->buildForm($f, 'settings-field-'.preg_replace('/[^a-zA-Z0-9]/', '-', $name), '
        <tr>
            <td class="row1 field-title"><?= $aData[\'title\'] ?></td>
            <td class="row2">
                <input <?= \HTML::attributes($aData[\'attr\']) ?> name="<?= $aData[\'fullName\'] ?>[<?= $key ?>]" value="<?= isset($aData[\'value\'][$key]) ? \HTML::escape($aData[\'value\'][$key]) : \'\' ?>" />
                <?= $aData[\'tipHtml\'] ?>
                <?= $aData[\'htmlAfter\'] ?>
            </td>
        </tr>
        '); ?>
<?php };

# Генерация HTML для поля типа FIELD_TEXTAREA
$textarea = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false);
    \HTML::attributeAdd($f['attr'], 'name', $name);
    if ( ! isset($f['attr']['class'])) { \HTML::attributeAdd($f['attr'], 'class', 'stretch'); }
    if ( ! isset($f['attr']['rows']))  { \HTML::attributeAdd($f['attr'], 'rows', '5'); }
    if ( ! isset($f['attr']['style']['resize']))  { \HTML::attributeAdd($f['attr'], 'style', ['resize' =>'vertical']); }
?>
    <tr>
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <textarea<?= \HTML::attributes($f['attr']) ?>><?= \HTML::escape($f['value']) ?></textarea>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_TEXTAREA_LANG
$textareaLang = function($f) use(& $name, & $tip) {
    $f['fullName'] = $name;
    if ( ! isset($f['attr']['class'])) { \HTML::attributeAdd($f['attr'], 'class', 'stretch'); }
    if ( ! isset($f['attr']['rows']))  { \HTML::attributeAdd($f['attr'], 'rows', '5'); }
    if ( ! isset($f['attr']['style']['resize']))  { \HTML::attributeAdd($f['attr'], 'style', ['resize' => 'vertical']); }
    $f['tipHtml'] = $tip($f);
    return $this->locale->buildForm($f, 'settings-field-'.preg_replace('/[^a-zA-Z0-9]/', '-', $name), '
        <tr>
            <td class="row1 field-title"><?= $aData[\'title\'] ?></td>
            <td class="row2">
                <textarea <?= \HTML::attributes($aData[\'attr\']) ?> name="<?= $aData[\'fullName\'] ?>[<?= $key ?>]"><?= isset($aData[\'value\'][$key]) ? \HTML::escape($aData[\'value\'][$key]) : \'\' ?></textarea>
                <?= $aData[\'tipHtml\'] ?>
                <?= $aData[\'htmlAfter\'] ?>
            </td>
        </tr>
        '); ?>
<?php };

# Генерация HTML для поля типа FIELD_WYSIWYG
$jwysiwyg = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false); ?>
    <tr>
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <?= \tpl::jwysiwyg($f['value'], $name, $f['width'], $f['height'], $f['params'], $f['JSObjectName']); ?>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_WYSIWYG_LANG
$jwysiwygLang = function($f) use(& $name, & $tip) {
    $f['fullName'] = $name;
    $f['tipHtml'] = $tip($f);
    return $this->locale->buildForm($f, 'settings-field-'.preg_replace('/[^a-zA-Z0-9]/', '-', $name), '
        <tr>
            <td class="row1 field-title"><?= $aData[\'title\'] ?></td>
            <td class="row2">
                <?= \tpl::jwysiwyg(isset($aData[\'value\'][$key]) ? \HTML::escape($aData[\'value\'][$key]) : \'\', $aData[\'fullName\'].\'[\'.$key.\']\', $aData[\'width\'], $aData[\'height\'], $aData[\'params\'], $aData[\'JSObjectName\'] ? $aData[\'JSObjectName\'].$key : \'\'); ?>
                <?= $aData[\'tipHtml\'] ?>
                <?= $aData[\'htmlAfter\'] ?>
            </td>
        </tr>
        '); ?>
<?php };

# Генерация HTML для поля типа FIELD_WYSIWYG_FCK
$wysiwygFck = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false); ?>
    <tr>
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <?= \tpl::wysiwyg($f['value'], $name, $f['width'], $f['height'], $f['toolbarMode'], $f['theme']); ?>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_WYSIWYG_FCK_LANG
$wysiwygFckLang = function($f) use(& $name, & $tip) {
    $f['fullName'] = $name;
    $f['tipHtml'] = $tip($f);
    return $this->locale->buildForm($f, 'settings-field-'.preg_replace('/[^a-zA-Z0-9]/', '-', $name), '
        <tr>
            <td class="row1 field-title"><?= $aData[\'title\'] ?></td>
            <td class="row2">
                <?= \tpl::wysiwyg(isset($aData[\'value\'][$key]) ? $aData[\'value\'][$key] : \'\', $aData[\'fullName\'].\'[\'.$key.\']\', $aData[\'width\'], $aData[\'height\'], $aData[\'toolbarMode\'], $aData[\'theme\']); ?>
                <?= $aData[\'tipHtml\'] ?>
                <?= $aData[\'htmlAfter\'] ?>
            </td>
        </tr>
        '); ?>
<?php };

# Генерация HTML для загруженного изображения
$imageHTML = function($v = array()) use( & $img) { ob_start();ob_implicit_flush(false);
    if (empty($v)) {
        $v['name'] = '__name__';
        $v['filename'] = '__filename__';
        $v['record'] = '__record__';
        $v['url_s'] = '__url_s__';
        $v['url_o'] = '__url_o__';
    }
    ?>
    <li class="j-image relative left" style="margin: 0 5px 5px 0;">
        <input type="hidden" class="j-image-filename" name="<?= $v['name'] ?>" value="<?= $v['filename'] ?>" />
        <a href="<?= $v['url_o'] ?>" rel="fb-group-<?= $v['record'] ?>" class="thumbnail j-image-fb"  style="cursor: move;">
            <img src="<?= $v['url_s'] ?>" alt="" style="width: 100px;height: 100px;" />
        </a>
        <a class="but cross j-images-delete" style="position:absolute;right:2px;top:7px;" href="#" data-fn="<?= $v['filename'] ?>"></a>
    </li>
    <?php return ob_get_clean(); };
# Генерация HTML для поля типа FIELD_IMAGES
$images = function($f) use(& $name, & $img, & $imageHTML, & $tip) { ob_start();ob_implicit_flush(false);  $img->setField($f['id']); ?>
    <tr class="j-images-upload-bl <?= $f['hidden'] ? 'displaynone' : '' ?>" data-record="<?= $f['id'] ?>" data-limit="<?= $f['limit'] ?>" data-uploaded="<?= count($f['value']) ?>" data-name="<?= $name ?>">
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <div><a href="#" class="ajax j-images-add"><?= _t('extensions', '+ загрузить изображение (до [size])', ['size' => $img->getMaxSize(true)])?></a><?= $tip($f) ?></div>
            <?= $f['htmlAfter'] ?>
            <ul class="j-images-list" style="display: block; margin: 0;">
                <? if ( ! empty($f['value']) && is_array($f['value'])) { foreach ($f['value'] as $v) {
                    $v['name'] = $name.'[]';
                    $v['record'] = $f['id'];
                    echo $imageHTML($v);
                } } ?>
            </ul>
            <div class="clearfix"></div>
        </td>
    </tr>
<?php return ob_get_clean(); };
# Генерация HTML для поля типа FIELD_IMAGES_LINK
$imagesLink = function($f) use(& $name, & $img, & $fields, & $tip) {  ob_start();ob_implicit_flush(false);
    if ( ! isset($fields[ $f['uploader'] ])) return;
    $img->setField($f['uploader']);
    $fi = $fields[ $f['uploader'] ];
    $filenames = array();
    if (is_array($f['value'])) {
        foreach ($f['value'] as $v) {
            $filenames[] = $v['filename'];
        }
    }
?>
    <tr class="j-image-link-bl" data-record="<?= $fi['id'] ?>" data-name="<?= $name ?>[]" data-filenames="<?= join(',', $filenames) ?>" data-limit="<?= $f['limit'] ?>">
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <div><a href="#" class="ajax j-image-link-add"><?= _t('extensions', '+ загрузить изображение (до [size])', ['size' => $img->getMaxSize(true)])?></a><?= $tip($f) ?></div>
            <ul class="j-image-link-list" style="display: block; margin: 0;">
            </ul>
            <div class="clearfix"></div>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для загруженного файла
$fileHTML = function($v = array()) use( & $fls) {  ob_start();ob_implicit_flush(false);
    if (empty($v)) {
        $v['name'] = '__name__';
        $v['filename'] = '__filename__';
        $v['record'] = '__record__';
        $v['filesize'] = '__filesize__';
        $v['rfilename'] = '__rfilename__';
        $v['url'] = '__url__';
    } else {
        $v['url'] = $fls->getUrl($v);
        $v['filesize'] = \tpl::filesize($v['filesize']);
    }
    ?>
    <li class="relative j-file" style="margin-bottom: 5px;">
        <input type="hidden" class="j-file-filename" name="<?= $v['name'] ?>" value="<?= $v['filename'] ?>" />
        <div style="display:inline-block; cursor: move;">
            <a href="<?= $v['url'] ?>" class="nolink" target="_blank">
                <?= $v['rfilename'] ?>
            </a>
            <span class="desc">(<?= $v['filesize'] ?>)</span>
        </div>
        <a class="but cross j-files-delete" href="#" data-fn="<?= $v['filename'] ?>"></a>
    </li>
    <?php return ob_get_clean(); };
# Генерация HTML для поля типа FIELD_FILES
$files = function($f) use(& $name, & $fls, & $fileHTML, & $tip) { ob_start();ob_implicit_flush(false);
    $fls->setField($f['id']);
?>
    <tr class="j-files-upload-bl <?= $f['hidden'] ? 'displaynone' : '' ?>" data-record="<?= $f['id'] ?>" data-limit="<?= $f['limit'] ?>" data-uploaded="<?= count($f['value']) ?>" data-name="<?= $name ?>">
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <div><a href="#" class="ajax j-files-add"><?= _t('extensions', '+ загрузить файл (до [size])', ['size' => $fls->getMaxSize(true)])?></a><?= $tip($f) ?></div>
            <?= $f['htmlAfter'] ?>
            <ul class="j-files-list" style="display: block; margin: 0;">
                <? if ( ! empty($f['value']) && is_array($f['value'])) { foreach ($f['value'] as $v) {
                    $v['name'] = $name.'[]';
                    $v['record'] = $f['id'];
                    echo $fileHTML($v);
                } } ?>
            </ul>
            <div class="clearfix"></div>
        </td>
    </tr>
<?php return ob_get_clean(); };
# Генерация HTML для поля типа FIELD_IMAGES_LINK
$filesLink = function($f) use(& $name, & $fls, & $fields, & $tip) { ob_start();ob_implicit_flush(false);
    if ( ! isset($fields[ $f['uploader'] ])) return;
    $fls->setField($f['uploader']);
    $fi = $fields[ $f['uploader'] ];
    $filenames = array();
    if (is_array($f['value'])) {
        foreach ($f['value'] as $v) {
            $filenames[] = $v['filename'];
        }
    }
?>
    <tr class="j-file-link-bl" data-record="<?= $fi['id'] ?>" data-name="<?= $name ?>[]" data-filenames="<?= join(',', $filenames) ?>" data-limit="<?= $f['limit'] ?>">
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <div><a href="#" class="ajax j-file-link-add"><?= _t('extensions', '+ загрузить файл (до [size])', ['size' => $fls->getMaxSize(true)])?></a><?= $tip($f) ?></div>
            <ul class="j-file-link-list" style="display: block; margin: 0;">
            </ul>
            <div class="clearfix"></div>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_SELECT
$select = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false);
    if (empty($f['options'])) return;
    \HTML::attributeAdd($f['attr'], 'name', $name);
?>
<tr>
    <td class="row1 field-title"><?= $f['title'] ?></td>
    <td class="row2">
        <select<?= \HTML::attributes($f['attr']) ?>><?= \HTML::selectOptions($f['options'], $f['value'], $f['empty'], 'id', 'title') ?></select>
        <?= $tip($f) ?>
        <?= $f['htmlAfter'] ?>
    </td>
</tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_CUSTOM
$custom = function($f) use(& $name) {
    if (is_callable($f['htmlCallback'], true)) {
        return call_user_func($f['htmlCallback'], $name, $f);
    }
    return '';
};

# Генерация HTML для поля типа FIELD_CHECKBOX
$checkbox = function($f) use(& $name, & $tip) {  ob_start();ob_implicit_flush(false);
    \HTML::attributeAdd($f['attr'], 'type', 'checkbox');
    \HTML::attributeAdd($f['attr'], 'name', $name);
    \HTML::attributeAdd($f['attr'], 'value', 1);
    if ($f['value']) {
        \HTML::attributeAdd($f['attr'], 'checked', 'checked');
    }
?>
    <tr>
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <label class="checkbox inline">
                <input<?= \HTML::attributes($f['attr']) ?>/>
                <?= $f['label'] ?>
            </label>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_CHECKBOX_LIST
$checkboxList = function($f) use(& $name, & $tip) {
    if ( ! is_array($f['value'])) return '';
    ob_start();ob_implicit_flush(false);
    \HTML::attributeAdd($f['attr'], 'type', 'checkbox');
    if ( ! isset($f['attr']['value'])) { \HTML::attributeAdd($f['attr'], 'value', '1'); }
?>
    <tr class="j-checkbox-list-bl" data-sortable="<?= $f['sortable'] ? 1 : 0 ?>">
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <ul class="j-checkbox-list" style="display: block; margin: 0;">
                <?php foreach ($f['value'] as $k => $v):
                    if ( ! isset($f['options'][$k])) continue;
                    $o = $f['options'][$k];
                    $attr = $f['attr'];
                    \HTML::attributeAdd($attr, 'name', $name.'['.$k.']');
                    if ( ! empty($v['checked'])) {
                        \HTML::attributeAdd($attr, 'checked', 'checked');
                    }
                    ?>
                    <li class="j-checkbox" style="margin: 0 5px 5px 0;">
                        <label class="checkbox inline" <?= $f['sortable'] ? 'style="cursor: move;"' : '' ?>>
                            <input<?= \HTML::attributes($attr) ?>/>
                            <?= $o['title'] ?>
                        </label>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для тега
$tagHTML = function($v = array(), $sortable = false) { ob_start();ob_implicit_flush(false);
    if (empty($v)) {
        $v['name'] = '__name__';
        $v['title'] = '__title__';
        $v['value'] = '__value__';
    }
    ?>
    <span class="label j-tag" style="margin:0 2px 2px 2px;min-width:30px;<?= $sortable ? 'cursor:move;' : '' ?>">
        <?= $v['title'] ?>
        <a href="javascript:;" class="j-tag-delete" style="margin-left: 3px;"><i class="icon-remove icon-white" style="margin-top: 0;"></i></a>
        <input class="j-tag-value" type="hidden" name="<?= $v['name'] ?>[]" value="<?= $v['value'] ?>">
    </span>
    <?php return ob_get_clean(); };
# Генерация HTML для поля типа FIELD_TAGS
$tags = function($f) use(& $name, & $tagHTML, & $tip) { ob_start();ob_implicit_flush(false);
    \HTML::attributeAdd($f['attr'], 'type', 'text');
    if ( ! isset($f['attr']['class'])) { \HTML::attributeAdd($f['attr'], 'class', 'stretch'); }
    \HTML::attributeAdd($f['attr'], 'class', 'autocomplete');
    \HTML::attributeAdd($f['attr'], 'class', 'j-autocomplete');
    if ( ! isset($f['attr']['placeholder'])) { \HTML::attributeAdd($f['attr'], 'placeholder', $f['placeholder']); }
?>
    <tr class="j-tags-bl" data-id="<?= $f['id'] ?>" data-name="<?= $name ?>" data-sortable="<?= $f['sortable'] ? 1 : 0 ?>">
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <input<?= \HTML::attributes($f['attr']) ?>/>
            <?= $tip($f) ?>
            <div class="j-tags-list"><?php if ( ! empty($f['values']) && is_array($f['values'])) {
                foreach ($f['value'] as $v) {
                    foreach ($f['values'] as $k => $vv) {
                        if ($vv['value'] == $v) {
                            break;
                        }
                    }
                    $vv['name'] = $name; echo $tagHTML($vv, $f['sortable']);
            } } ?></div>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_AUTOCOMPLETE
$autocomplete = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false);
    if ( ! empty($f['values']) && is_string($f['values'])) {
        \HTML::attributeAdd($f['attr'], 'value', \HTML::escape($f['values']));
    }
    \HTML::attributeAdd($f['attr'], 'type', 'text');
    if ( ! isset($f['attr']['class'])) { \HTML::attributeAdd($f['attr'], 'class', 'stretch'); }
    \HTML::attributeAdd($f['attr'], 'class', 'autocomplete');
    \HTML::attributeAdd($f['attr'], 'class', 'j-autocomplete');
    if ( ! isset($f['attr']['placeholder'])) { \HTML::attributeAdd($f['attr'], 'placeholder', $f['placeholder']); }
?>
    <tr class="j-autocomplete-bl" data-id="<?= $f['id'] ?>">
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <input type="hidden" class="j-value-input" name="<?= $name ?>" value="<?= \HTML::escape($f['value'])  ?>">
            <input<?= \HTML::attributes($f['attr']) ?>/>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean(); };

# Генерация HTML для поля типа FIELD_PASSWORD
$password = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false);
    \HTML::attributeAdd($f['attr'], 'name', $name);
    \HTML::attributeAdd($f['attr'], 'value', str_repeat('*', mb_strlen($f['value'])));
    if ( ! empty($f['value'])) { \HTML::attributeAdd($f['attr'], 'disabled', 'disabled');  }
?>
    <tr>
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <input type="password" <?= \HTML::attributes($f['attr']) ?>/>
            <?php if ( ! empty($f['value'])): ?>
                <a href="javascript:" onclick="$(this).hide().prev().removeProp('disabled').val('').focus();" class=""><i class="icon icon-pencil"></i></a>
            <?php endif; ?>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean();  };

# Генерация HTML для поля типа FIELD_NUMBER
$number = function($f) use(& $name, & $tip) { ob_start();ob_implicit_flush(false);
    \HTML::attributeAdd($f['attr'], 'name', $name);
    \HTML::attributeAdd($f['attr'], 'value', \HTML::escape($f['value']));
?>
    <tr>
        <td class="row1 field-title"><?= $f['title'] ?></td>
        <td class="row2">
            <input type="number" <?= \HTML::attributes($f['attr']) ?>/>
            <?= $tip($f) ?>
            <?= $f['htmlAfter'] ?>
        </td>
    </tr>
<?php return ob_get_clean();  };

# Генерация HTML для поля типа FIELD_DIVIDER
$divider = function($f) { ob_start(); ob_implicit_flush(false);
?>
    <tr>
        <td class="row1" colspan="2" <?= ! empty($f['align']) ? 'style="text-align:'.$f['align'].';"' : '' ?>>
            <?= ! empty($f['content']) ? $f['content'] : '' ?>
        </td>
    </tr>
<?php return ob_get_clean();  };


if ( ! isset($index)) $index = 1; # сквозной индекс для всех групп

# Генерация HTML для блока группа полей внутри типа FIELD_GROUP_FIELDS
$groupBlock = function($groupID, $prefixName, $value = array())
                use(& $fields, & $index, & $name, & $textLang, & $text, & $jwysiwygLang, & $jwysiwyg, & $tags, & $imagesLink, & $filesLink, & $textarea, & $textareaLang,
                    & $select, & $custom, & $autocomplete, & $checkbox, & $checkboxList, & $wysiwygFck, & $wysiwygFckLang, & $number, & $divider) {
    ob_start();ob_implicit_flush(false);
?>
    <div class="well well-sm j-group-fields" data-index="<?= $index ?>" style="margin: 5px 0; padding: 10px 20px 10px 10px;">
        <div class="relative">
            <a href="javascript:" class="j-group-fields-delete" style="position:absolute; right:-17px; top:-8px;"><i class="icon-remove disabled"></i></a>
        </div>
        <table class="admtbl tbledit">
            <tbody><tr>
                <td class="row1" width="135" style="min-height: 1px;"></td>
                <td class="row2" style="min-height: 1px;"></td>
            </tr>
            </tbody>
            <?php
            foreach ($fields as $f) {
                if ($f['group'] != $groupID) continue;
                if (isset($value[ $f['name'] ])) {
                    $f['value'] = $value[ $f['name'] ];
                    $this->fillValues($f);
                }
                $name = $prefixName.'['.$index.']['.$f['name'].']';
                switch ($f['type']) {
                    case Form::FIELD_TEXT:
                        echo $text($f);
                        break;
                    case Form::FIELD_TEXT_LANG:
                        echo $textLang($f);
                        break;
                    case Form::FIELD_TEXTAREA:
                        echo $textarea($f);
                        break;
                    case Form::FIELD_TEXTAREA_LANG:
                        echo $textareaLang($f);
                        break;
                    case Form::FIELD_WYSIWYG:
                        echo $jwysiwyg($f);
                        break;
                    case Form::FIELD_WYSIWYG_LANG:
                        echo $jwysiwygLang($f);
                        break;
                    case Form::FIELD_WYSIWYG_FCK:
                        echo $wysiwygFck($f);
                        break;
                    case Form::FIELD_WYSIWYG_FCK_LANG:
                        echo $wysiwygFckLang($f);
                        break;
                    case Form::FIELD_TAGS:
                        echo $tags($f);
                        break;
                    case Form::FIELD_CHECKBOX:
                        echo $checkbox($f);
                        break;
                    case Form::FIELD_CHECKBOX_LIST:
                        echo $checkboxList($f);
                        break;
                    case Form::FIELD_AUTOCOMPLETE:
                        echo $autocomplete($f);
                        break;
                    case Form::FIELD_IMAGES_LINK:
                        echo $imagesLink($f);
                        break;
                    case Form::FIELD_FILES_LINK:
                        echo $filesLink($f);
                        break;
                    case Form::FIELD_SELECT:
                        echo $select($f);
                        break;
                    case Form::FIELD_NUMBER:
                        echo $number($f);
                        break;
                    case Form::FIELD_DIVIDER:
                        echo $divider($f);
                        break;
                    case Form::FIELD_CUSTOM:
                        echo $custom($f);
                        break;
                }
            }
            ?>
        </table>
        <div class="relative">
            <div class="desc" style="position:absolute; right:-16px; bottom:-8px; cursor: move;"> ↓ ↑ </div>
        </div>
    </div>
    <?php $index++; return ob_get_clean(); };
# Генерация HTML для поля типа FIELD_GROUP_FIELDS
$groupFields = function($f) use(& $name, & $groupBlock, & $tip) { $prefixName = $name; ?>
    <tr class="j-group-fields-bl" data-id="<?= $f['id'] ?>" data-name="<?= $prefixName ?>">
        <td class="row1" colspan="2">
            <div><a href="#" class="ajax j-group-field-add"><?= $f['title'] ?></a><?= $tip($f) ?></div>
            <div class="j-group-fields-list"><?php if ( ! empty($f['value'])) { foreach ($f['value'] as $v) {
                    echo $groupBlock($f['id'], $prefixName, $v);
                } } ?></div>
        </td>
    </tr>
<?php };

if ( ! empty($groupAddMode)) return; # флаг окончания формирования HTML для ajax запроса 'group-fields-block'

# Генерация основного HTML кодя конструктора
$useFCK = false;
?>
<div id="j-extension-settings-form-<?= $u ?>">
    <?php if ( ! empty($tabs)): $tab = reset($tabs);$tab = $tab['name']; $cntTabs = 0; foreach ($tabs as $t) { if ($t['hidden']) continue; $cntTabs++; } ?>
        <?php if ($cntTabs > 0): ?>
            <div class="tabsBar relative">
                <? foreach ($tabs as $t): ?>
                    <span class="tab<?= $t['name'] == $tab ? ' tab-active' : '' ?><?= $t['hidden'] ? ' displaynone' : '' ?>"><a href="javascript:" class="j-setting-tab" data-name="<?= $t['name'] ?>"><?= $t['title'] ?></a></span>
                <? endforeach; ?>
                <span class="progress j-progress" style="display:none; position: absolute; right:0; top:5px;"></span>
            </div>
        <?php else: ?>
            <span class="progress j-progress" style="display:none; position: absolute; right:6px; top:60px;"></span>
        <?php endif; ?>
    <?php foreach ($tabs as $t): ?>
    <div class="<?= $t['name'] != $tab ? ' displaynone' : '' ?> j-setting-tab-bl" data-name="<?= $t['name'] ?>">
        <table class="admtbl tbledit">
            <tbody><tr>
                <td class="row1" width="150" style="min-height: 1px;"></td>
                <td class="row2" style="min-height: 1px;"></td>
            </tr>
            </tbody>
            <?php
            if ( ! empty($fields)) {
                foreach ($fields as $f) {
                    # вставляем код для полей каждого конкретного типа
                    if ($f['group'])  {
                        if (in_array($f['type'], [Form::FIELD_WYSIWYG_FCK, Form::FIELD_WYSIWYG_FCK_LANG])) {
                            $useFCK = true;
                        }
                        continue;
                    }
                    if ($f['tab'] != $t['id']) continue;
                    $name = $t['name'].'['.$f['name'].']';
                    switch ($f['type']) {
                        case Form::FIELD_TEXT:
                            echo $text($f);
                            break;
                        case Form::FIELD_TEXT_LANG:
                            echo $textLang($f);
                            break;
                        case Form::FIELD_TEXTAREA:
                            echo $textarea($f);
                            break;
                        case Form::FIELD_TEXTAREA_LANG:
                            echo $textareaLang($f);
                            break;
                        case Form::FIELD_WYSIWYG:
                            echo $jwysiwyg($f);
                            break;
                        case Form::FIELD_WYSIWYG_LANG:
                            echo $jwysiwygLang($f);
                            break;
                        case Form::FIELD_WYSIWYG_FCK:
                            echo $wysiwygFck($f);
                            $useFCK = true;
                            break;
                        case Form::FIELD_WYSIWYG_FCK_LANG:
                            echo $wysiwygFckLang($f);
                            $useFCK = true;
                            break;
                        case Form::FIELD_IMAGES:
                            echo $images($f);
                            break;
                        case Form::FIELD_FILES:
                            echo $files($f);
                            break;
                        case Form::FIELD_CHECKBOX:
                            echo $checkbox($f);
                            break;
                        case Form::FIELD_CHECKBOX_LIST:
                            echo $checkboxList($f);
                            break;
                        case Form::FIELD_TAGS:
                            echo $tags($f);
                            break;
                        case Form::FIELD_AUTOCOMPLETE:
                            echo $autocomplete($f);
                            break;
                        case Form::FIELD_SELECT:
                            echo $select($f);
                            break;
                        case Form::FIELD_CUSTOM:
                            echo $custom($f);
                            break;
                        case Form::FIELD_PASSWORD:
                            echo $password($f);
                            break;
                        case Form::FIELD_NUMBER:
                            echo $number($f);
                            break;
                        case Form::FIELD_DIVIDER:
                            echo $divider($f);
                            break;
                        case Form::FIELD_GROUP_FIELDS:
                            echo $groupFields($f);
                            break;
                    }
                }
            }
            ?>
        </table>
    </div>
    <?php endforeach;
    endif; ?>
</div>
<script type="text/javascript">
var jExtensionSettingsForm<?= $u ?> = function () {
    var $block, $progress, index = <?= $index ?>, uploader = {}, imageLinkIndex = 0;
    var ajaxUrl = '<?= $this->ajaxUrl() ?>&form_action=';

    $(function() {
        $block = $('#j-extension-settings-form-<?= $u ?>');
        $progress = $block.find('.j-progress');

        var $tabs = $block.find('.j-setting-tab-bl');
        $block.on('click', '.j-setting-tab', function (e) {
            e.preventDefault();
            var $el = $(this);
            $el.parent().addClass('tab-active').siblings().removeClass('tab-active');
            $tabs.addClass('displaynone');
            $tabs.filter('[data-name="'+$el.data('name')+'"]').removeClass('displaynone');
        });

        $block.find('.j-images-upload-bl').each(function () {
            initImages($(this));
        });

        $block.find('.j-files-upload-bl').each(function () {
            initFiles($(this));
        });

        $block.find('.j-checkbox-list-bl').each(function () {
            initCheckboxList($(this));
        });

        $block.find('a.j-image-fb').fancybox();

        $block.find('.j-tags-bl').each(function () {
            initTags($(this));
        });

        $block.find('.j-autocomplete-bl').each(function () {
            initAutocomplete($(this));
        });

        $block.find('.j-group-fields-bl').each(function () {
            initGroupFields($(this));
        });

        $block.find('.j-image-link-bl').each(function () {
            initImageLink($(this));
        });

        $block.find('.j-file-link-bl').each(function () {
            initFileLink($(this));
        });

        $block.find('.j-popover').each(function () {
            initPopover($(this));
        });

        <?php /*
                 Удаляем не прилинковавшиеся изображения для типа FIELD_IMAGES_LINK и файлы для типа FIELD_FILES_LINK
                (можно надобовлять блоков и не сохранитю форму. При следующем сохранении формы удалятся не используемые файлы)
        */ ?>
        $block.find('.j-images-upload-bl.displaynone').each(function () {
            $(this).find('.j-image').each(function() {
                var $el = $(this);
                if ($el.hasClass('linked')) return;
                $el.find('.j-images-delete').trigger('click');
            });
        });
        $block.find('.j-files-upload-bl.displaynone').each(function () {
            $(this).find('.j-file').each(function() {
                var $el = $(this);
                if ($el.hasClass('linked')) return;
                $el.find('.j-files-delete').trigger('click');
            });
        });

        <?php if($useFCK): ?>
        $block.closest('form').find('.j-submit').on('click', function () {
            for (var i in FCKeditorAPI.Instances) {
                if ( ! FCKeditorAPI.Instances.hasOwnProperty(i)) continue;
                FCKeditorAPI.GetInstance(i).UpdateLinkedField();
            }
        });
        <?php endif; ?>

        <?php \bff::hook('extension.settings.form.js.onReady', $this, $u) ?>

    });

    function initImages($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        var $list = $bl.find('.j-images-list');
        var record = intval($bl.data('record'));
        var limit = intval($bl.data('limit'));
        var uploaded = $bl.data('uploaded');
        var $addu = $bl.find('.j-images-add');
        var $add = $addu.parent();
        uploader[record] = new qq.FileUploaderBasic({
            button: $addu.get(0),
            action: ajaxUrl + 'upload-img&record=' + record,
            multiple: true,
            maxConnections: 1,
            limit: limit,
            uploaded: uploaded,
            onSubmit: function(id, fileName) {
                $progress.show();
            },
            onComplete: function(id, fileName, data) {
                if(data && data.success) {
                    var html = <?= \func::php2js($imageHTML()) ?>;
                    html = html.replace(/__filename__/g, data.filename);
                    html = html.replace(/__url_s__/g, data.s);
                    html = html.replace(/__url_o__/g, data.o);
                    html = html.replace(/__record__/g, record);
                    var htmlLink = html;
                    html = html.replace(/__name__/g, $bl.data('name'));
                    $list.append(html);
                    $list.find('a.j-image-fb').fancybox();
                    $list.sortable('refresh');
                    <?php /* Обрабатываем возможно прилинкованные FIELD_IMAGES_LINK */ ?>
                    if (imageLinkIndex) {
                        var $groupBl = $block.find('.j-group-fields[data-index="'+imageLinkIndex+'"]');
                        var $imageLink = $groupBl.find('.j-image-link-bl[data-record="'+record+'"]');
                        var lim = intval($imageLink.data('limit'));
                        htmlLink = htmlLink.replace(/__name__/g, $imageLink.data('name'));
                        htmlLink = htmlLink.replace(/fb-group-/g, 'fb-group-'+imageLinkIndex+'-');
                        htmlLink = htmlLink.replace(/cursor: move;/g, '');
                        var $imagesList = $imageLink.find('.j-image-link-list');
                        $imagesList.append(htmlLink);
                        $imageLink.find('a.j-image-fb').fancybox();
                        $imagesList.sortable('refresh');
                        if ($imagesList.find('.j-image').length >= lim) {
                            $imageLink.find('.j-image-link-add').addClass('displaynone');
                        }
                    }
                    imageLinkIndex = 0;
                } else {
                    if(data.errors) {
                        bff.error( data.errors );
                    }
                }
                if( ! uploader[record].getInProgress()) {
                    $progress.hide();
                }
                if (uploader[record].getUploaded() >= limit) {
                    $add.addClass('displaynone');
                }
                return true;
            }
        });

        $bl.on('click', '.j-images-delete', function (e) {
            e.preventDefault();
            var $el = $(this);
            var $bl = $el.closest('.j-images-upload-bl');
            $list.after('<input type="hidden" name="delete_image_'+$bl.data('record')+'[]" value="'+$el.data('fn')+'" />');
            $el.closest('.j-image').remove();
            uploader[record].decrementUploaded();
            $add.removeClass('displaynone');
        });

        if (intval(uploaded) >= limit) {
            $add.addClass('displaynone');
        }
        $list.sortable();
    }

    function initFiles($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        var $list = $bl.find('.j-files-list');
        var record = intval($bl.data('record'));
        var limit = intval($bl.data('limit'));
        var uploaded = $bl.data('uploaded');
        var $addu = $bl.find('.j-files-add');
        var $add = $addu.parent();
        uploader[record] = new qq.FileUploaderBasic({
            button: $addu.get(0),
            action: ajaxUrl + 'upload-file&record=' + record,
            multiple: true,
            maxConnections: 1,
            limit: limit,
            uploaded: uploaded,
            onSubmit: function(id, fileName) {
                $progress.show();
            },
            onComplete: function(id, fileName, data) {
                if(data && data.success) {
                    var html = <?= \func::php2js($fileHTML()) ?>;
                    html = html.replace(/__filename__/g, data.filename);
                    html = html.replace(/__rfilename__/g, data.rfilename);
                    html = html.replace(/__filesize__/g, data.filesize);
                    html = html.replace(/__url__/g, data.url);
                    html = html.replace(/__record__/g, record);
                    var htmlLink = html;
                    html = html.replace(/__name__/g, $bl.data('name'));
                    $list.append(html);
                    $list.sortable('refresh');
                    <?php /* Обрабатываем возможно прилинкованные FIELD_FILES_LINK */ ?>
                    if (imageLinkIndex) {
                        var $groupBl = $block.find('.j-group-fields[data-index="'+imageLinkIndex+'"]');
                        var $fileLink = $groupBl.find('.j-file-link-bl[data-record="'+record+'"]');
                        var lim = intval($fileLink.data('limit'));
                        htmlLink = htmlLink.replace(/__name__/g, $fileLink.data('name'));
                        var $filesList = $fileLink.find('.j-file-link-list');
                        $filesList.append(htmlLink);
                        $filesList.sortable('refresh');
                        if ($filesList.find('.j-file').length >= lim) {
                            $fileLink.find('.j-file-link-add').parent().addClass('displaynone');
                        }
                    }
                    imageLinkIndex = 0;
                } else {
                    if(data.errors) {
                        bff.error( data.errors );
                    }
                }
                if( ! uploader[record].getInProgress()) {
                    $progress.hide();
                }
                if (uploader[record].getUploaded() >= limit) {
                    $add.addClass('displaynone');
                }
                return true;
            }
        });

        $bl.on('click', '.j-files-delete', function (e) {
            e.preventDefault();
            var $el = $(this);
            var $bl = $el.closest('.j-files-upload-bl');
            $list.after('<input type="hidden" name="delete_file_'+$bl.data('record')+'[]" value="'+$el.data('fn')+'" />');
            $el.closest('.j-file').remove();
            uploader[record].decrementUploaded();
            $add.removeClass('displaynone');
        });

        if (intval(uploaded) >= limit) {
            $add.addClass('displaynone');
        }
        $list.sortable();
    }

    function initImageLink($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        var record = intval($bl.data('record'));
        var $addu = $bl.find('.j-image-link-add');
        var $add = $addu.parent();
        var $list = $bl.find('.j-image-link-list');
        var limit = intval($bl.data('limit'));
        uploader[record]._createUploadButton($addu.get(0));

        <?php /* Запомним для какой группы будем загружать изображения, чтоб потом добавить превью в uploader.onComplete  */ ?>
        $addu.on('change', function (e) {
            imageLinkIndex = $bl.closest('.j-group-fields').data('index');
        });

        $bl.on('click', '.j-images-delete', function (e) {
            e.preventDefault();
            var $el = $(this);
            $add.removeClass('displaynone');
            var $img = $el.closest('.j-image');
            var $orig = $block.find('.j-images-upload-bl[data-record="'+record+'"]').find('.j-image-filename[value="'+$img.find('.j-image-filename').val()+'"]').closest('.j-image');
            $orig.find('.j-images-delete').trigger('click');
            $img.remove();
        });

        <?php /* Склонируем блок с превью изображения внутрь блока FIELD_IMAGES_LINK, строится только на стороне javascript  */ ?>
        var fn = $bl.data('filenames');
        if (fn.length) {
            fn = fn.split(',');
            for (var i in fn) {
                if ( ! fn.hasOwnProperty(i)) continue;
                if ($list.find('.j-image').length >= limit) continue;
                var $orig = $block.find('.j-images-upload-bl[data-record="'+record+'"]').find('.j-image-filename[value="'+fn[i]+'"]').closest('.j-image');
                if ($orig.length) {
                    $orig.addClass('linked');
                    $list.append($orig.clone());
                    var $img = $list.find('.j-image-filename[value="'+fn[i]+'"]').closest('.j-image');
                    $img.find('.j-image-filename').attr('name', $bl.data('name'));
                    $img.find('.j-image-fb').attr('rel', 'fb-group-'+$bl.closest('.j-group-fields').data('index')+'-'+record).css('cursor', 'auto');
                }
            }
            $bl.find('a.j-image-fb').fancybox();
        }
        $list.sortable();
        if ($list.find('.j-image').length >= limit) {
            $add.addClass('displaynone');
        }
    }

    function initFileLink($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        var record = intval($bl.data('record'));
        var $addu = $bl.find('.j-file-link-add');
        var $add = $addu.parent();
        var $list = $bl.find('.j-file-link-list');
        var limit = intval($bl.data('limit'));
        uploader[record]._createUploadButton($addu.get(0));

        <?php /* Запомним для какой группы будем загружать изображения, чтоб потом добавить превью в uploader.onComplete  */ ?>
        $addu.on('change', function (e) {
            imageLinkIndex = $bl.closest('.j-group-fields').data('index');
        });

        $bl.on('click', '.j-files-delete', function (e) {
            e.preventDefault();
            var $el = $(this);
            $add.removeClass('displaynone');
            var $img = $el.closest('.j-file');
            var $orig = $block.find('.j-files-upload-bl[data-record="'+record+'"]').find('.j-file-filename[value="'+$img.find('.j-file-filename').val()+'"]').closest('.j-file');
            $orig.find('.j-files-delete').trigger('click');
            $img.remove();
        });

        <?php /* Склонируем блок с файлом внутрь блока FIELD_FILES_LINK, строится только на стороне javascript  */ ?>
        var fn = $bl.data('filenames');
        if (fn.length) {
            fn = fn.split(',');
            for (var i in fn) {
                if ( ! fn.hasOwnProperty(i)) continue;
                if ($list.find('.j-file').length >= limit) continue;
                var $orig = $block.find('.j-files-upload-bl[data-record="'+record+'"]').find('.j-file-filename[value="'+fn[i]+'"]').closest('.j-file');
                if ($orig.length) {
                    $orig.addClass('linked');
                    $list.append($orig.clone());
                    var $img = $list.find('.j-file-filename[value="'+fn[i]+'"]').closest('.j-file');
                    $img.find('.j-file-filename').attr('name', $bl.data('name'));
                }
            }
        }
        $list.sortable();
        if ($list.find('.j-file').length >= limit) {
            $add.addClass('displaynone');
        }
    }

    function initTags($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        var $list = $bl.find('.j-tags-list');
        var $ac = $bl.find('.j-autocomplete');
        var sortable = intval($bl.data('sortable'));
        var ac = $.autocomplete($ac, ajaxUrl + 'autocomplete&id=' + $bl.data('id'),{
            onShow:function ($dd) { $dd.css('width', $ac.outerWidth() - 1); },
            onSelect: function(id, title, ex){
                id = intval(id);
                if (id && $list.find('.j-tag-value[value="'+id+'"]').length == 0) {
                    var html = <?= \func::php2js($tagHTML()) ?>;
                    html = html.replace(/__value__/g, id);
                    html = html.replace(/__title__/g, title);
                    html = html.replace(/__name__/g, $bl.data('name'));
                    $list.append(html);
                    if (sortable) {
                        $list.sortable('refresh');
                        $list.find('.j-tag:last').css('cursor','move');
                    }
                }
                $ac.val('');
            }
        });

        $bl.on('click', '.j-tag-delete', function (e) {
            e.preventDefault();
            $(this).closest('.j-tag').remove();
        });
        if (sortable) {
            $list.sortable();
        }
    }

    function initAutocomplete($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        var $ac = $bl.find('.j-autocomplete');
        var ac = $.autocomplete($ac, ajaxUrl + 'autocomplete&id=' + $bl.data('id'),{
            valueInput: $bl.find('.j-value-input'),
            onShow:function ($dd) { $dd.css('width', $ac.outerWidth() - 1); }
        });
    }

    function initGroupFields($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        var $list = $bl.find('.j-group-fields-list');

        <?php /* При добавление группы полей HTML код формируем на стороне php */ ?>
        $bl.on('click', '.j-group-field-add', function (e) {
            e.preventDefault();
            index++;
            bff.ajax(ajaxUrl + 'group-fields-block', {id: $bl.data('id'), fname: $bl.data('name'), index:index}, function (data) {
                if (data && data.success) {
                    <?php /* Язык для добавленных полей переключим как у всей формы  */ ?>
                    var $lang = $block.find('.j-lang-togglers:first').find('.j-lang-toggler.active');
                    $list.append(data.html);
                    bff.langTab($lang.data('lng'), '', $lang);

                    var $gr = $list.find('.j-group-fields[data-index="'+data.index+'"]');

                    $gr.find('.j-tags-bl').each(function () {
                        initTags($(this));
                    });

                    $gr.find('.j-autocomplete-bl').each(function () {
                        initAutocomplete($(this));
                    });

                    $gr.find('.j-image-link-bl').each(function () {
                        initImageLink($(this));
                    });

                    $gr.find('.j-file-link-bl').each(function () {
                        initFileLink($(this));
                    });

                    $gr.find('.j-checkbox-list-bl').each(function () {
                        initCheckboxList($(this));
                    });

                    $gr.find('.j-popover').each(function () {
                        initPopover($(this));
                    });

                    <?php \bff::hook('extension.settings.form.js.group.append', $this, $u) ?>

                    $list.sortable('refresh');
                }
            }, $progress);
        });

        $bl.on('click', '.j-group-fields-delete', function (e) {
            e.preventDefault();
            var $gr = $(this).closest('.j-group-fields');
            $gr.find('.j-images-delete').trigger('click');
            $gr.find('.j-files-delete').trigger('click');
            $gr.remove();
        });

        $list.sortable({
            items:'.j-group-fields'
        });
    }

    function initCheckboxList($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        if (intval($bl.data('sortable'))) {
            $bl.find('.j-checkbox-list').sortable();
        }
    }

    function initPopover($bl)
    {
        if ($bl.hasClass('i')) return;
        $bl.addClass('i');
        $bl.popover({container:'body', html:true});
    }

    <?php \bff::hook('extension.settings.form.js.object', $this, $u) ?>

    return {
        <?php \bff::hook('extension.settings.form.js.return', $this, $u) ?>
    }
}();
</script>

