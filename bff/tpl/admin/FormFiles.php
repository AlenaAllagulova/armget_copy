<?php namespace bff\tpl\admin;

use \bff\files\Attachment;
use \bff\utils\Files as UtilsFiles;
use \bff\extend\Extension as Extension;

/**
 * Работа с файлами конструктора для таба настройки
 * @version 0.1
 * @modified 26.sep.2018
 */

class FormFiles extends \Component
{
    /** @var null|Attachment */
    protected $attach = null;

    /** @var string путь к директории хранения файлов */
    protected $path = '';
    /** @var string URL путь к директории хранения файлов */
    protected $url = '';
    /** @var int максимально допустимый объем файла (в байтах) */
    protected $maxSize = 10485760; # 10 mb
    /** @var string список разрешенных расширений файлов */
    protected $extensionsAllowed = 'jpg,jpeg,gif,png,bmp,tiff,ico,odt,doc,docx,docm,xls,xlsx,xlsm,ppt,rtf,pdf,djvu,zip,gzip,gz,7z,rar,txt,xml,mp3,wav,aac,ac3,aif,flac,m4a,m4b,mpa,264,3gp,avi,flv,flc,h264,mkv,mp4,mpe,mpg,mts,swf,webm';
    /** @var boolean сообщать об ошибках */
    protected $assignErrors = false;

    /** @var Extension */
    protected $extension;

    /** @var Form */
    protected $form;

    /** @var boolean
     * true - файлы доступны по прямому URL (хранятся в /public_html/files/extensions/{extension-id}/)
     * false - файлы НЕдоступны по прямому URL (хранятся в /files/extensions/{extension-id}/)
     */
    protected $publicStore = true;

    protected $recordID = 0;

    protected $preload = false;


    public function __construct($settings = array())
    {
        $this->init();
        $this->setSettings($settings);
    }

    /**
     * Инициализация компонента для работы с формой
     * @param Extension $extension
     * @param Form $form
     */
    public function setExtension($extension, $form)
    {
        $this->extension = $extension;
        $this->form = $form;
    }

    /**
     * Применение настроек поля с ID
     * @param integer $fieldID ID поля
     */
    public function setField($fieldID)
    {
        $field = $this->form->getField($fieldID);
        if (empty($field['type']) || $field['type'] != Form::FIELD_FILES) return;

        $this->recordID = $fieldID;
        $this->publicStore = $field['public'];

        $folder = $this->extension->getExtensionId();

        if ($this->isPublicStore()) {
            $this->path = \bff::path('extensions').$folder.DS;
            $this->url = \bff::url('extensions').$folder.DS;
        } else {
            $this->path = PATH_BASE.'files'.DS.'extensions'.DS.$folder.DS;
        }

        if ( ! file_exists($this->path)) {
            UtilsFiles::makeDir($this->path, 0775, true);
        }
        if ( ! file_exists($this->path)) { $this->errors->set($this->extension->langAdmin('Can\'t create folder: [path]', array('path'=>$this->path)));  return; }
        if ( ! is_writable($this->path)) { $this->errors->set($this->extension->langAdmin('Can\'t write to folder: [path]', array('path'=>$this->path))); return; }

        if ( ! empty($field['maxSize'])) {
            $this->maxSize = $field['maxSize'];
        }
        if ( ! empty($field['extensions'])) {
            $this->extensionsAllowed = $field['extensions'];
        }
        $this->preload = ! empty($field['preload']);
    }

    /**
     * Получаем способ хранения файлов
     * @return boolean
     */
    public function isPublicStore()
    {
        return $this->publicStore;
    }

    /**
     * Установка разрешенных расширений
     * @param $extensions
     */
    public function setAllowedExtensions($extensions)
    {
        $this->extensionsAllowed = $extensions;
    }

    /**
     * Инициализация компонента загрузки файлов
     */
    protected function initAttach()
    {
        if ($this->attach) return;
        $this->attach = new Attachment($this->path, $this->maxSize);
        $this->attach->setAllowedExtensions((
        is_array($this->extensionsAllowed) ?
            $this->extensionsAllowed :
            explode(',', $this->extensionsAllowed)
        ));
        $this->attach->setFiledataAsString(false);
        $this->attach->setCheckFreeDiskSpace(false);
        $this->attach->setAssignErrors($this->assignErrors);
    }

    /**
     * Загрузка файла при помощи QQ-загрузчика
     */
    public function uploadQQ()
    {
        $this->initAttach();
        $file = $this->attach->uploadQQ();
        $this->saveData($file);
        return $file;
    }

    /**
     * Данные о загруженных файлах
     * @return array
     */
    public function getData()
    {
        $data = $this->form->config($this->recordID, array());
        if ($this->preload && empty($data)) {
            $data = $this->preloadFiles();
        }
        unset($data['empty']);
        return $data;
    }

    /**
     * Максимальный размер загружаемого файла
     * @param bool $format
     * @param bool $formatExtTitle
     * @return int|string
     */
    public function getMaxSize($format = false, $formatExtTitle = false)
    {
        return ($format ? \tpl::filesize($this->maxSize, $formatExtTitle) : $this->maxSize);
    }

    /**
     * Сохранение данных о загруженном файле
     * @param $file
     */
    public function saveData($file)
    {
        if (empty($file)) return;
        if ( ! empty($file['error']))  return;
        $data = $this->getData();
        unset($file['error']);
        $data[] = $file;
        $this->form->configUpdate($this->recordID, $data);
    }

    /**
     * Формирование url для скачивания файла
     * @param array $file данные о файле
     * @return string
     */
    public function getUrl($file)
    {
        if ($this->isPublicStore()) {
            return $this->url.$file['filename'];
        }
        return 'javascript:';
    }

    /**
     * Формирование пути к файлу
     * @param array $file данные о файле
     * @return bool|string
     */
    public function getPath($file)
    {
        if ( ! isset($file['filename'])) return false;
        return $this->path.$file['filename'];
    }

    /**
     * Сохранение порядка файлов
     * @param array $files
     */
    public function saveOrder($files)
    {
        if (empty($files) ||  ! is_array($files)) return;
        $data = $this->getData();
        $save = array();
        foreach ($files as $v) {
            foreach ($data as $kk => $vv) {
                if ($v == $vv['filename']) {
                    $save[] = $vv;
                    unset($data[$kk]);
                    break;
                }
            }
        }
        foreach ($data as $v) {
            $save[] = $v;
        }

        $this->form->configUpdate($this->recordID, $save);
    }

    /**
     * Удаление файлов
     * @param array $files
     */
    public function deleteFiles($files)
    {
        if (empty($files)) return;
        if ( ! is_array($files)) {
            $files = array($files);
        }

        $data = $this->getData();
        foreach ($data as $k => $v) {
            if (in_array($v['filename'], $files)) {
                $path = $this->getPath($v);
                if (file_exists($path)) {
                    @unlink($path);
                }
                unset($data[$k]);
            }
        }
        if (empty($data)) {
            $data['empty'] = 1;
        }
        $this->form->configUpdate($this->recordID, $data);
    }

    /**
     * Предзагрузка файлов
     * @return array|mixed
     */
    protected function preloadFiles()
    {
        do {
            $field = $this->form->getField($this->recordID);
            if (empty($field['preload'])) break;
            $this->preload = false;
            foreach ($field['preload'] as $v) {
                if ( ! file_exists($v)) continue;
                
                $ext = UtilsFiles::getExtension($v);
                $fn = \func::generator(14, false) . '.' . $ext;
                $path = $this->path.$fn;
                @copy($v, $path);
                if ( ! file_exists($path)) continue;
                $this->saveData([
                    'filename'  => $fn,
                    'filesize'  => filesize($path),
                    'rfilename' => pathinfo($v, PATHINFO_BASENAME),
                    'extension' => $ext,
                ]);
            }
            return $this->getData();
        } while(false);
        return array();
    }


}