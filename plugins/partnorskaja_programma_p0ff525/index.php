<?php

class Plugin_Partnorskaja_programma_p0ff525 extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title' => 'Партнёрская программа',
            'plugin_version' => '{VERSION}',
            'extension_id' => 'p0ff525ea364774cd73f0256e57ba7a532718c5d',
        ));
    }

    protected function start()
    {
        bff::i()->moduleRegister('referalsystem', $this->path('src'));

        bff::autoloadEx(array(
            'SvcHooks' => array('app',
                'plugins' . DS .
                trim(str_replace(PATH_PLUGINS, '', __DIR__)
                    . DS . 'src' . DS . 'svc.hooks.php'),
            )));

        bff::hooks()->routes(function ($routes, $options) {
            return array_merge($routes, [
                'referalsystem_link' => ['pattern' => 'ref/', 'callback' => 'referalsystem/index'],
                'referalsystem_my' => ['pattern' => 'ref/my/', 'callback' => 'referalsystem/my']
            ]);
        }
        );

        bff::hooks()->javascriptExtra(true,
            function () {
                \tpl::includeJS([$this->url('js/clipboard.min.js')]);
            }
        );

        bff::hookAdd('users.user.activated', function ($userID, $userData) {
            Referalsystem::makeAchievement(Referalsystem::ACHIEVEMENT_REGISTRATION, null, $userID);
        });
        bff::hookAdd('users.user.register', function ($userID, $userData) {
            Referalsystem::bindHash($userID);
        });
        if (BFF_PRODUCT == 'do') {
            BbsHooks::svcActivate(function ($svcID, array $svcData, $itemID, array $itemData, array $scvSettings) {
                Referalsystem::makeAchievementBillPayed($svcData['price'], User::id());
                return $svcID;
            });
            ShopsHooks::svcActivate(function ($svcID, array $svcData, $itemID, array $itemData, array $scvSettings) {
                Referalsystem::makeAchievementBillPayed($svcData['price'], User::id());
                return $svcID;
            });
        }
        elseif (BFF_PRODUCT == 'freelance')
        {
            SvcHooks::svcUserActivate(function ($svcID, array $svcData, $itemID, $itemData) {
                Referalsystem::makeAchievementBillPayed($svcData['price'], User::id());
                return $svcID;
            });

            SvcHooks::svcOrderActivate(function ($svcID, array $svcData, $itemID, $itemData) {
                if (is_array($svcData) && ! empty($svcData) && ! isset($svcData['price'])) {
                    $svcData = reset($svcData);
                }
                Referalsystem::makeAchievementBillPayed($svcData['price'], User::id());
                return $svcID;
            });

            SvcHooks::svcShopActivate(function ($svcID, array $svcData, $itemID, $itemData) {
                Referalsystem::makeAchievementBillPayed($svcData['price'], User::id());
                return $svcID;
            });

            bff::hookAdd('users.user.verify', function ($userID, $userData) {
                Referalsystem::makeAchievement(Referalsystem::ACHIEVEMENT_VERIFICATION, null, $userID);
            });

            bff::hookAdd('order.added', function ($orderID, $orderData) {
                Referalsystem::makeAchievementOrder(Referalsystem::ACHIEVEMENT_ORDER_ADDED, $orderID, $orderData);
            });
        }
    }
}