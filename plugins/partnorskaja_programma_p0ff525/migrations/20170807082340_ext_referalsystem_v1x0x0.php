<?php

use bff\db\migrations\Migration as Migration;


class ExtReferalsystemV1x0x0 extends Migration
{
    public function migrate()
    {
        $this->table('referalsystem_users_statuses',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('user_id', 'integer', ['signed' => false, 'null' => true, 'default' => null])
            ->addColumn('referal_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('hash', 'string', ['null' => false, 'limit' => '16'])
            ->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['hash'], ['unique' => true])
            ->addForeignKey(
                ['user_id'],
                TABLE_USERS,
                'user_id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->addForeignKey(
                ['referal_id'],
                TABLE_USERS,
                'user_id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();

        $this->table('referalsystem_achievements',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['uid']])
            ->addColumn('uid', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('bounty', 'float', ['null' => false])
            ->addColumn('is_on', 'boolean', ['null' => false, 'default' => true])
            ->create();

        $this->table(
            'referalsystem_users_achievements',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['user_status_id', 'achievement_id']])
            ->addColumn('user_status_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('achievement_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('bounty', 'float', ['null' => false])
            ->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey(
                ['user_status_id'],
                'referalsystem_users_statuses',
                'id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->addForeignKey(
                ['achievement_id'],
                'referalsystem_achievements',
                'uid',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();

        $this->table('referalsystem_users_keys',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('user_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('hash', 'string', ['null' => false, 'limit' => '32'])
            ->addColumn('blocked', 'boolean', ['null' => false, 'default' => false])
            ->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['hash'], ['unique' => true])
            ->addForeignKey(
                ['user_id'],
                TABLE_USERS,
                'user_id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();

        $this->table('referalsystem_users_hashes',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('user_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('hash', 'string', ['null' => false, 'limit' => '32'])
            ->addColumn('created', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['hash'], ['unique' => true])
            ->addForeignKey(
                ['user_id'],
                TABLE_USERS,
                'user_id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();

        $data = [
            ['uid' => 1, 'bounty' => '0', 'is_on'  => true],
            ['uid' => 2, 'bounty' => '0', 'is_on'  => true],
            ['uid' => 3, 'bounty' => '0', 'is_on'  => true]
        ];

        $this->table('referalsystem_achievements')
            ->insert($data)
            ->save();
    }

    public function rollback()
    {
        $this->dropIfExists('referalsystem_users_keys');
        $this->dropIfExists('referalsystem_users_achievements');
        $this->dropIfExists('referalsystem_achievements');
        $this->dropIfExists('referalsystem_users_statuses');
        $this->dropIfExists('referalsystem_users_hashes');
    }
}
