<?php

use bff\db\migrations\Migration as Migration;

class ExtP0ff525ea364774cd73f0256e57ba7a532718c5dV1x0x1 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $data = [
            ['uid' => 4, 'bounty' => '0', 'is_on'  => true]
        ];

        $this->table('referalsystem_achievements')
            ->insert($data)
            ->save();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {

    }
}