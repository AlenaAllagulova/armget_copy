<?php

if(BFF_PRODUCT == 'freelance') {
    return [
        [
            'file' => 'modules/users/users.class.php',
            'search' => 'case \'finished\':',
            'replace' => 'bff::hook(\'users.user.activated\', User::id(), User::data(\'*\'));',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 1,
            'index' => 1,
        ],
        [
            'file' => 'modules/users/users.adm.class.php',
            'search' => '# триггер активации аккаунта',
            'replace' => 'bff::hook(\'users.user.activated\', $nUserID, $aData);',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 1,
            'index' => 1,
        ],
        [
            'file' => 'modules/users/users.adm.class.php',
            'search' => '$aData[\'verified_reason\'] = \'\';',
            'replace' => ' if($aData[\'verified\'] == static::VERIFIED_STATUS_APPROVED) {
                            bff::hook(\'users.user.verify\', $nUserID, $aData);
                           }',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 1,
            'index' => 1,
        ],
        [
            'file' => 'modules/fairplay/fairplay.bl.class.php',
            'search' => 'public function svcActivate($workflowID, $svcID, $svcData = false, array &$settings = array())',
            'replace' => '# хуки
                    $customActivation = bff::filter(\'fairplay.svc.activate\', $svcID, $svcData, $workflowID, $settings);
                    if (is_bool($customActivation)) {
                        return $customActivation;
                    }
                    ',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 1,
            'index' => 1,
        ],
        [
            'file' => 'modules/orders/orders.bl.class.php',
            'search' => '$svcData = Svc::model()->svcData($svcData, \'id, keyword, settings, type\');',
            'replace' => '$svcData = Svc::model()->svcData($svcData, \'id, keyword, settings, price, type\');',
            'replace-file' => false,
            'position' => 'replace',
            'offset' => 0,
            'index' => 1,
        ],
        [
            'file' => 'modules/orders/orders.class.php',
            'search' => '$nOrderID = $this->model->orderSave(0, $aData, \'d\');',
            'replace' => 'bff::hook(\'order.added\', $nOrderID, $aData);',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 15,
            'index' => 1,
        ],
        [
            'file' => 'modules/orders/orders.adm.class.php',
            'search' => 'Users::sendMailTemplateToUser($data[\'user_id\'], \'orders_order_approved\', array(',
            'replace' => 'bff::hook(\'order.added\', $orderID, array_merge($data, $aUpdate));',
            'replace-file' => false,
            'position' => 'before',
            'offset' => 1,
            'index' => 1,
        ],
        [
            'file' => 'tpl/header.php',
            'search' => '\'my.settings\' => Users::url(\'my.settings\'),',
            'replace' => '\'my.referal\' => Referalsystem::url(\'list\'),',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 0,
            'index' => 1,
        ],
        [
            'file' => 'tpl/header.php',
            'search' => '<li><a href="<?= $url[\'my.settings\'] ?>"><i class="fa fa-cogs"></i> <?= _t(\'users\', \'Настройки\'); ?></a></li>',
            'replace' => '<li><a href="<?= $url[\'my.referal\'] ?>"><i class="fa fa-handshake-o" aria-hidden="true"></i> <?= _t(\'users\', \'Реферальная система\'); ?></a></li>',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 0,
            'index' => 1,
        ],
        [
            'file' => 'modules/shop/shop.bl.class.php',
            'search' => '$aUpdate = array(\'svc\' => $aData[\'svc\']);',
            'replace' => '# хуки
                    $customActivation = bff::filter(\'shop.svc.activate\', $nSvcID, $svcData, $nProductID, $aSvcSettings);
                    if (is_bool($customActivation)) {
                        return $customActivation;
                    }
                    ',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 1,
            'index' => 1,
        ],
        [
            'file' => 'modules/users/users.bl.class.php',
            'search' => '$this->userAuth($nUserID, \'user_id\', $aData[\'password\']);',
            'replace' => 'bff::hook(\'users.user.register\', $nUserID, $aData, array(\'pass\'=>$sPassword, \'activation\'=>$aActivation));',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 2,
            'index' => 1,
        ],
        [
            'file' => 'modules/users/users.bl.class.php',
            'search' => 'switch($nSvcID){',
            'replace' => '# хуки
        $customActivation = bff::filter(\'user.svc.activate\', $nSvcID, $aSvcData, $nUserID, $aSvcSettings);
        if (is_bool($customActivation)) {
            return $customActivation;
        }',
            'replace-file' => false,
            'position' => 'before',
            'offset' => 2,
            'index' => 1,
        ],
    ];
} elseif (BFF_PRODUCT == 'do') {
    return [
        [
            'file' => 'modules/users/users.class.php',
            'search' => 'case \'finished\':',
            'replace' => 'bff::hook(\'users.user.activated\', User::id(), User::data(\'*\'));',
            'replace-file' => false,
            'position' => 'after',
            'offset' => 1,
            'index' => 1,
        ],
    ];
}
