<?php
class SvcHooks {
    /**
     * Хук успешной активации платной услуги пользователя
     * @see \Svc::activate
     * @param callable $callback {
     *  @param $nSvcID,
     *  @param $aSvcData,
     *  @param $nUserID,
     *  @param $aSvcSettings
     * }
     * @param int|null $priority приоритет вызова
     * @return \Hook
     */
    public static function svcUserActivate(callable $callback, $priority = NULL)
    {
        return bff::hooks()->add('user.svc.activate', $callback, $priority);
    }

    /**
     * Хук успешной активации платной услуги безопасной сделки
     * @see \Svc::activate
     * @param callable $callback {
     *  @param $nSvcID,
     *  @param $aSvcData,
     *  @param $nWorkflowID,
     *  @param $aSvcSettings
     * }
     * @param int|null $priority приоритет вызова
     * @return \Hook
     */
    public static function svcFairplayActivate(callable $callback, $priority = NULL)
    {
        return bff::hooks()->add('fairplay.svc.activate', $callback, $priority);
    }

    /**
     * Хук успешной активации платной услуги заказа
     * @see \Svc::activate
     * @param callable $callback {
     *  @param $nSvcID,
     *  @param $aSvcData,
     *  @param $nOrderID,
     *  @param $aSvcSettings
     * }
     * @param int|null $priority приоритет вызова
     * @return \Hook
     */
    public static function svcOrderActivate(callable $callback, $priority = NULL)
    {
        return bff::hooks()->add('orders.svc.activate', $callback, $priority);
    }

    /**
     * Хук успешной активации платной услуги магазина
     * @see \Svc::activate
     * @param callable $callback {
     *  @param $nSvcID,
     *  @param $aSvcData,
     *  @param $nProductID,
     *  @param $aSvcSettings
     * }
     * @param int|null $priority приоритет вызова
     * @return \Hook
     */
    public static function svcShopActivate(callable $callback, $priority = NULL)
    {
        return bff::hooks()->add('shop.svc.activate', $callback, $priority);
    }
}