<?php

class Referalsystem extends ReferalsystemBase
{
    /**
     * Module config
     * @return null|string
     */
    public function config()
    {
        if(Request::isAJAX()) {
            $aResponse = array();
            $aConfig = $this->input->postgetm(self::configKeys());
            $this->configSave($aConfig);
            $this->ajaxResponseForm($aResponse);
            return null;
        }
        $aData = self::configGet();
        return $this->viewPHP($aData, 'admin.config');
    }

    /**
     * List achievements
     * @return null|string
     */
    public function achievementsListing()
    {
            if(Request::isAJAX()) {
            $aResponse = array();
            $aParams = array(
                'is_on' => TYPE_ARRAY_BOOL,
                'bounty' => TYPE_ARRAY_PRICE
            );
            $aConfig = $this->input->postgetm($aParams);
            foreach (self::ACHIEVEMENTS() as $key => $val) {
                $bIsOn = isset($aConfig['is_on'][$key]);
                $this->model->achievementSave(
                    $key,
                    array(
                        'is_on' => $bIsOn,
                        'bounty' => $aConfig['bounty'][$key])
                );
            }
            $this->ajaxResponseForm($aResponse);
            return null;
        }
        $aAchievements = $this->model->achievementsList();
        $aData = array('achievements' => $aAchievements);
        return $this->viewPHP($aData, 'admin.achievements.config');
    }

    /**
     * List user statuses
     * @return string
     */
    public function usersListing()
    {
        $aData = array();
        $nTotal = $this->model->usersListing(array(), true);
        $oPgn = new Pagination($nTotal, self::ADMIN_PAGE_SIZE, $this->adminLink('usersListing' . '&page=' . Pagination::PAGE_ID));
        $aData['items'] = $this->model->usersListing(array(), false, $oPgn->getLimitOffset());
        $aData['pgn'] = $oPgn->view();
        return $this->viewPHP($aData, 'admin.users.listing');
    }

    /**
     * Referal links
     * @return string
     */
    public function linksListing()
    {
        $aData = array();
        $nTotal = $this->model->userLinkListing(array(), true);
        $oPgn = new Pagination($nTotal, self::ADMIN_PAGE_SIZE, $this->adminLink('linksListing' . '&page=' . Pagination::PAGE_ID));
        $aData['items'] = $this->model->userLinkListing(array(), false, $oPgn->getLimitOffset());
        $aData['pgn'] = $oPgn->view();
        return $this->viewPHP($aData, 'admin.keys.listing');
    }

    /**
     * Block referal link (ajax)
     */
    public function blockKey()
    {
        $nKeyID = $this->input->postget('id', TYPE_UINT);
        $aResponse = array();
        $aResponse['success'] = $this->model->blockChangeUserLink($nKeyID);
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Delete referal link
     */
    public function deleteKey()
    {
        $nKeyID = $this->input->postget('id', TYPE_UINT);
        $aResponse = array();
        $aResponse['success'] = $this->model->deleteUserLink($nKeyID);
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Delete user
     * @return null
     */
    public function deleteUser()
    {
        $nKeyID = $this->input->postget('id', TYPE_UINT);
        $aResponse = array();
        $aUserStatusData = $this->model->usersListing(array('id' => $nKeyID));
        if(!empty($aUserStatusData)) {
            $aUserStatusData = reset($aUserStatusData);
            if(!empty($aUserStatusData['user_id'])) return null;
        }
        $aResponse['success'] = $this->model->deleteUser($nKeyID);
        $this->ajaxResponseForm($aResponse);
        return null;
    }
}