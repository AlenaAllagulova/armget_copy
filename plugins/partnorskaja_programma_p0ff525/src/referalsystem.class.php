<?php

class Referalsystem extends ReferalsystemBase
{
    /**
     * Referal link requests dispatcher
     */
    public function index()
    {
        $sCurrentHash = $this->input->cookie(self::COOKIE_NAME, TYPE_NOTAGS);
        $sReferalHash = $this->input->get(self::HASH_GET_NAME, TYPE_NOTAGS);

        if(empty($sCurrentHash) && !User::id() && !empty($sReferalHash)) {
            $sCurrentHash = func::generator(0x10);

            $configUserLifetime = $this->configGet();
            $configUserLifetime = $configUserLifetime[self::CONFIG_GLOBAL_USER_LIFETIME];

            Request::setCOOKIE(self::COOKIE_NAME, $sCurrentHash, $configUserLifetime);
            $this->model->addFreshReferalUser($sReferalHash, $sCurrentHash);
        }

        $this->redirect(bff::urlBase());
    }

    /**
     * User referal program listing
     * @return null|string
     */
    public function my()
    {
        $nUserID = User::id();
        if(!$nUserID) {
            $this->errors->error404();
            return null;
        }
        $aData = array();
        $aListData = array();

        $nPage= $this->input->get('page', TYPE_UINT);

        $nCount = $this->model->usersListing(array('referal_id' => $nUserID), true);

        $oPgn = new Pagination(
            $nCount,
            self::FRONTEND_PAGE_SIZE,
            array('link' => self::url('list'),
                'query' => array('page' => $nPage))
        );
        $oPgn->setCurrentPage($nPage);
        $aData['pgn'] = $oPgn->view();

        $aItems = $this->model->usersListing(array('referal_id' => $nUserID), false, $oPgn->getLimitOffset());
        $aListData['items'] = $aItems;

        $aData['list'] = $this->viewPHP($aListData, 'my.list');
        return $this->viewPHP($aData, 'my');
    }

    /**
     * Info about user achievements (ajax)
     */
    public function achievementInfo()
    {
        $nUserStatusID = $this->input->postget('id', TYPE_UINT);
        $aResponse = array();
        $aData = array();
        $aData['items'] = $this->model->userAchievementsList($nUserStatusID);
        $aResponse['modal'] = $this->viewPHP($aData, 'achievement.info.modal');
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Clear old users statuses (cron)
     */
    public function clearOldReferalsCron()
    {
        if(!bff::cron()) return false;
        $aConfig = $this->configGet();
        $this->model->clearOldReferals($aConfig[self::CONFIG_GLOBAL_USER_LIFETIME]);
    }

    /**
     * Cron tasks
     * @return array
     */
    public function cronSettings()
    {
        return array(
            'clearOldReferalsCron' => array('period' => '0 0 * * *'),
        );
    }

    /**
     * Bind User::id() to Hash
     * @param $userID
     * @return int
     */
    public static function bindHash($userID)
    {
        $sHash = self::i()->input->cookie(self::COOKIE_NAME, TYPE_NOTAGS);
        return self::model()->bindHash($userID, $sHash);

    }

    /**
     * Link to user's profile
     * @param array $user @ref user data
     * @param string $type link type:
     *  text - without link,
     *  strong - <strong>,
     *  no-login - do not write login,
     *  no-surname - do not write surname,
     *  icon - icon
     * @param string $tab profile tab
     * @param array $attr tag attributes
     * @return string
     */
    public static function userLink(array &$user, $type = '', $tab = '', array $attr = array())
    {
        foreach (['name','surname','login'] as $k) {
            if (!isset($user[$k])) $user[$k] = '';
        }

        # surname
        $surname = (strpos($type,'no-surname') !== false || empty($user['surname'])
            ? ''
            : ' '.$user['surname']);

        # <a>
        $prefA = '';
        $postA = '';
        if (strpos($type, 'text') === false) {
            $attr['href'] = Users::url('profile', array('login' => $user['login'], 'tab' => $tab));
            $prefA = '<a '.HTML::attributes($attr).'>';
            $postA = '</a>';
        }

        # strong
        $prefTag = '';
        $postTag = '';
        if (strpos($type, 'strong') !== false) {
            $prefTag = '<strong>';
            $postTag = '</strong>';
        }

        # login
        $login = ' ['.$user['login'].']';
        if (isset($user['login_title'])) {
            $login = ' ['.$user['login_title'].']';
        }
        if (strpos($type, 'no-login') !== false) {
            if ( ! empty($user['name']) || ! empty($surname)) {
                $login = '';
            }
        }

        # icon
        $icon = '';
        if (strpos($type, 'icon') !== false) {
            $icon = '<i class="fa fa-user c-link-icon"></i>';
        }

        return $prefA.$prefTag.$icon.$user['name'].$surname.$login.$postTag.$postA;
    }

}