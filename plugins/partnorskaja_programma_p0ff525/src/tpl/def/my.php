<?
    $nUserID = User::id();
    $sHash = $this->getReferalLink($nUserID);
    $sLink = !empty($sHash) ? Referalsystem::url('link', array('hash' => $sHash)) : false;
?>

<div class="container referal-key-page-container">
    <div class="row">
        <div class="col-sm-12">
            <? if(!empty($sLink)): ?>
            <div class="my-referal-key form-group">
                <span class="referal-key-title"><?= _t('referal-system', 'Ваша реферальная ссылка') ?>:</span>
                <input id="copy" class="referal-key-input form-control mrgb10" style="opacity: 1; cursor:text;  display: inline-block; max-width: 320px;" type="text" readonly="true" value="<?= $sLink ?>"/>

                <button class="j-btn-copy btn" data-clipboard-target="#copy">
                    <i class="fa fa-clipboard" aria-hidden="true"></i><?= _t('referal-system','Копировать') ?>
                </button>

                <div class="c-fixedAlerts alert alert-success copied" style="display: none;"><?= _t('bills', 'Ссылка скопирована в буфер обмена'); ?></div>
            </div>
            <? endif; ?>
        </div>
    </div>
    <div><?= $list ?></div>
    <div><?= $pgn ?></div>
</div>
<script>
    <? js::start() ?>
        var copy = new Clipboard('.j-btn-copy');

        copy.on('success', function(e) {
            $('.copied').show();
            $('.copied').fadeOut(5000);
        });

        $('.referal-key-input').focus(function () {
            if(this.value == this.defaultValue){
                this.select();
            }
        });
    <? js::stop() ?>
</script>
