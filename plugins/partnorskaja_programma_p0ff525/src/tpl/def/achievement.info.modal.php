<?
$aAchivements = Referalsystem::ACHIEVEMENTS();
$sCurrency = Site::currencyDefault();
?>
<div class="modal fade ref-users-modal" id="user-info-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('referal-system','Полученное вознаграждение') ?></h4>
            </div>
            <div class="modal-body">
                <ul class="ref-reward-list">
                    <? foreach ($items as $item): ?>
                        <li>
                            <span><?= $aAchivements[$item['achievement_id']]['title'] ?> - </span>
                            <span class="bounty"><?= $item['bounty'] ?></span>
                            <span><?= $sCurrency ?></span>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= _t('referal-system','Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>