<? $aKeys = array(
    'created' => array('title' => _t('referal-system', 'Дата')),
    'status ' => array('title' => _t('referal-system', 'Статус')),
    'name'    => array('title' => _t('referal-system', 'Реферал')),
); ?>
<p class="title-ref-users-table">
    <?= _t('referal-system','Привлеченные пользователи') ?>
</p>
<div class="table-responsive list-ref-users">
    <table class="table">
        <thead>
            <tr>
                <? foreach ($aKeys as $key): ?>
                    <td><?= $key['title'] ?></td>
                <? endforeach; ?>
            </tr>
        </thead>
        <? foreach ($items as $user): ?>
            <tr>
                <td><?= $user['created'] ?></td>
                <td>
                    <a class="j-get-info-ref-user" data-id="<?= $user['user_status_id'] ?>" href="#">
                        <?= empty($user['user_id']) ? _t('referal-system', 'Не зарегистрирован') : 'Зарегистрирован' ?>
                    </a>
                </td>
                <td>
                    <? if(!empty( $user['login'])): ?>
                        <a href="<?= Users::url('profile', array('login' => $user['login'])); ?>" style="display: inline-block; width: 30px;height: 30px;">
                            <img style="border-radius: 50%;" src="<?= UsersAvatar::url($user['user_id'], $user['avatar'], UsersAvatar::szSmall) ?>" ?>
                        </a>
                    <? else: ?>
                    <? endif; ?>
                    <?= !empty($user['user_id']) ? Referalsystem::userLink($user) : '-' ?>
                </td>
            </tr>
        <? endforeach; ?>
    </table>
    <div class="ref-modal-box"></div>
</div>

<script type="text/javascript">
    <? js::start() ?>
        $('.j-get-info-ref-user').on('click', function () {
            var id = $(this).attr('data-id');
            getInfo(id);
        });
        function getInfo(id) {
            nothing();
            bff.ajax(bff.ajaxURL('referalsystem&ev=achievementInfo', ''), {id:id}, function (resp) {
                if(resp && resp.success) {
                    var modal = $(resp.modal);
                    modal.appendTo('.ref-modal-box');
                    var modalContentBox = $('#user-info-modal');
                    modalContentBox.on('hidden.bs.modal', function () {
                        $(this).remove();
                    });
                    modalContentBox.modal();
                }
            });
        }
    <? js::stop() ?>
</script>

