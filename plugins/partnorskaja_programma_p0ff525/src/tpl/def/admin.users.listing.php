<?=
    tplAdmin::blockStart(
    _t('referal-system', 'Пользователи'),
    true,
    array('class'=>(!empty($act) ? 'hidden' : '')));
?>

    <table class="table table-condensed table-hover admtbl" id="j-list">
        <thead>
        <tr class="">
            <?
            $aHeaderCols = array(
                'id' => array('t'=>_t('referal-system', 'ID'),'w'=>60,'order'=>'desc'),
                'user_id' => array('t'=>_t('referal-system', 'Реферал'),'w'=>165,'align'=>'left'),
                'hash' => array('t'=>_t('referal-system', 'Идентификатор'),'w'=>125,'align'=>'left'),
                'referal_id' => array('t'=>_t('referal-system', 'Пользователь'),'w'=>125,'align'=>'left'),
                'created' => array('t'=>_t('referal-system', 'Дата'),'w'=>125,'align'=>'left'),
                'action' => array('t'=>_t('referal-system', 'Действие'),'w'=>85)
            );
            ?>
            <? foreach($aHeaderCols as $k=>$v) : ?>
                <th
                    <? if( ! empty($v['w']) ) : ?>
                        width="<?= $v['w'] ?>"
                    <? endif; ?>
                    <? if( ! empty($v['align']) ) : ?>
                        class="<?= $v['align'] ?>"
                    <? endif; ?>
                >
                    <?= $v['t']; ?>
                </th>
            <? endforeach; ?>
        </tr>
        </thead>
        <? foreach($items as $k=>$v) :
                $id = $v['user_status_id'];
                $aReferalData = array('name' => $v['referal_name'], 'surname' => $v['referal_surname'], 'login' => $v['referal_login']);
            ?>
            <tr class="row<?= $k%2 ?>">
                <td><?= $id ?></td>
                <td class="left">
                    <a href="#" onclick="<?= !empty($v['user_id']) ? 'return bff.userinfo('. $v['user_id'].');' : 'nothing();' ?>">
                        <?= !empty($v['user_id']) ? Referalsystem::userNameFormated($v) : _t('referal-system', 'Не зарегистрирован') ?>
                    </a>
                </td>
                <td class="left"><?= $v['hash'] ?></td>
                <td class="left">
                    <a href="#" onclick="<?= !empty($v['referal_id']) ? 'return bff.userinfo('. $v['referal_id'].');' : '' ?>">
                        <?= Referalsystem::userNameFormated($aReferalData) ?>
                    </a>
                </td>
                <td class="left"><?= $v['created'] ?></td>
                <td>
                    <? if(empty($v['user_id'])): ?>
                        <a class="but del item-del" onclick="nothing(); delUser(<?= $id ?>)"></a>
                    <? endif; ?>
                </td>
            </tr>
        <? endforeach; if( empty($items) ) { ?>
            <tr class="norecords">
                <td colspan="<?= sizeof($aHeaderCols) ?>"><?= _t('referal-system', 'Нет пользователей') ?></td>
            </tr>
        <? } ?>
    </table>

    <?= $pgn; ?>

    <script type="text/javascript">
        <? js::start() ?>
        function delUser(id) {
            bff.ajax(bff.adminLink('deleteUser', 'referalsystem'), {id: id}, function(resp) {
                if(resp && resp.success) {
                    location.reload();
                }
            })
        }
        <? js::stop() ?>
    </script>

<?= tplAdmin::blockStop() ?>