<?=
    tplAdmin::blockStart(
    _t('referal-system', 'Пользователи'),
    true,
    array('class'=>(!empty($act) ? 'hidden' : '')));
?>

    <table class="table table-condensed table-hover admtbl" id="j-list">
        <thead>
        <tr class="">
            <?
            $aHeaderCols = array(
                'id' => array('t'=>_t('referal-system', 'ID'),'w'=>60,'order'=>'desc'),
                'user_id' => array('t'=>_t('referal-system', 'Пользователь'),'w'=>165,'align'=>'left'),
                'hash' => array('t'=>_t('referal-system', 'Ссылка'),'w'=>125,'align'=>'left'),
                'created' => array('t'=>_t('referal-system', 'Дата'),'w'=>125,'align'=>'left'),
                'action' => array('t'=>_t('referal-system', 'Действие'),'w'=>85)
            );
            ?>
            <? foreach($aHeaderCols as $k=>$v) : ?>
                <th
                    <? if( ! empty($v['w']) ) : ?>
                        width="<?= $v['w'] ?>"
                    <? endif; ?>
                    <? if( ! empty($v['align']) ) : ?>
                        class="<?= $v['align'] ?>"
                    <? endif; ?>
                >
                    <?= $v['t']; ?>
                </th>
            <? endforeach; ?>
        </tr>
        </thead>
        <? foreach($items as $k=>$v) :
                $id = $v['user_key_id'];
            ?>
            <tr class="row<?= $k%2 ?>">
                <td><?= $id ?></td>
                <td class="left">
                    <a href="#" onclick="<?= !empty($v['user_id']) ? 'return bff.userinfo('. $v['user_id'].');' : 'nothing();' ?>">
                        <?= !empty($v['user_id']) ? Referalsystem::userNameFormated($v) : _t('referal-system', 'Не зарегистрирован') ?>
                    </a>
                </td>
                <td class="left"><?= Referalsystem::url('link', array(Referalsystem::HASH_GET_NAME => $v['hash'])) ?></td>
                <td class="left"><?= $v['created'] ?></td>
                <td>
                    <a class="but del <?= $v['blocked'] ? 'block' : 'unblock' ?>" onclick="nothing(); blockKey(<?= $id ?>)" id="block-btn-<?= $id ?>"></a>
                    <a class="but del item-del" onclick="nothing(); delKey(<?= $id ?>)"></a>
                </td>
            </tr>
        <? endforeach; if( empty($items) ) { ?>
            <tr class="norecords">
                <td colspan="<?= sizeof($aHeaderCols) ?>"><?= _t('referal-system', 'Нет пользователей') ?></td>
            </tr>
        <? } ?>
    </table>

    <?= $pgn; ?>

    <script type="text/javascript">
        <? js::start() ?>
        function delKey(id) {
            bff.ajax(bff.adminLink('deleteKey', 'referalsystem'), {id: id}, function(resp) {
                if(resp && resp.success) {
                    location.reload();
                }
            })
        }
        function blockKey(id) {
            bff.ajax(bff.adminLink('blockKey', 'referalsystem'), {id: id}, function(resp) {
                if(resp && resp.success) {
                    var btn = $('#block-btn-'+id);
                    if(btn.hasClass('block')) {
                        btn.removeClass('block');
                        btn.addClass('unblock');
                    } else {
                        btn.removeClass('unblock');
                        btn.addClass('block');
                    }
                }
            })
        }
        <? js::stop() ?>
    </script>

<?= tplAdmin::blockStop() ?>