<?
    $aTabs = array();
    foreach (Referalsystem::ACHIEVEMENTS() as $achievement => $config) {
        $aTabs[$achievement] = $config['title'];
    }
    $tabKeys = array_keys($aTabs);
    $tab = reset($tabKeys);
?>

<? if(count($aTabs) > 1): ?>
<div class="tabsBar">
    <? foreach($aTabs as $k=>$v): ?>
        <span class="tab<?= $k==$tab ? ' tab-active' : '' ?> menu-tab-<?= $k ?>"><a href="#" class="tab-link" data-tab="<?= $k ?>"><?= $v ?></a></span>
    <? endforeach; ?>
    <div class="progress" style="margin-left: 5px; display: none;" id="form-progress"></div>
</div>
<? endif; ?>

<form name="ReferalSystemAchievementsConfigForm" id="jReferalSystemAchievementsConfigForm" action="<?= $this->adminLink(null) ?>" method="get">
    <table class="admtbl tbledit">
        <? foreach ($aTabs as $k => $tabTitle): $config = Referalsystem::ACHIEVEMENTS()[$k]?>
            <tr class="tab-<?= $k ?> <?= $k==$tab ? '' : 'hidden' ?>">
                <td>
                    <table>
                        <? if(!isset($config['global_configured']) || !$config['global_configured']): ?>
                            <tr>
                                <td class="row1 field-title"><?= _t('referal-system', 'Вознаграждение') ?></td>
                                <td class="row2">
                                    <input type="number" name="bounty[<?= $k ?>]" value="<?= $achievements[$k]['bounty'] ?>"/>
                                </td>
                            </tr>
                        <? else: ?>
                            <tr>
                                <td class="row1 field-title">
                                    <a href="<?= $this->adminLink('config', 'referalsystem') ?>"><?= _t('referal-system', 'Настраивается глобально') ?></a>
                                    <input type="hidden" name="bounty[<?= $k ?>]" value="0"/>
                                </td>
                            </tr>
                        <? endif; ?>
                        <tr>
                            <td class="row1 field-title"><?= _t('referal-system', 'Включена') ?></td>
                            <td class="row2">
                                <input type="checkbox" value="true" name="is_on[<?= $k ?>]" <?= $achievements[$k]['is_on'] ? 'checked' : '' ?>/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <? endforeach; ?>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" name="saveBtn" class="btn btn-success button submit" value="<?= _t('referal-system', 'Save') ?>" />
                <input type="button" name="cancelBtn" class="btn button cancel" value="<?= _t('referal-system', 'Cancel') ?>" />
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    <? js::start() ?>
    function ReferalSystemAchievementsConfigForm() {
        this.form = $('#jReferalSystemAchievementsConfigForm');
        this.tabBar = $('div.tabsBar');
        this.tab = <?= $tab ?>;
        this.ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        var self = this;

        $("input[name='saveBtn']").click(function(e) {
            nothing(e);
            self.Save();
        });

        $("input[name='cancelBtn']").click(function(e) {
            self.Cancel();
        });
        $(".tab-link").click(function(e) {
            nothing(e);
            self.Tab($(this).data('tab'));
        });
    }

    ReferalSystemAchievementsConfigForm.prototype = {
        Save: function() {
            var data = this.form.serialize();
            bff.ajax(this.ajaxUrl, data, function(data, error){
                if(data && data.success) {
                    bff.success('<?= _t('referal-system','Данные успешно сохранены') ?>');
                } else {
                    bff.error(error);
                }
            });
        },
        Cancel: function() {
            location.reload();
        },
        Tab: function(tab) {

            $('tr.tab-'+this.tab).hide();
            this.tabBar.find('span.tab-active').removeClass('tab-active');

            $('tr.tab-'+tab).show();
            this.tabBar.find('span.menu-tab-'+tab).addClass('tab-active');

            this.tab = tab;
            console.log(this.tab);
        }
    };
    new ReferalSystemAchievementsConfigForm();
    <? js::stop() ?>
</script>