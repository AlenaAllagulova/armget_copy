<form name="ReferalSystemConfigForm" id="jReferalSystemConfigForm" action="<?= $this->adminLink(null) ?>" method="get">
    <table class="admtbl tbledit">
        <? foreach (Referalsystem::configData() as $key => $val): ?>
            <? if($val['type'] == TYPE_BOOL):?>
                <tr class="required">
                    <td class="row1 field-title" width="100"><?= $val['label'] ?></td>
                    <td class="row2">
                        <input type="checkbox" name="<?= $key ?>" value="true" <?= $$key ? 'checked' : '' ?>/>
                    </td>
                </tr>
            <? elseif ($val['type'] == TYPE_UINT): ?>
                <tr class="required">
                    <td class="row1 field-title"><?= $val['label'] ?></td>
                    <td class="row2">
                        <input type="number" name="<?= $key ?>" value="<?= $$key ?>"/>
                        <? if(!is_null($val['units'])): ?><span><?= $val['units'] ?></span><? endif; ?>
                    </td>
                </tr>
            <? endif; ?>
        <?  endforeach; ?>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" name="saveBtn" class="btn btn-success button submit" value="<?= _t('referal-system', 'Save') ?>"/>
                <input type="button" name="cancelBtn" class="btn button cancel" value="<?= _t('referal-system', 'Cancel') ?>"/>
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    <? js::start() ?>
    function ReferalSystemConfigForm() {
        this.form = $('#jReferalSystemConfigForm');
        this.ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        var self = this;

        $("input[name='saveBtn']").click(function(e) {
            nothing(e);
            self.Save();
        });

        $("input[name='cancelBtn']").click(function(e) {
            self.Cancel();
        });
    }

    ReferalSystemConfigForm.prototype = {
        Save: function() {
            var data = this.form.serialize();
            bff.ajax(this.ajaxUrl, data, function(data, error){
                if(data && data.success) {
                    bff.success('<?= _t('referal-system','Данные успешно сохранены') ?>');
                } else {
                    bff.error(error);
                }
            });
        },
        Cancel: function() {
            location.reload();
        }
    };
    new ReferalSystemConfigForm();
    <? js::stop() ?>
</script>
