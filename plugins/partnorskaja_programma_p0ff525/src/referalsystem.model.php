<?php

class ReferalsystemModel extends Model
{
    /** @var ReferalsystemBase */
    public $controller;

    const TABLE_ACHIEVEMENTS = 'referalsystem_achievements';
    const TABLE_USERS_STATUSES = 'referalsystem_users_statuses';
    const TABLE_USERS_ACHIEVEMENTS = 'referalsystem_users_achievements';
    const TABLE_USERS_KEYS = 'referalsystem_users_keys';
    const TABLE_USERS_HASHES = 'referalsystem_users_hashes';

    /**
     * Save achievement
     * @param $nAchievementID
     * @param $aData
     * @return bool
     */
    public function achievementSave($nAchievementID, $aData)
    {
        if(isset($aData['uid'])) {
            unset($aData['uid']);
        }
        return $this->db->update(self::TABLE_ACHIEVEMENTS, $aData, array('uid' => $nAchievementID));
    }

    /**
     * Get achievement
     * @param $nAchievementID
     * @return mixed
     */
    public function achievementGet($nAchievementID)
    {
        return $this->db->select_row(
            self::TABLE_ACHIEVEMENTS,
            array('bounty', 'is_on'),
            array('uid' => $nAchievementID));
    }

    /**
     * Get achievements list
     * @return array
     */
    public function achievementsList()
    {
        $aData = $this->db->select('SELECT A.* FROM ' . self::TABLE_ACHIEVEMENTS . ' A ');
        $aRes = array();
        foreach ($aData as $data) {
            $aRes[$data['uid']] = $data;
        }
        return $aRes;
    }

    /**
     * Get user link
     * @param $nUserID
     * @return string|bool
     */
    public function userLink($nUserID)
    {
        $aHash = $this->db->select_row(self::TABLE_USERS_KEYS, array('hash', 'blocked'), array('user_id' => $nUserID));
        if(isset($aHash['blocked']) && $aHash['blocked']) return false;
        if(empty($aHash)) {
            $sHash = func::generator(Referalsystem::LINK_LENGTH);
            $bRes = $this->db->insert(self::TABLE_USERS_KEYS, array('user_id' => $nUserID, 'hash' => $sHash));
            return $bRes ? $sHash : $bRes;
        }
        return $aHash['hash'];
    }

    /**
     * Get links list
     * @param array $aFilter
     * @param bool $bCount
     * @param int $nLimit
     * @return array|bool
     */
    public function userLinkListing(array $aFilter, $bCount = false, $nLimit = null)
    {
        $aFilter = $this->prepareFilter($aFilter, 'UK');
        if($bCount) {
            return $this->db->one_data('SELECT COUNT(*) 
                    FROM ' . self::TABLE_USERS_KEYS . ' UK '
                . $aFilter['where']
                , $aFilter['bind']);
        }
        $aLinks = $this->db->select('SELECT 
                    UK.id as user_key_id, UK.blocked, UK.hash, UK.created,
                    U.user_id as user_id, U.name, U.surname, U.login 
                    FROM ' . self::TABLE_USERS_KEYS . ' UK 
                    INNER JOIN ' . TABLE_USERS . ' U USING(user_id)'
            . $aFilter['where']
            . (!is_null($nLimit) ?  $nLimit : '')
            , $aFilter['bind']);
        return $aLinks;
    }

    /**
     * Get user by referal link
     * @param $sHash
     * @return mixed
     */
    public function userByLink($sHash)
    {
        return $this->db->select_data(self::TABLE_USERS_KEYS, 'user_id', array('hash' => $sHash, 'blocked' => false));
    }

    /**
     * Block user referal link
     * @param $nLinkID
     * @return bool
     */
    public function blockChangeUserLink($nLinkID)
    {
        $aBlock = $this->db->select_data(self::TABLE_USERS_KEYS, 'blocked',  array('id' => $nLinkID));
        return $this->db->update(self::TABLE_USERS_KEYS, array('blocked' => !$aBlock), array('id' => $nLinkID));
    }

    /**
     * Delete user referal link
     * @param $nLinkID
     * @return bool
     */
    public function deleteUserLink($nLinkID)
    {
        return $this->db->delete(self::TABLE_USERS_KEYS, array('id' => $nLinkID));
    }

    /**
     * Delete referal user
     * @param $nUserID
     * @return bool
     */
    public function deleteUser($nUserID)
    {
        return $this->db->delete(self::TABLE_USERS_STATUSES, array('id' => $nUserID));
    }

    /**
     * Add new referal user (unregistered)
     * @param $sHash
     * @param $nNewHash
     * @return mixed
     */
    public function addFreshReferalUser($sHash, $nNewHash)
    {
        $nReferal = $this->userByLink($sHash);
        return $this->db->insert(self::TABLE_USERS_STATUSES, array('referal_id' => $nReferal, 'hash' => $nNewHash));
    }

    /**
     * Add new referal user (registered)
     * @param $sHash
     * @param $nNewUserID
     * @return mixed
     */
    public function registerFreshReferalUser($sHash, $nNewUserID)
    {
        return $this->db->update(self::TABLE_USERS_STATUSES, array('user_id' => $nNewUserID), array('hash' => $sHash));
    }

    /**
     * Save user
     * @param $nUserStatusID
     * @param $aData
     * @return bool
     */
    public function userSave($nUserStatusID, $aData)
    {
        return $this->db->update(self::TABLE_USERS_STATUSES, $aData, array('id' => $nUserStatusID));
    }

    /**
     * Get user
     * @param array $aFilter
     * @return array|bool
     */
    public function getReferalUser($aFilter)
    {
        return $this->db->select_row(self::TABLE_USERS_STATUSES, array('id', 'referal_id', 'user_id'), $aFilter);
    }

    /**
     * Get referal data
     * @param array $aFilter
     * @return array|bool
     */
    public function getReferalData($aFilter)
    {
        $aFilter = $this->prepareFilter($aFilter, 'US');
        return $this->db->one_array('
            SELECT 
                US.id as user_status_id, US.user_id, US.referal_id, US.hash, US.created,
                U.login, U.name, U.surname, U.avatar,  
                UK.hash as hash_key
            FROM ' . self::TABLE_USERS_STATUSES . ' US 
            LEFT JOIN ' . TABLE_USERS . ' U ON U.user_id = US.referal_id 
            LEFT JOIN ' . self::TABLE_USERS_KEYS . ' UK ON UK.user_id = US.referal_id '
            . $aFilter['where']
            , $aFilter['bind']);
    }

    /**
     * List referal users
     * @param array $aFilter
     * @param bool $bCount
     * @param int $nLimit
     * @return array|bool
     * @internal param int $nOffset
     */
    public function usersListing(array $aFilter, $bCount = false, $nLimit = null)
    {
        $aFilter = $this->prepareFilter($aFilter, 'US');
        if($bCount) {
            return $this->db->one_data('SELECT COUNT(*) FROM '
                . self::TABLE_USERS_STATUSES . ' US '
                . $aFilter['where']
                , $aFilter['bind']);
        }
        return $this->db->select('SELECT 
            US.id as user_status_id, US.user_id, US.referal_id, US.hash, US.created, U.login, U.name, U.surname, 
            UR.name as referal_name, UR.surname as referal_surname, UR.login as referal_login, U.avatar  
            FROM ' . self::TABLE_USERS_STATUSES . ' US 
            LEFT JOIN ' . TABLE_USERS . ' U USING(user_id) 
            LEFT JOIN ' . TABLE_USERS . ' UR ON UR.user_id = US.referal_id '
            . $aFilter['where']
            . (!is_null($nLimit) ?  $nLimit : '')
            , $aFilter['bind']);
    }

    /**
     * Add user achievement
     * @param $nUserStatusID
     * @param $nAchievementID
     * @param $nBounty
     * @return int
     */
    public function userAddAchievement($nUserStatusID, $nAchievementID, $nBounty)
    {
        return $this->db->insert(self::TABLE_USERS_ACHIEVEMENTS,
            array('user_status_id' => $nUserStatusID, 'achievement_id' => $nAchievementID, 'bounty' => $nBounty));
    }

    /**
     * Chech if this achievement already exists for user
     * @param $nUserStatusID
     * @param $nAchievementID
     * @return mixed
     */
    public function userCheckAchievement($nUserStatusID, $nAchievementID)
    {
        return $this->db->one_data(
            'SELECT EXISTS (SELECT * FROM ' . self::TABLE_USERS_ACHIEVEMENTS . ' 
            WHERE user_status_id = :user_status_id AND achievement_id = :achievement_id)',
            array('user_status_id' => $nUserStatusID, 'achievement_id' => $nAchievementID));
    }

    /**
     * Get achievements of user
     * @param $nUserStatusID
     * @return mixed
     */
    public function userAchievementsList($nUserStatusID)
    {
        return $this->db->select_rows(
            self::TABLE_USERS_ACHIEVEMENTS,
            array('achievement_id', 'bounty'),
            array('user_status_id' => $nUserStatusID)
        );
    }

    /**
     * Remove users statuses older than $nDaysNum
     * @param int $nDaysNum
     */
    public function clearOldReferals($nDaysNum)
    {
        $this->db->exec(
            'DELETE FROM ' . self::TABLE_USERS_STATUSES . ' WHERE DATEDIFF(NOW(), created) > :days AND user_id IS NULL',
            array('days' => $nDaysNum));
        $this->db->exec(
            'DELETE FROM ' . self::TABLE_USERS_HASHES . ' WHERE DATEDIFF(NOW(), created) > :days',
            array('days' => $nDaysNum));
    }

    /**
     * Bing cookie hash to registered user
     * @param $nUserID
     * @param $sHash
     * @return int
     */
    public function bindHash($nUserID, $sHash)
    {
        if ( ! empty($this->db->one_data('SELECT user_id FROM ' . self::TABLE_USERS_HASHES . ' WHERE hash = :hash ', ['hash' => $sHash]))) {
            return false;
        }
        return $this->db->insert(self::TABLE_USERS_HASHES, ['user_id' => $nUserID, 'hash' => $sHash]);
    }

    public function findHashByUser($nUserID)
    {
        return $this->db->one_data(
            'SELECT hash FROM ' . self::TABLE_USERS_HASHES . ' WHERE user_id = :id ', ['id' => $nUserID]);
    }
}