<?php

abstract class ReferalsystemBase extends Module
{
    /** @var ReferalsystemModel */
    public $model = null;
    public $securityKey = 'ac4f13625bbd46f7bd60e8cd1e2adbdd';

    const MODULE_NAME = 'referalsystem';

    const COOKIE_NAME = 'referal_hash';
    const HASH_GET_NAME = 'hash';
    const LINK_ROOT = 'ref';

    const FRONTEND_PAGE_SIZE = 15;
    const ADMIN_PAGE_SIZE = 15;

    const LINK_LENGTH = 8;

    const ACHIEVEMENT_REGISTRATION = 1;
    const ACHIEVEMENT_VERIFICATION = 2;
    const ACHIEVEMENT_BILL_PAYED = 3;
    const ACHIEVEMENT_ORDER_ADDED = 4;

    const CONFIG_PREFIX = self::MODULE_NAME . '_';
    const CONFIG_GLOBAL_IS_ON = 'isOn';
    const CONFIG_GLOBAL_USER_LIFETIME = 'userLifetime';

    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_GROUP_FROM = 'paymentsFirstFrom';
    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_GROUP_TO = 'paymentsFirstTo';
    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_PERCENT = 'paymentsFirstPersent';

    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_GROUP_FROM = 'paymentsSecondFrom';
    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_GROUP_TO = 'paymentsSecondTo';
    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_PERCENT = 'paymentsSecondPersent';

    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_GROUP_FROM = 'paymentsThirdFrom';
    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_GROUP_TO = 'paymentsThirdTo';
    const CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_PERCENT = 'paymentsThirdPersent';

    /**
     * Achievements config
     * @return array
     */
    public static function ACHIEVEMENTS()
    {
        $achievements = [
            self::ACHIEVEMENT_REGISTRATION =>
                [
                    'title' => _t('referal-system', 'Регистрация'),
                    'bounty_message' => _t('referal-system', 'Начисление средств за регистрацию реферала')],
            self::ACHIEVEMENT_BILL_PAYED =>
                [
                    'title' => _t('referal-system', 'Реферал потратил средства'),
                    'bounty_message' => _t('referal-system', 'Отчисление процента потраченной суммы рефералом'),
                    'global_configured' => true, 'multi' => true],
        ];
        if(BFF_PRODUCT == 'freelance') {
            $achievements[self::ACHIEVEMENT_VERIFICATION] =
                [
                    'title' => _t('referal-system', 'Верификация'),
                    'bounty_message' => _t('referal-system', 'Начисление средств за верификацию реферала')
                ];
            $achievements[self::ACHIEVEMENT_ORDER_ADDED] =
                [
                    'title' => _t('referal-system', 'Создание заказа'),
                    'bounty_message' => _t('referal-system', 'Начисление средств за создание заказа рефералом')
                ];
        }
        return $achievements;
    }

    /**
     * Config keys
     * @return array
     */
    public static function configKeys()
    {
        return self::configByKey('type');
    }

    /**
     * Config defaults values
     * @return array
     */
    public static function configDefaults()
    {
        return self::configByKey('default');
    }

    /**
     * Config by key
     * @return array
     */
    public static function configByKey($sKey)
    {
        $aData = self::configData();
        $keys = [];
        foreach($aData as $key => $val) {
            $keys[$key] = $val[$sKey];
        }
        return $keys;
    }

    /**
     * Config keys
     * @return array
     */
    public static function configData()
    {
        return [
            self::CONFIG_GLOBAL_IS_ON =>
                [
                    'type' => TYPE_BOOL,
                    'label' => _t('referal-system','Включена'),
                    'units' => null,
                    'default' => false
                ],
            self::CONFIG_GLOBAL_USER_LIFETIME =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Время хранения информации о потенциальном реферале'),
                    'units' => _t('referal-system','дней'),
                    'default' => 0
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_GROUP_FROM =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Количество отчислений (1 группа) от'),
                    'units' => _t('referal-system','платежей'),
                    'default' => 0
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_GROUP_TO =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Количество отчислений (1 группа) до'),
                    'units' => _t('referal-system','платежей'),
                    'default' => 10
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_PERCENT =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Процент отчислений (1 группа)'),
                    'units' => _t('referal-system','%'),
                    'default' => 10
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_GROUP_FROM =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Количество отчислений (2 группа) от'),
                    'units' => _t('referal-system','платежей'),
                    'default' => 11
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_GROUP_TO =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Количество отчислений (2 группа) до'),
                    'units' => _t('referal-system','платежей'),
                    'default' => 20
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_PERCENT =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Процент отчислений (2 группа)'),
                    'units' => _t('referal-system','%'),
                    'default' => 5
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_GROUP_FROM =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Количество отчислений (3 группа) от'),
                    'units' => _t('referal-system','платежей'),
                    'default' => 21
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_GROUP_TO =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Количество отчислений (3 группа) до'),
                    'units' => _t('referal-system','платежей'),
                    'default' => 0
                ],
            self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_PERCENT =>
                [
                    'type' => TYPE_UINT,
                    'label' => _t('referal-system','Процент отчислений (3 группа)'),
                    'units' => _t('referal-system','%'),
                    'default' => 3
                ],
        ];
    }

    /**
     * Get config
     * @return array
     */
    public static function configGet()
    {
        $aConfig = config::getWithPrefix(self::CONFIG_PREFIX);
        $aDefaults = self::configDefaults();

        foreach (self::configKeys() as $key => $val) {
            if(!isset($aConfig[$key])) {
                $aConfig[$key] = $aDefaults[$key];
            }
        }
        return $aConfig;
    }

    /**
     * get referal link for user
     * @param $nUserID
     * @return bool|string
     */
    public function getReferalLink($nUserID)
    {
        return $this->model->userLink($nUserID);
    }

    /**
     * @return Referalsystem
     */
    public static function i()
    {
        return bff::module(self::MODULE_NAME);
    }

    /**
     * @return ReferalsystemModel
     */
    public static function model()
    {
        return bff::model(self::MODULE_NAME);
    }

    /**
     * @param $key
     * @param array $opts
     * @param bool $dynamic
     * @return null|string
     */
    public static function url($key, array $opts = [], $dynamic = false)
    {
        $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            case 'list':
                return $base . '/' . self::LINK_ROOT . '/my/' . static::urlQuery($opts);
                break;
            case 'link':
                return $base . '/' . self::LINK_ROOT . '/' . static::urlQuery($opts);
                break;
        }
        return null;
    }

    /**
     * Имя пользователя
     * @param array $user @ref данные о пользователе
     * @return string
     */
    public static function userNameFormated(array &$user)
    {
        foreach (['name','surname','login','pro'] as $k) {
            if (!isset($user[$k])) $user[$k] = '';
        }
        if(!empty($user['name'])) {
            if(!empty($user['surname'])) {
                $name = $user['name'] . ' ' . mb_substr($user['surname'], 0, 1) . '.';
            } else {
                $name = $user['name'];
            }
        } else {
            if(!empty($user['surname'])) {
                $name = $user['surname'];
            } else {
                $name = $user['login'];
            }
        }

        return $name;
    }

    /**
     * Add achievement for user
     * @param $nAchievementID
     * @param null $nUserID
     * @param null $bNewUserID
     * @return bool
     */
    public static function makeAchievement($nAchievementID, $nUserID = null, $bNewUserID = null)
    {
        if(!in_array($nAchievementID, array_keys(self::ACHIEVEMENTS()))) return false;
        if(is_null($nUserID) && is_null($bNewUserID)) return false;

        $oModel = self::model();

        $aConfig = self::configGet();
        if(!$aConfig[self::CONFIG_GLOBAL_IS_ON]) return false;

        $aFilter = array();
        if(!is_null($nUserID)) {
            $aFilter['user_id'] = $nUserID;
        } else {
            $sHash = self::i()->input->cookie(self::COOKIE_NAME, TYPE_NOTAGS);
            if(empty($sHash)) {
                $sHash = $oModel->findHashByUser($bNewUserID);
            }
            $aFilter['hash'] = $sHash;
        }

        $aData = $oModel->getReferalUser($aFilter);
        if(empty($aData)) return false;

        $aAchievement = $oModel->achievementGet($nAchievementID);
        if(empty($aAchievement)) return false;
        if(empty($aAchievement['is_on'])) return false;

        $aAchievementConf = self::ACHIEVEMENTS();
        $aAchievementConf = $aAchievementConf[$nAchievementID];

        if(empty($oModel->userLink($aData['referal_id']))) return false;

        $oUserModel = Users::model();

        $aUserReferal = $oUserModel->userData($aData['referal_id'], array('balance'));

        if(empty($aUserReferal)) return false;

        if($oModel->userCheckAchievement($aData['id'], $nAchievementID)) return false;

        if(!empty($aAchievement['bounty'])) {

            $nBillID = Bills::i()->createBill_InGift(
                $aData['referal_id'],
                $aUserReferal['balance'] + $aAchievement['bounty'],
                $aAchievement['bounty'],
                $aAchievementConf['bounty_message']);

            if (empty($nBillID)) return false;

            $oUserModel->userSave($aData['referal_id'],
                array('balance' => $aUserReferal['balance'] + $aAchievement['bounty']));
        }

        $oModel->userAddAchievement($aData['id'], $nAchievementID, $aAchievement['bounty']);

        if(empty($aData['user_id']) && !is_null($bNewUserID)) {
            $oModel->userSave($aData['id'], ['user_id' => $bNewUserID]);
        }

        return true;
    }

    /**
     * Add achievement for user(achievement may be invoked many times)
     * @param int $nUserID
     * @param float $fBillAmount
     * @return bool
     */
    public static function makeAchievementBillPayed($fBillAmount, $nUserID)
    {
        $aConfig = self::configGet();
        if(!$aConfig[self::CONFIG_GLOBAL_IS_ON]) return false;
        $nAchievementID = self::ACHIEVEMENT_BILL_PAYED;
        $aFilter = ['user_id' => $nUserID];

        $oModel = self::model();

        $aData = $oModel->getReferalUser($aFilter);
        if(empty($aData)) return false;

        $aAchievement = $oModel->achievementGet($nAchievementID);
        if(empty($aAchievement)) return false;
        if(empty($aAchievement['is_on'])) return false;

        $aAchievementConf = self::ACHIEVEMENTS();
        $aAchievementConf = $aAchievementConf[$nAchievementID];

        if(empty($oModel->userLink($aData['referal_id']))) return false;

        $oUserModel = Users::model();

        $aUserReferal = $oUserModel->userData($aData['referal_id'], array('balance'));

        if(empty($aUserReferal)) return false;

        $nBillsNumber = Bills::model()->billsList(['user_id' => $nUserID, 'status' => Bills::STATUS_COMPLETED], true);
        $nBillsNumber = intval($nBillsNumber);

        $aGlobalConf = self::configGet();

        $fPercent = 0;

        if(
            $nBillsNumber >= $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_GROUP_FROM]
            && $nBillsNumber <= $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_GROUP_TO]) {
            $fPercent = $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_FIRST_PERCENT];
        } elseif(
            $nBillsNumber >= $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_GROUP_FROM]
            && $nBillsNumber <= $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_GROUP_TO]) {
            $fPercent = $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_SECOND_PERCENT];
        } elseif(
            $nBillsNumber >= $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_GROUP_FROM]
            && $nBillsNumber <= $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_GROUP_TO]) {
            $fPercent = $aGlobalConf[self::CONFIG_GLOBAL_NUMBER_OF_PAYMENTS_THIRD_PERCENT];
        }

        $fBounty = $aUserReferal['balance'] + $fBillAmount/100 * $fPercent;

        $nBillID = Bills::i()->createBill_InGift(
            $aData['referal_id'],
            $fBounty,
            $fBillAmount/100 * $fPercent,
            $aAchievementConf['bounty_message']);

        if (empty($nBillID)) return false;

        $oUserModel->userSave($aData['referal_id'],
            array('balance' => $fBounty));

        return true;
    }

    /**
     * Add new order for user-referal
     * @param $nAchievementID
     * @param $orderID
     * @param $orderData
     * @return bool
     */
    public static function makeAchievementOrder($nAchievementID, $orderID, $orderData)
    {
        if(!in_array($nAchievementID, array_keys(self::ACHIEVEMENTS()))) return false;
        if(is_null($orderID)) return false;

        $aConfig = self::configGet();
        if(!$aConfig[self::CONFIG_GLOBAL_IS_ON]) return false;

        if(empty($orderData) || !key_exists('moderated', $orderData) || !key_exists('user_id', $orderData))
            $orderData = Orders::model()->orderData($orderID);

        if(empty($orderData['moderated'])) return false;

        $oModel = self::model();
        $aReferalData = $oModel->getReferalUser(['user_id' => $orderData['user_id']]);
        if(empty($aReferalData)) return false;

        $aAchievement = $oModel->achievementGet($nAchievementID);
        if(empty($aAchievement)) return false;
        if(empty($aAchievement['is_on'])) return false;

        $aAchievementConf = self::ACHIEVEMENTS();
        $aAchievementConf = $aAchievementConf[$nAchievementID];

        if(empty($oModel->userLink($aReferalData['referal_id']))) return false;

        $oUserModel = Users::model();

        $aUserData = $oUserModel->userData($aReferalData['referal_id'], array('balance'));

        if(empty($aUserData)) return false;

        /* TODO - используется для разового начисления
        if($oModel->userCheckAchievement($aReferalData['id'], $nAchievementID)) return false;
        */

        if(!empty($aAchievement['bounty'])) {

            $nBillID = Bills::i()->createBill_InGift(
                $aReferalData['referal_id'],
                $aUserData['balance'] + $aAchievement['bounty'],
                $aAchievement['bounty'],
                $aAchievementConf['bounty_message']);

            if (empty($nBillID)) return false;

            $oUserModel->userSave($aReferalData['referal_id'],
                array('balance' => $aUserData['balance'] + $aAchievement['bounty']));
        }

        $oModel->userAddAchievement($aReferalData['id'], $nAchievementID, $aAchievement['bounty']);

        return true;
    }
}