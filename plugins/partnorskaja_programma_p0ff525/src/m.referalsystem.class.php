<?php

class M_Referalsystem
{
    /**
     * @param CMenu $menu
     * @param Security $security
     */
    static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $module = Referalsystem::MODULE_NAME;
        $menuTitle = _t('referal-system', 'Реферальная система');

        if ($security->haveAccessToModuleToMethod($module, 'config')) {
            $menu->assign(
                $menuTitle,
                _t('referal-system', 'Настройки'),
                'referalsystem',
                'config',
                true,
                1
            );
            $menu->assign(
                $menuTitle,
                _t('referal-system', 'Настройки достижений'),
                'referalsystem',
                'achievementsListing',
                true,
                2
            );
            $menu->assign(
                $menuTitle,
                _t('referal-system', 'Список пользователей'),
                'referalsystem',
                'usersListing',
                true,
                3
            );
            $menu->assign(
                $menuTitle,
                _t('referal-system', 'Список реферальных ссылок'),
                'referalsystem',
                'linksListing',
                true,
                4
            );
        }
    }
}