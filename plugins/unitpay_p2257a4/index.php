<?php

class Plugin_Unitpay_p2257a4 extends Plugin
{
    private $aSupportedCurrencies = ['EUR', 'UAH', 'BYR', 'USD', 'RUB'];

    const PS_UNITPAY = 1024; # ID
    const KEY = 'unitpay';

    const FORM_URL = 'https://unitpay.ru/pay/';

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id' => 'p2257a44b684b30ee6900ae9105cf751572a8b56',
            'plugin_title' => 'Платежная система Unitpay',
            'plugin_version' => '1.0.0',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'public_key' => array(
                'title' => 'Public API key проекта', # Ex. 144331-5dc18,
                'input' => 'password',
                'placeholder' => 'например 144331-5dc19'
            ),
            'secret_key' => array(
                'title' => 'Secret API key', # Ex. e307388a5647e0cc3a2d3711dab2fe8c
                'input' => 'password',
                'placeholder' => 'например e307388a5647e0cc3a2d3711dab2fe8n'
            ),
            'test_mode' => array(
                'type' => TYPE_BOOL,
                'title' => 'Тестовый режим',
                'input' => 'checkbox',
            ),
            'process_url' => array( #webhook
                'title' => 'Обработчик платежа',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('process', array('ps' => static::KEY)),
            ),
            'success_url' => array(
                'title' => 'Страница успешного платежа',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('success'),
            ),
            'fail_url' => array(
                'title' => 'Страница ошибки оплаты',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('fail'),
            ),
        ));
    }

    protected function start()
    {
        bff::hookAdd('bills.pay.systems.user', array($this, 'user_list'));
        bff::hookAdd('bills.pay.systems.data', array($this, 'system_list'));
        bff::hookAdd('bills.pay.form', array($this, 'form'));
        bff::hookAdd('bills.pay.process', array($this, 'process'));
    }

    /**
     * Дополняем список доступных пользователю способов оплаты
     * @param array $list список систем оплат
     * @param array $extra : 'logoUrl', 'balanceUse'
     * @return array
     */
    public function user_list($list, $extra)
    {
        $list['unitpay'] = array(
            'id' => self::PS_UNITPAY,
            'logo_desktop' => $this->url('/unitpay-logo.png'),
            'logo_phone' => $this->url('/unitpay-logo.png'),
            'way' => '',
            'title' => _t('bills', 'Unitpay'), # Название способа оплаты
            'currency_id' => Site::currencyDefault('id'), # ID валюты в системе
            'enabled' => true, # Способ доступен пользователю
            'priority' => 0, # Порядок: 0 - последняя, 1+
        );
        return $list;
    }

    /**
     * Дополняем данными о системе оплаты
     * @param array $list
     * @return array
     */
    public function system_list($list)
    {
        $list[self::PS_UNITPAY] = array(
            'id' => self::PS_UNITPAY,
            'key' => self::KEY,
            'title' => _t('bills', 'Unitpay'), # Название системы для описания счета в админ. панели
            'desc' => '',
        );
        return $list;
    }

    /**
     * Форма выставленного счета, отправляемая системе оплаты
     * @param string $form HTML форма
     * @param integer $paySystem ID системы оплаты для которой необходимо сформировать форму
     * @param array $data дополнительные данные о выставляемом счете:
     *  amount - сумма для оплаты
     *  bill_id - ID счета
     *  bill_description - описание счета
     * @return string HTML
     */
    public function form($form, $paySystem, $data)
    {
        if ($paySystem != self::PS_UNITPAY) return $form;

        $publicKey = $this->getPublicKey();
        $secretKey = $this->getSecretKey();

        if (empty($publicKey)) {
            $this->log('Public api key for Payment system Unitpay not found');
            return $form;
        }

        if (empty($secretKey)) {
            $this->log('Secret api key for Payment system Unitpay not found');
            return $form;
        }

        $aParams = [];

        if ($this->isTestMode()) {
            $aParams = ['test' => true];
        }

        $unitPay = new UnitPay($secretKey);

        $sum = round($data['amount'], 2);

        $aBillCurrencyData = Site::model()->currencyData($data['bill_data']['currency_id'], false, true);
        $currency = strtoupper($aBillCurrencyData['keyword']);
        if (!in_array($currency, $this->aSupportedCurrencies)) {
            $this->log('Bill currency(' . $currency . ')don\'t supported with payment system Unitpay');
            return $form;
        }

        $aUserData = Users::model()->userData(User::id(), ['email', 'phone_number']);
        $unitPay
            ->setBackUrl(Bills::url('my.history'))
            ->setCustomerEmail($aUserData['email']);

        if (!empty($aUserData['phone_number'])) {
            $unitPay->setCustomerPhone(str_replace('+', '', $aUserData['phone_number']));
        }

        $sRedirectUrl = $unitPay->form(
            $publicKey,
            $sum,
            $data['bill_id'],                            # account
            $data['bill_description'],                   # description
            $currency
        );

        return $this->formCreate($sRedirectUrl, $aParams);
    }

    public function formCreate($sRedirectUrl, $aParams)
    {
        $form = '<form action="' . $sRedirectUrl . '" method="POST">';
        foreach ($aParams as $key => $param) {
            $form .= '<input type="hidden" name="' . $key . '" value="' . $param . '" />';
        }
        $form .= '</form>';
        return $form;
    }

    /**
     * Обработка запроса / webhook от системы оплаты
     * Метод должен завершать поток путем вызова bff::shutdown();
     * @param string $system ключ обрабываемой системы оплаты
     */
    public function process($system)
    {
        if ($system != static::KEY) return;

        $publicKey = $this->getPublicKey();
        $secretKey = $this->getSecretKey();

        if (empty($publicKey)) {
            $this->log('Public api key for Payment system Unitpay not found');
            return;
        }

        if (empty($secretKey)) {
            $this->log('Secter api key for Payment system Unitpay not found');
            return;
        }

        $unitPay = new UnitPay($secretKey);

        try {
            # проверка корректности поступившего запроса (method, params, signature, check ip address and etc)
            $unitPay->checkHandlerRequest();

            list($method, $params) = array($_GET['method'], $_GET['params']);

            # проверка корректности поступившего запроса на соответсвие данным по счету, ex $params['account'] is bill_id
            $aBill = Bills::model()->billData($params['account'], array(
                    'user_id',
                    'psystem',
                    'status',
                    'amount',
                    'currency_id',
                )
            );
            $aBillCurrencyData = Site::model()->currencyData($aBill['currency_id'], false, true);
            $currency = strtoupper($aBillCurrencyData['keyword']);
            if (empty($aBill) ||
                $aBill['psystem'] != self::PS_UNITPAY ||
                $params['orderSum'] < $aBill['amount'] ||
                $params['orderCurrency'] != $currency
            ) {
                // logging data and throw exception
                $this->log('Bill(id:' . $params['account'] . ') validation error in payment system Unitpay!');
                throw new InvalidArgumentException('Bill validation Error!');
            }
            switch ($method) {
                # проверка возможности оказания услуги абоненту, запрос отправляется до прохождения оплаты.
                # проверка готовность системы (проверить корректность суммы платежа, существование счета в БД и т. д.)
                case 'check':
                    if ($aBill['status'] != Bills::STATUS_WAITING) {
                        throw new InvalidArgumentException('Bill status validation Error!');
                    }
                    echo $unitPay->getSuccessHandlerResponse('Check Success. Ready to pay.');
                    break;
                # уведомление об успешном списании, случай успешной проводки по счету.
                case 'pay':
                    if ($aBill['status'] != Bills::STATUS_WAITING) {
                        throw new InvalidArgumentException('Bill status validation Error!');
                    }
                    if (!$this->isTestMode() && (!isset($params['test']) || !$params['test'])) {
                        $bResult = $this->bills()->processBill($params['account'], $params['orderSum'], self::PS_UNITPAY);
                    }
                    if (!empty($bResult) && $bResult !== true) {
                        $this->log('Something wrong during saving data bill #' . $params['account'] . ': ' . $bResult);
                        throw new InvalidArgumentException('Something wrong during saving data bill #' . $params['account']);
                    }
                    echo $unitPay->getSuccessHandlerResponse('Pay Success');
                    break;
                # Oшибка платежа на любом из этапов.
                # Если ошибка вызвана пустым/ошибочным ответом сервера партнера, то запрос отправлен не будет.
                # Следует учесть, что данный статус не конечный и возможны ситуации, когда после запроса ERROR может последовать запрос PAY
                case 'error':
                    echo $unitPay->getSuccessHandlerResponse('Error logged');
                    break;
            }
            # Обработка ошибок
        } catch (Exception $e) {
            echo $unitPay->getErrorHandlerResponse($e->getMessage());
        }

        bff::shutdown();
    }

    protected function getValue($name)
    {
        $value = false;
        if (isset($_POST[$name])) $value = $_POST[$name];
        if (isset($_GET[$name])) $value = $_GET[$name];
        return $value;
    }

    protected function getPublicKey()
    {
        return $this->config('public_key');
    }

    protected function getSecretKey()
    {
        return $this->config('secret_key');
    }

    public function isTestMode()
    {
        return $this->config('test_mode');
    }
}