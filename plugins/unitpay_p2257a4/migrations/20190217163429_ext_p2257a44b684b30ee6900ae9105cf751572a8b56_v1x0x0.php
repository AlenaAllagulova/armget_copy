<?php

use bff\db\migrations\Migration as Migration;

class ExtP2257a44b684b30ee6900ae9105cf751572a8b56V1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $users = $this->table(TABLE_BILLS);
        $users->changeColumn('psystem', 'integer', ['limit' => 11])
            ->save();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {

    }
}