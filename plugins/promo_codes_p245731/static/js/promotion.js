var JPromotion = (function(){
    var o = {lang:{}};

    function onCheckPromoCode() {
        var $form = $(o.form_id);
        if ( ! Boolean($form.length)) {
            return false;
        }

        var f = app.form($form, function ($f) {
            if (! Boolean(f.fieldStr(o.input_name).length)) {
                f.fieldError(o.input_name, o.lang.input_name);
                return false;
            }
            f.ajax(bff.ajaxURL('promotions', 'on-check-promo-code'), {}, function (data, errors) {
                if (data && data.success) {
                    $('#code_msg').text(data.resp);
                    app.alert.success(o.lang.accept_promocode);
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
            return false;
        });
    }

    return {
        onCheckPromoCode : function(options) {
            o = $.extend(o, options || {});
            return onCheckPromoCode();
        }
    }
}());