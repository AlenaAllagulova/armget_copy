1. Установите и Включите плагин в [списке плагинов](index.php?s=site&ev=plugins).

{theme:file:/modules/bills/tpl/def/my.pay.php}
2. Добавьте в файл шаблона **{theme:file:path}** следующий код:
```
<?php if (bff::hooksAdded('my.pay.forms')) { bff::hook('my.pay.forms', ['data' => &$aData]); }?>
```
Рекомендуемое расположение - сразу после строки: `<?= tpl::getBreadcrumbs($breadcrumbs); ?>`
{/theme:file}


