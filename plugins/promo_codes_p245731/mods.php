<?php

return array(
    array(
        'file' => '/modules/bills/tpl/def/my.pay.php',
        'search' => '<?= tpl::getBreadcrumbs($breadcrumbs); ?>',
        'replace' => '<? if (bff::hooksAdded(\'my.pay.forms\')) { bff::hook(\'my.pay.forms\', [\'data\' => &$aData]); }?>',
        'replace-file' => false,
        'position' => 'after',
        'offset' => 1,
        'index' => 1,
    ),
);