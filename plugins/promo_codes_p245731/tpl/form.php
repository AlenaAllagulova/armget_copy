<?php
/**
 * Пополнение счета посредством промокода : форма
 * @var $this Plugin_Promo_codes_p0331b3
 * @var $data array данные шаблона may.pay.php
 */
$this->js('js/promotion.js');
?>
<div class="row">
    <div class="col-md-8">
        <form action="" class="form" id="j-u-input-promocode-form">
            <div class="form-group j-required">
                <h6 class="mrgt30" id="code_msg"><?= _t('', 'Использовать промокод для пополнения счета') ?></h6>
                <input type="text" class="form-control" name="promocode"
                       id="j-u-input-promocode"
                       placeholder="<?= _t('', 'Введите промокод') ?>"
                       maxlength="100"/>
                <input type="text" name="user_id" value="<?= User::id(); ?>" hidden>
            </div>
            <input type="submit" class="btn btn-primary j-submit" value="<?= _t('', 'Отправить') ?>">
        </form>
    </div>
</div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        JPromotion.onCheckPromoCode(<?= func::php2js([
                'lang' => [
                    'input_name' => _t('', 'Поле <b>"Введите промокод"</b> обязательно к заполнению'),
                    'accept_promocode' => _t('', 'Промокод успешно применен'),
                ],
                'form_id' => '#j-u-input-promocode-form',
                'input_name' => 'promocode',
            ])
            ?>);
    });
    <? js::stop(); ?>
</script>
