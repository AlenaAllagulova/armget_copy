<?php

class Plugin_Promo_codes_p245731 extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id'   => 'p2457315ca45fc2414ed78fd284e121b24f69ea5',
            'plugin_title'   => 'Промокоды',
            'plugin_version' => '1.0.0',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            //
        ));
    }

    protected function start()
    {
        bff::i()->moduleRegister('promotions', $this->path('src'));

        // хук расширяющий варианты пополнения счета пользлвателем,  в данном случае использование промокодов
        bff::hookAdd('my.pay.forms', function($aParams){
            $this->form($aParams);
        });

    }

    public function form($context)
    {
        echo $this->viewPHP($context, 'tpl/form');
    }
}