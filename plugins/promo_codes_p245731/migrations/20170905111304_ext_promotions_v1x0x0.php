<?php

use bff\db\migrations\Migration as Migration;
use Phinx\Db\Adapter\MysqlAdapter;


class ExtPromotionsV1x0x0 extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function migrate()
    {
        $this->table( 'promotions_promotions_data',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['promotion_id']]
        )
            ->addColumn('promotion_id', 'integer', [
                'signed' => false,
                'identity' => true,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'limit' => 255,
                'null' => false,
                'default' => '',
            ])
            ->addColumn('prefix', 'string', [
                'limit' => 255,
                'null' => false,
                'default' => '',
            ])
            # номинал промокода в акции
            ->addColumn('value', 'integer', [
                'signed' => false,
                'null' => false,
            ])
            # количество кодов
            ->addColumn('quantity', 'integer', [
                'signed' => false,
                'null' => false,
            ])
            # статус
            ->addColumn('enabled', 'integer', ['signed' => false, 'default' => false, 'limit' => 3])
            # дата проведения
            ->addColumn('start_time', 'datetime', [
                'null' => false,
                'default' => 'CURRENT_TIMESTAMP'
            ])
            ->addColumn('stop_time', 'datetime', [
                'null' => true,
            ])
            # статус безсрочно
            ->addColumn('unlimited', 'integer', ['signed' => false, 'default' => false, 'limit' => 3])
            ->addColumn('created', 'datetime', [
                'null' => false,
                'default' => 'CURRENT_TIMESTAMP'
            ])
            ->addColumn('modified', 'datetime', [
                'null' => false,
                'default' => 'CURRENT_TIMESTAMP'
            ])
            ->create();


        $this->table('promotions_promotion_code',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['code_id']]
        )
            ->addColumn('code_id', 'integer', [
                'identity' => true,
                'null' => false,
            ])
            ->addColumn('promotion_data_id', 'integer', [
                'signed' => false,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'limit' => 19,
                'null' => false,
            ])
            # время активации
            ->addColumn('activation_time', 'datetime', [
                'default' => null,
                'null' => true,
            ])
            # Id пользователя
            ->addColumn('user_id', 'integer', [
                'signed' => false,
                'null' => true,
            ])
            ->addIndex(['code'],
                ['unique' => true]
            )
            ->addColumn('activated', 'integer', ['signed' => false, 'default' => false, 'limit' => 3])
            ->addForeignKey(
                ['promotion_data_id'],
                'promotions_promotions_data',
                'promotion_id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->addForeignKey(
                ['user_id'],
                TABLE_USERS,
                'user_id',
                ['delete' => 'RESTRICT', 'update' => 'CASCADE'])
            ->create();
    }

    public function rollback()
    {
        $this->dropIfExists('promotions_promotion_code');
        $this->dropIfExists('promotions_promotions_data');
    }
}
