<?php

class Promotions extends PromotionsBase
{
    # константы для генератора кода
    const CHARS_FOR_CODE = '12345ABCDEFGHIJKLMNOPQRSTUVWXYZ6789';
    const PARTS_OF_CODE = 4;
    const LENGTH_OF_PART = 4;
    const NUMBER_OF_PAGES = 10;

    public function promotions_listing()
    {
        if (!$this->haveAccessTo('promotions'))
            return $this->showAccessDenied();

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = [];
            switch ($sAct) {
                case 'add':
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validatePromotionData(0, $bSubmit);

                    if ($bSubmit) {
                        if ($this->errors->no()) {
                            $aData['prefix'] = self::generatePartString();
                            $aData['promotion_id'] = $this->model->promotionSave(0, $aData);
                            $this->codeSave($aData['quantity'], $aData['promotion_id']);
                        }
                        break;
                    }

                    $aData['promotion_id'] = 0;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.promotions.form');
                    break;
                case 'edit':
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nPromotionID = $this->input->postget('promotion_id', TYPE_UINT);
                    if (!$nPromotionID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        $aData = $this->validatePromotionData($nPromotionID, $bSubmit);
                        if ($this->errors->no()) {
                            $this->model->promotionSave($nPromotionID, $aData);
                            $this->codeSave($aData['quantity'], $nPromotionID);
                        }
                        $aData['promotion_id'] = $nPromotionID;
                    } else {
                        # получить данные промо
                        $aData = $this->model->promotionData($nPromotionID, true);
                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }

                        /**
                         * исправить ошибку с удалением
                         * исправить ошибку с сохранением
                         * сделать оповещение о сохранении
                         * Таб прикрутить к листингу
                         * сделать выгрузку в файл
                         */
                    }
                    # Получение информации про активированные коды
                    $aData['activated_codes'] = $this->model->getCountActivatedCodes($aData['promotion_id']);


                    $aResponse['form'] = $this->viewPHP($aData, 'admin.promotions.form');
                    break;
                case 'toggle':
                    $nPromotionID = $this->input->postget('promotion_id', TYPE_UINT);
                    if (!$nPromotionID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->promotionToggle($nPromotionID, $sToggleType);

                    break;
                case 'delete':
                    $nPromotionID = $this->input->postget('promotion_id', TYPE_UINT);
                    if (!$nPromotionID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->promotionData($nPromotionID, true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->promotionDelete($nPromotionID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    } else {

                    }
                    break;
                default:
                    $aResponse = false;
            }

            # ajax
            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = [];
        $this->input->postgetm([
            'page' => TYPE_UINT,
            'tab' => TYPE_INT,
            'title' => TYPE_NOTAGS,
        ], $f);
        # формируем фильтр списка акций
        $sql = [];
        $sqlOrder = '';
        $mPerpage = self::NUMBER_OF_PAGES;
        $aData['pgn'] = '';
        # Табы
        switch ($f['tab']) {
            case 0: # Активные по времени
                $sql[':stop_time_or_enabled'] = [
                    '((stop_time > :current_time OR unlimited = :unlimited) AND enabled = :enabled)',
                    ':current_time' => Date(DateTime::ATOM),
                    ':unlimited' => 1,
                    ':enabled' => 1
                ];
                break;
                break;
            case 1: # Неактивные по времени или отключенные
                $sql[':stop_time_or_enabled'] = [
                    '(stop_time < :current_time  OR enabled = :enabled)',
                    ':current_time' => Date(DateTime::ATOM),
                    ':enabled' => 0
                ];
                break;
            case 2: # Все
                break;
        }
        # Поисковая форма по "Название или ID"
        if (!empty($f['title'])) {
            $sql[':name'] = ['(promotion_id = ' . intval($f['title']) . ' OR name LIKE :name)', ':name' => '%' . $f['title'] . '%'];
        }
        if ($mPerpage !== false) {
            $nCount = $this->model->promotionsListing($sql, true);
            $sqlLimit = '';
            if ($nCount > 0) {
                $oPgn = new Pagination($nCount, $mPerpage, $this->adminLink('promotions_listing' . '&page=' . Pagination::PAGE_ID . '&tab=' . $f['tab']));
                $sqlLimit = $oPgn->getLimitOffset();
                $aData['pgn'] = $oPgn->view();
            }


            $aData['list'] = $this->model->promotionsListing($sql, false, $sqlLimit, $sqlOrder);
        } else {
            $aData['list'] = $this->model->promotionsListing($sql, false, '', $sqlOrder);
        }
        # Получение информации про активированные коды
        foreach ($aData['list'] as $k => $v) {
            $aData['list'][$k]['activated_codes'] = $this->model->getCountActivatedCodes($v['promotion_id']);
        }
        # Часть кода которая приходит по ajax и возращает кусок
        $aData['list'] = $this->viewPHP($aData, 'admin.promotions.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm([
                'list' => $aData['list'],
                'pgn' => $aData['pgn'],
            ]);
        }

        $aData['f'] = $f;
        $aData['promotion_id'] = $this->input->get('promotion_id', TYPE_UINT);
        $aData['act'] = $sAct;

        #Обновление счетчика кодов промоакции (на случай удалений)
        $this->model->updateQuantity($aData['promotion_id']);

        tpl::includeJS(['datepicker'], true);
        return $this->viewPHP($aData, 'admin.promotions.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nPromotionID ID акции или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validatePromotionData($nPromotionID, $bSubmit)
    {
        $aData = [];
        $this->input->postm([
            'name'       => TYPE_STR, # Название акции
            'quantity'   => TYPE_INT, # количество кодов
            'created'    => TYPE_STR, # Дата создания
            'value'      => TYPE_PRICE, # Номинал
            'start_time' => TYPE_DATE, # Дата начала акции
            'stop_time'  => TYPE_DATE, # Дата окончания акции
            'unlimited'  => TYPE_BOOL, # Безсрочно
            'enabled'    => TYPE_BOOL, # Включен
        ], $aData);

        if ($bSubmit) {
            //или безлимит или даты
            if ($aData['unlimited']) {
                $aData['stop_time'] = null;
            } elseif ($aData['start_time'] >= $aData['stop_time']) {
                $this->errors->set(_t('', 'Проверьте даты проведения акции'));
            }
        }
        return $aData;
    }


    /**
     * Генерируем часть промокода  XXXX (длиной LENGTH_OF_PART)
     * @return string
     */
    public static function generatePartString()
    {
        $characterString = '';
        for ($i = 0; $i < self::LENGTH_OF_PART; ++$i) {
            $random = str_shuffle(self::CHARS_FOR_CODE);
            $characterString .= $random[0];
        }

        return $characterString;
    }


    /**
     * Генерируем промокод  XXXX-XXXX-XXXX-XXXX
     * @param $sPrefix префикс акции
     * @return string промокод
     */
    public static function codeGenerate($sPrefix)
    {
        $aCharacterString[0] = $sPrefix;
        for ($i = 1; $i < self::PARTS_OF_CODE; ++$i) {
            $aCharacterString[$i] = self::generatePartString();
        }
        $sPromoCode = implode('-', $aCharacterString);

        return $sPromoCode;
    }


    /**
     * Сохранение в базу кодов при редактировании акции
     * При увеличении количества промокодов
     * @param $nQuantity
     * @param $nPromotionID
     * @return bool|int
     */
    public function codeSave($nQuantity, $nPromotionID)
    {
        $aFilter[':promotion_data_id'] = [
            " promotion_data_id = :promotion_data_id ",
            ':promotion_data_id' => $nPromotionID
        ];

        $sPrefix = $this->model->getPromotionPrefix($nPromotionID);
        $nQuantityOld = $this->model->codesListing($aFilter, true);

        if ($nQuantityOld == $nQuantity) {

            return true;

        } elseif ($nQuantityOld < $nQuantity) {

            $nCount = $nQuantity - $nQuantityOld;
            for ($i = 1; $i <= $nCount; $i++) {
                $aData[] = [
                    'promotion_data_id' => $nPromotionID,
                    'code' => self::codeGenerate($sPrefix),
                ];
            }
            return $this->model->codeSave($aData);

        } elseif ($nQuantityOld > $nQuantity) {

            $nCount = $nQuantityOld - $nQuantity;
            return $this->model->manyCodeDelete($nCount, $nPromotionID);
        }
    }


    /**
     * Листинг кодов
     * @return string
     */
    public function code_listing()
    {
        if (!$this->haveAccessTo('codes'))
            return $this->showAccessDenied();

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = [];
            switch ($sAct) {
                case 'delete': {
                    $nCodeID = $this->input->postget('code_id',
                        TYPE_UINT);
                    if (!$nCodeID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->getCodeData(['code_id' => $nCodeID], true);

                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->codeDelete($nCodeID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    } else {
                    }
                }
                    break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX())
                $this->ajaxResponseForm($aResponse);
        }

        $f = [];
        $this->input->postgetm([
            'page' => TYPE_UINT,
            'title' => TYPE_NOTAGS,
            'promotion_id' => TYPE_UINT,
        ], $f);

        # формируем фильтр списка
        $sql[':promotion_id'] = ['promotion_data_id = :promotion_id ', ':promotion_id' => $f['promotion_id']];
        $sqlOrder = '';
        $mPerpage = 15;
        $aData['pgn'] = '';

        if (!empty($f['title'])) {
            $sql[':code'] = ['code_id = ' . intval($f['title']) . ' OR code LIKE :code',
                ':code' => '%' . $f['title'] . '%'];
        }

        if ($mPerpage !== false) {

            $nCount = $this->model->codesListing($sql, true);
            $oPgn = new Pagination($nCount, $mPerpage, $this->adminLink('code_listing' . '&promotion_id=' . $f['promotion_id'] . '&page=' . Pagination::PAGE_ID));
            $sqlLimit = $oPgn->getLimitOffset();
            $aData['pgn'] = $oPgn->view();

            $aData['list'] = $this->model->codesListing($sql, false, $sqlLimit, $sqlOrder);
        } else {
            $aData['list'] = $this->model->codesListing($sql, false, '', $sqlOrder);
        }

        $aData['list'] = $this->viewPHP($aData, 'admin.codes.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list' => $aData['list'],
                'pgn' => $aData['pgn'],
            ));
        }

        $aData['f'] = $f;
        $aData['code_id'] = $this->input->get('code_id', TYPE_UINT);
        $aData['act'] = $sAct;
        $aData['promotion_id'] = $f['promotion_id'];

        return $this->viewPHP($aData, 'admin.codes.listing');
    }


    /**
     * Экспорт кодов
     */
    public function export()
    {
        $sContent = '';
        $this->input->postgetm([
            'promotion_id' => TYPE_UINT,
        ], $f);

        $sql[':promotion_id'] = ['promotion_data_id = :promotion_id ', ':promotion_id' => $f['promotion_id']];

        $aData = $this->model->getCodesForExport($sql);

        $filename = 'export_promocodes';

        header('Content-Disposition: attachment; filename=' . $filename . '.txt');
        header("Content-Type: text/plain");

        foreach ($aData as $item){
            $sContent .= $item['code'] . "\n";
        }

        echo $sContent;
        exit;
    }
}
