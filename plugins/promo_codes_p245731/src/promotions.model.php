<?php
define('ACTIVATED', 1); # статус кода - активирован
define('TABLE_PROMOTIONS_DATA', 'promotions_promotions_data');
define('TABLE_PROMOTION_CODE', 'promotions_promotion_code');

class PromotionsModel extends Model
{
    /** @var PromotionsBase */
    protected $controller;


    public function init()
    {
        parent::init();
    }


    /**
     * Сохранение акции
     * @param integer $nPromotionID ID акции
     * @param array $aData данные акции
     * @return boolean|integer
     */
    public function promotionSave($nPromotionID, array $aData)
    {
        if (empty($aData)) return false;

        if ($nPromotionID > 0) {
            $aData['modified'] = $this->db->now();

            $res = $this->db->update(TABLE_PROMOTIONS_DATA, $aData, ['promotion_id' => $nPromotionID]);

            return !empty($res);
        } else {
            $aData['created'] = $this->db->now();
            $aData['modified'] = $this->db->now();

            $nPromotionID = $this->db->insert(TABLE_PROMOTIONS_DATA, $aData);

            return $nPromotionID;
        }
    }


    /**
     * Удаление акции
     * @param integer $nPromotionID ID акции
     * @return boolean
     */
    public function promotionDelete($nPromotionID)
    {
        if (empty($nPromotionID)) return false;
        $res = $this->db->delete(TABLE_PROMOTIONS_DATA,['promotion_id' => $nPromotionID]);
        if (!empty($res)) {
            return true;
        }
        return false;
    }


    /**
     * Получение данных акции
     * @param integer $nPromotionID ID акции
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function promotionData($nPromotionID, $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array(
                'SELECT *
                 FROM ' . TABLE_PROMOTIONS_DATA . '
                 WHERE promotion_id = :promotion_id',
                [':promotion_id' => $nPromotionID]
            );

        } else {
            return false;
        }

        return $aData;
    }

    /**
     * Список акций (admin)
     * @param array $aFilter фильтр списка акций
     * @param bool $bCount только подсчет кол-ва акций
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function promotionsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter = $this->prepareFilter($aFilter, '');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(promotion_id) FROM ' . TABLE_PROMOTIONS_DATA . ' ' . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('
               SELECT promotion_id, `value`, quantity, `name`, created, start_time, stop_time, enabled
               FROM ' . TABLE_PROMOTIONS_DATA . ' 
               ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']);
    }


    /**
     * Переключатели акции
     * @param integer $nPromotionID ID акции
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function promotionToggle($nPromotionID, $sField)
    {
        if ($sField == 'enabled') {
            return $this->toggleInt(TABLE_PROMOTIONS_DATA, $nPromotionID, $sField, 'promotion_id');
        }
    }


    /**
     * Получить префик акции (admin). используется для формирования дополнительных кодов
     * @param $nPromotionID
     * @return mixed
     */
    public function getPromotionPrefix($nPromotionID)
    {
        $aFilter[':promotion_id'] = [
            " promotion_id = :promotion_id ",
            ':promotion_id' => $nPromotionID
        ];
        $aFilter = $this->prepareFilter($aFilter);

        return $this->db->one_data('SELECT prefix FROM '.TABLE_PROMOTIONS_DATA.' '.$aFilter['where'], $aFilter['bind']);

    }


    /**
     * Вносим данные при активации кода
     * @param $nCodeID
     * @param $nUserID
     * @return bool
     */
    public function registrationCode($nCodeID, $nUserID)
    {
        $aData['user_id'] = $nUserID;
        $aData['activation_time'] = $this->db->now();
        $aData['activated'] = ACTIVATED;

        $res = $this->db->update(TABLE_PROMOTION_CODE, $aData, ['code_id' => $nCodeID]);
        if (!empty($res)) {
            return true;
        }
        return false;
    }


    /**
     * Список кодов (admin)
     * @param array $aFilter фильтр списка
     * @param bool $bCount только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function codesListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter = $this->prepareFilter($aFilter, '');
        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(code_id) FROM '.TABLE_PROMOTION_CODE .' ' .$aFilter['where'], $aFilter['bind']);
        }
        return $this->db->select('SELECT * 
            FROM '.TABLE_PROMOTION_CODE.' PC LEFT JOIN '.TABLE_USERS.'  U ON PC.user_id = U.user_id 
            '.$aFilter['where'] . ( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '') . $sqlLimit,
            $aFilter['bind']
        );
    }


    /**
     * Получение данных промокода
     * @param $aFilter
     * @return mixed
     */
    public function getCodeData($aFilter)
    {
        $aFilter = $this->prepareFilter($aFilter, 'PC');

        $aData = $this->db->one_array('SELECT * 
            FROM ' . TABLE_PROMOTION_CODE . ' PC LEFT JOIN ' . TABLE_PROMOTIONS_DATA . '  PD ON PC.promotion_data_id = PD.promotion_id ' . $aFilter['where'],
            $aFilter['bind']
        );

        return $aData;
    }


    /**
     * Сохранение кодов в базу
     * @param $aData
     * @return bool
     */
    public function codeSave($aData)
    {
        return $this->db->multiInsert(TABLE_PROMOTION_CODE, $aData);
    }


    /**
     * Удаление кода по id
     * @param integer $nCodeID ID
     * @return boolean
     */
    public function codeDelete($nCodeID)
    {
        if (empty($nCodeID)) return false;

        $res = $this->db->exec('DELETE FROM ' . TABLE_PROMOTION_CODE . ' WHERE code_id = :code_id ', ['code_id'=>$nCodeID]);
        if (empty($res)) {
            return false;
        }
        return true;
    }


    /**
     * Автотическое удаление при изменении количества кодов в настройках акции
     * @param $nCount
     * @param $nPromotionID
     * @return bool
     */
    public function manyCodeDelete($nCount, $nPromotionID)
    {
        # выбираем прокоды доступные для автоматического удаления (неактивированные)
        $data = $this->db->select_rows_column(
            TABLE_PROMOTION_CODE,
            'code_id',
            ['promotion_data_id' => $nPromotionID, 'activated' => false]
        );

        # выбираем доступные для удаления id промокодов
        $data = array_slice($data, 0, $nCount);

        $nCountID = count($data);

        if ($nCountID > 1) {
            $sCodesID = implode(',', $data);
        } elseif ($nCountID == 1) {
            $sCodesID = $data[0];
        } elseif ($nCountID < $nCount) {
            $this->errors->set(_t('promotion', 'Недостаточное количество неактивированных кодов для автоматического удаления'));
            return false;
        }
        $res = $this->db->exec('DELETE FROM ' . TABLE_PROMOTION_CODE . ' WHERE code_id IN (' . $sCodesID . ')');

        if (!empty($res)) {
            return true;
        }
    }


    /**
     * Обновление данных общего количества кодов в акции
     * @param $nPromotionID
     * @return bool
     */
    public function updateQuantity($nPromotionID)
    {
        $nSumCodes = $this->db->one_data('SELECT COUNT(code_id) FROM '
            . TABLE_PROMOTION_CODE
            . ' WHERE promotion_data_id = :promotion_id', ['promotion_id' => $nPromotionID]  );

        $res = $this->db->exec('UPDATE ' . TABLE_PROMOTIONS_DATA .
            ' SET ' . 'quantity = ' . $nSumCodes .
            ' WHERE promotion_id =' . $nPromotionID
        );

        if (!empty($res)) {
            return true;
        }
        return false;
    }

    /**
     * Количество активированных кодов
     * @param $nPromotionID
     * @return mixed
     */
    public function getCountActivatedCodes($nPromotionID)
    {
        return $this->db->one_data('SELECT COUNT(code_id) FROM '. TABLE_PROMOTION_CODE .
            ' WHERE promotion_data_id = :promotion_id AND activated = '. ACTIVATED, ['promotion_id' => $nPromotionID]
        );
    }


    /**
     *
     * @param array $aFilter
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function getCodesForExport(array $aFilter, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter = $this->prepareFilter($aFilter, '');
        return $this->db->select('SELECT code 
            FROM ' . TABLE_PROMOTION_CODE . '  
            ' . $aFilter['where'] . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . $sqlLimit,
            $aFilter['bind']
        );
    }

}