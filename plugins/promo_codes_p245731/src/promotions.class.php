<?php
class Promotions extends PromotionsBase
{
    /**
     * Обработка формы промокодов по адресу user/bill/?pay=1
     */
    public function ajax()
    {
        $aResponse = [];
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'on-check-promo-code':
                $aParams = [
                    'user_id'   => TYPE_INT,
                    'promocode' => TYPE_STR,
                ];

                $this->input->postgetm($aParams, $aData);

                # Извлекаем информацию о промокоде
                $aFilter[':code'] = [
                    " code = :code ",
                    ':code' => $aData['promocode']
                ];
                $aCode = $this->model->getCodeData($aFilter);
                $bIsPromoEnabled = $this->model->promotionData($aCode['promotion_data_id'], true);

                if( ! $aCode){  # Нет кода в базе
                    $this->errors->set(_t('','Ошибка. Введен неверный промокод'));
                    break;
                } elseif ( ! empty($aCode['activation_time']) ){ # Код активирован
                    $this->errors->set( _t('','Ошибка. Промокод уже был зарегистрирован ранее'));
                    break;
                } elseif (date("Y-m-d H:i:s") > $aCode['stop_time'] && !$aCode['unlimited']){  # Проверка периода проведения акции
                    $this->errors->set( _t('','Ошибка. Истек срок действия промокода'));
                    break;
                } elseif (empty($bIsPromoEnabled['enabled'])) { # акция отключена
                    $this->errors->set( _t('','Ошибка. Данная акция не активна'));
                } else {
                    $this->createBillСode($aData['user_id'], $aCode);
                    $this->model->registrationCode($aCode['code_id'], $aData['user_id']);

                    $aResponse['resp'] = _t('', 'Промокод [promocode] успешно применен', ['promocode' => $aCode['code']]);
                    break;
                }
            default: {
                $this->errors->impossible();
            }
        }
        $this->ajaxResponseForm($aResponse);
    }


    /**
     * Проводка оплаты по базе
     * @param $nUserID
     * @param array $aCode
     * @return int|string
     */
    public function createBillСode($nUserID, array $aCode)
    {
        # Получаем баланс пользователя
        $oUserModel = Users::model();
        $aUserBalance = $oUserModel->userData($nUserID, ['balance']);

        $res = $this->bills()->updateUserBalance($nUserID, $aCode['value'], true);
        if (empty($res)) {
            return $this->showImpossible();
        }

        # создаем завершенный счет пополнения - промокод#$newBalance = $aUserBalance['balance'] + $aCode['value'];
        $sDescription = _t('', 'Пополнение счета через промокод [promocode]', ['promocode' => $aCode['code']]);
        $newBalance = $aUserBalance['balance'] + $aCode['value'];
        $nBillID = $this->bills()->createBill_InGift($nUserID, $newBalance, $aCode['value'], $sDescription);

        return $nBillID;
    }

}