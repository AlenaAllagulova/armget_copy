<?php
/**
 * @var $this Promotions
 */
?>

<?= tplAdmin::blockStart(_t('', 'Список кодов'), false,
    array('id' => 'PromotionCodesFormBlock', 'style' => 'display:none;'));
?>

<div id="PromotionCodesFormContainer"></div>

<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart('', true,
    ['id' => 'PromotionCodesListBlock', 'class' => (!empty($act) ?
        'hidden' : '')], [], []
);
?>

<?
$aTabs = [
    'promo' => _t('', _t('', 'Настройки акции')),
    'codes' => _t('', _t('', 'Промокоды')),
];
?>
<div class="tabsBar" id="PromotionsPromotionsFormTabs">
    <span class="tab">
        <a href="<?= $this->adminLink('promotions_listing', 'promotions&act=edit&promotion_id=' . $promotion_id) ?>"><?= $aTabs['promo']; ?></a>
    </span>
    <span class="tab tab-active">
        <a href="#"><?= $aTabs['codes']; ?></a>
    </span>
    <div class="clear"></div>
</div>
<div class="actionBar">
    <form method="get"
          action="<?= $this->adminLink(null)?>"
          id="PromotionCodesListFilters"
          onsubmit="return false;"
          class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>"/>
        <input type="hidden" name="ev" value="<?= bff::$event ?>"/>
        <input type="hidden" name="page" value="<?= $f['page'] ?>"/>
        <input type="hidden" name="promotion_id" value="<?= ($promotion_id)?$promotion_id:'' ?>"/>

        <div class="left">
            <div class="left" style="margin-left:8px;">
                <input style="width:175px;" type="text"
                       maxlength="150"
                       name="title"
                       placeholder="<?= _t('', 'Найти промокод'); ?>"
                       value="<?= HTML::escape($f['title']) ?>"/>
                <input type="button"
                       class="btn btn-small button cancel"
                       style="margin-left: 8px;"
                       onclick="jPromotionCodesList.submit(false);"
                       value="<?= _t('', 'найти'); ?>"/>
            </div>
            <div class="left" style="margin-left:8px;">
                <a class="ajax cancel" onclick="jPromotionCodesList.submit(true); return false;">
                    <?= _t('', 'сбросить'); ?>
                </a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="right">
            <div id="PromotionCodesProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="PromotionCodesListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">N</th>
        <th class="left"><?= _t('', 'Промокод'); ?></th>
        <th width="135"><?= _t('', 'Данные активации'); ?></th>
    </tr>
    </thead>
    <tbody id="PromotionCodesList">
    <?= $list ?>
    </tbody>
</table>
<div id="PromotionCodesListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">
    </div>
</div>

<script type="text/javascript">
    var jPromotionCodesList =
        (function () {
            var $progress, $block, $list, $listTable, $listPgn, filters,
                processing = false;
            var ajaxUrl = '<?= $this->adminLink(bff::$event . '&act=') ?>';

            $(function () {
                $progress = $('#PromotionCodesProgress');
                $block = $('#PromotionCodesListBlock');
                $list = $block.find('#PromotionCodesList');
                $listTable = $block.find('#PromotionCodesListTable');
                $listPgn = $block.find('#PromotionCodesListPgn');
                filters = $block.find('#PromotionCodesListFilters').get(0);
                $list.delegate('a.code-edit', 'click', function () {
                    var id = intval($(this).data('id'));
                    if (id > 0) jPromotionCodesFormManager.action('edit', id);
                    return false;
                });

                $list.delegate('a.code-del', 'click', function () {
                    var id = intval($(this).data('id'));
                    if (id > 0) del(id, this);
                    return false;
                });
            });

            function isProcessing() {
                return processing;
            }

            function del(id, link) {
                bff.ajaxDelete('<?=_t('','Удалить?');?>', id, ajaxUrl + 'delete&code_id=' + id, link,
                    {
                        progress: $progress, repaint: false, onComplete: function () {
                        bff.success('<?=_t('','Запись успешно удалена');?>');
                    }
                    });
                return false;
            }

            function updateList(updateUrl) {
                if (isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function (data) {
                    if (data) {
                        $list.html(data.list);
                        $listPgn.html(data.pgn);
                        if (updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title,
                                $(filters).attr('action') + '?' + f + '&promotion_id=<?=$promotion_id?>');
                        }
                    }
                }, function (p) {
                    $progress.toggle();
                    processing = p;
                    $list.toggleClass('disabled');
                });
            }

            function setPage(id) {
                filters.page.value = intval(id);
            }

            return {
                submit: function (resetForm) {
                    if (isProcessing()) return false;
                    setPage(1);
                    if (resetForm) {
                        filters['title'].value = '';
                    }
                    updateList();
                },
                page: function (id) {
                    if (isProcessing()) return false;
                    setPage(id);
                    updateList();
                },
                refresh: function (resetPage, updateUrl) {
                    if (resetPage) setPage(0);
                    updateList(updateUrl);
                },
                toggle: function (show) {
                    if (show === true) {
                        $block.show();
                        if (bff.h) window.history.pushState({}, document.title,
                            $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                }
            };
        }());
</script>