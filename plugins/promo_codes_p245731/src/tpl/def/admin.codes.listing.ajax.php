<?php
/**
 * @var $this Promotion
 */
$i = 0;
foreach ($list as $k=>&$v):
    $i++;
    $id = $v['code_id']; ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $i ?></td>
        <td class="left"><?= $v['code'] ?></td>
        <td class="left"><?= $v['activation_time'] ?></td>
        <td class="left"><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);"><?= $v['email'] ?></a></td>
        <td>
            <a class="but del code-del" title="<?=_t('','Удалить');?>" href="#" data-id="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="3">
    <?= _t('','ничего не найдено');?>
        </td>
    </tr>
<? endif;