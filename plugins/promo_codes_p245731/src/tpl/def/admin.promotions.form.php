<?php
/**
 * @var $this Promotions
 */
$aData = HTML::escape($aData, 'html', ['name','created','start_time','stop_time']);
$edit = ! empty($promotion_id);

$aTabs = [
    'promo' => _t('', _t('','Настройки акции')),
    'codes' => _t('', _t('','Промокоды')),
];

?>
<form name="PromotionsPromotionsForm" id="PromotionsPromotionsForm" action="<?= $this->adminLink(null) ?>"
      method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>"/>
    <input type="hidden" name="save" value="1"/>
    <input type="hidden" name="promotion_id" value="<?= $promotion_id ?>"/>
    <input type="hidden" name="created" id="promotion-created" value="<?= $created ?>"/>

    <div class="tabsBar" id="PromotionsPromotionsFormTabs">
        <span class="tab tab-active">
            <a href="#"><?= $aTabs['promo']; ?></a>
        </span>
        <span class="tab">
            <a href="<?= $this->adminLink('code_listing', 'promotions&promotion_id=' . $promotion_id) ?>">
                <?= $aTabs['codes']; ?>
            </a>
        </span>
        <div class="clear"></div>
    </div>

    <div class="j-tab j-tab-promo">
        <table class="admtbl tbledit">
            <tr class="required">
                <td class="row1" width="100"><span class="field-title"><?= _t('', 'Название акции'); ?>
                        <span class="required-mark">*</span>:</span>
                </td>
                <td class="row2">
                    <input class="stretch" type="text" required id="promotion-name" name="name" value="<?= $name ?>"/>
                </td>
            </tr>
            <tr class="required">
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Количество промокодов'); ?>:</span>
                </td>
                <td class="row2">
                    <input class="short" type="number" step="1" min="0" max="100" id="promotion-quantity" name="quantity"
                           value="<?= $quantity ?>" placeholder="<?= _t('', 'Укажите количество кодов');  ?>"/>
                    <? if(!empty($activated_codes)):?>
                        <span style="font-weight:bold; text-decoration:none;">
                            <?= _t('', 'Активировано: [activated_codes]', ['activated_codes'=> $activated_codes ]);?>
                        </span>
                    <?endif;?>
                    <span class="desc">
                        <?= _t('', '(При изменении количество промокодов будет изменяться автоматически)'); ?>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Сумма'); ?>:</span>
                </td>
                <td class="row2">
                    <input class="short" type="text" id="promotion-name" name="value" value="<?= $value ?>"/>
                    <span class="desc"><?=Site::currencyDefault()?></span>
                </td>
            </tr>
            <tr>
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Дата начала акции'); ?>
                        <span class="required-mark">*</span>:
                    </span>
                </td>
                <td class="row2">
                    <input class="short" type="text" id="promotion-start_time" name="start_time"
                           value="<?= (!empty($start_time) && $start_time != '0000-00-00' ? date('d-m-Y', strtotime($start_time)) : '') ?>"/>
                </td>
            </tr>
            <tr>
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Дата окончания акции'); ?>:</span>
                </td>
                <td class="row2">
                    <input class="short" type="text" id="promotion-stop_time" name="stop_time"
                           <?=(!empty($unlimited))?'disabled':'';?>
                           value="<?= (! empty($unlimited) || (!$edit)? '' : date('d-m-Y', strtotime($stop_time))) ?>"/>

                    <label class="checkbox">
                        <span class="desc"><?= _t('', 'Бессрочно'); ?></span>
                        <input type="checkbox"
                               id="promotion-unlimited"
                               name="unlimited"<? if ($unlimited): ?>
                               checked="checked"<? endif; ?> />
                    </label>
                </td>
            </tr>
            <tr>
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Включено:'); ?></span>
                </td>
                <td class="row2">
                    <label class="checkbox">
                        <input type="checkbox" id="promotion-enabled" name="enabled"<? if(empty ($promotion_id) or $enabled): ?> checked="checked"<? endif; ?> />
                    </label>
                </td>
            </tr>
            <? if($edit):?>
                <tr>
                    <td class="row1">
                        <span class="field-title"><?= _t('', 'Статус:'); ?></span>
                    </td>
                    <td class="row2">
                        <? if($enabled != 0 && (date("Y-m-d H:i:s") <= $stop_time) || $enabled != 0 && $stop_time == 0): ?>
                            <span style="color:#52af0e; font-weight:bold; text-decoration:none;">
                                <?= _t('', 'Активна'); ?>
                            </span>
                        <? else: ?>
                            <span style="color:red; font-weight:bold; text-decoration:none;">
                                <?= _t('', 'Не активна');?>
                            </span> <?= _t('', '(Акция отключена или дата окончания уже прошла)'); ?>
                        <? endif;?>
                    </td>
                </tr>
            <? endif; ?>
            <? if($edit):?>
                <tr>
                    <td class="row1">
                        <span class="field-title"><?= _t('', 'Экспорт кодов:'); ?></span>
                    </td>
                    <td class="row2">
                        <a href="<?= $this->adminLink('export', 'promotions&promotion_id=' . $promotion_id) ?>">
                            <?=_t('','Загрузить файл')?>
                        </a>
                    </td>
                </tr>
            <? endif; ?>
            <tr class="footer">
                <td colspan="2">
                    <input type="submit"
                           class="btn btn-success button submit"
                           value="<?= _t('', 'Сохранить'); ?>"
                           onclick="jPromotionsPromotionsForm.save(false);"/>
                    <? if ($edit): ?>
                        <input type="button"
                               class="btn btn-success button submit"
                               value="<?= _t('', 'Сохранить и вернуться'); ?>"
                               onclick="jPromotionsPromotionsForm.save(true);" />
                    <? endif; ?>
                    <? if ($edit): ?>
                        <input type="button" onclick="jPromotionsPromotionsForm.del(); return false;"
                                             class="btn btn-danger button delete"
                                             value="<?= _t('', 'Удалить'); ?>" />
                    <? endif; ?>
                    <input type="button"
                           class="btn button cancel"
                           value="<?= _t('', 'Отмена'); ?>"
                           onclick="jPromotionsPromotionsFormManager.action('cancel');"/>
                </td>
            </tr>
        </table>

    </div>
</form>

<script type="text/javascript">
    var jPromotionsPromotionsForm =
        (function(){
            var $progress, $form, formChk, id = parseInt(<?= $promotion_id ?>), f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#PromotionsPromotionsFormProgress');
                $form = $('#PromotionsPromotionsForm');
                bff.datepicker('#promotion-start_time', {dateFormat:'dd-mm-yy'});
                bff.datepicker('#promotion-stop_time', {dateFormat:'dd-mm-yy'});

                f = $form.get(0);
                // табы
                $form.find('#PromotionsPromotionsFormTabs .j-tab-toggler').on('click', function (e) {
                    nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-' + key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });

                var stopTime = document.getElementById("promotion-stop_time");
                $form.find('#promotion-unlimited').on('click', function (e) {
                    if (document.getElementById("promotion-unlimited").checked) {
                        stopTime.disabled = true;
                        stopTime.value = '';
                    }
                    else {
                        stopTime.disabled = false;
                    }
                });
            });
            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&promotion_id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('<?=_t('','Запись успешно удалена');?>');
                                jPromotionsPromotionsFormManager.action('cancel');
                                jPromotionsPromotionsList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    if( ! formChk.check(true) ) return;
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('<?=_t('','Данные успешно сохранены')?>');
                            if(returnToList || ! id) {
                                jPromotionsPromotionsFormManager.action('cancel');
                                jPromotionsPromotionsList.refresh( ! id);
                            }
                        }
                    }, $progress);
                },
                onShow: function ()
                {
                    formChk = new bff.formChecker($form);
                },
            };
        }());

    </script>