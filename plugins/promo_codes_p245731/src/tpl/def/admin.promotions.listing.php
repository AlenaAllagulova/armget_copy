<?php
/**
 * @var $this Promotions
 */

?>
<?= tplAdmin::blockStart(_t('','Добавление'), false, ['id' => 'PromotionsPromotionsFormBlock', 'style' => 'display:none;']); ?>
<div id="PromotionsPromotionsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart('', true, ['id' => 'PromotionsPromotionsListBlock', 'class' => (!empty($act) ? 'hidden' : '')],
    ['title' => '+ ' . _t('','добавить'), 'class' => 'ajax', 'onclick' => 'return jPromotionsPromotionsFormManager.action(\'add\',0);'],
    []
); ?>
<?
$aTabs = [
    0 => ['t' => _t('','Активные акции')],
    1 => ['t' => _t('','Неактивные акции')],
    2 => ['t' => _t('','Все')],
];
?>

<div class="tabsBar" id="PromotionsPromotionsListTabs">
    <? foreach ($aTabs as $k => $v) : ?>
        <span class="tab <? if ($f['tab'] == $k): ?>tab-active<? endif; ?>">
            <a href="#" onclick="return jPromotionsPromotionsList.onTab(<?= $k ?>,this);"><?= $v['t'] ?></a>
        </span>
    <? endforeach; ?>
</div>

<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="PromotionsPromotionsListFilters"
          onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>"/>
        <input type="hidden" name="ev" value="<?= bff::$event ?>"/>
        <input type="hidden" name="page" value="<?= $f['page'] ?>"/>
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>"/>

        <div class="left">
            <div class="left" style="margin: 8px 0 0 8px;">
                <input style="width:284px;" type="text" maxlength="150" name="title" placeholder="<?=_t('','Название или ID');?>"
                       value="<?= HTML::escape($f['title']) ?>"/>
                <input type="button" class="btn btn-small button cancel" style="margin-left: 8px;"
                       onclick="jPromotionsPromotionsList.submit(false);" value="<?=_t('','найти');?>"/>
            </div>
            <div class="left" style="margin: 8px 0 0 8px;">
                <a class="ajax cancel" onclick="jPromotionsPromotionsList.submit(true); return false;"><?=_t('','сбросить');?></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="right">
            <div id="PromotionsPromotionsProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="PromotionsPromotionsListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">ID</th>
        <th class="left"><?=_t('','Название акции');?></th>
        <th><?=_t('','Количество кодов / активировано');?></th>
        <th><?=_t('','Сумма');?></th>
        <th><?=_t('','Сроки проведения акции');?></th>
        <th width="100"><?=_t('','Дата создания');?></th>
        <th width="135"><?=_t('','Действие');?></th>
    </tr>
    </thead>
    <tbody id="PromotionsPromotionsList">
    <?= $list ?>
    </tbody>
</table>
<div id="PromotionsPromotionsListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    var jPromotionsPromotionsFormManager = (function () {
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function () {
            $formContainer = $('#PromotionsPromotionsFormContainer');
            $progress = $('#PromotionsPromotionsProgress');
            $block = $('#PromotionsPromotionsFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if( ! empty($act)):?>action('<?= $act ?>',<?= $promotion_id ?>);<? endif; ?>
        });

        function onFormToggle(visible) {
            if (visible) {
                jPromotionsPromotionsList.toggle(false);
                if (jPromotionsPromotionsForm) jPromotionsPromotionsForm.onShow();
            } else {
                jPromotionsPromotionsList.toggle(true);
            }
        }

        function initForm(type, id, params) {
            if (process) return;
            bff.ajax(ajaxUrl, params, function (data) {
                if (data && (data.success || intval(params.save) === 1)) {
                    $blockCaption.html(type == 'add' ? '<?=_t('','Добавление')?>' : '<?=_t('','Редактирование')?>');
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo($blockCaption, {duration: 500, offset: -300});
                    onFormToggle(true);
                    if (bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act=' + type + '&promotion_id=' + id);
                    }
                } else {
                    jPromotionsPromotionsList.toggle(true);
                }
            }, function (p) {
                process = p;
                $progress.toggle();
            });
        }

        function action(type, id, params) {
            params = $.extend(params || {}, {act: type});
            switch (type) {
                case 'add': {
                    if (id > 0) return action('edit', id, params);
                    if ($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                }
                    break;
                case 'cancel': {
                    $block.hide();
                    onFormToggle(false);
                }
                    break;
                case 'edit': {
                    if (!(id || 0)) return action('add', 0, params);
                    params.promotion_id = id;
                    initForm(type, id, params);
                }
                    break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jPromotionsPromotionsList =
        (function () {
            var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, tab = <?= $f['tab'] ?>;
            var ajaxUrl = '<?= $this->adminLink(bff::$event . '&act='); ?>';

            $(function () {
                $progress = $('#PromotionsPromotionsProgress');
                $block = $('#PromotionsPromotionsListBlock');
                $list = $block.find('#PromotionsPromotionsList');
                $listTable = $block.find('#PromotionsPromotionsListTable');
                $listPgn = $block.find('#PromotionsPromotionsListPgn');
                filters = $block.find('#PromotionsPromotionsListFilters').get(0);

                $list.delegate('a.promotion-edit', 'click', function () {
                    var id = intval($(this).data('id'));
                    if (id > 0) jPromotionsPromotionsFormManager.action('edit', id);
                    return false;
                });

                $list.delegate('a.promotion-toggle', 'click', function () {
                    var id = intval($(this).data('id'));
                    var type = $(this).data('type');
                    if (id > 0) {
                        var params = {progress: $progress, link: this};
                        bff.ajaxToggle(id, ajaxUrl + 'toggle&type=' + type + '&promotion_id=' + id, params);
                    }
                    return false;
                });

                $list.delegate('a.promotion-del', 'click', function () {
                    var id = intval($(this).data('id'));
                    if (id > 0) del(id, this);
                    return false;
                });

                $(window).bind('popstate', function (e) {
                    if ('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec(loc.search.toString());
                    if (actForm != null) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jPromotionsPromotionsFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jPromotionsPromotionsFormManager.action('cancel');
                        updateList(false);
                    }
                });

            });

            function isProcessing() {
                return processing;
            }

            function del(id, link) {
                bff.ajaxDelete('<?=_t('','Удалить?');?>', id, ajaxUrl + 'delete&promotion_id=' + id, link, {
                    progress: $progress,
                    repaint: false
                });
                return false;
            }

            function updateList(updateUrl) {
                if (isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function (data) {
                    if (data) {
                        $list.html(data.list);
                        $listPgn.html(data.pgn);
                        if (updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                    }
                }, function (p) {
                    $progress.toggle();
                    processing = p;
                    $list.toggleClass('disabled');
                });
            }

            function setPage(id) {
                filters.page.value = intval(id);
            }

            return {
                submit: function (resetForm) {
                    if (isProcessing()) return false;
                    setPage(1);
                    if (resetForm) {
                        filters['title'].value = '';
                        //
                    }
                    updateList();
                },
                page: function (id) {
                    if (isProcessing()) return false;
                    setPage(id);
                    updateList();
                },
                onTab: function (tabNew, link) {
                    if (isProcessing() || tabNew == tab) return false;
                    setPage(1);
                    tab = filters.tab.value = tabNew;
                    updateList();
                    $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
                    return false;
                },
                refresh: function (resetPage, updateUrl) {
                    if (resetPage) setPage(0);
                    updateList(updateUrl);
                },
                toggle: function (show) {
                    if (show === true) {
                        $block.show();
                        if (bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                }
            };
        }());
</script>