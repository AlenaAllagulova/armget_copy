<?php
/**
 * @var $this Promotions
 */
foreach ($list as $k=>&$v):
    $promotion_id = $v['promotion_id']; ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $promotion_id ?></td>
        <td class="left"><?= $v['name'] ?></td>
        <td><?= $v['quantity'] ?> | <?= $v['activated_codes'] ?></td>
        <td><?= $v['value'] ?></td>
        <td><?= tpl::date_format2($v['start_time'], true, false) ?> - <br>
            <?=($v['stop_time']) ? tpl::date_format2($v['stop_time'], true, false): _t('','(без срока окончания)') ?>
        </td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a class="but <?= ($v['enabled']?'un':'') ?>block promotion-toggle" title="<?=_t('','Включен');?>" href="#" data-type="enabled" data-id="<?= $promotion_id ?>"></a>
            <a class="but edit promotion-edit" title="<?=_t('','Редактировать');?>" href="#" data-id="<?=$promotion_id ?>"></a>
            <a class="but del promotion-del" title="<?=_t('','Удалить');?>" href="#" data-id="<?=$promotion_id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="7">
    <?=_t('','ничего не найдено');?>
        </td>
    </tr>
<? endif;