<?php

class Module_ extends \bff\base\Module
{
    /**
     * Отображаем краткую страницу с текстом (frontend)
     * @param string $sTitle заголовок страницы
     * @param string $sContent контент страницы
     * @return string HTML
     */
    public function showShortPage($sTitle, $sContent)
    {
        View::setLayout('short');
        $aData = array(
            'title'   => $sTitle,
            'content' => $sContent,
        );

        return View::template('short.page', $aData);
    }

    /**
     * Отображаем уведомление "Успешно..." (frontend)
     * @param string $sTitle заголовок сообщения
     * @param string $sMessage текст сообщения
     * @return string HTML
     */
    public function showSuccess($sTitle = '', $sMessage = '')
    {
        $aData = array(
            'message' => $sMessage,
        );

        return $this->showShortPage($sTitle, View::template('message.success', $aData));
    }

    /**
     * Отображаем уведомление об "Ошибке..." (frontend)
     * @param string $sTitle заголовок сообщения
     * @param string|integer $mMessage текст сообщения или ID сообщения (константа Errors)
     * @param bool $bAuth требуется авторизация
     * @return string HTML
     */
    public function showForbidden($sTitle = '', $mMessage = '', $bAuth = false)
    {
        if (empty($sTitle)) {
            $sTitle = _t('', 'Доступ запрещен');
        }
        $aData = array(
            'message' => (is_integer($mMessage) ? $this->errors->getSystemMessage($mMessage) : $mMessage),
            'auth'    => $bAuth,
        );

        return $this->showShortPage($sTitle, View::template('message.forbidden', $aData));
    }

    public function showForbiddenGuests()
    {
        return $this->showForbidden('', _t('', 'Только для зарегистрированных пользователей'), true);
    }
}