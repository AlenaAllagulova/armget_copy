<?php

class Security_ extends \bff\base\Security
{
    public function init()
    {
        # Путь к админ.панели, формат: "/dir/dir/", "/dir/", "/" (слэш в начале и конце пути обязателен)
        $this->sessionCookie['admin-path'] = '/admin/';
        # Домен админ.панели, формат: "admin.example.com" или false (SITEHOST)
        $this->sessionCookie['admin-domain'] = false;

        parent::init();
    }

    public function getAdminPath()
    {
        return rtrim($this->sessionCookie['admin-path'], '/');
    }

    public function getUserPasswordMD5($sPassword, $sSalt = '')
    {
        if (empty($sSalt)) {
            $sSalt = $this->getUserInfo('password_salt');
        }

        return md5('&^%$^&*(hVAb][CKj9vyeyhtJR' . $sSalt . '[t5pGET37mXm6DFdc]' . $sSalt . 'L2W2U3' . md5($sPassword) . 'E5H75522rXxx2SNs6C&^%$&');
    }

    public function getRememberMePasswordMD5($sPassword)
    {
        return (md5(':324%^@x9228(y;44^&(478__?:)*' . md5($sPassword) . 'mmmm;[[;!918_%*S__#d$%**c'));
    }

    public function getRememberMeIPAddressMD5($sExtra = '')
    {
        return md5('(*&^$34354' . Request::remoteAddress() . '##(_asd*' . $sExtra . '^&$_986)$');
    }

    public function getType()
    {
        return $this->getUserInfo('type');
    }

    public function getPro()
    {
        return $this->getUserInfo('pro');
    }
}