<?php
/*
 * Класс генерации PDF файлов
 *
 * Предварительно в директорию app/external необходимо установить один из компонентов:
 * - spipu/html2pdf
 * - mpdf/mpdf
 *
 * Пример:
 * cd example.com/
 * composer require spipu/html2pdf
 *
 * или
 *
 * cd example.com/
 * composer require mpdf/mpdf
 *
 * тестировалось с версиями
    "require": {
        "spipu/html2pdf": "^5.0",
        "mpdf/mpdf": "^6.1"
    }
 */

class PdfGenerator
{
    /** @var \Errors object */
    public $errors = null;

    public function __construct()
    {
        define("_MPDF_TEMP_PATH", PATH_BASE.'files'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR);
        define("_MPDF_TTFONTDATAPATH", PATH_BASE.'files'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR);

        $this->errors = bff::DI('errors');
    }

    /**
     * Определение класса для генерации PDF файлов
     * @return string имя класса
     */
    public function generator()
    {
        foreach(array('mPDF', 'Html2Pdf' => '\Spipu\Html2Pdf\Html2Pdf') as $k => $v){
            if (is_integer($k)) {
                if (class_exists($v)) {
                    return $v;
                }
            } else {
                if (class_exists($v)) {
                    return $k;
                }
            }
        }
        return '';
    }

    /**
     * Генерация PDF файла на основании HTML данных
     * @param string $content HTML данные
     * @param array $params параметры array(
                        'filename'    => Путь и имя файла или только имя файла
                        'dest'        => false - вывод на stdout или 'F' - запись в файл 'filename' - полный путь к файлу
                        'orientation' => Опиентация бумаги 'P' или 'L'
                        'format'      => Формат бумаги 'A4'
                        'error_prefix' => пефикс для ошибки, если надо
                    );
     * @param bool $silence тихий режим (false - выполнять errors->set или true - только запись ошибки в лог)
     */
    public function output($content, $params = array(), $silence = true)
    {
        $generator = $this->generator();
        if(empty($generator)){
            bff::log('PDF generate class not found');
            return;
        }
        $params += array(
            'filename'    => 'example.pdf',
            'orientation' => 'P',
            'format'      => 'A4',
            'dest'        => false,
        );
        $method = 'output_'.$generator;
        if (method_exists($this, $method)) {
            $this->$method($content, $params, $silence);
        }else{
            bff::log('PDF generate method not found');
        }
    }

    /**
     * Генерация pdf файла с помощью mPDF
     * @param string $content HTML  данные
     * @param array $params параметры
     * @param bool $silence тихий режим (false - выполнять errors->set или true - только запись ошибки в лог)
     */
    protected function output_mPDF($content, $params = array(), $silence = true)
    {
        // https://github.com/mpdf/mpdf
        try {
            //Кодировка | Формат | Размер шрифта | Шрифт
            //Отступы: слева | справа | сверху | снизу | шапка | подвал
            $mpdf = new mPDF('utf-8', $params['format'].($params['orientation'] == 'L' ? '-L' : ''), '10', 'freesans', 10, 10, 10, 10, 0, 0);
            $mpdf->WriteHTML($content);
            $mpdf->Output($params['filename'], $params['dest'] ? $params['dest'] : 'I');
        } catch (Exception $e) {
            bff::log($e->getMessage());
            if ( ! $silence) {
                $this->errors->set( (isset($params['error_prefix']) ? $params['error_prefix'] : '').$e->getMessage());
            }
        }
    }

    /**
     * Генерация pdf файла с помощью Html2Pdf
     * @param string $content HTML  данные
     * @param array $params параметры
     * @param bool $silence тихий режим (false - выполнять errors->set или true - только запись ошибки в лог)
     */
    protected function output_Html2Pdf($content, $params = array(), $silence = true)
    {
        // http://html2pdf.fr/en/example
        // https://github.com/spipu/html2pdf
        $content = '<page backtop="10mm" backbottom="10mm" backleft="5mm" backright="5mm" style="font-size: 10pt;"> '.$content.'</page>';
        try {
            $html2pdf = new Spipu\Html2Pdf\Html2Pdf($params['orientation'], $params['format'], 'en');
            //$html2pdf->setDefaultFont('dejavusans');
            //$html2pdf->setDefaultFont('dejavusansmono');
            //$html2pdf->setDefaultFont('dejavuserif');
            //$html2pdf->setDefaultFont('dejavuserifcondensed');
            //$html2pdf->setDefaultFont('freemono');
            $html2pdf->setDefaultFont('freesans');
            $html2pdf->writeHTML($content);
            $html2pdf->Output($params['filename'], $params['dest'] ? $params['dest'] : 'I');
        } catch (Exception $e) {
            bff::log($e->getMessage());
            if ( ! $silence) {
                $this->errors->set( (isset($params['error_prefix']) ? $params['error_prefix'] : '').$e->getMessage());
            }
        }
    }
}