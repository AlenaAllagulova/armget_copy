<?php

/**
 * Класс работы с данными авторизованного пользователя
 * @abstract
 */
abstract class User_ extends \bff\base\User
{
    /**
     * Получаем тип текущего авторизованного пользователя
     * @param integer|boolean $typeID ID проверяемого типа
     * @return integer|boolean
     */
    public static function type($typeID = false)
    {
        if ($typeID) {
            return (bff::security()->getType() == $typeID);
        }

        return bff::security()->getType();
    }

    /**
     * Проверим является ли исполнителем текущий авторизованный пользователь
     * @return bool
     */
    public static function isWorker()
    {
        return static::type(Users::TYPE_WORKER);
    }

    /**
     * Проверим является ли клиентом текущий авторизованный пользователь
     * @return bool
     */
    public static function isClient()
    {
        return static::type(Users::TYPE_CLIENT);
    }

    /**
     * Проверим имеет ли текущий авторизованный пользователь статус "PRO"
     * @return bool
     */
    public static function isPro()
    {
        return bff::security()->getPro();
    }

    /**
     * Когда завершается срок действия статуса "PRO"
     * @return bool
     */
    public static function proExpire()
    {
        $expire = bff::security()->getUserInfo('pro_expire');
        if ($expire != '0000-00-00 00:00:00') {
            $expire = strtotime($expire);
            if ($expire > time()) {
                return date('d.m.Y', $expire);
            }
        }
        return '';
    }

    /**
     * Является ли текущий авторизованный пользователь "доверенным"
     * @param string $module название модуля
     * @return bool
     */
    public static function isTrusted($module = '')
    {
        $trusted = bff::security()->getUserInfo('trusted');
        if (!$trusted) {
            if (static::isPro() && config::sysAdmin('users.trusted.pro', false, TYPE_BOOL)) {
                $trusted = true;
            }
        }
        return $trusted;
    }

    /**
     * Получаем роль текущего авторизованного пользователя
     * @param integer|boolean $roleID ID проверяемой роли
     * @return integer|boolean
     */
    public static function role($roleID = false)
    {
        if ($roleID) {
            return (bff::security()->getUserInfo('role_id') == $roleID);
        }

        return (int)bff::security()->getUserInfo('role_id');
    }

    /**
     * Является ли текущий авторизованный пользователь "Компанией"
     * @return bool
     */
    public static function isCompany()
    {
        if (Users::rolesEnabled()) {
            return static::role(Users::ROLE_COMPANY);
        } else{
            return false;
        }
    }

}