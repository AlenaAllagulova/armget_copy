<?php

class Theme_Custom extends Theme
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'theme_title'   => 'Доработки (пример)',
            'theme_version' => '1.0.0',
        ));
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
        \bff::hookAdd('css.extra', function() {
            tpl::includeCSS('custom');
        });
    }
}