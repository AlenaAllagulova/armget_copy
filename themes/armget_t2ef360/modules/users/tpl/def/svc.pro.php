<?php
tpl::includeJS('svc', false, 3);
$bPro = $user['pro'];
$cur = Site::currencyDefault();
?>
    <div class="container">
        <section class="l-mainContent">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                    <h1 class="small"><?= $svc['title_view'][LNG] ?></h1>

                        <? if($bPro): ?><?= _t('svc', 'Ваш аккаунт действителен до [date]', array('date' => tpl::date_format_pub($user['pro_expire'], 'd.m.Y'))); ?><? endif; ?>

                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <h6 class="text-center"><?= $bPro ? _t('svc', 'Продлить аккаунт') : _t('svc', 'Купить аккаунт') ?></h6>
                                <form class="form" method="post" id="j-svc-pro">
                                    <div class="se-radio-form">
                                        <? foreach($svc['mass'] as $k => $v): ?>
                                        <label class="se-radio-item">
                                            <input type="radio" name="m" value="<?= $k ?>" data-sum="<?= $v ?>">
                                            <span class="se-radio-price">= <?= $v ?> <?= $cur ?></span>
                                            <?= number_format(round($v / $k, 2), 2)?> <?= $cur ?> x <?= tpl::declension($k, _t('svc', 'месяц;месяца;месяцев')) ?>
                                        </label>
                                        <? endforeach; ?>
                                    </div>
                                    <div class="c-formSubmit text-center">
                                        <button class="btn btn-primary c-formSuccess j-submit"><?= _t('svc', 'Оплатить'); ?></button>
                                        <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="hidden-xs">
                            <h6><?= _t('svc', 'Подробное описание'); ?></h6>

                            <div class="table se-table">
                              <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th><?= _t('svc', 'Возможности'); ?></th>
                                    <th class="se-table-pro-column text-center"><?= _t('svc', 'Аккаунт'); ?> <span class="arm-pro"><img src="<?= bff::url('/img/icon/pro.svg')?>" alt=""></span></th>
                                    <th class="text-center"><?= _t('svc', 'Базовый аккаунт'); ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td><?= _t('svc', 'Количество предложений на заказы <small>(в день)</small>'); ?></td>
                                    <td class="se-table-pro-column text-center"><?= Orders::offersLimit(true) > 999 ? _t('svc', 'Неограничено') : Orders::offersLimit(true) ?></td>
                                    <td class="text-center"><?= Orders::offersLimit() > 999 ? _t('svc', 'Неограничено') : Orders::offersLimit() ?></td>
                                  </tr>
                                  <tr>
                                    <td><?= _t('svc', 'Возможность отвечать на заказы с пометкой "Только для [pro]"', array('pro'=>'<span class="arm-pro"><img src="'.bff::url('/img/icon/pro.svg').'" alt=""></span>')); ?></td>
                                    <td class="se-table-pro-column text-center"><i class="fa fa-check"></i></td>
                                    <td class="text-center">-</td>
                                  </tr>
                                  <tr>
                                    <td><?= _t('svc', 'Размещение в каталоге выше остальных'); ?></td>
                                    <td class="se-table-pro-column text-center"><i class="fa fa-check"></i></td>
                                    <td class="text-center">-</td>
                                  </tr>
                                  <tr>
                                    <td><?= _t('svc', 'Множитель рейтинга'); ?></td>
                                    <td class="se-table-pro-column text-center">x <?= Users::ratingMultiplier() ?></td>
                                    <td class="text-center">-</td>
                                  </tr>
                                  <? $nSpecLimit = Users::specializationsLimit(); if($nSpecLimit): ?>
                                  <tr>
                                    <td><?= _t('svc', 'Количество специализаций, по которым размещаетесь в каталоге'); ?></td>
                                    <td class="se-table-pro-column text-center"><?= Users::specializationsLimit(true) ?></td>
                                    <td class="text-center"><?= $nSpecLimit ?></td>
                                  </tr>
                                  <? endif;
                                    if(Portfolio::previewOnlyPro()): ?>
                                  <tr>
                                    <td><?= _t('svc', 'Превью работ в портфолио'); ?></td>
                                    <td class="se-table-pro-column text-center"><i class="fa fa-check"></i></td>
                                    <td class="text-center">-</td>
                                  </tr>
                                  <? endif; ?>
                                </tbody>
                              </table>
                            </div>
                        </div>

                        <div class="visible-xs">
                            <h6><?= _t('svc', 'Преимущества аккаунта Pro'); ?></h6>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-check text-primary"></i> <?= Orders::offersLimit(true) > 999 ? _t('svc', 'Неограниченное количество предложений на заказы (в день)') : _t('svc', '[N_decl] на заказы (в день)', array('N_decl' => tpl::declension(Orders::offersLimit(true), _t('svc', 'предложение;предложения;предложений')))) ?></li>
                                <li><i class="fa fa-check text-primary"></i> <?= _t('svc', 'Возможность отвечать на заказы с пометкой "Только для [pro]"', array('pro'=>'<span class="arm-pro"><img src="'.bff::url('/img/icon/pro.svg').'" alt=""></span>')); ?></li>
                                <li><i class="fa fa-check text-primary"></i> <?= _t('svc', 'Размещение в каталоге выше остальных'); ?></li>
                                <li><i class="fa fa-check text-primary"></i> <?= _t('svc', 'Множитель рейтинга'); ?> <span class="text-primary">x <?= Users::ratingMultiplier() ?></span></li>
                                <? if($nSpecLimit): ?>
                                <li><i class="fa fa-check text-primary"></i> <?= _t('svc', '[n_decl], по которым размещаетесь в каталоге', array('n_decl' => tpl::declension(Users::specializationsLimit(true), _t('svc', 'специализация;специализации;специализаций')))); ?></li>
                                <? endif;
                                if(Portfolio::previewOnlyPro()): ?>
                                <li><i class="fa fa-check text-primary"></i> <?= _t('svc', 'Превью работ в портфолио'); ?></li>
                                <? endif; ?>
                            </ul>
                        </div>

                </div>
            </div>
        </section>
    </div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jSvcPro.init(<?= func::php2js(array(
            'lang' => array(
                'select_month' => _t('svc', 'Выберите период'),
                'svc' => $bPro ? _t('svc', 'Продлить') : _t('svc', 'Купить'),
                'buy' => _t('svc', 'Пополнить'),
                'success' => _t('svc', 'Услуга успешно активирована'),
            ),
            'balance' => $user['balance'],
        )) ?>);
    });
    <? js::stop(); ?>
</script>