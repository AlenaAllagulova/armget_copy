<?php
if( ! empty($list)):
    $aStatus = Users::aWorkerStatus();
    $tagsLimit = Users::searchTagsLimit();
    $lng_spec = _t('users', 'Специализация:');
    ?>
    <div class="list-users mrgt20">
    <? foreach($list as $k => $v): ?>
        <div class="list-users__box">
            <div class="list-users__header">
                <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="list-users__avatar">
                    <?= tpl::userAvatar($v) ?>
                </a>
                <div class="">
                    <div class="">
                            <span class="list-users__name">
                                <?= tpl::userLink($v, 'no-login'); ?>
                            </span>
                        <? if($v['status'] != Users::WORKER_STATUS_NONE): ?>
                            <span class="label <?= $aStatus[ $v['status'] ]['c'] ?>">
                                    <?= $aStatus[ $v['status'] ]['t'] ?>
                                </span>
                        <? endif; ?>
                    </div>
                    <div class="">
                            <span class="hidden-sm hidden-xs">
                                <?= $lng_spec; ?>
                            </span>
                        <a href="<?= Users::url('search-spec', $v) ?>">
                            <?= $v['main_spec_title'] ?>
                        </a>
                    </div>
                    <? if( ! empty($v['city_data']['title'])): ?>
                        <div>
                            <i class="fa fa-map-marker"></i> <?= $v['city_data']['title'] ?><?= ! empty($v['country_data']['title']) ? ', '.$v['country_data']['title'] : '' ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>

            <div class="">
                <? if( ! empty($v['aTags'])): $n = 0; foreach($v['aTags'] as $vv): ?>
                    <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                    <a href="<?= Users::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
                <? endforeach; endif; ?>
            </div>

            <ul class="l-item-features">
                <li>
                    <a href="#" class="ajax-link  j-map-marker" data-n="<?= $k ?>" data-id="<?= $v['user_id'] ?>"><i class="fa fa-map-marker"></i> <span><?= _t('users', 'показать на карте'); ?></span></a>
                </li>
                <? if( ! empty($v['main_price'])): ?>
                    <li>
                        <i class="fa fa-clock-o"></i>
                        <?= ! empty($v['main_spec_price_title']) ? $v['main_spec_price_title'] : _t('users', 'Цена'); ?><?=
                        ! empty($v['main_price_rate_text'][LNG]) ? ' '.$v['main_price_rate_text'][LNG] : '' ?>:
                        <?= $v['main_price'] ?>
                        <?= Site::currencyData($v['main_price_curr'], 'title_short'); ?>
                    </li>
                <? endif; ?>
                <li>
                    <i class="fa fa-bar-chart-o" aria-hidden="true"></i>
                    <strong><?= Users::rating($v['rating'], $v['pro']) ?></strong>
                </li>
                <li class="comments-link comments-link_user">
                    <i class="fa fa-comments mrgr5" aria-hidden="true"></i>
                    <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                </li>
            </ul>
        </div>
    <? endforeach; ?>
    </div>
<? else: ?>
    <div class="alert alert-info mrgt20"><?= _t('users', 'Исполнители не найдены'); ?></div>
<? endif;
