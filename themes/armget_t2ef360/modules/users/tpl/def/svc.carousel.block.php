<?php
tpl::includeCSS(array('owl.carousel'), true);
tpl::includeJS(array('owl.carousel.min'), false);
$ordersPlus = Site::indexView(Site::INDEX_VIEW_ORDERS_PLUS);
?>
<? if( ! empty($list)): ?>
    <div class="l-carousel-box">

        <? if($ordersPlus): ?>
        <div class="l-carousel-container in-carousel">
            <div class="container">
        <? else: ?>
        <div class="container">
            <div class="l-carousel-container">
        <? endif; ?>
                <div id="j-carousel" class="owl-carousel l-carousel l-freelancers-carousel">
                    <? foreach($list as $v): ?>
                    <div>
                        <div class="media l-freelancers-carousel-item">
                            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="pull-left">
                                <?= tpl::userAvatar($v) ?>
                            </a>
                            <div class="media-body">
                                <? if($v['title']): ?>
                                    <a class="f-freelancer-name" href="<?= Users::url('profile', array('login' => $v['login'])) ?>"><?= $v['title'] ?></a>
                                <? else: ?>
                                    <?= tpl::userLink($v, 'no-login', '', array('class'=>'f-freelancer-name')) ?>
                                <? endif; ?>
                                <div class="form-group">
                                    <?= nl2br($v['message']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
                <? if(User::id() && (Users::useClient() && User::isWorker() || ! Users::useClient())): ?>
                <a href="<?= Svc::url('view', array('keyword' => $svc['keyword'])) ?>" class="l-carousel-how_link"><small><?= _t('svc', 'Как сюда попасть?'); ?></small></a>
                <? endif; ?>
            </div>
        </div>
    </div>
<script type="text/javascript">
    <? js::start() ?>
    $(function(){
        var $carousel = $('#j-carousel');
        if($carousel.length){
            $carousel.owlCarousel({
                items : 5, //10 items above 1000px browser width
                itemsDesktop : [1000,5], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,3], // betweem 900px and 601px
                itemsTablet: [600,1], //2 items between 600 and 0
                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                navigation : true,
                navigationText : ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                pagination : true,
                autoPlay : false
            });
        }

    });
    <? js::stop() ?>
</script>
<? endif; ?>