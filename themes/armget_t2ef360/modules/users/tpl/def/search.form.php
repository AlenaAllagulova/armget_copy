<?php
    tpl::includeJS(array('autocomplete'), true);
    $lng_all = _t('', 'Все');
    $lng_back = _t('form', 'Вернуться назад');
?>
    <button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#j-users-search-form-block"><i class="fa fa-cog pull-right"></i><?= _t('', 'Фильтр'); ?> <span class="small hidden-sm">(<?= _t('users', 'Найдено'); ?> <span class="j-users-count"><?= $count ?></span>)</span></button>
<div class="l-borderedBlock-armget">
    <div class="l-leftColumn l-borderedBlock collapse" id="j-users-search-form-block">
        <form action="" method="get">
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="m" value="<?= $f['m']?>" />

        <a href="#j-users-specs" data-toggle="collapse" data-parent="#accordion" class="h6 active"><i class="fa fa-chevron-down pull-right"></i><?= _t('', 'Специализации') ?></a>
        <div class="collapse in j-collapse" id="j-users-specs">
            <? if (Specializations::catsOn()): ?>
                <ul class="l-left-categories l-inside visible-md visible-lg">
                    <? foreach($specs as &$v): if(empty($v['specs'])) continue; ?>
                        <li<? if($v['a']){ ?> class="opened"<? } ?>>
                            <a href="<?= $v['url'] ?>" class="j-cat-title">
                                <i class="fa fa-caret-<?= $v['a'] ? 'down' : 'right'?>"></i>
                                <?= $v['title'] ?>
                            </a>
                            <ul<? if(!$v['a']){ ?> class="hidden"<? } ?>>
                                <li<? if($v['a'] && ! $spec_id){ ?> class="checked"<? } ?>><a href="<?= $v['url'] ?>"><?= $lng_all; ?></a></li>
                                <? foreach($v['specs'] as &$vv): ?>
                                    <li<? if($vv['a']){ ?> class="checked"<? } ?>><a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a></li>
                                <? endforeach; unset($vv); ?>
                            </ul>
                        </li>
                    <? endforeach; unset($v); ?>
                </ul>

                <div class="collapse in" id="j-mobile-cats">
                    <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                        <? foreach($specs as &$v): ?>
                            <li><a href="<?= $v['url'] ?>" class="j-mobile-cat" data-id="<?= $v['id']; ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                        <? endforeach; unset($v); ?>
                    </ul>
                </div>

                <? foreach($specs as &$v): if(empty($v['specs'])) continue; ?>
                    <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                        <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                            <li class="active">
                                <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= $lng_back; ?></span><br>
                                    <?= $v['title'] ?>
                                </a>
                                <ul>
                                    <li<? if($v['a'] && ! $spec_id){ ?> class="checked"<? } ?>>
                                        <a href="<?= $v['url'] ?>"><?= $lng_all; ?></a>
                                    </li>
                                    <? foreach($v['specs'] as &$vv): ?>
                                        <li<? if($vv['a']){ ?> class="checked"<? } ?>>
                                            <a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a>
                                        </li>
                                    <? endforeach; unset($vv); ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                <? endforeach; unset($v); ?>

            <? else: ?>

                <ul class="l-left-categories l-inside visible-md visible-lg">
                    <? foreach($specs as &$v): ?>
                        <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></li>
                    <? endforeach; unset($v); ?>
                </ul>

                <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                    <? foreach($specs as &$v): ?>
                        <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></li>
                    <? endforeach; unset($v); ?>
                </ul>

            <? endif; ?>
        </div>

        <div id="j-users-search-form-dp"><?= ! empty($dp) ? $dp : '' ?></div>

        <? if(Users::profileMap()): ?>
            <? if( ! Geo::filterEnabled()): ?>
            <? $bCountry = Geo::countrySelect(); $bOpenRegion = $f['ct'] || ($bCountry && $f['c']); ?>
            <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpenRegion ? ' active' : '' ?>"><i class="fa <?= $bOpenRegion ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('users', 'Регион'); ?></a>
            <div class="collapse <?= $bOpenRegion ? 'in' : '' ?> j-collapse" id="j-left-region">
                <div class="l-inside">
                    <div class="form">
                        <? if($bCountry): ?>
                            <div class="form-group">
                                <select name="c" class="form-control" id="j-left-region-country" autocomplete="off"><?= HTML::selectOptions(Geo::countryList(), $f['c'], _t('', 'Все страны'), 'id', 'title') ?></select>
                            </div>
                        <? endif; ?>
                        <div class="relative">
                            <input type="hidden" name="r" value="<?= $f['r'] ?>" id="j-region-pid-value" />
                            <input type="hidden" name="ct" value="<?= $f['ct'] ?>" id="j-region-city-value" />
                            <input type="text" class="form-control input-sm<?= $bCountry && empty($f['c']) ? ' hidden' : '' ?>" id="j-region-city-select" value="<?= $region_title ?>" placeholder="<?= _t('users', 'Или введите название региона'); ?>" />
                        </div>
                        <div class="relative mrgt20">
                            <input type="hidden" name="me" value="<?= $f['me'] ?>" id="j-region-metro-value" />
                            <input type="text" class="form-control input-sm<?= empty($metro_exists) ? ' hidden' : '' ?>" id="j-region-metro-select" value="<?= $metro_title ?>" placeholder="<?= _t('users', 'Или введите название станции метро'); ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <? else: if( ! empty($metro_exists)): $openMetro = $f['me']; ?>
                <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="h6<?= $openMetro ? ' active' : '' ?>"><i class="fa <?= $openMetro ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('users', 'Метро'); ?></a>
                <div class="collapse <?= $openMetro ? 'in' : '' ?> j-collapse" id="j-left-region">
                    <div class="l-inside">
                        <div class="form">
                            <div class="relative">
                                <input type="hidden" name="me" value="<?= $f['me'] ?>" id="j-region-metro-value" />
                                <input type="text" class="form-control input-sm" id="j-region-metro-select" value="<?= $metro_title ?>" placeholder="<?= _t('users', 'Введите название станции метро'); ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            <? endif; endif; ?>
        <? endif; ?>

        <? $bOpen = ! empty($f['exf']) || ! empty($f['ext']); ?>
        <a href="#j-left-experience" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpen ? ' active' : '' ?>"><i class="fa <?= $bOpen ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('users', 'Опыт работы <small>(лет)</small>'); ?></a>
        <div class="collapse<?= $bOpen ? 'in' : '' ?> j-collapse" id="j-left-experience">
            <div class="l-inside l-budget">
                <div class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm j-input-change" placeholder="<?= _t('', 'От'); ?>" name="exf" value="<?= ! empty($f['exf']) ? $f['exf'] : '' ?>" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-sm j-input-change" placeholder="<?= _t('', 'До'); ?>" name="ext"  value="<?= ! empty($f['ext']) ? $f['ext'] : '' ?>"/>
                    </div>
                </div>
            </div>
        </div>

        <? $aFlags = array(
            'fv'  => _t('users', 'Мои избранные'),
            'ex'  => _t('users', 'Только с примерами работ'),
            'op'  => _t('users', 'Только с отзывами'),
            'neg' => _t('users', 'Без отрицательных отзывов'),
            'fr'  => _t('users', 'Только со статусом "свободен"'),
            'vr'  => _t('users', 'Только подтвержденные [icon]', array('icon' => '<i class="fa fa-check-circle c-verified"></i>')),
            'pro' => _t('users', 'Только [pro]', array('pro' => '<i class="arm-pro"><img src="'. bff::url('/img/icon/pro.svg').'" alt=""></i>')),
        );
        if( ! Users::verifiedEnabled()){
            unset($aFlags['vr']);
        }
        if( ! bff::servicesEnabled()){
            unset($aFlags['pro']);
        }
        if( ! User::id() || Users::useClient() && ! User::isClient()){
            unset($aFlags['fv']);
        }
        ?>
        <? $bOpen = false; foreach($aFlags as $k => $v){ $bOpen |= $f[$k] ? 1 : 0; } ?>
        <a href="#left-more" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpen ? ' active' : '' ?>"><i class="fa <?= $bOpen ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('users', 'Дополнительно'); ?></a>
        <div class="collapse <?= $bOpen ? 'in' : '' ?> j-collapse" id="left-more">
            <div class="l-inside">
                <ul class="l-left-categories">
                    <? foreach($aFlags as $k => $v): ?>
                        <input type="hidden" name="<?= $k ?>" value="<?= $f[$k] ?>" />
                        <li class="<?= $f[$k] ? 'checked ' : '' ?>j-checkbox-flag" data-name="<?= $k ?>"><span><i class="fa <?= $f[$k] ? 'fa-check-square-o' : 'fa-square-o' ?>"></i> <?= $v ?></span></li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="l-inside text-center  mrgt10">
            <a href="#" class="clear-btn j-clear-filter"><?= _t('', 'Очистить фильтр'); ?></a>
        </div><!-- /.l-inside -->

        </form>

    </div>
</div>

    <div class="visible-md visible-lg">

        <?= ! empty($stairs) ? $stairs : '' ?>

        <? # Баннер: Исполнители: список ?>
        <?= Banners::view('users_list', array('pos'=>'left', 'spec'=>$spec_id)) ?>

    </div>