<?php
    /**
     * @var $this Orders
     */
    tpl::includeJS(array('autocomplete'), true);

    $isOneSpec = Orders::searchOneSpec();
    $isSpecCatsOn = Specializations::catsOn();

    $specClassLiCat = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(count($cat['specs']) == count($aSelected[ $cat['id'] ])){
                return ' class="opened checked" ';
            }
            return ' class="opened subchecked" ';
        }
        return '';
    };
    $specClassLiCatI = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(count($cat['specs']) == count($aSelected[ $cat['id'] ])){
                return 'fa-check-circle';
            }
            return 'fa-circle';
        }
        return 'fa-circle-o';
    };
    $catsClassLiCat = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(is_array($aSelected[ $cat['id'] ])){
                if(count($cat['sub']) == count($aSelected[ $cat['id'] ])){
                    return ' class="opened checked" ';
                }
                return ' class="opened subchecked" ';
            } else {
                return ' class="checked" ';
            }
        }
        return '';
    };
    $catsClassLiCatI = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(is_array($aSelected[ $cat['id'] ])){
                if(count($cat['sub']) == count($aSelected[ $cat['id'] ])){
                    return 'fa-check-circle';
                }
                return 'fa-circle';
            } else {
                return 'fa-check-circle';
            }
        }
        return 'fa-circle-o';
    };
?>
    <button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#j-orders-search-form-block"><i class="fa fa-cog pull-right"></i><?= _t('', 'Фильтр'); ?> <span class="small">(<?= _t('orders', 'Найдено'); ?> <span class="j-orders-count"><?= $count ?></span>)</span></button>
    <div class="l-borderedBlock-armget">
        <div class="l-leftColumn l-borderedBlock collapse" id="j-orders-search-form-block">
            <form action="" method="get">
            <input type="hidden" name="orders" value="<?= $f['orders'] ?>" />
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            <input type="hidden" name="m" value="<?= $f['m'] ?>" />
            <input type="hidden" name="4me" value="<?= $f['4me'] ?>" />
            <? if($f['t'] == Orders::TYPE_SERVICE): ?>
                <a href="#j-left-categories" data-toggle="collapse" data-parent="#accordion" class="h6 active"><i class="fa fa-chevron-down pull-right"></i><?= _t('', 'Специализации'); ?></a>
                <div class="collapse in j-collapse" id="j-left-categories">
                    <ul class="l-left-categories l-inside <?= $isSpecCatsOn ? 'visible-md visible-lg' : '' ?>">
                    <?  if($isSpecCatsOn):
                           foreach($specs as &$v): ?>
                            <li<?= $specClassLiCat($v, $fspecs) ?> data-id="<?= $v['id'] ?>"><span><i class="fa <?= $specClassLiCatI($v, $fspecs) ?> j-cat-check"></i> <span class="j-cat-title"><?= $v['title'] ?></span></span>
                                <ul class="<?= empty($fspecs[ $v['id'] ]) ? 'hidden' : '' ?>">
                                    <? foreach($v['specs'] as &$vv): ?>
                                        <li<?= ! empty($fspecs[ $v['id'] ][ $vv['id'] ]) ? ' class="checked"' : '' ?>><span class="j-spec" data-id="<?= $vv['id'] ?>" data-cat="<?= $v['id'] ?>" <? if($isOneSpec): ?>data-url="<?= $vv['url'] ?>" <? else: ?>data-keyword="<?= $vv['keyword'] ?>"<? endif; ?>><i class="fa <?= ! empty($fspecs[ $v['id'] ][ $vv['id'] ]) ? 'fa-check-square-o' : 'fa-square-o' ?>"></i> <?= $vv['title'] ?></span></li>
                                    <? endforeach; unset($vv); ?>
                                </ul>
                            </li>
                        <? endforeach; unset($v);
                        else:
                            if($isOneSpec):
                                foreach($specs as &$v): ?>
                                    <li<?= ! empty($fspecs[ $v['id'] ]) ? ' class="active"' : '' ?>><a href="<?= $v['url']; ?>"><i class="fa fa-caret-right"></i> <?= $v['title'] ?></a></li>
                            <?  endforeach; unset($v);
                            else:
                                foreach($specs as &$v): ?>
                                    <li<?= ! empty($fspecs[ $v['id'] ]) ? ' class="checked"' : '' ?>><span class="j-spec-nocat" data-id="<?= $v['id'] ?>" data-keyword="<?= $v['keyword'] ?>"><i class="fa <?= empty($fspecs[ $v['id'] ]) ? 'fa-square-o' : 'fa-check-square-o' ?>"></i> <?= $v['title'] ?></span></li>
                            <?  endforeach; unset($v);
                            endif;
                        endif; ?>
                    </ul>

                    <?  if($isSpecCatsOn): ?>
                        <!-- Mobile Categories -->
                        <div class="collapse in" id="j-mobile-cats">
                            <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                                <? foreach($specs as &$v): ?>
                                    <li><a href="#" class="j-mobile-cat" data-id="<?= $v['id'] ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                                <? endforeach; unset($v); ?>
                            </ul>
                        </div>
                        <? foreach($specs as &$v): ?>
                        <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                            <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                                <li class="active">
                                    <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= _t('form', 'Вернуться назад'); ?></span><br>
                                        <?= $v['title'] ?>
                                    </a>
                                    <ul>
                                        <? foreach($v['specs'] as &$vv): ?>
                                        <li class="checkbox">
                                            <label>
                                                <input class="j-mobile-spec" type="checkbox" value="<?= $vv['keyword'] ?>" <?= ! empty($fspecs[ $v['id'] ][ $vv['id'] ]) ? ' checked="checked"' : '' ?> <? if($isOneSpec): ?>data-url="<?= $vv['url'] ?>" <? endif; ?>data-id="<?= $vv['id'] ?>" data-cat="<?= $v['id'] ?>" autocomplete="off">
                                                <span>
                                                    <?= $vv['title']?>
                                                </span>
                                            </label>
                                        </li>
                                        <? endforeach; unset($vv); ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <? endforeach; unset($v); ?>
                    <? endif; ?>
                </div><!-- /#left-categories -->

                <? if(Orders::useProducts()):
                    $aServiceTypes = array(
                        Orders::SERVICE_TYPE_NONE    => array('id' => Orders::SERVICE_TYPE_NONE,    't' => _t('orders','Все')),
                        Orders::SERVICE_TYPE_ONE     => array('id' => Orders::SERVICE_TYPE_ONE,     't' => _t('orders','Разовые заказы')),
                        Orders::SERVICE_TYPE_JOB     => array('id' => Orders::SERVICE_TYPE_JOB,     't' => _t('orders','Постоянная работа')),
                        Orders::SERVICE_TYPE_CONTEST => array('id' => Orders::SERVICE_TYPE_CONTEST, 't' => _t('orders','Конкурсы')),
                    );
                    $aServiceType = isset($aServiceTypes[ $f['st'] ]) ? $aServiceTypes[ $f['st'] ] : reset($aServiceTypes);
                    $bOpenSt = $f['st'] != Orders::SERVICE_TYPE_NONE;
                ?>
                    <a href="#j-left-service-type" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpenSt ? ' active' : '' ?>"><i class="fa <?= $bOpenSt ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('orders', 'Тип заказа'); ?></a>
                    <div class="collapse <?= $bOpenSt ? 'in' : '' ?> j-collapse" id="j-left-service-type">
                        <div class="l-inside">
                            <div class="row">
                                <div class="col-sm-12">
                                    <? foreach($aServiceTypes as $v): ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="j-f-st" <?= $f['st'] == $v['id'] ? 'checked="checked" ' : '' ?>value="<?= $v['id'] ?>" autocomplete="off" name="st" />
                                                <span>
                                                    <?= $v['t'] ?>
                                                </span>
                                            </label>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <? else: ?>
                    <input type="hidden" name="st" value="<?= $f['st'] ?>" />
                <? endif; ?>
            <? endif; ?>
            <? if($f['t'] == Orders::TYPE_PRODUCT): ?>
                <input type="hidden" name="t" value="<?= $f['t'] ?>" />
                <a href="#j-left-categories" data-toggle="collapse" data-parent="#accordion" class="h6 active"><i class="fa fa-chevron-down pull-right"></i><?= _t('orders', 'Категории'); ?></a>
                <div class="collapse in j-collapse" id="j-left-categories">
                    <ul class="l-left-categories l-inside <?= $isSpecCatsOn ? 'visible-md visible-lg' : '' ?>">
                        <? foreach($cats as &$v):
                            $bSub = ! empty($v['sub']);
                            if( ! $bSub){  $sData = ($isOneSpec ? 'data-url="'.$v['url'].'"' : 'data-keyword="'.$v['keyword'].'"'); } ?>
                        <li<?= $catsClassLiCat($v, $fcats) ?> data-id="<?= $v['id'] ?>"><span<?= ! $bSub ? ' class="j-cat-parent" '.$sData : '' ?> data-id="<?= $v['id'] ?>"><i class="fa <?= $catsClassLiCatI($v, $fcats) ?> <?= $bSub ? 'j-cat-check' : '' ?>"></i> <span class="<?= $bSub ? 'j-cat-title' : '' ?>"><?= $v['title'] ?></span></span>
                            <? if($bSub): ?>
                            <ul class="<?= empty($fcats[ $v['id'] ]) ? 'hidden' : '' ?>">
                                <? foreach($v['sub'] as &$vv): $sData = ($isOneSpec ? 'data-url="'.$vv['url'].'"' : 'data-keyword="'.$vv['keyword'].'"'); ?>
                                    <li<?= ! empty($fcats[ $v['id'] ][ $vv['id'] ]) ? ' class="checked"' : '' ?>><span class="j-cat" data-id="<?= $vv['id'] ?>" data-cat="<?= $v['id'] ?>" <?= $sData ?>><i class="fa <?= ! empty($fcats[ $v['id'] ][ $vv['id'] ]) ? 'fa-check-square-o' : 'fa-square-o' ?>"></i> <?= $vv['title'] ?></span></li>
                                <? endforeach; unset($vv); ?>
                            </ul>
                            <? endif; ?>
                        </li>
                        <? endforeach; unset($v); ?>
                    </ul>

                    <!-- Mobile Categories -->
                    <div class="collapse in" id="j-mobile-cats">
                        <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                            <? foreach($cats as &$v):
                                if( ! empty($v['sub'])): ?>
                                    <li><a href="#" class="j-mobile-cat" data-id="<?= $v['id'] ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                                <? else:
                                    $sData = ($isOneSpec ? 'data-url="'.$v['url'].'"' : 'data-keyword="'.$v['keyword'].'"'); ?>
                                    <li>
                                        <label>
                                            <input type="checkbox" class="j-cat-mobile" value="<?= $v['keyword'] ?>" <?= ! empty($fcats[ $v['id'] ]) ? ' checked="checked"' : '' ?> <?= $sData ?> data-id="<?= $v['id'] ?>" autocomplete="off" />
                                            <span>
                                                <?= $v['title']?>
                                            </span>
                                        </label>
                                    </li>
                                <? endif;
                            endforeach; unset($v); ?>
                        </ul>
                    </div>
                    <? foreach($cats as &$v): if(empty($v['sub'])) continue; ?>
                        <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                            <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                                <li class="active">
                                    <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= _t('form', 'Вернуться назад'); ?></span><br>
                                        <?= $v['title'] ?>
                                    </a>
                                    <ul>
                                        <? foreach($v['sub'] as &$vv): $sData = ($isOneSpec ? 'data-url="'.$vv['url'].'"' : 'data-keyword="'.$vv['keyword'].'"'); ?>
                                            <li class="checkbox">
                                                <label>
                                                    <input class="j-cat-mobile" type="checkbox" value="<?= $vv['keyword'] ?>" <?= ! empty($fcats[ $v['id'] ][ $vv['id'] ]) ? ' checked="checked"' : '' ?> <?= $sData ?> data-id="<?= $vv['id'] ?>" data-cat="<?= $v['id'] ?>" autocomplete="off" />
                                                    <?= $vv['title']?>
                                                </label>
                                            </li>
                                        <? endforeach; unset($vv); ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <? endforeach; unset($v); ?>

                </div>
            <? endif; ?>

            <div id="j-orders-search-form-dp"><?= ! empty($dp) ? $dp : '' ?></div>

            <? if( ! Geo::filterEnabled()): $bCountry = Geo::countrySelect(); $bOpenRegion = $f['ct'] || ($bCountry && $f['c']); ?>
            <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpenRegion ? ' active' : '' ?>"><i class="fa <?= $bOpenRegion ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('orders', 'Регион'); ?></a>
            <div class="collapse <?= $bOpenRegion ? 'in' : '' ?> j-collapse" id="j-left-region">
                <div class="l-inside">
                    <div class="form">
                        <? if($bCountry): ?>
                        <div class="form-group">
                            <select name="c" class="form-control" id="j-left-region-country" autocomplete="off"><?= HTML::selectOptions(Geo::countryList(), $f['c'], _t('', 'Все'), 'id', 'title') ?></select>
                        </div>
                        <? endif; ?>
                        <div class="relative">
                            <input type="hidden" name="r" value="<?= $f['r'] ?>" id="j-region-pid-value" />
                            <input type="hidden" name="ct" value="<?= $f['ct'] ?>" id="j-region-city-value" />
                            <input type="text" class="form-control <?= $bCountry && empty($f['c']) ? ' hidden' : '' ?>" id="j-region-city-select" value="<?= $region_title ?>" placeholder="<?= _t('orders', 'Или введите название региона'); ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <? endif; ?>

            <div id="j-order-price-block" ><?= $this->viewPHP($aData, 'search.form.price.ex'); ?></div>

            <? if(bff::servicesEnabled()): ?>
            <div class="l-inside">
                <input type="hidden" name="pro" value="<?= $f['pro'] ?>" />
                <ul class="l-left-categories">
                    <li class="<?= $f['pro'] ? 'checked ' : '' ?>j-checkbox-pro">
                        <span>
                            <i class="fa <?= $f['pro'] ? 'fa-check-square-o' : 'fa-square-o' ?>"></i>
                            <?= _t('orders', 'Только для'); ?>
                            <i class="arm-pro"><img src="<?= bff::url('/img/icon/pro.svg')?>" alt=""></i>
                        </span>
                    </li>
                </ul>
            </div>
            <? endif; ?>

            <div class="l-inside text-center mrgt10">
                <? if( ! $isOneSpec): ?>
                    <input type="button" class="visible-sm visible-xs btn btn-block mrgb10 btn-primary btn-sm j-mobile-submit" value="<?= _t('', 'Применить'); ?>" />
                <? endif; ?>
                <a href="#" class="clear-btn j-clear-filter">
                    <?= _t('', 'Очистить фильтр'); ?>
                </a>
            </div>

            </form>

        </div>
    </div>
