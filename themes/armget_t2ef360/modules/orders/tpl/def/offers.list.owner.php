<?php

/**
 * Список заявок к заказу: владелец заказа
 * @var $this Orders
 */

tpl::includeJS('orders.offers', false, 3);

$statusList = array(
    Orders::OFFER_STATUS_CANDIDATE => array(
        'id'  => Orders::OFFER_STATUS_CANDIDATE,
        'tl'  => _t('orders-offers', 'Кандидат'),
        'tb'  => _t('orders-offers', 'Кандидат'),
        'cla' => 'label-candidate',
        'cli' => '',
        'fa'  => 'fa-check text-green'
    ),
    Orders::OFFER_STATUS_PERFORMER => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER,
        'tl'  => _t('orders-offers', 'Исполнитель'),
        'tb'  => _t('orders-offers', 'Исполнитель'),
        'cla' => 'label-chosen',
        'cli' => 'o-chosen-worker',
        'fa'  => 'fa-trophy text-orange',
    ),
    Orders::OFFER_STATUS_PERFORMER_START => array(),
    Orders::OFFER_STATUS_CANCELED  => array(
        'id'  => Orders::OFFER_STATUS_CANCELED,
        'tl'  => _t('orders-offers', 'Отказано'),
        'tb'  => _t('orders-offers', 'Отказать'),
        'cla' => 'label-decline',
        'cli' => 'o-declined-worker',
        'fa'  => 'fa-times text-red'
    ),
);
$ordersOpinions = Orders::ordersOpinions();
if($ordersOpinions){
    foreach(array(
        0, Orders::OFFER_STATUS_CANDIDATE,       Orders::OFFER_STATUS_PERFORMER,        Orders::OFFER_STATUS_CANCELED,
           Orders::OFFER_STATUS_PERFORMER_START, Orders::OFFER_STATUS_PERFORMER_AGREE,  Orders::OFFER_STATUS_PERFORMER_DECLINE,
           Orders::OFFER_STATUS_INVITE_DECLINE
        ) as $v){
            if( ! isset($statuses[$v])){
                $statuses[$v] = 0;
            }
    }
    $statuses[0] += $statuses[Orders::OFFER_STATUS_PERFORMER_START];
    $statuses[0] += $statuses[Orders::OFFER_STATUS_PERFORMER_AGREE];
    $statuses[Orders::OFFER_STATUS_PERFORMER_DECLINE] += $statuses[Orders::OFFER_STATUS_INVITE_DECLINE];
    $statusList[Orders::OFFER_STATUS_PERFORMER_START] = $statusList[Orders::OFFER_STATUS_PERFORMER];
    $statusList[Orders::OFFER_STATUS_PERFORMER_START]['id'] = Orders::OFFER_STATUS_PERFORMER_START;
    $statusList[Orders::OFFER_STATUS_PERFORMER_AGREE] = $statusList[Orders::OFFER_STATUS_PERFORMER];
    $statusList[Orders::OFFER_STATUS_PERFORMER_AGREE]['id'] = Orders::OFFER_STATUS_PERFORMER_AGREE;
    unset($statusList[Orders::OFFER_STATUS_PERFORMER]);
}else{
    unset($statusList[Orders::OFFER_STATUS_PERFORMER_START]);
}
$fairplayEnabled = bff::fairplayEnabled();
$showButtons = true;
if($ordersOpinions){
    if( ! empty($performer_id) && ! empty($performer_status) && $performer_status == Orders::OFFER_STATUS_PERFORMER_AGREE){
        $showButtons = false;
    }
}
?>
<header class="title">
    <h4 class="o-propositions-title"><i class="fa fa-comments-o"></i> <?= _t('orders-offers', 'Предложения от исполнителей'); ?> <span class="o-count-proposals"><?= $offers_cnt ?></span></h4>
</header>
<? if( ! empty($offers)): ?>
    <? if($ordersOpinions): ?>
    <div class="l-menu-filter">
        <button type="button" class="l-tabs-toggle btn btn-default j-status-filter-title" data-toggle="collapse" data-target="#j-status-filter"><i class="fa fa-chevron-down pull-right"></i><span class="j-title"><?= _t('orders', 'На рассмотрении'); ?></span> <span class="label label-count"><?= $statuses[0] ?></span></button>
        <ul class="nav nav-tabs collapse" id="j-status-filter">
            <li class="active"><a href="#" class="j-status-filter" data-status="0"><span class="j-title"><?= _t('orders', 'На рассмотрении'); ?></span> <span class="label label-count"><?= $statuses[0] ?></span></a></li>
            <li><a href="#" class="j-status-filter" data-status="<?= Orders::OFFER_STATUS_CANCELED ?>"><i class="fa fa-times text-red"></i> <span class="j-title"><?= _t('orders', 'Отказано'); ?></span> <span class="label label-count"><?= $statuses[Orders::OFFER_STATUS_CANCELED] ?></span></a></li>
            <li><a href="#" class="j-status-filter" data-status="<?= Orders::OFFER_STATUS_CANDIDATE ?>"><i class="fa fa-user text-green"></i> <span class="j-title"><?= _t('orders', 'Кандидаты'); ?></span> <span class="label label-count"><?= $statuses[Orders::OFFER_STATUS_CANDIDATE] ?></span></a></li>
            <li><a href="#" class="j-status-filter" data-status="<?= Orders::OFFER_STATUS_PERFORMER_DECLINE ?>"><i class="fa fa-times text-grey"></i> <span class="j-title"><?= _t('orders', 'Отказались'); ?></span> <span class="label label-count"><?= $statuses[Orders::OFFER_STATUS_PERFORMER_DECLINE] ?></span></a></li>
        </ul>
    </div>
    <? endif; ?>
    <ul class="o-freelancersList media-list" id="j-offers">

        <? foreach($offers as $v):
            $avatar =  UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szSmall, $v['sex']);
            $statusSet = array_key_exists($v['status'], $statusList);
            if($statusSet){
                $status = $statusList[ $v['status'] ];
            }
            if($v['user_blocked']): ?>
                <li class="media">
                    <div class="alert alert-danger">
                        <?= _t('', 'Пользователь заблокирован')?>
                    </div>
                </li>
                <? continue;
            endif; ?>
            <?
            # Стоимость:
            $sPrice = '';
            if( ! empty($v['price_from']) &&  ! empty($v['price_to'])){
                $sPrice = $v['price_from'].' - '.$v['price_to'];
            } else {
                if( ! empty($v['price_from'])){
                    $sPrice = _t('', 'от ').$v['price_from'];
                }
                if( ! empty($v['price_to'])){
                    $sPrice = _t('', 'до ').$v['price_to'];
                }
            }
            $sPrice .= ! empty($sPrice) ? ' '.Site::currencyData($v['price_curr'], 'title_short') : '';
            $aPriceRate = ( $v['price_rate_text'] ? func::unserialize($v['price_rate_text']) : array(LNG=>'') );
            if ( ! empty($aPriceRate[LNG]) && ! empty($sPrice)) { $sPrice .= ' '.$aPriceRate[LNG]; }

            # Сроки:
            $sTerms = '';$term = 0;
            if( ! empty($v['terms_from']) && ! empty($v['terms_to'])){
                $sTerms = $v['terms_from'].' - '.$v['terms_to'];
                $term = $v['terms_to'];
            } else {
                if( ! empty($v['terms_from'])){
                    $sTerms = _t('', 'от ').$v['terms_from'];
                    $term = $v['terms_from'];
                }
                if( ! empty($v['terms_to'])){
                    $sTerms = _t('', 'до ').$v['terms_to'];
                    $term = $v['terms_to'];
                }
            }
            if ( ! empty($sTerms)) {
                $sTerms = ' / '.$sTerms.' '.Specializations::aTerms($v['terms_type'], 'title');
            }
            ?>

            <li class="media<?= $statusSet ? ' '.$status['cli'] : '' ?><?= $ordersOpinions && ! in_array($v['status'], array(0, Orders::OFFER_STATUS_PERFORMER_START)) ? ' hidden' : '' ?> j-offer" data-status="<?= $v['status'] ?>" data-id="<?= $v['id'] ?>">
                <? if($ordersOpinions && $v['status'] == Orders::OFFER_STATUS_PERFORMER_START):
                    if( ! empty($v['workflow'])){ $aData['workflow'] = func::unserialize($v['workflow']); }
                    $aData['offerID'] = $v['id']; ?><?= $this->viewPHP($aData, 'offers.list.alert.performer.start'); ?>
                <? endif; ?>
                <a href="<?= Users::url('profile', array('login' => $v['login'])) ?>" class="o-freelancer-avatar">
                    <img src="<?= $avatar ?>" class="img-circle" alt="<?= tpl::avatarAlt($v); ?>" />
                    <?= tpl::userOnline($v) ?>
                </a>
                <div class="media-body">
                    <div class="o-freelancer-info">
                        <div class="j-worker-info">
                            <?= tpl::userLink($v, 'strong no-login') ?> <span class="label j-label <?= $statusSet ? $status['cla'] : '' ?>"><?= $statusSet ? $status['tl'] : ''?></span>
                            <? if($v['is_new']): ?><span class="label label-new"><?= _t('orders', 'Новое предложение') ?></span><? endif; ?>
                            <? if($v['status'] == Orders::OFFER_STATUS_INVITE_DECLINE): ?><span class="label label-count"><?= _t('orders', 'Отказался от предложения') ?></span><? endif; ?>
                            <? if($showButtons): ?>
                            <div class="o-freelancer-buttons">
                                <div class="btn-group">
                                    <? foreach($statusList as $vv):
                                            if($vv['id'] == Orders::OFFER_STATUS_PERFORMER_AGREE) continue;
                                            $bActive = $v['status'] == $vv['id'];
                                            $bDisabled = $ordersOpinions
                                                ? ( $performer_id && $v['user_id'] == $performer_id ) || ($performer_id && $vv['id'] == Orders::OFFER_STATUS_PERFORMER_START)
                                                : $vv['id'] == (Orders::OFFER_STATUS_PERFORMER && $performer_id && $v['user_id'] != $performer_id);
                                            $start = '';
                                            if($vv['id'] == Orders::OFFER_STATUS_PERFORMER_START){
                                                $start = ' data-fp="'.$v['fairplay'].'" data-term="'.$term.'" ';
                                            }
                                        ?>
                                    <button type="button" class="btn btn-default btn-sm <?= $bActive ? 'active' : '' ?> j-status j-status-<?= $vv['id'] ?>" <?= $bDisabled ? 'disabled="disabled"' : '' ?> data-id="<?= $v['id'] ?>" data-s="<?= $vv['id'] ?>"<?= $start ?>><i class="fa <?= $vv['fa'] ?> hidden-xs"></i> <?= $vv['tb'] ?></button>
                                    <? endforeach; ?>
                                </div>
                            </div>
                            <? endif; ?>
                        </div>

                        <? if($v['spec_id']): ?>
                            <div>
                                <?= _t('orders-offers', 'Специализация:'); ?> <a href="<?= Users::url('search-spec', $v) ?>"><?= $v['spec_title'] ?></a>
                            </div>
                        <? endif; ?>
                        <div class="o-feedbacks-inf">
                            <?= _t('orders-offers', 'Отзывы работодателей'); ?> <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                        </div>
                        <div class="o-feedbacks-date">
                            <span> <?= _t('', 'Добавлено [date]', array('date' => tpl::date_format3($v['created'])))?></span>
                            <? if($v['reg3_city']): ?><span><i class="fa fa-map-marker"></i> <?= Geo::regionTitle($v['reg3_city']) ?><?= Geo::countrySelect() ? ', '.Geo::regionTitle($v['reg1_country']) : '' ?></span><? endif; ?>
                        </div>
                        <? if($fairplayEnabled && ! $fairplay && $v['fairplay']): ?>
                            <div>
                                <?= _t('fp', 'Предпочитаю оплату работы через <a [link]>Безопасную Сделку</a>', array('link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?>
                            </div>
                        <? endif; ?>
                    </div>

                    <div class="o-proposal-text j-chat-block">
                        <div class="o-proposed-terms"><strong><?= $sPrice ?></strong> <?= $sTerms ?></div>
                        <?= nl2br($v['descr']) ?>

                        <div class="o-proposal-chat">
                            <div class="pull-left">
                                <a href="#o-proposal-chat-window-<?= $v['id'] ?>" class="ajax-link j-open-chat j-open-chat-response" data-toggle="collapse" data-parent="#accordion" data-id="<?= $v['id'] ?>"><i class="fa fa-reply c-link-icon"></i><span><?= _t('orders-offers', 'Написать ответ'); ?></span></a>
                                <? if($v['chatcnt']): ?><a href="#o-proposal-chat-window-<?= $v['id'] ?>" class="ajax-link j-open-chat" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-comment c-link-icon"></i><span class="j-open-chat-title"><?= _t('orders-offers', 'Развернуть переписку'); ?></span>
                                    [<?= $v['chatcnt'] ?>]<?= $v['chat_new_client'] ? ' <span class="label label-new">+'.$v['chat_new_client'].'</span>' : '' ?></a><? endif; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="o-proposal-chat-window collapse" id="o-proposal-chat-window-<?= $v['id'] ?>" data-cnt="<?= $v['chatcnt'] ?>" data-id="<?= $v['id'] ?>">
                            <ul class="j-chat"></ul>

                            <form role="form" action="" method="post">
                                <input type="hidden" name="id" value="<?= $v['id'] ?>" />
                                <div class="form-group">
                                    <textarea name="message" class="form-control mrgt20" rows="3"></textarea>
                                </div>
                                <button class="btn btn-primary btn-sm j-submit"><?= _t('', 'Отправить'); ?></button>
                            </form>

                        </div>

                    </div>

                    <? if($v['contacts']):
                        $aContacts = func::unserialize($v['contacts']);
                        $aTypes = Users::aContactsTypes();
                        foreach($aContacts as $vv){
                            $aTypes[ $vv['t'] ]['links'][] = tpl::linkContact($vv, true);;
                        } ?>
                        <div class="o-collapsed-info">
                            <button type="button" class="o-collapsed-info_toggle btn btn-default" data-toggle="collapse" data-target="#o-contact-info-<?= $v['id'] ?>"><i class="fa fa-chevron-down pull-right"></i><?= _t('', 'Контакты'); ?></button>
                            <div class="collapse" id="o-contact-info-<?= $v['id'] ?>">
                                <div class="o-collapsed-inner">
                                <? if(Catcher::isViewContactsWithPrepay()): ?>
                                    <? $isOwner = User::isCurrent($v['user_id']);
                                    $isShowContacts = Catcher::i()->isShowContacts(User::id(), $v['user_id']);?>
                                    <? if( User::id() && ($isOwner || $isShowContacts)): ?>
                                        <!--noindex-->
                                        <table class="table table-condensed">
                                            <tbody>
                                            <? foreach($aTypes as $vv): if( empty( $vv['links'])) continue;?>
                                                <tr>
                                                    <td><?= $vv['t'] ?>:</td>
                                                    <td>
                                                        <?= join('<br />', $vv['links']) ?>
                                                    </td>
                                                </tr>
                                            <? endforeach; ?>
                                            </tbody>
                                        </table>
                                        <!--/noindex-->
                                    <? endif;?>
                                <? else: # стандартное поведение в проекте?>
                                    <? if(Users::isContactsView($v['user_id'], $v['user_type'], $v['pro'], $contactsHideReason)):?>
                                        <!--noindex-->
                                        <table class="table table-condensed">
                                            <tbody>
                                            <? foreach($aTypes as $vv): if( empty( $vv['links'])) continue;?>
                                                <tr>
                                                    <td><?= $vv['t'] ?>:</td>
                                                    <td>
                                                        <?= join('<br />', $vv['links']) ?>
                                                    </td>
                                                </tr>
                                            <? endforeach; ?>
                                            </tbody>
                                        </table>
                                    <!--/noindex-->
                                    <? else: ?>
                                        <?= $contactsHideReason ?>
                                    <? endif; ?>
                                <? endif; ?>
                                </div>
                            </div>
                        </div>

                    <? endif; ?>

                    <?= ! empty($v['examples']) ? $v['examples'] : '' ?>
                </div>
            </li>
        <? endforeach; ?>
    </ul>
<? endif; ?>
<? if($ordersOpinions): ?>
<div class="modal fade" id="j-modal-worker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('', 'Выбрать исполнителя'); ?></h4>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-unstyled-morespace">
                    <li><strong><?= $order_title ?></strong></li>
                    <li><?= _t('orders', 'Исполнитель:'); ?> <span class="nowrap j-worker-name"></span></li>
                </ul>
                <form method="post">
                    <input type="hidden" name="id" value="0" />
                    <? if($fairplayEnabled): ?>
                        <div class="form-horizontal">
                            <div class="form-group o-propose-inputs">
                                <label for="j-workflow-period" class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Срок'); ?></label>
                                <div class="col-sm-9">
                                    <div class="input-group j-required">
                                        <input type="text" name="term" id="j-workflow-period" class="form-control">
                                        <div class="o-propose-inputs-txt"><?= _t('fp', 'дней'); ?></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group o-propose-inputs">
                                <label for="j-workflow-price" class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Бюджет'); ?></label>
                                <div class="col-sm-9">
                                    <div class="input-group j-required">
                                        <input type="text" name="price" id="j-workflow-price" class="form-control input-sm" value="<?= $price ?>">
                                        <div class="o-propose-inputs-txt"><?= Site::currencyDefault('title_short'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <? if(Catcher::isUseOrderPrepay() && isset($prepay) && !empty($prepay)):?>
                                <div class="form-group o-propose-inputs">
                                    <label class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Предоплата'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="text" name="perpay" class="form-control input-sm" readonly="readonly" disabled="disabled" value="<?= $prepay ?>">
                                            <div class="o-propose-inputs-txt"><?= Site::currencyDefault('title_short'); ?></div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="form-group <?=($order_service_type == Orders::SERVICE_TYPE_JOB)? 'hidden': ''?>">
                                <label class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Способ оплаты'); ?></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_USE ?>">
                                            <?= _t('fp', 'Безопасная сделка (с резервированием бюджета) [link]', array('link' => '<a href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-question-circle"></i> </a>')); ?>
                                        </label>
                                        <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Безопасное сотрудничество с гарантией возврата денег. Вы резервируете бюджет заказа, а мы гарантируем вам возврат суммы, если работа выполнена некачественно или не в срок.'); ?></div>
                                    </div>
                                    <div class="radio <?= (! Catcher::onlyFairplay())? '': 'hidden'?>">
                                        <label>
                                            <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_NONE ?>">
                                            <?= _t('fp', 'Прямая оплата исполнителю на его кошелек/счет'); ?>
                                        </label>
                                        <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Сотрудничество без участия сайта в процессе оплаты. Вы сами договариваетесь с исполнителем о способе и порядке оплаты. И самостоятельно регулируете все претензии, связанные с качеством и сроками выполнения работы.'); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="form-group">
                        <textarea rows="5" class="form-control" name="message" placeholder="<?= _t('orders', 'Сообщение исполнителю (не обязательно)'); ?>"></textarea>
                        <div class="help-block j-help-block"></div>
                    </div>
                    <div class="c-formSubmit">
                        <button type="submit" class="btn btn-success c-formSuccess j-submit"><?= _t('orders', 'Выбрать исполнителем'); ?></button>
                        <a class="c-formCancel ajax-link j-cancel" href="#" data-dismiss="modal"><span><?= _t('form', 'Отмена'); ?></span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<? endif; ?>
<script type="text/javascript">
<? js::start() ?>
jOrdersOfferChat.init(<?= func::php2js(array(
    'lang'   => array(
        'chat_title_show' => _t('orders-offers', 'Развернуть переписку'),
        'chat_title_hide' => _t('orders-offers', 'Скрыть переписку'),
    ),
    'id' => $this->input->get('chat'),
)) ?>);
jOrdersOffersPerformer.init(<?= func::php2js(array(
    'lang'   => array(
        'left'    => _t('users','[symbols] осталось'),
        'symbols' => explode(';', _t('users', 'знак;знака;знаков')),
        'candidate' => _t('orders', 'Исполнитель был успешно назначен кандидатом'),
    ),
    'statuses' => $statusList,
    'ordersOpinions' => $ordersOpinions,
    'fairplayEnabled' => $fairplayEnabled ? 1 : 0,
    'fairplay'        => $fairplay,
    'fairplay_use'    => Orders::FAIRPLAY_USE,
    'fairplay_none'   => Orders::FAIRPLAY_NONE,
    'st' => array(
        'canceled'  => Orders::OFFER_STATUS_CANCELED,
        'performer' => Orders::OFFER_STATUS_PERFORMER,
        'candidate' => Orders::OFFER_STATUS_CANDIDATE,
        'start'     => Orders::OFFER_STATUS_PERFORMER_START,
        'agree'     => Orders::OFFER_STATUS_PERFORMER_AGREE,
        'decline'   => Orders::OFFER_STATUS_PERFORMER_DECLINE,
        'invite_no' => Orders::OFFER_STATUS_INVITE_DECLINE,
        ),
)) ?>);
<? js::stop() ?>
</script>