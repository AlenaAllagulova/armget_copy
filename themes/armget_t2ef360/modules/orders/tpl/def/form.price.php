<?php
$aPriceSett = array(
    'rates' => array(),
    'price_title' => _t('orders', 'Бюджет'),
    'price_title_mod' => _t('orders', 'По договоренности'),
    'curr' => 0,
);
if( ! empty($spec['spec_id'])){
    $aPriceSett = Specializations::i()->aPriceSett($spec['spec_id']);
}
$bShowPriceEx = ! empty($aPriceSett['ex']);
$sliderPrice =  Catcher::sliderPrice();
?>
<div class="form-group o-propose-inputs">
    <label for="price" class="col-sm-3 control-label o-control-label">
        <span class="j-single <?= !empty($aData['type']) && $aData['type'] == 1 ? 'hidden': ''?>">
            <?= $aPriceSett['price_title'] ?>
        </span>
        <span class="j-full-time <?= !empty($aData['type']) &&  $aData['type'] != 1 ? 'hidden' : ''?>">
            <?= _t('','Оплата труда')?>
        </span>
        <i class="text-danger">*</i>
    </label>
    <div class="col-sm-9">
        <div class="input-group <?= $bShowPriceEx ? 'radio' : '' ?> j-price-ex-radio">
            <input class="<?= $bShowPriceEx ? '' : 'hidden' ?> j-price-ex j-price-ex-<?= Specializations::PRICE_EX_PRICE ?>" type="radio" name="price_ex" value="<?= Specializations::PRICE_EX_PRICE ?>" <?= $price_ex == Specializations::PRICE_EX_PRICE ? ' checked="checked"' : '' ?> />
            <input id="price-number" type="text" name="price" value="<?= $price ?>" maxlength="9" class="form-control input-sm" />
            <select name="price_curr" id="price_curr" class="form-control input-sm">
                <?= Site::currencyOptions( ! empty($price_curr) ? $price_curr : $aPriceSett['curr']) ?>
            </select>
            <select name="price_rate" class="form-control input-sm <?= empty($aPriceSett['rates']) ? 'hidden' : '' ?>">
                <?= ! empty($aPriceSett['rates']) ? HTML::selectOptions($aPriceSett['rates'], $price_rate) : '' ?>
            </select>
        </div>
        <div class="radio <?= $bShowPriceEx && ($aPriceSett['ex'] & Specializations::PRICE_EX_AGREE)  ? '' : 'hidden' ?> j-price-ex j-price-ex-<?= Specializations::PRICE_EX_AGREE ?>">
            <label>
                <input type="radio" name="price_ex" value="<?= Specializations::PRICE_EX_AGREE ?>" <?= $price_ex == Specializations::PRICE_EX_AGREE ? ' checked="checked"' : '' ?>> <?= $aPriceSett['price_title_mod'] ?>
            </label>
        </div>
    </div>
</div>

<?if(Catcher::isUseOrderPrepay()):?>
<div class="form-group o-propose-inputs">
    <label for="price" class="col-sm-3 control-label o-control-label"><?= _t('', 'Оставить предоплату')?></label>
    <div class="col-sm-9">
        <div class="form-inline">
            <div class="form-group">
                <div class="input-group">
                    <? if($sliderPrice): ?>
                        <div class="input-group-addon j-count-percent">%</div>
                    <? endif; ?>
                    <input type="number"
                           style="border-radius: 0!important;"
                           id="input-number"
                           step="1"
                           min="<?=Catcher::MIN_PREPAY?>"
                           name="prepay"
                           value="<?= (!empty($edit) && !empty($prepay))? $prepay : Catcher::MIN_PREPAY?>"
                           <?= (!empty($edit) && !empty($prepay))? 'readonly="readonly"' : ''?>
                           class="form-control input-sm" />
                    <div class="input-group-addon"><?= Site::currencyDefault()?></div>
                </div>
            </div>
        </div>
        <? if($sliderPrice): ?>
            <div class="mrgt50 mrgb50 <?= (!empty($edit) && !empty($prepay))? 'hidden' : ''?>">
                <div id="slider-connect"></div>
            </div>
        <? endif; ?>
    </div>
</div>
<?endif;?>