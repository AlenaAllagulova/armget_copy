<?php
if (empty($nProhibited)) $nProhibited = 0;

if ($nProhibited & 1): ?>
<div class="alert alert-info"><?= _t('orders', 'Вы превысили лимит предложений на сегодня.'); ?>
<? if (bff::servicesEnabled() && ! User::isPro()): ?>
    <?= _t('orders', 'Для увеличения лимита необходимо купить <a [link_pro]>аккаунт</a><span class="arm-pro"><img src="'.bff::url('/img/icon/pro.svg').'" alt=""></span>', array('link_pro' => 'href="'.Svc::url('view', array('keyword' => 'pro')).'"')); ?>
<? endif; ?>
</div>
<? elseif ($nProhibited & 2): ?>
    <div class="alert alert-info"><?= _t('orders', 'Вы не можете оставить предложение. Только для [pro].', array('pro'=>'<span class="arm-pro"><img src="'.bff::url('/img/icon/pro.svg').'" alt=""></span>')); ?>
        <?= _t('orders', 'Купить <a [link_pro]>аккаунт PRO</a>', array('link_pro' => 'href="'.Svc::url('view', array('keyword' => 'pro')).'"')); ?>
    </div>
<? elseif ($nProhibited & 4): ?>
    <div class="alert alert-warning">
        <?= _t('orders', 'Вы не можете оставить предложение к заказу, который не соответствует вашей специализации.'); ?>
    </div>
<? endif;