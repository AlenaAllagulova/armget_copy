<?php

/**
 * Список заявок к заказу: зарегистрированный пользователь
 * @var $this Orders
 */

$statusList = array(
    Orders::OFFER_STATUS_CANDIDATE => array(
        'id' => Orders::OFFER_STATUS_CANDIDATE,
        't'  => _t('orders-offers', 'Вы кандидат'),
        'd'  => _t('orders-offers', 'Работодатель определил Вас как кандидата на этот заказ. Это значит, что Вы прошли предварительный отбор и, возможно, будете выбраны исполнителем.'),
        'c'  => 'alert-chosen',
    ),
    Orders::OFFER_STATUS_PERFORMER => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER,
        't'  => _t('orders-offers', 'Вы исполнитель'),
        'd'  => _t('orders-offers', 'Работодатель определил Вас как исполнителя на этот заказ.'),
        'c'  => 'alert-candidate',
    ),
    Orders::OFFER_STATUS_CANCELED  => array(
        'id'  => Orders::OFFER_STATUS_CANCELED,
        't'  => _t('orders-offers', 'Вам отказанно'),
        'd'  => _t('orders-offers', 'Работодатель отказал Вам.'),
        'c'  => 'alert-decline',
    ),
    Orders::OFFER_STATUS_PERFORMER_DECLINE => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_DECLINE,
        't'  => _t('orders-offers', 'Отказались'),
        'd'  => _t('orders-offers', 'Вы отказались от заказа.'),
        'c'  => 'alert-decline',
    ),
    Orders::OFFER_STATUS_PERFORMER_AGREE => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_AGREE,
        't'  => _t('orders-offers', 'Вы исполнитель'),
        'd'  => _t('orders-offers', 'Вы согласились стать исполнителем на этот заказ.'),
        'c'  => 'alert-candidate',
    ),
);
$fairplayEnabled = bff::fairplayEnabled();
?>
<header class="title">
    <h4 class="o-propositions-title"><i class="fa fa-comments-o"></i> <?= _t('orders-offers', 'Предложения от исполнителей'); ?> <span class="o-count-proposals"><?= $offers_cnt ?></span></h4>
</header>
<? if( ! empty($offers)): ?>
    <ul class="o-freelancersList media-list" id="j-offers-performer-start">

        <? foreach($offers as $v):
            $avatar =  UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szSmall, $v['sex']);
            $is_my = ! empty($v['bMy']);
            if($v['user_blocked']): ?>
                <li class="media">
                    <div class="alert alert-danger">
                        <?= _t('', 'Пользователь заблокирован')?>
                    </div>
                </li>
            <? continue;
            endif; ?>
            <li class="media<? if($is_my){ ?> my-proposal<? } ?>">
                <? if($is_my && array_key_exists($v['status'], $statusList)): $status = $statusList[ $v['status'] ];
                        if($fairplayEnabled && $v['status'] == Orders::OFFER_STATUS_PERFORMER_AGREE && ! empty($fairplay) && ! empty($order_id)){
                            Fairplay::i()->checkReserved($order_id, $keyword, $status['d']);
                        }
                    ?>
                <div class="alert <?= $status['c'] ?>">
                    <div><strong><?= $status['t'] ?></strong></div>
                    <?= $status['d'] ?>
                </div>
                <? endif; ?>
                <?= ! empty($v['performer_start']) ? $v['performer_start'] : '' ?>

                <a href="<?= Users::url('profile', array('login' => $v['login'])) ?>" class="o-freelancer-avatar">
                    <img src="<?= $avatar ?>" class="img-circle" alt="<?= tpl::avatarAlt($v); ?>" />
                    <?= tpl::userOnline($v) ?>
                </a>
                <div class="media-body">
                    <div class="o-freelancer-info">
                        <div>
                            <strong><?= tpl::userLink($v, 'no-login') ?></strong>
                            <? if($v['status'] == Orders::OFFER_STATUS_INVITE_DECLINE): ?><span class="label label-count"><?= _t('orders', 'Отказался от предложения') ?></span><? endif; ?>
                        </div>
                        <? if($v['spec_id']): ?>
                            <div>
                                <?= _t('orders-offers', 'Специализация:'); ?> <a href="<?= Users::url('search-spec', $v) ?>"><?= $v['spec_title'] ?></a>
                            </div>
                        <? endif; ?>
                        <div class="o-feedbacks-inf">
                            <?= _t('orders-offers', 'Отзывы работодателей'); ?> <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                        </div>
                        <div class="o-feedbacks-date">
                            <span> <?= _t('', 'Добавлено [date]', array('date' => tpl::date_format3($v['created'])))?></span>
                            <? if($v['reg3_city']): ?><span><i class="fa fa-map-marker"></i> <?= Geo::regionTitle($v['reg3_city']) ?><?= Geo::countrySelect() ? ', '.Geo::regionTitle($v['reg1_country']) : '' ?></span><? endif; ?>
                        </div>
                        <? if($fairplayEnabled && ! $fairplay && $v['fairplay']): ?>
                        <div>
                            <?= _t('fp', 'Предпочитаю оплату работы через <a [link]>Безопасную Сделку</a>', array('link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?>
                        </div>
                        <? endif; ?>
                    </div>

                    <?
                    # Стоимость:
                    $sPrice = '';
                    if( ! empty($v['price_from']) &&  ! empty($v['price_to'])){
                        $sPrice = $v['price_from'].' - '.$v['price_to'];
                    } else {
                        if( ! empty($v['price_from'])){
                            $sPrice = _t('', 'от ').$v['price_from'];
                        }
                        if( ! empty($v['price_to'])){
                            $sPrice = _t('', 'до ').$v['price_to'];
                        }
                    }
                    $sPrice .= ! empty($sPrice) ? ' '.Site::currencyData($v['price_curr'], 'title_short') : '';
                    $aPriceRate = ( $v['price_rate_text'] ? func::unserialize($v['price_rate_text']) : array(LNG=>'') );
                    if ( ! empty($aPriceRate[LNG]) && ! empty($sPrice)) { $sPrice .= ' '.$aPriceRate[LNG]; }

                    # Сроки:
                    $sTerms = '';
                    if( ! empty($v['terms_from']) && ! empty($v['terms_to'])){
                        $sTerms = $v['terms_from'].' - '.$v['terms_to'];
                    } else {
                        if( ! empty($v['terms_from'])){
                            $sTerms = _t('', 'от ').$v['terms_from'];
                        }
                        if( ! empty($v['terms_to'])){
                            $sTerms = _t('', 'до ').$v['terms_to'];
                        }
                    }
                    if ( ! empty($sTerms)) {
                        $sTerms = ' / '.$sTerms.' '.Specializations::aTerms($v['terms_type'], 'title');
                    }
                    ?>
                    <div class="o-proposal-text j-chat-block">
                        <div class="o-proposed-terms"><strong><?= $sPrice ?></strong> <?= $sTerms ?></div>
                        <?= $v['descr'] ?>
                    </div>

                    <? if($v['contacts']):
                        $aContacts = func::unserialize($v['contacts']);
                        $aTypes = Users::aContactsTypes();
                        foreach($aContacts as $vv){
                            $aTypes[ $vv['t'] ]['links'][] = tpl::linkContact($vv, true);;
                        } ?>
                        <div class="o-collapsed-info">
                            <button type="button" class="o-collapsed-info_toggle btn btn-default" data-toggle="collapse" data-target="#o-contact-info-<?= $v['id'] ?>"><i class="fa fa-chevron-down pull-right"></i><?= _t('', 'Контакты'); ?></button>
                            <div class="collapse" id="o-contact-info-<?= $v['id'] ?>">
                                <div class="o-collapsed-inner">
                                <? if(Catcher::isViewContactsWithPrepay()): ?>
                                    <? $isOwner = User::isCurrent($v['user_id']);
                                    $isShowContacts = Catcher::i()->isShowContacts(User::id(), $v['user_id']);?>
                                    <? if(User::id() && ($isOwner || $isShowContacts)): ?>
                                        <!--noindex-->
                                        <table class="table table-condensed">
                                            <tbody>
                                            <? foreach($aTypes as $vv): if( empty( $vv['links'])) continue;?>
                                                <tr>
                                                    <td><?= $vv['t'] ?>:</td>
                                                    <td>
                                                        <?= join('<br />', $vv['links']) ?>
                                                    </td>
                                                </tr>
                                            <? endforeach; ?>
                                            </tbody>
                                        </table>
                                        <!--/noindex-->
                                    <? endif;?>
                                <? else: # стандартное поведение в проекте?>
                                    <? if(Users::isContactsView($v['user_id'], $v['user_type'], $v['pro'], $contactsHideReason)): ?>
                                    <!--noindex-->
                                    <table class="table table-condensed">
                                        <tbody>
                                        <? foreach($aTypes as $vv): if( empty( $vv['links'])) continue;?>
                                            <tr>
                                                <td><?= $vv['t'] ?>:</td>
                                                <td>
                                                    <?= join('<br />', $vv['links']) ?>
                                                </td>
                                            </tr>
                                        <? endforeach; ?>
                                        </tbody>
                                    </table>
                                    <!--/noindex-->
                                    <? else: ?>
                                        <?= $contactsHideReason ?>
                                    <? endif; ?>
                                <? endif; ?>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>

                    <?= ! empty($v['examples']) ? $v['examples'] : '' ?>

                </div>
            </li>
        <? endforeach; ?>
    </ul>
<? endif;
    if($offers_cnt > count($offers)): ?>
    <div class="alert alert-info mrgt20">
        <?= _t('orders-offers', 'Остальные предложения скрыты и видны только заказчику'); ?>
    </div>
<? endif;