<?php
    if( ! isset($nTagID)) $nTagID = 0;
if( ! empty($list)):
    $lng_pro_only = _t('orders', 'Только для [pro]', array('pro'=>'<span class="arm-pro"><img src="'.bff::url('/img/icon/pro.svg').'" alt=""></span>'));
    $lng_offers = _t('orders', 'предложение;предложения;предложений');
    $aServiceTypes = Orders::aServiceTypes();
    $fairplayEnabled = bff::fairplayEnabled();
    ?>
    <ul class="media-list l-projectList l-search-results">
        <? foreach($list as $v): ?>
            <li class="media">
                <div class="l-list-num"><?= $v['n'] ?>.</div>

                <div class="media-body">
                    <header class="l-project-title">
                        <? if($v['status'] == Orders::STATUS_CLOSED): ?><i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i><? endif; ?>
                        <? if($fairplayEnabled && $v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
                        <a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a>
                    </header>

                    <div class="l-project-head">
                        <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                            <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                                <span class="l-price_na"><?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?></span>
                            <? else: ?>
                                <span class="l-price"><?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><small><?= $v['price_rate_text'][LNG] ?></small><? endif; ?></span>
                            <? endif; ?>
                        <? endif; ?>
                        <? if( ! empty($v['aTags'])): foreach($v['aTags'] as $vv):
                            $bSearch = ($nTagID == $vv['id']);
                            if ( ! $bSearch && $f['q']) {
                                $bSearch = $f['q'] == $vv['tag'];
                            }
                            ?>
                            <a href="<?= Orders::url('search-tag', $vv) ?>" class="l-tag<?= $bSearch ? ' l-search-tag' : '' ?>"><?= $vv['tag'] ?></a>
                        <? endforeach; endif; ?>
                    </div>
                    <article>
                        <p><?= nl2br(tpl::truncate($v['descr'], config::sysAdmin('orders.search.keywords.list.descr.truncate', 250, TYPE_UINT))); ?></p>
                    </article>

                    <ul class="l-item-features">
                        <? if($v['pro']): ?><li><span class="small"><?= $lng_pro_only; ?></span></li><? endif; ?>
                        <? if( ! empty($v['city_data']['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $v['city_data']['title']?></li><? endif; ?>
                        <? if($v['type'] == Orders::TYPE_SERVICE):?><li><i class="fa fa-<?= $aServiceTypes[ $v['service_type'] ]['c'] ?>"></i> <?= $aServiceTypes[ $v['service_type'] ]['t'] ?></li><? endif; ?>
                        <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($v['created'], false, true) ?></li>
                        <? if($v['expire'] != '0000-00-00 00:00:00'): ?><li><i class="fa fa-calendar"></i> <?= _t('orders', 'до [date]', array('date' => tpl::date_format3($v['expire'], 'd.m.Y'))); ?></li><? endif; ?>
                        <li><a href="<?= $v['url_view'] ?>#offers"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['offers_cnt'], $lng_offers)?></span></a></li>
                    </ul>
                </div>
            </li>
        <? endforeach; ?>
    </ul><!-- /.project-list -->
<? else: ?>
    <div class="alert alert-info"><?= _t('orders', 'Заказы не найдены'); ?></div>
<? endif;