<?php
if($f['t'] != Orders::TYPE_SERVICE) return;
if( ! isset($nSpecID)){
    $nSpecID = Specializations::ROOT_SPEC;
}
if( ! empty($fspecs)){
    $aSpecs = $fspecs;
    if(Specializations::catsOn()){
        $aSpecs = reset($aSpecs);
    }
    if(count($aSpecs) == 1){
        $keys = array_keys($aSpecs);
        $nSpecID = reset($keys);
    }
}
if( ! isset($aData['nSpecID'])){
    $aData['nSpecID'] = $nSpecID;
}
$aPriceSett = array(
    'rates' => array(),
    'price_title' => _t('orders', 'Бюджет'),
    'price_title_mod' => _t('orders', 'По договоренности'),
    'curr' => 0,
);
if($nSpecID) {
    $aPriceSett = Specializations::i()->aPriceSett($nSpecID);
}

$bShowPriceEx = ! empty($aPriceSett['ex']);
$bOpenBudget =  $f['pf'] || $f['pt'] || $f['pex']; ?>
<a href="#j-left-budget" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpenBudget ? ' active' : '' ?>"><i class="fa <?= $bOpenBudget ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= $aPriceSett['price_title'] ?></a>
<div class="collapse <?= $bOpenBudget ? 'in' : '' ?> j-collapse" id="j-left-budget">
    <div class="l-inside l-budget pdb10">
        <div class="form-inline">
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="pf" value="<?= ! empty($f['pf']) ? $f['pf'] : '' ?>" placeholder="<?= _t('', 'От'); ?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" name="pt" value="<?= ! empty($f['pt']) ? $f['pt'] : '' ?>" placeholder="<?= _t('', 'До'); ?>">
            </div>
            <div class="form-group">
                <select class="form-control " name="pc"><?= Site::currencyOptions($f['pc']) ?></select>
            </div>
            <div class="checkbox mrgt10 j-price-ex">
                <? if($bShowPriceEx && $aPriceSett['ex'] & Specializations::PRICE_EX_AGREE): ?>
                    <label>
                        <input type="checkbox" name="pex" value="<?= Specializations::PRICE_EX_AGREE ?>" <?= $f['pex'] & Specializations::PRICE_EX_AGREE ? ' checked="checked"' : ''?>>
                        <span>
                            <?= $aPriceSett['price_title_mod'] ?>
                        </span>
                    </label>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>

