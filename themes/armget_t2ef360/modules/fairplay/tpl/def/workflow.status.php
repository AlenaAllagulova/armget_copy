<?php
/** @var $this Fairplay */
$userID = User::id();
$isClient = $userID == $client_id;
$isWorker = $userID == $worker_id;
$opinions = Opinions::aTypes();
$usePrePay = false;
if (Catcher::isUseOrderPrepay()){
    if(isset($order['prepay']) && !empty($order['prepay']) || (isset($prepay) && !empty($prepay))){
        $usePrePay = true;
        $price = (isset($order['prepay']) && !empty($order['prepay']) ? $order['prepay'] : $prepay);
    }
}
?>
<? if( ! empty($status) && ($isClient || $isWorker)):?>
    <div class="l-project-bs-content j-status" data-id="<?= $id ?>">
    <? switch($status){
        case Fairplay::STATUS_RESERVATION: ?>
            <div class="l-project-bs-content-icon">
                <i class="fa fa-money"></i>
            </div>
            <div class="l-project-bs-content-in">
                <div class="l-project-bs-content-title">
                    <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; резервирование суммы'); ?>
                </div>
                <? if($isClient): $verified = Fairplay::financeVerified($userID);
                        $commission = $this->commission($price, $t);
                        $sum = $price + $commission; ?>
                    <div class="l-project-bs-content-descr">
                        <?= _t('fp', 'Исполнитель подтвердил заказ и готов его выполнить. 
                                      Пожалуйста, зарезервируйте сумму [price_title] (+[percent] комиссии) на сайте &mdash; 
                                      после этого начнется выполнение работы по заказу. [text_prepay]',
                                    [
                                        'percent'     => $t,
                                        'price_title' => $usePrePay ? 'предоплаты' : 'оплаты',
                                        'text_prepay' => $usePrePay ? '<br>Назначенная Вами предоплата '. $price.' '. Site::currencyDefault(): '',
                                    ]
                        ); ?>
                    </div>
                    <ul class="l-project-bs-content-controls">
                        <? if($verified == Fairplay::VERIFIED_STATUS_APPROVED): $noWarn = true; ?>
                            <li><a href="<?= $url_view ?>#chat" class="btn btn-success j-fairplay-chat"><?= _t('fp', 'Обсудить условия'); ?></a></li>
                            <li><button class="btn btn-success j-fairplay-reserved" data-id="<?= $id ?>"><?= _t('fp', 'Зарезервировать [sum]', array('sum' => tpl::formatPrice($sum).' '.Site::currencyDefault('title_short'))); ?></button></li>
                        <? else: if($verified == Fairplay::VERIFIED_STATUS_WAIT): ?>
                            <li><button type="button" class="btn btn-success" disabled="disabled"><?= _t('fp', 'Проверка данных модератором'); ?></button></li>
                        <? else: ?>
                            <li><a href="<?= Users::url('my.settings', array('tab' => 'finance')) ?>" class="btn btn-success"><?= _t('fp', 'Перейти на страницу Финансы'); ?></a></li>
                        <? endif; endif; ?>
                        <li><?= _t('', 'или'); ?> <a href="#" class="j-fairplay-cancel" data-id="<?= $id ?>"><?= _t('fp', 'отменить заказ'); ?></a></li>
                    </ul>
                    <? if(empty($noWarn)): ?>
                    <div class="l-project-bs-content-help">
                        <i class="fa fa-warning text-warning"></i> <?= _t('fp', 'Обратите внимание: перед резервированием вам необходимо заполнить данные на странице "Финансы".'); ?>
                    </div>
                    <? endif; ?>
                <? else: ?>
                    <div class="l-project-bs-content-descr">
                        <?= _t('fp', 'Вы подтвердили заказ и готовы его выполнить. Далее вам необходимо дождаться, пока заказчик зарезервирует на сайте сумму оплаты и только после этого начинать выполнение работы по заказу.'); ?>
                    </div>
                    <ul class="l-project-bs-content-controls">
                        <li><a href="<?= $url_view ?>#chat" class="btn btn-success j-fairplay-chat"><?= _t('fp', 'Обсудить заказ'); ?></a></li>
                    </ul>
                    <div class="l-project-bs-content-help">
                        <i class="fa fa-warning text-warning"></i> <?= _t('fp', 'Пожалуйста, не начинайте выполнение работы, пока заказчик не зарезервирует сумму оплаты. Вы получите уведомление, как только сумма будет перечислена на сайт.'); ?>
                    </div>
                <? endif; ?>
            </div>
        <? break;
        case Fairplay::STATUS_EXECUTION:
            if($fairplay && ! empty($extra['complete'])): ?>
                <div class="l-project-bs-content-icon">
                    <i class="fa fa-check"></i>
                </div>
                <div class="l-project-bs-content-in">
                    <div class="l-project-bs-content-title">
                        <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; работа выполнена'); ?>
                    </div>
                    <div class="l-project-bs-content-descr">
                        <? if($isClient): ?>
                            <?= _t('fp', 'Исполнитель закончил выполнение работы по заказу. Пожалуйста, примите работу и завершите сотрудничество (с выплатой зарезервированной суммы исполнителю) или верните заказ в работу, если он выполнен не полностью.'); ?>
                        <? else: ?>
                            <?= _t('fp', 'Вы уведомили заказчика о завершении работы по заказу. Теперь вам необходимо дождаться реакции заказчика. Если результат его устраивает, он завершит заказ и вам будут переведены деньги. В противном случае он вернет заказ на доработку.'); ?>
                        <? endif; ?>
                    </div>
                    <ul class="l-project-bs-content-controls">
                        <li><a href="<?= $url_view ?>#chat" class="btn btn-success j-fairplay-chat"><?= _t('fp', 'Обсудить заказ'); ?></a></li>
                        <? if($isClient): ?>
                            <li><button class="btn btn-success j-fairplay-close" data-id="<?= $id ?>"><?= _t('fp', 'Завершить сотрудничество'); ?></button></li>
                            <li><button class="btn btn-danger j-fairplay-continue" data-id="<?= $id ?>"><?= _t('fp', 'вернуть заказ в работу'); ?></button></li>
                            <? if(! $arbitrage): ?>
                                <li><?= _t('', 'или'); ?> <a href="#" class="ajax-link j-arbitrage" data-id="<?= $id ?>"><span><?= _t('fp', 'обратиться в Арбитраж'); ?></span></a></li>
                            <? endif;
                        endif; ?>
                    </ul>
                </div>
            <? break; endif; ?>
            <div class="l-project-bs-content-icon">
                <i class="fa fa-wrench"></i>
            </div>
            <div class="l-project-bs-content-in">
                <div class="l-project-bs-content-title">
                    <? if($fairplay): ?>
                        <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; выполнение работы'); ?>
                    <? else: ?>
                        <?= _t('fp', 'Заказ с прямой оплатой &mdash; выполнение работы'); ?>
                    <? endif; ?>
                </div>
                <div class="l-project-bs-content-descr">
                    <? if($isClient): ?>
                        <?= _t('fp', 'Исполнитель выполняет работу по заказу. В процессе сотрудничества вы можете продолжить обсуждение заказа, задания в нем и полученных от исполнителя результатов. Как только работа будет завершена и принята вами, пожалуйста, завершите заказ и оставьте отзыв о сотрудничестве.'); ?>
                    <? else: ?>
                        <? if($fairplay): ?>
                            <?= _t('fp', 'Сумма зарезервирована. Заказчик ожидает выполнения работы по заказу. В процессе сотрудничества вы можете продолжить обсуждения заказа, задания в нем и передаваемых вами результатов. Как только работа будет завершена, пожалуйста, сообщите об этом заказчику, чтобы он мог завершить заказ с перечислением вам зарезервированной суммы оплаты.'); ?>
                        <? else: ?>
                            <?= _t('fp', 'Вы принялись за выполнение заказа. Заказчик ожидает выполнения работы. В процессе сотрудничества вы можете продолжить обсуждения заказа. Как только работа будет завершена, пожалуйста, сообщите об этом заказчику, чтобы он мог завершить заказ и оставить отзыв.'); ?>
                        <? endif; ?>
                    <? endif; ?>
                </div>
                <ul class="l-project-bs-content-controls">
                    <li><a href="<?= $url_view ?>#chat" class="btn btn-success j-fairplay-chat"><?= _t('fp', 'Обсудить заказ'); ?></a></li>
                    <? if($isClient): ?>
                        <li><button class="btn btn-success j-fairplay-close" data-id="<?= $id ?>"><?= _t('fp', 'Завершить сотрудничество'); ?></button></li>
                        <? if( ! $fairplay): ?><li><?= _t('', 'или'); ?> <a href="#" class="ajax-link j-fairplay-change" data-id="<?= $id ?>"><span><?= _t('fp', 'изменить срок, сумму'); ?></span></a></li><? endif; ?>
                    <? else: ?>
                        <? if($fairplay && empty($extra['complete'])): ?>
                            <li><button class="btn btn-success j-fairplay-complete" data-id="<?= $id ?>"><?= _t('fp', 'Уведомить о выполнении работы'); ?></button></li>
                        <? endif; ?>
                    <? endif; ?>
                    <? if($fairplay && ! $arbitrage): ?>
                        <li><?= _t('', 'или'); ?> <a href="#" class="ajax-link j-arbitrage" data-id="<?= $id ?>"><span><?= _t('fp', 'обратиться в Арбитраж'); ?></span></a></li>
                    <? endif; ?>
                </ul>
                <? if($fairplay && ! $arbitrage): ?>
                    <div class="l-project-bs-content-help">
                    <? if($isClient): ?>
                        <i class="fa fa-warning text-warning"></i> <?= _t('fp', 'Если в процессе сотрудничества у вас позникнут проблемы с исполнителем, рекомендуем обратиться в арбитраж и урегулировать конфликт с помощью арбитра.'); ?>
                    <? else: ?>
                        <i class="fa fa-warning text-warning"></i> <?= _t('fp', 'Если в процессе сотрудничества у вас позникнут проблемы с заказчиком, рекомендуем обратиться в арбитраж и урегулировать конфликт с помощью арбитра.'); ?>
                    <? endif; ?>
                    </div>
                <? endif; ?>
            </div>
            <? break;
        case Fairplay::STATUS_REWORK:
            if( ! empty($extra['complete'])): ?>
                <div class="l-project-bs-content-icon">
                    <i class="fa fa-check"></i>
                </div>
                <div class="l-project-bs-content-in">
                    <div class="l-project-bs-content-title">
                        <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; работа выполнена'); ?>
                    </div>
                    <div class="l-project-bs-content-descr">
                        <? if($isClient): ?>
                            <?= _t('fp', 'Исполнитель закончил выполнение работы по заказу. Пожалуйста, примите работу и завершите сотрудничество (с выплатой зарезервированной суммы исполнителю) или верните заказ в работу, если он выполнен не полностью.'); ?>
                        <? else: ?>
                            <?= _t('fp', 'Вы уведомили заказчика о завершении работы по заказу. Теперь вам необходимо дождаться реакции заказчика. Если результат его устраивает, он завершит заказ и вам будут переведены деньги. В противном случае он вернет заказ на доработку.'); ?>
                        <? endif; ?>
                    </div>
                    <ul class="l-project-bs-content-controls">
                        <li><a href="<?= $url_view ?>#chat" class="btn btn-success j-fairplay-chat"><?= _t('fp', 'Обсудить заказ'); ?></a></li>
                        <? if($isClient): ?>
                            <li><button class="btn btn-success j-fairplay-close" data-id="<?= $id ?>"><?= _t('fp', 'Завершить сотрудничество'); ?></button></li>
                            <li><button class="btn btn-danger j-fairplay-continue" data-id="<?= $id ?>"><?= _t('fp', 'вернуть заказ в работу'); ?></button></li>
                            <? if(! $arbitrage): ?>
                                <li><?= _t('', 'или'); ?> <a href="#" class="ajax-link j-arbitrage" data-id="<?= $id ?>"><span><?= _t('fp', 'обратиться в Арбитраж'); ?></span></a></li>
                            <? endif;
                        endif; ?>
                    </ul>
                </div>
            <? break; endif; ?>
                <div class="l-project-bs-content-icon">
                    <i class="fa fa-repeat"></i>
                </div>
                <div class="l-project-bs-content-in">
                    <div class="l-project-bs-content-title">
                        <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; доработка'); ?>
                    </div>
                    <div class="l-project-bs-content-descr">
                        <? if($isClient): ?>
                            <?= _t('fp', 'Заказ возвращен в работу, вы можете продолжить сотрудничество с исполнителем. Как только работа будет полностью завершена и принята вами, не забудьте завершить заказ и оставить отзыв о сотрудничестве'); ?>
                        <? else: ?>
                            <?= _t('fp', 'Заказчик вернул заказ в работу. Пожалуйста, обсудите все необходимые правки и уведомите его об этом.'); ?>
                        <? endif; ?>
                    </div>
                    <ul class="l-project-bs-content-controls">
                        <li><a href="<?= $url_view ?>#chat" class="btn btn-success j-fairplay-chat"><?= _t('fp', 'Обсудить заказ'); ?></a></li>
                        <? if($isClient): ?>
                            <li><button class="btn btn-success j-fairplay-close" data-id="<?= $id ?>"><?= _t('fp', 'Завершить сотрудничество'); ?></button></li>
                        <? else: ?>
                            <li><button class="btn btn-success j-fairplay-complete" data-id="<?= $id ?>"><?= _t('fp', 'Уведомить о выполнении работы'); ?></button></li>
                        <? endif; ?>
                        <? if( ! $arbitrage): ?>
                            <li><?= _t('', 'или'); ?> <a href="#" class="ajax-link j-arbitrage" data-id="<?= $id ?>"><span><?= _t('fp', 'обратиться в Арбитраж'); ?></span></a></li>
                        <? endif; ?>
                    </ul>
                    <? if( ! $arbitrage): ?>
                        <div class="l-project-bs-content-help">
                            <? if($isClient): ?>
                                <i class="fa fa-warning text-warning"></i> <?= _t('fp', 'Если в процессе сотрудничества у вас позникнут проблемы с исполнителем, рекомендуем обратиться в арбитраж и урегулировать конфликт с помощью арбитра.'); ?>
                            <? else: ?>
                                <i class="fa fa-warning text-warning"></i> <?= _t('fp', 'Если в процессе сотрудничества у вас позникнут проблемы с заказчиком, рекомендуем обратиться в арбитраж и урегулировать конфликт с помощью арбитра.'); ?>
                            <? endif; ?>
                        </div>
                    <? endif; ?>
                </div>
            <? break;
        case Fairplay::STATUS_PAYMENT:
            if($isWorker):
                $verified = Fairplay::financeVerified($userID);
                $client = Users::model()->userData($client_id, array('user_id', 'login', 'name', 'surname', 'email', 'blocked'));
                ?>
                    <div class="l-project-bs-content-icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="l-project-bs-content-in">
                        <div class="l-project-bs-content-title">
                            <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; выплата сумм'); ?>
                        </div>
                        <div class="l-project-bs-content-descr">
                            <?= _t('fp', 'Заказчик [user] принял и подтвердил результат выполненной по заказу работы.', array(
                                'user' => tpl::userLink($client, 'no-login'),
                            )); ?><br>
                            <?= _t('fp', 'К выплате [sum] &mdash; <strong>ожидание выплаты суммы.</strong>', array('sum' => tpl::formatPrice($price).' '.Site::currencyDefault('title_short'))); ?><br />
                            <?= _t('fp', 'Заказчик оставил [opinion] отзыв.', array(
                                'opinion' => '<strong class="'.$opinions[ $client_opinion_type ]['ct'].'">'.$opinions[ $client_opinion_type ]['tl'].'</strong>',
                            )); ?>
                            <? if($worker_opinion): ?>
                                <?= _t('fp', 'Вы оставили [opinion] отзыв.', array(
                                    'opinion' => '<strong class="'.$opinions[ $worker_opinion_type ]['ct'].'">'.$opinions[ $worker_opinion_type ]['tl'].'</strong>',
                                )); ?>
                            <? endif; ?>
                        </div>
                        <ul class="l-project-bs-content-controls">
                            <? if($verified == Fairplay::VERIFIED_STATUS_APPROVED): $noWarn = true; ?>
                                <? if($worker_opinion): ?>
                                    <li><a href="#" class="ajax-link j-opinion-view" data-id="<?= $worker_opinion ?>" data-wf="<?= $id ?>"><span><?= _t('fp', 'Ваш отзыв'); ?></span></a></li>
                                <? else: ?>
                                    <li><a href="#" class="btn btn-success j-fairplay-opinion-add" data-id="<?= $id ?>"><?= _t('fp', 'Оставить отзыв'); ?></a></li>
                                <? endif; ?>
                            <? else: if($verified == Fairplay::VERIFIED_STATUS_WAIT): ?>
                                <li><button type="button" class="btn btn-success" disabled="disabled"><?= _t('fp', 'Проверка данных модератором'); ?></button></li>
                            <? else: ?>
                                <li><a href="<?= Users::url('my.settings', array('tab' => 'finance')) ?>" class="btn btn-success"><?= _t('fp', 'Перейти на страницу Финансы'); ?></a></li>
                            <? endif; endif; ?>
                            <li><a href="#" class="ajax-link j-opinion-view" data-id="<?= $client_opinion ?>" data-wf="<?= $id ?>"><span><?= _t('fp', 'Отзыв заказчика'); ?></span></a></li>
                        </ul>
                        <? if(empty($noWarn)): ?>
                            <div class="l-project-bs-content-help">
                                <i class="fa fa-warning text-warning"></i> <?= _t('fp', 'Обратите внимание: перед выплатой сумм Вам необходимо заполнить данные на странице "Финансы".'); ?>
                            </div>
                        <? endif; ?>
                    </div>
                <? break;
            endif;
        case Fairplay::STATUS_CLOSED: ?>
            <div class="l-project-bs-content-icon">
                <i class="fa fa-check"></i>
            </div>
            <div class="l-project-bs-content-in">
                <div class="l-project-bs-content-title">
                    <? if($fairplay): ?>
                        <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; работа выполнена'); ?>
                    <? else: ?>
                        <?= _t('fp', 'Заказ с прямой оплатой &mdash; работа выполнена'); ?>
                    <? endif; ?>
                </div>

                <div class="l-project-bs-content-descr">
                    <? if($isClient): ?>
                        <? if($fairplay): if($status == Fairplay::STATUS_PAYMENT): ?>
                                <p><?= _t('fp', 'Вы приняли и подтвердили результат выполнения работы по заказу. Исполнителю будет перечислена зарезервировання сумма оплаты.'); ?></p>
                                <?= _t('fp', 'К выплате исполнителю [sum] &mdash; <strong>Выплата суммы</strong>', array('sum' => tpl::formatPrice($price).' '.Site::currencyDefault('title_short'))); ?><br />
                                <?= _t('fp', 'Процесс выполнения заказа завершен, спасибо за сотрудничество!'); ?><br /><br />
                            <? else: ?>
                                <p><?= _t('fp', 'Вы приняли и подтвердили результат выполнения работы по заказу. Исполнителю перечислена зарезервировання сумма оплаты.'); ?></p>
                            <? endif; ?>
                        <? endif; ?>
                        <? if($worker_opinion): ?>
                            <?= _t('fp', 'Вы оставили [opinion1] отзыв. Исполнитель оставил [opinion2] отзыв.', array(
                                'opinion1' => '<strong class="'.$opinions[ $client_opinion_type ]['ct'].'">'.$opinions[ $client_opinion_type ]['tl'].'</strong>',
                                'opinion2' => '<strong class="'.$opinions[ $worker_opinion_type ]['ct'].'">'.$opinions[ $worker_opinion_type ]['tl'].'</strong>',
                            )); ?>
                        <? else: ?>
                            <?= _t('fp', 'Вы оставили [opinion] отзыв. Ожидаем отзыв исполнителя.', array(
                                'opinion' => '<strong class="'.$opinions[ $client_opinion_type ]['ct'].'">'.$opinions[ $client_opinion_type ]['tl'].'</strong>',
                            )); ?>
                        <? endif; ?>
                    <? else: ?>
                        <?= _t('fp', 'Заказчик подтвердил выполнение вами работы и оставил [opinion] отзыв.', array(
                            'opinion' => '<strong class="'.$opinions[ $client_opinion_type ]['ct'].'">'.$opinions[ $client_opinion_type ]['tl'].'</strong>',
                        )); ?>
                        <? if($worker_opinion): ?>
                            <?= _t('fp', 'Вы оставили [opinion] отзыв.', array(
                                'opinion' => '<strong class="'.$opinions[ $worker_opinion_type ]['ct'].'">'.$opinions[ $worker_opinion_type ]['tl'].'</strong>',
                            )); ?>
                        <? else: ?>
                            <?= _t('fp', 'Оставьте свой отзыв. '); ?>
                        <? endif; ?>
                    <? endif; ?>
                </div>
                <ul class="l-project-bs-content-controls">
                    <? if($userID == $client_id): ?>
                        <li><a href="#" class="ajax-link j-opinion-view" data-id="<?= $client_opinion ?>" data-wf="<?= $id ?>"><span><?= _t('fp', 'Ваш отзыв'); ?></span></a></li>
                        <? if($worker_opinion): ?><li><a href="#" class="ajax-link j-opinion-view" data-id="<?= $worker_opinion ?>" data-wf="<?= $id ?>"><span><?= _t('fp', 'Отзыв исполнителя'); ?></span></a></li><? endif; ?>
                    <? else: ?>
                        <? if($worker_opinion): ?>
                            <li><a href="#" class="ajax-link j-opinion-view" data-id="<?= $worker_opinion ?>" data-wf="<?= $id ?>"><span><?= _t('fp', 'Ваш отзыв'); ?></span></a></li>
                        <? else: ?>
                            <li><a href="#" class="btn btn-success j-fairplay-opinion-add" data-id="<?= $id ?>"><?= _t('fp', 'Оставить отзыв'); ?></a></li>
                        <? endif; ?>
                        <li><a href="#" class="ajax-link j-opinion-view" data-id="<?= $client_opinion ?>" data-wf="<?= $id ?>"><span><?= _t('fp', 'Отзыв заказчика'); ?></span></a></li>
                    <? endif; ?>
                </ul>
            </div>
        <? break;
        case Fairplay::STATUS_CANCEL: ?>
            <div class="l-project-bs-content-icon">
                <i class="fa fa-close"></i>
            </div>
            <div class="l-project-bs-content-in">
                <div class="l-project-bs-content-title">
                    <?= _t('fp', 'Заказ с оплатой через Безопасную сделку &mdash; заказ отменен'); ?>
                </div>
                <div class="l-project-bs-content-descr">
                    <? if($userID == $client_id): ?>
                        <?= _t('fp', 'Вы отменили заказ.'); ?>
                    <? else: ?>
                        <?= _t('fp', 'К сожалению, заказчик отменил свой заказ.'); ?>
                    <? endif; ?>
                </div>
            </div>
        <? break;
    } ?>
    </div>
<? endif;
static $inited;
if(empty($inited) && ! Request::isAJAX()):
    $inited = 1;
    tpl::includeJS('fairplay.status', false, 1); ?>
    <script type="text/javascript">
        <? js::start() ?>
        jFairplayStauses.init(<?= func::php2js(array(
            'lang'  => array(
                'left'        => _t('users','[symbols] осталось'),
                'symbols'     => explode(';', _t('users', 'знак;знака;знаков')),
                'check_type'  => _t('opinions', 'Укажите тип отзыва'),
                'cancel_confirm' => _t('fp', 'Вы действительно желаете отменить заказ?'),
                'success' => _t('', 'Операция выполнена успешно'),
            )
        )) ?>);
        <? js::stop() ?>
    </script>
<? endif;
