<?php
    /**
     * Кабинет пользователя: Сообщения / Переписка
     * @var $this InternalMail
     * @var $i array
     */
    tpl::includeJS('internalmail.my', false, 2);

    $url_back = HTML::escape($url_back);
    $lng_move_f = _t('internalmail', 'Поместить в папку');

    $inviteEnabled = false;
    if (Orders::invitesEnabled()) {
        do {
            if ( ! User::id() ) break;
            if (Users::useClient() && ! User::isClient()) break;
            $inviteEnabled = true;
        } while(false);
    }

?>
    <div class="container">

        <section class="l-mainContent">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="p-profileContent">
                        <div class="i-topTitle">
                            <div class="i-imailDialog-title" id="j-my-chat-folders">
                                <div class="i-goback">
                                    <a href="<?= InternalMail::url('my.messages') ?>"><i class="fa fa-angle-left c-link-icon"></i><span><?= _t('internalmail', 'Назад к сообщениям'); ?></span></a>
                                </div>
                                <div class="i-dialog-img">
                                    <a href="<?= Users::url('profile', array('login' => $i['login'])); ?>"><img src="<?= $i['avatar'] ?>" alt="<?= tpl::avatarAlt($i); ?>" /><?= tpl::userOnline($i) ?></a>
                                </div>
                                <div><?= tpl::userLink($i, 'strong') ?></div>
                                <? if( InternalMail::FOLDERS_ENABLED ):
                                    $title = $lng_move_f;
                                    if( ! empty($i['folders'])){
                                        $title = array(); foreach($i['folders'] as $vv) { $title[] = $folders[ $vv ]['title']; }
                                        $title = join(', ', $title);
                                    } ?>
                                <span class="dropdown">
                                    <a href="#" id="dLabel" class="dropdown-toggle ajax-link" data-toggle="dropdown"><span class="j-f-title"><?= $title ?></span> <i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                                        <? foreach($folders as $kk => $vv): if( ! $kk) continue; ?>
                                            <li class="<?= ! empty($vv['class']) ? $vv['class'] : '' ?><?= in_array($kk, $i['folders']) ? ' active' : '' ?>"><a href="#" data-user-id="<?= $i['user_id'] ?>" data-folder-id="<?= $kk ?>" class="j-f-action"><?= $vv['title'] ?></a></li>
                                        <? endforeach; ?>
                                    </ul>
                                </span>
                                <? endif; ?>
                            </div>
                        </div>

                        <div class="i-imailDialog-window" id="j-my-chat-list">
                            <?= $list ?>
                        </div>

                        <? # Форма отправки сообщения ?>
                        <? if( $i['blocked'] ): ?>
                            <div class="alert alert-error text-center">
                                <?= _t('internalmail', 'Аккаунт пользователя заблокирован') ?>
                            </div>
                        <? else:
                            if ( $i['ignoring'] ): ?>
                            <div class="alert alert-error text-center">
                                <?= _t('internalmail', 'Пользователь запретил отправлять ему сообщения') ?>
                            </div>
                        <? else: ?>
                            <form class="i-imailDialog-form form" method="POST" action="<?= InternalMail::url('my.messages', array('i'=>$i['login'])) ?>" id="j-my-chat-form" enctype="multipart/form-data">
                                <input type="hidden" name="act" value="send" />
                                <input type="hidden" name="i" value="<?= $i['login'] ?>" />

                                <div class="form-group">
                                    <textarea name="message" rows="4" class="form-control" placeholder="<?= _t('internalmail', 'Сообщение') ?>"></textarea>
                                </div>

                                <button class="btn btn-sm btn-primary"><?= _t('internalmail', 'Отправить'); ?></button>
                                <? if(InternalMail::attachmentsEnabled()): ?>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="<?= $this->attach()->getMaxSize() ?>" />
                                    <input type="file" name="attach" class="j-upload-file hidden" />

                                    <button class="btn btn-sm btn-default pull-right j-upload-file-btn"><?= _t('internalmail', 'Прикрепить файл') ?></button>
                                    <div class="pull-right j-file-name hidden">
                                        <span class="j-name"></span> <a href="#" class="link-delete j-upload-delete"><i class="fa fa-times"></i></a>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="help-block text-right mrgb0"><?= _t('internalmail', 'Максимальный размер файла - [maxSize]', array('maxSize'=>tpl::filesize($this->attach()->getMaxSize()) )) ?></div>
                                <? endif; ?>
                                <? if($inviteEnabled): ?>
                                    <a href="<?= Orders::invitesEnabledFullForm() ? Orders::url('add', ['worker_invite' => $i['user_id']]) : Users::url('profile',['login' => $i['login'], 'invite' => true]) ?>" class="btn btn-sm btn-default">
                                        <i class="fa fa-clipboard"></i>
                                        <?= _t('orders', 'Предложить заказ'); ?>
                                    </a>
                                <? endif; ?>
                            </form>

                            <? endif; endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<script type="text/javascript">
<? js::start() ?>
    $(function(){
        jMyChat.init(<?= func::php2js(array(
            'lang' => array(
                'message' => _t('internalmail','Сообщение слишком короткое'),
                'success' => _t('internalmail','Сообщение было успешно отправлено'),
                'move_folder' => $lng_move_f,
            ),
            'ajax' => true,
        )) ?>);
    });
<? js::stop() ?>
</script>