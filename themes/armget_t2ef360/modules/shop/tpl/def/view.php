<?php
if( ! empty($regions)){
    $aFirstRegions = reset($regions);
}
?>
<div class="container">

    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

    <section class="l-mainContent">
        <div class="row">

            <!-- Project-->
            <div class="col-md-9">

                <div class="l-borderedBlock">

                    <div class="l-inside">

                        <h1 class="small"><?= $title ?></h1>

                        <div>
                            <?= Shop::priceBlock($aData, $cat, false) ?>
                            <? if( ! empty($tags)): foreach($tags as $v):?>
                                <a href="<?= Shop::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                            <? endforeach; endif; ?>
                        </div>

                        <article class="sh-shop-item-text">
                            <p><?= nl2br($descr); ?></p>
                        </article>

                    </div>

                    <? if($imgcnt): ?>
                    <div class="l-inside">
                        <div class="sh-item-gallery">
                            <? foreach($images as $k => $v): ?>
                            <a href="<?= $v['i'][ ShopProductImages::szView ]; ?>" class="fancyzoom" rel="fancy-gallery-<?= $id ?>">
                                <img src="<?= $v['i'][ ShopProductImages::szThumbnail ]; ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" />
                            </a>
                            <? endforeach ?>
                        </div>
                    </div>
                    <script type="text/javascript">
                        <?
                            tpl::includeJS('fancybox2', true);
                            js::start(); ?>
                            $(function(){
                                $('.fancyzoom').fancybox({
                                    openEffect	: 'none',
                                    closeEffect	: 'none',
                                    nextEffect  : 'fade',
                                    prevEffect : 'fade',
                                    fitToView: false,
                                    helpers: {
                                        overlay: {locked: false}
                                    }
                                });

                            });
                        <? js::stop(); ?>
                    </script>
                    <? endif; ?>

                    <? if( ! empty($dynprops)): ?><div class="l-inside l-dymanic-features"><?= $dynprops ?></div><? endif; ?>

                    <div class="l-inside o-clientInfo">

                        <div class="row">
                            <div class="col-sm-7">
                                <a href="<?= Users::url('profile', array('login' => $login)) ?>" class="o-client-avatar">
                                    <img src="<?= $avatar_small ?>" alt="<?= tpl::avatarAlt($aData); ?>" />
                                    <?= tpl::userOnline($aData) ?>
                                </a>
                                <div class="o-client-about">
                                    <div>
                                        <?= tpl::userLink($aData, 'no-login'); ?>
                                    </div>
                                    <div class="visible-lg visible-md visible-sm">
                                        <?= _t('shop', 'Зарегистрирован на сайте') ?> <?= tpl::date_format_spent($user_created, false, false)?>
                                    </div>
                                    <div class="o-feedbacks-inf">
                                        <?= _t('shop', 'Отзывы исполнителей:') ?> <?= tpl::opinions($opinions_cache, array('login' => $login)) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-5 o-client-more-works">
                                <a href="<?= Shop::url('user.listing', array('login' => $login)) ?>" class="sh-showAllWorks"><span><?= _t('shop', 'Другие товары продавца'); ?></span><i class="fa fa-angle-right c-link-icon"></i></a>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <? if( ! empty($contacts)): $aTypes = Users::aContactsTypes(); ?>
                            <? if(Catcher::isViewContactsWithPrepay()): ?>
                                <? $isOwner = User::isCurrent($user_id);
                                $isShowContacts = Catcher::i()->isShowContacts(User::id(), $user_id);?>
                                <? if( User::id() && ($isOwner || $isShowContacts)): ?>
                                    <div class="o-collapsed-info sh-item-contacts">
                                        <button type="button" class="o-collapsed-info_toggle btn btn-default" data-toggle="collapse" data-target="#o-contact-info-1"><i class="fa fa-chevron-down pull-right"></i><?= _t('users', 'Контакты'); ?></button>
                                        <div class="collapse" id="o-contact-info-1">
                                            <div class="o-collapsed-inner">
                                                <h6><?= _t('users', 'Контакты'); ?></h6>
                                                <!--noindex-->
                                                <table class="table table-condensed">
                                                    <tbody>
                                                    <? foreach($contacts as $v): ?>
                                                        <tr>
                                                            <td><?= $aTypes[ $v['t'] ]['t'] ?>:</td>
                                                            <td><?= tpl::linkContact($v, true); ?></td>
                                                        </tr>
                                                    <? endforeach; ?>
                                                    </tbody>
                                                </table>
                                                <!--/noindex-->
                                            </div>
                                        </div>
                                    </div>
                                <? endif;?>
                            <? else: # стандартное поведение в проекте?>
                                <? if(Users::isContactsView($user_id, $type, $pro, $contactsHideReason, 'mrgb0')): ?>
                                    <div class="o-collapsed-info sh-item-contacts">
                                        <button type="button" class="o-collapsed-info_toggle btn btn-default" data-toggle="collapse" data-target="#o-contact-info-1"><i class="fa fa-chevron-down pull-right"></i><?= _t('users', 'Контакты'); ?></button>
                                        <div class="collapse" id="o-contact-info-1">
                                            <div class="o-collapsed-inner">
                                            <h6><?= _t('users', 'Контакты'); ?></h6>
                                            <!--noindex-->
                                            <table class="table table-condensed">
                                                <tbody>
                                                    <? foreach($contacts as $v): ?>
                                                        <tr>
                                                            <td><?= $aTypes[ $v['t'] ]['t'] ?>:</td>
                                                            <td><?= tpl::linkContact($v, true); ?></td>
                                                        </tr>
                                                    <? endforeach; ?>
                                                </tbody>
                                            </table>
                                            <!--/noindex-->
                                            </div>
                                        </div>
                                    </div>
                                <? else: ?>
                                    <div class="o-collapsed-info sh-item-contacts">
                                        <button type="button" class="o-collapsed-info_toggle btn btn-default" data-toggle="collapse" data-target="#o-contact-info-1"><i class="fa fa-chevron-down pull-right"></i><?= _t('users', 'Контакты'); ?></button>
                                        <div class="collapse" id="o-contact-info-1">
                                            <div class="o-collapsed-inner">
                                                <h6><?= _t('users', 'Контакты'); ?></h6>
                                                <?= $contactsHideReason ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            <? endif; ?>
                        <? endif; ?>

                    </div>

                    <?= User::isCurrent($user_id) ? $this->viewPHP($aData, 'owner.view.controls') : '' ?>

                    <div class="l-inside">
                        <ul class="l-item-features">
                            <? if( ! empty($aFirstRegions['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $aFirstRegions['title']?></li><? endif; ?>
                            <li><span><i class="fa fa-eye"></i> <?= _t('', 'Просмотров: [total]', array('total'=>$views_total)); ?></span></li>
                        </ul>
                    </div>

                </div>

            </div>

            <div class="col-md-3 visible-lg visible-md">
                <? # Баннер: Магазин: просмотр ?>
                <?= Banners::view('shop_view', array('pos'=>'right', 'cat'=>$cat_id)) ?>
            </div>

        </div>
    </section>

</div>