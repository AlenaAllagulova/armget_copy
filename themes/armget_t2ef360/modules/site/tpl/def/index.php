<?php
tpl::includeJS(['dist/search']);
$carousel_block = Users::i()->svc_carousel_block();
$cats_block = Catcher::i()->getIndexCatsBlock();
$sSearchLink = Users::url('list');
if( User::isWorker()){
    $sSearchLink = Orders::url('list');
}
$formSearchName = 'main_search';
View::setLayout('general');

$userID = User::id();
$user = Users::model()->userData($userID, array('type'));
$oTheme = bff::theme();
?>
<div class="index-bg" style="background: url(<?= bff::url('/img/index-bg.png')?>)">
    <div class="container index-bg__col">
        <div class="text-center index-bg__top">
            <div class="">
                <h1 class="index-bg__h1">
                    <?= _t('','Бесплатный поиск надежных исполнителей <br> для любых IT-задач *')?>
                </h1>
                <h2 class="index-bg__h2">
                    <?= _t('','Поиск заказчиков для проверенных вебмастеров')?>
                </h2>
            </div>
        </div>
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <form role="search" method="get" class="g-search" autocomplete="off" action="<?= $sSearchLink;?>" >
                        <div class="g-search__box">
                            <input type="search"
                                   name="q"
                                   class="form-control"
                                   id="<?=$formSearchName?>"
                                   placeholder="<?= _t('users', 'Опишите задачу'); ?>"
                                   oninput="Search.getSuggests('<?=$formSearchName?>')"
                            >
                            <button class="btn btn-primary ">
                                <?=_t('', ' Найти услуги')?>
                            </button>
                        </div>
                        <ul id="suggest_list" class="suggest-list">
                        </ul>
                    </form>
                    <div class="index-bg__text mrgt10 text-center">
                        <?= _t('','Например: Фотомонтаж и обработка')?>
                    </div>
                </div>

            </div>
        <div class="row index-bg__bottom hidden">
            <div class=" text-center col-sm-6 pdb10">
                <a href="<?= Catcher::getIndexBtnUrl(Users::TYPE_WORKER)?
                    Catcher::getIndexBtnUrl(Users::TYPE_WORKER):
                    Users::url('list') ?>"
                   class="btn btn-primary index-bg__btn">
                    <?=_t('index-btn-client', 'Я заказчик')?>
                </a>
                <div class="index-bg__text mrgt20 ">
                    <?= _t('','Добавь задание или выбери нужного специалиста <br> в каталоге исполнителей')?>
                </div>
            </div>
            <div class=" text-center col-sm-6 pdb10">
                <a href="<?= Catcher::getIndexBtnUrl(Users::TYPE_WORKER)?
                    Catcher::getIndexBtnUrl(Users::TYPE_WORKER):
                    Orders::url('list') ?>"
                   class="btn btn-primary index-bg__btn">
                    <?=_t('index-btn-worker', 'Я исполнитель')?>
                </a>
                <div class="index-bg__text mrgt20">
                    <?= _t('','Зарегистрируйся и получай самый большой поток <br> заявок от заказчиков')?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $cats_block; ?>

<div class="type-work-container">
    <div class="container ">
        <div class="row pdb20 flex flex_center flex_wrap">
            <div class="col-sm-6 ">
                <p class="type-work__article ">
                    <?= $oTheme->config('block-1_title')?>
                </p>
                <div class="text-center visible-xs ">
                    <img src="<?= bff::url('/img/workAtHome.svg')?>" alt="" class=" mrgb10" style="width: 50%;">
                </div>
                <div class="type-work__text ">
                    <?= $oTheme->config('block-1_descr')?>
                </div>
                <? if ($user['type'] == Users::TYPE_CLIENT || !$userID): ?>
                    <a href="<?= Orders::url('add')?>" class="btn btn-primary">
                        <?= _t('','Разовый заказ')?>
                    </a>
                <? endif; ?>
            </div>
            <div class="col-sm-6 mrgt10 text-center  hidden-xs">
                <img src="<?= bff::url('/img/workAtHome.svg')?>" alt="">
            </div>
        </div>
        <hr class="visible-xs mrgt0 mrgb0">
        <div class="row pdt20 flex flex_center flex_wrap">
            <div class="col-sm-6 mrgb10 text-center  hidden-xs">
                <img src="<?= bff::url('/img/workAtOffice.svg')?>" alt="">
            </div>
            <div class="col-sm-6">
                <p class="type-work__article ">
                    <?= $oTheme->config('block-2_title')?>
                </p>
                <div class="text-center visible-xs ">
                    <img src="<?= bff::url('/img/workAtOffice.svg')?>" alt="" class=" mrgb10" style="width: 50%;">
                </div>
                <div class="type-work__text ">
                    <?= $oTheme->config('block-2_descr')?>

                </div>

                <? if ($user['type'] == Users::TYPE_CLIENT || !$userID): ?>
                    <a href="<?= Orders::url('add')?>" class="btn btn-primary j-work-at-office">
                        <?= _t('','Постоянная работа')?>
                    </a>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <p class="index-article text-center">
        <?= $oTheme->config('list_title')?>
    </p>
    <div class="row about">
        <div class="col-md-10 col-md-offset-1">
            <div class="row mrgb50  ">
                <div class="col-sm-2 about__img">
                    <div class="about__img-box">
                        <img class="about__img_first" src="<?= bff::url('/img/Group1.svg')?>" alt="">
                    </div>
                </div>
                <div class="col-sm-10">
                    <p class="about__article">
                        <?= $oTheme->config('list-item-1_title')?>
                    </p>
                    <span class="about__text">
                         <?= $oTheme->config('list-item-1_descr')?>
                    </span>
                </div>
            </div>
            <div class="row mrgb50  ">
                <div class="col-sm-2 about__img">
                    <div class="about__img-box">
                        <img class="about__img_second" src="<?= bff::url('/img/Group2.svg')?>" alt="">
                    </div>
                </div>
                <div class="col-sm-10">
                    <p class="about__article">
                        <?= $oTheme->config('list-item-2_title')?>
                    </p>
                    <span class="about__text">
                         <?= $oTheme->config('list-item-2_descr')?>
                    </span>
                </div>
            </div>
            <div class="row mrgb50  ">
                <div class="col-sm-2 about__img">
                    <div class="about__img-box">
                        <img class="about__img_thread" src="<?= bff::url('/img/Group3.svg')?>" alt="">
                    </div>
                </div>
                <div class="col-sm-10">
                    <p class="about__article">
                        <?= $oTheme->config('list-item-3_title')?>
                    </p>
                    <span class="about__text">
                        <?= $oTheme->config('list-item-3_descr')?>
                    </span>
                </div>
            </div>
            <div class="row mrgb50  ">
                <div class="col-sm-2 about__img">
                    <div class="about__img-box">
                        <img class="about__img about__img_four" src="<?= bff::url('/img/Group4.svg')?>" alt="">
                    </div>
                </div>
                <div class="col-sm-10">
                    <p class="about__article">
                        <?= $oTheme->config('list-item-4_title')?>
                    </p>
                    <span class="about__text">
                        <strong>
                            <?= $oTheme->config('list-item-4-point-1_title')?>
                        </strong>
                        <?= $oTheme->config('list-item-4-point-1_descr')?>
                        <br>
                        <br>
                        <strong>
                            <?= $oTheme->config('list-item-4-point-2_title')?>
                        </strong>
                        <?= $oTheme->config('list-item-4-point-2_descr')?>
                        <br>
                        <br>
                        <strong>
                            <?= $oTheme->config('list-item-4-point-3_title')?>
                        </strong>
                        <?= $oTheme->config('list-item-4-point-3_descr')?>
                        <br>
                        <br>
                        <strong>
                            <?= $oTheme->config('list-item-4-point-4_title')?>
                        </strong>
                        <?= $oTheme->config('list-item-4-point-4_descr')?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<? if(!empty($carousel_block)): ?>
    <div class="container">
        <p class="index-article text-center">
            <?= _t('','Лучшие исполнители на сайте')?>
        </p>
    </div>
    <div class="mrgb30"></div>
    <?= $carousel_block; ?>
<? endif; ?>

<script type="text/javascript">
    <? js::start() ?>
        $('.j-work-at-office').on('click', function (e) {
            e.preventDefault();
            localStorage.setItem('workAtOffice', 'true');
            location.href = $(this).attr('href');
        });
    <? js::stop() ?>
</script>

