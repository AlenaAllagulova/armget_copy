class MyOwnCode {
    
    UserAuth(options)
    {
        this.options = options;
        this._types = null;
        this._catsBox = null;
        this._cats = null;

        this.setTypes = (selector) => {
            this._types = $(selector);
            return this;
        };

        this.getTypes = () => {
            return this._types;
        };

        this.setCatsBox = (selector) => {
            this._catsBox = $(selector);
            return this;
        };

        this.getCatsBox = () => {
            return this._catsBox;
        };

        this.setCats = (selector) => {
            this._cats = $(selector);
            return this;
        };

        this.getCats = () => {
            return this._cats;
        };

        this.setTypesOnChange = () => {
            this.getTypes().on('change', (e) => {
                if ($(e.currentTarget).val() == this.options.TYPE_WORKER) {
                    this.getCatsBox().show();
                } else {
                    this.getCatsBox().hide();
                }
            });
        };

        this.setSearchableOptionList = () => {
            let op = this.options;
            this.getCats().searchableOptionList({
                showSelectAll: false,
                showSelectionBelowList: true,
                maxHeight: '300px',
                disabled: true,
                texts: {
                    'searchplaceholder': op.lang.searchplaceholder
                },
                events: {
                    onOpen: (sol) => {
                        if (sol.getSelection().length >= parseInt(op.specMaxLimit)) {
                            sol.close();
                            return false;
                        }
                    },
                    onChange: (sol, changedElements) => {
                        if (sol.getSelection().length >= parseInt(op.specMaxLimit)) {
                            sol.close();
                            $('.sol-input-container').find('input').attr('disabled', 'disabled');
                        } else {
                            $('.sol-input-container').find('input').removeAttr('disabled');
                        }
                    }
                }
            });
        };

        return {
            Register:() => {
                let op = this.options;

                this.setTypes(op.inputTypes);
                this.setCatsBox(op.boxSpecs);
                this.setCats(op.selectSpecs);

                this.setTypesOnChange();
                this.setSearchableOptionList();

            },
        }
    }
}