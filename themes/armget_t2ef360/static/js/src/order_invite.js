class OrderInvite{

    static makeInvite(order_id, worker_id, msg){
        bff.ajax(bff.ajaxURL('orders', 'invite'), {user: worker_id, order: order_id, message: '', hash: app.csrf_token}, function (resp, errors){
            if (resp && resp.success) {
                app.alert.success(msg);
                $('#j-order-invite-'+order_id).hide();
            } else {
                app.alert.error(errors);
            }
        });
    }
}