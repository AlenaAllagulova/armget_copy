class Search {

    static getSuggests(search_form) {
        let form = search_form;
        let idInputSearch = $('#' + form);
        let qStr = idInputSearch.val();
        if(qStr.length == 0){
            $('#suggest_list').html('');
        }
        if (qStr.length > 2) {
            bff.ajax(bff.ajaxURL('catcher&ev=getSuggest', ''), {
                q_str: qStr,
                search_form: form
            }, function (d, error) {
                if (d.hasOwnProperty('list')){
                    $('#suggest_list').html('');
                    $('#suggest_list').append(d.list);
                }
            });
        }
    }

}