'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var OrderInvite = function () {
    function OrderInvite() {
        _classCallCheck(this, OrderInvite);
    }

    _createClass(OrderInvite, null, [{
        key: 'makeInvite',
        value: function makeInvite(order_id, worker_id, msg) {
            bff.ajax(bff.ajaxURL('orders', 'invite'), { user: worker_id, order: order_id, message: '', hash: app.csrf_token }, function (resp, errors) {
                if (resp && resp.success) {
                    app.alert.success(msg);
                    $('#j-order-invite-' + order_id).hide();
                } else {
                    app.alert.error(errors);
                }
            });
        }
    }]);

    return OrderInvite;
}();
//# sourceMappingURL=order_invite.js.map
