<?php
    $url = array(
        'user.login'    => Users::url('login'),
        'user.register' => Users::url('register'),
        'user.logout'   => Users::url('logout'),
    );
?>
<?  ?>
    <!-- Topline -->
<? if( ! bff::security()->isLogined() ) { ?>
    <div class="l-topBar not-logged">
        <div class="container">
            <div class="text-center">
                <a href="<?= $url['user.login'] ?>"> <?= _t('users', 'Вход'); ?></a>
                <a href="<?= $url['user.register'] ?>"><?= _t('users', 'Регистрация'); ?></a>
            </div>
        </div><!-- /.container -->
    </div>
<? } else {
    $user = User::data(array('name', 'login', 'balance', 'workflow'));
    $user += bff::security()->userCounter(array());
    $workflowEnabled = bff::fairplayEnabled() && $user['workflow'];
    if( ! isset($user['cnt_internalmail_new'])){ $user['cnt_internalmail_new'] = 0; }
    if( ! isset($user['cnt_orders_offers_client'])){ $user['cnt_orders_offers_client'] = 0; }
    if( ! isset($user['cnt_orders_offers_worker'])){ $user['cnt_orders_offers_worker'] = 0; }
    if( ! isset($user['cnt_fairplay_workflows'])){ $user['cnt_fairplay_workflows'] = 0; }
    if(Users::useClient()){
        if(User::isWorker()){
            $user['new_orders_cnt'] = $user['cnt_orders_offers_worker'];
        }else{
            $user['new_orders_cnt'] = $user['cnt_orders_offers_client'];
        }
        $msgOrders = tpl::declension($user['new_orders_cnt'], _t('orders', 'новое событие;новых события;новых событий'));
    }else{
        $user['new_orders_cnt'] = $user['cnt_orders_offers_client'];
        $user['new_offers_cnt'] = $user['cnt_orders_offers_worker'];
        $msgOrders = tpl::declension($user['new_orders_cnt'], _t('orders', 'новое событие;новых события;новых событий'));
        $msgOffers = tpl::declension($user['new_offers_cnt'], _t('orders', 'новое событие;новых события;новых событий'));
    }
    $userDropdown = array();
    $url += array(
        'my.settings' => Users::url('my.settings'),
        'my.messages' => InternalMail::url('my.messages'),
        'my.orders'   => Users::url('profile', array('login'=>$user['login'],'tab'=>'orders')),
        'my.offers'   => Users::url('profile', array('login'=>$user['login'],'tab'=>'offers')),
        'my.cabinet'  => Users::url('profile', array('login'=>$user['login'])),
        'my.history'  => Bills::url('my.history'),
    );
    if($workflowEnabled){
        $url += array(
            'my.workflows'   => Users::url('profile', array('login'=>$user['login'],'tab'=>'workflows')),
        );
        $msgWorkflows = tpl::declension($user['cnt_fairplay_workflows'], _t('orders', 'новое событие;новых события;новых событий'));
    }
    if(empty($user['name'])){
        $user['name'] = '['.$user['login'].']';
    }
    $isPro = User::isPro();
    $buyPro = bff::servicesEnabled() && User::isWorker() && ! $isPro && Users::proEnabled();
    $mobileCounter = $user['cnt_internalmail_new'] + $user['new_orders_cnt'] + (isset($user['new_offers_cnt']) ? $user['new_offers_cnt'] : 0);
?>
    <div class="l-topBar logged<? if(Users::useClient() && User::isClient()){ ?> l-topBar-client<? } ?>">
        <div class="container">

            <ul class="l-topBar-menu pull-left">
                <li class="hidden-xs"><a href="<?= $url['my.cabinet'] ?>"><i class="fa fa-user"></i> <span><?= $user['name'] ?></span><? if($isPro): ?> <span class="arm-pro active show-tooltip" data-placement="bottom" data-container="body" title="<?= _t('users', 'до [expire]', array('expire' => User::proExpire())); ?>"><img src="<?= bff::url('/img/icon/pro.svg')?>" alt=""></span><? endif; ?></a></li>

                <? if($buyPro){ ?><li class="hidden-xs"><a href="<?= Svc::url('view', array('keyword' => 'pro')) ?>"><span class="l-tablet-visible"><?= _t('svc', 'Купить'); ?></span> <span class="arm-pro"><img src="<?= bff::url('/img/icon/pro.svg')?>" alt=""></span></a></li><? } ?>
                <li class="visible-xs"><a href="#j-topbar-center" data-toggle="collapse" class="l-topBar-center-toggle collapsed"><i class="fa fa-user"></i> <span><?= $user['name'] ?></span><? if($isPro): ?> <span class="arm-pro active"><img src="<?= bff::url('/img/icon/pro.svg')?>" alt=""></span><? endif; ?><i class="l-topBar-msg <?= ! $mobileCounter ? 'hidden' : '' ?>"><?= $mobileCounter ?></i></a></li>
            </ul>

            <div class="l-topBar-center collapse" id="j-topbar-center">
                <ul class="l-topBar-menu"><? $sMsgTitle = tpl::declension( $user['cnt_internalmail_new'], _t('internalmail', 'новое сообщение;новых сообщений;новых сообщений') ); ?>
                    <li class="visible-xs"><a href="<?= $url['my.cabinet'] ?>"><i class="fa fa-user"></i> <span><?= _t('', 'Профиль'); ?></span></a></li>
                    <li><a href="<?= $url['my.messages'] ?>" data-toggle="tooltip" data-placement="bottom" class="show-tooltip" title="<?= $sMsgTitle ?>" data-original-title="<?= $sMsgTitle ?>"><i class="fa fa-envelope-o hidden-sm"></i> <span><?= _t('', 'Сообщения'); ?></span><i class="l-topBar-msg <?= ! $user['cnt_internalmail_new'] ? 'hidden' : '' ?> j-cnt-msg"><?= $user['cnt_internalmail_new'] ?></i></a></li>
                    <li><a href="<?= $url['my.orders'] ?>" data-toggle="tooltip" data-placement="bottom" class="show-tooltip" title="<?= $msgOrders ?>" data-original-title="<?= $msgOrders ?>"><i class="fa fa-book hidden-sm"></i> <span><?= _t('users', 'Заказы') ?></span></a><i class="l-topBar-msg <?= ! $user['new_orders_cnt'] ? 'hidden' : '' ?>"><?= $user['new_orders_cnt'] ?></i></li>
                    <? if($workflowEnabled): ?><li><a href="<?= $url['my.workflows'] ?>" data-toggle="tooltip" data-placement="bottom" class="show-tooltip" title="<?= $msgWorkflows ?>" data-original-title="<?= $msgWorkflows ?>"><i class="fa fa-rocket hidden-sm"></i> <span><?= _t('fp', 'Ход работ') ?></span></a><i class="l-topBar-msg <?= ! $user['cnt_fairplay_workflows'] ? 'hidden' : '' ?>"><?= $user['cnt_fairplay_workflows'] ?></i></li><? endif; ?>
                    <? if(!Users::useClient()) { ?><li><a href="<?= $url['my.offers'] ?>" data-toggle="tooltip" data-placement="bottom" class="show-tooltip" title="<?= $msgOffers ?>" data-original-title="<?= $msgOffers ?>"><i class="fa fa-comments"></i> <span><?= _t('orders', 'Заявки') ?></span></a><i class="l-topBar-msg <?= ! $user['new_offers_cnt'] ? 'hidden' : '' ?>"><?= $user['new_offers_cnt'] ?></i></li><? } ?>
                    <? if(bff::servicesEnabled()): ?><li><a href="<?= $url['my.history'] ?>"><i class="fa fa-money hidden-sm"></i> <span><?= _t('bill', 'Счет:'); ?> </span> <?= $user['balance'] ?> <?= Site::currencyDefault(); ?></a></li><? endif; ?>
                    <? if($buyPro): ?><li class="visible-xs"><a href="<?= Svc::url('view', array('keyword' => 'pro')) ?>"><?= _t('svc', 'Купить'); ?> <span class="arm-pro"><img src="<?= bff::url('/img/icon/pro.svg')?>" alt=""></span></a></li><? endif; ?>
                </ul>
            </div>

            <div class="l-topBar-settings dropdown">
                <a href="#" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="<?= $url['my.settings'] ?>"><i class="fa fa-cogs"></i> <?= _t('users', 'Настройки'); ?></a></li>
                    <? if(Users::accountsJoin()): ?><li><a href="javascript:;" onclick="app.user.changeJoin();"><i class="fa fa-refresh"></i> <?= User::isWorker() ? _t('users', 'В профиль заказчика') : _t('users', 'В профиль исполнителя') ?></a></li><? endif; ?>
                    <li><a href="<?= $url['user.logout'] ?>"><i class="fa fa-sign-out"></i> <?= _t('users', 'Выход'); ?></a></li>
                </ul>
            </div>

        </div>
    </div>
<? } ?>

<? if( ! empty($user['cnt_orders_offers_worker']) && Orders::i()->invitesCount()):  ?>
    <div class="alert alert-warning l-topAlert"><?= _t('orders', 'У вас есть <a [href]>неподтвержденные заказы</a>', array('href' => 'href="'.(Users::useClient() ? $url['my.orders'] : $url['my.offers']).'"')); ?></div>
<? endif; ?>


<? # Баннер: Растяжка сверху (top) ?>
<? if($banner = Banners::view('top', array('pos'=>'top', 'no-empty'=>true))){ ?>
<div class="l-topBanner">
	<?= $banner ?>
</div>
<? } ?>

    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">

            <!-- Logo and toggle button -->
            <div class="navbar-header relative">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <i class="fa fa-align-justify"></i> <?= _t('', 'Меню'); ?>
                </button>
                <a class="navbar-brand" href="<?= Geo::url(); ?>">
                    <div class="logo">
                        <img src="<?= Site::logoURL('header') ?>" alt="" />
                        <? $title = Site::titleHeader(); if ( ! empty($title)) { ?><span><?= $title ?></span><? } ?>
                    </div>
                </a>
                <? if( $geoFilter = Geo::filterEnabled()): $geo = Geo::filter(); ?>
                    <span class="navbar-header-dropdown-toggle">
                        <a href="javascript:void(0);" class="ajax-link" id="j-geo-region-select"><span><?= ! empty($geo['title']) ? $geo['title'] : _t('geo', 'Ваш регион') ?></span><b class="caret"></b></a>
                    </span>
                <? endif; ?>
            </div>
            <?= $geoFilter ? Geo::i()->filterForm() : '' ?>

            <!-- Menu -->
            <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
                <ul class="nav navbar-nav <?= (Site::indexView(Site::INDEX_VIEW_ORDERS_PLUS) ? 'in-navbar' : '') ?>">
                    <?  $aMainMenu = Sitemap::view('main');
                    if(!bff::moduleExists('shop') || ! Shop::enabled()){
                        unset($aMainMenu['shop']);
                    }
                    if(!bff::moduleExists('blog') || ! Blog::enabled()){
                        unset($aMainMenu['blog']);
                    }
                    if(!bff::moduleExists('qa') || ! Qa::enabled()){
                        unset($aMainMenu['qa']);
                    }
                    if(!bff::moduleExists('articles') || ! Articles::enabled()){
                        unset($aMainMenu['more']['sub']['articles']);
                    }
                    foreach($aMainMenu as $k => $v) {
                        if( ! empty($v['sub'])){
                            ?>
                            <li class="dropdown">
                                <a href="<?= $v['link'] ?>" class="dropdown-toggle" data-toggle="dropdown">
                                    <?= $v['title'] ?><b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <? foreach($v['sub'] as $kk => $vv){
                                        ?><li<? if($vv['a']) { ?> class="active"<? } ?>><a href="<?= $vv['link'] ?>"><?= $vv['title'] ?></a></li><?
                                       } ?>
                                </ul>
                            </li>
                            <?
                        }else{
                            ?><li<? if($v['a']) { ?> class="active"<? } ?>><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></li><?
                    } }
                    ?>
                </ul>

            </div>
        </div>
    </nav>