<?php $aShareArticleData = Catcher::getArticleShareData(); # for share in messengers?>
<!DOCTYPE html>
<html class="no-js">
<head>
<?= SEO::i()->metaRender(array('content-type'=>true, 'csrf-token'=>true)) ?>
<? if (!empty($aShareArticleData['main_img'])): ?>
    <meta property="og:image" content="<?= $aShareArticleData['main_img'] ;?>" />
    <meta property="og:image:width" content="100" />
    <meta property="og:image:height" content="100" />
<? endif; ?>
<?= View::template('css'); ?>
</head>
<body data-dbq="<?= bff::database()->statQueryCnt(); ?>">
<?= View::template('alert'); ?>
<div id="l-wrap">
    <?= View::template('header'); ?>
    <?= $centerblock; ?>
    <?= Seo::i()->getTextBlock(); ?>
</div>
<?= View::template('footer'); ?>
</body>
</html>