<?php

use bff\db\migrations\Migration as Migration;

class ExtT2ef360767b6a1bb2d13ab8321a6b32e781507a4V1x0x1 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table( 'bff_catcher_words',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']]
        )
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('enabled', 'boolean', ['null' => false, 'default' => true])
            ->create();

        $this->table(
            'bff_catcher_words_lang',
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' =>['id','lang']]
        )
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('title','string',['limit'=>100])
            ->addColumn('lang','string',['limit'=>2])
            ->addIndex(['id','lang'], ['unique' => true])
            ->addForeignKey(
                'id',
                'bff_catcher_words',
                'id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $exists = $this->table('bff_catcher_words_lang')->hasForeignKey('id');
        if ($exists) {
            $this->table('bff_catcher_words_lang')->dropForeignKey('id');
        }

        $this->dropIfExists('bff_catcher_words_lang');
        $this->dropIfExists('bff_catcher_words');
    }
}