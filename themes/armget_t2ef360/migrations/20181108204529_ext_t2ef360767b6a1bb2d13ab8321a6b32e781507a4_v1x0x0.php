<?php

use bff\db\migrations\Migration as Migration;

class ExtT2ef360767b6a1bb2d13ab8321a6b32e781507a4V1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX . 'orders')
            ->addColumn('prepay', 'integer', ['signed' => false, 'null' => true, 'default' => 0])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $table = $this->table(DB_PREFIX . 'orders');
        $table->removeColumn('prepay')
            ->update();

    }
}