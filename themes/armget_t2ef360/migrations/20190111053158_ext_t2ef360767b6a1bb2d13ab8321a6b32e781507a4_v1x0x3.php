<?php

use bff\db\migrations\Migration as Migration;

class ExtT2ef360767b6a1bb2d13ab8321a6b32e781507a4V1x0x3 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $row = [
            [
                'id'            => 4096,
                'type'          => 1,
                'keyword'       => 'post_site',
                'module'        => 'orders',
                'module_title'  => 'Заказы',
                'title'         => 'Размещение на сайте',
                'price'         => '20.00',
                'settings'      => 'YTo0OntzOjI6Im9uIjtpOjE7czoxMDoidGl0bGVfdmlldyI7YToxOntzOjI6InJ1IjtzOjM2OiLQoNCw0LfQvNC10YnQtdC90LjQtSDQvdCwINGB0LDQudGC0LUiO31zOjExOiJkZXNjcmlwdGlvbiI7YToxOntzOjI6InJ1IjtzOjE5MjoiPHA+0KPRgdC70YPQs9CwINGA0LDQt9C80LXRidC10L3QuNC1INC90LAg0YHQsNC50YLQtSDQv9C+0LzQvtC20LXRgiDQstCw0Lwg0LLRgdC10LPQtNCwINC/0L7QtNC00LXRgNC20LjQstCw0YLRjCDRgdCy0L7QtSDQv9GA0LXQtNC70L7QttC10L3QuNC1INCyINCw0LrRgtGD0LDQu9GM0L3QvtC8INGB0L7RgdGC0L7Rj9C90LjQuC48L3A+Ijt9czoxNjoiZGVzY3JpcHRpb25fZnVsbCI7YToxOntzOjI6InJ1IjtzOjE5MjoiPHA+0KPRgdC70YPQs9CwINGA0LDQt9C80LXRidC10L3QuNC1INC90LAg0YHQsNC50YLQtSDQv9C+0LzQvtC20LXRgiDQstCw0Lwg0LLRgdC10LPQtNCwINC/0L7QtNC00LXRgNC20LjQstCw0YLRjCDRgdCy0L7QtSDQv9GA0LXQtNC70L7QttC10L3QuNC1INCyINCw0LrRgtGD0LDQu9GM0L3QvtC8INGB0L7RgdGC0L7Rj9C90LjQuC48L3A+Ijt9fQ==',
                'modified'      => '2019-01-01 15:19:44',
                'modified_uid'  => 1,
                'num'           => 13,
                'icon_s'         => '',
            ],
            [
                'id'            => 8192,
                'type'          => 1,
                'keyword'       => 'post_site_tg',
                'module'        => 'orders',
                'module_title'  => 'Заказы',
                'title'         => 'Размещение на сайте и TG',
                'price'         => '30.00',
                'settings'      => 'YTo0OntzOjI6Im9uIjtpOjE7czoxMDoidGl0bGVfdmlldyI7YToxOntzOjI6InJ1IjtzOjQyOiLQoNCw0LfQvNC10YnQtdC90LjQtSDQvdCwINGB0LDQudGC0LUg0LggVEciO31zOjExOiJkZXNjcmlwdGlvbiI7YToxOntzOjI6InJ1IjtzOjE5MjoiPHA+0KPRgdC70YPQs9CwINGA0LDQt9C80LXRidC10L3QuNC1INC90LAg0YHQsNC50YLQtSDQv9C+0LzQvtC20LXRgiDQstCw0Lwg0LLRgdC10LPQtNCwINC/0L7QtNC00LXRgNC20LjQstCw0YLRjCDRgdCy0L7QtSDQv9GA0LXQtNC70L7QttC10L3QuNC1INCyINCw0LrRgtGD0LDQu9GM0L3QvtC8INGB0L7RgdGC0L7Rj9C90LjQuC48L3A+Ijt9czoxNjoiZGVzY3JpcHRpb25fZnVsbCI7YToxOntzOjI6InJ1IjtzOjE5MjoiPHA+0KPRgdC70YPQs9CwINGA0LDQt9C80LXRidC10L3QuNC1INC90LAg0YHQsNC50YLQtSDQv9C+0LzQvtC20LXRgiDQstCw0Lwg0LLRgdC10LPQtNCwINC/0L7QtNC00LXRgNC20LjQstCw0YLRjCDRgdCy0L7QtSDQv9GA0LXQtNC70L7QttC10L3QuNC1INCyINCw0LrRgtGD0LDQu9GM0L3QvtC8INGB0L7RgdGC0L7Rj9C90LjQuC48L3A+Ijt9fQ==',
                'modified'      => '2019-01-01 16:19:44',
                'modified_uid'  => 1,
                'num'           => 14,
                'icon_s'         => '',
            ],
            [
                'id'            => 16384,
                'type'          => 1,
                'keyword'       => 'post_site_tg_vk',
                'module'        => 'orders',
                'module_title'  => 'Заказы',
                'title'         => 'Размещение на сайте, TG и VK',
                'price'         => '40.00',
                'settings'      => 'YTo0OntzOjI6Im9uIjtpOjE7czoxMDoidGl0bGVfdmlldyI7YToxOntzOjI6InJ1IjtzOjQ2OiLQoNCw0LfQvNC10YnQtdC90LjQtSDQvdCwINGB0LDQudGC0LUsIFRHINC4IFZLIjt9czoxMToiZGVzY3JpcHRpb24iO2E6MTp7czoyOiJydSI7czoyMDI6IjxwPtCj0YHQu9GD0LPQsCDRgNCw0LfQvNC10YnQtdC90LjRjyDQvdCwINGB0LDQudGC0LUsIFRHINC4IFZLINC/0L7QvNC+0LbQtdGCINCy0LDQvCDQstGB0LXQs9C00LAg0L/QvtC00LTQtdGA0LbQuNCy0LDRgtGMINGB0LLQvtC1INC/0YDQtdC00LvQvtC20LXQvdC40LUg0LIg0LDQutGC0YPQsNC70YzQvdC+0Lwg0YHQvtGB0YLQvtGP0L3QuNC4LjwvcD4iO31zOjE2OiJkZXNjcmlwdGlvbl9mdWxsIjthOjE6e3M6MjoicnUiO3M6MjEyOiI8cD7Qo9GB0LvRg9Cz0LAmbmJzcDvRgNCw0LfQvNC10YnQtdC90LjRjyDQvdCwINGB0LDQudGC0LUsIFRHINC4IFZLJm5ic3A70L/QvtC80L7QttC10YIg0LLQsNC8INCy0YHQtdCz0LTQsCDQv9C+0LTQtNC10YDQttC40LLQsNGC0Ywg0YHQstC+0LUg0L/RgNC10LTQu9C+0LbQtdC90LjQtSDQsiDQsNC60YLRg9Cw0LvRjNC90L7QvCDRgdC+0YHRgtC+0Y/QvdC40LguPC9wPiI7fX0=',
                'modified'      => '2019-01-01 17:19:44',
                'modified_uid'  => 1,
                'num'           => 15,
                'icon_s'         => '',
            ],
            [
                'id'            => 32768,
                'type'          => 1,
                'keyword'       => 'post_site_tg_vk_fb',
                'module'        => 'orders',
                'module_title'  => 'Заказы',
                'title'         => 'Размещение на сайте, TG, VK и FB',
                'price'         => '50.00',
                'settings'      => 'YTo0OntzOjI6Im9uIjtpOjE7czoxMDoidGl0bGVfdmlldyI7YToxOntzOjI6InJ1IjtzOjUwOiLQoNCw0LfQvNC10YnQtdC90LjQtSDQvdCwINGB0LDQudGC0LUsIFRHLCBWSyDQuCBGQiI7fXM6MTE6ImRlc2NyaXB0aW9uIjthOjE6e3M6MjoicnUiO3M6MjA2OiI8cD7Qo9GB0LvRg9Cz0LAg0YDQsNC30LzQtdGJ0LXQvdC40Y8g0L3QsCDRgdCw0LnRgtC1LCBURywgVksg0LggRkIg0L/QvtC80L7QttC10YIg0LLQsNC8INCy0YHQtdCz0LTQsCDQv9C+0LTQtNC10YDQttC40LLQsNGC0Ywg0YHQstC+0LUg0L/RgNC10LTQu9C+0LbQtdC90LjQtSDQsiDQsNC60YLRg9Cw0LvRjNC90L7QvCDRgdC+0YHRgtC+0Y/QvdC40LguPC9wPiI7fXM6MTY6ImRlc2NyaXB0aW9uX2Z1bGwiO2E6MTp7czoyOiJydSI7czoyMDY6IjxwPtCj0YHQu9GD0LPQsCDRgNCw0LfQvNC10YnQtdC90LjRjyDQvdCwINGB0LDQudGC0LUsIFRHLCBWSyDQuCBGQiDQv9C+0LzQvtC20LXRgiDQstCw0Lwg0LLRgdC10LPQtNCwINC/0L7QtNC00LXRgNC20LjQstCw0YLRjCDRgdCy0L7QtSDQv9GA0LXQtNC70L7QttC10L3QuNC1INCyINCw0LrRgtGD0LDQu9GM0L3QvtC8INGB0L7RgdGC0L7Rj9C90LjQuC48L3A+Ijt9fQ==',
                'modified'      => '2019-01-01 18:19:44',
                'modified_uid'  => 1,
                'num'           => 16,
                'icon_s'         => '',
            ],
        ];

        $this->insert(TABLE_SVC, $row);

    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {

    }
}