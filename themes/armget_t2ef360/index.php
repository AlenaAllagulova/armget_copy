<?php

class Theme_Armget_t2ef360 extends Theme
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id'  => 't2ef360767b6a1bb2d13ab8321a6b32e781507a4',
            'theme_title'   => 'ArmGet',
            'theme_version' => '1.0.0',
            'theme_parent'  => 'base',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'block-1_title' => array(
                'title' => 'Блок №1 Заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка для Блок №1'),
                'default' => $this->lang('Не нашли исполнителя в каталоге?'),
            ),
            'block-1_descr' => array(
                'title' => 'Блок №1 Описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания для Блок №1'),
                'default' => $this->lang('Для решения разовой задачи, воспользуйтесь формой 
                        “Разовый заказ”, подробно опишите вашу задачу и разместите 
                        ваше задание на бирже. Затем ждите предложения от 
                        фрилансеров. После того, как к вам поступит достаточно 
                        откликов, выберите лучшего исполнителя и оформите заказ.'),
            ),
            'block-2_title' => array(
                'title' => 'Блок №2 Заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка для Блок №2'),
                'default' => $this->lang('Нужен удаленный сотрудник на постоянную работу?'),
            ),
            'block-2_descr' => array(
                'title' => 'Блок №2 Описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания для Блок №2'),
                'default' => $this->lang('Если хотите пригласить фрилансера на постоянную работу, 
                        то вы можете разместить свою вакансию на бирже, через форму “Постоянная работа”. 
                        В зависимости от выбранного пакета размещения, вакансию могут увидеть не только 
                        фрилансеры нашей биржи, но и подписчики нашего канала в Telegram, наших групп в 
                        Вконтакте и страницы в Facebook, с общей аудиторией более 200 000 человек'),
            ),
            'list_title' => array(
                'title' => 'Список преимуществ биржи Заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка списка преимуществ биржи'),
                'default' => $this->lang('Только у нас'),
            ),
            'list-item-1_title' => array(
                'title' => 'Элемент списка №1 Заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка элемента списка №1'),
                'default' => $this->lang('Бесплатное размещение задач для вебмастеров и IT специалистов *'),
            ),
            'list-item-1_descr' => array(
                'title' => 'Элемент списка №1 Описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания элемента списка №1'),
                'default' => '',
            ),
            'list-item-2_title' => array(
                'title' => 'Элемент списка №2 Заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка элемента списка №2'),
                'default' => $this->lang('Размещение вакансий от компаний и кадровых агентств по найму удалённых 
                         сотрудников на постоянную работу'),
            ),
            'list-item-2_descr' => array(
                'title' => 'Элемент списка №2 Описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания элемента списка №2'),
                'default' => '',
            ),
            'list-item-3_title' => array(
                'title' => 'Элемент списка №3 Заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка элемента списка №3'),
                'default' => $this->lang('Возможность самостоятельно подобрать нужного специалиста в каталоге 
                        исполнителей'),
            ),
            'list-item-3_descr' => array(
                'title' => 'Элемент списка №3 Описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания элемента списка №3'),
                'default' => $this->lang('В каталоге, на главной странице биржи, вы можете самостоятельно 
                        подобрать нужного вам специалиста по таким критериям, как рейтинг, 
                        наличие положительных отзывов, портфолио и др'),
            ),

            'list-item-4_title' => array(
                'title' => 'Элемент списка №4 Заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка элемента списка №4'),
                'default' => $this->lang('Все сделки с исполнителями проходят через Безопасную сделку'),
            ),
            'list-item-4-point-1_title' => array(
                'title' => 'Элемент списка №4, point 1 заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка point 1'),
                'default' => $this->lang('Безопасная сделка'),
            ),
            'list-item-4-point-1_descr' => array(
                'title' => 'Элемент списка №4, point 1 описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания point 1'),
                'default' => $this->lang(' позволяет проводить финансовый расчет между Заказчиком и 
                                Исполнителем через биржу, как гаранта выполнения своих обязательств обеими 
                                сторонами, сводя, таким образом, риски денежных потерь обеими сторонами к нулю.'),
            ),
            'list-item-4-point-2_title' => array(
                'title' => 'Элемент списка №4, point 2 заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка point 2'),
                'default' => $this->lang('Заказчик'),
            ),
            'list-item-4-point-2_descr' => array(
                'title' => 'Элемент списка №4, point 2 описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания point 2'),
                'default' => $this->lang(' гарантированно вернет свои средства, если исполнитель нарушил сроки 
                                или не выполнил работу по согласованному заданию.'),
            ),
            'list-item-4-point-3_title' => array(
                'title' => 'Элемент списка №4, point 3 заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка point 3'),
                'default' => $this->lang('Исполнитель'),
            ),
            'list-item-4-point-3_descr' => array(
                'title' => 'Элемент списка №4, point 3 описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания point 3'),
                'default' => $this->lang(' гарантированно получит оплату за работу, если он выполнил задание 
                                в соответствии с требованиями заказчика качественно и в срок.'),
            ),
            'list-item-4-point-4_title' => array(
                'title' => 'Элемент списка №4, point 4 заголовок',
                'input' => 'text',
                'placeholder' => $this->langAdmin('Вставьте текст заголовка point 4'),
                'default' => $this->lang('Спорные сделки рассматривает арбитражный отдел биржи!'),
            ),
            'list-item-4-point-4_descr' => array(
                'title' => 'Элемент списка №4, point 4 описание',
                'input' => 'textarea',
                'placeholder' => $this->langAdmin('Вставьте текст описания point 4'),
                'default' => '',
            ),
        ), ['titleRow' => 130,]);
        
        $this->cssEdit(array(
            static::CSS_FILE_CUSTOM => ['path' => $this->path('/static/css/custom.css', false), 'save' => true],
        ));
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
        $this->css('/css/nouislider.css');
        $this->css('/css/sol.css');
        $this->css('/css/dist/armget.css');

        $this->js('/js/nouislider.js');
        $this->js('/js/sol.js');
        $this->js('/js/dist/myowncode.js');

        bff::hookAdd('orders.service.types', function ($list){
            # according to task ACC-40
            if (isset($list[Orders::SERVICE_TYPE_CONTEST])){
                unset($list[Orders::SERVICE_TYPE_CONTEST]);
            }
           return $list;
        });
    }
}