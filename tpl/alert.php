<div class="c-fixedAlerts" id="j-alert-global" style="display: none;">
    <div class="alert j-wrap">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong class="j-title"></strong>
        <div class="j-message"></div>
    </div>
</div>
