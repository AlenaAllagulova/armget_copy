<?php

?>
<ol class="breadcrumb">
    <li><a href="<?= Geo::url() ?>"><i class="fa fa-home"></i></a></li>
    <? foreach($crumbs as $v) {
        if( ! empty($v['active'])){
            ?><li class="active"><?= $v['title'] ?></li><?
        } else {
            if ( ! empty($v['link'])) {
                ?><li><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></li><?
            } else {
                ?><li><?= $v['title'] ?></li><?
            }
        }
    } ?>
</ol>
