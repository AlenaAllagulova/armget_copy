<!DOCTYPE html>
<html class="no-js">
<head>
<?= SEO::i()->metaRender(array('content-type'=>true, 'csrf-token'=>true)) ?>
<?= View::template('css'); ?>
</head>
<body data-dbq="<?= bff::database()->statQueryCnt(); ?>">
<?= View::template('alert'); ?>
<div id="l-wrap">
    <?= $centerblock; ?>
</div>
<?= View::template('footer'); ?>
</body>
</html>