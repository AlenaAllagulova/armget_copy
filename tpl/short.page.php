<!-- Main Container -->
<div class="container">

    <!-- BTGIN signBlock -->
    <section class="s-signBlock">
        <div class="s-signBlock-top">
            <div class="s-signBlock-logo">
                <a href="<?= Geo::url() ?>"><img src="<?= bff::url('/img/signLogo.png') ?>" alt="" /></a>
            </div><!-- /.signBlock-logo -->
            <h1 class="small j-shortpage-title"><?= $title ?></h1>
        </div><!-- /.signBlock-top -->

        <?= $content ?>

    </section>
    <!-- END signBlock -->

</div><!-- /.container -->
