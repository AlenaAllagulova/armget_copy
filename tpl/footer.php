<?php
    $aFooterMenu = Sitemap::view('footer');
    $aCounters = Site::i()->getCounters();
?>
<p class="c-scrolltop" id="j-scrolltop">
    <a href="#"><span><i class="fa fa-arrow-up"></i></span><?= _t('', 'Наверх'); ?></a>
</p>
<footer id="l-footer">
    <div class="container">
        <div class="row">

            <div class="col-sm-3">
                <?  # Выбор языка:
                $languages = bff::locale()->getLanguages(false, config::sys('locale.hidden', array()));
                if (sizeof($languages) > 1) { ?>
                    <div class="mrgb10">
                    <?= _t('', 'Язык:') ?>
                    <span class="dropdown">
                    <a href="#" class="ajax-link" data-toggle="dropdown"><span><?= $languages[LNG]['title'] ?></span><i class="caret"></i></a>
                    <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                        <? foreach($languages as $k=>$v) { ?>
                            <li<? if($k==LNG){ ?> class="active"<? } ?>>
                                <a href="<?= bff::urlLocaleChange($k) ?>">
                                    <?= $v['title'] ?>
                                </a>
                            </li>
                        <? } ?>
                    </ul>
                    </span>
                    </div>
                <? } ?>
                <?= config::get('copyright_'.LNG); ?>
<?  ?>
                <ul class="l-footer-links">
                <? if( ! empty($aFooterMenu['col1']['sub']) ) {
                    foreach($aFooterMenu['col1']['sub'] as $v) {
                        echo '<li><a href="'.$v['link'].'" class="'.$v['style'].'">'.$v['title'].'</a></li>';
                    }
                }
                ?>
                </ul>
                <div class="l-footer-age">
                    18+
                </div>
            </div><!-- ./col-sm-3 -->

            <div class="col-sm-6">

                <div class="row">
                    <? if( ! empty($aFooterMenu['col2']['sub']) ) { ?>
                    <div class="col-xs-6">
                        <ul class="l-footer-links">
                            <? foreach($aFooterMenu['col2']['sub'] as $v) {
                                echo '<li><a href="'.$v['link'].'" class="'.$v['style'].'">'.$v['title'].'</a></li>';
                            } ?>
                        </ul>
                    </div>
                    <? } ?>
                    <? if( ! empty($aFooterMenu['col3']['sub']) ) { ?>
                    <div class="col-xs-6">
                        <ul class="l-footer-links"><? foreach($aFooterMenu['col3']['sub'] as $v) {
                                echo '<li><a href="'.$v['link'].'" class="'.$v['style'].'">'.$v['title'].'</a></li>';
                            } ?>
                        </ul>
                    </div>
                    <? } ?>

                </div>

            </div><!-- ./col-sm-3 -->

            <div class="col-sm-3">
                <!-- Social Icons -->
                <div class="social-icons">
                    <ul>
                        <? foreach (Site::i()->socialLinks(true) as $k=>$v): ?>
                            <li class="<?= $k ?>">
                                <a href="<?= $v['url'] ?>" target="_blank"><i class="<?= $v['icon'] ?>"></i></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div><!-- /social-icons -->
                <div class="clearfix"></div>
                <div class="l-footer-counters">
                    <? if( ! empty($aCounters)) { ?>
                        <? foreach($aCounters as $v) { ?><?= $v['code'] ?><? } ?>
                    <? } ?>
                </div>
            </div>

        </div>
    </div><!-- /.container -->
</footer>
<?= View::template('js'); ?>
<?= js::renderInline(js::POS_FOOT); ?>