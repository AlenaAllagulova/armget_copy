<?php
# данные отсутствуют (общее их кол-во == 0)
if($total <= 0 || sizeof($links) === 1) return;
?>
<ul class="pagination">
    <? if($prev): ?><li><a <?= $prev['href'] ?> <?= $prev['onclick']; ?>>&laquo;</a></li><? else: ?><li class="disabled"><span>&laquo;</span></li><? endif; ?>
    <? if($first): ?><li><a <?= $first['href'] ?> <?= $first['onclick']; ?>>1</a></li><li><span>...</span></li><? endif; ?>
    <? foreach($links as $v): ?>
        <? if($v['active']): ?>
            <li class="active"><span><?= $v['page'] ?></span></li>
        <? else: ?>
            <li class="hidden-xs"><a <?= $v['href'] ?> <?= $v['onclick']; ?>><?= $v['page'] ?></a></li>
        <? endif; ?>
    <? endforeach; ?>
    <? if($last): ?><li><span>...</span></li><li><a <?= $last['href'] ?> <?= $last['onclick']; ?>><?= $last['page'] ?></a></li><? endif; ?>
    <? if($next): ?><li><a <?= $next['href'] ?> <?= $next['onclick']; ?>>&raquo;</a></li><? else: ?><li class="disabled"><span>&raquo;</span></li><? endif; ?>
</ul>
