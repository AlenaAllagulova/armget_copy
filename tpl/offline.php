<!DOCTYPE html>
<html class="no-js">
<head>
<?= SEO::i()->metaRender(array('content-type'=>true)) ?>
<?= View::template('css'); ?>
</head>
<body>
    <div id="l-wrap">
        <div class="container">
            <section class="s-signBlock">
                <div class="s-signBlock-top">
                    <div class="s-signBlock-logo">
                        <a href="#"><img src="<?= Site::logoURL('offline') ?>" alt="" /></a>
                    </div>
                    <h1 class="small"><?= _t('', 'Сайт временно отключен') ?></h1>
                </div>
                <div class="s-signBlock-form">
                    <div class="s-signBlock-div" style="text-align: center;">
                        <?= config::get('offline_reason_'.LNG); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>