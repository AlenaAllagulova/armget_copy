<div class="l-menu-filter">
    <button type="button" class="l-tabs-toggle btn btn-default" data-toggle="collapse" data-target="#<?= $class ?>"><i class="fa fa-chevron-down pull-right"></i><span class="j-title"><?= $tabs[ $a ]['t'] ?></span></button>
    <ul class="nav nav-tabs collapse" id="<?= $class ?>">
        <? foreach($tabs as $k => $v): ?>
            <li class="j-tab-<?= $k ?><?= $a == $k ? ' active' : '' ?>"><a href="<?= ! empty($v['url']) ? $v['url'] : '#' ?>" data-a="<?= $k ?>"><?= $v['t'] ?></a></li>
        <? endforeach; ?>
    </ul>
</div>
<? if(empty($noAjax)): ?>
<script type="text/javascript">
<? js::start() ?>
app.tabs('<?= $class ?>');
<? js::stop() ?>
</script>
<? endif; ?>
