<!DOCTYPE html>
<html class="no-js">
<head>
<?= SEO::i()->metaRender(array('content-type'=>true, 'csrf-token'=>true)) ?>
<title><?= _t('','ArmGet-фриланс (freelance) биржа, удалённая работа, вакансии')?></title>
<meta name="keywords" lang="ru" content="<?= _t('','фриланс, freelance, удаленная работа, вакансии, фриланс биржа, работа на дому')?>" />
<meta name="description" lang="ru" content="<?= _t('','ArmGet - биржа фриланса и удалённой работы. Заказчики могут бесплатно размещать разовые задачи для вебмастеров и IT-специалистов. Для Исполнителей - это возможность найти разовую подработку или постоянную работу.')?>" />
<?= View::template('css'); ?>
</head>
<body data-dbq="<?= bff::database()->statQueryCnt(); ?>">
<?= View::template('alert'); ?>
<div id="l-wrap">
    <?= View::template('header'); ?>
    <?= $centerblock; ?>
    <?= Seo::i()->getTextBlock(); ?>
</div>
<?= View::template('footer'); ?>
</body>
</html>