<?php

return [
    # связь с администрацией
    'contacts-write' => [
        'pattern'  => 'help/write(.*)',
        'callback' => 'contacts/write/',
        'priority' => 420,
    ],
    'contacts-contact' => [
        'pattern'  => 'contact',
        'callback' => 'contacts/write/',
        'priority' => 430,
    ],
];