<?php

/**
 * Права доступа группы:
 *  - opinions: Отзывы
 *      - manage: Управление отзывами (список, модерация, удаление)
 */
class Opinions_ extends OpinionsBase
{
    public function listing()
    {
        if (!$this->haveAccessTo('manage')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'delete':
                {

                    $nOpinionsID = $this->input->postget('id', TYPE_UINT);
                    if (!$nOpinionsID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $res = $this->opinionDelete($nOpinionsID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                break;
                case 'approve':
                {

                    $nOpinionsID = $this->input->postget('id', TYPE_UINT);
                    if (!$nOpinionsID) {
                        $this->errors->unknownRecord(); break;
                    }

                    $aData = $this->model->opinionData($nOpinionsID, array('id','user_id','moderated','type','author_id'));
                    if (empty($aData)) {
                        $this->errors->impossible(); break;
                    }

                    $res = $this->model->opinionSave($nOpinionsID, array('moderated'=>1));
                    if (empty($res)) {
                        $this->errors->impossible(); break;
                    }
                    if (static::premoderation() && $aData['moderated'] == 0)
                    {
                        # Обновляем счетчики отзывов + рейтинг
                        $this->onUserOpinionsChange($aData['user_id'], 'add', $aData['type'], 0, $nOpinionsID);

                        # Отправим уведомление
                        $this->opinionEnotify($aData, 'add');
                    }
                    # обновляем счетчик отзывов "на модерации"
                    $this->moderationCounterUpdate();

                }
                break;
                case 'dev-users-counters-update': # пересчет счетчиков отзывов пользователей
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }

                    $this->model->reCalcUsersOpinionsCache();
                    Users::model()->cronCalcSpecsPositions();
                    # обновляем счетчик отзывов "на модерации"
                    $this->moderationCounterUpdate();

                    $this->adminRedirect(($this->errors->no() ? Errors::SUCCESS : Errors::IMPOSSIBLE), bff::$event);
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'tab'     => TYPE_UINT,
            'perpage' => TYPE_UINT,
            'type'  => TYPE_UINT,   # тип
            'author'=> TYPE_NOTAGS, # пользователь автор отзыва
            'user'  => TYPE_NOTAGS, # пользователь о ком отзыв
            'fr'   => TYPE_NOTAGS, # дата добавления от
            'to'   => TYPE_NOTAGS, # дата добавления до
            'ans'   => TYPE_BOOL,   # только с ответами
        ));

        # формируем фильтр списка отзывов
        $sql = array();
        $sqlOrder = 'O.created';
        $aData['pgn'] = '';
        $aData['f'] = $f;

        switch ($f['tab']) {
            case 1: # Все
            {
            }
            break;
            default: # На модерации
            {
                $sql[':mod'] = 'O.moderated != 1';
            }
            break;
        }

        # пользователь автор отзыва
        if (!empty($f['author'])) {
            $sql[':author'] = array(
                '(O.author_id = ' . intval($f['author']) . ' OR aU.email LIKE :author OR aU.login LIKE :author)',
                ':author' => '%' . $f['author'] . '%'
            );
        }

        # пользователь о ком отзыв
        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(O.user_id = ' . intval($f['user']) . ' OR uU.email LIKE :user OR uU.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }

        # дата добавления
        if (!empty($f['fr'])) {
            $from = strtotime($f['fr']);
            if (!empty($from)) {
                $sql[':fr'] = array('O.created >= :fr', ':fr' => date('Y-m-d 00:00:00', $from));
            }
        }
        if (!empty($f['to'])) {
            $to = strtotime($f['to']);
            if (!empty($to)) {
                $sql[':to'] = array('O.created <= :to', ':to' => date('Y-m-d 23:59:59', $to));
            }
        }

        if( ! empty($f['ans'])){
            $sql[':ans'] = 'O.answer IS NOT NULL';
        }

        if($f['type']){
            $sql['type'] = $f['type'];
        }

        $nCount = $this->model->opinionsListing($sql, true);
        $aPerpage = $this->preparePerpage($f['perpage'], array(20, 40, 60));
        $sFilter = http_build_query($f);
        unset($f['page']);
        $oPgn = new Pagination($nCount, $f['perpage'], $this->adminLink("listing&$sFilter&page=" . Pagination::PAGE_ID));
        $aData['pgn'] = $oPgn->view();

        $aData['opinions'] = ($nCount > 0 ?
            $this->model->opinionsListing($sql, false, $oPgn->getLimitOffset(), $sqlOrder) :
            array());

        $aData['perpage'] = $aPerpage;

        return $this->viewPHP($aData, 'admin.opinions.listing');
    }

    /**
     * Форма редактирования пользователя
     * Таб "Отзывы"
     * @param int $nUserID ID редактируемого пользователя
     * @return string HTML
     */
    public function user_listing($nUserID)
    {
        if ( ! $nUserID || ! $this->security->haveAccessToAdminPanel()) return '';
        $sql = array('author_id'=>$nUserID);

        $aData['count'] = $this->model->opinionsListing($sql, true);
        $aData['opinions'] = ($aData['count'] > 0 ?
            $this->model->opinionsListing($sql, false, '', 'O.created') :
            array());

        return $this->viewPHP($aData, 'admin.opinions.listing.ajax');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

}