<?php

class M_Opinions_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        if ($security->haveAccessToModuleToMethod('opinions','manage')) {
            $menuTitle = _t('opinions', 'Opinions');
            $menu->assign($menuTitle, 'Список', 'opinions', 'listing', true, 10,
                array('counter' => 'opinions_items_moderating'));
            $menu->adminHeaderCounter($menuTitle, 'opinions_items_moderating', 'opinions', 'listing', 6, '', array('parent'=>'moderation'));
        }
    }
}