<?php
/**
 * @var $this Opinions
 */
    $aUserTypes = Users::aTypes();
    $aTypes = Opinions::aTypes();
    $ordersOpinions = Orders::ordersOpinions();

    if( ! empty($list)): ?>
        <ul class="o-freelancersList p-feedbacksList media-list">
            <? foreach($list as $v):
                $id  = $v['id'];
                $isAuthor = User::isCurrent($v['author_id']);
            ?>
            <li class="media">
                <a name="<?= $v['login'] ?>"></a>
                <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="o-freelancer-avatar">
                    <img src="<?= UsersAvatar::url($v['opponent_id'], $v['avatar'], UsersAvatar::szSmall, $v['sex']); ?>" class="img-circle" alt="<?= tpl::avatarAlt($v); ?>" />
                    <?= tpl::userOnline($v) ?>
                </a>
                <div class="media-body">
                    <div class="o-freelancer-info">
                        <div>
                            <?= tpl::userLink($v, 'strong'); ?>
                        </div>
                        <div>
                            <span class="label label-<?= $aTypes[ $v['type'] ]['cl'] ?>"><?= $aTypes[ $v['type'] ]['t'] ?></span>
                            <? if($ordersOpinions && $v['keyword']):
                                $link = '<a href="'.Orders::url('view', array('id' => $v['order_id'], 'keyword' => $v['keyword'])).'">'.tpl::truncate($v['title'], config::sysAdmin('opinions.list.ajax.title.truncate', 50, TYPE_UINT)).'</a>';
                                $text = $v['author_id'] == $v['performer_id']
                                    ? _t('opinions', 'по заказу [link]', array('link' => $link))
                                    : _t('opinions', 'за выполнение заказа [link]', array('link' => $link));
                                ?>
                                <?= $text ?>
                            <? endif; ?>
                        </div>
                        <span><?= _t('opinions', 'отзыв добавлен'); ?> <?= tpl::dateFormat($v['created']); ?><?
                            if($v['created'] != $v['modified']): ?>, <?= _t('opinions', 'отредактирован'); ?> <?= tpl::dateFormat($v['modified']); ?><? endif;
                            ?></span>
                    </div>

                    <div class="f-infoText p-infoText">
                        <? if($isAuthor && Opinions::premoderation() && ! $v['moderated']): ?><div class="alert alert-warning" role="alert"><?= _t('opinions', 'Ожидает проверки модератора') ?></div><? endif; ?>

                        <div class="j-opinion">
                            <?= nl2br($v['message']); ?>
                            <? if($isAuthor): ?>
                                <div class="p-feedback-answer-controls">
                                    <a href="#" class="ajax-link j-opinion-edit" data-id="<?= $id ?>"><i class="fa fa-edit"></i> <span><?= _t('form', 'Редактировать'); ?></span></a>
                                    <a href="#" class="ajax-link link-delete j-opinion-delete" data-id="<?= $id ?>"><i class="fa fa-times"></i> <span><?= _t('form', 'Удалить'); ?></span></a>
                                </div>
                            <? endif; ?>
                        </div>

                        <? if($isAuthor): ?>
                        <div class="j-my-opinion-edit-<?= $id ?>" style="display:none;">
                            <form action="" method="post">
                                <input type="hidden" name="id" value="<?= $id ?>" />
                                <div class="form-group j-required">
                                    <textarea name="message" class="form-control" rows="5"><?= $v['message'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <? foreach(Opinions::aTypes() as $vv): ?>
                                        <label class="radio-inline radio-inline-sm radio-<?= $vv['cf'] ?>">
                                            <input type="radio" name="type" value="<?= $vv['id'] ?>" <?= $vv['id'] == $v['type'] ? 'checked="checked"' : '' ?> /> <?= $vv['t'] ?>
                                        </label>
                                    <? endforeach; ?>
                                </div>
                                <div class="c-formSubmit">
                                    <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                    <a href="#" class="ajax-link c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                                </div>
                            </form>
                        </div>
                        <? endif; ?>

                        <? if(User::isCurrent($v['user_id'])):
                            if( ! empty($v['answer'])): ?>
                                <div class="p-feedback-answer">
                                    <div class="p-feedback-answer-heading"><?= _t('opinions', 'Ваш ответ:'); ?></div>

                                    <div class="j-answer">
                                        <?= nl2br($v['answer']); ?>
                                        <div class="p-feedback-answer-controls">
                                            <a href="#" class="ajax-link j-answer-edit" data-id="<?= $id ?>"><i class="fa fa-edit"></i> <span><?= _t('form', 'Редактировать'); ?></span></a>
                                            <a href="#" class="ajax-link link-delete j-answer-delete" data-id="<?= $id ?>"><i class="fa fa-times"></i> <span><?= _t('form', 'Удалить'); ?></span></a>
                                        </div>
                                    </div>

                                    <div class="j-answer-edit-<?= $id ?>" style="display:none;">
                                        <form role="form" method="post" action="">
                                            <input type="hidden" name="id" value="<?= $id ?>" />
                                            <div class="form-group j-required">
                                                <textarea class="form-control" name="answer" rows="5"><?= $v['answer'] ?></textarea>
                                            </div>
                                            <div class="c-formSubmit">
                                                <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                                <a href="#" class="ajax-link c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <? else: ?>
                                <div class="mrgt10">
                                    <a href="#j-answer-add-<?= $id ?>" data-toggle="collapse" class="ajax-link j-answer-add" data-id="<?= $id ?>"><i class="fa fa-comments-o"></i> <span><?= _t('opinions', 'Ответить'); ?></span></a>
                                </div>
                                <div class="collapse mrgt10" id="j-answer-add-<?= $id ?>">
                                    <div class="p-add-comment">
                                        <form role="form" method="post" action="">
                                            <input type="hidden" name="id" value="<?= $id ?>" />
                                            <div class="form-group j-required">
                                                <textarea rows="4" name="answer" class="form-control" placeholder="<?= _t('', 'Комментарий'); ?>"></textarea>
                                            </div>
                                            <div class="c-formSubmit">
                                                <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('opinions', 'Ответить'); ?></button>
                                                <a href="#j-answer-add-<?= $id ?>" data-toggle="collapse" class="ajax-link c-formCancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <? endif;
                        else:
                            if( ! empty($v['answer'])): ?>
                            <div class="p-feedback-answer">
                                <div class="p-feedback-answer-heading"><?= _t('opinions', 'Ответ'); ?> <?= $aUserTypes[ $v['user_type'] ]['genitivus'] ?>:</div>
                                <?= nl2br($v['answer']); ?>
                            </div>
                            <? endif;
                        endif; ?>
                    </div>
                </div>
            </li>
            <? endforeach; ?>
        </ul>
<? else: ?>
        <div class="alert alert-info">
            <?= _t('opinions', 'Отзывы не найдены'); ?>
        </div>
<? endif;