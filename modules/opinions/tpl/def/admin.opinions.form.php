<?php
/**
 * @var $this Opinions
 */
$edit = ! empty($id);
?>
<form name="OpinionsOpinionsForm" id="OpinionsOpinionsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <table class="admtbl tbledit">
        <tr>
            <td class="row1" width="100"><span class="field-title">Текст:</span></td>
            <td class="row2">
                <textarea class="stretch" id="opinion-message" name="message"><?= HTML::escape($message) ?></textarea>
            </td>
        </tr>
        <tr>
            <td class="row1"><span class="field-title">Ответ:</span></td>
            <td class="row2">
                <textarea class="short" id="opinion-answer" name="answer"><?= HTML::escape($answer) ?></textarea>
            </td>
        </tr>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jOpinionsOpinionsForm.save(false);" />
                <? if ($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jOpinionsOpinionsForm.save(true);" /><? } ?>
                <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jOpinionsOpinionsFormManager.action('cancel');" />
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    var jOpinionsOpinionsForm =
        (function(){
            var $progress, $form, formChk, id = parseInt(<?= $id ?>);
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#OpinionsOpinionsFormProgress');
                $form = $('#OpinionsOpinionsForm');

            });
            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('Запись успешно удалена');
                                jOpinionsOpinionsFormManager.action('cancel');
                                jOpinionsOpinionsList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    if( ! formChk.check(true) ) return;
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                            if(returnToList || ! id) {
                                jOpinionsOpinionsFormManager.action('cancel');
                                jOpinionsOpinionsList.refresh( ! id);
                            }
                        }
                    }, $progress);
                },
                onShow: function ()
                {
                    formChk = new bff.formChecker($form);
                }
            };
        }());
</script>