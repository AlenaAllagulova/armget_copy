<?php
/**
 * @var $this Opinions
 */
tpl::includeJS(array('datepicker'), true);
tplAdmin::adminPageSettings(array(
    'fordev' => array(
        array('title'=>_t('', 'пересчет счетчиков отзывов пользователей'), 'onclick'=>"return bff.confirm('sure', {r:'".$this->adminLink(bff::$event.'&act=dev-users-counters-update')."'})", 'icon'=>'icon-refresh', 'debug-only'=>true),
    )
));
$aTabs = array(
    0 => array('t' => _t('orders', 'На модерации'), 'counter'=>config::get('opinions_items_moderating', 0)),
    1 => array('t' => _t('orders', 'Все')),
);

?>
    <form action="" name="filter" class="form-inline" id="j-opinions-filter">

    <div class="tabsBar" id="j-opinions-tabs">
        <?php foreach($aTabs as $k=>$v) { ?>
            <span class="tab<?= $k== $f['tab'] ? ' tab-active' : '' ?>"><a href="#" class="j-tab" data-id="<?= $k ?>"><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
        <?php } ?>
        <label class="pull-right"><?= _t('', 'по') ?><select name="perpage" class="j-perpage" style="width: 45px; height: 20px !important; border:0;"><?= $perpage ?></select></label>
    </div>

    <div class="actionBar">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>" class="j-tab-id" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <div class="controls controls-row">
            <select name="type" style="width: 100px;" class="j-change"><?= HTML::selectOptions(Opinions::aTypes(), $f['type'], _t('opinions', '- Тип -'), 'id', 'plural') ?></select>
            <input type="text" name="author" value="<?= $f['author'] ?>"  placeholder="От кого" style="width: 115px;" title="ID / логин / e-mail" />
            <input type="text" name="user" value="<?= $f['user'] ?>"  placeholder="Кому" style="width: 115px;" title="ID / логин / e-mail" />
            <input type="text" name="fr" value="<?= $f['fr'] ?>"  placeholder="добавлен: от" style="width: 90px;" class="bff-datepicker"  />
            <input type="text" name="to" value="<?= $f['to'] ?>"  placeholder="добавлен: до" style="width: 90px;" class="bff-datepicker"  />
            <label class="checkbox"><input type="checkbox" name="ans" value="1" class="j-change" <?= $f['ans'] ? 'checked="checked"' : '' ?>/> С ответами</label>
            <input type="submit" value="<?= _t('', 'search') ?>" class="btn btn-small button submit" />
            <a class="cancel" id="j-filter-cancel"><?= _t('', 'reset') ?></a>
            <div class="clearfix"></div>
        </div>
    </div>

    </form>

<?= $this->viewPHP($aData, 'admin.opinions.listing.ajax'); ?>

<?= $pgn; ?>

<script type="text/javascript">
    $(function(){
        var $filter = $('#j-opinions-filter');
        bff.datepicker('.bff-datepicker', {yearRange: '-3:+3'});

        $('#j-filter-cancel').on('click', function(e){ nothing(e);
            var filter = $filter.get(0);
            filter.elements.page.value = 1;
            filter.elements.fr.value = '';
            filter.elements.to.value = '';
            filter.elements.user.value = '';
            filter.elements.author.value = '';
            filter.elements.type.value = 0;
            $(filter.elements.ans).prop('checked', false);
            filter.submit();
        });

        $('.j-change').change(function(){
            $filter.get(0).elements.page.value = 1;
            $filter.submit();
        });
    });
</script>
