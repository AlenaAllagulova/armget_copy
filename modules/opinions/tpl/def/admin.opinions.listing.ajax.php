<?php
/**
 * @var $this Opinions
 */
tpl::includeJS('comments', true);
$aAdminTypes = array(
    Opinions::TYPE_NEGATIVE => 'opinion2',
    Opinions::TYPE_POSITIVE => 'opinion3',
    Opinions::TYPE_NEUTRAL  => 'opinion1',
);
?>
<div class="comments">
    <?
    foreach($opinions as $v):
        ?>
        <div class="comment" id="j-opinion-<?= $v['id'] ?>">
            <a name="j-opinion-<?= $v['id'] ?>" ></a>
            <div class="ccontent">
                <div class="tb"><div class="tl"><div class="tr"></div></div></div>
                <div class="ctext">
                    <div class="j-opinion-view"><?= nl2br($v['message']) ?></div>
                    <? if( ! empty($v['answer'])): ?><div class="answer"><?= nl2br($v['answer']) ?></div><? endif; ?>
                </div>
                <div class="bl"><div class="bb"><div class="br"></div></div></div>
            </div>
            <div class="info" style="margin:0;">
                <ul>
                    <li><p><a href="#" onclick="return bff.userinfo(<?= $v['author_id'] ?>);" class="userlink author<? if($v['author_blocked']){ ?> blocked<? } ?>"><?= ( ! empty($v['author_name']) ? $v['author_name'] : $v['author_login'] ) ?> (<?= $v['author_email'] ?>)</a></p></li>
                    <li><a class="<?= $aAdminTypes[ $v['type'] ] ?>" onclick="return false;"></a></li>
                    <li class="date"><?= tpl::date_format2($v['created'], true); ?></li>
                    <? if($v['moderated'] == 2){ ?><li><i class="icon-pencil disabled" title="отредактирован пользователем"></i></li><? } ?>
                    <? if($v['moderated'] != 1){ ?><li><a href="#" class="ajax text-success" onclick="jOpinions.approve(<?= $v['id'] ?>, this); return false;"><?= _t('', 'Approve') ?></a></li><? } ?>
                    <li><a href="#" class="text-error delete ajax" onclick="jOpinions.del(<?= $v['id']?>); return false;"><?= _t('', 'Delete') ?></a></li>
                    <li><p><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink <? if($v['user_blocked']){ ?> blocked<? } ?>"><?= ( ! empty($v['user_name']) ? $v['user_name'] : $v['user_login'] ) ?> (<?= $v['user_email'] ?>)</a></p></li>
                    <? if($v['order_id']): ?><li><a href="#" onclick="return false;" class="nolink bold">заказ #<?= $v['order_id'] ?></a></li><? endif; ?>
                </ul>
            </div>
        </div>
    <? endforeach;
    if(empty($opinions)): ?>
        <div class="alignCenter valignMiddle" style="height:30px; padding-top:15px;">
            <span class="desc"><?= _t('opinions', 'нет отзывов') ?></span>
        </div>
    <? endif; ?>
</div>
<script type="text/javascript">
    var jOpinions = (function() {
        var $filter, processing = false;
        var ajax_url = '<?= $this->adminLink('listing&act='); ?>';

        $(function(){
            $filter = $('#j-opinions-filter');

            if($filter && $filter.length) {
                $filter.find('.j-perpage').on('change', function(){
                    $filter.submit();
                });
                $('#j-opinions-tabs').on('click', '.j-tab', function(e){ nothing(e);
                    $filter.find('.j-tab-id').val( $(this).data('id') );
                    $filter.submit();
                });
            }
        });

        return {
            del: function(id)
            {
                if( ! bff.confirm('sure')) return;
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'delete', {id: id}, function(data) {
                    if(data) {
                        $('#j-opinion-'+id).slideUp();
                        location.reload();
                    }
                    processing = false;
                });
            },
            approve: function(id, link)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'approve', {id: id}, function(data) {
                    if(data) {
                        $(link).remove();
                        location.reload();
                    }
                    processing = false;
                });
            }
        };
    }());
</script>