<?php

use bff\files\Attachment;

class UsersResumeAttachment_ extends Attachment
{
    public function getUrl()
    {
        return bff::url('resume');
    }

    public function deleteOld($nUserID)
    {
        $aOldResume = Users::model()->userData($nUserID, array('resume_file'));
        if($aOldResume['resume_file']){
            $data = explode(';', strval($aOldResume['resume_file']), 4);
            if( ! empty($data['0'])){
                $sFileName = $this->path.'/'.$data['0'];
                if(file_exists($sFileName)){
                    @unlink($sFileName);
                }
            }
        }
    }

    /**
     * Формирование ссылки вложения
     * @param string $attachData данные о вложении, в формате "fileName;fileSize;extension;realFilename"
     * @param boolean $targetBlank открывать в новой вкладке
     * @param array $linkAttr дополнительные атрибуты ссылки
     * @param string $sFileTitle название файла
     * @return string
     */
    public function getAttachLink($attachData, $targetBlank = true, array $linkAttr = array(), & $sFileTitle = '')
    {
        if (empty($attachData)) {
            return '';
        }

        $data = explode(';', strval($attachData), 4);
        if (empty($data) || sizeof($data) < 3) {
            return '';
        }

        $linkAttr['href'] = $this->getUrl() . $data[0];
        if ($targetBlank) {
            $linkAttr['target'] = '_blank';
        }
        $linkAttr['class'] = 'p-profile-resume-download';
        $sFileTitle = !empty($data[3]) ? $data[3] : $data[0];

        return '<a' . HTML::attributes($linkAttr) . '>' .
        (!empty($data[3]) ? _t('','Скачать').' '.HTML::escape($data[3]) : $data[0]) .
        ' <small>(' . tpl::filesize($data[1]) . ')</small> <i class="fa fa-download pull-right"></i>'.
        '</a>';
    }
}