<?php

class UsersTags_ extends bff\db\Tags
{
    protected function initSettings()
    {
        $this->tblTags = TABLE_TAGS;
        $this->postModeration = true;
        $this->lang = array(
            'list'         => _t('','Список навыков'),
            'add_title'    => _t('','Добавление навыка'),
            'add_text'     => _t('','Введите навыки, каждый с новой строки'),
            'edit'         => _t('','Редактирование навыка'),
            'replace_text' => _t('','Введите название навыка для замены'),
        );
        $this->tblTagsIn = TABLE_USERS_IN_TAGS;
        $this->tblTagsIn_ItemID = 'user_id';
        $this->urlItemsListing = $this->adminLink('listing&tag=', 'users');
    }

    public function formFront($nItemID, $aParams = array())
    {
        $aData = array(
            'tags'   => ($nItemID > 0 ? $this->tagsGet($nItemID) : array()),
            'params' => $aParams,
        );

        return $this->viewPHP($aData, 'settings.tags');
    }
}