<?php

class UsersVerifiedImages_ extends CImagesUploaderTable
{
    /**
     * Константы размеров
     */
    const szOrginal = 'o'; # original - оригинальное изображение

    static function defaultUrl($sSizePrefix)
    {
        static $i;
        if( ! isset($i)) {
            $i = new UsersVerifiedImages();
        }

        return $i->url.'default-'.$sSizePrefix.'.png';
    }

    function initSettings()
    {
        $this->path    = PATH_BASE . 'files' . DS . 'verified'. DS;
        $this->pathTmp = bff::path('tmp', 'images');
        $this->url     = bff::url('', 'images');
        $this->urlTmp  = bff::url('tmp', 'images');

        $this->tableRecords = TABLE_USERS;
        $this->tableImages  = TABLE_USERS_VERIFIED_FILES;

        $this->folderByID = config::sysAdmin('users.verified.images.folderbyid', true, TYPE_BOOL); # раскладываем файлы изображений по папкам (1+=>0, 1000+=>1, ...)
        $this->filenameLetters = config::sysAdmin('users.verified.images.filename.letters', 8, TYPE_UINT); # кол-во символов в названии файла
        $this->limit = Users::verifiedFilesLimit(); # лимит изображений
        $this->maxSize = config::sysAdmin('users.verified.images.max.size', 5242880, TYPE_UINT); # 2мб (2мб: 2097152, 5мб: 5242880)

        $this->tableRecords_id = 'user_id';
        $this->tableRecords_cnt = 'verified_images';
        $this->useFav = false;

        $this->minWidth = config::sysAdmin('users.verified.images.min.width', 250, TYPE_UINT);
        $this->minHeight = config::sysAdmin('users.verified.images.min.height', 250, TYPE_UINT);
        $this->maxWidth = config::sysAdmin('users.verified.images.max.width', 6000, TYPE_UINT);
        $this->maxHeight = config::sysAdmin('users.verified.images.max.height', 6000, TYPE_UINT);

        # размеры изображений
        $this->sizes = bff::filter('users.verified.images.sizes', array(
            self::szOrginal => array('o' => true),
        ));
    }

    /**
     * Просмотр файла. Отдадим контент файла на stdout
     * @param array $data данные файла
     */
    public function show($data)
    {
        if (empty($data['filename'])) return;
        $file = $this->getPath($data, static::szOrginal);
        if (file_exists($file)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $fmime = finfo_file($finfo, $file);
            finfo_close($finfo);

            header('Content-Type: '.$fmime);
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    /**
     * Удаление старых документов для проверки
     */
    public function deleteExpire()
    {
        if (!Users::verifiedEnabled()) return;

        $days = config::sysAdmin('users.verified.files.expire', 0, TYPE_UINT);  # 0 - никогда не удалять
        if ($days <= 0) return;

        $expire = date('Y-m-d H:i:s', strtotime('-'.$days.' days'));
        $this->db->select_iterator('SELECT * FROM '.$this->tableImages.' WHERE created < :expire', array(':expire' => $expire),
            function($data){
                $this->setUserID($data['user_id']);
                $this->setRecordID($data[$this->fRecordID]);
                $this->deleteFile($data);
        });
        $this->db->exec('DELETE FROM '.$this->tableImages.' WHERE created < :expire', array(':expire' => $expire));
    }

    public function urlDefault($sSizePrefix)
    {
        return '';
    }
}