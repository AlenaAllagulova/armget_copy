<?php
    /**
     * @var $this Users
     * @var $svc array
     */
    tpl::includeJS(array('wysiwyg','autocomplete','ui.sortable'), true);
    tplAdmin::adminPageSettings(array('icon'=>false));
    $saveUrl = $this->adminLink('svc_services&act=');
    $aData['pricePrefix'] = $pricePrefix = '&nbsp;<span class="desc">'.Site::currencyDefault().'</span>';
?>

<div class="tabsBar">
    <form id="j-services-tabs" action="">
    <? $tabActiveKey = $this->input->get('tab', TYPE_NOTAGS);
       $i = 0; foreach($svc as $k=>&$v)
       {
          $v['active'] = ( ! empty($tabActiveKey) ? $k == $tabActiveKey : !$i++ );
    ?>
        <div class="left">
            <span style="margin: 0 2px;" class="tab<? if( ! $v['on'] ) { ?> desc<? } if( $v['active'] ){ ?> tab-active<? } ?>"<? if(FORDEV){ ?> title="<?= $v['id'].':'.$k ?>"<? } ?> onclick="return jSvcServices.onTab('<?= $k ?>', this);"><?= $v['title'] ?></span>
            <input type="hidden" name="svc[<?= $v['id'] ?>]" value="<?= $v['id'] ?>" />
        </div>
    <? } unset($v); ?>
    </form>
    <div class="progress" style="display:none; margin-left:5px;" id="j-services-progress"></div>
</div>

<script type="text/javascript">
var jSvcServices = (function(){
    var urlAjax = '<?= $saveUrl; ?>';
    var $tabs, $progress;

    $(function(){
        $tabs = $('#j-services-tabs-content').find('.j-tab-content');
        $progress = $('#j-services-progress');
        $('textarea.wy').bffWysiwyg({autogrow: false});

        var $svcOrder = $('#j-services-tabs').sortable({
            update: function(event, ui) {
                bff.ajax(urlAjax+'reorder', $svcOrder.serialize(), function(data) {
                    if(data && data.success) {
                        bff.success('Порядок услуг был успешно изменен');
                    }
                }, $progress);
            }
        });
        $svcOrder.sortable('refresh');

        $('.j-svc-service-form').each(function(){
            var $form = $(this);
            bff.iframeSubmit($form, function(data){
                if(data && data.success) {
                    bff.success('Настройки успешно сохранены');
                    setTimeout(function(){ location.reload(); }, 1000);
                }
            });
        });
    });

    return {
        onTab: function(key,link)
        {
            $tabs.addClass('hidden');
            $tabs.filter('#j-services-'+key).removeClass('hidden');
            $(link).addClass('tab-active').parent().siblings().find('.tab').removeClass('tab-active');
            if( bff.h ) {
                window.history.pushState({}, document.title, '<?= $this->adminLink('svc_services&tab=') ?>'+key);
            }
            return false;
        },
        iconDelete: function(link)
        {
            var $block = $(link).parent();
            $block.hide().find('input.del-icon').val(1);
            $block.prev().show();
            return false;
        }
    };
}());
</script>

<div id="j-services-tabs-content">
    <? foreach($svc as $k=>$v)
    {
        $ID = $v['id'];
    ?>
    <div id="j-services-<?= $k ?>" class="j-tab-content<? if( ! $v['active'] ){ ?> hidden"<? } ?>">
        <form action="<?= $saveUrl.'update' ?>" class="j-svc-service-form" id="j-services-form-<?= $k ?>" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $ID ?>" />
            <table class="admtbl tbledit">
                <?= $this->locale->buildForm($v, 'users-svc-'.$ID,'
                <tr>
                    <td class="row1 field-title" width="150">Название:</td>
                    <td class="row2">
                        <input type="text" name="title_view[<?= $key ?>]" value="<?= (isset($aData[\'title_view\'][$key]) ? HTML::escape($aData[\'title_view\'][$key]) : \'\') ?>" class="stretch lang-field" />
                    </td>
                </tr>
                <tr>
                    <td class="row1 field-title">Описание<br />(краткое):</td>
                    <td class="row2">
                        <?= tpl::jwysiwyg((isset($aData[\'description\'][$key]) ? $aData[\'description\'][$key] : \'\'), \'description-\'.$key.$aData[\'id\'].\',description[\'.$key.\']\', 0, 100); ?>
                    </td>
                </tr>
                <tr>
                    <td class="row1 field-title">Описание<br />(подробное):</td>
                    <td class="row2">
                        <?= tpl::jwysiwyg((isset($aData[\'description_full\'][$key]) ? $aData[\'description_full\'][$key] : \'\'), \'description_full-\'.$key.$aData[\'id\'].\',description_full[\'.$key.\']\', 0, 130); ?>
                    </td>
                </tr>
                '); ?>
                <? $aData['v'] = $v; ?>
                <?= $this->viewPHP($aData, 'admin.svc.service.'.$k); ?>
                <?  $oIcon = Users::svcIcon($ID);
                    foreach($oIcon->getVariants() as $iconField=>$icon) {
                        $oIcon->setVariant($iconField);
                        $icon['uploaded'] = ! empty($v[$iconField]);
                    ?>
                    <tr>
                        <td class="row1">
                            <span class="field-title"><?= $icon['title'] ?></span>:<? if(sizeof($icon['sizes']) == 1) { $sz = current($icon['sizes']); ?><br /><span class="desc"><?= ($sz['width'].'x'.$sz['height']) ?></span><? } ?>
                        </td>
                        <td class="row2">
                            <input type="file" name="<?= $iconField ?>" <? if($icon['uploaded']){ ?>style="display:none;" <? } ?> />
                            <? if($icon['uploaded']) { ?>
                                <div style="margin:5px 0;">
                                    <input type="hidden" name="<?= $iconField ?>_del" class="del-icon" value="0" />
                                    <img src="<?= $oIcon->url($ID, $v[$iconField], $icon['key']) ?>" alt="" /><br />
                                    <a href="#" class="ajax desc cross but-text" onclick="return jSvcServices.iconDelete(this);">удалить</a>
                                </div>
                            <? } ?>
                        </td>
                    </tr>
                    <? }
                ?>
                <tr>
                    <td class="row1 field-title"><?= _t('', 'Enabled') ?>:</td>
                    <td class="row2">
                        <input type="checkbox" name="on" <? if($v['on']){ ?>checked="checked"<? } ?> />
                    </td>
                </tr>
                <tr><td colspan="2"><hr class="cut" /></td></tr>
                <tr>
                    <td class="footer" colspan="2">
                        <div class="left"><input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" /></div>
                        <div class="right desc">
                            последние изменения: <span class="j-last-modified"><?= tpl::date_format2($v['modified'], true); ?>, <a class="bold desc ajax" href="#" onclick="return bff.userinfo(<?= $v['modified_uid'] ?>);"><?= $v['modified_login'] ?></a></span>
                        </div>
                        <div class="clear"></div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <? } ?>
</div>
<?