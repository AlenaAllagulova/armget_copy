<?php
/**
 * @var $it array
 */
    $days = strtotime($it['expire']) - time();
    $days = round($days / 86400);
?>
<div class="se-service-section j-stairs" data-sum="0">
    <input type="hidden" name="spec[<?= $i ?>]" class="j-spec" value="<?= $it['spec_id'] ?>" />
    <input type="hidden" name="cat[<?= $i ?>]"  class="j-cat" value="<?= $it['cat_id'] ?>" />

    <h5><?= $it['spec_title'] ?></h5>
    <div class="clearfix"></div>
    <table class="table se-service-section-table">
        <tr>
            <td><?= _t('svc', 'Дата оплаты:'); ?></td>
            <td><?= tpl::date_format2($it['payed'], true) ?></td>
        </tr>
        <tr>
            <td><?= _t('svc', 'Срок окончание:'); ?></td>
            <td><?= tpl::date_format2($it['expire'], true) ?></td>
        </tr>
        <tr>
            <td><?= _t('svc', 'Осталось:'); ?></td>
            <td><strong class="text-success"><?= tpl::declension($days, _t('', 'день;дня;дней'), true); ?></strong></td>
        </tr>
    </table>
    <table class="table se-service-section-table">
        <tr>
            <td><?= _t('svc', 'Продлить на:'); ?></td>
            <td>
                <div class="se-service-section-input">

                    <div class="input-group">
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-default btn-sm j-minus" disabled="disabled">
                                  <i class="fa fa-minus"></i>
                              </button>
                          </span>
                        <input type="text" name="weeks[<?= $i ?>]"  class="form-control input-sm j-weeks" value="0" min="1" max="10" />
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-default btn-sm j-plus">
                                  <i class="fa fa-plus"></i>
                              </button>
                          </span>
                    </div>
                    <?= _t('svc', 'недель'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td><?= _t('svc', 'Сумма:'); ?></td>
            <td><span><strong class="j-sum">0 <?= $cur ?></strong></span></td>
        </tr>
    </table>
</div>