<?php
?>
<div class="p-additional-info j-spec-dp">
    <a href="#j-additional-info-<?= $prefix ?>" data-toggle="collapse" data-parent="#accordion" class="ajax-link"><i class="fa fa-cog"></i> <span><?= _t('users', 'Дополнительные параметры'); ?></span></a>

    <div class="collapse" id="j-additional-info-<?= $prefix ?>">

        <div class="p-additional-info-inside">
            <?= $pr ?>
            <?= $dp ?>
        </div>
    </div>
</div>