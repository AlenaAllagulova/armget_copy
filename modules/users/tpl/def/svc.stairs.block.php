<?php
$nUserID = User::id();
$bAdd = true;

if (empty($list) && ( ! $nUserID || ! User::isWorker())) return;
?>
    <h5 class="mrgb5"><?= _t('svc', 'Предложения исполнителей'); ?></h5>

    <ul class="l-leftCol-freelancers media-list" id="j-svc-stairs-block">

        <? foreach($list as $v): $bEdit = $v['user_id'] == $nUserID; ?>
            <li class="media">
                <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="pull-left">
                    <?= tpl::userAvatar($v) ?>
                </a>
                <div class="media-body <?= $v['portfolio'] ? 'dropdown' : '' ?>">
                    <?= tpl::userLink($v, 'no-login') ?>
                    <div class="j-stairs">
                        <strong<?= $bEdit ? ' class="j-title"' : '' ?>><?= tpl::truncate($v['title'], config::sysAdmin('users.svc.stairs.block.title.truncate', 40, TYPE_UINT)) ?></strong>
                        <p<?= $bEdit ? ' class="j-message"' : '' ?>><?= nl2br(tpl::truncate($v['message'], config::sysAdmin('users.svc.stairs.block.message.truncate', 80, TYPE_UINT))) ?></p>
                        <? if($bEdit): ?><a href="#" class="ajax-link mrgr10 j-edit-stairs"><span><?= _t('', 'Редактировать'); ?></span></a><? endif; ?>
                        <? if( ! empty($v['portfolio_tab'])):
                            if( $v['portfolio']): ?>
                            <a href="#" class="dropdown-toggle ajax-link" data-toggle="dropdown"><span><?= _t('', 'Подробнее'); ?></span> <i class="fa fa-caret-right"></i></a>
                            <div class="dropdown-menu l-leftCol-dropdown<?= $bEdit ? ' j-stairs-descr' : '' ?>">
                                <? if($v['description']): ?>
                                    <div<?= $bEdit ? ' class="j-edit-stairs-descr-bl"': '' ?>>
                                        <strong<?= $bEdit ? ' class="j-description"': '' ?>><?= nl2br(tpl::truncate($v['description'], config::sysAdmin('users.svc.stairs.block.description.truncate', 400, TYPE_UINT))) ?></strong>
                                        <? if($bEdit):?><a href="#" class="ajax-link j-edit-stairs-descr"><span><?= _t('', 'Редактировать'); ?></span></a><? endif; ?>
                                    </div>
                                <? endif; ?>
                                <? if($bEdit):?>
                                <div class="hidden j-edit-stairs-descr-form">
                                    <form method="post" action="">
                                        <input type="hidden" name="id" value="<?= $v['id'] ?>" />
                                        <div class="mrgb10">
                                            <textarea rows="2" class="form-control" name="description"><?= $v['description'] ?></textarea>
                                        </div>
                                        <div class="help-block j-description-counter"></div>
                                        <button type="submit" class="btn btn-sm btn-success j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                        <button type="button" class="btn btn-sm btn-default j-cancel"><?= _t('form', 'Отмена'); ?></button>
                                    </form>
                                </div>
                                <? endif; ?>
                                <? if( ! empty($v['portfolio_works'])): $v['portfolio_works'] = func::unserialize($v['portfolio_works']);
                                    if( ! empty($v['portfolio_works'])): ?>
                                        <div class="o-propose-works">
                                            <? foreach($v['portfolio_works'] as $vv): ?>
                                                <a href="<?= Portfolio::url('user.view', array('login' => $v['login'], 'id' => $vv['id'], 'keyword' => $vv['keyword'])) ?>" class="o-project-thumb_proposal">
                                                    <div class="o-inner added">
                                                        <img src="<?= PortfolioItemPreview::url($vv['id'], $vv['preview']); ?>" alt="<?= ! empty($vv['title']) ? tpl::imageAlt(array('t' => $vv['title'])) : '' ?>" />
                                                    </div>
                                                </a>
                                            <? endforeach; ?>
                                            <? for($i = count($v['portfolio_works']); $i < 3; $i++): ?>
                                                <div class="o-project-thumb_proposal">
                                                    <div class="o-inner added">
                                                        <img src="<?= PortfolioItemPreview::url(0, '') ?>" alt="" />
                                                    </div>
                                                </div>
                                            <? endfor; ?>
                                            <div class="clearfix"></div>
                                        </div>
                                <? endif; endif;?>
                                <div class="mrgt15">
                                    <a href="<?= Users::url('profile', array('login' => $v['login'], 'tab' => 'portfolio')); ?>"><?= _t('users', 'Все [n_works] пользователя', array('n_works' => $v['portfolio'] > 2 ? tpl::declension($v['portfolio'], _t('','работа;работы;работ')) : _t('','работы')))?></a>
                                </div>
                            </div>
                        <? else: ?>
                            <a href="<?= Users::url('profile', array('login' => $v['login'], 'tab' => 'portfolio')); ?>"><?= _t('svc', 'Подробнее'); ?></a>
                        <? endif; endif; ?>
                        <? if($v['user_id'] == $nUserID): $bAdd = false; ?><br /><a href="<?= Svc::url('view', array('keyword' => 'stairs', 'up' => 1, 'cat_id' => $v['cat_id'], 'spec_id' => $v['spec_id'])) ?>"><?= _t('svc', 'Изменить положение'); ?></a>
                        <? endif; ?>
                    </div>
                    <? if($bEdit): tpl::includeJS('svc');?>
                        <div class="j-stairs-edit-form hidden">
                            <form method="post" action="">
                                <input type="hidden" name="id" value="<?= $v['id'] ?>" />
                                <div class="mrgb10">
                                    <input type="text" name="title" class="form-control" value="<?= HTML::escape($v['title']) ?>" maxlength="40"/>
                                </div>
                                <div class="mrgb10">
                                    <textarea rows="2" class="form-control" name="message"><?= HTML::escape($v['message']) ?></textarea>
                                </div>
                                <div class="help-block j-message-counter"></div>
                                <button type="submit" class="btn btn-sm btn-success j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                <button type="button" class="btn btn-sm btn-default j-cancel"><?= _t('form', 'Отмена'); ?></button>
                            </form>
                        </div>
                        <script type="text/javascript">
                            <? js::start(); ?>
                            jSvcStairsEdit.init(<?= func::php2js(array(
                                    'lang' => array(
                                        'maxlength_left' => _t('users','[symbols] осталось'),
                                        'maxlength_symbols' => explode(';', _t('users', 'знак;знака;знаков')),
                                    ),
                                    'message_length' => 80,
                                    'descr_length'   => 400,
                                )) ?>);
                            <? js::stop();?>
                        </script>
                    <? endif; ?>
                </div>
            </li>
        <? endforeach; ?>
        <? if($bAdd && User::id() && (Users::useClient() && User::isWorker() || ! Users::useClient())): ?>
        <? $url = Svc::url('view', array('keyword' => 'stairs', 'cat_id' => $cat_id, 'spec_id' => $spec_id)); ?>
        <li class="media">
            <a href="<?= $url ?>" class="pull-left">
                <img src="<?= UsersAvatar::url(0, '', UsersAvatar::szSmall); ?>" class="img-circle" alt="<?= tpl::avatarAlt(); ?>" />
            </a>
            <div class="media-body l-leftCol-wannahere">
                <a href="<?= $url ?>"><?= _t('svc', 'Как сюда попасть?'); ?></a>
            </div>
            <div class="clearfix"></div>
        </li>
        <? endif ; ?>
    </ul>