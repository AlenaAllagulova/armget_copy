<?php
    tpl::includeJS(array('autocomplete'), true);
    $lng_all = _t('', 'Все');
    $lng_back = _t('form', 'Вернуться назад');
    $bShowRegion = Users::profileMap() && ! $bPriceAdmin && ! Geo::filterEnabled();
?>
<button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#j-users-price-form-block"><i class="fa fa-cog pull-right"></i><?= _t('', 'Фильтр'); ?></button>
<div class="l-leftColumn l-borderedBlock collapse" id="j-users-price-form-block">
    <form action="" method="get">
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />

        <a href="#j-users-specs" data-toggle="collapse" data-parent="#accordion" class="h6 active"><i class="fa fa-chevron-down pull-right"></i><?= _t('users', 'Специализации'); ?></a>
        <div class="collapse in j-collapse" id="j-users-specs">
        <? if (Specializations::catsOn()): ?>
            <ul class="l-left-categories l-inside visible-md visible-lg">
                <? foreach($specs as &$v): if(empty($v['specs'])) continue; ?>
                    <li<? if($v['a']){ ?> class="opened"<? } ?>><a href="<?= $v['url'] ?>" class="j-cat-title"><i class="fa fa-caret-<?= $v['a'] ? 'down' : 'right'?>"></i> <?= $v['title'] ?></a>
                        <ul<? if(!$v['a']){ ?> class="hidden"<? } ?>>
                            <li<? if($v['a'] && ! $spec_id){ ?> class="checked"<? } ?>><a href="<?= $v['url'] ?>"><?= $lng_all; ?></a></li>
                            <? foreach($v['specs'] as &$vv): ?>
                                <li<? if($vv['a']){ ?> class="checked"<? } ?>><a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a></li>
                            <? endforeach; unset($vv); ?>
                        </ul>
                    </li>
                <? endforeach; unset($v); ?>
            </ul>

            <div class="collapse in" id="j-mobile-cats">
                <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                    <? foreach($specs as &$v): ?>
                        <li><a href="<?= $v['url'] ?>" class="j-mobile-cat" data-id="<?= $v['id']; ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                    <? endforeach; unset($v); ?>
                </ul>
            </div>

            <? foreach($specs as &$v): if(empty($v['specs'])) continue; ?>
                <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                    <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                        <li class="active">
                            <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= $lng_back; ?></span><br>
                                <?= $v['title'] ?>
                            </a>
                            <ul>
                                <li<? if($v['a'] && ! $spec_id){ ?> class="checked"<? } ?>>
                                    <a href="<?= $v['url'] ?>"><?= $lng_all; ?></a>
                                </li>
                                <? foreach($v['specs'] as &$vv): ?>
                                    <li<? if($vv['a']){ ?> class="checked"<? } ?>>
                                        <a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a>
                                    </li>
                                <? endforeach; unset($vv); ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            <? endforeach; unset($v); ?>

        <? else: ?>

            <ul class="l-left-categories l-inside visible-md visible-lg">
                <? foreach($specs as &$v): ?>
                    <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></li>
                <? endforeach; unset($v); ?>
            </ul>

            <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                <? foreach($specs as &$v): ?>
                    <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></li>
                <? endforeach; unset($v); ?>
            </ul>

        <? endif; ?>
        </div>

        <? if($bShowRegion): ?>
            <? $bCountry = Geo::countrySelect(); $bOpenRegion = $f['ct'] || ($bCountry && $f['c']); ?>
            <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpenRegion ? ' active' : '' ?>"><i class="fa <?= $bOpenRegion ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('users', 'Регион'); ?></a>
            <div class="collapse <?= $bOpenRegion ? 'in' : '' ?> j-collapse" id="j-left-region">
                <div class="l-inside">
                    <div class="form">
                        <? if($bCountry): ?>
                            <div class="form-group">
                                <select name="c" class="form-control" id="j-left-region-country" autocomplete="off"><?= HTML::selectOptions(Geo::countryList(), $f['c'], _t('', 'Все страны'), 'id', 'title') ?></select>
                            </div>
                        <? endif; ?>
                        <div class="relative">
                            <input type="hidden" name="r" value="<?= $f['r'] ?>" id="j-region-pid-value" />
                            <input type="hidden" name="ct" value="<?= $f['ct'] ?>" id="j-region-city-value" />
                            <input type="text" class="form-control input-sm<?= $bCountry && empty($f['c']) ? ' hidden' : '' ?>" id="j-region-city-select" value="<?= $region_title ?>" placeholder="<?= _t('users', 'Или введите название региона'); ?>" />
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>

    </form>
</div>
<?
