<?php
if(empty($attr['value'])) $attr['value'] = '+'.intval($countrySelected['phone_code']);
$code = $countrySelected['country_code'];
?>
<div class="c-phoneSelect j-phone-number">
    <a href="#" class="country-icon" data-type="country-icon" data-default="<?= $countrySelected['country_code'] ?>"><span class="country-icon-<?= $code ?>"></span></a>
    <input type="text" <?= HTML::attributes($attr) ?> pattern="[0-9+]*" maxlength="30" class="form-control j-required j-phone-number-input" data-default="<?= '+'.intval($countrySelected['phone_code']) ?>" />
    <div class="c-phoneDropdown j-phone-number-country-list displaynone">
        <ul>
            <? foreach ($countryList as $v): ?>
                <li<? if ($v['id'] == $countrySelectedID): ?> class="active"<? endif; ?>>
                    <a href="#" data="{id:<?= $v['id'] ?>,cc:'<?= $v['country_code'] ?>',pc:'+<?= intval($v['phone_code']) ?>'}" class="j-country-item"><div class="country-icon"><span class="country-icon country-icon-<?= $v['country_code'] ?>"></span></div> <?= $v['title'] ?> <i>+<?= $v['phone_code'] ?></i></a>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
</div>

