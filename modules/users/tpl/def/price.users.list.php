<?php
if( ! empty($list)): $cnt = 0; ?>
<div class="row">
    <? foreach($list as $v):  ?>
    <? if($cnt && $cnt % 3 == 0): ?>
        </div>
        <div class="row">
    <? endif; $cnt++; ?>
    <div class="col-sm-4">
        <div class="se-price-user">
            <a href="<?= Users::url('profile', array('login' => $v['login'], 'tab' => 'price')); ?>" class="se-price-user-avatar">
                <img src="<?= UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szSmall, $v['sex']); ?>" class="img-circle" alt="" />
            </a>
            <div class="se-price-user-userinfo">
                <div><?= tpl::userLink($v, 'no-login', 'price'); ?></div>
                <div class="o-feedbacks-inf">
                    <?= _t('opinions', 'Opinions:'); ?>
                    <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                </div>
                <? if( ! empty($v['city_data']['title'])): ?>
                    <div>
                        <i class="fa fa-map-marker"></i> <?= $v['city_data']['title'] ?><?= ! empty($v['country_data']['title']) ? ', '.$v['country_data']['title'] : '' ?>
                    </div>
                <? endif; ?>
                <div>
                    <?= _t('users', 'Стоимость:'); ?>
                    <? if($v['price']): ?>
                        <strong><?= tpl::formatPrice($v['price']); ?> <?= Site::currencyData($v['price_curr'],'title_short')?></strong> <span> / <?= $service['measure'] ?></span>
                    <? else: ?>
                        <?= _t('users', 'бесплатно'); ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
    <? endforeach; ?>
</div>
<? else: ?>
    <div class="alert alert-info mrgt20"><?= _t('users', 'Исполнители, отвечающие вашему запросу, не найдены'); ?></div>
<? endif;