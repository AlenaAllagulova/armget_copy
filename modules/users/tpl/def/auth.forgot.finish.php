<?php
/**
* Восстановление пароля. Шаг2: Смена пароля
* @var $this Users
*/
?>
<div class="s-signBlock-form">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="s-signBlock-div">

                <form action="" class="form" role="form" id="j-u-forgot-finish-form">
                    <input type="hidden" name="key" value="<?= HTML::escape($key) ?>" />
                    <input type="hidden" name="social" value="<?= $social ?>" />
                    <div class="form-group">
                        <label for="j-u-forgot-finish-pass"><?= _t('users', 'Новый пароль') ?> <i class="text-danger">*</i></label>
                        <input type="password" name="pass" class="form-control" id="j-u-forgot-finish-pass" placeholder="<?= _t('users','Введите ваш пароль') ?>" autocomplete="off" maxlength="100" />
                    </div>

                    <button type="submit" class="btn btn-primary"><?= _t('users', 'Изменить пароль') ?></button>

                </form>

            </div><!-- /.s-signBlock-div -->

        </div><!-- /.col-md-6 -->
    </div><!-- /.row -->

</div><!-- /.signBlock-form -->
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.forgotFinish(<?= func::php2js(array(
        'lang' => array(
            'pass' => _t('users', 'Укажите пароль'),
            'success' => _t('users', 'Ваш пароль был успешно изменен.'),
        ),
    )) ?>);
});
<? js::stop(); ?>
</script>