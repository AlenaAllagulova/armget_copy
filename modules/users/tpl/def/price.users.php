<?php
tpl::includeJS('users.price.service', false);
?>
    <div class="container">
        <section class="l-mainContent" id="j-users-price-service">
            <div class="row">
                <aside class="col-md-3">
                    <?= $form ?>
                </aside>
                <div class="col-md-9 l-content-column" id="j-users-price-service-list">

                    <a href="<?= $spec['link'] ?>"><i class="fa fa-angle-left c-link-icon"></i> <?= _t('users', 'Назад в специализацию "[name]"', array('name' => $spec['title'])); ?></a>

                    <header class="title-type-2">
                        <h1 class="mrgt0"><?= $service['title'] ?></h1>
                    </header>

                    <div class="mrgb10">
                        <?= nl2br($service['description']); ?>
                    </div>

                    <div class="l-list-heading hidden-xs">
                        <h6 class="pull-left"><?= _t('users', 'Предоставляют данную услугу'); ?></h6>
                        <div class="clearfix"></div>
                    </div>

                    <div class="j-list"><?= $list ?></div>

                    <div class="j-pagination"><?= $pgn ?></div>

                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
    <? js::start() ?>
        jUsersPriceService.init(<?= func::php2js(array(
        'lang' => array(),
        'ajax' => true,
        'defCountry'    => Geo::defaultCountry(),
        'preSuggest'    => Geo::countrySelect() && $f['c'] ? Geo::regionPreSuggest($f['c'], 2) : '',
        'cat' => $cat_id,
        'spec'  => $spec_id,
    )) ?>);
    <? js::stop() ?>
    </script>
<?
