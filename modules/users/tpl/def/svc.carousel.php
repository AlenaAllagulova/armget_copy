<?php
    tpl::includeJS('svc', false, 3);
    $cur = Site::currencyDefault();
?>
    <div class="container">
        <section class="l-mainContent">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                    <h1 class="small"><?= $svc['title_view'][LNG] ?></h1>

                    <div class="row">
                        <div class="col-sm-6">

                            <h6><?= _t('svc', 'Как это работает?'); ?></h6>
                            <?= $svc['description_full'][LNG] ?>

                            <form class="mrgt20" action="" method="post" id="j-svc-carousel">
                                <div>
                                    <a class="f-freelancer-avatar" href="<?= Users::url('my.profile'); ?>">
                                        <img src="<?= UsersAvatar::url($user['id'], $user['avatar'], UsersAvatar::szSmall, $user['sex']); ?>" alt="<?= tpl::avatarAlt($user); ?>" />
                                        <?= tpl::userOnline($user) ?>
                                    </a>
                                    <div class="media-body">
                                        <div class="mrgb10">
                                            <?= tpl::userLink($user, '', '', array('class'=>'f-freelancer-name')) ?>
                                        </div>
                                        <div class="form-group">
                                            <input name="title" type="text" class="form-control input-sm" placeholder="<?= _t('', 'Заголовок'); ?>" value="<?= ! empty($title) ? HTML::escape($title) : '' ?>">
                                        </div>
                                        <div class="form-group j-required">
                                            <textarea name="message" rows="2" class="form-control input-sm" placeholder="<?= _t('svc', 'Текст сюда'); ?>"><?= ! empty($message) ? $message : '' ?></textarea>
                                        </div>

                                        <button class="btn btn-primary"><?= ($user['balance'] >= $svc['price'] ?
                                            _t('svc', 'Опубликовать за [price] [cur]', array('price' => $svc['price'], 'cur' => $cur)) :
                                            _t('svc', 'Пополнить счет на [price] [cur]', array('price' => $svc['price'], 'cur' => $cur)) ) ?></button>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-sm-6">
                            <? if (!empty($list)) { ?><h6><?= _t('svc', 'Кто тут был?'); ?></h6><? } ?>
                            <? foreach($list as $v): ?>
                            <div class="media l-freelancers-carousel-item se-freelancers-carousel-item">
                                <div class="s-freelancer">
                                    <a class="f-freelancer-avatar" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                                        <img src="<?= UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szSmall, $v['sex']); ?>" alt="<?= tpl::avatarAlt($v); ?>" />
                                    </a>
                                    <div class="media-body">
                                        <?= tpl::userLink($v, '', '', array('class'=>'f-freelancer-name')) ?>
                                        <div><strong><?= $v['title'] ?></strong></div>
                                        <div class="form-group">
                                            <?= nl2br($v['message']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="j-pay-form-request" class="hidden"></div>
    </div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jSvcCarousel.init(<?= func::php2js(array(
            'lang' => array(),
        )) ?>);
    });
    <? js::stop(); ?>
</script>