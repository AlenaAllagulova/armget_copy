<?php
foreach($list as $v): $aData['v'] = $v; ?>
    <div class="comment" id="j-item-<?= $v['id'] ?>">
        <?= $this->viewPHP($aData, 'admin.stairs.view'); ?>
    </div>
<? endforeach;
if(empty($list)): ?>
    <div class="alignCenter valignMiddle" style="height:30px; padding-top:15px;">
        <span class="desc"><?= _t('users', 'нет элементов') ?></span>
    </div>
<? endif; ?>
