    <a name="j-item-<?= $v['id'] ?>" ></a>
    <div class="ccontent">
        <div class="tb"><div class="tl"><div class="tr"></div></div></div>
        <div class="ctext">
            <b><?= $v['title'] ?></b>
            <div class="j-item-view"><?= nl2br($v['message']) ?></div>
        </div>
        <div class="bl"><div class="bb"><div class="br"></div></div></div>
    </div>
    <div class="info" style="margin:0;">
        <ul>
            <li><p><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink author"><?= ( ! empty($v['name']) ? $v['name'] : $v['login'] ) ?> (<?= $v['email'] ?>)</a></p></li>
            <li class="date"><?= tpl::date_format2($v['payed'], true); ?></li>
            <li><a href="#" class="ajax text-success" onclick="jCarousel.edit(<?= $v['id'] ?>); return false;"><?= _t('', 'Edit') ?></a></li>
            <li><a href="#" class="text-error delete ajax" onclick="jCarousel.del(<?= $v['id'] ?>); return false;"><?= _t('', 'Delete') ?></a></li>
        </ul>
    </div>
