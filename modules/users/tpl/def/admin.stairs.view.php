<?php
if ( ! isset($cats)) {
    $cats = Users::aSvcStairs() + Specializations::model()->specializationsInAllCategories(array('id','title'), array('id','title'));
}
if ( ! isset($specs)) {
    if (Specializations::catsOn()) {
        $specs = array();
        foreach ($cats as $vv) {
            if ( ! empty($vv['specs'])) {
                $specs += $vv['specs'];
            }
        }
    } else {
        $specs = $cats;
    }
}

$sCatTitle = '';
if ($v['spec_id'] < 0 && isset($cats[ $v['spec_id']])) {
    $sCatTitle = $cats[ $v['spec_id']]['title'];
} else if(isset($cats[ $v['cat_id']])) {
    $sCatTitle = $cats[ $v['cat_id']]['title'];
} else if(isset($specs[ $v['spec_id'] ])) {
    $sCatTitle = $specs[ $v['spec_id']]['title'];
}
?>
<a name="j-item-<?= $v['id'] ?>" ></a>
<div class="ccontent">
    <div class="tb"><div class="tl"><div class="tr"></div></div></div>
    <div class="ctext">
        <div class="desc"><b><?= $sCatTitle ?></b></div>
        <div><b><?= $v['title'] ?></b></div>
        <div><?= nl2br($v['message']) ?></div>
        <div class="desc"><?= nl2br($v['description']) ?></div>
    </div>
    <div class="bl"><div class="bb"><div class="br"></div></div></div>
</div>
<div class="info" style="margin:0;">
    <ul>
        <li><p><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink author"><?= ( ! empty($v['name']) ? $v['name'] : $v['login'] ) ?> (<?= $v['email'] ?>)</a></p></li>
        <li class="date"><?= tpl::date_format2($v['payed'], true); ?></li>
        <li><a href="#" class="ajax text-success" onclick="jStairs.edit(<?= $v['id'] ?>); return false;"><?= _t('', 'Edit') ?></a></li>
        <li><a href="#" class="text-error delete ajax" onclick="jStairs.del(<?= $v['id'] ?>); return false;"><?= _t('', 'Delete') ?></a></li>
    </ul>
</div>