<?php
    /**
     * Авторизация пользователя
     * @var $this Users
     */
?>
<div class="s-signBlock-form">
    <div class="row">
        <div class="col-sm-6 s-signBlock-div">

            <form action="" class="form-horizontal" role="form" id="j-u-login-form">
                <? if( ! empty($back)): ?><input type="hidden" name="back" value="<?= HTML::escape($back) ?>" /><? endif; ?>
                <div class="form-group j-required">
                    <label for="j-u-login-email" class="col-md-5 control-label"><?= _t('users', 'Электронная почта') ?> <i class="text-danger">*</i></label>
                    <div class="col-md-7">
                        <input type="text"  name="email" class="form-control" id="j-u-login-email" placeholder="<?= _t('users', 'Введите ваш email или login') ?>" />
                    </div>
                </div>
                <div class="form-group j-required">
                    <label for="j-u-login-pass" class="col-md-5 control-label"><?= _t('users', 'Пароль') ?> <i class="text-danger">*</i></label>
                    <div class="col-md-7">
                        <input type="password" name="pass" class="form-control" id="j-u-login-pass" placeholder="<?= _t('users', 'Введите ваш пароль') ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-5 col-md-7">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"/> <?= _t('users', 'Запомнить меня'); ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-5 col-md-7">
                        <button type="submit" class="btn btn-primary j-submit" ><?= _t('users', 'Войти на сайт') ?></button>
                    </div>
                </div>
            </form>

        </div><!-- /.s-signBlock-div -->
        <div class="col-sm-6 s-signBlock-div s-social-buttons">
            <? foreach($providers as $k=>$v) { ?>
                <a href="#" class="btn btn-sm s-btn-social s-<?= $v['class'] ?> j-u-login-social-btn"  data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}" ><?= $v['title'] ?></a>
            <? } ?>
<?  ?>
        </div><!-- /.s-signBlock-div -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="s-signBlock-other">
                <div class="row">
                    <div class="col-sm-6">
                        <h6><?= _t('users', 'Впервые на нашем сайте?') ?></h6>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-default btn-sm" href="<?= Users::url('register') ?>"><?= _t('users', 'Зарегистрируйтесь') ?></a>
                    </div>
                </div>
            </div><!-- /.s-signBlock-other -->
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="s-signBlock-other">
                <div class="row">
                    <div class="col-sm-6">
                        <h6><?= _t('users', 'Вы забыли свой пароль?') ?></h6>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-default btn-sm" href="<?= Users::url('forgot') ?>"><?= _t('users', 'Восстановить пароль') ?></a>
                    </div>
                </div>
            </div><!-- /.s-signBlock-other -->
        </div>

    </div><!-- /.row -->

</div><!-- /.signBlock-form -->
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.login(<?= func::php2js(array(
        'login_social_url' => Users::url('login.social'),
        'login_social_return' => $back,
        'lang' => array(
            'email' => _t('users', 'E-mail адрес указан некорректно'),
            'pass' => _t('users', 'Укажите пароль'),
        ),
    )) ?>);
});
<? js::stop(); ?>
</script>