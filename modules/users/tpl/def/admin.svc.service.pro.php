<?php
?>
<tr>
    <td class="row1"><span class="field-title">Стоимость</span><span class="required-mark">*</span>:</td>
    <td class="row2" id="j-svc-service-form-pro-price">
        <table class="admtbl tbledit">
            <thead>
                <tr class="header">
                    <th width="60" class="left">Месяцы</th>
                    <th width="115" class="left">Цена в мес.,<?= $pricePrefix; ?></th>
                    <th width="115" class="left">Стоимость,<?= $pricePrefix; ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="j-svc-service-form-pro-mass">
                <? $i = 1; if( ! empty($v['mass'])): foreach($v['mass'] as $m => $p): ?>
                    <tr>
                        <td><input type="number" name="mass[<?= $i ?>][m]" min="1" max="100" step="1" class="j-month" maxlength="2" value="<?= $m ?>" style="width: 55px;" placeholder="месяцы" /></td>
                        <td><input type="text" class="j-per-month" maxlength="12" pattern="[0-9\.\,]*" value="<?= number_format(round($p/$m, 2), 2, '.', '') ?>" style="width: 110px;" placeholder="цена в мес." /></td>
                        <td><input type="text" name="mass[<?= $i ?>][p]" class="j-price" maxlength="12" pattern="[0-9\.\,]*" value="<?= number_format($p, 2, '.', '') ?>" style="width: 110px;" placeholder="стоимость" /></td>
                        <td><a href="#" class="but cross disabled j-delete"></a></td>
                    </tr>
                <? $i++; endforeach; endif; ?>
            </tbody>
        </table>
        <a class="ajax j-mass-add" href="#">+ добавить</a>
    </td>
</tr>
<script type="text/javascript">
    $(function(){
        var i = <?= $i ?>;
        var $block = $('#j-svc-service-form-pro-price');
        var $mass = $('#j-svc-service-form-pro-mass');

        $block.on('click', '.j-mass-add', function(){
            $mass.append('<tr>'+
                '<td><input type="number" name="mass['+i+'][m]" min="1" max="100" step="1" class="j-month" maxlength="2" value="" style="width: 55px;" placeholder="месяцы" /></td>'+
                '<td><input type="text" class="j-per-month" maxlength="12" pattern="[0-9\.\,]*" value="" style="width: 110px;" placeholder="цена в мес." /></td>'+
                '<td><input type="text" name="mass['+i+'][p]" class="j-price" maxlength="12" pattern="[0-9\.\,]*" value="" style="width: 110px;" placeholder="стоимость" /></td>'+
                '<td><a href="#" class="but cross disabled j-delete"></a></td>'+
            '</tr>');
            i++;
            return false;
        });

        $block.on('click', '.j-delete', function(){
            $(this).closest('tr').remove();
            return false;
        });

        $block.on('change', '.j-month', function(){
            calcPerMonth($(this));
            return false;
        });

        $block.on('keyup', '.j-per-month', function(){
            calcPerMonth($(this));
            return false;
        });

        $block.on('keyup', '.j-price', function(){
            calcPerMonth($(this), true);
            return false;
        });

        function calcPerMonth($el, isPrice){
            var $tr = $el.closest('tr');
            var $m = $tr.find('.j-month');
            var $p = $tr.find('.j-price');
            var $pm = $tr.find('.j-per-month');
            var m = intval($m.val());
            var p = floatval($p.val());
            var pm = floatval($pm.val());

            if (isPrice) {
                if (m > 0 && p > 0) {
                    pm = p / m;
                    $m.val(m);
                    $pm.val(pm.toFixed(2));
                } else {
                    $pm.val('');
                }
            } else {
                if (m > 0 && pm > 0) {
                    p = m * pm;
                    $m.val(m);
                    if($el.hasClass('j-month')) $pm.val(pm.toFixed(2));
                    $p.val(p.toFixed(2));
                } else {
                    $p.val('');
                }
            }
        }

        function floatval(str)
        {
            var fl = parseFloat((str+'').replace(',','.'));
            return isNaN(fl) ? 0 : fl;
        }
    });
</script>