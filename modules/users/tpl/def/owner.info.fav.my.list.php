<?php
/** @var $this Users */
if( ! empty($favs_my)): ?>
    <div class="row">
    <? $cnt = 1; foreach($favs_my as $v): ?>
        <div class="col-sm-4 j-my-fav">
            <div class="p-favorites-user">
                <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="p-favorites-avatar">
                    <?= tpl::userAvatar($v) ?>
                </a>
                <div class="p-favorites-userinfo">
                    <div><?= tpl::userLink($v, 'strong') ?></div>
                    <?  $aNote = array('id' => $v['note_id'], 'user_id' => $v['user_id'], 'note' => $v['note'], 'bFav' => 1);
                    echo($this->viewPhp($aNote, 'my.note.block'));
                    ?>
                    <a class="ajax-link link-delete j-delete-fav" href="#" data-id="<?= $v['user_id'] ?>"><i class="fa fa-times"></i> <span><?= _t('users', 'Удалить из избранных'); ?></span></a>
                </div>
            </div>
        </div>
        <? if( ! ( $cnt % 3)): ?>
            </div>
            <div class="row">
        <? endif; $cnt++; ?>
    <? endforeach; ?>
    </div>
<? endif;
