<?php
?>
<tr>
    <td class="row1" colspan="2"><b>Стоимость рамещения (за неделю)</b></td>
</tr>
<tr>
    <td class="row1"><span class="field-title">Главная страница</span>:<div style="width: 100px; height: 1px;"></div></td>
    <td class="row2">
        <input type="text" name="price_main" value="<?= $v['price_main'] ?>" maxlength="6" class="input-small" /><?= $pricePrefix; ?>
    </td>
</tr>
<tr>
    <td class="row1"><span class="field-title">Исполнители (общий список)</span>:</td>
    <td class="row2">
        <input type="text" name="price_user" value="<?= $v['price_user'] ?>" maxlength="6" class="input-small" /><?= $pricePrefix; ?>
    </td>
</tr>
<? if(Specializations::catsOn()): ?>
<tr>
    <td class="row1"><span class="field-title">Исполнители (категория)</span>:</td>
    <td class="row2">
        <input type="text" name="price_cat" value="<?= $v['price_cat'] ?>" maxlength="6" class="input-small" /><?= $pricePrefix; ?>
    </td>
</tr>
<? endif; ?>
<tr>
    <td class="row1"><span class="field-title">Исполнители (специализация)</span>:</td>
    <td class="row2">
        <input type="text" name="price_spec" value="<?= $v['price_spec'] ?>" maxlength="6" class="input-small" /><?= $pricePrefix; ?>
    </td>
</tr>
<tr>
    <td class="row1"><span class="field-title">Стоимость поднятия</span>:</td>
    <td class="row2">
        <input type="text" name="price_up" value="<?= $v['price_up'] ?>" maxlength="6" class="input-small" /><?= $pricePrefix; ?>
    </td>
</tr>