<?php
    $bFav = ! empty($bFav);
?>
<div class="o-your-note <?=  $bFav ? ' p-your-note ' : '' ?><?= ! empty($classes) ? ' '.$classes.' ' : '' ?>j-note">

    <strong><?= _t('users', 'Ваша заметка:'); ?></strong>
    <a href="#" class="ajax-link show-tooltip j-add" <?= $id ? 'style="display:none;"' : '' ?> data-placement="bottom"  title="<?= _t('users', 'Ваша заметка будет видна только вам.'); ?>" data-original-title="<?= _t('users', 'Ваша заметка будет видна только вам.'); ?>"><span><?= _t('', 'Добавить'); ?></span></a>

    <div class="j-notes-show" <?= ! $id ? 'style="display:none;"' : '' ?> data-id="<?= $id ?>">
        <div class="j-text"><?= nl2br($note)?></div>
        <div class="o-noteLinks">
            <a class="ajax-link j-edit" href="#" ><i class="fa fa-pencil"></i> <span><?= _t('form', 'Редактировать'); ?></span></a>
            <a class="ajax-link <?=  $bFav ? '' : 'link-delete' ?> j-delete" href="#"><i class="fa fa-times"></i> <span><?= _t('form', 'Удалить'); ?></span></a>
        </div>
    </div>

    <div class="o-addnote j-notes-form-block">
        <form method="post" action="">
            <input type="hidden" name="id" value="<?= $id ?>" />
            <input type="hidden" name="user_id" value="<?= $user_id ?>" />
            <div class="form-group">
                <textarea name="note" class="form-control" rows="2" placeholder="<?= _t('users', 'Введите текст заметки'); ?>"><?= $note ?></textarea>
            </div>
            <div class="c-formSubmit">
                <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
                <a href="#" class="ajax-link c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
            </div>
        </form>

    </div>
</div>
