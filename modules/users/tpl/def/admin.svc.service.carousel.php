<?php
if (empty($v['amount'])){
    $v['amount'] = 10;
}
?>
<tr>
    <td class="row1"><span class="field-title">Стоимость</span><span class="required-mark">*</span>:</td>
    <td class="row2">
        <input type="text" name="price" value="<?= $v['price'] ?>" class="input-mini" /><?= $pricePrefix; ?>
    </td>
</tr>
<tr>
    <td class="row1"><span class="field-title">Количество пользователей в карусели</span><span class="required-mark">*</span>:</td>
    <td class="row2">
        <input type="number" min="1" name="amount" value="<?= intval($v['amount']) ?>" class="input-mini" />
    </td>
</tr>

