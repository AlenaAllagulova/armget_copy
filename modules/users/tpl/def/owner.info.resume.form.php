<?php
/** @var $this Users */
?>
<form action="" method="post" enctype="multipart/form-data">
    <input class="hidden" type="file" name="resume_file" value="" />
    <input type="hidden" name="resume_file_del" value="0" />
    <input type="hidden" name="act" value="resume-save" />

    <div class="form-group">
        <?= tpl::jwysiwyg($resume_text, 'resume_text', 0, 300, array('controls' => array(
                'insertImageSimple' => array('visible' => false),
                'fullscreen'        => array('visible' => false),
                'fontSize'          => array('visible' => false),
                'fontColor'         => array('visible' => false),
                'blockquote'        => array('visible' => false),
                'html'              => array('visible' => false),
        ))) ?>
    </div>

    <div class="p-profile-resume-download <?= empty($resume_file) ? 'hidden' : '' ?> j-attach-info">
        <span class="j-file-name"><?= HTML::escape($resume_file_title) ?></span>
        <a href="#" data-toggle="tooltip" data-placement="bottom" class="show-tooltip link-delete pull-right j-del-attach" title="<?= _t('form', 'Удалить'); ?>" data-original-title="<?= _t('form', 'Удалить'); ?>"><i class="fa fa-trash-o"></i></a>
    </div>

    <div class="c-formSubmit">
        <div class="form-group j-attach-block">
            <button class="btn btn-default btn-sm c-formSuccess j-attach"><?= _t('users', 'Загрузить резюме'); ?></button>
        </div>
        <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
        <a href="#" class="ajax-link c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
    </div>
</form>