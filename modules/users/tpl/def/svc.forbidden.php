<?php
?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1 class="small"><?= _t('users', 'Доступ запрещен') ?></h1>

                <p><?= $title ?></p>
                <p><a href="<?= bff::urlBase() ?>"><?= _t('', 'Перейти на главную страницу'); ?></a></p>

            </div>
        </div>
    </section>
</div>
