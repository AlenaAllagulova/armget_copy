<?php
/**
 * Прайс пользователя
 * @var $this Users
 * @var $specs array
 * @var $services_cnt integer
 */
?>
<div class="p-profileContent">
    <? if( ! empty($services_cnt)): ?>
    <ul class="se-price-list">
        <? foreach($specs as $v): if(empty($v['services']) && empty($v['services_cnt'])) continue; ?>
        <li>
            <div class="se-price-list-header">
                <h2><?= $v['spec_title'] ?></h2>
                <? if( ! empty($v['services'])): ?>
                <div class="se-price-list-header-edit">
                    <a href="<?= Users::url('my.settings', array('spec_service' => $v['spec_id'])) ?>"><i class="fa fa-edit c-link-icon"></i><span><?= _t('users', 'Изменить прайс'); ?></span></a>
                </div>
                <? endif; ?>
            </div>

            <table class="table se-price-list-table">
                <thead class="hidden-xs">
                <tr>
                    <th><?= _t('', 'Услуга'); ?></th>
                    <th class="se-price-list-table-price text-left"><?= _t('', 'Стоимость'); ?></th>
                </tr>
                </thead>
                <tbody>
                <? if( ! empty($v['services'])): $cnt = 0; foreach($v['services'] as $vv): if($vv['price'] == 0 && ! $vv['price_free']) continue; $cnt++; ?>
                <tr>
                    <td class="se-price-list-table-name">
                        <div><?= $vv['title'] ?></div>
                    </td>
                    <td class="se-price-list-table-price text-left">
                        <? if($vv['price']): ?>
                        <strong><?= tpl::formatPrice($vv['price']) ?> <?= Site::currencyData($vv['price_curr'],'title_short')?></strong> <span>/ <?= $vv['measure'] ?></span>
                        <? else: ?>
                            <?= _t('users', 'бесплатно') ?>
                        <? endif; ?>
                    </td>
                </tr>
                <? endforeach;
                endif; if(empty($cnt)): ?>
                    <tr>
                        <td colspan="2">
                            <div class="text-grey"><?= _t('users', 'Нет услуг в данной специализации.'); ?> <a href="<?= Users::url('my.settings', array('spec_service' => $v['spec_id'])) ?>"><i class="fa fa-plus-circle c-link-icon"></i><?= _t('users', 'Добавить услугу'); ?></a></div>
                        </td>
                    </tr>
                <? endif; ?>
                </tbody>
            </table>
        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('users', 'Услуги не найдены'); ?></div>
<? endif; ?>
</div>
<?
