<?php
?>
<form method="post" action="">
    <input type="hidden" name="id" value="<?= $id ?>" />
    <a name="j-item-<?= $id ?>" ></a>
    <div class="ccontent">
        <div class="tb"><div class="tl"><div class="tr"></div></div></div>
        <div class="ctext">
            <div class="j-spec"><?= $specs_select ?></div>
            <input type="text" name="title" value="<?= HTML::escape($title); ?>" placeholder="<?= _t('', 'Заголовок')?>" class="stretch" />
            <textarea name="message" placeholder="<?= _t('', 'Описание')?>" style="height: 75px;"><?= HTML::escape($message) ?></textarea>
            <textarea name="description"  placeholder="<?= _t('', 'Подробное описание')?>" style="height: 85px;"><?= HTML::escape($description) ?></textarea>
        </div>
        <div class="bl"><div class="bb"><div class="br"></div></div></div>
    </div>
    <div class="info" style="margin:0;">
        <ul>
            <li><p><a href="#" onclick="return bff.userinfo(<?= $user_id ?>);" class="userlink author"><?= ( ! empty($name) ? $name : $login ) ?> (<?= $email ?>)</a></p></li>
            <li class="date"><?= tpl::date_format2($payed, true); ?></li>
            <li>
                <a href="#" class="btn btn-mini btn-success" onclick="jStairs.save($(this), <?= $id ?>); return false;"><?= _t('', 'Save') ?></a>
                <a href="#" class="btn btn-mini" onclick="jStairs.cancel(<?= $id ?>); return false;"><?= _t('', 'Cancel') ?></a>
            </li>
        </ul>
    </div>
</form>
