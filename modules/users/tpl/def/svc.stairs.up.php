<?php
tpl::includeJS('svc', false, 3);
$cur = Site::currencyDefault();
?>
    <div class="container">

        <section class="l-mainContent">

            <div class="row" id="j-stairs-up-block">
                <div class="col-md-8 col-md-offset-2">

                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                    <h1 class="small"><?= _t('svc', 'Выбор позиции в лестнице'); ?></h1>

                    <div class="row">

                        <aside class="col-md-4">

                            <button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#left-column"><?= _t('', 'Показать разделы'); ?></button>

                            <div class="l-leftColumn collapse" id="left-column">

                                <h5><?= _t('svc', 'Ваши разделы'); ?> <i class="text-danger">*</i></h5>

                                <div class="collapse in" id="left-categories">
                                    <ul class="se-left-categories">
                                        <? foreach($cats as $v):
                                            $bSpecs = $v['id'] > 0 && Specializations::catsOn();
                                            $bExpire = isset($v['expire']);
                                            $bActive = $bSpecs ? $current['cat_id'] == $v['id'] : $current['spec_id'] == $v['id'];
                                            ?>
                                            <li class="<?= $bSpecs ? 'opened' : '' ?><?= $bActive ? ' active' : '' ?>">
                                                <? $url = '';
                                                    if($bExpire){
                                                        $name = $bSpecs ? 'cat_id' : 'spec_id';
                                                        $url = Svc::url('view', array('keyword' => 'stairs', 'up' => 1, $name => $v['id']));
                                                    } ?>
                                                <a href="<?= $url ?>"><?= $v['title'] ?></a>
                                                <? if($bExpire): ?>
                                                <div class="se-left-categories-subline">
                                                    <?= _t('', 'до'); ?> <?= tpl::date_format2($v['expire']) ?> <span>(<a href="<?= Svc::url('view', array('keyword' => 'stairs', $name => $v['id'])) ?>"><?= _t('svc', 'продлить'); ?></a>)</span>
                                                </div>
                                                <? endif; ?>
                                                <? if($bSpecs && ! empty($v['specs'])): ?>
                                                    <ul>
                                                        <? foreach($v['specs'] as $vv):
                                                            $bActive = $current['spec_id'] == $vv['id']
                                                            ?>
                                                            <li<?= $bActive ? ' class="active"' : '' ?>>
                                                                <a href="<?= Svc::url('view', array('keyword' => 'stairs', 'up' => 1, 'spec_id' => $vv['id'])) ?>"><?= $vv['title'] ?></a>
                                                                <? if(isset($vv['expire'])): ?>
                                                                    <div class="se-left-categories-subline">
                                                                        <?= _t('', 'до'); ?> <?= tpl::date_format2($vv['expire']) ?> <span>(<a href="<?= Svc::url('view', array('keyword' => 'stairs', 'spec_id' => $vv['id'])) ?>"><?= _t('svc', 'продлить'); ?></a>)</span>
                                                                    </div>
                                                                <? endif; ?>
                                                            </li>
                                                        <? endforeach; ?>
                                                    </ul>
                                                <? endif; ?>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </aside>

                        <div class="col-md-8">

                            <h5><?= _t('svc', 'Поднять аккаунт'); ?></h5>

                            <div class="j-list"><?= $list ?></div>

                            <div class="c-formSubmit">
                                <button class="btn btn-primary c-formSuccess j-submit hidden"></button>
                                <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                            </div>

                        </div>


                    </div>

                </div>
            </div>

        </section>

    </div>

<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jSvcStairsUp.init(<?= func::php2js(array(
            'lang' => array(
                'cur' => $cur,
                'buy' => _t('svc', 'Пополнить на'),
                'up' => _t('svc', 'Поднять за'),
                'success' => _t('svc', 'Услуга успешно активирована'),
            ),
            'balance' => $balance,
            'price'   => $svc['price_up'],
            'cat_id'  => $current['cat_id'],
            'spec_id' => $current['spec_id'],
        )) ?>);
    });
    <? js::stop(); ?>
</script>