<?php
/** @var $this Users */
?>

<div class="j-resume-text"><?= $resume_text ?></div>

<? if( ! empty($resume_file)): ?><?= $this->resumeAttach()->getAttachLink($resume_file, true, array(), $resume_file_title) ?><? endif; ?>
<? if($bMy): ?><a href="#" class="ajax-link j-resume-edit"><i class="fa fa-edit"></i> <span><?= _t('users', 'Редактировать резюме'); ?></span></a><? endif; ?>
