<?php
tpl::includeJS('comments', true);
?>
<div class="comments">
    <?
    foreach($list as $v): $aData['v'] = $v; ?>
    <div class="comment" id="j-item-<?= $v['id'] ?>">
        <?= $this->viewPHP($aData, 'admin.carousel.view'); ?>
    </div>
    <? endforeach;
    if(empty($list)): ?>
        <div class="alignCenter valignMiddle" style="height:30px; padding-top:15px;">
            <span class="desc"><?= _t('users', 'нет элементов') ?></span>
        </div>
    <? endif; ?>
</div>
<script type="text/javascript">
    var jCarousel = (function() {
        var processing = false;
        var ajax_url = '<?= $this->adminLink('carousel&act='); ?>';

        return {
            del: function(id)
            {
                if( ! bff.confirm('sure')) return;
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'delete', {id: id}, function(data) {
                    if(data) {
                        $('#j-item-'+id).remove();
                    }
                    processing = false;
                });
            },
            edit:function(id)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'form', {id: id}, function(data) {
                    if(data) {
                        $('#j-item-'+id).html(data.html).find('.ccontent').addClass('self');
                    }
                    processing = false;
                });

            },
            save:function($el, id)
            {
                if(processing) return;
                processing = true;
                $el = $($el);
                var $f = $el.closest('form');
                bff.ajax(ajax_url+'save', $f.serialize(), function(data) {
                    if(data) {
                        $('#j-item-'+id).html(data.html);
                    }
                    processing = false;
                });
            },
            cancel:function(id)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'cancel', {id: id}, function(data) {
                    if(data) {
                        $('#j-item-'+id).html(data.html);
                    }
                    processing = false;
                });
            }
        };
    }());
</script>