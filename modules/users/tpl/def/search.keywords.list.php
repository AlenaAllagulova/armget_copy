<?php
if( ! isset($nTagID)) $nTagID =0;
if( ! empty($list)):
    $aStatus = Users::aWorkerStatus();
    ?>
    <ul class="media-list o-freelancersList l-search-results">
    <? foreach($list as &$v): ?>
        <li class="media">
            <div class="l-list-num"><?= $v['n'] ?>.</div>
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                <?= tpl::userAvatar($v) ?>
            </a>
            <div class="media-body">
                <div class="row o-freelancer-info">
                    <div class="col-sm-6">
                        <div class="f-freelancer-name">
                            <?= tpl::userLink($v); ?>
                            <? if($v['status'] != Users::WORKER_STATUS_NONE): ?>
                                <span class="label <?= $aStatus[ $v['status'] ]['c'] ?>">
                                    	<?= $aStatus[ $v['status'] ]['t'] ?>
                                </span>
                            <? endif; ?>
                        </div>
                        <div>
                            <span class="hidden-sm hidden-xs"><?= _t('users', 'Специализация:'); ?></span> <a href="<?= Users::url('search-spec', $v) ?>"><?= $v['main_spec_title'] ?></a>
                        </div>
                        <? if( ! empty($v['city_data']['title'])): ?>
                            <div>
                                <i class="fa fa-map-marker"></i> <?= $v['city_data']['title'] ?><?= ! empty($v['country_data']['title']) ? ', '.$v['country_data']['title'] : '' ?>
                            </div>
                        <? endif; ?>
                    </div>

                    <div class="col-sm-3">
                        <div class="o-feedbacks-inf">
                            <?= _t('opinions', 'Opinions:'); ?> <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                        </div>
                        <div>
                            <?= _t('users', 'Рейтинг:'); ?> <strong><?= Users::rating($v['rating'], $v['pro']) ?></strong>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <? if( ! empty($v['main_price'])): ?>
                            <div>
                                <i class="fa fa-clock-o"></i>
                                <?= ! empty($v['main_spec_price_title']) ? $v['main_spec_price_title'] : _t('users', 'Цена'); ?><?=
                                ! empty($v['main_price_rate_text'][LNG]) ? ' '.$v['main_price_rate_text'][LNG] : '' ?>:
                                <?= tpl::formatPrice($v['main_price']) ?>
                                <?= Site::currencyData($v['main_price_curr'], 'title_short'); ?>
                            </div>
                        <? endif; ?>
                        <? if( ! empty($v['main_budget'])): ?>
                            <div>
                                <i class="fa fa-dollar"></i> <?= _t('users', 'Бюджет от:'); ?>
                                <?= tpl::formatPrice($v['main_budget']) ?>
                                <?= Site::currencyData($v['main_budget_curr'], 'title_short'); ?>
                            </div>
                        <? endif; ?>
                    </div>

                </div>

                <? if( ! empty($v['aTags'])): ?>
                    <div class="l-project-head">
                    <? foreach($v['aTags'] as $vv):
                        $bSearch = $nTagID == $vv['id'];
                        if( ! $bSearch && $f['q']){
                            $bSearch = $f['q'] == $vv['tag'];
                        }
                        ?>
                        <a href="<?= Users::url('search-tag', $vv) ?>" class="l-tag<?= $bSearch ? ' l-search-tag' : '' ?>"><?= $vv['tag'] ?></a>
                    <? endforeach; ?>
                    </div>
                <? endif; ?>

            </div>

        </li>
    <? endforeach; unset($v); ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('users', 'Исполнители не найдены'); ?></div>
<? endif;
