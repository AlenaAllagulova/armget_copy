<?php
    $aStatus = Users::aWorkerStatus();
?>
<div>
    <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="o-freelancer-avatar">
        <?= tpl::userAvatar($v) ?>
    </a>
    <div class="f-mapView o-freelancer-info media-body">
        <strong><?= tpl::userLink($v); ?>
            <? if($v['status'] != Users::WORKER_STATUS_NONE): ?>
                <span class="label <?= $aStatus[ $v['status'] ]['c'] ?>">
                    <?= $aStatus[ $v['status'] ]['t'] ?>
                </span>
            <? endif; ?>
        </strong>

        <div>
            <span class="hidden-sm hidden-xs"><?= _t('users', 'Специализация:'); ?></span> <a href="<?= Users::url('search-spec', $v) ?>"><?= $v['main_spec_title'] ?></a>
        </div>
        <? if( ! empty($v['city_data']['title'])): ?>
            <div>
                <i class="fa fa-map-marker"></i> <?= $v['city_data']['title'] ?><?= ! empty($v['country_data']['title']) ? ', '.$v['country_data']['title'] : '' ?>
            </div>
        <? endif; ?>
        <div class="o-feedbacks-inf">
            <?= _t('opinions', 'Opinions:'); ?> <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
        </div>
        <div class="f-mapView-features">
            <? if( ! empty($v['main_price'])): ?>
            <span><i class="fa fa-clock-o"></i> <?= tpl::formatPrice($v['main_price']) ?> <?= Site::currencyData($v['main_price_curr'], 'title_short'); ?><?=
                ! empty($v['main_price_rate_text'][LNG]) ? '/'.$v['main_price_rate_text'][LNG] : '' ?></span>
            <? endif; ?>
            <? if( ! empty($v['main_budget'])): ?>
            <span><i class="fa fa-dollar"></i> <?= _t('users', 'Бюджет от:'); ?> <?= tpl::formatPrice($v['main_budget']) ?>
                <?= Site::currencyData($v['main_budget_curr'], 'title_short'); ?></span>
            <? endif; ?>
        </div>
        <? if( ! empty($v['aTags'])): foreach($v['aTags'] as $vv):?>
            <a href="<?= Users::url('search-tag', $vv) ?>" class="l-tag"><?= $vv['tag'] ?></a>
        <? endforeach; endif; ?>
    </div>
</div>