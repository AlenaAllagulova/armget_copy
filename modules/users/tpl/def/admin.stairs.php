<?php
tpl::includeJS('comments', true);
?>
<form action="" name="filter" class="form-inline" id="j-stairs-filter">
    <div class="actionBar" style="margin-left: 17px;">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>"/>

        <div class="controls controls-row">
            <div class="left" id="j-stairs-filter-spec"><?= $cats_specs_select ?></div>
            <div class="left" style="margin-left: 10px;">
                <input type="submit" value="<?= _t('', 'search') ?>" class="btn btn-small button submit" />
                <a class="cancel" id="j-filter-cancel"><?= _t('', 'reset') ?></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</form>

<div class="comments" id="j-stairs-items"><?= $list ?></div>
<div id="j-stairs-pgn"><?= $pgn ?></div>

<script type="text/javascript">
    var jStairs = (function() {
        var processing = false;
        var ajax_url = '<?= $this->adminLink('stairs&act='); ?>';
        var $filter, $list, $fspec, $pgn;

        $(function(){
            $filter = $('#j-stairs-filter');
            $list = $("#j-stairs-items");
            $fspec = $('#j-stairs-filter-spec');
            $pgn = $('#j-stairs-pgn');

            $filter.on('change', function(){
                setPage(0);
                updateList();
            });

            $list.on('change', '.j-cat', function(){
                if(processing) return;
                processing = true;
                var $f = $(this).closest('form');
                bff.ajax(ajax_url+'specs', $f.serialize(), function(data) {
                    if(data && data.specs) {
                        $f.find('.j-spec').html(data.specs);
                    }
                    processing = false;
                });
            });

            $(window).bind('popstate',function(){
                if('state' in window.history && window.history.state === null) return;
                updateList(false);
            });
        });

        function isProcessing()
        {
            return processing;
        }

        function setPage(id)
        {
            $filter.find('[name="page"]').val(intval(id));
        }

        function updateList(updateUrl)
        {
            if(isProcessing()) return;
            var f = $filter.serialize();
            bff.ajax(ajax_url, f, function(data){
                if(data) {
                    $list.html(data.list);
                    $fspec.html(data.specs);
                    $pgn.html( data.pgn );
                    if(updateUrl !== false && bff.h) {
                        window.history.pushState({}, document.title, $filter.attr('action') + '?' + f);
                    }
                }
            }, function(p){ processing = p; $list.toggleClass('disabled'); });
        }

        return {
            page: function(id)
            {
                if(isProcessing()) return false;
                setPage(id);
                updateList();
            },
            del: function(id)
            {
                if( ! bff.confirm('sure')) return;
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'delete', {id: id}, function(data) {
                    if(data) {
                        $('#j-item-'+id).remove();
                    }
                    processing = false;
                });
            },
            edit:function(id)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'form', {id: id}, function(data) {
                    if(data) {
                        $('#j-item-'+id).html(data.html).find('.ccontent').addClass('self');
                    }
                    processing = false;
                });
            },
            save:function($el, id)
            {
                if(processing) return;
                processing = true;
                $el = $($el);
                var $f = $el.closest('form');
                bff.ajax(ajax_url+'save', $f.serialize(), function(data) {
                    if(data) {
                        $('#j-item-'+id).html(data.html);
                    }
                    processing = false;
                });
            },
            cancel:function(id)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'cancel', {id: id}, function(data) {
                    if(data) {
                        $('#j-item-'+id).html(data.html);
                    }
                    processing = false;
                });
            }
        };
    }());
</script>