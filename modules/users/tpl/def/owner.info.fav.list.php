<?php
if( ! empty($favs)): ?>
<div class="row">
    <? $cnt = 1; foreach($favs as $v): ?>
    <div class="col-sm-4">
        <div class="p-favorites-user">
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="p-favorites-avatar">
                <?= tpl::userAvatar($v) ?>
            </a>
            <div class="p-favorites-userinfo">
                <div><?= tpl::userLink($v, 'strong') ?></div>
                <div class="o-feedbacks-inf">
                    <?= _t('opinions', 'Opinions:') ?>
                    <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])) ?>
                </div>
            </div>
        </div>
    </div>
        <? if( ! ( $cnt % 3)): ?>
            </div>
            <div class="row">
        <? endif; $cnt++; ?>
    <? endforeach; ?>
    </div>
<? endif;
