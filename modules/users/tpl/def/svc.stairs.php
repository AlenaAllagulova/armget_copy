<?php
tpl::includeJS('svc', false, 3);
$aData['cur'] = $cur = Site::currencyDefault();
$i = 1; $aData['i'] = & $i;
?>
    <div class="container">
        <section class="l-mainContent" id="j-stairs-block">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                    <h1 class="small"><?= $svc['title_view'][LNG] ?></h1>

                    <div class="row">

                        <aside class="col-md-4" id="j-stairs-specs">

                            <button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#left-column"><?= _t('svc', 'Показать разделы'); ?></button>

                            <div class="l-leftColumn collapse" id="left-column">

                                <h5><?= _t('', 'Выберите раздел'); ?> <i class="text-danger">*</i></h5>

                                <ul class="se-left-categories">
                                    <? foreach($cats as $v):
                                        $bSpecs = ! empty($v['specs']);
                                        $bDisabled = false;
                                        if ($v['id'] < 0 ) {
                                            if (isset($list['spec'][$v['id']])) {
                                                $bDisabled = true;
                                            }
                                        } else {
                                            if (isset($list['cat'][ $v['id'] ])) {
                                                $bDisabled = true;
                                            }
                                        }
                                        ?>
                                    <li <?= $bSpecs ? 'class="opened"' : '' ?>>
                                        <?= $bSpecs ? '<span>' : '' ?>
                                        <div class="checkbox">
                                          <label>
                                              <input type="checkbox" name="<?= $bSpecs ? 'cat' : 'spec' ?>[]" value="<?= $v['id'] ?>" <?= $bDisabled ? ' checked="checked" disabled="disabled" ' : '' ?> /> <?= $v['title'] ?>
                                          </label>
                                        </div>
                                        <?= $bSpecs ? '</span>' : '' ?>
                                    <? if($bSpecs): ?>
                                        <ul>
                                            <? foreach($v['specs'] as $vv):
                                                $bDisabled = false;
                                                if (isset($list['spec'][ $vv['id'] ])) {
                                                    $bDisabled = true;
                                                }
                                                ?>
                                            <li>
                                                <span>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="spec[]" value="<?= $vv['id'] ?>" <?= $bDisabled ? ' checked="checked" disabled="disabled" ' : '' ?> /> <?= $vv['title'] ?>
                                                        </label>
                                                    </div>
                                                </span>
                                            </li>
                                            <? endforeach; ?>
                                        </ul>
                                    <? endif; ?>
                                    </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        </aside>

                        <div class="col-md-8">
                            <h5><?= _t('svc', 'Выбранные разделы'); ?></h5>
                            <form action="" method="post" >
                                <? foreach($cats as $v) {
                                    if ($v['id'] < 0 ){
                                        if(isset($list['spec'][ $v['id'] ])) {
                                            $d = $aData;
                                            $d['it'] = $list['spec'][ $v['id'] ];
                                            $d['it']['spec_title'] = $v['title'];
                                            echo $this->viewPHP($d, 'svc.stairs.section');
                                            unset($list['spec'][ $v['id'] ]);
                                        }
                                    } else {
                                        if (isset($list['cat'][ $v['id'] ])) {
                                            $d = $aData;
                                            $d['it'] = $list['cat'][ $v['id'] ];
                                            $d['it']['spec_title'] = $v['title'];
                                            echo $this->viewPHP($d, 'svc.stairs.section');
                                            unset($list['cat'][ $v['id'] ]);
                                        }
                                        if( ! empty($v['specs'])){
                                            foreach($v['specs'] as $vv){
                                                if(isset($list['spec'][ $vv['id'] ])){
                                                    $d = $aData;
                                                    $d['it'] = $list['spec'][ $vv['id'] ];
                                                    $d['it']['spec_title'] = $vv['title'];
                                                    echo $this->viewPHP($d, 'svc.stairs.section');
                                                    unset($list['spec'][ $vv['id'] ]);
                                                }
                                            }
                                        }
                                    }
                                } ?>

                            <div id="j-add-block">
                            </div>

                            <div class="c-formSubmit">
                                <button class="btn btn-primary c-formSuccess j-submit hidden"></button>
                                <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                            </div>
                            </form>

                        </div>


                    </div>

                </div>
            </div>
            <div id="j-add-form" class="hidden">
                <div class="se-service-section j-stairs j-add-__counter__">
                    <input type="hidden" name="spec[__counter__]" class="j-spec" value="0" />
                    <input type="hidden" name="cat[__counter__]"  class="j-cat" value="0" />

                    <div class="se-service-close"><a href="#" class="j-close"><i class="fa fa-times"></i></a></div>
                    <h5 class="j-title"></h5>
                    <div class="clearfix"></div>

                    <div>
                        <a class="f-freelancer-avatar" href="<?= Users::url('my.profile'); ?>">
                            <img src="<?= UsersAvatar::url($user['id'], $user['avatar'], UsersAvatar::szSmall, $user['sex']); ?>" alt="<?= tpl::avatarAlt($user); ?>" />
                            <?= tpl::userOnline($user) ?>
                        </a>
                        <div class="media-body">
                            <div class="mrgb10">
                                <?= tpl::userLink($user, '', '', array('class'=>'f-freelancer-name')) ?>
                            </div>
                            <div class="form-group j-required">
                                <input name="title[__counter__]" type="text" class="form-control input-sm" placeholder="<?= _t('', 'Заголовок'); ?>" maxlength="40" value="<?= ! empty($title) ? HTML::escape($title) : '' ?>" />
                            </div>
                            <div class="form-group j-required">
                                <textarea name="message[__counter__]" rows="3" class="form-control input-sm j-message" placeholder="<?= _t('svc', 'Описание'); ?>"><?= ! empty($message) ? HTML::escape($message) : '' ?></textarea>
                                <div class="help-block j-message-counter"></div>
                            </div>
                            <div class="form-group j-required">
                                <textarea name="description[__counter__]" rows="4" class="form-control input-sm j-description" placeholder="<?= _t('svc', 'Подробное описание'); ?>"><?= ! empty($description) ? HTML::escape($description) : '' ?></textarea>
                                <div class="help-block j-description-counter"></div>
                            </div>
                        </div>
                    </div>

                    <table class="table se-service-section-table">
                        <tr>
                            <td><?= _t('svc', 'Заказать на:'); ?></td>
                            <td>
                                <div class="se-service-section-input">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-sm j-minus">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </span>
                                        <input type="text" name="weeks[__counter__]" class="form-control input-sm j-weeks" value="1" min="1" max="10" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default btn-sm j-plus">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <?= _t('svc', 'недель'); ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><?= _t('svc', 'Сумма:'); ?></td>
                            <td><span><strong class="j-sum">0 <?= $cur ?></strong></span></td>
                        </tr>
                    </table>
                </div>
            </div>

        </section>

    </div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jSvcStairsSelect.init(<?= func::php2js(array(
            'lang' => array(
                'cur' => $cur,
                'buy' => _t('svc', 'Пополнить на'),
                'publish' => _t('svc', 'Опубликовать за'),
                'success' => _t('svc', 'Услуга успешно активирована'),
                'maxlength_left' => _t('users','[symbols] осталось'),
                'maxlength_symbols' => explode(';', _t('users', 'знак;знака;знаков')),
            ),
            'balance' => $user['balance'],
            'i' => $i,
            'price' => array(
                'main' => $svc['price_main'],
                'user' => $svc['price_user'],
                'spec' => $svc['price_spec'],
                'cat'  => (isset($svc['price_cat'])?$svc['price_cat']:0),
            ),
            'spec_main' => Users::SVC_STAIRS_MAIN,
            'spec_user' => Users::SVC_STAIRS_USER,
            'cat_id'    => $cat_id,
            'spec_id'   => $spec_id,
            'message_length' => 80,
            'descr_length'   => 400,
        )) ?>);
    });
    <? js::stop(); ?>
</script>