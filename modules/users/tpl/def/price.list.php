<?php
    if( ! empty($list)):
        if( ! isset($aSecs)) {
            $aSecs = array();
            if (Specializations::catsOn()) {
                foreach ($specs as $v) {
                    if (!empty($v['specs'])) {
                        $aSecs += $v['specs'];
                    }
                }
            } else {
                $aSecs = $specs;
            }
        }
        if ($bPriceAdmin) {
            $sHead = '<thead class="hidden-xs">
                    <tr>
                        <th>'._t('users', 'Услуга').'</th>
                        <th class="se-price-list-table-price text-left">'._t('', 'Средняя стоимость').'</th>
                    </tr>
                  </thead>';
        } else {
            $bMin = $aConfig['specs_services_price_author_rows'] & Specializations::SERVICES_PRICE_AUTHOR_ROWS_MIN;
            $bAvg = $aConfig['specs_services_price_author_rows'] & Specializations::SERVICES_PRICE_AUTHOR_ROWS_AVG;
            $bMax = $aConfig['specs_services_price_author_rows'] & Specializations::SERVICES_PRICE_AUTHOR_ROWS_MAX;
            $sHead = '<thead class="hidden-xs">
                    <tr>
                        <th>'._t('users', 'Услуга').'</th>
                        '.($bMin ? '<th class="se-price-list-table-price">'._t('users', 'Мин. цена').'</th>' : '').'
                        '.($bMax ? '<th class="se-price-list-table-price">'._t('users', 'Макс. цена').'</th>' : '').'
                        '.($bAvg ? '<th class="se-price-list-table-price">'._t('users', 'Средняя цена').'</th>' : '').'
                    </tr>
                  </thead>';
        }
        ?>
        <ul class="se-price-list mrgt30">
            <? foreach($list as $k => $v): ?>
            <li>
                <h2><?= ! empty($aSecs[$k]['title']) ? $aSecs[$k]['title'] : '' ?></h2>
                <table class="table se-price-list-table">
                    <?= $sHead ?>
                    <tbody>
                    <? foreach($v as $vv): ?>
                    <tr>
                        <? if($bPriceAdmin): ?>
                            <td class="se-price-list-table-name">
                                <div><? if(empty($vv['link'])): ?><?= $vv['title'] ?><? else: ?><a href="<?= $vv['link'] ?>"><?= $vv['title'] ?></a><? endif; ?></div>
                            </td>
                            <td class="se-price-list-table-price text-left">
                                <strong><?= tpl::formatPrice($vv['price']); ?> <?= Site::currencyData($vv['price_curr'],'title_short') ?></strong> <span>/ <?= $vv['measure'] ?></span>
                            </td>
                        <? else: ?>
                            <td class="se-price-list-table-name">
                                <div><? if(empty($vv['link'])): ?><?= $vv['title'] ?><? else: ?><a href="<?= $vv['link'] ?>"><?= $vv['title'] ?></a><? endif; ?></div>
                                <span><?= $vv['measure'] ?></span>
                            </td>
                            <? if($bMin): ?>
                            <td class="se-price-list-table-price">
                                <span class="visible-xs"><?= _t('', 'мин'); ?></span> <strong><?= $vv['price_min'] ?> <?= Site::currencyData($vv['price_curr'],'title_short') ?></strong>
                            </td>
                            <? endif; if($bMax): ?>
                            <td class="se-price-list-table-price">
                                <span class="visible-xs"><?= _t('', 'макс'); ?></span> <strong><?= $vv['price_max'] ?> <?= Site::currencyData($vv['price_curr'],'title_short')?></strong>
                            </td>
                            <? endif; if($bAvg): ?>
                            <td class="se-price-list-table-price">
                                <span class="visible-xs"><?= _t('', 'в среднем'); ?></span>
                                <strong><?= $vv['price_avg'] ?> <?= Site::currencyData($vv['price_curr'],'title_short')?></strong>
                            </td>
                        <? endif; endif; ?>
                    </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </li>
            <? endforeach; ?>
        </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('users', 'Услуги не найдены'); ?></div>
<? endif;
