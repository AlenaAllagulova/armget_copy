<?php
?>
<div class="p-additional-info-parameters">
    <h6><?= ! empty($priceSett['price_title']) ? $priceSett['price_title'] : _t('users', 'Цена') ?></h6>
    <div class="form-group">
        <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="<?= $prefix?>[<?= $spec ?>][price]" value="<?= ! empty($values['price']) ? $values['price'] : '' ?>" />
        </div>

        <? if( ! empty($priceSett['rates'])): ?>
        <div class="col-sm-4">
            <select  class="form-control input-sm" name="<?= $prefix?>[<?= $spec ?>][price_rate]"><?= HTML::selectOptions($priceSett['rates'], ( ! empty($values['price_rate']) ? $values['price_rate'] : 0) ) ?></select>
        </div>
        <? endif; ?>
        <div class="col-sm-4">
            <select  class="form-control input-sm" name="<?= $prefix?>[<?= $spec ?>][price_curr]"><?= Site::currencyOptions( ! empty($values['price_curr']) ? $values['price_curr'] : ( ! empty($priceSett['curr']) ? $priceSett['curr'] : Site::currencyDefault('id'))) ?></select>
        </div>
    </div>
</div>
<div class="p-additional-info-parameters">
    <h6><?= _t('users', 'Бюджет от:') ?></h6>
    <div class="form-group">
        <div class="col-sm-4">
            <input type="text" class="form-control input-sm" name="<?= $prefix?>[<?= $spec ?>][budget]" value="<?= ! empty($values['budget']) ? $values['budget'] : '' ?>" />
        </div>
        <div class="col-sm-4">
            <select class="form-control input-sm" name="<?= $prefix?>[<?= $spec ?>][budget_curr]"><?= Site::currencyOptions( ! empty($values['budget_curr']) ? $values['budget_curr'] : ( ! empty($priceSett['curr']) ? $priceSett['curr'] : Site::currencyDefault('id'))) ?></select>
        </div>
    </div>
</div>