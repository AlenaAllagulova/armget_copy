<?php
$nUserID = User::id();
$cur = Site::currencyDefault();

?>
<div class="se-stairs-users visible-lg j-desktop">

    <? $isUp = true; $pos = 0; foreach($list as $v): $bMy = $v['user_id'] == $nUserID; if($bMy){ $isUp = false;} $pos++; ?>
    <div class="se-stairs-user-block j-block<?= $bMy ? ' se-block-self j-my' : '' ?>" data-sum="<?= $v['sum'] ?>" data-pos="<?= $pos ?>" <?= $bMy ? 'data-sumorig="'.$v['sum'].'"' : '' ?>>
        <div class="media">
            <div class="se-stairs-user-up">
                <div><div>
                    <h3<?= $bMy ? ' class="j-sum-sum"' : '' ?>><?= round($v['sum']) ?></h3>
                    <? if($isUp): ?><a href="#" class="j-current"><?= _t('', 'Хочу сюда'); ?></a><? endif; ?>
                    <? if($bMy): ?>
                    <a href="#"><i class="fa fa-arrow-up j-up"></i></a>
                    <span class="j-sum">0 <?= $cur ?></span>
                    <a href="#"><i class="fa fa-arrow-down j-down"></i></a>
                    <? endif; ?>
                </div></div>
            </div>
            <div class="media-body">
                <div class="media">
                    <div class="l-inside">
                        <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                            <?= tpl::userAvatar($v) ?>
                        </a>
                        <div class="media-body">
                            <div class="f-freelancer-name">
                                <?= tpl::userLink($v, 'no-login') ?>
                            </div>
                            <strong><?= $v['title'] ?></strong>
                            <div><?= nl2br($v['message']) ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? endforeach; ?>

</div>

<div class="se-stairs-users hidden-lg j-mobile">

    <? if(false): ?>
    <a href="#" class="se-stairs-show_users ajax-link">
        <span>показать 5 из 20 выше</span>
    </a>
    <? endif; ?>

    <? $isUp = true; $pos = 0; foreach($list as $v): $bMy = $v['user_id'] == $nUserID; if($bMy){ $isUp = false;} $pos++; ?>
    <div class="se-stairs-user-block j-block<?= $bMy ? ' se-block-self j-my' : '' ?>" data-sum="<?= $v['sum'] ?>" data-pos="<?= $pos ?>" <?= $bMy ? 'data-sumorig="'.$v['sum'].'"' : '' ?>>

        <div class="media">
            <div class="se-stairs-user-up">
                <div><div>
                    <h3<?= $bMy ? ' class="j-sum-sum"' : '' ?>><?= round($v['sum']) ?></h3>
                    <? if($isUp): ?><a href="#" class="j-current"><?= _t('', 'Хочу сюда'); ?></a><? endif; ?>
                    <? if($bMy): ?>
                        <a href="#"><i class="fa fa-arrow-up j-up"></i></a>
                        <span class="j-sum">0 <?= $cur ?></span>
                        <a href="#"><i class="fa fa-arrow-down j-down"></i></a>
                    <? endif; ?>
                </div></div>
            </div>
            <div class="media-body">
                <div class="media">
                    <div class="l-inside">
                        <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                            <?= tpl::userAvatar($v) ?>
                        </a>
                        <div class="media-body">
                            <div class="f-freelancer-name">
                                <?= tpl::userLink($v, 'no-login') ?>
                            </div>
                            <strong><?= $v['title'] ?></strong>
                            <div><?= nl2br($v['message']) ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <? endforeach; ?>

    <? if(false): ?>
    <a href="#" class="se-stairs-show_users ajax-link">
        <span>показать 5 из 8 ниже</span>
    </a>
    <? endif; ?>

</div>
