<?php

?>
<tr>
    <td>
        <?= ! empty($priceSett['price_title']) ? $priceSett['price_title'] : 'Цена' ?>:
    </td>
    <td>
        <input type="text" name="<?= $prefix?>[<?= $spec ?>][price]" value="<?= ! empty($values['price']) ? $values['price'] : '' ?>" style="width: 65px;" />
        <? if( ! empty($priceSett['rates'])): ?>
            <select name="<?= $prefix?>[<?= $spec ?>][price_rate]" style="width: 100px;"><?= HTML::selectOptions($priceSett['rates'], ( ! empty($values['price_rate']) ? $values['price_rate'] : 0) ) ?></select>
        <? endif; ?>
        <select name="<?= $prefix?>[<?= $spec ?>][price_curr]" style="width: 55px;"><?= Site::currencyOptions( ! empty($values['price_curr']) ? $values['price_curr'] : ( ! empty($priceSett['curr']) ? $priceSett['curr'] : Site::currencyDefault('id'))) ?></select>
    </td>
</tr>
<tr>
    <td>
        Бюджет от:
    </td>
    <td>
        <input type="text" name="<?= $prefix?>[<?= $spec ?>][budget]" value="<?= ! empty($values['budget']) ? $values['budget'] : '' ?>" style="width: 65px;" />
        <select name="<?= $prefix?>[<?= $spec ?>][budget_curr]" style="width: 55px;"><?= Site::currencyOptions( ! empty($values['budget_curr']) ? $values['budget_curr'] : ( ! empty($priceSett['curr']) ? $priceSett['curr'] : Site::currencyDefault('id'))) ?></select>
    </td>
</tr>