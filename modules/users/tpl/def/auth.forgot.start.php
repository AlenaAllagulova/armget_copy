<?php
/**
 * Восстановление пароля. Шаг1: Инициация восстановления пароля по E-mail адресу
 * @var $this Users
 */
?>
<div class="s-signBlock-form">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="s-signBlock-div">

                <form action="" class="form" role="form" id="j-u-forgot-start-form">
                    <input type="hidden" name="social" value="<?= $social ?>" />
                    <div class="form-group">
                        <label for="j-u-forgot-start-email"><?= _t('users', 'Электронная почта или логин') ?> <i class="text-danger">*</i></label>
                        <input type="text" class="form-control j-required" name="email" id="j-u-forgot-start-email" placeholder="<?= _t('users', 'Введите ваш email или логин') ?>" maxlength="100" />
                    </div>
                    <button type="submit" class="btn btn-primary"><?= _t('users', 'Восстановить пароль') ?></button>

                </form>

            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="s-signBlock-div">
                <div class="s-signBlock-other">
                    <div class="row">
                        <div class="col-sm-6">
                            <h6><?= _t('users', 'Впервые на сайте?') ?></h6>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a class="btn btn-default btn-sm" href="<?= Users::url('register') ?>"><?= _t('users', 'Зарегистрироваться') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.forgotStart(<?= func::php2js(array(
        'lang' => array(
            'email' => _t('users', 'E-mail адрес указан некорректно'),
            'success' => _t('users', 'На ваш электронный ящик были высланы инструкции по смене пароля.'),
        ),
    )) ?>);
});
<? js::stop(); ?>
</script>