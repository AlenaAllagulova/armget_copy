<?php
?>
<div class="modal fade" id="j-stat-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-lg">
        <div class="modal-content v-stat-popup">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= _t('', 'Закрыть'); ?></span></button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('', 'Статистика исполнителя'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="v-stat-popup__graph" id="j-stat-chart"></div>
                    </div>
                    <div class="col-md-4 v-stat-popup__info">
                        <p>
                            <?= _t('users', 'Просмотров сегодня:'); ?> <?= $today ?><br>
                            <?= _t('users', 'Просмотров всего:'); ?> <?= $total ?>
                        </p>
                        <p><?= _t('users', 'С [from] по [to]', array('from' => tpl::date_format2($from), 'to' => tpl::date_format2($to))); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
