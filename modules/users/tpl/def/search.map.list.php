<?php
if( ! empty($list)):
    $aStatus = Users::aWorkerStatus();
    $tagsLimit = Users::searchTagsLimit();
    ?>
    <ul class="o-freelancersList media-list">
    <? foreach($list as $k => $v): ?>
    <li class="media">
        <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
            <?= tpl::userAvatar($v) ?>
        </a>
        <div class="media-body">
            <div class="row o-freelancer-info">
                <div class="col-sm-6">
                    <div>
                        <strong><?= tpl::userLink($v); ?></strong>
                        <? if($v['status'] != Users::WORKER_STATUS_NONE): ?>
                            <span class="label <?= $aStatus[ $v['status'] ]['c'] ?>">
                                    	<?= $aStatus[ $v['status'] ]['t'] ?>
                                </span>
                        <? endif; ?>
                    </div>
                    <div>
                        <span class="hidden-sm hidden-xs"><?= _t('users', 'Специализация:'); ?></span> <a href="<?= Users::url('search-spec', $v) ?>"><?= $v['main_spec_title'] ?></a>
                    </div>
                    <? if( ! empty($v['city_data']['title'])): ?>
                        <div>
                            <i class="fa fa-map-marker"></i> <?= $v['city_data']['title'] ?><?= ! empty($v['country_data']['title']) ? ', '.$v['country_data']['title'] : '' ?>
                        </div>
                    <? endif; ?>
                </div>

                <div class="col-sm-3">
                    <div class="o-feedbacks-inf">
                        <?= _t('opinions', 'Opinions:'); ?> <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                    </div>
                    <div>
                        <?= _t('users', 'Рейтинг:'); ?> <strong><?=  Users::rating($v['rating'], $v['pro']) ?></strong>
                    </div>
                </div>

                <div class="col-sm-3">
                    <? if( ! empty($v['main_price'])): ?>
                        <div>
                            <i class="fa fa-clock-o"></i>
                            <?= ! empty($v['main_spec_price_title']) ? $v['main_spec_price_title'] : _t('users', 'Цена'); ?><?=
                            ! empty($v['main_price_rate_text'][LNG]) ? ' '.$v['main_price_rate_text'][LNG] : '' ?>:
                            <?= tpl::formatPrice($v['main_price']) ?>
                            <?= Site::currencyData($v['main_price_curr'], 'title_short'); ?>
                        </div>
                    <? endif; ?>
                    <? if( ! empty($v['main_budget'])): ?>
                        <div>
                            <i class="fa fa-dollar"></i> <?= _t('users', 'Бюджет от:'); ?>
                            <?= tpl::formatPrice($v['main_budget']) ?>
                            <?= Site::currencyData($v['main_budget_curr'], 'title_short'); ?>
                        </div>
                    <? endif; ?>
                </div>

            </div>

            <? if( ! empty($v['aTags'])): $n = 0; ?>
                <p>
                <? foreach($v['aTags'] as $vv): ?>
                    <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                    <a href="<?= Users::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
                <? endforeach;?>
                </p>
            <? endif; ?>

            <a href="#" class="ajax-link  j-map-marker" data-n="<?= $k ?>" data-id="<?= $v['user_id'] ?>"><i class="fa fa-map-marker"></i> <span><?= _t('users', 'показать на карте'); ?></span></a>
        </div>
    </li>
    <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('users', 'Исполнители не найдены'); ?></div>
<? endif;
