<?php

class SendmailModel_ extends SendmailModelBase
{

    /**
     * Формирование рассылки
     * @param $filter array фильтр для выбора получателей
     * @param bool $count только подсчет количества
     * @param array $settings параметры рассылки
     * @return bool|int
     */
    public function initMassend($filter, $count = false, $settings = array())
    {
        $filter = $this->prepareFilter($filter, 'U');
        if ($count) {
            return (int) $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);
        }
        if (empty($settings)) {
            return false;
        }

        # создадим рассылку
        $id = $this->db->insert(TABLE_MASSEND, array(
            'type'     => Sendmail::TYPE_MANUAL,
            'started'  => $this->db->now(),
            'settings' => serialize($settings),
        ));
        if (!$id) {
            return false;
        }

        $filter['bind']['massend'] = $id;
        # добавим получателей
        $this->db->exec('
            INSERT INTO '.TABLE_MASSEND_RECEIVERS.' (massend_id, user_id)
                SELECT :massend AS massend_id, U.user_id
                FROM '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);

        # рассчитаем количество
        $this->db->exec('
            UPDATE '.TABLE_MASSEND.'
            SET total = ( SELECT COUNT(*) FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id)
            WHERE id = :id', array(':id' => $id));

        return $id;
    }

    /**
     * Первая открытая рассылка
     * @return mixed
     */
    public function lastedMassend()
    {
        return $this->db->one_array('SELECT * FROM ' . TABLE_MASSEND . ' WHERE status = 0 ORDER BY finished LIMIT 1');
    }

    /**
     * Список получателей для рассылки SELECT ... FOR UPDATE
     * @param integer $massendID ID рассылки
     * @return mixed
     */
    public function massendReceivers($massendID)
    {
        if(empty($massendID)) return array();

        $users = $this->db->select_key('
            SELECT user_id, tpl_data FROM ' . TABLE_MASSEND_RECEIVERS . '
            WHERE massend_id = :id AND success = 0
            LIMIT 100
            FOR UPDATE', 'user_id', array(':id' => $massendID));

        if(empty($users)) return array();

        $receivers = $this->db->select_key('
            SELECT user_id as id, name, surname, email, login
            FROM ' . TABLE_USERS . '
            WHERE user_id IN (' . join(',', array_keys($users)) . ')', 'id');

        foreach($receivers as & $v){
            if(isset($users[ $v['id'] ])){
                $v += $users[ $v['id'] ];
                $v['tpl_data'] = func::unserialize($v['tpl_data']);
            }
        } unset($v);
        return $receivers;
    }

    /**
     * Обновления статуса получателей в рассылке
     * @param integer $massendID ID рассылки
     * @param array $receivers массив с ID получателей
     */
    public function updateReceivers($massendID, $receivers)
    {
        if(empty($massendID)) return;
        if(empty($receivers)) return;

        $this->db->update(TABLE_MASSEND_RECEIVERS, array(
            'success' => 1,
        ), array(
                'massend_id' => $massendID,
                'user_id' => $receivers,
            )
        );
    }

    /**
     * Закрытие рассылки
     * @param $massendID
     * @param bool|false $force
     */
    public function closeMassend($massendID, $force = false)
    {
        if(empty($massendID)) return;
        $data = $this->db->one_array('SELECT * FROM ' . TABLE_MASSEND . ' WHERE id = :id LIMIT 1', array(':id' => $massendID));
        if(empty($data)){
            $this->db->exec('DELETE FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id', array(':id' => $massendID));
            return;
        }

        $receivers = $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id', array(':id' => $massendID));
        if(empty($receivers)){
            $this->db->exec('DELETE FROM '.TABLE_MASSEND.' WHERE id = :id', array(':id' => $massendID));
            return;
        }

        $success = $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id AND success = 1', array(':id' => $massendID));
        if($receivers == $success || $force){
            if($data['type'] == Sendmail::TYPE_AUTO){
                $this->db->exec('DELETE FROM '.TABLE_MASSEND.' WHERE id = :id', array(':id' => $massendID));
                $this->db->exec('DELETE FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id', array(':id' => $massendID));
            }else{
                $this->db->update(TABLE_MASSEND, array(
                    'status'   => 1,
                    'finished' => $this->db->now(),
                    'success'  => $success,
                    'fail'     => $data['total'] - $success,
                ), array('id' => $massendID));
            }
        }else{
            $this->db->update(TABLE_MASSEND, array(
                'success'  => $success,
                'finished' => $this->db->now(),
            ), array('id' => $massendID));
        }
    }

}