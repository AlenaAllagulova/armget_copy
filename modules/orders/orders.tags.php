<?php

class OrdersTags_ extends bff\db\Tags
{
    protected function initSettings()
    {
        $this->tblTags = TABLE_TAGS;
        $this->postModeration = true;
        $this->lang = array(
            'list'         => _t('','Список навыков'),
            'add_title'    => _t('','Добавление навыка'),
            'add_text'     => _t('','Введите навыки, каждый с новой строки'),
            'edit'         => _t('','Редактирование навыка'),
            'replace_text' => _t('','Введите название навыка для замены'),
        );
        $this->tblTagsIn = TABLE_ORDERS_IN_TAGS;
        $this->tblTagsIn_ItemID = 'order_id';
        $this->urlItemsListing = $this->adminLink('orders_list&tag=', 'orders');
    }

    /**
     * Сохранение тегов
     * @param integer $nItemID ID заказа
     * @param mixed $mData данные
     * @return boolean
     */
    public function tagsSave($nItemID, $mData = null)
    {
        return parent::tagsSave($nItemID, $mData);

        # Дублируем связь тегов с заказом
//        $tagsID = $this->db->select_one_column('SELECT T.id
//                    FROM ' . $this->tblTags . ' T,
//                         ' . $this->tblTagsIn . ' TI
//                    WHERE TI.' . $this->tblTagsIn_ItemID . ' = :item_id
//                      AND TI.tag_id = T.id',
//            array(':item_id' => $nItemID)
//        );
//        if ( ! empty($tagsID)) {
//            $tagsID = join(',', array_map('intval', $tagsID));
//        } else {
//            $tagsID = '';
//        }
//        Orders::model()->orderSave($nItemID, array(
//            'tags' => $tagsID,
//        ));
    }

    public function formFront($nItemID, $aParams = array())
    {
        $aData = array(
            'tags'   => ($nItemID > 0 ? $this->tagsGet($nItemID) : array()),
            'params' => $aParams,
        );

        return $this->viewPHP($aData, 'form.tags');
    }
}