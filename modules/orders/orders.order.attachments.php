<?php

use bff\utils\Files;

class OrdersOrderAttachments_ extends Component
{
    /** @var integer ID пользователя */
    protected $userID = 0;

    /** @var integer ID записи */
    protected $recordID = 0;

    /**
     * Кол-во символов в генерируемой части названия сохранямого файла
     * @var integer
     */
    protected $filenameLetters = 6;

    /** @var string Путь к файлам */
    protected $path = '';
    /** @var string Путь ко временным файлам */
    protected $pathTmp = '';
    /** @var string URL к файлам */
    protected $url = '';
    /** @var string URL ко временным файлам */
    protected $urlTmp = '';

    /**
     * Раскладывать ли файлы в зависимости от recordID, по папкам folder = (recordID / 1000)
     * @var boolean
     */
    protected $folderByID = false;

    /** @var string Название таблицы для хранения данных о записи */
    protected $tableRecords = '';

    /** @var string Название таблицы для хранения данных о загруженных файлах */
    protected $tableAttachments = '';

    /** @var string Название id-столбца в таблице для хранения данных о записи */
    protected $tableRecords_id = 'id';

    /** @var string Название поля для хранения ID записи (в таблице $tableAttachments) */
    protected $fRecordID = 'item_id';

    /**
     * Максимально доступное кол-во файлов у одной записи
     * 0 - неограничено
     * @var integer
     */
    protected $limit = 5;

    /**
     * Максимально допустимый размер файла
     * @example: 5242880 - 5мб, 4194304 - 4мб, 3145728 - 3мб, 2097152 - 2мб
     */
    protected $maxSize = 4194304;

    public function __construct($nRecordID = 0)
    {
        $this->init();
        $this->setRecordID($nRecordID);
        $this->setUserID($this->security->getUserID());
        $this->initSettings();
    }

    /**
     * Инициализация настроек
     */
    public function initSettings()
    {
        $this->path = bff::path('attachments/orders');
        $this->pathTmp = bff::path('attachments/tmp');
        $this->url = bff::url('attachments/orders');
        $this->urlTmp = bff::url('attachments/tmp');

        $this->tableRecords = TABLE_ORDERS;
        $this->tableAttachments = TABLE_ORDERS_ATTACHMENTS;
        $this->fRecordID = 'order_id';

        $this->folderByID = config::sysAdmin('orders.order.attachments.folderbyid', true, TYPE_BOOL); # раскладываем файлы по папкам (id(5)=>0, id(1005)=>1, ...)
        $this->filenameLetters = config::sysAdmin('orders.order.attachments.filename.letters', 8, TYPE_UINT); # кол-во символов в названии файла
        $this->limit = Orders::attachmentsLimit(); # лимит файлов у заказа
        $this->maxSize = config::sysAdmin('orders.order.attachments.max.size', 5242880, TYPE_UINT); # 2мб (2мб: 2097152, 5мб: 5242880)
    }

    public function setRecordID($nRecordID)
    {
        $this->recordID = $nRecordID;
    }

    public function setUserID($nUserID)
    {
        $this->userID = $nUserID;
    }

    /**
     * Получение максимально допустимого размера файла
     * @param boolean $bFormat применить форматирование
     * @param boolean $bFormatExtTitle полное название объема данных (при форматировании)
     * @return mixed
     */
    public function getMaxSize($bFormat = false, $bFormatExtTitle = false)
    {
        return ($bFormat ? tpl::filesize($this->maxSize, $bFormatExtTitle) : $this->maxSize);
    }

    /**
     * Устанавливаем максимально допустимый размер
     * @param integer $nMaxSize размер в байтах
     */
    public function setMaxSize($nMaxSize)
    {
        $this->maxSize = $nMaxSize;
    }

    /**
     * Получение максимально доступного кол-ва файлов у одной записи
     * @return integer
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Установка максимально доступного кол-ва файлов у одной записи
     * @param $nLimit integer
     * @return integer
     */
    public function setLimit($nLimit)
    {
        return ($this->limit = $nLimit);
    }

    /**
     * Формирование URL изображения
     * @param array $aAttach : filename - название файла gen(N).ext, dir - # папки, srv - ID сервера
     * @param boolean $bTmp tmp-изображение
     * @return string|array URL
     */
    public function getURL($aAttach, $bTmp = false)
    {
        return ($bTmp ?
            $this->urlTmp . '0' :
            $this->url . ($this->folderByID ? (isset($aAttach['dir']) ? $aAttach['dir'] : $this->getDir()) . '/' : '') . $this->recordID) .
        $aAttach['filename'];
    }

    /**
     * Получаем данные о загруженных и сохраненных на текущий момент файлах
     * @param mixed $nCount кол-во изображений, false - если не знаем
     * @param string $sKey по какому полю строить ключи в результате
     * @return array данные о изображениях или FALSE
     */
    public function getData($nCount = false, $sKey = 'id')
    {
        if (($nCount !== false && intval($nCount) === 0) || $this->recordID === 0) {
            return array();
        }

        return $this->db->select_key('SELECT * FROM ' . $this->tableAttachments . '
                WHERE ' . $this->fRecordID . ' = :id ORDER BY num', $sKey,
            array(':id' => $this->recordID)
        );
    }

    /**
     * Сохранение порядка файлов
     * @param array $aAttachments данные об файлах array(imageID=>Filename, ...) или array(filename, ...)
     * @param boolean $bContainsTmp - $aAttachments может содержать tmp (соответственно сохраняем порядок только всех не-tmp файлов)
     * @return boolean
     */
    public function saveOrder($aAttachments, $bContainsTmp = true)
    {
        if (empty($aAttachments)) {
            return false;
        }

        $aData = $this->loadRecordData($this->recordID);
        if (empty($aData)) {
            return false;
        }

        $sqlUpdate = array();
        $i = 1;

        if ($bContainsTmp) {
            $aSavedAttachments = $this->getData($aData['attachcnt'], 'filename');
            foreach ($aAttachments as $fn) {
                if (isset($aSavedAttachments[$fn])) {
                    $attachID = (int)$aSavedAttachments[$fn]['id'];
                    $sqlUpdate[] = "WHEN $attachID THEN $i";
                    $i++;
                }
            }
        } else {
            foreach ($aAttachments as $attachID => $fn) {
                $attachID = intval($attachID);
                $sqlUpdate[] = "WHEN $attachID THEN $i";
                $i++;
            }
        }

        if (!empty($sqlUpdate)) {
            # обновляем порядок
            $this->db->exec('UPDATE ' . $this->tableAttachments . '
                                SET num = CASE id ' . join(' ', $sqlUpdate) . ' ELSE num END
                                WHERE ' . $this->fRecordID . ' = :id', array(':id' => $this->recordID)
            );
        }

        return true;
    }

    /**
     * Загрузка файла при помощи QQ-загрузчика
     * @return mixed
     */
    public function uploadQQ()
    {
        $oAttach = $this->oAttach();
        $oAttach->setAssignErrors(true);

        $mRes = $oAttach->uploadQQ();
        if ($mRes) {
            $mRes['path'] = $this->pathTmp;

            return $this->save($mRes);
        }

        return false;
    }

    /**
     * Удаление прикрепления
     * @param integer $nAttachID ID фйла
     * @param mixed $aData информация об файле $nAttachID или FALSE
     * @return boolean
     */
    public function deleteAttach($nAttachID, $aData = false)
    {
        $bSave = false;
        if ($aData === false) {
            $aData = $this->db->one_array('SELECT A.*, R.attachcnt
                                FROM ' . $this->tableAttachments . ' A,
                                     ' . $this->tableRecords . ' R
                                WHERE A.' . $this->fRecordID . ' = :id
                                  AND A.id = :attachid
                                  AND A.' . $this->fRecordID . ' = R.' . $this->tableRecords_id,
                array(':id' => $this->recordID, ':attachid' => $nAttachID)
            );

            if (empty($aData)) {
                return false;
            }

            $bSave = true;
        }

        $res = $this->db->delete($this->tableAttachments, array('id'             => $nAttachID,
                                                                $this->fRecordID => $this->recordID
            )
        );
        if (empty($res)) {
            return false;
        }

        $this->deleteFile($aData);

        if ($bSave) {
            $aUpdate = array('attachcnt = attachcnt - 1');
            $this->saveRecordData($this->recordID, $aUpdate);
        }

        return true;
    }

    /**
     * Удаление файлов прикреплений
     * @param array $aAttach информация о файле прикреплении: filename[, dir, srv]
     * @param boolean $bTmp tmp-изображение
     * @return boolean
     */
    public function deleteFile($aAttach, $bTmp = false)
    {
        if (empty($aAttach)) {
            return false;
        }
        $aAttach['filename'] = Files::cleanFilename($aAttach['filename']);
        $path = $this->getPath($aAttach, $bTmp);
        if (file_exists($path)) {
            @unlink($path);
        }
        if ($bTmp) {
            if (!empty($aAttach['id'])) {
                $this->db->delete($this->tableAttachments, array('id' => $aAttach['id'], $this->fRecordID => 0));
            } else {
                $this->db->delete($this->tableAttachments, array('filename'       => $aAttach['filename'],
                                                                 $this->fRecordID => 0
                    )
                );
            }
        }

        return true;
    }

    /**
     * Удаление tmp прикрепления(-й)
     * @param string|array $mFilename имя файла (нескольких файлов)
     * @return boolean
     */
    public function deleteTmpFile($mFilename)
    {
        if (is_array($mFilename)) {
            $nSuccess = 0;
            foreach ($mFilename as $file) {
                $res = $this->deleteFile(array('filename' => $file), true);
                if ($res) {
                    $nSuccess++;
                }
            }

            return (sizeof($mFilename) === $nSuccess);
        } else {
            return $this->deleteFile(array('filename' => $mFilename), true);
        }
    }

    /**
     * Удаление всех прикреплений связанных с записью
     * @param boolean $bUpdateQuery актуализировать ли данные о прикреплениях записи (после их удаления)
     * @return boolean
     */
    public function deleteAllAttachments($bUpdateQuery = false)
    {
        $aAttachments = $this->db->select('SELECT * FROM ' . $this->tableAttachments . ' WHERE ' . $this->fRecordID . ' = :id',
            array(':id' => $this->recordID)
        );
        if (empty($aAttachments)) {
            return false;
        } # нечего удалять

        foreach ($aAttachments as $v) {
            $this->deleteFile($v, false);
        }

        $this->db->delete($this->tableAttachments, array($this->fRecordID => $this->recordID));

        if ($bUpdateQuery) {
            $sqlUpdate = array('attachcnt' => 0);
            $this->saveRecordData($this->recordID, $sqlUpdate);
        }

        return true;
    }

    /**
     * Переносим temp-прикрепления в постоянную папку
     * @param string $sFieldname ключ в массиве $_POST, тип TYPE_ARRAY_STR
     * @param boolean $bEdit используем при редактировании записи
     * @return boolean
     */
    public function saveTmp($sFieldname = 'attach', $bEdit = false)
    {
        $this->checkFolderByID();

        $aAttach = $this->input->post($sFieldname, TYPE_ARRAY_STR);

        if ($bEdit) {
            $nNum = $this->getRecordAttachmentsMaxNum();
            $aData = $this->loadRecordData($this->recordID);
        } else {
            $nNum = 1;
            $aData = array();
        }
        if (empty($aData)) {
            $aData = array('attachcnt' => 0);
        }

        $nServerID = $this->getRandServer();
        $sRandDir = $this->getDir();
        $aAttachFile = array('filename' => '', 'dir' => $sRandDir, 'srv' => $nServerID);

        $aAttachTmp = $this->getTmpAttachments();

        $sqlInsert = array();
        $sqlNOW = $this->db->now();

        # проверяем не превышается ли limit
        if ($this->limit && ($this->limit > ($aData['attachcnt'] + sizeof($aAttach)))) {
            $nOffset = $this->limit - $aData['attachcnt'];
            $aAttachRemove = array_slice($aAttach, $nOffset);
            # сохраняем только, не превышая лимит
            $aAttach = array_slice($aAttach, 0, $nOffset);
            # остальные удаляем
            if (!empty($aAttachRemove)) {
                foreach ($aAttachRemove as $fn) {
                    $aAttachFile['filename'] = $fn;
                    if (isset($aAttachTmp[$fn])) {
                        $aAttachFile = $aAttachTmp[$fn] + $aAttachFile;
                    }
                    $this->deleteFile($aAttachFile, true);
                }
            }
        }

        $i = 0;
        foreach ($aAttach as $filename) {
            $aAttachFile['filename'] = $filename;

            $pathTmp = $this->getPath($aAttachFile, true);
            $pathSave = $this->getPath($aAttachFile, false);

            @rename($pathTmp, $pathSave);

            if (isset($aAttachTmp[$filename]['id'])) {
                $this->db->update($this->tableAttachments,
                    array(
                        $this->fRecordID => $this->recordID,
                        'user_id'        => $this->userID,
                        'filename'       => $filename,
                        'created'        => $sqlNOW,
                        'dir'            => $sRandDir,
                        'srv'            => $nServerID,
                        'num'            => $nNum++,
                    ), array('id' => $aAttachTmp[$filename]['id'])
                );
            } else {
                $aAttachFile['filename'] = $filename;

                $sqlInsert[] = array(
                    $this->fRecordID => $this->recordID,
                    'user_id'        => $this->userID,
                    'filename'       => $filename,
                    'created'        => $sqlNOW,
                    'size'           => '',
                    'origin'         => '',
                    'dir'            => $sRandDir,
                    'srv'            => $nServerID,
                    'num'            => $nNum++,
                );
            }
            $i++;
        }

        if (!empty($sqlInsert)) {
            $res = $this->db->multiInsert($this->tableAttachments, $sqlInsert);
            if (empty($res)) {
                $this->errors->set(_t('uploader', 'Ошибка сохранения данных об прикреплениях в таблицу "[table]"', array('table' => $this->tableAttachments)), true);
                $i = 0;
            }
        }

        $aData['attachcnt'] = ($bEdit ? $aData['attachcnt'] + $i : $i);

        $this->saveRecordData($this->recordID, $aData);

        return $aData;
    }

    /**
     * Получаем дату самого последнего добавленного прикрепления
     * @param boolean $buildHash сформировать hash на основе даты
     * @return integer|string
     */
    public function getLastUploaded($buildHash = true)
    {
        $lastUploaded = $this->db->one_data('SELECT MAX(created) FROM ' . $this->tableAttachments . '
                    WHERE ' . $this->fRecordID . ' = :id
                    LIMIT 1', array(':id' => $this->recordID)
        );
        if (!empty($lastUploaded)) {
            $lastUploaded = strtotime($lastUploaded);
        } else {
            $lastUploaded = mktime(0, 0, 0, 1, 1, 2000);
        }

        return ($buildHash ? $this->getLastUploadedHash($lastUploaded) : $lastUploaded);
    }

    /**
     * Формируем hash на основе даты самого последнего добавленного прикрепления
     * @return integer
     */
    public function getLastUploadedHash($lastUploaded)
    {
        $base64 = base64_encode($lastUploaded);

        return md5(strval($lastUploaded - 1000) . SITEHOST . $base64) . '.' . $base64;
    }

    /**
     * Выполняем проверку, загружались ли новые прикрепления
     * @param string $lastUploaded hash даты последнего загруженного прикрепления
     * @return boolean
     */
    public function newAttachmentsUploaded($lastUploaded)
    {
        # проверка hash'a
        if (empty($lastUploaded) || ($dot = strpos($lastUploaded, '.')) !== 32) {
            return true;
        }
        $date = intval(base64_decode(mb_substr($lastUploaded, $dot + 1)));
        if ($this->getLastUploadedHash($date) !== $lastUploaded) {
            return true;
        }
        # выполнялась ли загрузка новых изображений
        if ($this->getLastUploaded(false) > intval($date)) {
            return true;
        }

        return false;
    }

    /**
     * Получения объекта для загрузки файлов
     * @return \bff\files\Attachment
     */
    protected function oAttach()
    {
        $oAttach = new \bff\files\Attachment($this->pathTmp, $this->maxSize);
        $oAttach->setFiledataAsString(false);

        return $oAttach;
    }

    /**
     * Сохранение загруженного файла
     * @param array $aData данные о загруженном файле:
     *      path
     *      filename
     *      filesize
     *      rfilename - оригинальное название файла
     *      extension
     * @return array информация об успешно загруженном файле изображения: [extension, filename, size, srv, id, origin] или FALSE
     */
    protected function save($aData)
    {
        if (empty($aData) || empty($aData['filename'])) {
            return false;
        }
        if (!file_exists($aData['path'] . $aData['filename'])) {
            return false;
        }

        $bTmp = empty($this->recordID);

        $sFilename = func::generator($this->filenameLetters) . '.' . $aData['extension'];

        $this->checkFolderByID();

        $nServerID = $this->getRandServer();
        $sRandDir = $this->getDir();
        $aAttach = array('filename' => $sFilename, 'dir' => $sRandDir, 'srv' => $nServerID);

        $sNewFilename = $this->getPath($aAttach, $bTmp);

        @rename($aData['path'] . $aData['filename'], $sNewFilename);

        if ($bTmp) {
            $aAttachData = array(
                $this->fRecordID => 0,
                'user_id'        => 0,
                'filename'       => $sFilename,
                'created'        => $this->db->now(),
                'size'           => $aData['filesize'],
                'origin'         => $aData['rfilename'],
            );
            $nAttachID = $this->db->insert($this->tableAttachments, $aAttachData);
        } else {
            $nNum = $this->getRecordAttachmentsMaxNum();

            $aAttachData = array(
                $this->fRecordID => $this->recordID,
                'user_id'        => $this->userID,
                'filename'       => $sFilename,
                'created'        => $this->db->now(),
                'size'           => $aData['filesize'],
                'origin'         => $aData['rfilename'],
                'dir'            => $sRandDir,
                'srv'            => $nServerID,
                'num'            => $nNum,
            );

            $nAttachID = $this->db->insert($this->tableAttachments, $aAttachData);

            if (empty($nAttachID)) {
                return false;
            }

            $aUpdate = array(
                'attachcnt = attachcnt + 1',
            );

            $this->saveRecordData($this->recordID, $aUpdate);
        }

        return array(
            'id'        => $nAttachID,
            'filename'  => $sFilename,
            'extension' => $aData['extension'],
            'dir'       => $sRandDir,
            'srv'       => $nServerID,
            'size'      => $aData['filesize'],
            'origin'    => $aData['rfilename'],
        );
    }

    /**
     * Выполняем проверку существования папки (формируемую по ID)
     * Если нет - создаем.
     */
    protected function checkFolderByID()
    {
        // динамические папки используются
        if ($this->folderByID &&
            // изображение не tmp
            !empty($this->recordID) &&
            // ID записи приближается к окончанию текущей 1000
            (($this->recordID - (intval($this->recordID / 1000) * 1000)) > 950)
        ) {
            $sDir = $this->path . intval(($this->recordID + 100) / 1000);
            if (!file_exists($sDir)) {
                Files::makeDir($sDir);
            }
        }
    }

    /**
     * Формирование ID сервера хранения изображения
     * @return integer ID сервера
     */
    protected function getRandServer()
    {
        return 1;
    }

    /**
     * Формирование название директории исходя из ID записи
     * @return string название директории
     */
    protected function getDir()
    {
        return (string)intval($this->recordID / 1000);
    }

    /**
     * Формирование пути к файлу
     * @param array $aAttach : filename - название файла gen(N).ext, dir - # папки, srv - ID сервера
     * @param boolean $bTmp tmp-файл
     * @return string Путь
     */
    protected function getPath($aAttach, $bTmp = false)
    {
        return ($bTmp ?
            $this->pathTmp . '0' :
            $this->path . ($this->folderByID ? (isset($aAttach['dir']) ? $aAttach['dir'] : $this->getDir()) . DS : '') . $this->recordID) .
        $aAttach['filename'];
    }

    /**
     * Получаем наибольший num текущих файлов записи + 1
     * @return integer Num
     */
    protected function getRecordAttachmentsMaxNum()
    {
        return ((int)$this->db->one_data('SELECT MAX(num) FROM ' . $this->tableAttachments . '
                    WHERE ' . $this->fRecordID . ' = :id', array(':id' => $this->recordID)
        )) + 1;
    }

    /**
     * Сохраняем данные о записи
     * @param integer $nRecordID ID записи
     * @param array $aRecordData данные
     * @return mixed
     */
    protected function saveRecordData($nRecordID, array $aRecordData)
    {
        return $this->db->update($this->tableRecords, $aRecordData, array(
                $this->tableRecords_id => $nRecordID
            )
        );
    }

    /**
     * Получаем данные о записи
     * @param integer $nRecordID ID записи
     * @return array
     */
    protected function loadRecordData($nRecordID)
    {
        return $this->db->one_array('SELECT ' . $this->tableRecords_id . ', attachcnt
                    FROM ' . $this->tableRecords . '
                    WHERE ' . $this->tableRecords_id . ' = :id',
            array(':id' => $nRecordID)
        );
    }

    /**
     * Получаем данные о временных прикреплениях
     * @return array
     */
    protected function getTmpAttachments()
    {
        return $this->db->select_key('SELECT * FROM ' . $this->tableAttachments . '
                    WHERE ' . $this->fRecordID . ' = 0', 'filename'
        );

    }

}