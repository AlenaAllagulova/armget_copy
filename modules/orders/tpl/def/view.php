<?php
    /**
     * Просмотр заказа
     * @var $this Orders
     */
    tpl::includeJS('orders.order.view', false, 4);

    $bMapEnabled = Orders::mapEnabled() && ($addr_lat != 0) && ($addr_lng != 0);
    if ($bMapEnabled) {
        Geo::mapsAPI(true);
    }

    if( ! empty($regions)){
        $aFirstRegions = reset($regions);
    }

    $bOwner = User::isCurrent($user_id);

    $bAnonymous = true;
    switch(Orders::ordersContactsView()){
        case Orders::ORDERS_CONTACTS_VIEW_ALL:
            $bAnonymous = false;
            break;
        case Orders::ORDERS_CONTACTS_VIEW_AUTH:
            if(User::id()){
                $bAnonymous = false;
            }
            break;
        case Orders::ORDERS_CONTACTS_VIEW_PRO:
            if(User::isPro()){
                $bAnonymous = false;
            }
            break;
    }
    $bDeleteAllow = Orders::deleteAllow();
    $ordersOpinions = Orders::ordersOpinions();
    $invitesEnabled = Orders::invitesEnabled();
    $fairplayEnabled = bff::fairplayEnabled();
?>
    <div class="container">

        <?= tpl::getBreadcrumbs($breadcrumbs); ?>

        <section class="l-mainContent">
            <div class="row">

            <!-- Project-->
            <div class="col-md-9" id="j-order-view">

                <div class="l-borderedBlock">

                    <div class="l-inside o-clientInfo">

                        <? if($bAnonymous): ?>
                            <div class="o-client-avatar">
                                <img src="<?= UsersAvatar::url(0, '', UsersAvatar::szSmall); ?>" alt="<?= tpl::avatarAlt(); ?>" />
                            </div>
                            <div class="o-client-about">
                                <div>
                                    <?= _t('orders', 'Заказчик')?>
                                </div>
                                <div class="visible-lg visible-md visible-sm">
                                    <?= _t('orders', 'Зарегистрирован на сайте') ?> <?= tpl::date_format_spent($user_created, false, false)?>
                                </div>
                                <div class="o-feedbacks-inf">
                                    <?= _t('orders', 'Отзывы фрилансеров:') ?> <?= tpl::opinions($opinions_cache, array('login' => $login), true) ?>
                                </div>
                            </div>
                        <? else: ?>
                            <a href="<?= Users::url('profile', array('login' => $login)) ?>" class="o-client-avatar">
                                <img src="<?= $avatar_small ?>" alt="<?= tpl::avatarAlt($aData); ?>" />
                                <?= tpl::userOnline($aData) ?>
                            </a>
                            <div class="o-client-about">
                                <div>
                                    <?= tpl::userLink($aData, 'no-login') ?>
                                </div>
                                <div class="visible-lg visible-md visible-sm">
                                    <?= _t('orders', 'Зарегистрирован на сайте') ?> <?= tpl::date_format_spent($user_created, false, false)?>
                                </div>
                                <div class="o-feedbacks-inf">
                                    <?= _t('orders', 'Отзывы фрилансеров:') ?> <?= tpl::opinions($opinions_cache, array('login' => $login)) ?>
                                </div>
                            </div>

                            <?= Users::i()->note($user_id); ?>
                        <? endif; ?>

                    </div>

                    <? if($bOwner) echo $this->orderStatusBlock($aData); ?>

                    <div class="l-inside">

                        <header class="title-type-2 j-title">
                            <? if($price_ex == Specializations::PRICE_EX_AGREE): ?>
                                <div class="o-project-price_na o-project-price_hide-sm"><?= ! empty($price_rate_text[LNG]) ? $price_rate_text[LNG] : _t('orders', 'Договорная цена'); ?></div>
                            <? else: ?>
                                <div class="o-project-price o-project-price_hide-sm"><?= tpl::formatPrice($price) ?> <?= Site::currencyData($price_curr, 'title_short'); ?> <? if( ! empty($price_rate_text[LNG])): ?><small><?= $price_rate_text[LNG] ?></small><? endif; ?></div>
                            <? endif; ?>
                            <h1><? if($invitesEnabled && $visibility == Orders::VISIBILITY_PRIVATE): ?><i class="fa fa-eye-slash show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Приватный заказ'); ?>"></i> <? endif;
                                   if($status != Orders::STATUS_OPENED): ?><i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i><? endif;
                                   if($fairplayEnabled && $fairplay == Orders::FAIRPLAY_USE): ?><i data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip" class="fa fa-shield c-safe-color show-tooltip"></i><? endif; ?>
                                <?= $title ?></h1>
                            <? if($price_ex == Specializations::PRICE_EX_AGREE): ?>
                                <div class="o-project-price_na o-project-price_show-sm"><?= ! empty($price_rate_text[LNG]) ? $price_rate_text[LNG] : _t('orders', 'Договорная цена'); ?></div>
                            <? else: ?>
                                <div class="o-project-price o-project-price_show-sm"><?= tpl::formatPrice($price) ?> <?= Site::currencyData($price_curr, 'title_short'); ?> <? if( ! empty($price_rate_text[LNG])): ?><small><?= $price_rate_text[LNG] ?></small><? endif; ?></div>
                            <? endif; ?>
                            <div class="clearfix"></div>
                            <? if($expire != '0000-00-00 00:00:00'): ?>
                            <div class="mrgb10">
                                <i class="fa fa-calendar"></i> <strong><?= _t('orders', 'Предложения принимаются до [date]', array('date' => tpl::date_format3($expire, 'd.m.Y'))); ?></strong>
                            </div>
                            <? endif; ?>

                            <? if($performer_id): ?>
                                <? if($bOwner && $ordersOpinions && ! empty($performer_offer['status']) && $performer_offer['status'] == Orders::OFFER_STATUS_PERFORMER_START):
                                    if( ! empty($performer_offer['workflow'])){ $aData['workflow'] = func::unserialize($performer_offer['workflow']); }
                                    $aData['offerID'] = $performer_offer['id']; ?>
                                    <?= $this->viewPHP($aData, 'offers.list.alert.performer.start'); ?>
                                <? else: ?>
                                    <div class="l-project-author mrgt0">
                                        <?= _t('orders', 'Исполнитель:'); ?> <span class="nowrap"><?= tpl::userLink($performer, 'no-login') ?></span>
                                    </div>
                                <? endif; ?>
                            <? endif; ?>


                        </header>

                        <article>
                            <?= nl2br($descr) ?>
                        </article>

                        <? if($bMapEnabled): ?>
                            <div class="l-mapView">
                                <div id="map-desktop" class="map-google"></div>
                            </div>
                        <? endif; ?>

                        <? if(Orders::imagesLimit() && $aData['imgcnt']): ?>
                            <div id="j-order-view-carousel" class="owl-carousel l-carousel l-images-carousel">
                                <? foreach($images as $k => $v): ?>
                                <div class="item">
                                    <a href="<?= $v['i'][ OrdersOrderImages::szView ]; ?>" class="fancyzoom" rel="fancy-gallery-<?= $id ?>">
                                        <img src="<?= $v['i'][ OrdersOrderImages::szSmall ]; ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" />
                                    </a>
                                </div>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>

                        <? if(Orders::attachmentsLimit() && $aData['attachcnt']): ?>
                        <ul class="o-order-files">
                            <? foreach($attachments as $v): ?>
                            <li><a href="<?= $v['i'] ?>" target="_blank"><i class="fa fa-file-o c-li c-link-icon"></i><span><?= $v['origin'] ?></span></a> (<?= $v['size'] ?>)</li>
                            <? endforeach; ?>
                        </ul>
                        <? endif; ?>

                    </div>

                    <? if( ! empty($tags)): ?>
                    <div class="l-inside">
                        <? foreach($tags as $v): ?>
                            <a href="<?= Orders::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                        <? endforeach; ?>
                    </div>
                    <?  endif; ?>

                    <? if( ! empty($dynprops)): ?><div class="l-inside l-dymanic-features"><?= $dynprops ?></div><? endif; ?>

                    <? if($contacts):
                        $aContacts = func::unserialize($contacts);
                        $aTypes = Users::aContactsTypes();
                        foreach($aContacts as $vv){
                            $aTypes[ $vv['t'] ]['links'][] = tpl::linkContact($vv, true);
                        } if ( ! empty($aContacts)) { ?>
                        <div class="l-inside">
                            <div class="o-collapsed-info">
                                <button type="button" class="o-collapsed-info_toggle btn btn-default" data-toggle="collapse" data-target="#o-contact-info-<?= $user_id ?>"><i class="fa fa-chevron-down pull-right"></i><?= _t('', 'Контакты'); ?></button>
                                <div class="collapse" id="o-contact-info-<?= $user_id ?>">
                                    <div class="o-collapsed-inner">
                                        <h6><?= _t('orders', 'Контакты'); ?></h6>
                                        <? if(Users::isContactsView($user_id, $user_type, $pro, $contactsHideReason, 'mrgb0')): ?>
                                        <!--noindex-->
                                        <table class="table table-condensed">
                                            <tbody>
                                            <? foreach($aTypes as $vv): if( empty( $vv['links'])) continue;?>
                                                <tr>
                                                    <td><?= $vv['t'] ?>:</td>
                                                    <td>
                                                        <?= join('<br />', $vv['links']) ?>
                                                    </td>
                                                </tr>
                                            <? endforeach; ?>
                                            </tbody>
                                        </table>
                                        <!--/noindex-->
                                        <? else: ?>
                                            <?= $contactsHideReason ?>
                                        <? endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? } endif; ?>

                    <? if($bOwner):
                        $openClose = in_array($status, array(Orders::STATUS_OPENED, Orders::STATUS_CLOSED));
                        if($openClose && $ordersOpinions && ! empty($performer_offer['status']) && $performer_offer['status'] == Orders::OFFER_STATUS_PERFORMER_AGREE){
                            $openClose = false;
                        }
                        ?>
                        <div class="l-inside p-profileOrder-controls-mobile">
                            <? if($fairplayEnabled && ! empty($workfow['url'])): ?><a href="<?= $workfow['url'] ?>"><i class="fa fa-rocket c-link-icon"></i></a><? endif; ?>
                            <a href="<?= Orders::url('edit', array('id' => $id))?>"><i class="fa fa-pencil c-link-icon"></i></a>
                            <? if(bff::servicesEnabled() && $visibility == Orders::VISIBILITY_ALL && $status == Orders::STATUS_OPENED): ?><a href="<?= Orders::url('promote', array('id' => $id))?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i></a><? endif; ?>
                            <? if($openClose): ?>
                                <a href="#" class="<?= $status != Orders::STATUS_OPENED ? 'hidden' : '' ?> j-hide"><i class="fa fa-lock c-link-icon"></i></a>
                                <a href="#" class="<?= $status != Orders::STATUS_CLOSED ? 'hidden' : '' ?> j-show"><i class="fa fa-unlock c-link-icon"></i></a>
                                <? if($bDeleteAllow): ?><a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i></a><? endif; ?>
                            <? endif; ?>
                        </div>
                        <div class="l-inside p-profileOrder-controls o-controls">
                            <? if($fairplayEnabled && ! empty($workfow['url'])): ?><a href="<?= $workfow['url'] ?>"><i class="fa fa-rocket  c-link-icon"></i><?= _t('fp', 'Ход работ'); ?></a><? endif; ?>
                            <a href="<?= Orders::url('edit', array('id' => $id))?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                            <? if(bff::servicesEnabled() && $visibility == Orders::VISIBILITY_ALL && $status == Orders::STATUS_OPENED): ?><a href="<?= Orders::url('promote', array('id' => $id))?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i><?= _t('', 'Рекламировать'); ?></a><? endif; ?>
                            <? if($openClose): ?>
                                <a href="#" class="<?= $status != Orders::STATUS_OPENED ? 'hidden' : '' ?> j-hide"><i class="fa fa-lock c-link-icon"></i><?= _t('orders', 'Снять с публикации'); ?></a>
                                <a href="#" class="<?= $status != Orders::STATUS_CLOSED ? 'hidden' : '' ?> j-show"><i class="fa fa-unlock c-link-icon"></i><?= _t('orders', 'Публиковать заново'); ?></a>
                                <? if($bDeleteAllow): ?><a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a><? endif; ?>
                            <? endif; ?>
                        </div>
                    <? else: if( ! empty($workfow['url'])): ?>
                        <div class="l-inside p-profileOrder-controls-mobile">
                            <a href="<?= $workfow['url'] ?>"><i class="fa fa-rocket c-link-icon"></i></a>
                        </div>
                        <div class="l-inside p-profileOrder-controls o-controls">
                            <a href="<?= $workfow['url'] ?>"><i class="fa fa-rocket  c-link-icon"></i><?= _t('fp', 'Ход работ'); ?></a>
                        </div>
                    <? endif; endif; ?>

                    <div class="l-inside">
                        <ul class="l-item-features">
                            <? if($order_pro): ?><li><span class="small"><?= _t('orders', 'Только для'); ?> <span class="pro">pro</span></span></li><? endif; ?>
                            <? if( ! empty($aFirstRegions['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $aFirstRegions['title']?></li><? endif; ?>
                            <? if($type == Orders::TYPE_SERVICE):?><li><i class="fa fa-<?= $serviceTypes[ $service_type ]['c'] ?>"></i> <?= $serviceTypes[ $service_type ]['t'] ?></li><? endif; ?>
                            <li><i class="fa fa-clock-o"></i> <?= tpl::date_format3($created, false, true) ?></li>
                            <? if($expire != '0000-00-00 00:00:00'): ?><li><i class="fa fa-calendar"></i> <?= _t('orders', 'до [date]', array('date' => tpl::date_format3($expire, 'd.m.Y'))); ?></li><? endif; ?>
                            <li><span><i class="fa fa-eye"></i> <?= _t('', 'Просмотров: [total]', array('total'=>$views_total)); ?></span></li>
                        </ul>
                    </div>

                </div>

                <? if($visibility == Orders::VISIBILITY_ALL):
                    $aConfig = $this->configLoad();
                    if( ! empty($aConfig['share_code'])): ?>
                        <?= $aConfig['share_code'] ?>
                    <?  endif;
                endif; ?>

                <a name="offers"></a>

                <?= $offerAddForm ?>

                <?= $offers_list ?>

            </div>

            <div class="col-md-3 visible-lg visible-md">
                <? if( ! empty($invites)): ?>
                <h5 class="mrgt0"><?= _t('orders', 'Вы предложили заказ:'); ?></h5>
                <ul class="media-list mrgb15" id="j-order-invites-list">

                    <? $cnt = 0; $limit = 3; foreach($invites as $v): $cnt++; ?>
                    <li class="media<?= $cnt > $limit ? ' hidden' : '' ?>">
                        <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                            <?= tpl::userAvatar($v) ?>
                        </a>
                        <div class="media-body">
                            <div class="o-freelancer-info">
                                <div class="f-freelancer-name">
                                    <?= tpl::userLink($v); ?>
                                </div>
                                <? if( ! empty($v['main_spec_title'])): ?><strong><?= $v['main_spec_title'] ?></strong><? endif; ?>
                                <? if( ! empty($v['city_data']['title'])): ?>
                                    <div>
                                        <i class="fa fa-map-marker"></i> <?= $v['city_data']['title'] ?><?= ! empty($v['country_data']['title']) ? ', '.$v['country_data']['title'] : '' ?>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </li>
                    <? endforeach; ?>
                </ul>
                <? if($cnt > $limit): ?>
                    <div class="text-center mrgb30">
                        <a href="#" class="ajax-link" id="j-more-invites"><span><?= _t('', 'Показать еще'); ?></span></a>
                    </div>
                <? endif; endif; ?>

                <? # Баннер: Заказы: просмотр ?>
                <?= Banners::view('orders_view', array('pos'=>'right', 'spec'=>(!empty($specs[0]['spec_id']) ? $specs[0]['spec_id'] : 0))) ?>
            </div>

            </div>
        </section>

    </div>
<script type="text/javascript">
<? js::start() ?>
jOrdersOrderView.init(<?= func::php2js(array(
    'lang' => array(
        'order_delete' => _t('orders', 'Удалить заказ'),
    ),
    'id' => $id,
    'lat' => $addr_lat,
    'lng' => $addr_lng,
    'balloon' => ! empty($addr_addr) ? '<div><i class="fa fa-map-marker"></i> '.( ! empty($aFirstRegions['title']) ? $aFirstRegions['title'].', ' : '') .$addr_addr.'</div>' : false,
)) ?>);
<? if(Orders::imagesLimit() && $aData['imgcnt']):
    tpl::includeCSS(array('owl.carousel'), true);
    tpl::includeJS('owl.carousel.min', false);
    tpl::includeJS('fancybox2', true);
?>
$(function(){
    var $carousel = $('#j-order-view-carousel');
    if($carousel.length){
        $carousel.owlCarousel({
            items : 5, //10 items above 1000px browser width
            itemsDesktop : [1000,4], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            navigation : true,
            navigationText : ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            pagination : true,
            autoPlay : false
        });
    }
    $('.fancyzoom').fancybox({
        openEffect	: 'none',
        closeEffect	: 'none',
        nextEffect  : 'fade',
        prevEffect : 'fade',
        fitToView: false,
        helpers: {
            overlay: {locked: false}
        }
    });
});
<? endif; ?>
<? js::stop() ?>
</script>