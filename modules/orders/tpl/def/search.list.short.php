<?php
if( ! empty($list)):
    $aServiceTypes = Orders::aServiceTypes();
    $fairplayEnabled = bff::fairplayEnabled();
?>
<table class="l-projectList-table">
    <tbody>
    <? foreach($list as $v): ?>
    <tr<? if($v['svc_marked']){ ?> class="l-projectList-table-highlited"<? } ?>>
        <td class="l-projectList-table-title">
            <? if($v['svc_fixed']): ?><i class="fa fa-thumb-tack l-projectList-table-fixed"></i><? endif; ?>
            <? if($fairplayEnabled && $v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
            <? if($v['type'] == Orders::TYPE_SERVICE):?><i class="fa fa-<?= $aServiceTypes[ $v['service_type'] ]['c'] ?> show-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?= $aServiceTypes[ $v['service_type'] ]['t'] ?>" data-original-title="<?= $aServiceTypes[ $v['service_type'] ]['t'] ?>"></i><? endif; ?>
            <? if($v['status'] == Orders::STATUS_CLOSED): ?><i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i><? endif; ?>
            <span><a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a></span></td>
        <td class="l-projectList-table-pro"><? if($v['pro']): ?><span class="pro">pro</span><? endif; ?></td>
        <td class="l-projectList-table-comments"><a href="<?= $v['url_view'] ?>#offers" class="ajax-link"><i class="fa fa-comments-o"></i> <?= $v['offers_cnt'] ?></a></td>
        <td class="l-projectList-table-address"><? if( ! empty($v['city_data']['title'])): ?><i class="fa fa-map-marker"></i> <?= $v['city_data']['title']?><? endif; ?></td>
        <td class="l-projectList-table-date"><?= tpl::date_format3_month($v['created']) ?></td>
    </tr>
    <? endforeach; ?>
    </tbody>
</table>
<? else: ?>
    <div class="alert alert-info"><?= _t('orders', 'Заказы не найдены'); ?></div>
<? endif;

