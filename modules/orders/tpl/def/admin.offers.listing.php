<?php
    /**
     * @var $this Orders
     */
    $aTabs = array(
        0 => array('t' => _t('orders', 'На модерации'), 'counter'=>config::get('orders_offers_moderating', 0)),
        1 => array('t' => _t('orders', 'Все')),
    );
?>
<?= tplAdmin::blockStart('Заказы / Заявки ', true, array('id'=>'OrdersOffersListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array(),
    array(
        array('title'=>_t('', 'пересчет счетчиков заявок к заказам'), 'onclick'=>"return bff.confirm('sure', {r:'".$this->adminLink(bff::$event.'&act=dev-offers-counters-update')."'})", 'icon'=>'icon-refresh', 'debug-only'=>true),
    )
); ?>

    <form action="" name="filter" class="form-inline" id="j-orders-offers-filter">
        <div class="tabsBar" id="j-orders-offers-tabs">
            <?php foreach($aTabs as $k=>$v) { ?>
                <span class="tab<?= $k== $f['tab'] ? ' tab-active' : '' ?>"><a href="#" class="j-tab" data-id="<?= $k ?>"><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
            <?php } ?>
            <label class="pull-right"><?= _t('', 'по') ?><select name="perpage" class="j-perpage" style="width: 45px; height: 20px !important; border:0;"><?= $perpage ?></select></label>
        </div>
        <div class="actionBar">
            <input type="hidden" name="s" value="<?= bff::$class ?>" />
            <input type="hidden" name="ev" value="<?= bff::$event ?>" />
            <input type="hidden" name="tab" value="<?= $f['tab'] ?>" class="j-tab-id" />
            <div class="controls controls-row">
                <? /*
                <input type="text" name="item" placeholder="ID заказа" value="<?= ($item > 0 ? $item : '') ?>" class="input-medium" />
                <input type="submit" class="btn btn-small button submit" value="<?= _t('', 'search') ?>" />
                */ ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </form>

<?= $this->viewPHP($aData, 'admin.offers.listing.list'); ?>

<?= $pgn; ?>

<?= tplAdmin::blockStop(); ?>