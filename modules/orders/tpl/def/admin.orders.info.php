<?php
/**
 * @var $this Orders
 */
?>
<div id="popupOrderInfo" class="ipopup">
    <div class="ipopup-wrapper">
        <div class="ipopup-title">Информация о заказе №<?= $id ?></div>
        <div class="ipopup-content" style="width:500px;">
            <table class="admtbl tbledit">
                <tr>
                    <td class="row1 field-title right" width="134">Пользователь:</td>
                    <td class="row2">
                        <?= ($user_id ? '<a href="#" onclick="return bff.userinfo('.$user_id.');">'.$user['email'].'</a>' : 'Аноним' ); ?>
                    </td>
                </tr>
                <tr>
                    <td class="row1 field-title right" style="vertical-align: top;">Заголовок:</td>
                    <td class="row2">
                        <?= HTML::escape($title) ?>
                        <? if($visibility == Orders::VISIBILITY_PRIVATE): ?><i class="icon icon-eye-close disabled" title="Приватный заказ"></i><? endif; ?>
                        <? if(bff::fairplayEnabled() && $fairplay == Orders::FAIRPLAY_USE): ?><i class="icon icon-fairplay" title="<?= _te('orders','Безопасная сделка') ?>"></i><? endif; ?>
                    </td>
                </tr>
                <? /*
                <tr>
                    <td class="row1 field-title right" style="vertical-align: top;">Категория:</td>
                    <td class="row2"><?
                        $i=0; $j=sizeof($cats_path);
                        foreach($cats_path as $v) {
                            echo '<a href="'.$this->adminLink('listing&cat='.$v['id']).'">'.$v['title'].'</a>'.(++$i < $j ? ' &raquo; ':'');
                        }
                        ?></td>
                </tr>
                */ ?>
                <? bff::hook('orders.admin.order.info', array('data'=>&$aData)) ?>
                <tr>
                    <td colspan="2">
                        <?
                        $aData['is_popup'] = true;
                        echo $this->viewPHP($aData, 'admin.orders.form.status');
                        ?>
                    </td>
                </tr>
            </table>
            <div class="ipopup-content-bottom">
                <ul class="right">
                    <? /* if($claims_cnt){ ?><li><a href="<?= $this->adminLink('edit&tab=claims&id='.$id); ?>" class="clr-error">есть жалобы (<?= $claims_cnt ?>)</a></li><? } */ ?>
                    <li><span class="post-date" title="дата создания"><?= tpl::date_format2($created); ?></span></li>
                    <li><a href="#" onclick="$.fancybox.close(); jOrdersOrdersFormManager.action('edit',<?= $id ?>);" class="edit_s"> редактировать <span style="display:inline;" class="desc">#<?= $id ?></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>