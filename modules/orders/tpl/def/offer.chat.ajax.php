<?php
    $nUserID = User::id();
    foreach($chat as $v): $bOwner = ($v['author_id'] == $nUserID); ?>
        <li>
            <div><?= tpl::userLink($v, 'strong no-login'.($bOwner ? ' text' : ''))?> <small><?= tpl::date_format3($v['created'])?></small>
                <? if( ! $bOwner && $v['is_new']): ?><span class="label label-new"><?= _t('', 'новое') ?></span><? endif; ?>
            </div>
            <?= nl2br($v['message']); ?>
        </li>
    <? endforeach;