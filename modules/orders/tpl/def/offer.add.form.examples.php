<?php
    $aItems = array(
        'portfolio' => array('t' => _t('', 'Портфолио'), 'a' => 0),
        'shop'      => array('t' => _t('', 'Магазин'), 'a' => 0),
    );
    if(empty($portfolio)){  unset($aItems['portfolio']);  }
    if(empty($shop)){  unset($aItems['shop']);  }
    if(empty($aItems)) return;
    $t = array_keys($aItems);
    $t = reset($t);
    $aItems[$t]['a'] = 1;
    $aSpecs = array();
    if( ! empty($specs)){
        foreach($specs as $v){
            $aSpecs[] = $v['spec_id'];
        }
    }
?>
<!-- Modal -->
<div class="modal fade" id="j-examples-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header pdb0">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title" id="myModalLabel">
                    <ul class="modal-title-tabs nav">
                        <? foreach($aItems as $k => $v): ?>
                        <li<?= $v['a'] ? ' class="active"' : ''?>><a href="#modal-tabs-<?= $k ?>" aria-controls="modal-tabs-<?= $k ?>" role="tab" data-toggle="tab"><?= $v['t'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="modal-body">

                <div class="tab-content">

                    <? if( ! empty($portfolio)): ?>
                    <div role="tabpanel" class="tab-pane active" id="modal-tabs-portfolio">
                        <div class="panel-group mrgb0" id="accordion-portfolio">
                            <? foreach($portfolio as $v): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-portfolio" href="#collapse-s-<?= $v['spec_id'] ?>" class="ajax-link">
                                        <span><?= $v['title'] ?></span>
                                    </a>
                                </div>
                                <div id="collapse-s-<?= $v['spec_id'] ?>" class="panel-collapse collapse<?= in_array($v['spec_id'], $aSpecs) ? ' in' : '' ?>">
                                    <div class="panel-body">
                                        <ul class="list-unstyled">
                                            <? foreach($v['items'] as $vv): ?>
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="j-example-portfolio" value="<?= $vv['id'] ?>" data-preview="<?= $vv['preview'] ?>"> <span><?= $vv['title'] ?></span>
                                                    </label>
                                                </div>
                                            </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <? endif; ?>

                    <? if( ! empty($shop)): ?>
                    <div role="tabpanel" class="tab-pane <?=  empty($portfolio) ? 'active' : '' ?>" id="modal-tabs-shop">
                        <div class="panel-group mrgb0" id="accordion-shop">
                            <? foreach($shop as $v): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-shop" href="#collapse-c-<?= $v['cat_id'] ?>" class="ajax-link">
                                        <span><?= $v['title'] ?></span>
                                    </a>
                                </div>
                                <div id="collapse-c-<?= $v['cat_id'] ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="list-unstyled">
                                            <? foreach($v['items'] as $vv): ?>
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="j-example-shop" value="<?= $vv['id'] ?>" data-preview="<?= $vv['img_s'] ?>"> <span><?= $vv['title'] ?></span>
                                                    </label>
                                                </div>
                                            </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <? endif; ?>
                </div>

            </div><!-- /.modal-body -->

            <div class="modal-footer">
                <button type="button" class="btn btn-primary mrgr10 j-submit"><i class="fa fa-check"></i> <?= _t('', 'Добавить'); ?></button>
                <a href="#propose" class="ajax-link" data-dismiss="modal"><span><?= _t('form', 'Отмена'); ?></span></a>
            </div>

        </div>
    </div>
</div>
