<?php
$aPriceSett = array(
    'rates' => array(),
    'price_title' => _t('orders', 'Бюджет'),
    'price_title_mod' => _t('orders', 'По договоренности'),
    'curr' => 0,
);
if( ! empty($specs)){
    $spec = reset($specs);
} else {
    if(empty($spec)) {
        $spec = array('spec_id' => 0);
    }
}
if($spec['spec_id']){
    $aPriceSett = Specializations::i()->aPriceSett($spec['spec_id']);
}
$bShowPriceEx = ! empty($aPriceSett['ex']);
?>
<tr>
    <td class="row1 field-title"><?= $aPriceSett['price_title'] ?></td>
    <td class="row2">
        <div class="input-group <?= $bShowPriceEx ? 'radio' : '' ?> j-price-ex-radio">
            <input type="radio" name="price_ex" class="<?= $bShowPriceEx ? '' : 'hidden' ?> j-price-ex j-price-ex-<?= Specializations::PRICE_EX_PRICE ?>" value="<?= Specializations::PRICE_EX_PRICE ?>" id="j-price_ex-price" <?= $price_ex == Specializations::PRICE_EX_PRICE ? ' checked="checked"' : '' ?>/>
            <input type="text" name="price" value="<?= $price ?>" maxlength="6" style="width: 65px;" onfocus="$('#j-price_ex-price').prop('checked', true);"/>
            <select class="j-price-curr" name="price_curr" style="width: 55px;" onfocus="$('#j-price_ex-price').prop('checked', true);"><?= Site::currencyOptions($price_curr) ?></select>
            <select class="<?= empty($aPriceSett['rates']) ? ' hide' : '' ?> j-price-rate" name="price_rate" style="width: auto;" onfocus="$('#j-price_ex-price').prop('checked', true);"><?= HTML::selectOptions($aPriceSett['rates'], $price_rate); ?></select>
        </div>
        <div class="<?= $bShowPriceEx && ($aPriceSett['ex'] & Specializations::PRICE_EX_AGREE)  ? '' : 'hidden' ?> j-price-ex j-price-ex-<?= Specializations::PRICE_EX_AGREE ?>">
            <label class="radio inline"><input type="radio" name="price_ex" value="<?= Specializations::PRICE_EX_AGREE ?>" <?= $price_ex == Specializations::PRICE_EX_AGREE ? ' checked="checked"' : '' ?> /><?= $aPriceSett['price_title_mod'] ?></label>
        </div>
    </td>
</tr>
