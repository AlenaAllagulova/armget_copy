<?php
$isPublic = ! empty($public);
$isPrivate = ! empty($private);
?>
<div class="modal fade" id="j-modal-offer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="j-listing">
                <div class="modal-header pdb0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title pd0">
                        <ul class="modal-title-tabs nav">
                            <li class="active"><a href="#modal-offer-tabs-new" aria-controls="modal-offer-tabs-new" role="tab" data-toggle="tab"><?= _t('orders', 'Новый'); ?></a></li>
                            <? if($isPublic): ?><li><a href="#modal-offer-tabs-public" aria-controls="modal-offer-tabs-public" role="tab" data-toggle="tab"><?= _t('orders', 'Публичные'); ?></a></li><? endif; ?>
                            <? if($isPrivate): ?><li><a href="#modal-offer-tabs-private" aria-controls="modal-offer-tabs-private" role="tab" data-toggle="tab"><?= _t('orders', 'Приватные'); ?></a></li><? endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="modal-offer-tabs-new">
                        <div class="modal-body">
                            <?= $form ?>
                        </div>
                    </div>

                    <? if($isPublic): ?>
                    <div role="tabpanel" class="tab-pane" id="modal-offer-tabs-public">
                        <div class="modal-body">
                            <?= $public ?>
                        </div>
                    </div>
                    <? endif; ?>

                    <? if($isPrivate): ?>
                    <div role="tabpanel" class="tab-pane" id="modal-offer-tabs-private">
                        <div class="modal-body">
                            <div class="help-block"><?= _t('orders', 'Приватные заказы доступны только тем, кому Вы сделаете предложение'); ?></div>
                            <?= $private ?>
                        </div>
                    </div>
                    <? endif; ?>

                </div>
            </div>
            <div class="hidden j-invite">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <?= _t('orders', 'Предложить заказ'); ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <a href="#" class="ajax-link j-another"><i class="fa fa-angle-left c-link-icon"></i><span><?= _t('orders', 'Выбрать другой заказ'); ?></span></a>
                    <ul class="o-orderSelect mrgb10 mrgt10">
                        <li class="active j-order">
                        </li>
                    </ul>
                    <div class="form-group mrgt10">
                        <label for="modal-offer-msg"><?= _t('orders', 'Ваше сообщение'); ?></label>
                        <textarea rows="4" class="form-control j-message" id="modal-offer-msg" placeholder="<?= _t('orders', 'Опишите ваши пожелания потенциальному исполнителю'); ?>"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success j-submit"><?= _t('orders', 'Предложить'); ?></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= _t('form', 'Отмена'); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>