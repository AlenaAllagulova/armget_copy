<?php
if( ! empty($ordersInvites)): ?>
<div id="j-orders-invites">
    <? foreach($ordersInvites as $v): ?>
        <div class="alert p-profile-alert-invite alert-dismissible j-invite" data-id="<?= $v['id'] ?>">
            <h4><?= _t('orders', 'Вам предложили заказ'); ?></h4>
            <div class="p-profile-alert-invite-msg">
                <h5><a href="<?= $v['link'] ?>" target="_blank"><?= $v['title'] ?></a></h5>
                <div class="l-project-terms">
                    <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                        <span class="l-project-terms-budget"><?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?></span>
                    <? else: ?>
                        <span class="l-project-terms-budget"><strong><?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><?= $v['price_rate_text'][LNG] ?><? endif; ?></strong></span>
                    <? endif; ?>
                </div>
                <div class="l-project-author">
                    <?= _t('orders', 'Заказчик:'); ?> <?= tpl::userLink($v, 'no-login') ?>
                </div>
                <? if( ! empty($v['message'])): ?>
                    <div class="alert-in">
                        <div class="alert-in-title"><?= _t('orders', 'Сообщение от заказчика:'); ?></div>
                        <div><?= nl2br($v['message']) ?></div>
                    </div>
                <? endif; ?>
                <ul class="l-project-bs-content-controls">
                    <li><button class="btn btn-success j-add-offer" data-id="<?= $v['id'] ?>"><?= _t('orders', 'Обсудить условия'); ?></button></li>
                    <li><?= _t('', 'или'); ?> <a href="#j-modal-decline-<?= $v['id'] ?>" data-toggle="modal" class="ajax-link link-hover-red"><span><?= _t('orders', 'отказаться от него'); ?></span></a></li>
                </ul>
            </div>
        </div>
    <? endforeach; ?>
    <? foreach($ordersInvites as $v): ?>
        <div class="modal fade" id="j-modal-decline-<?= $v['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?= _t('orders', 'Отказаться от предложения'); ?></h4>
                    </div>
                    <form method="post">
                        <div class="modal-body">
                            <p><?= _t('orders', 'Вы уверены, что хотите отказаться от заказа [link]?', array('link' => '<a href="'.$v['link'].'" class="nowrap">'.$v['title'].'</a>')); ?></p>
                            <div class="form-group">
                                <label for="modal-decline-reason-<?= $v['id'] ?>"><?= _t('orders', 'Причина отказа'); ?></label>
                                <textarea rows="4" class="form-control" id="modal-decline-reason-<?= $v['id'] ?>" placeholder="<?= _t('orders', 'Укажите причину отказа'); ?>" name="message"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger j-decline" data-id="<?= $v['id'] ?>"><?= _t('orders', 'Отказаться'); ?></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= _t('form', 'Отмена'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>
<? endif;