<?php

/**
 * Список примеров заявки
 * @var $this Orders
 */

if( ! empty($examples)): ?>
<div class="o-collapsed-info">
    <button type="button" class="o-collapsed-info_toggle btn btn-default" data-toggle="collapse" data-target="#o-contact-info-examples-<?= $id ?>"><i class="fa fa-chevron-down pull-right"></i><?= _t('orders-offers', 'Работы'); ?></button>

    <div class="collapse" id="o-contact-info-examples-<?= $id ?>">

        <div class="o-collapsed-inner">

            <div class="o-propose-works">
                <? foreach($examples as $k => $v):
                    if( ! empty($v['img'])): ?>
                        <a href="<?= OrdersOfferImages::url($v['img']['item_id'], $v['img'], OrdersOfferImages::szView) ?>" class="o-project-thumb_proposal j-fancy-zoom-<?= $id ?>" rel="fancy-gallery-<?= $id ?>">
                            <div class="o-inner added">
                                <img src="<?= $v['url_cache'] ?>" alt="" />
                            </div>
                        </a>
                        <? endif;
                    if($v['portfolio_id']): ?>
                        <a href="<?= $v['portfolio_link'] ?>" class="o-project-thumb_proposal">
                            <div class="o-inner added">
                                <img src="<?= $v['portfolio_preview'] ?>" alt="" />
                            </div>
                        </a>
                    <? endif;
                    if($v['product_id']): ?>
                        <a href="<?= $v['product_link'] ?>" class="o-project-thumb_proposal">
                            <div class="o-inner added">
                                <img src="<?= $v['product_preview'] ?>" alt="" />
                            </div>
                        </a>
                    <? endif;
                endforeach; ?>
                <div class="clearfix"></div>
            </div>

        </div>

        <? if( ! empty($bAllWork)): ?>
        <div class="o-all-works">
            <a href="<?= Users::url('profile',array('login'=>$login, 'tab'=>'portfolio')); ?>"><?= _t('orders-offers', 'Все работы исполнителя'); ?></a>
        </div>
        <? endif; ?>
    </div>
</div>
    <script type="text/javascript">
        <?
            tpl::includeJS('fancybox2', true);
            js::start(); ?>
        $(function(){
            $('.j-fancy-zoom-<?= $id ?>').fancybox({
                openEffect	: 'none',
                closeEffect	: 'none',
                nextEffect  : 'fade',
                prevEffect : 'fade',
                helpers: {
                    overlay: {locked: false}
                }
            });
        });
        <? js::stop(); ?>
    </script>
<? endif;