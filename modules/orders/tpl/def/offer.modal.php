<?php
$nDefCurr = config::sys('currency.default');
$nSpecID = 0;
$type_service = ($type == Orders::TYPE_SERVICE);
if ($type_service) {
    $spec = array();
    if( ! empty($specs)){ $spec = reset($specs); }
    if( ! empty($spec['spec_id'])){  $nSpecID = $spec['spec_id']; }
    $aPriceSett = Specializations::i()->aPriceSett($nSpecID);
}
?>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('orders', 'Ваше предложение'); ?></h4>
            </div>
            <form method="post">
                <input type="hidden" name="order_id" value="<?= $id ?>" />
                <div class="modal-body">

                    <div class="form-group o-propose-inputs o-price-inputs">
                        <label for="j-price-from"><?= $type_service && ! empty($aPriceSett['price_title']) ? $aPriceSett['price_title'] : _t('', 'Стоимость'); ?>:</label>
                        <div class="input-group">
                            <input name="price_from" type="text" id="j-price-from" class="form-control input-sm" placeholder="<?= _t('', 'от'); ?>"/>
                            <input name="price_to" type="text" class="form-control input-sm" placeholder="<?= _t('', 'до'); ?>"/>
                            <select name="price_curr" class="form-control input-sm"><?= Site::currencyOptions($nDefCurr) ?></select>
                            <? if ($type_service && ! empty($aPriceSett['rates'])): ?>
                                <select name="price_rate" class="form-control input-sm"><?= HTML::selectOptions($aPriceSett['rates']) ?></select>
                            <? endif; ?>
                        </div>
                    </div>

                    <? if($type_service): ?>
                        <div class="form-group o-propose-inputs o-price-inputs j-terms">
                            <label for="terms"><?= _t('', 'Сроки'); ?>:</label>
                            <div class="input-group">
                                <input name="terms_from" type="text" class="form-control input-sm" placeholder="<?= _t('', 'от'); ?>"/>
                                <input name="terms_to" type="text" class="form-control input-sm" placeholder="<?= _t('', 'до'); ?>"/>
                                <select name="terms_type" class="form-control input-sm"><?= Specializations::aTerms() ?></select>
                            </div>
                        </div>
                    <? endif; ?>

                    <div class="form-group j-required">
                        <label for="text" class="control-label"><?= _t('orders-offers', 'Ваше сообщение'); ?></label>
                        <textarea name="descr" rows="4" id="text" class="form-control" placeholder="<?= _t('orders-offers', 'Сообщение клиенту'); ?>"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success j-submit"><?= _t('', 'Отправить'); ?></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= _t('form', 'Отмена'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>