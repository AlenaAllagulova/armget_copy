<?php
    $aServiceTypes = Orders::aServiceTypes();
    $fairplayEnabled = bff::fairplayEnabled();
?>
<div class="l-mapView_ballon">
    <header class="l-project-title">
        <? if($v['status'] == Orders::STATUS_CLOSED): ?><i class="fa fa-lock"></i><? endif; ?>
        <? if($fairplayEnabled && $v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
        <a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a>
    </header>
    <div class="l-project-head">
        <? if($v['type'] == Orders::TYPE_SERVICE): ?>
            <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                <span class="l-price_na"><?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?></span>
            <? else: ?>
                <span class="l-price"><?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><small><?= $v['price_rate_text'][LNG] ?></small><? endif; ?></span>
            <? endif; ?>
        <? endif; ?>
        <? if( ! empty($v['aTags'])): foreach($v['aTags'] as $vv):?>
            <a href="<?= Orders::url('search-tag', $vv) ?>" class="l-tag hidden-xs"><?= $vv['tag'] ?></a>
        <? endforeach; endif; ?>
    </div>

    <article>
        <p><?= nl2br(tpl::truncate($v['descr'], config::sysAdmin('orders.search.map.balloon.descr.truncate', 200, TYPE_UINT))); ?></p>
    </article>

    <ul class="l-item-features">
        <? if($v['pro']): ?><li><span class="small"><?= _t('orders', 'Только для'); ?> <span class="pro">pro</span></span></li><? endif; ?>
        <? if( ! empty($v['city_data']['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $v['city_data']['title']?></li><? endif; ?>
        <? if($v['type'] == Orders::TYPE_SERVICE):?><li><i class="fa fa-<?= $aServiceTypes[ $v['service_type'] ]['c'] ?>"></i> <?= $aServiceTypes[ $v['service_type'] ]['t'] ?></li><? endif; ?>
        <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($v['created'], false, true) ?></li>
        <li><a href="<?= $v['url_view'] ?>#offers"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['offers_cnt'], _t('orders', 'предложение;предложения;предложений'))?></span></a></li>
    </ul>
</div>