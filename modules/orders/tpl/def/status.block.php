<?php
/**
 * @var $this Orders
 */
?>
<? if(!$moderated && Orders::premoderation()): ?><div class="alert alert-warning" role="alert"><?= _t('orders', 'Ожидает проверки модератора') ?></div><? endif; ?>
<? if($status == Orders::STATUS_BLOCKED): ?>
    <div class="alert alert-danger" role="alert"><b><?= _t('orders', 'Заблокирован модератором.') ?></b>
    <? if( ! empty($blocked_reason)): ?><br /><?= _t('', 'Причина блокировки:'); ?> <?= tpl::blockedReason($blocked_reason) ?><? endif; ?>
    </div>
<? endif; ?>