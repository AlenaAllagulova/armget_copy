<?php
    $nUserID = User::id();
    $aServiceTypes = Orders::aServiceTypes();
    $bDeleteAllow = Orders::deleteAllow();
    $tagsLimit = Orders::searchTagsLimit();
    $ordersOpinions = Orders::ordersOpinions();
    $fairplayEnabled = bff::fairplayEnabled();
    if( ! empty($list)): ?>
        <ul class="l-projectList o-projectList">
            <? foreach($list as $v):
                $my = $v['user_id'] == $nUserID;
                $urlView = Orders::url('view', array('id' => $v['id'], 'keyword' => $v['keyword']));
                $openClose = in_array($v['status'], array(Orders::STATUS_OPENED, Orders::STATUS_CLOSED));
                $delete = $bDeleteAllow;
                if($openClose && $ordersOpinions && $v['performer_id'] && ! empty($v['offer_status']) && $v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE){
                    $openClose = false;
                    $delete = false;
                }
                ?>
                <li class="<?= $v['svc_marked'] ? 'highlited' : '' ?> j-order">
                    <header class="l-project-title">
                        <? if($my): ?><a href="#" class="l-project-title-delete link-red ajax-link j-trash" data-id="<?= $v['id'] ?>"><i class="fa fa-trash-o c-link-icon"></i><span class="hidden-xs"><?= $v['removed'] ? _t('orders', 'Из корзины') : _t('orders', 'В корзину') ?></span></a><? endif; ?>
                        <? if($v['visibility'] == Orders::VISIBILITY_PRIVATE): ?><i class="fa fa-eye-slash show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Приватный заказ'); ?>"></i><? endif; ?>
                        <? if($v['status'] == Orders::STATUS_CLOSED): ?><i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i><? endif; ?>
                        <? if($fairplayEnabled && $v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
                        <? if($v['pro']): ?><span class="pro show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('', 'Только для PRO'); ?>" data-original-title="<?= _t('', 'Только для PRO'); ?>">pro</span><? endif; ?>
                        <a href="<?= $urlView ?>"><?= $v['title'] ?></a>
                    </header>

                    <? if($my): ?>
                    <div class="p-profileOrder-controls-mobile">
                        <? if($fairplayEnabled && ! empty($v['workflow']['url'])): ?><a href="<?= $v['workflow']['url'] ?>"><i class="fa fa-rocket c-link-icon"></i></a><? endif; ?>
                        <a href="<?= Orders::url('edit', array('id'=>$v['id'])); ?>"><i class="fa fa-pencil c-link-icon"></i></a>
                        <? if(bff::servicesEnabled() && $v['visibility'] == Orders::VISIBILITY_ALL && $v['status'] == Orders::STATUS_OPENED): ?><a href="<?= Orders::url('promote', array('id' => $v['id'])); ?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i></a><? endif; ?>
                        <? if($openClose): ?>
                            <a href="#" class="<?= $v['status'] != Orders::STATUS_OPENED ? 'hidden' : '' ?> j-hide" data-id="<?= $v['id'] ?>"><i class="fa fa-lock c-link-icon"></i></a>
                            <a href="#" class="<?= $v['status'] != Orders::STATUS_CLOSED ? 'hidden' : '' ?> j-show" data-id="<?= $v['id'] ?>"><i class="fa fa-unlock c-link-icon"></i></a>
                        <? endif; ?>
                        <? if($delete): ?><a href="#" class="link-delete j-delete" data-id="<?= $v['id'] ?>"><i class="fa fa-times c-link-icon"></i></a><? endif; ?>
                    </div>
                    <? endif; ?>

                    <div class="l-project-head">
                        <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                            <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                                <span class="l-price_na"><?= _t('orders', 'По договоренности'); ?></span>
                            <? else: ?>
                                <span class="l-price"><?= tpl::formatPrice($v['price']); ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><small><?= $v['price_rate_text'][LNG] ?></small><? endif; ?></span>
                            <? endif; ?>
                        <? endif; ?>
                        <? if( ! empty($v['aTags'])): $n = 0; foreach($v['aTags'] as $vv):?>
                            <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                            <a href="<?= Orders::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
                        <? endforeach; endif; ?>
                    </div>

                    <? if($my): ?>
                    <div class="p-profileOrder-controls">
                        <? if($fairplayEnabled && ! empty($v['workflow']['url'])): ?><a href="<?= $v['workflow']['url'] ?>"><i class="fa fa-rocket c-link-icon"></i><?= _t('fp', 'Ход работ'); ?></a><? endif; ?>
                        <a href="<?= Orders::url('edit', array('id'=>$v['id'])); ?>"><i class="fa fa-pencil c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                        <? if(bff::servicesEnabled() && $v['visibility'] == Orders::VISIBILITY_ALL && $v['status'] == Orders::STATUS_OPENED): ?><a href="<?= Orders::url('promote', array('id' => $v['id'])); ?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i><?= _t('orders', 'Рекламировать'); ?></a><? endif; ?>
                        <? if($openClose): ?>
                            <a href="#" class="<?= $v['status'] != Orders::STATUS_OPENED ? 'hidden' : '' ?> j-hide" data-id="<?= $v['id'] ?>"><i class="fa fa-lock c-link-icon"></i><?= _t('orders', 'Снять с публикации'); ?></a>
                            <a href="#" class="<?= $v['status'] != Orders::STATUS_CLOSED ? 'hidden' : '' ?> j-show" data-id="<?= $v['id'] ?>"><i class="fa fa-unlock c-link-icon"></i><?= _t('orders', 'Публиковать заново'); ?></a>
                        <? endif; ?>
                        <? if($delete): ?><a href="#" class="link-delete j-delete" data-id="<?= $v['id'] ?>"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a><? endif; ?>
                    </div>
                    <?= $this->orderStatusBlock($v); ?>
                    <? endif; ?>

                    <? if($v['performer_id']):
                        $performerLink = array(
                            'name'     => $v['performer_name'],
                            'surname'  => $v['performer_surname'],
                            'pro'      => $v['performer_pro'],
                            'login'    => $v['performer_login'],
                            'verified' => $v['performer_verified'],
                        ); ?>
                        <? if($ordersOpinions && ! $fairplayEnabled && ! empty($v['offer_status'])):
                            if($v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_START): ?>
                                <div class="alert alert-chosen">
                                    <strong><?= _t('orders', 'Исполнитель выбран:'); ?></strong>  <?= tpl::userLink($performerLink, 'no-login no-verified no-pro') ?>
                                    <div class="mrgb10"><?= _t('orders', 'Вы предложили этому исполнителю выполнить работу по заказу. Работа начнется после того, как он примет предложение.'); ?></div>
                                    <button type="button" class="btn btn-sm btn-danger j-performer-start-cancel" data-id="<?= $v['performer_offer_id'] ?>"><?= _t('orders', 'Отменить предложение'); ?></button>
                                </div>
                            <? else: if($v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE): ?>
                                <div class="alert alert-chosen o-freelancer-defined p-profile-alert">
                                    <?  $meEnd = isset($v['opinions'][ $nUserID ]); if($meEnd) { $myOpinion = $v['opinions'][ $nUserID ]; }
                                        $workerEnd = isset($v['opinions'][ $v['performer_id'] ]); if($workerEnd) { $workerOpinion = $v['opinions'][ $v['performer_id'] ]; }
                                    ?>
                                    <span><?= $meEnd ? _t('orders', 'Вы завершили заказ.')
                                                     : ( $workerEnd ? _t('orders', 'Исполнитель [link] завершил(а) заказ.', array('link' => tpl::userLink($workerOpinion, 'no-login no-verified no-pro')))
                                                                    : _t('orders', 'Исполнитель определен:').' '.tpl::userLink($performerLink, 'no-login')) ?></span>
                                    <div class="p-profile-alert-controls">
                                        <? if($workerEnd): ?><a href="#" class="ajax-link j-opinion-show" data-id="<?= $workerOpinion['id'] ?>"><span><?= _t('orders', 'Отзыв исполнтеля'); ?></span></a><? endif; ?>
                                        <? if($meEnd): ?>
                                            <a href="#" class="ajax-link j-opinion-show" data-id="<?= $myOpinion['id'] ?>"><span><?= _t('orders', 'Ваш отзыв'); ?></span></a>
                                        <? else: ?>
                                            <button type="button" class="btn btn-success btn-sm j-opinion-add" data-id="<?= $v['id'] ?>"><?= $workerEnd ? _t('orders', 'Оставить отзыв') : _t('orders', 'Завершить заказ'); ?></button>
                                        <? endif; ?>
                                    </div>
                                </div>
                            <? endif; endif; ?>
                        <? else: ?>
                        <div class="alert alert-chosen o-freelancer-defined">
                            <span><?= _t('orders', 'Исполнитель определен:'); ?></span>
                            <?= tpl::userLink($performerLink, 'no-login') ?>
                        </div>
                        <? endif; ?>
                    <? endif; ?>

                    <? if(empty($v['performer_id']) && ! empty($v['declined'])): ?>
                    <div class="alert alert-decline p-profile-alert p-profile-alert">
                        <i class="fa fa-times pull-right"></i>
                        <span><?= _t('', 'Исполнитель'); ?> <?= tpl::userLink($v['declined'], 'no-login no-verified no-pro') ?></span>
                        <strong><?= _t('orders', 'отказался от предложения'); ?></strong>.
                        <? if( ! empty($v['declined']['message'])): ?>
                        <div class="alert-in mrgt10">
                            <strong><?= _t('orders', 'Сообщение от исполнителя:'); ?></strong>
                            <div><?= nl2br($v['declined']['message']); ?></div>
                        </div>
                        <? endif; ?>
                    </div>
                    <? endif; ?>

                    <article>
                        <p><?= nl2br($v['descr']); ?></p>
                    </article>

                    <ul class="l-item-features">
                        <? if( ! empty($v['city_data']['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $v['city_data']['title']?></li><? endif; ?>
                        <? if($v['type'] == Orders::TYPE_SERVICE):?><li><i class="fa fa-<?= $aServiceTypes[ $v['service_type'] ]['c'] ?>"></i> <?= $aServiceTypes[ $v['service_type'] ]['t'] ?></li><? endif; ?>
                        <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($v['created'], false, true) ?></li>
                        <li><a href="<?= $urlView ?>#offers"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['offers_cnt'], _t('orders', 'предложение;предложения;предложений'))?></span></a><?= ! empty($v['offers_new']) ? ' <span class="label label-new">+'.$v['offers_new'].'</span>' : '' ?></li>
                        <? if($v['offers_cnt'] && ! empty($v['chat_new'])): ?>
                            <li><a href="<?= $urlView ?>#offers"><i class="fa fa-comment c-link-icon"></i><span><?= tpl::declension($v['chat_cnt'], _t('orders', 'сообщение;сообщения;сообщений'))?></span></a><?= $v['chat_new'] ? ' <span class="label label-new">+'.$v['chat_new'].'</span>' : '' ?></li>
                        <? endif; ?>
                    </ul>

                </li>
            <? endforeach; ?>
        </ul>
<? else: ?>
        <div class="alert alert-info"><?= _t('orders', 'Заказы не найдены'); ?></div>
<? endif; ?>