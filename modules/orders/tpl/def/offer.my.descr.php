<?php
    /**
     * @var $this Orders
     * @var $id integer
     * @var $chatcnt integer
     * @var $descr string
     * @var $allow_edit boolean
     */
    tpl::includeJS('orders.offers', false, 3);
?>
<div id="j-offer-descr">
    <div class="j-text"><?= nl2br($descr) ?></div>

    <div class="o-proposal-chat">
        <div class="pull-left">
            <? if($allow_edit): ?><a href="#" class="ajax-link j-edit"><i class="fa fa-pencil c-link-icon"></i><span><?= _t('form', 'Редактировать'); ?></span></a><?
            else: ?><a href="#j-offer-chat-<?= $id ?>" class="ajax-link j-open-chat j-open-chat-response" data-toggle="collapse" data-parent="#accordion" data-id="<?= $id ?>"><i class="fa fa-reply c-link-icon"></i><span><?= _t('orders-offers', 'Написать ответ'); ?></span></a><? endif; ?>
        </div>
        <div class="pull-right">
            <a href="#j-offer-chat-<?= $id ?>" class="ajax-link j-open-chat" data-toggle="collapse" data-parent="#accordion"><?
                ?><i class="fa fa-comment c-link-icon"></i><?
                ?><span class="j-open-chat-title"><?= _t('orders-offers', 'Развернуть переписку'); ?></span> [<?= $chatcnt ?>]
                <? if( ! empty($chat_new_worker)): ?><span class="label label-new">+<?= $chat_new_worker ?></span><? endif; ?>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div id="j-offer-edit-form-block" style="display:none;">
    <form action="" method="post">
        <input type="hidden" name="id" value="<?= $id ?>" />
        <div class="form-group">
            <textarea name="descr" class="form-control" rows="3"><?= HTML::escape($descr) ?></textarea>
        </div>
        <div class="c-formSubmit">
            <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить') ?></button>
            <a href="#" class="ajax-link c-formCancel j-cancel"><span><?= _t('form', 'Отмена') ?></span></a>
        </div>
    </form>
</div>

<div class="o-proposal-chat-window collapse" id="j-offer-chat-<?= $id ?>" data-cnt="<?= $chatcnt ?>" data-id="<?= $id ?>">
    <ul class="j-chat"></ul>
    <form role="form" action="" method="post">
        <input type="hidden" name="id" value="<?= $id ?>" />
        <div class="form-group">
            <textarea name="message" class="form-control mrgt20" rows="3"></textarea>
        </div>
        <button class="btn btn-primary btn-sm j-submit"><?= _t('', 'Отправить') ?></button>
    </form>
</div>

<script type="text/javascript">
<? js::start() ?>
<? if($allow_edit): ?>
jOrdersOfferEdit.init(<?= func::php2js(array(
        'lang' => array(),
    )) ?>);
<? endif; ?>
jOrdersOfferChat.init(<?= func::php2js(array(
        'lang'   => array(
            'chat_title_show' => _t('orders-offers', 'Развернуть переписку'),
            'chat_title_hide' => _t('orders-offers', 'Скрыть переписку'),
        ),
        'id' => $this->input->get('chat'),
    )) ?>);
<? js::stop() ?>
</script>