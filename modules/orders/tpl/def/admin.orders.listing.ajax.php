<?php
$urlEdit = $this->adminLink(bff::$event.'&act=edit&id=');
$fairplayEnabled = bff::fairplayEnabled();
$aServiceTypes = Orders::aServiceTypes();
foreach($list as $k=>$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?><?= $v['status'] == Orders::STATUS_NOTACTIVATED ? ' disabled' : '' ?>">
        <? if($f['tab'] == 2): ?><td><label class="checkbox inline"><input type="checkbox" name="i[]" onclick="jOrdersOrdersList.massModerate('check',this);" value="<?= $id ?>" class="check j-item-check" /></label></td><? endif; ?>
        <td class="small"><?= $id ?></td>
        <td class="left">
            <a href="<?= Orders::url('view', array('id' => $id, 'keyword' => $v['keyword'])) ?>" class="but linkout" target="_blank"></a>
            <a href="#" class="nolink" onclick="return bff.orderinfo(<?= $id ?>);"><?= $v['title'] ?></a>
            <? if($v['visibility'] == Orders::VISIBILITY_PRIVATE): ?><i class="icon icon-eye-close disabled" title="Приватный заказ"></i><? endif; ?>
            <? if($fairplayEnabled && $v['fairplay'] == Orders::FAIRPLAY_USE): ?><i class="icon icon-fairplay" title="Безопасная сделка"></i><? endif; ?>
        </td>
        <td>
            <? # для списка "на модерации", указываем причину отправления на модерацию:
            if($f['tab'] == 2) {
                if($v['moderated'] == 0) {
                    if( $v['status'] == Orders::STATUS_BLOCKED ) {
                        ?><i class="icon-ban-circle disabled" title="отредактировано пользователем после блокировки"></i><?
                    } else {
                        ?><i class="icon-plus disabled" title="новый заказ"></i><?
                    }
                } elseif($v['moderated'] == 2) {
                    ?><i class="icon-pencil disabled" title="отредактировано пользователем"></i><?
                }
            } ?>
        </td>
        <td> <?# todo: ACC-34 front - icon job & icon svc ?>
            <? if($v['service_type'] == Orders::SERVICE_TYPE_JOB ): ?>
                <i class="fa fa-<?= $aServiceTypes[Orders::SERVICE_TYPE_JOB]['c'] ?>" title="<?= $aServiceTypes[Orders::SERVICE_TYPE_JOB]['t'] ?>"></i>
                <span title="<?= $aServiceTypes[Orders::SERVICE_TYPE_JOB]['t'] ?>"><img src="<?= bff::url('/img/icon/anchor.svg')?>" width="14" alt=""></span>
            <? endif; ?>
            <? if($f['tab'] == 2 || $f['tab'] == 3) {?>
                <? if(($v['svc'] & Orders::SVC_POST_SITE)
                       || ($v['svc'] & Orders::SVC_POST_SITE_TG)
                       || ($v['svc'] & Orders::SVC_POST_SITE_TG_VK)
                       || ($v['svc'] & Orders::SVC_POST_SITE_TG_VK_FB)): ?>
                    <span
                            title="<?= _t('', 'размещение') ?>"
                            style="
                                display: inline-block;
                                padding: 0 2px;
                                background: lightgreen;
                                border-radius: 3px;
                            "
                    >
                        <i class="icon-shopping-cart"></i>
                    </span>
                <? endif; ?>
            <?}?>
        </td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a class="but edit order-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
            <a class="but images<? if(!$v['imgcnt']){ ?> disabled<? } ?>" href="<?= $urlEdit.$id.'&ftab=images' ?>"></a>
            <a class="but del order-del" title="<?= _t('', 'Delete') ?>" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="5">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;