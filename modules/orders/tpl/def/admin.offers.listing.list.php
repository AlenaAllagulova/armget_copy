<?php
/**
 * @var $this Orders
 */
tpl::includeJS('comments', true);
$bCommonListing = (bff::$event == 'offers_list');
if($bCommonListing){
    $urlEdit = $this->adminLink('orders_list&act=edit&id=');
}
?>
<div class="comments" id="j-item-claims">
    <?
    foreach($offers as $v):
        $offerID = $v['id'];
        ?>
        <div class="comment" id="j-offer-<?= $offerID ?>">
            <a name="j-offer-<?= $offerID ?>" ></a>
            <div class="ccontent">
                <div class="tb"><div class="tl"><div class="tr"></div></div></div>
                <div class="ctext">
                    <div class="j-offer-view"><?= nl2br($v['descr']) ?></div>
                </div>
                <div class="bl"><div class="bb"><div class="br"></div></div></div>
            </div>
            <div class="info" style="margin:0;">
                <ul>
                    <li><p><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink author<? if($v['ublocked']){ ?> blocked<? } ?>"><?= ( ! empty($v['name']) ? $v['name'] : $v['login'] ) ?> (<?= $v['email'] ?>)</a></p></li>
                    <li class="date"><?= tpl::date_format2($v['created'], true); ?></li>
                    <? if($v['moderated'] != 1){ ?><li><a href="#" class="ajax text-success" onclick="jOffers.approve(<?= $offerID ?>, this); return false;"><?= _t('', 'Approve') ?></a></li><? } ?>
                    <li><a href="#" class="text-error delete ajax" onclick="jOffers.del(<?= $offerID ?>); return false;"><?= _t('', 'Delete') ?></a></li>
                    <li><? if($bCommonListing){ ?><a href="<?= $urlEdit.$v['order_id'] ?>" onclick="return bff.orderinfo(<?= $v['order_id'] ?>);"><?= _t('orders', 'Order #[id]', array('id'=>$v['order_id'])) ?></a><? } ?> <a class="but linkout" target="_blank" href="<?= Orders::url('view', array('id' => $v['order_id'], 'keyword' => $v['keyword'])) ?>"></a></li>
                    <? switch($v['status']){
                        case Orders::OFFER_STATUS_CANDIDATE:?><li class="text-warning">Кандидат</li><? break;
                        case Orders::OFFER_STATUS_PERFORMER:?><li class="text-success">Исполнитель</li><? break;
                        case Orders::OFFER_STATUS_CANCELED:?><li class="text-error">Отказано</li><? break;
                        case Orders::OFFER_STATUS_PERFORMER_START:?><li class="text-info">Предложили стать исполнителем</li><? break;
                        case Orders::OFFER_STATUS_PERFORMER_AGREE:?><li class="text-success">Исполнитель</li><? break;
                        case Orders::OFFER_STATUS_PERFORMER_DECLINE:?><li class="text-error">Отказался стать исполнителем</li><? break;
                    } ?>
                </ul>
            </div>
        </div>
    <? endforeach;
    if(empty($offers)): ?>
        <div class="alignCenter valignMiddle" style="height:30px; padding-top:15px;">
            <span class="desc"><?= _t('orders', 'нет предложений') ?></span>
        </div>
    <? endif; ?>
</div>
<script type="text/javascript">
    var jOffers = (function() {
        var $filter, processing = false;
        var ajax_url = '<?= $this->adminLink('offers_list&act='); ?>';

        $(function(){
            $filter = $('#j-orders-offers-filter');

            if($filter && $filter.length) {
                $filter.find('.j-perpage').on('change', function(){
                    $filter.submit();
                });
                $('#j-orders-offers-tabs').on('click', '.j-tab', function(e){ nothing(e);
                    $filter.find('.j-tab-id').val( $(this).data('id') );
                    $filter.submit();
                });
            }
        });

        return {
            del: function(id)
            {
                if( ! bff.confirm('sure')) return;
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'delete', {id: id}, function(data) {
                    if(data) {
                        $('#j-offer-'+id).slideUp();
                    }
                    processing = false;
                });
            },
            approve: function(id, link)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'approve', {id: id}, function(data) {
                    if(data) {
                        $(link).remove();
                    }
                    processing = false;
                });
            }
        };
    }());
</script>