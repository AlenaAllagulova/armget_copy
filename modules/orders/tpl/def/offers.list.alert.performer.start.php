<?php
$fairplayEnabled = bff::fairplayEnabled();
$orderView = ! empty($performer);
$offerID = ! empty($offerID) ? $offerID : 0;
?>
<div class="alert alert-chosen <?= $orderView ? 'j-performer-start-cancel-'.$offerID : 'j-performer' ?>">
    <? if($orderView): ?>
        <strong><?= _t('orders', 'Исполнитель выбран:'); ?></strong>  <?= tpl::userLink($performer, 'no-login') ?>
    <? else: ?>
        <strong><?= _t('orders', 'Выбран исполнителем'); ?></strong>
    <? endif; ?>
    <? if($fairplayEnabled && ! empty($workflow)): ?>
        <div class="l-project-terms">
            <span class="l-project-terms-budget"><?= _t('fp', 'Бюджет:'); ?> <strong><?= tpl::formatPrice($workflow['price']) ?> <?= Site::currencyDefault('title_short'); ?></strong></span>,
            <span class="l-project-terms-time"><?= _t('fp', 'Срок:'); ?> <?= Fairplay::tplTerm($workflow['term']) ?></span>
        </div>
        <div class="l-project-deal">
            <? if($workflow['fairplay'] == Orders::FAIRPLAY_USE): ?><?= _t('fp', 'Работа через <a [link]>Безопасную Сделку</a>', array(
                'link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?><?
            else: ?><?= _t('fp', 'Работа через прямую оплату</a>'); ?><? endif; ?>
        </div>
    <? endif; ?>
    <div class="mrgb10"><?= _t('orders', 'Вы предложили этому исполнителю выполнить работу по заказу. Работа начнется после того, как он примет предложение.'); ?></div>
    <button type="button" class="btn btn-sm btn-danger <?= $orderView ? 'j-performer-start-cancel-general' : 'j-performer-start-cancel' ?>" data-id="<?= $offerID ?>"><?= _t('orders', 'Отменить предложение'); ?></button>
    <? if($fairplayEnabled): ?>
        <?= _t('', 'или'); ?> <a class="ajax-link j-workflow-change" href="#" data-id="<?= $offerID ?>"><span><?= _t('fp', 'изменить срок, сумму или тип оплаты'); ?></span></a>
    <? endif; ?>
</div>