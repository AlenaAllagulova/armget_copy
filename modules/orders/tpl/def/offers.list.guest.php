<?php

/**
 * Список заявок к заказу: незарегистрированный пользователь
 * @var $this Orders
 */

?>
<header class="title">
    <h4 class="o-propositions-title"><i class="fa fa-comments-o"></i> <?= _t('orders-offers', 'Предложения от исполнителей'); ?> <span class="o-count-proposals"><?= $offers_cnt ?></span></h4>
</header>
<div class="alert alert-info mrgt20">
    <?= _t('orders-offers', 'Всего [cnt].', array('cnt' => tpl::declension($offers_cnt, _t('orders-offers', 'предложение;предложения;предложений')))) ?> <a href="<?= Users::url('register') ?>"><?= _t('', 'Зарегистрируйтесь'); ?></a> <?= _t('', 'или'); ?> <a href="<?= Users::url('login') ?>"><?= _t('', 'войдите') ?></a> <?= _t('', 'для просмотра.') ?>
</div>
