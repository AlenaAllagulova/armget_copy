<?php
/**
 * @var $this Orders
 *
 * Необходимые данные о Заказе:
 * id, status, status_changed, moderated, svc, blocked_reason
 *
 * Необходимыые данные пользователя:
 * user['blocked']
 */

$blocked = ($status == Orders::STATUS_BLOCKED);
$is_popup = ! empty($is_popup);
$only_form = ! empty($only_form);
?>
<? if ( ! $only_form) { ?>
<script type="text/javascript">
var jOrdersOrdersStatus = (function(){
    var $block, url = '<?= $this->adminLink('orders_status&act=', 'orders'); ?>';
    var data = {id: <?= $id ?>, blocked: <?= ($blocked ? 1 : 0) ?>, is_popup: <?= ($is_popup ? 1 : 0) ?>};
    $(function(){
        $block = $('#j-i-status-block');
    });

    function _progress() {
        $block.toggleClass('disabled');
    }

    return {
        activate: function(){
            bff.ajax(url+'activate', data, function(resp){
                if(resp && resp.success) {
                    $block.html(resp.html);
                }
            }, _progress);
        },
        approve: function(){
            bff.ajax(url+'approve', data, function(resp){
                if(resp && resp.success) {
                    $block.html(resp.html);
                }
            }, _progress);
        },
        close: function(){
            if( ! bff.confirm('sure')) return;
            bff.ajax(url+'close', data, function(resp){
                if(resp && resp.success) {
                    $block.html(resp.html);
                }
            }, _progress);
        },
        open: function(){
            if( ! bff.confirm('sure')) return;
            bff.ajax(url+'open', data, function(resp){
                if(resp && resp.success) {
                    $block.html(resp.html);
                }
            }, _progress);
        },
        changeBlocked: function(step, block)
        {
            var $blocked_reason = $block.find('.j-i-blocked-reason');
            var $buttons = $block.find('.j-i-status-buttons');
            switch(step)
            {
                case 1: { // block/unblock, change reason
                    $block.find('#i_blocked').hide();
                    $block.find('#i_blocked_error, #i_blocking').show(0, function(){
                        $blocked_reason.focus();
                        $buttons.hide();
                    });
                } break;
                case 2: { // cancel
                    if(data.blocked == 1) {
                        $block.find('#i_blocking').hide();
                        $block.find('#i_blocked').show();
                    } else {
                        $block.find('#i_blocked_error').hide();
                    }
                    $buttons.show();
                } break;
                case 3: { // save
                    data.blocked_reason = $blocked_reason.val();
                    bff.ajax(url+'block', data, function(resp){
                        if(resp && resp.success) {
                            data.blocked = resp.blocked;
                            $block.html(resp.html);
                        }
                    }, _progress);
                } break;
                case 4: { // unblock
                    if (!bff.confirm('sure')) break;
                    data.unblock = 1;
                    jOrdersOrdersStatus.changeBlocked(3);
                    data.unblock = 0;
                } break;
            }
            return false;
        }
    };
}());
</script>

<div class="<? if( ! $is_popup ) { ?>well well-small<? } ?>" id="j-i-status-block">
<? } ?>
    <table class="admtbl tbledit">
        <tr>
            <td class="row1 field-title<? if($is_popup) { ?> right<? } ?>" style="width:<?= ( $is_popup ? 133 : 88 ) ?>px;">Статус:</td>
            <td class="row2"><strong><?
                if( $user['blocked'] ) {
                    ?>Аккаунт пользователя был заблокирован<?
                } else {
                    switch($status) {
                        case Orders::STATUS_NOTACTIVATED: { echo 'Неактивирован'; } break;
                        case Orders::STATUS_OPENED: { echo 'Открыт'; } break;
                        case Orders::STATUS_CLOSED: { echo 'Закрыт'; } break;
                        case Orders::STATUS_BLOCKED: { echo ($moderated == 0?'Ожидает проверки (был заблокирован)':'Заблокирован'); } break;
                    }
                }
                ?></strong><? if($status_changed != '0000-00-00 00:00:00'){ ?>&nbsp;&nbsp;<span class="desc">(<?= tpl::date_format2($status_changed, true, true); ?>)</span><? } ?>
            </td>
        </tr>
        <? if ( $status != Orders::STATUS_NOTACTIVATED && ! $user['blocked'] ) { ?>
            <tr>
                <td class="row1" colspan="2">
                    <div class="alert alert-danger <?= (!$blocked ? 'hidden':'') ?>" id="i_blocked_error">
                        <div><?= _t('', 'Причина блокировки:') ?><div class="right desc" id="i_blocked_reason_warn" style="display:none;"></div></div>
                        <div class="clear"></div>
                        <div id="i_blocked">
                            <span id="i_blocked_text"><?= (!empty($blocked_reason) ? tpl::blockedReason($blocked_reason) :'?') ?></span> - <a href="#" onclick="jOrdersOrdersStatus.changeBlocked(1,0); return false;" class="ajax desc">изменить</a>
                        </div>
                        <div id="i_blocking" style="display: none;">
                            <textarea name="blocked_reason" class="autogrow j-i-blocked-reason" style="height:60px; min-height:60px;"><?= $blocked_reason; ?></textarea>
                            <a onclick="return jOrdersOrdersStatus.changeBlocked(3, 1);" class="btn btn-mini btn-success" href="#"><?= (!$blocked ? _t('', 'continue'):_t('orders', 'изменить причину')) ?></a>
                            <? if($blocked) { ?><a onclick="return jOrdersOrdersStatus.changeBlocked(4);" class="btn btn-success btn-mini" href="#"><?= _t('', 'unblock') ?></a><? } ?>
                            <a onclick="return jOrdersOrdersStatus.changeBlocked(2);" class="btn btn-mini" href="#"><?= _t('', 'cancel') ?></a>
                        </div>
                    </div>
                </td>
            </tr>
           <? if( ! ($moderated && $blocked) ) { ?>
                <tr class="j-i-status-buttons">
                    <td class="row1" colspan="2" <? if( $is_popup ) { ?> style="padding-left: 90px;" <? } ?>>
                        <?
                        if ($moderated == 0) { ?>
                            <input class="btn btn-mini btn-success success button" type="button" onclick="jOrdersOrdersStatus.approve();" value="<?= ($blocked ? 'проверено, все впорядке' : 'проверено') ?>" />
                        <? } else {
                            if ( $moderated == 2 ) {
                                ?><input class="btn btn-mini btn-success success button" type="button" onclick="jOrdersOrdersStatus.approve();" value="<?= 'проверено' ?>" /> <?
                            }
                            if ($status ==  Orders::STATUS_CLOSED) {
                                ?><input class="btn btn-mini submit button" type="button" onclick="jOrdersOrdersStatus.open();" value="открыть" /> <?
                            } else if ($status == Orders::STATUS_OPENED) {
                                ?><input class="btn btn-mini submit button" type="button" onclick="jOrdersOrdersStatus.close();" value="закрыть" /> <?
                            }
                        }
                        if ( ! $blocked) { ?>
                            <a class="btn btn-mini text-error" onclick="jOrdersOrdersStatus.changeBlocked(1); return false;" id="i_block_lnk"><?= _t('', 'block') ?></a>
                        <? } ?>
                    </td>
                </tr>
            <? }
        } # endif: ! $user['blocked']
        else if(intval($status) === Orders::STATUS_NOTACTIVATED) { ?>
            <tr>
                <td class="row1" colspan="2">
                    <input class="btn btn-mini submit button" type="button" onclick="jOrdersOrdersStatus.activate();" value="активировать" />
                </td>
            </tr>
        <? } ?>
        <? bff::hook('orders.admin.order.form.status', array('is_popup'=>$is_popup,'data'=>&$aData)) ?>
    </table>
<? if( ! $only_form) { ?>
</div>
<? } ?>