<?php
?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h1 class="small"><?= _t('orders', 'Доступ запрещен') ?></h1>

                <p><?= _t('orders', 'Добавлять заказы могут только заказчики') ?></p>
                <p><a href="<?= Orders::url('list') ?>"><?= _t('orders', 'Перейти на главную страницу'); ?></a></p>

            </div>
        </div>
    </section>
</div>