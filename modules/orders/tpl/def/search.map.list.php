<?php
if( ! empty($list)):
$tagsLimit = Orders::searchTagsLimit();
$fairplayEnabled = bff::fairplayEnabled();
?>
<ul class="l-projectList">
    <?
    $lng_show_on_map = _t('orders', 'показать на карте');
    foreach($list as $k => $v): ?>
    <!-- project -->
    <li>

        <header class="l-project-title">
            <? if($v['status'] == Orders::STATUS_CLOSED): ?><i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i><? endif; ?>
            <? if($fairplayEnabled && $v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
            <a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a>
            <a href="#" class="ajax-link pull-right small hidden-xs j-map-marker" data-n="<?= $k ?>" data-id="<?= $v['id'] ?>"><i class="fa fa-map-marker"></i> <span><?= $lng_show_on_map; ?></span></a>
        </header>

        <div class="l-project-head">
            <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                    <span class="l-price_na"><?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?></span>
                <? else: ?>
                    <span class="l-price"><?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><small><?= $v['price_rate_text'][LNG] ?></small><? endif; ?></span>
                <? endif; ?>
            <? endif; ?>
            <? if( ! empty($v['aTags'])): $n = 0; foreach($v['aTags'] as $vv):?>
                <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                <a href="<?= Orders::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
            <? endforeach; endif; ?>
        </div>

        <a href="#" class="ajax-link visible-xs j-map-marker" data-id="<?= $k ?>"><i class="fa fa-map-marker"></i> <span><?= $lng_show_on_map; ?></span></a>
    </li>
    <? endforeach; ?>
</ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('orders', 'Заказы не найдены'); ?></div>
<? endif;

