<?php
$fairplayEnabled = bff::fairplayEnabled();
tpl::includeJS('orders.respondent.list', false, 4);
?>
<div class="alert alert-chosen j-offer" data-id="<?= $id ?>">
    <strong><?= _t('orders', 'Вас выбрали исполнителем'); ?></strong>
    <? if($fairplayEnabled): $workflow = func::unserialize($workflow); ?>
        <div class="l-project-terms">
            <span class="l-project-terms-budget"><?= _t('fp', 'Бюджет:'); ?> <strong><?= tpl::formatPrice($workflow['price']) ?> <?= Site::currencyDefault('title_short'); ?></strong></span>,
            <span class="l-project-terms-time"><?= _t('fp', 'Срок:'); ?> <?= Fairplay::tplTerm($workflow['term']) ?></span>
        </div>
        <div class="l-project-deal">
            <? if($workflow['fairplay'] == Orders::FAIRPLAY_USE): ?><?= _t('fp', 'Работа через <a [link]>Безопасную Сделку</a>', array(
                'link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?><?
            else: ?><?= _t('fp', 'Работа через прямую оплату</a>'); ?><? endif; ?>
        </div>
    <? endif; ?>
    <div class="mrgb10"><?= _t('orders', 'Вам предложили выполнить работу по заказу. Работа начнется после того, как вы примите предложение.'); ?></div>
    <ul class="alert-controls">
        <li><button class="btn btn-sm btn-success j-agree" data-id="<?= $id ?>"><?= _t('orders', 'Подтвердить заказ'); ?></button></li>
        <li><?= _t('', 'или'); ?> <a href="#" class="ajax-link link-hover-red j-decline" data-id="<?= $id ?>"><span><?= _t('orders', 'отказаться от него'); ?></span></a></li>
    </ul>
</div>
<script type="text/javascript">
    <? js::start() ?>
    jOrdersRespondentList.init(<?= func::php2js(array(
        'lang'  => array(
            'decline_short' => _t('orders', 'Укажите причину отказа'),
        ),
        'ordersOpinions' => Orders::ordersOpinions(),
        'invitesEnabled' => Orders::invitesEnabled(),
        'ajax'  => true,
    )) ?>);
    <? js::stop() ?>
</script>