<?php
?>
<div class="modal fade" id="j-modal-decline-<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('orders', 'Отказаться от предложения'); ?></h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <p><?= _t('orders', 'Вы уверены, что хотите отказаться от заказа [link]?', array('link' => '<a href="'.$link.'" class="nowrap">'.$title.'</a>')); ?></p>
                    <div class="form-group">
                        <label for="modal-decline-reason-<?= $id ?>"><?= _t('orders', 'Причина отказа'); ?></label>
                        <textarea rows="4" class="form-control" id="modal-decline-reason-<?= $id ?>" placeholder="<?= _t('orders', 'Укажите причину отказа'); ?>" name="message"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger j-decline" data-id="<?= $id ?>"><?= _t('orders', 'Отказаться'); ?></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= _t('form', 'Отмена'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>