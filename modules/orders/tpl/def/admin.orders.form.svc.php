<?php
$svc = intval($svc);
if(empty($svc)): ?>
    <div style="padding:5px 0;" class="desc">Нет активированных на текущий момент услуг</div>
<? else:
    if($svc & Orders::SVC_MARK): ?>
        <div style="margin: 10px 0">Выделено до <b><?= tpl::date_format3($svc_marked_to, 'd.m.Y H:i') ?></b></div>
    <? endif;
    if($svc & Orders::SVC_FIX): ?>
        <div style="margin: 10px 0">Закреплено до <b><?= tpl::date_format3($svc_fixed_to, 'd.m.Y H:i') ?></b></div>
    <? endif;
    if($svc & Orders::SVC_HIDDEN): ?>
        <div style="margin: 10px 0">Скрытый</div>
    <? endif;
endif; ?>
    <? bff::hook('orders.admin.order.form.svc', array('data'=>&$aData)); ?>
    <div style="margin: 10px 0"><a href="<?= $this->adminLink('listing&item='.$id, 'bills') ?>">История активации услуг заказа #<?= $id ?></a></div>
<?