<?php
if( ! empty($list)): ?>
<ul class="o-orderSelect">
    <? foreach($list as $v): ?>
        <li class="j-order" data-id="<?= $v['id'] ?>">
            <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                <span class="o-orderSelect-price"><?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?></span>
            <? else: ?>
                <span class="o-orderSelect-price"><?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><small><?= $v['price_rate_text'][LNG] ?></small><? endif; ?></span>
            <? endif; ?>
            <div class="o-orderSelect-name"><?= $v['title'] ?></div>
        </li>
    <? endforeach; ?>
</ul>
<? endif;