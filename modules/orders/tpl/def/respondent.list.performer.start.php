<?php
if( ! empty($performerStart)):
    $fairplayEnabled = bff::fairplayEnabled(); ?>
<div id="j-offers-performer-start">
    <? foreach($performerStart as $v): ?>
    <div class="alert p-profile-alert-invite j-offer" data-id="<?= $v['id'] ?>">
        <h4><?= _t('orders', 'Вам предложили стать исполнителем'); ?></h4>
        <div class="p-profile-alert-invite-msg">
            <h5>
                <a href="<?= $v['link'] ?>" target="_blank"><?= $v['title'] ?></a>
            </h5>
            <?
            if($fairplayEnabled): $workflow = func::unserialize($v['workflow']); ?>
                <div class="l-project-terms">
                    <span class="l-project-terms-budget"><?= _t('fp', 'Бюджет:'); ?> <strong><?= tpl::formatPrice($workflow['price']) ?> <?= Site::currencyDefault('title_short'); ?></strong></span>,
                    <span class="l-project-terms-time"><?= _t('fp', 'Срок:'); ?> <?= Fairplay::tplTerm($workflow['term']) ?></span>
                </div>
                <div class="l-project-deal">
                    <? if($workflow['fairplay'] == Orders::FAIRPLAY_USE): ?><?= _t('fp', 'Работа через <a [link]>Безопасную Сделку</a>', array(
                        'link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?><?
                    else: ?><?= _t('fp', 'Работа через прямую оплату</a>'); ?><? endif; ?>
                </div>
            <? else:
            # Стоимость:
            $sPrice = '';
            if( ! empty($v['price_from']) &&  ! empty($v['price_to'])){
                $sPrice = $v['price_from'].' - '.$v['price_to'];
            } else {
                if( ! empty($v['price_from'])){
                    $sPrice = _t('', 'от ').$v['price_from'];
                }
                if( ! empty($v['price_to'])){
                    $sPrice = _t('', 'до ').$v['price_to'];
                }
            }
            $sPrice .= ! empty($sPrice) ? ' '.Site::currencyData($v['price_curr'], 'title_short') : '';

            $aPriceRate = ( $v['price_rate_text'] ? func::unserialize($v['price_rate_text']) : array(LNG=>'') );
            if ( ! empty($aPriceRate[LNG]) && ! empty($sPrice)) { $sPrice .= ' '.$aPriceRate[LNG]; }

            # Сроки:
            $sTerms = '';
            if( ! empty($v['terms_from']) && ! empty($v['terms_to'])){
                $sTerms = $v['terms_from'].' - '.$v['terms_to'];
            } else {
                if( ! empty($v['terms_from'])){
                    $sTerms = _t('', 'от ').$v['terms_from'];
                }
                if( ! empty($v['terms_to'])){
                    $sTerms = _t('', 'до ').$v['terms_to'];
                }
            }
            if ( ! empty($sTerms)) {
                $sTerms = $sTerms.' '.Specializations::aTerms($v['terms_type'], 'title');
            }
            ?>
            <div class="l-project-terms">
                <? if( ! empty($sPrice)): ?><span class="l-project-terms-budget"><?= _t('', 'Бюджет:'); ?> <strong><?= $sPrice ?></strong></span><?= ! empty($sTerms) ? ',' :'' ?><? endif; ?>
                <? if( ! empty($sTerms)): ?><span class="l-project-terms-time"><?= _t('', 'Срок:'); ?> <?= $sTerms ?></span><? endif; ?>
            </div>
            <? endif; ?>
            <div class="l-project-author">
                <?= _t('orders', 'Заказчик:'); ?> <?= tpl::userLink($v, 'no-login') ?>
            </div>
            <? if( ! empty($v['message'])): ?>
            <div class="alert-in">
                <div class="alert-in-title"><?= _t('orders', 'Сообщение от заказчика:'); ?></div>
                <div><?= nl2br($v['message']) ?></div>
            </div>
            <? endif; ?>
            <ul class="l-project-bs-content-controls">
                <li><a href="<?= $v['link_chat'] ?>" class="btn btn-default"><?= _t('orders', 'Продолжить общение'); ?></a></li>
                <li><button class="btn btn-success j-agree" data-id="<?= $v['id'] ?>"><?= _t('orders', 'Подтвердить заказ'); ?></button></li>
                <li><?= _t('', 'или'); ?> <a href="#" data-id="<?= $v['id'] ?>" class="ajax-link link-hover-red j-decline"><span><?= _t('orders', 'отказаться от него'); ?></span></a></li>
            </ul>
        </div>
    </div>
    <? endforeach; ?>
    <? if(false) foreach($performerStart as $v): ?>
    <div class="modal fade" id="j-modal-decline-<?= $v['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= _t('orders', 'Отказаться от предложения'); ?></h4>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <p><?= _t('orders', 'Вы уверены, что хотите отказаться от заказа [link]?', array('link' => '<a href="'.$v['link'].'" class="nowrap">'.$v['title'].'</a>')); ?></p>
                        <div class="form-group">
                            <label for="modal-decline-reason-<?= $v['id'] ?>"><?= _t('orders', 'Причина отказа'); ?></label>
                            <textarea rows="4" class="form-control" id="modal-decline-reason-<?= $v['id'] ?>" placeholder="<?= _t('orders', 'Укажите причину отказа'); ?>" name="message"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger j-decline" data-id="<?= $v['id'] ?>"><?= _t('orders', 'Отказаться'); ?></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= _t('form', 'Отмена'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <? endforeach; ?>
</div>
<? endif; ?>
