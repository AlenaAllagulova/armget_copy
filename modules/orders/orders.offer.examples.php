<?php

class OrdersOfferExamples_ extends Component
{
    /** @var integer ID пользователя */
    protected $userID = 0;

    /** @var integer ID записи */
    protected $recordID = 0;

    /**
     * Максимально доступное кол-во примеров у одной заявки
     * 0 - неограничено
     * @var integer
     */
    protected $limit = 5;

    public function __construct($nRecordID = 0)
    {
        $this->init();
        $this->setRecordID($nRecordID);
        $this->setUserID(User::id());
        $this->initSettings();
    }

    function initSettings()
    {
        $this->limit = Orders::offerExamplesLimit();
    }

    public function setRecordID($nRecordID)
    {
        $this->recordID = $nRecordID;
    }

    public function setUserID($nUserID)
    {
        $this->userID = $nUserID;
    }

    public function save($sFieldname = 'sort')
    {
        $aTmp = $this->input->post($sFieldname, TYPE_ARRAY_STR);
        $oImages = Orders::i()->offerImages($this->recordID);
        $aDataImages = $oImages->getData();
        $aImages = array();
        foreach ($aDataImages as $v) {
            $aImages[$v['filename']] = $v;
        }

        $aExamples = $this->availableExamples();
        $aPortfolio = array();
        if ( ! empty($aExamples['portfolio'])) {
            foreach ($aExamples['portfolio'] as $v) {
                foreach ($v['items'] as $vv) {
                    $aPortfolio[$vv['id']] = $vv['id'];
                }
            }
        }

        $aShop = array();
        if ( ! empty($aExamples['shop'])) {
            foreach ($aExamples['shop'] as $v) {
                foreach ($v['items'] as $vv) {
                    $aShop[$vv['id']] = $vv['id'];
                }
            }
        }
        $aInsert = array();
        $nCnt = 0;
        foreach ($aTmp as $v) {
            $v = explode('=', $v);
            switch($v[0]){
                case 'fn':
                {
                    if (!empty($aImages[$v[1]])) {
                        $aImg = $aImages[$v[1]];
                        if ($nCnt < $this->limit) {
                            $aInsert[] = array(
                                'offer_id' => $this->recordID,
                                'image_id' => $aImg['id'],
                                'portfolio_id' => 0,
                                'product_id'   => 0,
                                'num' => $nCnt + 1,
                                'url_cache' => $oImages->getURL($aImg, OrdersOfferImages::szSmall, false),
                            );
                            $nCnt++;
                        }
                    }
                } break;
                case 'portfolio':
                {
                    if (!empty($aPortfolio[$v[1]])) {
                        $nID = $aPortfolio[$v[1]];
                        if ($nCnt < $this->limit) {
                            $aInsert[] = array(
                                'offer_id'     => $this->recordID,
                                'image_id'     => 0,
                                'portfolio_id' => $nID,
                                'product_id'   => 0,
                                'num'          => $nCnt + 1,
                                'url_cache'    => '',
                            );
                            $nCnt++;
                        }
                    }

                } break;
                case 'shop':
                {
                    if (!empty($aShop[$v[1]])) {
                        $nID = $aShop[$v[1]];
                        if ($nCnt < $this->limit) {
                            $aInsert[] = array(
                                'offer_id'     => $this->recordID,
                                'image_id'     => 0,
                                'portfolio_id' => 0,
                                'product_id'   => $nID,
                                'num'          => $nCnt + 1,
                                'url_cache'    => '',
                            );
                            $nCnt++;
                        }
                    }

                } break;
            }
        }
        if (!empty($aInsert)) {
            $this->db->multiInsert(TABLE_ORDERS_OFFERS_EXAMPLES, $aInsert);
            if ($nCnt) {
                $this->db->update(TABLE_ORDERS_OFFERS, array('examplescnt' => $nCnt), array('id' => $this->recordID));
            }
        }
    }

    /**
     * Получаем данные о загруженных и сохраненных на текущий момент изображениях
     * @param mixed $nCount кол-во изображений, false - если не знаем
     * @param string $sKey по какому полю строить ключи в результате
     * @param string $sLogin логин владельца заявки
     * @return array данные об изображениях или FALSE
     */
    public function getData($nCount = false, $sKey = 'id', $sLogin = '')
    {
        if (($nCount !== false && intval($nCount) === 0) || $this->recordID === 0) {
            return array();
        }
        Portfolio::i();
        $bShopPresent = (bff::moduleExists('shop') && Shop::enabled());
        $aData =  $this->db->select_key('
            SELECT E.*, P.keyword AS portfolio_keyword, P.preview AS portfolio_preview '.($bShopPresent ? ', S.keyword AS product_keyword, S.img_s AS product_preview' : '').'
            FROM ' . TABLE_ORDERS_OFFERS_EXAMPLES . ' E
                LEFT JOIN '.TABLE_PORTFOLIO_ITEMS.' P ON E.portfolio_id = P.id
                '.($bShopPresent ? ' LEFT JOIN '.TABLE_SHOP_PRODUCTS.' S ON E.product_id = S.id ' : '').'
            WHERE E.offer_id = :id
            ORDER BY E.num ', $sKey,
            array(':id' => $this->recordID)
        );

        $oImages = Orders::i()->offerImages($this->recordID);
        $aDataImages = $oImages->getData();
        foreach ($aData as $k => $v) {
            if (isset($aDataImages[ $v['image_id'] ])) {
                $aData[$k]['img'] = $aDataImages[ $v['image_id'] ];
            }
            if ($v['portfolio_id']) {
                $aData[$k]['portfolio_preview'] = PortfolioItemPreview::url($v['portfolio_id'], $v['portfolio_preview']);
                $aData[$k]['portfolio_link'] = Portfolio::url('user.view', array('login' => $sLogin, 'id' => $v['portfolio_id'], 'keyword' => $v['portfolio_keyword']));
            }
            if ($v['product_id'] && $bShopPresent) {
                $aData[$k]['product_link'] = Shop::url('user.view', array('login' => $sLogin, 'id' => $v['product_id'], 'keyword' => $v['product_keyword']));
                if ( ! $v['product_preview']) {
                    $aData[$k]['product_preview'] = ShopProductImages::defaultUrl(ShopProductImages::szSmall);
                }
            }
        }
        return $aData;
    }

    public function deleteAllExamples($bUpdateQuery = false)
    {
        Orders::i()->offerImages($this->recordID)->deleteAllImages($bUpdateQuery);
        $this->db->delete(TABLE_ORDERS_OFFERS_EXAMPLES, array('offer_id' => $this->recordID));

        return true;
    }

    /**
     * Данные о доступных для прикрепления к заявке: работах (portfolio) и товарах (shop)
     * @return array
     */
    public function availableExamples()
    {
        $nUserID = User::id();
        $aData = array();
        if ( ! $nUserID) return $aData;

        # Работы
        $aPortfolio = Portfolio::model()->itemsList(0, array('user_id'=>$nUserID));
        if ( ! empty($aPortfolio)) {
            $aData['portfolio'] = array();
            foreach ($aPortfolio as $v) {
                if ( ! empty($v)) {
                    foreach ($v as $vv) {
                        $vv['preview'] = PortfolioItemPreview::url($vv['id'], $vv['preview']);
                        if ( ! isset($aData['portfolio'][ $vv['spec_id'] ])) {
                            $aData['portfolio'][ $vv['spec_id'] ] = array(
                                'spec_id' => $vv['spec_id'],
                                'title'   => $vv['spec_title'],
                                'items'   => array(),
                            );
                        }
                        $aData['portfolio'][ $vv['spec_id'] ]['items'][ $vv['id'] ] = $vv;
                    }
                }
            }
        }

        # Товары
        if (bff::moduleExists('shop') && Shop::enabled()) {
            $aShop = Shop::model()->productsOwnerList(0, array('user_id'=>$nUserID));
            if ( ! empty($aShop)) {
                $aData['shop'] = array();
                foreach ($aShop as $v) {
                    if ( ! empty($v)) {
                        foreach($v as $vv){
                            if( ! isset($aData['shop'][ $vv['cat_id'] ])){
                                $aData['shop'][ $vv['cat_id'] ] = array(
                                    'cat_id' => $vv['cat_id'],
                                    'cat_id1' => $vv['cat_id1'],
                                    'cat_id2' => $vv['cat_id2'],
                                    'title'   => $vv['cat_title'],
                                    'items'   => array(),
                                );
                            }
                            if( ! $vv['img_s']){
                                $vv['img_s'] = ShopProductImages::defaultUrl(ShopProductImages::szSmall);
                            }
                            $aData['shop'][ $vv['cat_id'] ]['items'][ $vv['id'] ] = $vv;
                        }
                    }
                }
            }
        }

        return $aData;
    }

}