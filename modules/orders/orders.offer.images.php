<?php

class OrdersOfferImages_ extends CImagesUploaderTable
{
    /**
     * Константы размеров
     */
    const szSmall   = 's'; # small
    const szView    = 'v'; # view - просмотр
    const szOrginal = 'o'; # original - оригинальное изображение

    function initSettings()
    {
        $this->path = bff::path('offers', 'images');
        $this->pathTmp = bff::path('tmp', 'images');
        $this->url = bff::url('offers', 'images');
        $this->urlTmp = bff::url('tmp', 'images');

        $this->tableRecords = TABLE_ORDERS_OFFERS;
        $this->tableImages = TABLE_ORDERS_OFFERS_IMAGES;

        $this->folderByID = config::sysAdmin('orders.offer.images.folderbyid', true, TYPE_BOOL); # раскладываем файлы изображений по папкам (id(5)=>0, id(1005)=>1, ...)
        $this->filenameLetters = config::sysAdmin('orders.offer.images.filename.letters', 8, TYPE_UINT); # кол-во символов в названии файла
        $this->limit = Orders::offerExamplesLimit(); # лимит фотографий у заявки
        $this->maxSize = config::sysAdmin('orders.offer.images.max.size', 5242880, TYPE_UINT); # 2мб (2мб: 2097152, 5мб: 5242880)

        $config = Orders::i()->configLoad(array(
            'offers_images_width_min'  => 500,
            'offers_images_width_max'  => 5000,
            'offers_images_height_min' => 500,
            'offers_images_height_max' => 5000,
        ));
        $this->minWidth  = $config['offers_images_width_min'];
        $this->maxWidth  = $config['offers_images_width_max'];
        $this->minHeight = $config['offers_images_height_min'];
        $this->maxHeight = $config['offers_images_height_max'];

        # размеры изображений
        $this->sizes = bff::filter('orders.offer.images.sizes', array(
            self::szSmall   => array(
                'width'    => 220,
                'height'   => 180,
            ),
            self::szView    => array(
                'width'    => 670,
                'height'   => false,
                'vertical' => array('width' => false, 'height' => 670)
            ),
            self::szOrginal => array('o' => true),
        ));

        # настройки водяного знака
        $watermark = Site::i()->watermarkSettings('orders');
        if (!empty($watermark)) {
            $this->sizes[self::szView] += array(
                'watermark'       => true,
                'watermark_src'   => $watermark['file']['path'],
                'watermark_pos_x' => $watermark['pos_x'],
                'watermark_pos_y' => $watermark['pos_y'],
            );
        }

        # размеры изображений, полный URL которых необходимо кешировать
        $this->useFav = false;
    }

    public function urlDefault($sSizePrefix)
    {
        return $this->url . 'def-' . $sSizePrefix . '.png';
    }

    /**
     * Получаем данные об изображениях указанных заказов
     * @param array $aItemsID ID заказов
     * @return array массив параметров изображений сформированных: array(itemID=>data, ...)
     */
    public function getItemsImagesData($aItemsID)
    {
        if (empty($aItemsID)) {
            return array();
        }
        if (!is_array($aItemsID)) {
            $aItemsID = array($aItemsID);
        }
        $aData = $this->db->select('SELECT * FROM ' . $this->tableImages . '
                    WHERE ' . $this->fRecordID . ' IN (' . join(',', $aItemsID) . ')
                    ORDER BY num'
        );
        if (!empty($aData)) {
            $aData = func::array_transparent($aData, $this->fRecordID, false);
        }

        return $aData;
    }

    /**
     * Получаем дату самого последнего добавленного изображения
     * @param boolean $buildHash сформировать hash на основе даты
     * @return integer|string
     */
    public function getLastUploaded($buildHash = true)
    {
        $lastUploaded = $this->db->one_data('SELECT MAX(created) FROM ' . $this->tableImages . '
                    WHERE ' . $this->fRecordID . ' = :id
                    LIMIT 1', array(':id' => $this->recordID)
        );
        if (!empty($lastUploaded)) {
            $lastUploaded = strtotime($lastUploaded);
        } else {
            $lastUploaded = mktime(0, 0, 0, 1, 1, 2000);
        }

        return ($buildHash ? $this->getLastUploadedHash($lastUploaded) : $lastUploaded);
    }

    /**
     * Формируем hash на основе даты самого последнего добавленного изображения
     * @return integer
     */
    public function getLastUploadedHash($lastUploaded)
    {
        $base64 = base64_encode($lastUploaded);

        return md5(strval($lastUploaded - 1000) . SITEHOST . $base64) . '.' . $base64;
    }

    /**
     * Выполняем проверку, загружались ли новые изображения
     * @param string $lastUploaded hash даты последнего загруженного изображения
     * @return boolean
     */
    public function newImagesUploaded($lastUploaded)
    {
        # проверка hash'a
        if (empty($lastUploaded) || ($dot = strpos($lastUploaded, '.')) !== 32) {
            return true;
        }
        $date = intval(base64_decode(mb_substr($lastUploaded, $dot + 1)));
        if ($this->getLastUploadedHash($date) !== $lastUploaded) {
            return true;
        }
        # выполнялась ли загрузка новых изображений
        if ($this->getLastUploaded(false) > intval($date)) {
            return true;
        }

        return false;
    }

}