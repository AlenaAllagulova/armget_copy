<?php

class M_Orders_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('orders','Orders');
        $module = 'orders';

        # Заказы
        if ($security->haveAccessToModuleToMethod($module, 'orders') ||
            $security->haveAccessToModuleToMethod($module, 'orders-moderate')) {
            $menu->assign($menuTitle, _t('orders','Orders'), $module, 'orders_list', true, 10,
                array('rlink' => array('event' => 'orders_list&act=add'), 'counter' => 'orders_orders_moderating')
            );
            $menu->adminHeaderCounter($menuTitle, 'orders_orders_moderating', $module, 'orders_list&tab=2', 1, '', array('parent'=>'moderation'));
        }

        # Заявки
        if ($security->haveAccessToModuleToMethod($module, 'offers') ||
            $security->haveAccessToModuleToMethod($module, 'offers-moderate')) {
            $menu->assign($menuTitle, _t('orders','Заявки'), $module, 'offers_list', true, 20,
                array('counter' => 'orders_offers_moderating'));
            $menu->adminHeaderCounter(_t('orders','Заявки'), 'orders_offers_moderating', $module, 'offers_list', 2, '', array('parent'=>'moderation'));
        }

        # Навыки
        if ($security->haveAccessToModuleToMethod($module, 'tags')) {
            $menu->assign($menuTitle, _t('orders','Навыки'), $module, 'tags', true, 30);
        }

        # Услуги
        if ($security->haveAccessToModuleToMethod($module, 'svc')) {
            $menu->assign('Счет и услуги', 'Услуги: '.$menuTitle, $module, 'svc_services', true, 10);
        }

        # Настройки
        if ($security->haveAccessToModuleToMethod($module,'settings')) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 60);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module,'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 10);
        }

    }
}