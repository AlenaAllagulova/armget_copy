<?php

class NewsTags_ extends bff\db\Tags
{
    protected function initSettings()
    {
        $this->tblTags = TABLE_NEWS_TAGS;
        $this->tblTagsIn = TABLE_NEWS_NEWS_TAGS;
        $this->tblTagsIn_ItemID = 'news_id';
        $this->urlItemsListing = $this->adminLink('listing&tag=', 'news');
    }
}