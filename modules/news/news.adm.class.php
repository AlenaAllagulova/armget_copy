<?php

/**
 * Права доступа группы:
 *  - news: Новости
 *      - items: Управление новостями (список, добавление, редактирование, удаление)
 *      - tags: Управление тегами (список, добавление, редактирование, удаление)
 *      - settings: Настройки
 *      - seo: SEO
 */
class News_ extends NewsBase
{
    public function listing()
    {
        if ( ! $this->haveAccessTo('items'))
            return $this->showAccessDenied();

        $sAct = $this->input->postget('act',TYPE_STR);
        if ( ! empty($sAct) || Request::isPOST())
        {
            $aResponse = array();
            switch ($sAct)
            {
                case 'add':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateNewsData(0, $bSubmit);
                    if ($bSubmit)
                    {
                        if ($this->errors->no()) {

                            $nNewsID = $this->model->newsSave(0, $aData);
                            if ($nNewsID > 0) {
                                $this->newsTags()->tagsSave($nNewsID);
                            }
                        }
                        break;
                    }

                    $aData['id'] = 0;

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.news.form');
                } break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nNewsID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nNewsID) { $this->errors->unknownRecord(); break; }

                    if ($bSubmit)
                    {
                        $aData = $this->validateNewsData($nNewsID, $bSubmit);
                        if ($this->errors->no()) {

                            $this->model->newsSave($nNewsID, $aData);
                            $this->newsTags()->tagsSave($nNewsID);
                        }
                        $aData['id'] = $nNewsID;
                        break;
                    } else {
                        $aData = $this->model->newsData($nNewsID, true);
                        if (empty($aData)) { $this->errors->unknownRecord(); break; }
                    }

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.news.form');
                } break;
                case 'toggle':
                {
                    $nNewsID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nNewsID) { $this->errors->unknownRecord(); break; }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->newsToggle($nNewsID, $sToggleType);
                } break;
                case 'delete':
                {
                    $nNewsID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nNewsID) { $this->errors->impossible(); break; }

                    $aData = $this->model->newsData($nNewsID, true);
                    if (empty($aData)) { $this->errors->impossible(); break; }

                    $res = $this->model->newsDelete($nNewsID);
                    if ( ! $res) { $this->errors->impossible(); break; }
                    else {
                        $this->newsTags()->onItemDelete($nNewsID);
                    }
                } break;
                case 'tags-suggest': # autocomplete.fb
                {
                    $sQuery = $this->input->postget('tag', TYPE_STR);
                    $this->newsTags()->tagsAutocomplete($sQuery);
                }
                break;
                case 'tags-autocomplete': # autocomplete
                {
                    $sQuery = $this->input->post('q', TYPE_STR);
                    $this->newsTags()->tagsAutocomplete($sQuery);
                }
                break;
                case 'massend':
                {
                    $nNewsID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nNewsID) { $this->errors->impossible(); break; }

                    if( ! $this->model->massend($nNewsID)){
                        $this->errors->impossible(); break;
                    }
                }
                break;
                default: $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) $this->ajaxResponseForm($aResponse);
        }

        $f = array();
        $this->input->postgetm(array(
            'page'   => TYPE_UINT,
            'tag'     => TYPE_UINT,
        ), $f);

        # формируем фильтр списка
        $sql = array();
        $mPerpage = 15;
        $aData['pgn'] = '';

        if ($f['tag'] > 0) {
            $aData['tag'] = $this->newsTags()->tagData($f['tag']);
            if (empty($aData['tag'])) {
                $f['tag'] = 0;
            } else {
                $sql[':tag'] = $f['tag'];
            }
        }

        $aData['orders'] = array('id'=>'asc','created'=>'desc',);
        $f += $this->prepareOrder($orderBy, $orderDirection, 'id-asc', $aData['orders']);
        $f['order'] = $orderBy.'-'.$orderDirection; $sqlOrder = "$orderBy $orderDirection";

        if ($mPerpage!==false) {
            $nCount = $this->model->newsListing($sql, true);
            $oPgn = new Pagination($nCount, $mPerpage, '#', 'jNewsNewsList.page('.Pagination::PAGE_ID.'); return false;');
            $oPgn->pageNeighbours = 6;
            $aData['pgn'] = $oPgn->view(array('arrows'=>false));
            $aData['list'] = $this->model->newsListing($sql, false, $oPgn->getLimitOffset(), $sqlOrder);
        } else {
            $aData['list'] = $this->model->newsListing($sql, false, '', $sqlOrder);
        }

        $aData['list'] = $this->viewPHP($aData, 'admin.news.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list' => $aData['list'],
                'pgn'  => $aData['pgn'],
            ));
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        tpl::includeJS(array('wysiwyg','autocomplete','autocomplete.fb'), true);
        tpl::includeJS(array('datepicker'));
        if(static::commentsEnabled()) {
            $this->newsComments()->admListingIncludes();
        }

        return $this->viewPHP($aData, 'admin.news.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nNewsID ID новости или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateNewsData($nNewsID, $bSubmit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langNews, $aData);
        $aParams = array(
            'enabled'   => TYPE_BOOL,   # Включена
            'created'   => TYPE_NOTAGS, # Дата создания
            'mtemplate' => TYPE_BOOL, # Использовать общий шаблон SEO
        );
        if(static::commentsEnabled()){
            $aParams['comments_enabled'] = TYPE_BOOL; # Комментарии включены
        }
        $this->input->postm($aParams, $aData);

        if ($bSubmit)
        {
            # Дата создания
            $aData['created'] = trim(strval($aData['created']));
            $aData['created'] = date('Y-m-d', strtotime($aData['created']));

            # URL-Keyword
            $aData['keyword'] = mb_strtolower(func::translit($aData['title'][LNG]));
            $aData['keyword'] = preg_replace('/[^a-zA-Z0-9_\-]/', '', $aData['keyword']);


        } else {
            if (!$nNewsID) {
                $aData['created'] = date('Y-m-d');
                $aData['mtemplate'] = 1;
                $aData['comments_enabled'] = 1;
            }
        }
        return $aData;
    }

    public function tags()
    {
        if (!$this->haveAccessTo('tags')) {
            return $this->showAccessDenied();
        }

        return $this->newsTags()->manage();
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'share_code' => TYPE_STR,
            ), $aData);

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad();
        if( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

    public function comments_ajax()
    {
        if (!$this->haveAccessTo('news-comments')) {
            return $this->showAccessDenied();
        }

        $this->newsComments()->admAjax();
    }

    public function comments_mod()
    {
        if (!$this->haveAccessTo('news-comments')) {
            return $this->showAccessDenied();
        }

        return $this->newsComments()->admListingModerate(15);
    }

}