<?php

abstract class NewsBase_ extends Module
{
    /** @var NewsModel */
    var $model = null;
    var $securityKey = '389f54aff0d216498c2b91a818736456';

    const OWNER_ID = 1;

    public function init()
    {
        parent::init();

        $this->module_title = _t('news','Новости');

        bff::autoloadEx(array(
            'NewsTags'     => array('app', 'modules/news/news.tags.php'),
            'NewsComments' => array('app', 'modules/news/news.comments.php'),
        ));
    }

    /**
     * Shortcut
     * @return News
     */
    public static function i()
    {
        return bff::module('News');
    }

    /**
     * Shortcut
     * @return NewsModel
     */
    public static function model()
    {
        return bff::model('News');
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Список новостей
            case 'search':
                $url .= '/news/' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Поиск по тегу
            case 'tag':
                $url .= '/news/' . $opts['tag'].'-'.$opts['id']. static::urlQuery($opts, array('id','tag'));
                break;
            # Просмотр новости
            case 'view':
                $url .= '/news/'.$opts['keyword'].'-'.$opts['id'].'.html'.
                    static::urlQuery($opts, array('id','keyword'));
                break;
        }
        return bff::filter('news.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    public static function commentsEnabled()
    {
        return config::sysAdmin('news.comments', false, TYPE_BOOL);
    }

    /**
     * Склеивать теги в "еще" когда их кол-во превышает допустимый лимит
     * @return integer
     */
    public static function searchTagsLimit()
    {
        return config::sysAdmin('news.search.tags.limit', 0, TYPE_UINT);
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'news_massend' => array(
                'title'       => 'Новости: Рассылка новости',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при выполнении рассылки новости, из формы редактирования новости',
                'vars'        => array(
                    '{fio}'   => 'ФИО пользователя',
                    '{name}'  => 'Имя пользователя',
                    '{title}' => 'Заголовок новости',
                    '{msg}'   => 'Текст новости',
                    '{url}'   => 'Ссылка для просмотра',
                ),
                'impl'        => true,
                'priority'    => 120,
                'enotify'     => Users::ENOTIFY_NEWS,
            ),
        );

        return $aTemplates;
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        return array(
            'pages'  => array(
                'listing' => array(
                    't'      => 'Список',
                    'list'   => true,
                    'macros' => array(),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'listing-tag' => array(
                    't'      => 'Список по тегу',
                    'list'   => true,
                    'macros' => array(
                        'tag' => array('t' => 'Тег'),
                    )
                ),
                'view' => array(
                    't'       => 'Просмотр новости',
                    'inherit' => true,
                    'macros'  => array(
                        'title'       => array('t' => 'Заголовок (до 50 символов)'),
                        'title.full'  => array('t' => 'Заголовок (полный)'),
                        'description' => array('t' => 'Описание (до 150 символов)'),
                        'tags'        => array('t' => 'Теги (перечисление)'),
                    ),
                    'fields' => array(
                        'share_title'       => array(
                            't'    => 'Заголовок (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                        'share_description' => array(
                            't'    => 'Описание (поделиться в соц. сетях)',
                            'type' => 'textarea',
                        ),
                        'share_sitename'    => array(
                            't'    => 'Название сайта (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * Инициализация компонента работы с тегами
     * @return NewsTags
     */
    public function newsTags()
    {
        static $i;
        if (!isset($i)) {
            $i = new NewsTags();
        }

        return $i;
    }

    /**
     * Инициализация компонента работы с комментариями
     * @return NewsComments component
     */
    public function newsComments()
    {
        static $i;
        if (!isset($i)) {
            $i = new NewsComments();
        }
        return $i;
    }

}