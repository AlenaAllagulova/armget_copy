<?php

return [
    # поиск по тегу
    'news-search-tag' => [
        'pattern'  => 'news/(.+)-([\d]+)',
        'callback' => 'news/search/tag=$2',
        'priority' => 350,
    ],
    # просмотр новости
    'news-view' => [
        'pattern'  => 'news/(.+)-([\d]+).html',
        'callback' => 'news/view/id=$2',
        'priority' => 360,
    ],
    # главная
    'news-search' => [
        'pattern'  => 'news(/|)',
        'callback' => 'news/search',
        'priority' => 370,
    ],
];