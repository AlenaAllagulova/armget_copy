<?php

define('TABLE_NEWS',           DB_PREFIX.'news');
define('TABLE_NEWS_LANG',      DB_PREFIX.'news_lang');
define('TABLE_NEWS_TAGS',      DB_PREFIX.'news_tags');
define('TABLE_NEWS_NEWS_TAGS', DB_PREFIX.'news_news_tags');
define('TABLE_NEWS_COMMENTS',  DB_PREFIX.'news_comments');


class NewsModel_ extends Model
{
    /** @var NewsBase */
    var $controller;

    public $langNews = array(
        'title'        => TYPE_NOTAGS, # Название
        'descr_short'  => TYPE_STR,    # Краткое описание
        'descr_full'   => TYPE_STR,    # Подробное описание
        'mtitle'       => TYPE_NOTAGS, # Meta Title
        'mkeywords'    => TYPE_NOTAGS, # Meta Keywords
        'mdescription' => TYPE_NOTAGS, # Meta Description
        'share_title'    => TYPE_NOTAGS, # Meta Share Title
        'share_description' => TYPE_NOTAGS, # Meta Share Description
        'share_sitename'    => TYPE_NOTAGS, # Meta Share Sitename
    );

    public function init()
    {
        parent::init();
    }

    # --------------------------------------------------------------------
    #

    /**
     * Список новостей (admin)
     * @param array $aFilter фильтр списка
     * @param bool $bCount только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function newsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $sJoin = '';
        $aFilter[':lang'] = $this->db->langAnd(false, 'N', 'NL');
        if(isset($aFilter[':tag'])){
            $sJoin .= ', '.TABLE_NEWS_NEWS_TAGS.' T ';
            $aFilter[':tag'] = array('N.id = T.news_id AND T.tag_id = :tag', ':tag' => $aFilter[':tag']);
        }

        $aFilter = $this->prepareFilter($aFilter, 'N');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(N.id) FROM '.TABLE_NEWS.' N'.$sJoin.', '.TABLE_NEWS_LANG.' NL '.$aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT N.id, N.created, NL.title, N.enabled
               FROM '.TABLE_NEWS.' N'.$sJoin.', '.TABLE_NEWS_LANG.' NL
               '.$aFilter['where']
            .( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '')
            .$sqlLimit, $aFilter['bind']);
    }

    /**
     * Список новостей (frontend)
     * @param array $aFilter фильтр списка
     * @param bool $bCount только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function newsList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $sJoin = '';
        $aFilter['enabled'] = 1;

        if(isset($aFilter[':tag'])){
            $sJoin .= ', '.TABLE_NEWS_NEWS_TAGS.' TT ';
            $aFilter[':tag'] = array('N.id = TT.news_id AND TT.tag_id = :tag', ':tag' => $aFilter[':tag']);
        }

        if ( ! $bCount) $aFilter[':lang'] = $this->db->langAnd(false, 'N', 'NL');
        $aFilter = $this->prepareFilter($aFilter, 'N');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(N.id) FROM '.TABLE_NEWS.' N '.$sJoin.$aFilter['where'], $aFilter['bind']);
        }

        $aData = $this->db->select('
            SELECT N.id, N.keyword, N.created, NL.title, NL.descr_short, GROUP_CONCAT(T.tag_id) AS tags
            FROM '.TABLE_NEWS.' N
                LEFT JOIN '.TABLE_NEWS_NEWS_TAGS.' T ON N.id = T.news_id
                '.$sJoin.', '.TABLE_NEWS_LANG.' NL
            '.$aFilter['where'].'
            GROUP BY N.id
            '.( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '').'
            '.$sqlLimit, $aFilter['bind']);

        if (empty($aData)) {
            $aData = array();
        }

        return $aData;
    }

    /**
     * Получение данных новости
     * @param integer $nNewsID ID новости
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function newsData($nNewsID, $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT N.*
                    FROM '.TABLE_NEWS.' N
                    WHERE N.id = :id',
                array(':id'=>$nNewsID));
            if ( ! empty($aData)) {
                $this->db->langSelect($nNewsID, $aData, $this->langNews, TABLE_NEWS_LANG);
            }
        } else {
            $aData = $this->db->one_array('SELECT N.*, NL.*
                    FROM '.TABLE_NEWS.' N, '.TABLE_NEWS_LANG.' NL
                    WHERE N.id = :id '.$this->db->langAnd(true, 'N', 'NL'),
                array(':id'=>$nNewsID));
        }
        return $aData;
    }

    /**
     * Сохранение новости
     * @param integer $nNewsID ID новости
     * @param array $aData данные
     * @return boolean|integer
     */
    public function newsSave($nNewsID, array $aData)
    {
        if (empty($aData)) return false;

        if ($nNewsID > 0)
        {
            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_NEWS, array_diff_key($aData, $this->langNews), array('id'=>$nNewsID));

            $this->db->langUpdate($nNewsID, $aData, $this->langNews, TABLE_NEWS_LANG);

            return ! empty($res);
        }
        else
        {
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nNewsID = $this->db->insert(TABLE_NEWS, array_diff_key($aData, $this->langNews));
            if ($nNewsID > 0) {
                $this->db->langInsert($nNewsID, $aData, $this->langNews, TABLE_NEWS_LANG);
                //
            }
            return $nNewsID;
        }
    }

    /**
     * Переключатели новости
     * @param integer $nNewsID ID новости
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function newsToggle($nNewsID, $sField)
    {
        switch ($sField) {
            case 'enabled': { # Включен
                return $this->toggleInt(TABLE_NEWS, $nNewsID, $sField, 'id');
            } break;
        }
    }

    /**
     * Удаление новости
     * @param integer $nNewsID ID новости
     * @return boolean
     */
    public function newsDelete($nNewsID)
    {
        if (empty($nNewsID)) return false;
        $res = $this->db->delete(TABLE_NEWS, array('id'=>$nNewsID));
        if ( ! empty($res)) {
            $this->db->delete(TABLE_NEWS_LANG, array('id'=>$nNewsID));
            return true;
        }
        return false;
    }

    /**
     * Формирование рассылки новости
     * @param integer $nNewsID ID новости
     * @return bool
     */
    public function massend($nNewsID)
    {
        if (!$nNewsID) return false;

        $aData = $this->newsData($nNewsID);
        if (empty($aData)) return false;

        $nUsersCnt = $this->db->one_data('
            SELECT COUNT(*)
            FROM '.TABLE_USERS.' U
            WHERE U.blocked = 0 AND U.activated = 1 AND (U.enotify & :enotify != 0)
        ', array(
            ':enotify' => Users::ENOTIFY_NEWS,
        ));
        if (!$nUsersCnt) {
            return false;
        }

        $aMailData = array(
            'title'    => $aData['title'],
            'msg'      => $aData['descr_full'],
            'url'      => News::url('view', array('id'=>$aData['id'], 'keyword'=>$aData['keyword'])),
        );
        $aTplData = Sendmail::i()->getMailTemplate('news_massend', $aMailData);

        $nMassendID = $this->db->insert(TABLE_MASSEND, array(
                'type'     => Sendmail::TYPE_MANUAL,
                'total'    => $nUsersCnt,
                'started'  => $this->db->now(),
                'settings' => serialize(array(
                        'from'       => config::sys('mail.noreply'),
                        'subject'    => $aTplData['subject'],
                        'body'       => $aTplData['body'],
                        'is_html'    => $aTplData['is_html'],
                        'time_total' => 0,
                        'time_avg'   => 0
                    )
                ),
            )
        );
        if (empty($nMassendID)) {
            bff::log('Ошибка инициализации рассылки');
            return false;
        }

        $this->db->exec('
            INSERT INTO '.TABLE_MASSEND_RECEIVERS.' (massend_id, user_id)
                SELECT :massend AS massend_id, U.user_id
                FROM '.TABLE_USERS.' U
                WHERE U.blocked = 0 AND U.activated = 1 AND (U.enotify & :enotify != 0)
        ', array(
            ':massend' => $nMassendID,
            ':enotify' => Users::ENOTIFY_NEWS,
        ));

        $this->newsSave($nNewsID, array('delivered' => $this->db->now()));

        return true;
    }

    public function getLocaleTables()
    {
        return array(
            TABLE_NEWS => array('type' => 'table', 'fields' => $this->langNews, 'title' => _t('news', 'Записи')),
        );
    }
}