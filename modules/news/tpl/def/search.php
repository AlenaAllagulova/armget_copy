<?php
tpl::includeJS('news.search', false, 2);
?>
    <div class="container">

        <section class="l-mainContent">
            <div class="row">

                <aside class="col-md-3 visible-lg">
                    <?= $form ?>
                </aside>

                <div class="col-md-9 l-content-column" id="j-news-search">
                    <div class="j-list"><?= $list ?></div>
                    <div class="j-pagination"><?= $pgn ?></div>
                </div>

            </div>
        </section>
    </div>
<script type="text/javascript">
    <? js::start() ?>
    jNewsSearch.init(<?= func::php2js(array(
        'lang' => array(),
        'ajax' => true,
    )) ?>);
    <? js::stop() ?>
</script>