<?php
/**
 * @var $this News
 */

?>
<?= tplAdmin::blockStart('Добавление', false, array('id'=>'NewsNewsFormBlock','style'=>'display:none;')); ?>
<div id="NewsNewsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('news', 'News'), true, array('id'=>'NewsNewsListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>'+ добавить','class'=>'ajax','onclick'=>'return jNewsNewsFormManager.action(\'add\',0);'),
    array()
); ?>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="NewsNewsListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="order" value="<?= $f['order'] ?>" />
        <input type="hidden" name="tag" id="NewsListFiltersTagId" value="<?= $f['tag'] ?>" />


        <div class="left">
            <label class="relative" style="margin-left: 10px;">
                <input type="text" class="autocomplete input-medium" id="NewsListFiltersTagTitle" placeholder="тег" value="<?= ($f['tag']>0 ? $tag['tag'] : '') ?>" />
            </label>
            <input type="button" class="btn btn-small" value="<?= _t('', 'search') ?>"  onclick="jNewsNewsList.submit(false);" />
        </div>
        <div class="left" style="margin-left: 8px;"><a class="ajax cancel" onclick="jNewsNewsList.submit(true); return false;"><?= _t('', 'reset') ?></a></div>
        <div class="right">
            <div id="NewsNewsProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="NewsNewsListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">
            <a href="javascript: jNewsNewsList.onOrder('id');" class="ajax">ID</a>
            <div class="order-<?= $f['order_dir'] ?>" <? if($f['order_by']!='id') { ?>style="display:none;"<? } ?> id="news-order-id"></div>
        </th>
        <th class="left"><?= _t('', 'Title') ?></th>
        <th width="130">
            <a href="javascript: jNewsNewsList.onOrder('created');" class="ajax"><?= _t('', 'Created') ?></a>
            <div class="order-<?= $f['order_dir'] ?>" <? if($f['order_by']!='created') { ?>style="display:none;"<? } ?> id="news-order-created"></div>
        </th>
        <th width="135"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="NewsNewsList">
    <?= $list ?>
    </tbody>
</table>
<div id="NewsNewsListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    var jNewsNewsFormManager = (function(){
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function(){
            $formContainer = $('#NewsNewsFormContainer');
            $progress = $('#NewsNewsProgress');
            $block = $('#NewsNewsFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if( ! empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
        });

        function onFormToggle(visible)
        {
            if(visible) {
                jNewsNewsList.toggle(false);
                if(jNewsNewsForm) jNewsNewsForm.onShow();
            } else {
                jNewsNewsList.toggle(true);
            }
        }

        function initForm(type, id, params)
        {
            if( process ) return;
            bff.ajax(ajaxUrl,params,function(data){
                if(data && (data.success || intval(params.save)===1)) {
                    $blockCaption.html((type == 'add' ? 'Добавление' : 'Редактирование'));
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo( $blockCaption, {duration:500, offset:-300});
                    onFormToggle(true);
                    if(bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                    }
                } else {
                    jNewsNewsList.toggle(true);
                }
            }, function(p){ process = p; $progress.toggle(); });
        }

        function action(type, id, params)
        {
            <? $tab = $this->input->get('ftab', TYPE_NOTAGS); ?>
            params = $.extend(params || {}, {act:type<?= ! empty($tab) ? ',ftab:\''.$tab.'\'' : '' ?>});
            switch(type) {
                case 'add':
                {
                    if( id > 0 ) return action('edit', id, params);
                    if($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                } break;
                case 'cancel':
                {
                    $block.hide();
                    onFormToggle(false);
                } break;
                case 'edit':
                {
                    if( ! (id || 0) ) return action('add', 0, params);
                    params.id = id;
                    initForm(type, id, params);
                } break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jNewsNewsList =
        (function()
        {
            var $progress, $block, $list, $listTable, $listPgn, filters, processing = false;
            var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';
            var orders = <?= func::php2js($orders) ?>, orderby = '<?= $f['order_by'] ?>';

            $(function(){
                $progress  = $('#NewsNewsProgress');
                $block     = $('#NewsNewsListBlock');
                $list      = $block.find('#NewsNewsList');
                $listTable = $block.find('#NewsNewsListTable');
                $listPgn   = $block.find('#NewsNewsListPgn');
                filters    = $block.find('#NewsNewsListFilters').get(0);

                $list.delegate('a.news-edit', 'click', function(){
                    var id = intval($(this).data('id'));
                    if(id>0) jNewsNewsFormManager.action('edit',id);
                    return false;
                });

                $list.delegate('a.news-toggle', 'click', function(){
                    var id = intval($(this).data('id'));
                    var type = $(this).data('type');
                    if(id>0) {
                        var params = {progress: $progress, link: this};
                        bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                    }
                    return false;
                });

                $list.delegate('a.news-del', 'click', function(){
                    var id = intval($(this).data('id'));
                    if(id>0) del(id, this);
                    return false;
                });

                $(window).bind('popstate',function(){
                    if('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                    if( actForm!=null ) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jNewsNewsFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jNewsNewsFormManager.action('cancel');
                        updateList(false);
                    }
                });

                $block.find('#NewsListFiltersTagTitle').autocomplete( ajaxUrl+'tags-autocomplete',
                    {valueInput: '#NewsListFiltersTagId', placeholder: 'Тег', onSelect: function(){
                        jNewsNewsList.submit(false);
                    }});

            });

            function isProcessing()
            {
                return processing;
            }

            function del(id, link)
            {
                bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
                return false;
            }

            function updateList(updateUrl)
            {
                if(isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function(data){
                    if(data) {
                        $list.html( data.list );
                        $listPgn.html( data.pgn );
                        if(updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                    }
                }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
            }

            function setPage(id)
            {
                filters.page.value = intval(id);
            }

            return {
                submit: function(resetForm)
                {
                    if(isProcessing()) return false;
                    setPage(1);
                    if(resetForm) {
                        filters['tag'].value = 0;
                        $block.find('#NewsListFiltersTagTitle').val('');
                    }
                    updateList();
                },
                page: function (id)
                {
                    if(isProcessing()) return false;
                    setPage(id);
                    updateList();
                },
                onOrder: function(by)
                {
                    if(isProcessing() || !orders.hasOwnProperty(by))
                        return;

                    orders[by] = (orders[by] == 'asc' ? 'desc' : 'asc');
                    $('#news-order-'+orderby).hide();
                    orderby = by;
                    $('#news-order-'+orderby).removeClass('order-asc order-desc').addClass('order-'+orders[by]).show();

                    filters.order.value = orderby+'-'+orders[by];
                    setPage(1);

                    updateList();
                },
                refresh: function(resetPage, updateUrl)
                {
                    if(resetPage) setPage(0);
                    updateList(updateUrl);
                },
                toggle: function(show)
                {
                    if(show === true) {
                        $block.show();
                        if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                }
            };
        }());
</script>