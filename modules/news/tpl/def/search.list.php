<?php
    if( ! empty($list)): $tagsLimit = News::searchTagsLimit(); ?>
        <ul class="n-newsList media-list">
            <? foreach($list as $v): ?>
            <li class="media">
                <div class="media-body">
                    <div class="n-item-info">
                        <div class="n-date"><?= tpl::date_format2($v['created']) ?></div>
                        <header class="n-item-name">
                            <h2><a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a></h2>
                        </header>

                        <? if($v['tags']): $t = explode(',', $v['tags']); $n = 0; ?>
                        <div class="hidden">
                            <? foreach($t as $vv): if(empty($tags[ $vv ])) continue; ?>
                            <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                            <a href="<?= News::url('tag', array('id' => $tags[ $vv ]['id'], 'tag' => $tags[ $vv ]['tag'])) ?>" class="l-tag<?= ($tag_id == $vv ? ' active' : '') ?><?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $tags[ $vv ]['tag'] ?></a>
                            <? endforeach; ?>
                        </div>
                        <? endif; ?>

                        <article class="b-blogList-description">
                           <?= $v['descr_short'] ?>
                        </article>
                    </div>
                </div>
            </li>
            <? endforeach; ?>
        </ul>
    <? else: ?>
        <div class="alert alert-info"><?= _t('news', 'Новости не найдены'); ?></div>
    <? endif;
