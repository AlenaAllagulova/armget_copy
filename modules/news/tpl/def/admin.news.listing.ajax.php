<?php
/**
 * @var $this News
 */
foreach ($list as $k=>&$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $id ?></td>
        <td class="left"><?= $v['title'] ?></td>
        <td><?= tpl::date_format2($v['created']) ?></td>
        <td>
            <a class="but <?= ($v['enabled']?'un':'') ?>block news-toggle" title="Включен" href="#" data-type="enabled" data-id="<?= $id ?>"></a>
            <a class="but edit news-edit" title="<?= _t('', 'Edit') ?>" href="#" data-id="<?= $id ?>"></a>
            <a class="but del news-del" title="<?= _t('', 'Delete') ?>" href="#" data-id="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="4">
            ничего не найдено
        </td>
    </tr>
<? endif;