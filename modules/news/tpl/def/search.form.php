<?php
?>
<form method="get" action="" id="j-news-search-form">
    <input type="hidden" name="page" value="<?= $f['page'] ?>" />
</form>

<? if( ! empty($tags)): ?>
<div class="visible-md visible-lg">
    <h5><?= _t('news', 'Темы новостей'); ?></h5>

    <div class="a-article-menu">
        <? foreach($tags as $v): ?>
        <a href="<?= News::url('tag', array('tag' => $v['tag'], 'id' => $v['id'])) ?>" class="l-tag<?= ($tag_id == $v['id'] ? ' active' : '') ?>"><?= $v['tag'] ?> <small><?= $v['items'] ?></small></a>
        <? endforeach; ?>
    </div>
</div>
<? endif; ?>

<? # Баннер: Новости: список ?>
<?= Banners::view('news_list', array('pos'=>'left')) ?>