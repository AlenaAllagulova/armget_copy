<?php
    /**
     * @var $this News
     */
    $aTabs = array(
        'info' => 'Основные',
        'seo'  => 'SEO',
    );
    $edit = ! empty($id);
    $commentsEnabled = News::commentsEnabled();
    if($edit && $commentsEnabled){
        $aTabs['comments'] = 'Комментарии';
    }
    if (empty($delivered)) $delivered = '0000-00-00 00:00:00';
    $tab = $this->input->getpost('ftab', TYPE_NOTAGS);
    if( ! isset($aTabs[$tab])) {
        $tab = 'info';
    }
?>
<div id="NewsNewsFormBlock">
<form name="NewsNewsForm" id="NewsNewsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
<input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
<input type="hidden" name="save" value="1" />
<input type="hidden" name="id" value="<?= $id ?>" />
<div class="tabsBar" id="NewsNewsFormTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab<? if($k == $tab) { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
    <? } ?>
</div>
<div class="j-tab j-tab-info<?= $tab != 'info' ? ' hidden' : '' ?>">
    <table class="admtbl tbledit">
        <tr class="required">
            <td class="row1 field-title" width="100"><?= _t('','Дата') ?><span class="required-mark">*</span>:</td>
            <td class="row2">
                <div class="left">
                    <input type="text" id="news-created" name="created" value="<?= date('d-m-Y', strtotime($created)) ?>" style="width: 75px;"/>
                </div>
                <div class="right desc">
                    <? if($edit && $modified!='0000-00-00 00:00:00' ) { ?>
                        последние изменения: <?= tpl::date_format2($modified, true); ?>
                    <? } ?>
                </div>
                <div class="clear clearfix"></div>
            </td>
        </tr>

        <?= $this->locale->buildForm($aData, 'news-item','
<tr class="required">
    <td class="row1 field-title">Название<span class="required-mark">*</span>:</td>
    <td class="row2">
        <input class="stretch <?= $key ?>" type="text" id="news-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
    </td>
</tr>
<tr class="required">
    <td class="row1 field-title">Краткое описание<span class="required-mark">*</span>:</td>
    <td class="row2">
        <?= tpl::jwysiwyg($aData[\'descr_short\'][$key], \'descr_short[\'.$key.\']\', 0, 70); ?>
    </td>
</tr>
<tr class="required">
    <td class="row1 field-title">Подробное описание<span class="required-mark">*</span>:</td>
    <td class="row2">
        <?= tpl::jwysiwyg($aData[\'descr_full\'][$key], \'descr_full[\'.$key.\']\', 0, 215); ?>
    </td>
</tr>
'); ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'Tags') ?>:</td>
            <td class="row2">
                <?= $this->newsTags()->tagsForm($id, $this->adminLink('listing&act=tags-suggest'), '726'); ?>
            </td>
        </tr>
        <? if($commentsEnabled): ?>
        <tr>
            <td class="row1 field-title"></td>
            <td class="row2">
                <label class="checkbox">
                    <input name="comments_enabled" type="checkbox" <?= $comments_enabled ? ' checked="checked"' : '' ?> />
                    Комментарии разрешены
                </label>
            </td>
        </tr>
        <? endif; ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'Enabled') ?>:</td>
            <td class="row2">
                <label class="checkbox"><input type="checkbox" id="news-enabled" name="enabled"<? if($enabled){ ?> checked="checked"<? } ?> /></label>
            </td>
        </tr>
    </table>
</div>
<div class="j-tab j-tab-seo hidden">
    <?= SEO::i()->form($this, $aData, 'view'); ?>
</div>
<div style="margin-top: 10px;" class="j-tab j-tab-info j-tab-seo <?= ! in_array($tab, array('info', 'se')) ? 'hidden' : '' ?>">
    <div class="left">
        <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jNewsNewsForm.save(false);" />
        <? if ($edit): ?>
            <input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jNewsNewsForm.save(true);" />
        <? endif; ?>
        <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jNewsNewsFormManager.action('cancel');" />
    </div>
    <div class="right">
        <? if ($edit): ?>
        <input type="button" class="btn button j-massend-submit" value="<?= ($delivered == '0000-00-00 00:00:00' ? _t('', 'Выполнить рассылку') :  _t('', 'Повторить рассылку') ) ?>" onclick="jNewsNewsForm.massend();" />
        <? endif; ?>
    </div>
    <div class="clear"></div>
</div>
</form>
<? if($edit && $commentsEnabled): ?>
    <div class="j-tab j-tab-comments <?= $tab != 'comments' ? 'hidden' : '' ?>"><?= $this->newsComments()->admListing($id); ?></div>
<? endif; ?>
</div>

<script type="text/javascript">
var jNewsNewsForm =
    (function(){
        var $progress, $form, formChk, id = parseInt(<?= $id ?>);
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function(){
            $progress = $('#NewsNewsFormProgress');
            $form = $('#NewsNewsForm');

            bff.datepicker('#news-created', {yearRange: '-3:+3'});

            var $block = $('#NewsNewsFormBlock');
            // tabs
            $form.find('#NewsNewsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                var key = $(this).data('key');
                $block.find('.j-tab').addClass('hidden');
                $block.find('.j-tab-'+key).removeClass('hidden');
                $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
            });
        });
        return {
            del: function()
            {
                if( id > 0 ) {
                    bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                        false, {progress: $progress, repaint: false, onComplete:function(){
                            bff.success('Запись успешно удалена');
                            jNewsNewsFormManager.action('cancel');
                            jNewsNewsList.refresh();
                        }});
                }
            },
            save: function(returnToList)
            {
                if( ! formChk.check(true) ) return;
                bff.ajax(ajaxUrl, $form.serialize(), function(data){
                    if(data && data.success) {
                        bff.success('Данные успешно сохранены');
                        if(returnToList || ! id) {
                            jNewsNewsFormManager.action('cancel');
                            jNewsNewsList.refresh( ! id);
                        }
                    }
                }, $progress);
            },
            onShow: function()
            {
                formChk = new bff.formChecker($form);
            },
            massend: function()
            {
                if (bff.confirm('<?= $delivered == '0000-00-00 00:00:00' ? _t('news','Выполнить рассылку?') : _t('news','Рассылка уже проводилась [date]. Выполнить рассылку еще раз?', array('date' => tpl::date_format2($delivered))) ?>')){
                    bff.ajax(ajaxUrl, {act:'massend', id:<?= $id ?>}, function(data){
                        if(data && data.success) {
                            bff.success('Рассылка была успешно <a href="<?= $this->adminLink('massend_listing','sendmail') ?>">инициирована</a>');
                            $form.find('.j-massend-submit').prop('disabled', true);
                        }
                    }, $progress);
                }
            }
        };
    }());
</script>