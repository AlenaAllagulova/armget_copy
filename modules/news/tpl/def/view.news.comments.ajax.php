<?php
/** @var $this News */
$nUserID = User::id();
$land_you_delete = _t('', 'Вы удалили этот комментарий');
$aHide =  $this->newsComments()->getHideReasons();
foreach($aComments as $v):
    $bDelete = ($nUserID == $v['user_id']) || ($nNewsOwnerID == $nUserID);
    ?>
    <<?= $v['numlevel'] == 1 ? 'li' : 'div' ?> class="media l-comment-block j-comment-block">
        <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="o-freelancer-avatar">
            <?= tpl::userAvatar($v) ?>
        </a>
        <div class="media-body">
            <div class="l-comment-owner">
                <strong><?= tpl::userLink($v) ?></strong>
                <span class="c-base-date"><?= tpl::date_format_pub($v['created'], _t('news', 'd.m.Y в H:i')) ?></span>
            </div>

            <? if($v['deleted']):?>
                <div class="alert alert-deleted">
                    <? switch($v['deleted']){
                        case NewsComments::commentDeletedByItemOwner:
                            if($nNewsOwnerID == $nUserID) echo($land_you_delete.' <a href="#" data-id="'.$v['id'].'">'._t('news', 'Восстановить').'</a>');
                            else echo($aHide[$v['deleted']]);
                            break;
                        case NewsComments::commentDeletedByCommentOwner:
                            if($nUserID == $v['user_id']) echo($land_you_delete.' <a href="#" data-id="'.$v['id'].'" class="j-comment-restore">'._t('news', 'Восстановить').'</a>');
                            else echo($aHide[$v['deleted']]);
                            break;
                        default:
                            echo($aHide[$v['deleted']]);
                    } ?>
                </div>
            <? else: ?>
                <div class="l-comment-text j-comment">
                <div id="comment-text-show">
                    <?= $v['message'] ?>
                    <? if($nUserID): ?>
                    <div class="j-add-title">
                        <a href="#j-add-comment-<?= $v['id'] ?>" data-toggle="collapse" class="ajax-link j-add-comment"><i class="fa fa-comments-o"></i> <span><?= _t('news', 'Ответить'); ?></span></a>
                        <? if($bDelete): ?><a href="#" class="ajax-link pull-right link-delete j-comment-delete" data-id="<?= $v['id'] ?>"><i class="fa fa-times"></i> <span><?= _t('form', 'Удалить'); ?></span></a><? endif; ?>
                        <div class="clearfix"></div>
                    </div>

                    <div class="collapse p-feedback-comment" id="j-add-comment-<?= $v['id'] ?>">
                        <div class="p-add-comment">
                            <form role="form" class="form j-add-comment-form" role="form" method="post" action="">
                                <input type="hidden" name="id" value="<?= $nItemID ?>" />
                                <input type="hidden" name="parent" value="<?= $v['id'] ?>" />
                                <div class="form-group j-required">
                                    <textarea name="message" rows="4" class="form-control" placeholder="<?= _t('news', 'Комментарий'); ?>"></textarea>
                                </div>
                                <button class="btn btn-primary btn-sm j-submit"><?= _t('news', 'Ответить'); ?></button>
                                <a href="#" data-toggle="collapse" class="ajax-link j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                            </form>
                        </div>
                    </div>
                    <? endif; ?>
                </div>
                </div>
            <? endif; ?>
        </div>
        <? if( ! empty($v['sub'])):
            $aSub = array(
                'aComments'    => $v['sub'],
                'nNewsOwnerID' => $nNewsOwnerID,
                'nItemID'      => $nItemID,
            );
            ?>
        <?= $this->commentsList($aSub); ?>
        <? endif; ?>
    </<?= $v['numlevel'] == 1 ? 'li' : 'div' ?>>
<? endforeach;
