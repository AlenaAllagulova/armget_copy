<?php
tpl::includeJS('news.view', false);
?>
    <div class="container">

        <?= tpl::getBreadcrumbs($crumbs); ?>

        <section class="l-mainContent">
            <div class="row">

            <div class="col-md-9" id="j-news-view">
                <div class="a-articleView">

                    <div class="b-date"><?= tpl::dateFormat($created); ?></div>
                    <h1><?= $title ?></h1>
                    <?= $descr_full ?>

                    <div class="row">
                        <div class="col-sm-8">
                            <? foreach($tags as $v): ?>
                            <a href="<?= News::url('tag', array('id' => $v['id'], 'tag' => $v['tag'])) ?>" class="l-tag"><?= $v['tag'] ?></a>
                            <? endforeach; ?>
                        </div>

                        <div class="col-sm-4">
                            <div class="a-social-icons text-right">
                                <? $aConfig = $this->configLoad(); ?>
                                <?= ! empty($aConfig['share_code']) ? $aConfig['share_code'] : '' ?>
                            </div>
                        </div>
                    </div>

                </div>

                <? if(News::commentsEnabled()): ?>
                <a name="comments"></a>
                <div id="j-comments"><?= $this->viewPHP($aData, 'view.news.comments'); ?></div>
                <? endif; ?>


                <div class="l-news-controls">
                    <div class="pull-left">
                        <a href="<?= News::url('search') ?>"><i class="fa fa-angle-left c-link-icon"></i><?= _t('news', 'Назад к новостям'); ?></a>
                    </div>
                </div>

            </div>

            <div class="col-md-3 visible-lg visible-md">
                <? # Баннер: Новости: просмотр ?>
                <?= Banners::view('news_view', array('pos'=>'right')) ?>
            </div>

            </div><!-- /.row -->
        </section><!-- /.l-main-content -->

    </div><!-- /.container -->

<script type="text/javascript">
    <? js::start() ?>
    jNewsView.init(<?= func::php2js(array(
        'lang'   => array(),
        'id'    => $id,
    )) ?>);
    <? js::stop() ?>
</script>