<?php

class M_News_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('news', 'News');
        $module = 'news';
        # Список
        if ($security->haveAccessToModuleToMethod($module, 'items')) {
            $menu->assign($menuTitle, 'Список', $module, 'listing', true, 10,
                array('rlink'=>array('event'=>'listing&act=add') ));
        }

        # Комментарии
        if (News::commentsEnabled() && $security->haveAccessToModuleToMethod($module, 'news-comments')) {
            $menu->assign($menuTitle, 'Комментарии', $module, 'comments_mod', true, 15, array('counter' => 'news_comments_mod'));
            $menu->adminHeaderCounter($menuTitle, 'news_comments_mod', $module, 'comments_mod', 8, '', array('parent'=>'comments'));
        }

        # Теги
        if ($security->haveAccessToModuleToMethod($module, 'tags')) {
            $menu->assign($menuTitle, _t('','Tags'), $module, 'tags', true, 30);
        }

        # Настройки
        if( $security->haveAccessToModuleToMethod($module, 'settings')) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 50);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module,'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 45);
        }

    }
}