<?php

class News_ extends NewsBase
{
    public function init()
    {
        parent::init();

        if (bff::$class == $this->module_name && Request::isGET()) {
            bff::setActiveMenu('//more/news');
        }
    }

    /**
     * Поиск по тегу и просмотр общего списка
     * @return string HTML
     */
    public function search()
    {
        $nPageSize = config::sysAdmin('news.search.pagesize', 10, TYPE_UINT);

        $aFilter = array();
        $aData = array('pgn'=>'', 'tag_id'=>0);

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT,
            'tag'  => TYPE_UINT, # ID тега
        ));
        $aData['f'] = &$f;

        if ($f['tag']) {
            $aData['tag'] = $this->newsTags()->tagData($f['tag']);
            if (empty($aData['tag'])) {
                $this->errors->error404();
            }
            $aFilter[':tag'] = $f['tag'];
            $aData['tag_id'] = $f['tag'];
        } unset($f['tag']);

        # URL
        if ($aData['tag_id']) {
            $url = static::url('tag', array('id'=>$aData['tag']['id'], 'tag'=>$aData['tag']['tag']), true);
        } else {
            $url = static::url('search', array(), true);
        }

        $nCount = $this->model->newsList($aFilter, true);
        if ($nCount) {
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => static::urlDynamic($url),
                'query' => $f,
            ));
            $aData['list'] = $this->model->newsList($aFilter, false, $pgn->getLimitOffset(), 'created DESC');
            foreach ($aData['list'] as &$v) {
                $v['url_view'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword']));
            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        # Теги
        $aData['tags'] = func::array_transparent($this->newsTags()->tagsCloud(), 'id', true);

        $aData['list'] = $this->viewPHP($aData, 'search.list');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'   => $aData['pgn'],
                'list'  => $aData['list'],
            ));
        }

        # SEO:
        $seoData = array('page' => $f['page']);
        if ($aData['tag_id']) {
            # Список по тегу
            $this->urlCorrection(static::urlDynamic($url));
            $this->seo()->canonicalUrl($url, $seoData);
            $this->setMeta('listing-tag', array('tag' => $aData['tag']['tag'], 'page' => $f['page']));
        } else {
            # Список
            $this->urlCorrection(static::urlDynamic($url));
            $this->seo()->canonicalUrl($url, $seoData);
            $this->setMeta('listing', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        $aData['form'] = $this->viewPHP($aData, 'search.form');
        return $this->viewPHP($aData, 'search');
    }

    /**
     * Просмотр новости
     * @return string HTML
     */
    public function view()
    {
        $nNewsID = $this->input->get('id', TYPE_UINT);
        if ( ! $nNewsID) {
            $this->errors->error404();
        }
        $aData = $this->model->newsData($nNewsID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        # Теги
        $aData['tags'] = $this->newsTags()->tagsGet($nNewsID);

        # Хлебные крошки
        $aData['crumbs'] = array(
            array('title' => _t('news', 'News'), 'link' => static::url('search')),
            array('title' => $aData['title'], 'active' => true),
        );

        # SEO: Просмотр новости
        $url = static::url('view', array('id'=>$nNewsID, 'keyword'=>$aData['keyword']), true);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $seoTags = array(); foreach ($aData['tags'] as $v) $seoTags[] = $v['tag'];
        $this->setMeta('view', array(
            'title'      => tpl::truncate($aData['title'], config::sysAdmin('news.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full' => $aData['title'],
            'description'=> tpl::truncate(trim(strip_tags($aData['descr_short']), '.,'), config::sysAdmin('news.view.meta.description.truncate', 150, TYPE_UINT)),
            'tags'       => join(', ', $seoTags),
        ), $aData);
        # SEO: Open Graph
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], array(), $url, $aData['share_sitename']);

        if (static::commentsEnabled() && $aData['comments_enabled']) {
            $aData['comments'] = $this->newsComments()->commentsData($nNewsID, 0 ,true);
            $aData['comments']['nNewsOwnerID'] = static::OWNER_ID;
            $aData['comments']['nItemID'] = $nNewsID;
            $aData['comments_list'] = $this->commentsList($aData['comments']);
        }

        return $this->viewPHP($aData, 'view');
    }

    /**
     * Данные для карты сайта
     * @return array
     */
    public function getSitemapData()
    {
        $aData = $this->newsTags()->tagsCloud();
        foreach ($aData as &$v) {
            $v['url'] = static::url('tag', array('tag' => $v['tag'], 'id' => $v['id']));
        } unset($v);

        return array(
            'tags' => $aData,
            'link' => static::url('search'),
        );
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR))
        {
            case 'comment-add': # комментарии: добавление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $sMessage = $this->input->post('message', TYPE_TEXT, array('len'=>2500, 'len.sys' => 'news.comment.message.limit'));
                $min = config::sys('news.comment.message.min', 10, TYPE_UINT);
                if (mb_strlen($sMessage) < $min) {
                    $this->errors->set(_t('', 'Комментарий не может быть короче [min] символов', array('min' => $min)), 'message');
                    break;
                }

                # антиспам фильтр
                if(Site::i()->spamFilter(array(
                    array('text' => & $sMessage,   'error'=>_t('', 'В указанном вами комментарии присутствует запрещенное слово "[word]"')),
                ))){
                    break;
                }

                $nNewsID = $this->input->post('id', TYPE_UINT);
                if ( ! $nNewsID) {
                    $this->errors->reloadPage(); break;
                }

                $aNewsData = $this->model->newsData($nNewsID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aNewsData)){
                    $this->errors->reloadPage();
                }

                if ( ! $aNewsData['enabled']){
                    $this->errors->impossible(); break;
                }
                if ( ! $aNewsData['comments_enabled']){
                    $this->errors->set(_t('news', 'Добавление комментариев запрещено'), 'message');
                    break;
                }

                # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                if (Site::i()->preventSpam('news-comment', config::sysAdmin('news.prevent.spam', 10, TYPE_UINT))) {
                    break;
                }

                $nParent = $this->input->post('parent', TYPE_UINT);

                $aData = array(
                    'message' => $sMessage,
                    'name' => User::data('name'),
                );

                $nCommentID = $this->newsComments()->commentInsert($nNewsID, $aData, $nParent);
                if ($nCommentID) {
                    # оставили комментарий
                    $aCommentsData = array(
                        'aComments' => array(array(
                            'created'  => $this->db->now(),
                            'message'  => nl2br($aData['message']),
                            'name'     => User::data('name'),
                            'surname'  => User::data('surname'),
                            'pro'      => User::data('pro'),
                            'login'    => User::data('login'),
                            'sex'      => User::data('sex'),
                            'avatar'   => User::data('avatar'),
                            'last_activity' => User::data('last_activity'),
                            'deleted'  => 0,
                            'numlevel' => $nParent ? 2 : 1,
                            'user_id'  => User::id(),
                            'id'       => $nCommentID,
                        )),
                        'nNewsOwnerID' => static::OWNER_ID,
                        'nItemID'      => $nNewsID,
                    );
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                }

            } break;
            case 'comment-delete': # комментарии: удаление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $nCommentID = $this->input->post('id', TYPE_UINT);
                if ( ! $nCommentID) {
                    $this->errors->reloadPage();
                    break;
                }

                $nNewsID = $this->input->post('post_id', TYPE_UINT);
                if ( ! $nNewsID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aNewsData = $this->model->newsData($nNewsID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aNewsData)){
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! $aNewsData['enabled']){
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! $aNewsData['comments_enabled']){
                    $this->errors->reloadPage();
                    break;
                }

                $oComments = $this->newsComments();
                $aCommentData = $oComments->commentData($nNewsID, $nCommentID);
                if(empty($aCommentData)){
                    $this->errors->reloadPage();
                    break;
                }
                $nUserID = User::id();
                if ($aCommentData['user_id'] == $nUserID) { # владелец комментария
                    $oComments->commentDelete($nNewsID, $nCommentID, NewsComments::commentDeletedByCommentOwner);
                } else {
                    $this->errors->reloadPage();
                    break;
                }

                $aCommentsData = $oComments->commentsData($nNewsID, 0, true, $nCommentID);
                if( ! empty($aCommentsData)){
                    $aCommentsData['nNewsOwnerID'] = static::OWNER_ID;
                    $aCommentsData['nItemID'] = $nNewsID;
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                } else {
                    $aResponse['html'] = '';
                }
            } break;
            case 'comment-restore': # комментарии: восстановление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $nCommentID = $this->input->post('id', TYPE_UINT);
                if ( ! $nCommentID) {
                    $this->errors->reloadPage();
                    break;
                }

                $nNewsID = $this->input->post('post_id', TYPE_UINT);
                if ( ! $nNewsID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aNewsData = $this->model->newsData($nNewsID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aNewsData)){
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! $aNewsData['enabled']){
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! $aNewsData['comments_enabled']) {
                    $this->errors->reloadPage();
                    break;
                }

                $oComments = $this->newsComments();
                $aCommentData = $oComments->commentData($nNewsID, $nCommentID);
                if (empty($aCommentData)) {
                    $this->errors->reloadPage();
                    break;
                }
                $nUserID = User::id();
                switch($aCommentData['deleted']){
                    case NewsComments::commentDeletedByCommentOwner:
                        if ($aCommentData['user_id'] == $nUserID) {
                            $oComments->restore($nCommentID);
                        } else {
                            $this->errors->reloadPage();
                            break 2;
                        } break;
                    default:
                        $this->errors->reloadPage();
                        break 2;
                }

                $aCommentsData = $oComments->commentsData($nNewsID, 0, true, $nCommentID);
                if ( ! empty($aCommentsData)) {
                    $aCommentsData['nNewsOwnerID'] = static::OWNER_ID;
                    $aCommentsData['nItemID'] = $nNewsID;
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                } else {
                    $aResponse['html'] = '';
                }

            } break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Вывод списка комментариев, рекурсивно
     * @param array $aData
     * @return string
     */
    public function commentsList($aData)
    {
        return $this->viewPHP($aData, 'view.news.comments.ajax');
    }


}