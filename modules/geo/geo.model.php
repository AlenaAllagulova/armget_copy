<?php

class GeoModel_ extends GeoModelBase
{
    /**
     * Формируем список станций метро для автокомплитера
     * @param array $aFilter фильтр
     * @param int $nLimit
     * @param string $sOrderBy
     * @return mixed
     */
    public function metroAcList(array $aFilter, $nLimit = 0, $sOrderBy = '')
    {
        $aFilter = $this->prepareFilter($aFilter, 'M');

        return $this->db->select_key('SELECT M.*, M.title_' . LNG . ' as title
              FROM ' . TABLE_REGIONS_METRO . ' M
              ' . $aFilter['where'] . '
              ORDER BY ' . (!empty($sOrderBy) ? $sOrderBy : 'R.num') . '
              ' . (!empty($nLimit) ? $this->db->prepareLimit(0, $nLimit) : ''), 'id', $aFilter['bind']
        );
    }

}