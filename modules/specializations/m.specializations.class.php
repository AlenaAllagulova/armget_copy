<?php

class M_Specializations_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $module = 'specializations';

        $menuTitle = 'Специализации';
        # Категории
        if (Specializations::catsOn()) {
            if ($security->haveAccessToModuleToMethod($module, 'categories')) {
                $menu->assign($menuTitle, 'Категории', $module, 'categories_list', true, 6,
                    array('rlink' => array('event' => 'categories_list&act=add'))
                );
            }
        }

        # Специализации
        if ($security->haveAccessToModuleToMethod($module, 'specs')) {
            $menu->assign($menuTitle, 'Специализации', $module, 'specializations_list', true, 10,
                array('rlink' => array('event' => 'specializations_list&act=add'))
            );

            # Дин. свойства
            $menu->assign($menuTitle, 'Дин. св-ва специализаций', $module, 'dynprops_listing', false, 11);
            $menu->assign($menuTitle, 'Дин. св-ва специализаций', $module, 'dynprops_action', false, 12);
        }
        # Настройки
        if ($security->haveAccessToModuleToMethod($module,'settings') && Specializations::useServices()) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 20);
        }
    }
}