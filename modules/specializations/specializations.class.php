<?php

class Specializations_ extends SpecializationsBase
{
    /**
     * Выбор специализации
     * @param int $nIndex
     * @param array $aData данные о специализиации
     * @param array $aParam:
     *   inputName     - имя тега input
     *   allowDelete   - возможность удаления блока
     *   noCatOnSelect - не выводить названия категорий при выборе раздела
     *   emptyTitle    - заголовок пустого селектора
     * @return string
     */
    public function specSelect($nIndex = 0, array $aData = array(), $aParam = array())
    {
        $aData = $aData + $aParam;
        $aData['index'] = $nIndex;
        if (static::catsOn()) {
            if (empty($aData['cat_id'])) {
                $nRootCatID = $this->model()->categoryRootNodeID();
                $aData['cat_id'] = 0;
            } else {
                $nRootCatID = $aData['cat_id'];
            }
            $aData['aCats'] = $this->model()->categorySubOptions($nRootCatID, false, array('enabled' => 1));
        } else {
            $aData['cat_id'] = 0;
        }
        $aData['aSpecs'] = $this->model()->specializationsInCategory($aData['cat_id'], array('S.id, SL.title'), array(
                'enabled' => 1,
                'all'     => static::catsOn() ? 0 : 1
            )
        );
        if (empty($aData['inputName'])) {
            $aData['inputName'] = 'spec';
        }
        $aData['disabled'] = ( isset($aData['disabled']) ? intval($aData['disabled']) : 0 );

        return $this->viewPHP($aData, 'form.spec');
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            /**
             * Список под категорий или специализаций для категории
             * @param int 'cat' ID категории
             */
            case 'form-menu':
            {
                $aData = array();
                if (static::catsOn()) {
                    $nCatID = $this->input->postget('cat', TYPE_UINT);
                    $aData = $this->model()->categoryData($nCatID);
                    $aData['cat_title'] = $aData['title'];
                    $aData['cat_id'] = $aData['id'];
                    $aData['aCats'] = $this->model()->categorySubOptions($nCatID, false, array('enabled' => 1));
                } else {
                    $nCatID = 0;
                }
                $aData['aSpecs'] = $this->model()->specializationsInCategory($nCatID, array('S.id, SL.title'), array(
                        'enabled' => 1,
                        'all'     => static::catsOn() ? 0 : 1
                    )
                );
                $aResponse['menu'] = $this->viewPHP($aData, 'form.specs.ajax');
            }
            break;
            /**
             * Добавить новую (настройки профиля)
             */
            case 'add':
            {
                $nCount = $this->input->postget('cnt', TYPE_UINT);
                $aResponse['html'] = $this->specSelect($nCount, array(
                        'cat_id' => 0,
                        'spec_id' => 0
                    ), array(
                        'allowDelete' => 1,
                        'rootCat'     => 1
                    )
                );
            }
            break;
            /**
             * Данные о настройках цены
             */
            case 'price-sett':
            {
                $nSpecID = $this->input->postget('spec_id', TYPE_UINT);
                $aResponse['price_sett'] = $this->aPriceSett($nSpecID);
            }
            break;
            case 'spec-search':
            {
                $q = $this->input->postget('q', TYPE_STR);
                $data['q'] = $q;
                $data['list'] = $this->model->specializationInCatsSearch($q);
                $aResponse['html'] = $this->viewPHP($data, 'form.specs.search');
            }
            break;

            default:
                $this->errors->impossible();
        }

        $this->ajaxResponseForm($aResponse);
    }

}