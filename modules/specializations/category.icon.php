<?php

class CategoryIcon extends CImageUploader
{
    # варианты иконок
    const SMALL = 's'; # малая

    # ключи размеров
    const szOriginal = 'o'; # оригинал icon
    const szList     = 'l'; # list - cписок поиска


    private static $instance = null;

    public static function instance($nCategoryID)
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }
        self::$instance->setRecordID($nCategoryID);

        return self::$instance;
    }

    function initSettings()
    {
        $this->path = bff::path('cats', 'images');
        $this->pathTmp = bff::path('tmp', 'images');
        $this->url = bff::url('cats', 'images');
        $this->urlTmp = bff::url('tmp', 'images');

        $this->table = TABLE_SPECIALIZATIONS_CATS;
        $this->fieldID = 'id';
        $this->filenameLetters = 4;
        $aVariants = $this->getVariants();
        if (!empty($aVariants)) {
            $this->setVariant(key($aVariants));
        }
    }

    function url($nSvcID, $sFilename, $sVariantKey = self::szOriginal)
    {
        $this->setRecordID($nSvcID);
        if (empty($sFilename)) {
            # иконка-заглушка
            return $this->urlDefault();
        } else {
            return $this->getURL($sFilename, $sVariantKey);
        }

    }

    public function urlDefault()
    {
        return $this->url . 'logo.png';
    }

    function getVariants()
    {
        return array(
            'icon_' . self::SMALL => array(
                'title' => _t('cat-icon', 'Иконка категории'),
                'key'   => self::SMALL,
                'sizes' => array(
                    self::szList   => [
                        'width'    => 425,
                        'height'   => 425,
                        'o'        => true,
                    ],
                    self::SMALL => [
                        'width'      => 75,
                        'height'     => 75,
                        'o'          => true
                    ],
                ),
            ),
        );
    }

    function setVariant($sKey)
    {
        $aVariants = $this->getVariants();
        if (isset($aVariants[$sKey])) {
            $this->fieldImage = $sKey;
            $this->sizes = $aVariants[$sKey]['sizes'];
        }
    }
}