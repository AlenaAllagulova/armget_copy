<?php
$bRotate = true;
?>
<?= tplAdmin::blockStart('Добавление категории', false, array('id'=>'SpecializationsCategoriesFormBlock','style'=>'display:none;')); ?>
<div id="SpecializationsCategoriesFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart('Список категорий', true, array('id'=>'SpecializationsCategoriesListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>'+ добавить категорию','class'=>'ajax','onclick'=>'return jSpecializationsCategoriesFormManager.action(\'add\',0);'),
    array(
        array('title'=>'валидация nested-sets', 'onclick'=>"return bff.confirm('Длительная операция, продолжить?', {r:'".$this->adminLink(bff::$event.'&act=dev-treevalidate')."'})", 'icon'=>'icon-indent-left', 'debug-only'=>true),
    )); ?>
<div class="actionBar" style="min-height: 20px;">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="SpecializationsCategoriesListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <div class="left"></div>
        <div class="right">
            <div id="SpecializationsCategoriesProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="SpecializationsCategoriesListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th class="left"><?= _t('', 'Title') ?></th>
        <th width="135"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="SpecializationsCategoriesList">
    <?= $list ?>
    </tbody>
</table>
<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">
        <? if($bRotate){ ?>&darr; &uarr;<? } ?>
    </div>
    <br />
</div>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
    var jSpecializationsCategoriesFormManager = (function(){
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function(){
            $formContainer = $('#SpecializationsCategoriesFormContainer');
            $progress = $('#SpecializationsCategoriesProgress');
            $block = $('#SpecializationsCategoriesFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if(!empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
        });

        function onFormToggle(visible)
        {
            if(visible) {
                jSpecializationsCategoriesList.toggle(false);
                if(jSpecializationsCategoriesForm) jSpecializationsCategoriesForm.onShow();
            } else {
                jSpecializationsCategoriesList.toggle(true);
            }
        }

        function initForm(type, id, params)
        {
            if( process ) return;
            bff.ajax(ajaxUrl,params,function(data){
                if(data && (data.success || intval(params.save)===1)) {
                    $blockCaption.html((type == 'add' ? 'Добавление категории' : 'Редактирование категории'));
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo( $blockCaption, {duration:500, offset:-300});
                    onFormToggle(true);
                    if(bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                    }
                } else {
                    jSpecializationsCategoriesList.toggle(true);
                }
            }, function(p){ process = p; $progress.toggle(); });
        }

        function action(type, id, params)
        {
            params = $.extend(params || {}, {act:type});
            switch(type) {
                case 'add':
                {
                    if( id > 0 ) return action('edit', id, params);
                    if($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                } break;
                case 'cancel':
                {
                    $block.hide();
                    onFormToggle(false);
                } break;
                case 'edit':
                {
                    if( ! (id || 0) ) return action('add', 0, params);
                    params.id = id;
                    initForm(type, id, params);
                } break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jSpecializationsCategoriesList =
        (function()
        {
            var $progress, $block, $list, $listTable, $listPgn, filters, processing = false;
            var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';

            $(function(){
                $progress  = $('#SpecializationsCategoriesProgress');
                $block     = $('#SpecializationsCategoriesListBlock');
                $list      = $block.find('#SpecializationsCategoriesList');
                $listTable = $block.find('#SpecializationsCategoriesListTable');
                $listPgn   = $block.find('#SpecializationsCategoriesListPgn');
                filters    = $block.find('#SpecializationsCategoriesListFilters').get(0);

                $list.delegate('a.category-edit', 'click', function(){
                    var id = intval($(this).attr('rel'));
                    if(id>0) jSpecializationsCategoriesFormManager.action('edit',id);
                    return false;
                });

                $list.delegate('a.category-toggle', 'click', function(){
                    var id = intval($(this).attr('rel'));
                    var type = $(this).data('type');
                    if(id>0) {
                        var params = {progress: $progress, link: this};
                        bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                    }
                    return false;
                });

                $list.delegate('a.category-del', 'click', function(){
                    var id = intval($(this).attr('rel'));
                    if(id>0) del(id, this);
                    return false;
                });

                $list.delegate('a.category-expand', 'click', function(){
                    var id = intval($(this).attr('rel'));
                    if(id>0) {
                        bff.expandNS(id, ajaxUrl+'expand&id=', {cookie:app.cookiePrefix+'Categories_categories_expand', progress:$progress});
                    }
                    return false;
                });

                $list.on('click', '.j-specs', function(){
                    var $tr = $(this).closest('tr');
                    $tr.toggleClass('jspecs');
                    checkCategoriesDND();
                    if($tr.hasClass('jspecs')){
                        var id = intval($(this).data('id'));
                        if( ! id) return false;
                        bff.ajax('<?= $this->adminLink('specializations_list'); ?>', {cat:id, noActions:1}, function(data){
                            if(data.list){
                                $tr.after('<tr class="row0 jspecs-content">'+
                                    '<td colspan="2" class="left" style="padding: 0;">'+
                                    '<table id="jspecs-content-'+id+'" class="table table-condensed table-hover admtbl tblhover" style="margin: -1px 0px 0px;"><tbody>'+
                                    data.list +
                                    '</tbody></table>'+
                                    '</td></tr>');
                                bff.rotateTable('#jspecs-content-' + id, '<?= $this->adminLink('specializations_list'); ?>&act=rotate', $progress, false, {cat:id});
                            }
                        });
                    } else {
                        $tr.next('.jspecs-content').remove();
                    }
                    return false;
                });

                $(window).bind('popstate',function(){
                    if('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                    if( actForm!=null ) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jSpecializationsCategoriesFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jSpecializationsCategoriesFormManager.action('cancel');
                        updateList(false);
                    }
                });

                <? if($bRotate) { ?>bff.rotateTable($list, ajaxUrl+'rotate', $progress);<? } ?>
            });

            function checkCategoriesDND()
            {
                var cnt = $list.find('tr.jspecs').length;
                if(cnt){
                    $list.find(' > tr').addClass('nodrag nodrop');
                } else {
                    $list.find('tr.jspecs-content').remove();
                    $list.find('tr').removeClass('nodrag nodrop');
                    $list.tableDnDUpdate();
                }
            }

            function isProcessing()
            {
                return processing;
            }

            function del(id, link)
            {
                bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
                return false;
            }

            function updateList(updateUrl)
            {
                if(isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function(data){
                    if(data) {
                        $list.html( data.list );
                        $listPgn.html( data.pgn );
                        if(updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                        <? if($bRotate) { ?>$list.tableDnDUpdate();<? } ?>
                    }
                }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
            }

            function setPage(id)
            {
                // нет постраничности
            }

            return {
                submit: function(resetForm)
                {
                    if(isProcessing()) return false;
                    setPage(1);
                    if(resetForm) {
                        //
                    }
                    updateList();
                },
                page: function (id)
                {
                    if(isProcessing()) return false;
                    setPage(id);
                    updateList();
                },
                refresh: function(resetPage,updateUrl)
                {
                    if(resetPage) setPage(0);
                    updateList(updateUrl);
                },
                toggle: function(show)
                {
                    if(show === true) {
                        $block.show();
                        if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                }
            };
        }());

</script>