<?php
/**
 * @var Specializations $this
 */
$event = 'specialization_services_list';
$aData = HTML::escape($aData, 'html', array('keyword'));
$edit = !empty($id);
$aTabs = array(
    'info' => 'Основные',
    'seo' => 'SEO',
);
$priceUsers = Specializations::useServices(Specializations::SERVICES_PRICE_USERS);
?>
<form name="SpecializationServiceForm" id="SpecializationServiceForm" action="<?= $this->adminLink($event) ?>" method="post" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <input type="hidden" name="spec_id" value="<?= $spec_id ?>" />
    <? if (sizeof($aTabs) > 1) { ?>
        <div class="tabsBar" id="SpecializationServiceFormTabs">
            <? foreach($aTabs as $k=>$v) { ?>
                <span class="tab<? if($k == 'info') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
            <? } ?>
        </div>
    <? } ?>
    <!-- таб: Основные -->
    <div class="j-tab-service j-tab-service-info">
        <table class="admtbl tbledit">
            <?= $this->locale->buildForm($aData, 'specialization-service-title','
            <tr class="required">
                <td class="row1 field-title" width="100"><?= _t(\'\', \'Title\') ?><span class="required-mark">*</span>:</td>
                <td class="row2">
                    <input class="stretch <?= $key ?>" type="text" id="service-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
                </td>
            </tr>
        '); ?>
            <tr>
                <td class="row1 field-title"><?= _t('', 'Price') ?>:</td>
                <td class="row2">
                    <div class="left">
                        <input type="text" name="price" value="<?= $price ?>" style="width:70px;"/>
                        <select name="price_curr" style="width:70px;"><?= Site::currencyOptions($price_curr); ?></select>
                    </div>
                    <? if($priceUsers): ?>
                    <div class="left" style="margin: 4px 0 0 6px;">
                        <label class="checkbox">
                            <input type="checkbox" name="price_free" value="1" <?= $price_free ? 'checked="checked"' : '' ?> />
                            бесплатно <span class="desc">(в случае если пользователь не указал цену)</span>
                        </label>
                    </div>
                    <? endif; ?>
                </td>
            </tr>
            <?= $this->locale->buildForm($aData, 'specialization-service-measure','
            <tr>
                <td class="row1 field-title"><?= _t(\'\', \'Measure\') ?>:</td>
                <td class="row2">
                    <input class="stretch <?= $key ?>" type="text" name="measure[<?= $key ?>]" value="<?= HTML::escape($aData[\'measure\'][$key]); ?>" />
                </td>
            </tr>
        '); ?>
            <tr>
                <td class="row1 field-title">URL-Keyword:<br /><a href="#" onclick="return bff.generateKeyword('#service-title-<?= LNG ?>', '#service-keyword');" class="ajax desc small"><?= _t('', 'generate') ?></a></td>
                <td class="row2">
                    <input class="stretch" type="text" id="service-keyword" name="keyword" value="<?= $keyword ?>" />
                </td>
            </tr>
            <?= $this->locale->buildForm($aData, 'specialization-service-description','
            <tr>
                <td class="row1 field-title"><?= _t(\'\', \'Description\') ?>:</td>
                <td class="row2">
                    <textarea class="stretch <?= $key ?>" name="description[<?= $key ?>]" rows="6"><?= HTML::escape($aData[\'description\'][$key]); ?></textarea>
                </td>
            </tr>
        '); ?>
        </table>
    </div>
    <!-- таб: SEO: Исполнители -->
    <div class="j-tab-service j-tab-service-seo hidden">
        <?= SEO::i()->form(Users::i(), $aData, 'price-service'); ?>
    </div>
    <div style="margin-top: 10px;">
        <input type="submit" class="btn btn-success btn-small button submit" value="<?= _t('', 'Save') ?>" onclick="jSpecializationServiceForm.save(false);" />
        <? if($edit) { ?><input type="button" class="btn btn-success btn-small button submit" value="<?= _t('', 'Save and back') ?>" onclick="jSpecializationServiceForm.save(true);" /><? } ?>
        <? if($edit) { ?><input type="button" onclick="jSpecializationServiceForm.del(); return false;" class="btn btn-danger btn-small button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
        <input type="button" class="btn button btn-small cancel" value="<?= _t('', 'Cancel') ?>" onclick="jSpecializationServicesFormManager.action('cancel');" />
    </div>
</form>

<script type="text/javascript">
    var jSpecializationServiceForm =
        (function(){
            var $progress, $form, formChk, id = <?= $id ?>;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#SpecializationServiceFormProgress');
                $form = $('#SpecializationServiceForm');

                // tabs
                $form.find('#SpecializationServiceFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab-service').addClass('hidden');
                    $form.find('.j-tab-service-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });
            });

            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('Запись успешно удалена');
                                jSpecializationServicesFormManager.action('cancel');
                                jSpecializationServicesList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    if( ! formChk.check(true) ) return;
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                            if(returnToList || ! id) {
                                jSpecializationServicesFormManager.action('cancel');
                                jSpecializationServicesList.refresh( ! id);
                            }
                        }
                    }, $progress);
                },
                onShow: function ()
                {
                    formChk = new bff.formChecker($form);
                }
            };
        }());
</script>