<?php
if(empty($services)) return;
$prefix = 'specsServices['.$nSpecID.']';
?>
<div class="p-additional-info j-spec-services">
    <a href="#j-services-settings-<?= $nSpecID ?>" data-toggle="collapse" class="ajax-link"><i class="fa fa-cog"></i> <span><?= _t('specs', 'Настройки услуг'); ?></span></a>

    <div class="collapse" id="j-services-settings-<?= $nSpecID ?>">
        <? foreach($services as $v): $v['prefix'] = $prefix.'['.$v['id'].']' ?>
        <div class="p-additional-info-service">
            <div class="row j-spec-service">
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="<?= $v['prefix'] ?>[id]" value="1"<?= ! empty($user[ $v['id'] ]) ? ' checked="checked"' : '' ?>> <?= $v['title'] ?>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <? if(true): ?>
                    <div class="p-additional-info-service-form-v2">
                        <div class="p-additional-info-service-form-input">
                            <input type="text" name="<?= $v['prefix'] ?>[price]" class="form-control j-spec-service-ch" value="<?= isset($user[ $v['id'] ]['price']) ? $user[ $v['id'] ]['price'] : $v['price'] ?>"  placeholder="<?= _t('', 'Цена'); ?>" />
                        </div>
                        <div class=" input-group p-additional-info-service-form-select">
                            <select class="form-control j-spec-service-ch" name="<?= $v['prefix'] ?>[price_curr]">
                                <?= Site::currencyOptions( ! empty($user[ $v['id'] ]['price_curr']) ? $user[ $v['id'] ]['price_curr'] : ( ! empty($v['price_curr']) ? $v['price_curr'] : Site::currencyDefault('id'))) ?>
                            </select>
                        </div>
                    </div>
                    <small><?= $v['measure'] ?></small>
                    <? else: ?>
                    <div class="p-additional-info-service-form">
                        <div class="p-additional-info-service-form-input">
                            <input type="text" name="<?= $v['prefix'] ?>[price]" class="form-control j-spec-service-ch" value="<?= ! empty($user[ $v['id'] ]['price']) ? $user[ $v['id'] ]['price'] : $v['price'] ?>"  placeholder="<?= _t('', 'Цена'); ?>" />
                        </div>
                        <div class=" input-group p-additional-info-service-form-select">
                            <select class="form-control j-spec-service-ch" name="<?= $v['prefix'] ?>[price_curr]">
                                <?= Site::currencyOptions( ! empty($user[ $v['id'] ]['price_curr']) ? $user[ $v['id'] ]['price_curr'] : ( ! empty($v['price_curr']) ? $v['price_curr'] : Site::currencyDefault('id'))) ?>
                            </select>
                            <span class="input-group-addon"> / <?= $v['measure'] ?></span>
                        </div>
                    </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <? endforeach; ?>
    </div>
</div>