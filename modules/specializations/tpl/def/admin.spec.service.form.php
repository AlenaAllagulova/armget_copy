<?php
    if(empty($services)) return;
    $prefix = 'specsServices['.$nSpecID.']';
?>
<tr>
    <td class="row1 field-title">Услуги:</td>
    <td>
        <table>
    <? foreach($services as $v): $v['prefix'] = $prefix.'['.$v['id'].']' ?>
        <tr>
            <td>
                <label class="checkbox" style="margin-right: 5px;">
                    <input type="checkbox" name="<?= $v['prefix'] ?>[id]" value="1"<?= ! empty($user[ $v['id'] ]) ? ' checked="checked"' : '' ?>/>
                    <?= $v['title'] ?>
                </label>
            </td>
            <td><input type="text" name="<?= $v['prefix'] ?>[price]" value="<?= ! empty($user[ $v['id'] ]['price']) ? $user[ $v['id'] ]['price'] : $v['price'] ?>" style="width: 65px;" /></td>
            <td><select name="<?= $v['prefix'] ?>[price_curr]" style="width: 55px;"><?= Site::currencyOptions( ! empty($user[ $v['id'] ]['price_curr']) ? $user[ $v['id'] ]['price_curr'] : ( ! empty($v['price_curr']) ? $v['price_curr'] : Site::currencyDefault('id'))) ?></select></td>
            <td> / <?= $v['measure'] ?></td>
        </tr>
    <? endforeach; ?>
        </table>
    </td>
</tr>