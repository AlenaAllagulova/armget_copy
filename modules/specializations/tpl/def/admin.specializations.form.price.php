<?php
$aLangs = $this->locale->getLanguages();
$bLangsMore = sizeof($aLangs) > 1;
$price_curr = ( ! empty($price_sett['curr']) ? $price_sett['curr'] : Site::currencyDefault('id') );
?>
<div class="well well-small j-spec-price">
    <table class="admtbl tbledit<? if($bLangsMore) { ?> more-langs<? } ?>">
        <?php echo $this->locale->buildForm($aData, 'categories-item', '
                        <tr>
                            <td class="row1 field-title span3">Заголовок цены</td>
                            <td class="row2">
                                <input type="text" class="<?= $key ?>" name="price_title[<?= $key ?>]" value="<?= HTML::escape($aData[\'price_title\'][$key]); ?>" placeholder="Цена" maxlength="50" style="width: 225px;" />
                            </td>
                        </tr>
                        ');?>
        <tr>
            <td class="row1" width="140">Валюта по-умолчанию:</td>
            <td id="j-price-curr">
                <select name="price_sett[curr]" style="width:70px;"><?= Site::currencyOptions($price_curr); ?></select>
            </td>
        </tr>
        <tr>
            <td class="row1">Уточнение к стоимости:</td>
            <td>
                <table id="j-price-rates"></table>
                <a href="#" class="ajax" id="j-price-rates-add" onclick="jSpecFormPrice.addRate(); return false;">добавить</a><span class="desc">&nbsp;&nbsp;&uarr;&darr;</span>
            </td>
        </tr>
        <tr>
            <td class="row1">Модификатор:</td>
            <td>
                <label class="checkbox inline"><input type="checkbox" name="price_sett[ex][1]" value="<?= Specializations::PRICE_EX_AGREE ?>" <? if($price_sett['ex'] & Specializations::PRICE_EX_AGREE) { ?> checked="checked" <? } ?> /></label>
                <?= $this->locale->formField('price_title_mod', $price_title_mod, 'text', array('maxlength' => 50, 'placeholder'=>'По договоренности')); ?>
            </td>
        </tr>
    </table>
</div>