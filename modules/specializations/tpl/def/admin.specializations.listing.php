<?php
$bRotate = true;
?>
<?= tplAdmin::blockStart('Добавление специализации', false, array('id'=>'SpecializationsFormBlock','style'=>'display:none;')); ?>
<div id="SpecializationsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart('Список специализаций', true, array('id'=>'SpecializationsListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>'+ добавить специализацию','class'=>'ajax','onclick'=>'return jSpecializationsFormManager.action(\'add\',0);'),
    array(
        array('title'=>'Пересчитать кэш услуг', 'onclick'=>"return bff.confirm('Длительная операция, продолжить?', {r:'".$this->adminLink(bff::$event.'&act=dev-services-cache')."'})", 'icon'=>'icon-indent-left'),
    )); ?>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="SpecializationsListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <div class="left">
            <? if(Specializations::catsOn()):?><select name="cat" onchange="jSpecializationsList.onCat(this);"><?= $this->model->categoriesOptions($f['cat'], array('Все категории')) ?></select><? endif; ?>
        </div>
        <div class="right">
            <div id="SpecializationsProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="SpecializationsListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th class="left"><?= _t('', 'Title') ?></th>
        <th width="140"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="SpecializationsList">
    <?= $list ?>
    </tbody>
</table>
<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">
        <? if($bRotate){ ?>&darr; &uarr;<? } ?>
    </div>
    <br />
</div>

<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
var jSpecializationsFormManager = (function(){
    var $progress, $block, $blockCaption, $formContainer, process = false;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $formContainer = $('#SpecializationsFormContainer');
        $progress = $('#SpecializationsProgress');
        $block = $('#SpecializationsFormBlock');
        $blockCaption = $block.find('span.caption');

        <? if(!empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jSpecializationsList.toggle(false);
            if(jSpecializationsForm) jSpecializationsForm.onShow();
        } else {
            jSpecializationsList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? 'Добавление специализации' : 'Редактирование специализации'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
            } else {
                jSpecializationsList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        params = $.extend(params || {}, {act:type});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jSpecializationsList =
    (function()
    {
        var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, cat = <?= $f['cat'] ?>;
        var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';

        $(function(){
            $progress  = $('#SpecializationsProgress');
            $block     = $('#SpecializationsListBlock');
            $list      = $block.find('#SpecializationsList');
            $listTable = $block.find('#SpecializationsListTable');
            $listPgn   = $block.find('#SpecializationsListPgn');
            filters    = $block.find('#SpecializationsListFilters').get(0);

            $list.delegate('a.specialization-edit', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) jSpecializationsFormManager.action('edit',id);
                return false;
            });

            $list.delegate('a.specialization-toggle', 'click', function(){
                var id = intval($(this).attr('rel'));
                var type = $(this).data('type');
                if(id>0) {
                    var params = {progress: $progress, link: this};
                    bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                }
                return false;
            });

            $list.delegate('a.specialization-del', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) del(id, this);
                return false;
            });

            $list.delegate('a.specialization-expand', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) {
                    bff.expandNS(id, ajaxUrl+'expand&id=', {cookie:app.cookiePrefix+'Categories_specializations_expand', progress:$progress});
                }
                return false;
            });

            $list.delegate('a.specialization-dynprops', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) {
                    bff.redirect('<?= $this->adminLink('dynprops_listing&owner='); ?>'+id);
                }
                return false;
            });

            $list.delegate('a.specialization-dynprops-orders', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) {
                    bff.redirect('<?= $this->adminLink('dporders_listing&owner='); ?>'+id);
                }
                return false;
            });

            $(window).bind('popstate',function(){
                if('state' in window.history && window.history.state === null) return;
                var loc = document.location;
                var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                if( actForm!=null ) {
                    var actId = /id=([\d]+)/.exec(loc.search.toString());
                    jSpecializationsFormManager.action(actForm[1], actId && actId[1]);
                } else {
                    jSpecializationsFormManager.action('cancel');
                    updateList(false);
                }
            });

            <? if($bRotate) { ?>bff.rotateTable($list, ajaxUrl+'rotate', $progress, false, {cat:cat}, false, function(param){
                param.cat = cat;
            });<? } ?>
        });

        function isProcessing()
        {
            return processing;
        }

        function del(id, link)
        {
            bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
            return false;
        }

        function updateList(updateUrl)
        {
            if(isProcessing()) return;
            var f = $(filters).serialize();
            bff.ajax(ajaxUrl, f, function(data){
                if(data) {
                    $list.html( data.list );
                    $listPgn.html( data.pgn );
                    if(updateUrl !== false && bff.h) {
                        window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                    }
                    <? if($bRotate) { ?>$list.tableDnDUpdate();<? } ?>
                }
            }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
        }

        function setPage(id)
        {
            // нет постраничности
        }

        return {
            submit: function(resetForm)
            {
                if(isProcessing()) return false;
                setPage(1);
                if(resetForm) {
                    //
                }
                updateList();
            },
            page: function (id)
            {
                if(isProcessing()) return false;
                setPage(id);
                updateList();
            },
            refresh: function(resetPage,updateUrl)
            {
                if(resetPage) setPage(0);
                updateList(updateUrl);
            },
            toggle: function(show)
            {
                if(show === true) {
                    $block.show();
                    if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                }
                else $block.hide();
            },
            onCat:function(sel)
            {   cat = $(sel).val();
                updateList();
            }
        };
    }());

</script>