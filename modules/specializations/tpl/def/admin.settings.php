<?php
$aTabs = array(
    'services'  => 'Услуги',
);
?>
<?= tplAdmin::blockStart(_t('specs','Специализации').' / '._t('','Settings'), false, array('id'=>'SpecsSettingsFormBlock')); ?>

<form name="SpecsSettingsForm" id="SpecsSettingsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="edit" />
    <input type="hidden" name="save" value="1" />
    <div class="tabsBar" id="SpecsSettingsFormTabs">
        <? foreach($aTabs as $k=>$v) { ?>
            <span class="tab<? if($k == 'services') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
        <? } ?>
    </div>
    <div class="j-tab j-tab-services">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1" width="190"><span class="field-title">Формирование цен на услуги:</span></td>
                <td class="row2">
                    <label class="radio"><input type="radio" name="specs_services_price" value="<?= Specializations::SERVICES_PRICE_ADMIN ?>" <?= $specs_services_price == Specializations::SERVICES_PRICE_ADMIN ? 'checked="checked"' : '' ?>>только администратором</label>
                    <label class="radio"><input type="radio" name="specs_services_price" value="<?= Specializations::SERVICES_PRICE_USERS ?>" <?= $specs_services_price == Specializations::SERVICES_PRICE_USERS ? 'checked="checked"' : '' ?>>пользователями и администратором</label>
                </td>
            </tr>
            <tr class="j-specs_services_price j-specs_services_price-<?= Specializations::SERVICES_PRICE_USERS ?><?= $specs_services_price != Specializations::SERVICES_PRICE_USERS ? ' hide' : ''?>" >
                <td class="row1"><span class="field-title">Общий прайс формируется:</span></td>
                <td class="row2">
                    <label class="radio"><input type="radio" name="specs_services_price_author" value="<?= Specializations::SERVICES_PRICE_AUTHOR_ADMIN ?>" <?= $specs_services_price_author == Specializations::SERVICES_PRICE_AUTHOR_ADMIN ? 'checked="checked"' : '' ?>>администратором</label>
                    <label class="radio"><input type="radio" name="specs_services_price_author" value="<?= Specializations::SERVICES_PRICE_AUTHOR_USERS ?>" <?= $specs_services_price_author == Specializations::SERVICES_PRICE_AUTHOR_USERS ? 'checked="checked"' : '' ?>>пользователями</label>
                </td>
            </tr>
            <tr class="j-specs_services_price j-specs_services_price-<?= Specializations::SERVICES_PRICE_USERS ?> j-specs_services_price_author j-specs_services_price_author-<?= Specializations::SERVICES_PRICE_AUTHOR_USERS ?><?= $specs_services_price != Specializations::SERVICES_PRICE_USERS ? ' hide' : ''?><?= $specs_services_price_author != Specializations::SERVICES_PRICE_AUTHOR_USERS ? ' hidden' : ''?>">
                <td class="row1"><span class="field-title">Статистика цен:</span></td>
                <td class="row2">
                    <label class="checkbox"><input type="checkbox" name="specs_services_price_author_rows[]" value="<?= Specializations::SERVICES_PRICE_AUTHOR_ROWS_MIN ?>" <?= $specs_services_price_author_rows & Specializations::SERVICES_PRICE_AUTHOR_ROWS_MIN ? 'checked="checked"' : '' ?>>минимальные</label>
                    <label class="checkbox"><input type="checkbox" name="specs_services_price_author_rows[]" value="<?= Specializations::SERVICES_PRICE_AUTHOR_ROWS_AVG ?>" <?= $specs_services_price_author_rows & Specializations::SERVICES_PRICE_AUTHOR_ROWS_AVG ? 'checked="checked"' : '' ?>>средние</label>
                    <label class="checkbox"><input type="checkbox" name="specs_services_price_author_rows[]" value="<?= Specializations::SERVICES_PRICE_AUTHOR_ROWS_MAX ?>" <?= $specs_services_price_author_rows & Specializations::SERVICES_PRICE_AUTHOR_ROWS_MAX ? 'checked="checked"' : '' ?>>максимальные</label>
                </td>
            </tr>
        </table>
    </div>

    <div style="margin-top: 10px;">
        <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jSpecsSettingsForm.save(false);" />
        <div id="SpecsSettingsFormProgress" class="progress" style="display: none;"></div>
        <div class="clearfix"></div>
    </div>

</form>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
    var jSpecsSettingsForm =
        (function(){
            var $progress, $form, f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#SpecsSettingsFormProgress');
                $form = $('#SpecsSettingsForm');
                f = $form.get(0);

                // tabs
                $form.find('#SpecsSettingsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });

                $form.on('change', '[name="specs_services_price"]', function(){
                    var v = $(this).val();
                    $form.find('.j-specs_services_price').addClass('hide');
                    $form.find('.j-specs_services_price-'+v).removeClass('hide');
                });

                $form.on('change', '[name="specs_services_price_author"]', function(){
                    var v = $(this).val();
                    $form.find('.j-specs_services_price_author').addClass('hidden');
                    $form.find('.j-specs_services_price_author-'+v).removeClass('hidden');
                });
            });

            return {
                save: function()
                {
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                        }
                    }, $progress);
                }
            };
        }());
</script>