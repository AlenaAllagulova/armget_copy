<?php
foreach($list as $k=>$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>" id="dnds-<?= $id ?>">
        <td class="left">
            <?= $v['title'] ?>
        </td>
        <td class="left" style="width: 50px;">
            <a class="but edit service-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
            <a class="but del service-del" title="<?= _t('', 'Delete') ?>" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="2">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;