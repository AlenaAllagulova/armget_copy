<?php
if( ! empty($list)):
$q = mb_strtolower($q);
$upQ = mb_strtoupper(mb_substr($q, 0 , 1)).mb_substr($q, 1);
$upQQ = mb_strtoupper($q);
    foreach($list as $v):
        $v['spec_title'] = str_replace($q, '<em class="text-highlight">' . $q . '</em>', $v['spec_title']);
        $v['spec_title'] = str_replace($upQ, '<em class="text-highlight">' . $upQ . '</em>', $v['spec_title']);
        $v['spec_title'] = str_replace($upQQ, '<em class="text-highlight">' . $upQQ . '</em>', $v['spec_title']);
?>
    <li><a href="#" class="j-spec-search" data="{spec:<?= $v['spec_id']; ?>, cat:<?= $v['cat_id']; ?>}"><?= $v['cat_title'] ?> / <?= $v['spec_title'] ?></a></li>
<? endforeach;
else: ?>
    <li class="dropdown-menu-none"><?= _t('specs', 'Ничего не найдено'); ?></li>
<? endif;
