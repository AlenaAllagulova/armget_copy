<?php
$ordersDP = Orders::dynpropsEnabled();
foreach($list as $k=>$v):
    $id = $v['id']; $is_root = ($id == Specializations::ROOT_SPEC); ?>
    <tr class="row<?= ($k%2) ?><?= $is_root ? ' nodrag nodrop' : '' ?>" id="dnd-<?= $id ?>">
        <td class="left<?= $is_root ? ' disabled' : '' ?>" <?= ! empty($noActions) ? 'style="padding-left:28px;"' : '' ?>>
            <?= $v['title'] ?>
        </td>
        <td class="left">
            <? if(empty($noActions)): ?>
            <a class="but spec specialization-dynprops" title="<?= _t('', 'Дин.свойства исполнителей') ?>" href="#" rel="<?= $id ?>"></a>
            <? if($ordersDP): ?><a class="but sett specialization-dynprops-orders" title="<?= _t('', 'Дин.свойства заказов') ?>" href="#" rel="<?= $id ?>"></a><? endif; ?>
            <a class="but edit specialization-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
            <? if( ! $is_root): ?>
                <a class="but <?= ($v['enabled']?'un':'') ?>block specialization-toggle" title="<?= _t('', 'Enable') ?>" href="#" data-type="enabled" rel="<?= $id ?>"></a>
                <a class="but del specialization-del" title="<?= _t('', 'Delete') ?>" href="#" rel="<?= $id ?>"></a>
            <? endif; endif; ?>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="2">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;