<?php
/**
 * @var Specializations $this
 */
$event = 'specialization_services_list';
?>
<?= tplAdmin::blockStart('Добавление специализации', false, array('id'=>'SpecializationServicesFormBlock','style'=>'display:none;')); ?>
<div id="SpecializationServicesFormContainer"></div>
<?= tplAdmin::blockStop(); ?>
<div id="SpecializationServicesListBlock">
    <div class="actionBar displaynone">
        <form method="get" action="<?= $this->adminLink(NULL) ?>" id="SpecializationServicesListFilters" onsubmit="return false;" class="form-inline">
            <input type="hidden" name="s" value="<?= bff::$class ?>" />
            <input type="hidden" name="ev" value="<?= $event ?>" />
            <input type="hidden" name="spec" value="<?= $id ?>" />
        </form>
    </div>
    <table class="table table-condensed table-hover admtbl tblhover" id="SpecializationServicesListTable">
        <thead>
        <tr class="header nodrag nodrop">
            <th class="left" colspan="2">
                <?= _t('', 'Title') ?>
                <div class="right"  style="position: relative;">
                    <a href="#" class="ajax desc" onclick="return jSpecializationServicesFormManager.action('add',0);" style="font-weight: normal" ><?= _t('', '+ добавить услугу') ?></a>
                    <div id="SpecializationServicesProgress" class="progress" style="position:absolute; display: none;right: 0;top: -27px;"></div>
                </div>
            </th>
        </tr>
        </thead>
        <tbody id="SpecializationServicesList">
        </tbody>
    </table>
    <div>
        <div class="left">
        </div>
        <div class="right desc" style="width:60px; text-align:right; margin-right: 15px;">
            &darr; &uarr;
        </div>
        <br />
        <div class="clear-all"></div>
    </div>
</div>

<script type="text/javascript">
var jSpecializationServicesFormManager = (function(){
    var $progress, $formContainer, $block, $blockCaption, process = false;
    var ajaxUrl = '<?= $this->adminLink($event.'&spec='.$id); ?>';

    $(function(){
        $formContainer = $('#SpecializationServicesFormContainer');
        $progress = $('#SpecializationServicesProgress');
        $block = $('#SpecializationServicesFormBlock');
        $blockCaption = $block.find('span.caption');
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jSpecializationServicesList.toggle(false);
            if(jSpecializationServiceForm) jSpecializationServiceForm.onShow();
        } else {
            jSpecializationServicesList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? 'Добавление услуги' : 'Редактирование услуги'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                /*
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
                */
            } else {
                jSpecializationServicesList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        params = $.extend(params || {}, {act:type});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jSpecializationServicesList = (function(){
        var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, spec = <?= $id ?>;
        var ajaxUrl = '<?= $this->adminLink($event.'&act='); ?>';
        var inited = false;

        $(function(){
            $progress  = $('#SpecializationServicesProgress');
            $block     = $('#SpecializationServicesListBlock');
            $list      = $block.find('#SpecializationServicesList');
            $listTable = $block.find('#SpecializationServicesListTable');
            $listPgn   = $block.find('#SpecializationServicesListPgn');
            filters    = $block.find('#SpecializationServicesListFilters').get(0);

            $list.delegate('a.service-edit', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) jSpecializationServicesFormManager.action('edit',id);
                return false;
            });

            $list.delegate('a.service-toggle', 'click', function(){
                var id = intval($(this).attr('rel'));
                var type = $(this).data('type');
                if(id>0) {
                    var params = {progress: $progress, link: this};
                    bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                }
                return false;
            });

            $list.delegate('a.service-del', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) del(id, this);
                return false;
            });

            bff.rotateTable($list, ajaxUrl+'rotate', $progress, false, {spec:spec}, false, function(param){
                param.spec = spec;
            });
        });

        function isProcessing()
        {
            return processing;
        }

        function del(id, link)
        {
            bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
            return false;
        }

        function updateList(updateUrl)
        {
            if(isProcessing()) return;
            var f = $(filters).serialize();
            bff.ajax(ajaxUrl, f, function(data){
                if(data) {
                    $list.html( data.list );
                    $listPgn.html( data.pgn );
                    $list.tableDnDUpdate();
                }
            }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
        }

        return {
            submit: function(resetForm)
            {
                if(isProcessing()) return false;
                if(resetForm) {
                    //
                }
                updateList();
            },
            refresh: function(resetPage,updateUrl)
            {
                updateList(updateUrl);
            },
            toggle: function(show)
            {
                if(show === true) {
                    $block.show();
                }
                else $block.hide();
            },
            onShow: function(){
                if( ! inited){
                    inited = true;
                    updateList();
                }
            }
        };
    }());
</script>
