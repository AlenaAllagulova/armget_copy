<?php
foreach($list as $k=>$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>" id="dnd-<?= $id ?>" data-numlevel="<?= $v['numlevel'] ?>" data-pid="<?= $v['pid'] ?>">
        <td class="left">
            <a class="but folder but-text j-specs" data-id="<?= $id ?>" href="#"><?= $v['title'] ?></a>
        </td>
        <td>
            <a class="but edit category-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
            <a class="but <?= ($v['enabled']?'un':'') ?>block category-toggle" title="<?= _t('', 'Enable') ?>" href="#" data-type="enabled" rel="<?= $id ?>"></a>
            <a class="but del category-del" title="<?= _t('', 'Delete') ?>" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="2">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;