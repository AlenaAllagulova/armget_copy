<?php
use bff\utils\VideoParser;

abstract class PortfolioBase_ extends Module
{
    /** @var PortfolioModel */
    var $model = null;
    var $securityKey = 'bef5130ec2a4671047b40378ee6a51d1';

    # Статус работы
    const STATUS_ACTIVE     = 1; # активна
    const STATUS_BLOCKED    = 5; # заблокирована

    public function init()
    {
        parent::init();
        $this->module_title = _t('portfolio','Портфолио');

        bff::autoloadEx(array(
            'PortfolioItemImages'  => array('app', 'modules/portfolio/portfolio.item.images.php'),
            'PortfolioItemPreview' => array('app', 'modules/portfolio/portfolio.item.preview.php'),
        ));
    }

    /**
     * @return Portfolio
     */
    public static function i()
    {
        return bff::module('Portfolio');
    }

    /**
     * @return PortfolioModel
     */
    public static function model()
    {
        return bff::model('Portfolio');
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'portfolio_item_blocked' => array(
                'title'       => 'Портфолио: Работа заблокирована модератором',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при блокировке работы модератором',
                'vars'        => array(
                    '{fio}'            => 'ФИО исполнителя',
                    '{name}'           => 'Имя исполнителя',
                    '{item_id}'        => 'ID товара',
                    '{item_title}'     => 'Заголовок товара',
                    '{item_url}'       => 'Ссылка для просмотра товара',
                    '{blocked_reason}' => 'Причина блокировки',
                ),
                'impl'        => true,
                'priority'    => 40,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'profile',
            ),
            'portfolio_item_approved' => array(
                'title'       => 'Портфолио: Работа одобрена модератором',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при одобрении работы модератором',
                'vars'        => array(
                    '{fio}'          => 'ФИО исполнителя',
                    '{name}'         => 'Имя исполнителя',
                    '{item_id}'      => 'ID товара',
                    '{item_title}'   => 'Заголовок товара',
                    '{item_url}'     => 'Ссылка для просмотра товара',
                ),
                'impl'        => true,
                'priority'    => 41,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'profile',
            ),
        );
        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Добавление
            case 'add':
                $url .= '/portfolio/add'.(!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Инфо о статусе работы
            case 'status':
                $url .= '/portfolio/status'.static::urlQuery($opts);
                break;
            # Редактирование
            case 'edit':
                $url .= '/portfolio/edit'.static::urlQuery($opts);
                break;
            # Кабинет: портфолио
            case 'my.portfolio':
                $opts['login'] = User::data('login');
                $url = static::url('user.listing', $opts, $dynamic);
                break;
            # Кабинет: настройки портфолио
            case 'my.settings':
                $url  = Users::url('profile', array('login'=>User::data('login'), 'tab'=>'portfolio/settings'), $dynamic);
                $url .= (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Профиль: портфолио
            case 'user.listing':
                $url  = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'portfolio'), $dynamic);
                $url .= (!empty($opts) ? static::urlQuery($opts, array('login')) : '');
                break;
            # Профиль: просмотр работы
            case 'user.view':
                $url  = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'portfolio'), $dynamic);
                $url .= $opts['id'] . '-' . $opts['keyword'] .'.html'.
                    ( ! empty($opts) ? static::urlQuery($opts, array('id','keyword','login')) : '');
                break;
        }
        return bff::filter('portfolio.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    public static function aTermMeas()
    {
        return bff::filter('portfolio.term.meas', array(
            1 => array('id' => 1, 't' => _t('portfolio', 'В часах'),   'decl' => _t('portfolio', 'час;часа;часов')),
            2 => array('id' => 2, 't' => _t('portfolio', 'В днях'),    'decl' => _t('portfolio', 'день;дня;дней')),
            3 => array('id' => 3, 't' => _t('portfolio', 'В месяцах'), 'decl' => _t('portfolio', 'месяц;месяца;месяцев')),
        ));
    }

    /**
     * Использовать премодерацию работ
     * @return bool
     */
    public static function premoderation()
    {
        return config::sysAdmin('portfolio.premoderation', true, TYPE_BOOL);
    }

    /**
     * Максимально допустимое кол-во фотографий прикрепляемых к работе
     * 0 - возможность прикрепления фотографий выключена (недоступна)
     * @return integer
     */
    public static function imagesLimit()
    {
        return config::sysAdmin('portfolio.images.limit', 8, TYPE_UINT);
    }

    /**
     * Отображение превью работ доступно только для PRO (true/false)
     * @return bool
     */
    public static function previewOnlyPro()
    {
        return config::sysAdmin('portfolio.preview.pro', true, TYPE_BOOL);
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nItemID ID работы или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @param integer $nUserID ID пользователя
     * @return array параметры
     */
    protected function validateItemData($nItemID, $bSubmit, $nUserID)
    {
        $aData = array();
        $aParams = array(
            'title'      => TYPE_NOTAGS,# Название
            'descr'      => TYPE_NOTAGS,# Описание
            'spec_id'    => TYPE_UINT,  # Специализация
            'link'       => TYPE_NOTAGS,# Ссылка
            'video'      => TYPE_STR,   # Ссылка на видео
            'price'      => TYPE_PRICE, # стоимость
            'price_curr' => TYPE_UINT,  # валюта
            'price_rate' => TYPE_UINT,  # валюта срок
            'term'       => TYPE_UINT,  # срок
            'term_meas'  => TYPE_UINT,  # срок ед. измерения
        );

        if ( ! bff::adminPanel()) {
            $aParams['order']     = TYPE_UINT; # порядок сортировки
            $aParams['order-num'] = TYPE_UINT; # номер
        }

        $this->input->postm($aParams, $aData);

        if ($bSubmit)
        {
            if (empty($aData['title'])) {
                $this->errors->set(_t('portfolio','Введите название'));
            }

            if ( ! bff::adminPanel()) {
                # антиспам фильтр
                Site::i()->spamFilter(array(
                    array('text' => & $aData['title'], 'error'=>_t('', 'В указанном вами названии присутствует запрещенное слово "[word]"')),
                    array('text' => & $aData['descr'], 'error'=>_t('', 'В указанном вами описании присутствует запрещенное слово "[word]"')),
                ));
            }

            if ( ! $aData['spec_id']) {
                $this->errors->set(_t('portfolio','Выберите раздел'));
            } else {
                $aSpecs = Users::model()->userSpecs($nUserID, false);
                if ( ! array_key_exists($aData['spec_id'], $aSpecs)) {
                    $this->errors->set(_t('portfolio','Выберите раздел'));
                } else {
                    $aData['cat_id'] = $aSpecs[ $aData['spec_id'] ]['cat_id'];
                }

                # кеш для модификатора бюджета
                $aData['price_rate_text'] = '';
                $aPriceSett = Specializations::i()->aPriceSett($aData['spec_id'], true);
                $nPriceRate = $aData['price_rate'];
                if (!empty($aPriceSett['rates'][$nPriceRate])) {
                    $aRates = $aPriceSett['rates'][$nPriceRate];
                    foreach ($aRates as $k => $v) {
                        $aRates[$k] = mb_strtolower($v);
                    }
                    $aData['price_rate_text'] = serialize($aRates);
                } else {
                    $aData['price_rate'] = 0;
                }
            }

            if ( ! empty($aData['link'])) {
                if( ! filter_var($aData['link'], FILTER_VALIDATE_URL)){
                    $this->errors->set(_t('portfolio','Ссылка указана некорректно'));
                }
            }

            if ( ! empty($aData['video'])) {
                $oVideoParser = new VideoParser();
                $aEmbed = $oVideoParser->embed($aData['video']);
                if ( ! empty($aEmbed['html'])) {
                    $aData['video'] = serialize(array(
                        'orig_url' => $aData['video'],
                        'html' => $aEmbed['html'],
                    ));
                } else {
                    $aData['video'] = '';
                }
            }

            # URL-Keyword
            $aData['keyword'] = mb_strtolower(func::translit($aData['title']));
            $aData['keyword'] = trim(preg_replace('/[^a-zA-Z0-9_\-]/', '', $aData['keyword']), '-');
        }

        return $aData;
    }

    /**
     * Инициализация компонента PortfolioItemImages
     * @param integer $nItemID ID работы
     * @return PortfolioItemImages component
     */
    public function itemImages($nItemID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new PortfolioItemImages();
        }
        $i->setRecordID($nItemID);

        return $i;
    }

    /**
     * Инициализация компонента PortfolioItemPreview
     * @param integer $nItemID ID работы
     * @return PortfolioItemPreview component
     */
    public function itemPreview($nItemID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new PortfolioItemPreview();
        }
        $i->setRecordID($nItemID);

        return $i;
    }

    /**
     * Является ли текущий пользователь владельцем работы
     * @param integer $nItemID ID работы
     * @param integer|bool $nItemUserID ID пользователя заказа или FALSE (получаем из БД)
     * @return boolean
     */
    public function isItemOwner($nItemID, $nItemUserID = false)
    {
        $nUserID = User::id();
        if (!$nUserID) {
            return false;
        }

        if ($nItemUserID === false) {
            $aData = $this->model->itemData($nItemID, array('user_id'));
            if (empty($aData)) {
                return false;
            }

            $nItemUserID = $aData['user_id'];
        }

        return ($nItemUserID > 0 && $nUserID == $nItemUserID);
    }

    /**
     * Удаление работы
     * @param integer $nItemID ID работы
     * @return boolean
     */
    public function itemDelete($nItemID)
    {
        if (empty($nItemID)) return false;

        $aData = $this->model->itemData($nItemID);
        if (empty($aData)) {
            return false;
        }

        $res = $this->model()->itemDelete($nItemID);
        if (!empty($res)) {
            $this->itemImages($nItemID)->deleteAllImages(false);
            $this->itemPreview($nItemID)->delete(false);

            $this->model->makeUserWorksCache($aData['user_id']);  # обновим кеш работ

            # обновляем счетчик работ "на модерации"
            $this->moderationCounterUpdate();

            return true;
        }

        return false;
    }

    /**
     * Актуализация счетчика работ ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->itemsModeratingCounter();
            config::save('portfolio_items_moderating', $count, true);
        } else {
            config::saveCount('portfolio_items_moderating', $increment, true);
        }
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('portfolio', 'images') => 'dir-split', # изображения работ + preview
            bff::path('tmp', 'images') => 'dir-only', # tmp
        ));
    }

}