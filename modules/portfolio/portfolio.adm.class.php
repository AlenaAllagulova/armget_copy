<?php

/**
 * Права доступа группы:
 *  - portfolio: Портфолио
 *      - items: Управление работами (список, добавление, редактирование, удаление)
 *      - items-moderate: Модерация работ
 *      - settings: Настройки
 */
class Portfolio_ extends PortfolioBase
{
    public function listing()
    {
        if (!($this->haveAccessTo('items') || $this->haveAccessTo('items-moderate')))
            return $this->showAccessDenied();

        $accessManage = $this->haveAccessTo('items');

        $sAct = $this->input->postget('act',TYPE_STR);
        if ( ! empty($sAct) || Request::isPOST())
        {
            $aResponse = array();
            switch ($sAct)
            {
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nItemID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nItemID) { $this->errors->unknownRecord(); break; }

                    if ($bSubmit)
                    {
                        $aDataOld = $this->model->itemData($nItemID, array('user_id'), true);
                        $aData = $this->validateItemData($nItemID, $bSubmit, $aDataOld['user_id']);

                        if (!$accessManage) {
                            $this->errors->accessDenied();
                        }

                        if ($this->errors->no()) {
                            $this->model->itemSave($nItemID, $aData);

                            # сохраним превью
                            $aUpdate = array();
                            $oPreview = static::itemPreview($nItemID);
                            foreach ($oPreview->getVariants() as $iconField => $v) {
                                $oPreview->setVariant($iconField);
                                $aIconData = $oPreview->uploadFILES($iconField, true, false);
                                if (!empty($aIconData)) {
                                    $aUpdate[$iconField] = $aIconData['filename'];
                                } else {
                                    if ($this->input->post($iconField . '_del', TYPE_BOOL)) {
                                        if ($oPreview->delete(false)) {
                                            $aUpdate[$iconField] = '';
                                        }
                                    }
                                }
                            }
                            if ( ! empty($aUpdate)) {
                                $this->model->itemSave($nItemID, $aUpdate);
                            }

                            $this->model->makeUserWorksCache($aDataOld['user_id']); # обновим кеш работ
                        }
                        $aData['id'] = $nItemID;
                        $this->iframeResponseForm($aResponse);
                    } else {
                        $aData = $this->model->itemData($nItemID, array(), true);
                        if (empty($aData)) { $this->errors->unknownRecord(); break; }

                        $aData['specs'] = Users::model()->userSpecs($aData['user_id'], false);
                        foreach ($aData['specs'] as $k => $v) {
                            $aData['specs'][$k]['t'] = ( ! empty($v['cat_title']) ? $v['cat_title'].' / ' : '').$v['spec_title'];
                        }
                        $aData['img'] = $this->itemImages($nItemID);
                        $aData['images'] = $aData['img']->getData($aData['imgcnt']);

                        $aData['act'] = $sAct;
                        $aData['priceSett'] = Specializations::i()->aPriceSett($aData['spec_id']);
                        $aData['priceForm'] = $this->viewPHP($aData, 'admin.items.form.price');
                        $aResponse['form'] = $this->viewPHP($aData, 'admin.items.form');
                    }
                } break;
                case 'toggle':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $nItemID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nItemID) { $this->errors->unknownRecord(); break; }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->itemToggle($nItemID, $sToggleType);
                } break;
                case 'delete':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $nItemID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nItemID) { $this->errors->impossible(); break; }

                    $res = $this->itemDelete($nItemID);
                    if ( ! $res) { $this->errors->impossible(); break; }
                    else {
                    }
                } break;
                case 'img-upload': # изображения: загрузка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        $aResponse = array('success' => false, 'errors' => $this->errors->get());
                        $this->ajaxResponse($aResponse, true, false, true);
                    }

                    $nItemID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->itemImages($nItemID);
                    $mResult = $oImages->uploadQQ();
                    $aResponse = array('success' => ($mResult !== false && $this->errors->no()));

                    if ($mResult !== false) {
                        $aResponse = array_merge($aResponse, $mResult);
                        $aResponse = array_merge($aResponse,
                            $oImages->getURL($mResult,
                                array(PortfolioItemImages::szForm, PortfolioItemImages::szView),
                                empty($nItemID)
                            )
                        );
                    }
                    $aResponse['errors'] = $this->errors->get();
                    $this->ajaxResponse($aResponse, true, false, true);
                }
                break;
                case 'img-saveorder': # изображения: сохранение порядка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nItemID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->itemImages($nItemID);
                    $img = $this->input->post('img', TYPE_ARRAY);
                    if (!$oImages->saveOrder($img, false, true)) {
                        $this->errors->impossible();
                    }
                }
                break;
                case 'img-delete': # изображения: удаление
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nItemID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->itemImages($nItemID);
                    $nImageID = $this->input->post('image_id', TYPE_UINT);
                    $sFilename = $this->input->post('filename', TYPE_NOTAGS);
                    if (!$nImageID && empty($sFilename)) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($nImageID) {
                        $oImages->deleteImage($nImageID);
                    } else {
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'img-delete-all': # изображения: удаление всех изображений
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nItemID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->itemImages($nItemID);
                    if ($nItemID) {
                        $oImages->deleteAllImages(true);
                    } else {
                        $sFilename = $this->input->post('filenames', TYPE_ARRAY_STR);
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'price-form':
                {
                    $nSpecID = $this->input->post('id', TYPE_UINT);
                    if( ! $nSpecID){
                        $aResponse['html'] = '';
                        break;
                    }

                    $aData = array(
                        'price'      => 0,
                        'price_curr' => 0,
                        'price_rate' => 0,
                    );
                    $aData['priceSett'] = Specializations::i()->aPriceSett($nSpecID);
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.items.form.price');
                }
                break;
                case 'dev-users-counters-update': # пересчет счетчиков работ пользователей
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }

                    $this->model->userSpecsItemsCount();
                    # обновляем счетчик работ "на модерации"
                    $this->moderationCounterUpdate();

                    $this->adminRedirect(($this->errors->no() ? Errors::SUCCESS : Errors::IMPOSSIBLE), bff::$event);
                }
                break;
                default: $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = array();
        $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'tab'     => TYPE_UINT,
            'title'   => TYPE_NOTAGS,
            'user'    => TYPE_NOTAGS,
            'cat_id'  => TYPE_UINT,
            'spec_id' => TYPE_UINT,
        ), $f);

        # формируем фильтр списка работ
        $sql = array();
        $mPerpage = 15;
        $aData['pgn'] = '';

        $aData['orders'] = array('id'=>'asc','created'=>'asc',);
        $f += $this->prepareOrder($orderBy, $orderDirection, 'created-desc', $aData['orders']);
        $f['order'] = $orderBy.'-'.$orderDirection; $sqlOrder = "$orderBy $orderDirection";

        switch ($f['tab']) {
            case 0: # Активные
            {
                $sql['status'] = static::STATUS_ACTIVE;
                if (static::premoderation()) {
                    $sql[':mod'] = 'moderated > 0';
                }
            }
            break;
            case 2: # На модерации
            {
                $sql[':mod'] = 'moderated != 1';
            }
            break;
            case 3: # Заблокированные
            {
                $sql['status'] = static::STATUS_BLOCKED;
            }
            break;
            case 4: # Все
            {
            }
            break;
        }

        if ( ! empty($f['title'])) {
            $sql[':title'] = array('(I.id = '.intval($f['title']).' OR I.title LIKE :title)', ':title' => '%'.$f['title'].'%');
        }
        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(I.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user )',
                ':user' => '%' . $f['user'] . '%'
            );
        }
        if ( ! empty($f['cat_id'])) {
            $sql['cat_id'] = $f['cat_id'];
        }
        if ( ! empty($f['spec_id'])) {
            $sql['spec_id'] = $f['spec_id'];
        }

        if ($mPerpage!==false) {
            $nCount = $this->model->itemsListing($sql, true);
            $oPgn = new Pagination($nCount, $mPerpage, '#', 'jPortfolioItemsList.page('.Pagination::PAGE_ID.'); return false;');
            $oPgn->pageNeighbours = 6;
            $aData['pgn'] = $oPgn->view(array('arrows'=>false));
            $aData['list'] = $this->model->itemsListing($sql, false, $oPgn->getLimitOffset(), $sqlOrder);
        } else {
            $aData['list'] = $this->model->itemsListing($sql, false, '', $sqlOrder);
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);

        $aData['list'] = $this->viewPHP($aData, 'admin.items.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list'=>$aData['list'],
                'pgn'=>$aData['pgn'],
            ));
        }

        $aData['act'] = $sAct;
        $aData['specsOptions'] = Specializations::model()->specializationsOptions(0, 0, array('empty' => 'Все специализации'));

        tpl::includeJS(array('ui.sortable', 'qquploader'), true);

        return $this->viewPHP($aData, 'admin.items.listing');
    }

    public function item_status()
    {
        if ( ! $this->haveAccessTo('items-moderate')) {
            $this->errors->accessDenied();
        }


        $nItemID = $this->input->post('id', TYPE_UINT);
        if (!$nItemID) {
            $this->errors->unknownRecord();
        } else {
            $aData = $this->model->itemData($nItemID, array(), true);
            if (empty($aData)) {
                $this->errors->unknownRecord();
            }
        }
        if ( ! $this->errors->no()) {
            $this->ajaxResponseForm();
        }

        $aResponse = array();
        switch ($this->input->postget('act', TYPE_STR))
        {
            case 'approve': # модерация: одобрение
            {
                $aUpdate = array(
                    'moderated' => 1
                );

                if ($aData['status'] == self::STATUS_BLOCKED) {
                    /**
                     * В случае если "Одобряем" заблокированную работу
                     * => значит она после блокировки была отредактирована пользователем
                     */
                    $aUpdate[] = 'status_prev = status';
                    $aUpdate['status'] = ( in_array($aData['status_prev'], array(
                        self::STATUS_ACTIVE,
                    )) ? $aData['status_prev'] : self::STATUS_ACTIVE );
                    $aUpdate['status_changed'] = $this->db->now();
                }

                $res = $this->model->itemSave($nItemID, $aUpdate);
                $this->model->specUserActiveCount($aData['user_id'], $aData['spec_id']);
                $this->model->makeUserWorksCache($aData['user_id']); # обновим кеш работ

                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик работ "на модерации"
                $this->moderationCounterUpdate();

                # Отправим письмо исполнителю
                Users::sendMailTemplateToUser($aData['user_id'], 'portfolio_item_approved', array(
                    'item_id' => $nItemID,
                    'item_title' => $aData['title'],
                    'item_url' => static::url('view', array('id' =>$nItemID,'keyword'=>$aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            }
                break;
            case 'block': # модерация: блокировка / разблокировка
            {
                /**
                 * Блокировка работы (если уже заблокирована => изменение причины блокировки)
                 * @param string 'blocked_reason' причина блокировки
                 * @param integer 'id' ID работы
                 */

                $bUnblock = $this->input->post('unblock', TYPE_UINT);
                $sBlockedReason = $this->input->postget('blocked_reason', TYPE_NOTAGS, array('len' => 1000, 'len.sys' => 'portfolio.blocked_reason.limit'));
                $bBlocked = ($aData['status'] == self::STATUS_BLOCKED);

                if ($aData['user_id']) {
                    $aUserData = Users::model()->userData($aData['user_id'], array('user_id', 'blocked'));
                    if (!empty($aUserData['blocked'])) {
                        $this->errors->set(_t('portfolio', 'Для блокировки/разблокировки работы, разблокируйте аккаунт владельца данной работы'));
                        break;
                    }
                }

                $aUpdate = array(
                    'moderated'      => 1,
                    'blocked_reason' => $sBlockedReason,
                    'status_changed' => $this->db->now(),
                );

                $bBlockedResult = $bBlocked;
                if (!$bBlocked) { # блокируем
                    $aUpdate['status_prev'] = $aData['status'];
                    $aUpdate['status'] = self::STATUS_BLOCKED;
                    $bBlockedResult = true;

                    # Отправим письмо исполнителю
                    Users::sendMailTemplateToUser($aData['user_id'], 'portfolio_item_blocked', array(
                        'item_id' => $nItemID,
                        'item_title' => $aData['title'],
                        'item_url' => static::url('view', array('id'=>$nItemID, 'keyword'=>$aData['keyword'])),
                        'blocked_reason' => nl2br($sBlockedReason),
                    ), Users::ENOTIFY_GENERAL);

                } else {
                    if ($bUnblock) {
                        # разблокируем
                        $aUpdate['status'] = self::STATUS_ACTIVE;
                        $aUpdate['status_prev'] = self::STATUS_BLOCKED;
                        $bBlockedResult = false;

                        # Отправим письмо исполнителю
                        Users::sendMailTemplateToUser($aData['user_id'], 'portfolio_item_approved', array(
                            'item_id' => $nItemID,
                            'item_title' => $aData['title'],
                            'item_url' => static::url('view', array('id' =>$nItemID,'keyword'=>$aData['keyword'])),
                        ), Users::ENOTIFY_GENERAL);
                    }
                }

                $res = $this->model->itemSave($nItemID, $aUpdate);
                if (!$res) {
                    $this->errors->impossible();
                    break;
                }
                $this->model->specUserActiveCount($aData['user_id'], $aData['spec_id']);
                $this->model->makeUserWorksCache($aData['user_id']); # обновим кеш работ

                # обновляем счетчик работ "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['blocked'] = $bBlockedResult;
                $aResponse['reason'] = $sBlockedReason;

            }
                break;
        }

        if ($this->errors->no()) {
            $aData = $this->model->itemData($nItemID, array(), true);
            if ( ! empty($aData)) {
                $aData['only_form'] = true;
                $aResponse['html'] = $this->viewPHP($aData, 'admin.items.form.status');
            }
        }
        $this->ajaxResponseForm($aResponse);
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'share_code' => TYPE_STR,
            ), $aData);

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad();
        if ( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

}