<?php

return [
    # действия
    'portfolio-action' => [
        'pattern'  => 'portfolio/(add|edit|promote|status)',
        'callback' => 'portfolio/$1/',
        'priority' => 100,
    ],
];