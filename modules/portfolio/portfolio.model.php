<?php

define('TABLE_PORTFOLIO_ITEMS',        DB_PREFIX.'portfolio_items');
define('TABLE_PORTFOLIO_ITEMS_IMAGES', DB_PREFIX.'portfolio_items_images');
define('TABLE_PORTFOLIO_SPECS_USERS',  DB_PREFIX.'portfolio_specs_users');

class PortfolioModel_ extends Model
{
    /** @var PortfolioBase */
    var $controller;

    public function init()
    {
        parent::init();
    }

    # --------------------------------------------------------------------
    # Работы

    /**
     * Список работ (admin)
     * @param array $aFilter фильтр списка работ
     * @param bool $bCount только подсчет кол-ва работ
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function itemsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $sFrom = '';
        if (array_key_exists(':user', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS . ' U ';
            $aFilter[':users-join'] = ' I.user_id = U.user_id ';
        }

        $aFilter = $this->prepareFilter($aFilter, 'I');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(I.id) FROM '.TABLE_PORTFOLIO_ITEMS.' I '.$sFrom.$aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT I.id, I.user_id, I.created, I.title, I.status, I.moderated, I.imgcnt
               FROM '.TABLE_PORTFOLIO_ITEMS.' I '.$sFrom.'
               '.$aFilter['where']
            .( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '')
            .$sqlLimit, $aFilter['bind']);
    }

    /**
     * Список работ пользователя (frontend)
     * @param int $nUserID ID пользователя
     * @param array $aFilter фильтр списка работ
     * @param bool $bCount только подсчет кол-ва работ
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function itemsList($nUserID, array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        if( ! isset($aFilter['user_id'])) {
            $aFilter['user_id'] = $nUserID;
        }

        if ($nUserID != User::id()) {
            $aFilter['status'] = Portfolio::STATUS_ACTIVE;
            if (Portfolio::premoderation()) {
                $aFilter[':moderated'] = '(I.moderated > 0)';
            }
        }

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'I');
            return $this->db->one_data('SELECT COUNT(I.id) FROM ' . TABLE_PORTFOLIO_ITEMS . ' I ' . $aFilter['where'], $aFilter['bind']);
        }

        $aFilter[':spec'] = 'I.spec_id = S.id';
        $aFilter[':spec_lang'] = $this->db->langAnd(false, 'S', 'SL');
        $aFilter[':user'] = 'I.user_id = U.user_id';
        $aFilter = $this->prepareFilter($aFilter, 'I');
        if ( ! $sqlOrder) {
            $sqlOrder = 'SU.num, S.num, I.num';
        }

        $aData = $this->db->select('
            SELECT I.id, I.title, I.keyword, I.spec_id, I.cat_id, I.created, I.price, I.price_curr, I.price_rate_text, I.term, I.term_meas, I.num,
                   I.imgcnt, I.preview, I.status, I.user_id, I.moderated, I.blocked_reason,
                   S.keyword AS spec_keyword, SL.title AS spec_title, U.login, U.name, U.surname, U.verified, U.pro
            FROM ' . TABLE_PORTFOLIO_ITEMS . ' I
                LEFT JOIN '.TABLE_PORTFOLIO_SPECS_USERS.' SU ON I.user_id = SU.user_id AND I.spec_id = SU.spec_id
                , '.TABLE_SPECIALIZATIONS.' S, '.TABLE_SPECIALIZATIONS_LANG.' SL, '.TABLE_USERS.' U
            ' . $aFilter['where'] . '
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );

        $aResult = array();
        foreach ($aData as $v) {
            if ( ! isset($aResult[ $v['spec_id'] ])) {
                $aResult[ $v['spec_id'] ] = array();
            }
            if ( ! empty($v['price_rate_text'])) {
                $v['price_rate_text'] = func::unserialize($v['price_rate_text']);
            }
            $aResult[ $v['spec_id'] ][] = $v;
        }

        return $aResult;
    }

    /**
     * Получение данных работы
     * @param integer $nItemID ID работы
     * @param array $aFields
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function itemData($nItemID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT I.*, U.email, U.blocked as user_blocked
                    FROM ' . TABLE_PORTFOLIO_ITEMS . ' I, ' . TABLE_USERS . ' U
                    WHERE I.user_id = U.user_id AND I.id = :id',
                array(':id' => $nItemID)
            );
        } else {
            if (empty($aFields)) {
                $aFields = array('I.*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM ' . TABLE_PORTFOLIO_ITEMS . ' I
                    WHERE I.id = :id',
                array(':id' => $nItemID)
            );
        }
        if( ! empty($aData['video'])){
            $aData['video'] = func::unserialize($aData['video']);
        }
        if( ! empty($aData['price_rate_text'])){
            $aData['price_rate_text'] = func::unserialize($aData['price_rate_text']);
        }

        return $aData;
    }

    /**
     * Сохранение работы
     * @param integer $nItemID ID работы
     * @param array $aData данные работы
     * @param integer $nUserID ID пользователя
     * @return boolean|integer
     */
    public function itemSave($nItemID, array $aData, $nUserID = 0)
    {
        if (empty($aData)) return false;

        if( ! empty($nUserID)){
            $nUserID = User::id();
        }

        if (isset($aData['order']) || isset($aData['order-num'])) {
            $aData['num'] = $this->itemNum($aData, $nUserID);
            unset($aData['order'], $aData['order-num']);
        }

        if ($nItemID > 0)
        {
            if(isset($aData['spec_id'])){
                $aOldData = $this->itemData($nItemID, array('spec_id', 'user_id'));
            }

            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_PORTFOLIO_ITEMS, $aData, array('id'=>$nItemID));

            if(isset($aData['spec_id'])){
                if($aData['spec_id'] != $aOldData['spec_id']){
                    $this->specUserCheck($aOldData['user_id'], $aOldData['spec_id']);
                    $this->specUserCheck($aOldData['user_id'], $aData['spec_id']);
                }
            }

            $mResult = ! empty($res);
        }
        else
        {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nItemID = $this->db->insert(TABLE_PORTFOLIO_ITEMS, $aData);
            if ($nItemID > 0) {
                $this->specUserCheck($aData['user_id'], $aData['spec_id']);
                # обновим рейтинг
                $this->ratingUpdate($aData['user_id'], 'inc', $nItemID);
            }
            $mResult = $nItemID;
        }

        if( $mResult && ! empty($aData['num']) && ! empty($aData['spec_id'])){
            $this->db->exec('
                UPDATE '.TABLE_PORTFOLIO_ITEMS.' SET num = (SELECT @n:=@n+1 FROM (SELECT @n:=0) AS T)
                WHERE user_id = :user_id AND spec_id = :spec_id
                ORDER BY num', array('user_id' => $nUserID, 'spec_id' => $aData['spec_id']));
        }

        return $mResult;
    }

    /**
     * Расчет номера работы
     * @param array $aData данные о работе
     * @param integer $nUserID ID пользователя
     * @return int
     */
    protected function itemNum($aData, $nUserID)
    {
        if(empty($aData['spec_id'])) return 0;
        $nNum = 0;
        switch (intval($aData['order'])) {
            case 1: # первая по счету
                $nNum = 1;
                $this->db->update(TABLE_PORTFOLIO_ITEMS, array('num = num + 1'), array(
                    'user_id' => $nUserID,
                    'spec_id' => $aData['spec_id']
                ));
                break;
            case 2: # последняя
                $nNum = (int)$this->db->one_data(
                        'SELECT MAX(num) FROM '.TABLE_PORTFOLIO_ITEMS.' WHERE user_id = :user_id AND spec_id = :spec_id',
                        array(':user_id' => $nUserID, ':spec_id' => $aData['spec_id'])
                    );
                $nNum++;
                break;
            case 3: # N по счету
                $nNum = $aData['order-num'];
                $this->db->exec('
                    UPDATE '.TABLE_PORTFOLIO_ITEMS.' SET num = num + 1
                    WHERE user_id = :user_id AND spec_id = :spec_id AND num >= :num', array(
                        ':user_id' => $nUserID,
                        ':spec_id' => $aData['spec_id'],
                        ':num' => $nNum,
                    ));
                break;
        }

        return $nNum;
    }

    /**
     * Переключатели работы
     * @param integer $nItemID ID работы
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function itemToggle($nItemID, $sField)
    {
        switch ($sField) {
            case '?': {
                // $this->toggleInt(TABLE_PORTFOLIO_ITEMS, $nItemID, $sField, 'id');
            } break;
        }
    }

    /**
     * Удаление работы
     * @param integer $nItemID ID работы
     * @return boolean
     */
    public function itemDelete($nItemID)
    {
        $aData = $this->itemData($nItemID, array('user_id'));
        if (empty($nItemID)) return false;

        $res = $this->db->delete(TABLE_PORTFOLIO_ITEMS, array('id'=>$nItemID));
        if ( ! empty($res)) {
            $this->userSpecsItemsCount();
            if ( ! empty($aData['user_id'])) {
                # обновим рейтинг
                $this->ratingUpdate($aData['user_id'], 'dec', $nItemID);
            }
            return true;
        }
        return false;
    }

    /**
     * Данные для ссылки "следующая работа" (frontend)
     * @param integer $nItemID ID работы
     * @param integer $nUserID ID пользователя
     * @param string $sCreated дата создания работы
     * @return mixed
     */
    public function itemNext($nItemID, $nUserID, $sCreated)
    {
        $aFilter = array(
            'user_id' => $nUserID,
            ':id' => array('I.id != :id', ':id' => $nItemID),
        );

        if ( ! User::isCurrent($nUserID)) {
            $aFilter['status'] = Portfolio::STATUS_ACTIVE;
            if (Portfolio::premoderation()) {
                $aFilter[':moderated'] = 'I.moderated > 0';
            }
        }
        $aFilterFirst = $aFilter;

        $aFilter[':created'] = array('I.created > :created', ':created' => $sCreated);
        $aFilter = $this->prepareFilter($aFilter, 'I');

        $aData = $this->db->one_array('SELECT I.id, I.keyword
            FROM ' . TABLE_PORTFOLIO_ITEMS . ' I
            '.$aFilter['where'].'
            ORDER BY I.created ASC LIMIT 1', $aFilter['bind']
        );

        if (empty($aData)) {
            $aFilter = $this->prepareFilter($aFilterFirst, 'I');
            $aData = $this->db->one_array('SELECT I.id, I.keyword
                FROM ' . TABLE_PORTFOLIO_ITEMS . ' I
                '.$aFilter['where'].'
                ORDER BY I.created ASC LIMIT 1', $aFilter['bind']
            );
        }

        return $aData;
    }

    /**
     * Данные для ссылки "предыдущая работа" (frontend)
     * @param integer $nItemID ID работы
     * @param integer $nUserID ID пользователя
     * @param string $sCreated дата создания работы
     * @return mixed
     */
    public function itemPrev($nItemID, $nUserID, $sCreated)
    {
        $aFilter = array(
            'user_id' => $nUserID,
            ':id' => array('I.id != :id', ':id' => $nItemID),
        );

        if ( ! User::isCurrent($nUserID)) {
            $aFilter['status'] = Portfolio::STATUS_ACTIVE;
            if (Portfolio::premoderation()) {
                $aFilter[':moderated'] = 'I.moderated > 0';
            }
        }
        $aFilterLast = $aFilter;

        $aFilter[':created'] = array('I.created < :created', ':created' => $sCreated);
        $aFilter = $this->prepareFilter($aFilter, 'I');

        $aData = $this->db->one_array('SELECT I.id, I.keyword
            FROM ' . TABLE_PORTFOLIO_ITEMS . ' I
            '.$aFilter['where'].'
            ORDER BY I.created DESC LIMIT 1', $aFilter['bind']
        );

        if (empty($aData)) {
            $aFilter = $this->prepareFilter($aFilterLast, 'I');
            $aData = $this->db->one_array('SELECT I.id, I.keyword
                FROM ' . TABLE_PORTFOLIO_ITEMS . ' I
                '.$aFilter['where'].'
                ORDER BY I.created DESC LIMIT 1', $aFilter['bind']
            );
        }

        return $aData;
    }

    /**
     * Получаем общее кол-во работ, ожидающих модерации
     * @return integer
     */
    public function itemsModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(I.id)
                FROM ' . TABLE_PORTFOLIO_ITEMS . ' I
                WHERE I.moderated != 1'
        );
    }

    /**
     * Список специализаций пользователя
     * @param integer $nUserID ID пользователя
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @return mixed
     */
    public function specsOwnerList($nUserID, array $aFilter = array(), $bCount = false, $sqlLimit = '') // frontend
    {
        $aFilter[] = 'pid != 0';
        $aFilter[':user'] = array('U.spec_id = S.id AND U.user_id = :user', ':user' => $nUserID);

        if ($nUserID != User::id()) {
            $aFilter[':active'] = 'U.active_cnt > 0';
        } else {
            $aFilter[':active'] = 'U.item_cnt > 0';
        }

        if ( ! $bCount) {
            $aFilter[':lang'] = $this->db->langAnd(false, 'S', 'SL');
        }

        $aFilter = $this->prepareFilter($aFilter, 'S');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM ' . TABLE_PORTFOLIO_SPECS_USERS . ' U, ' . TABLE_SPECIALIZATIONS . ' S ' . $aFilter['where'], $aFilter['bind']);
        }

        $sqlOrder = 'U.num, S.num';
        $aData = $this->db->select_key('
            SELECT S.id, SL.title, U.descr, U.item_cnt, U.active_cnt, U.preview
            FROM ' . TABLE_PORTFOLIO_SPECS_USERS . ' U, ' . TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL
            ' . $aFilter['where'] . '
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, 'id', $aFilter['bind']
        );

        if ( ! empty($aData)) {
            //
        }

        return $aData;
    }

    /**
     * Проверка существования прикрепления раздела к пользователю (при добавлении работы).
     * @param $nUserID integer ID пользователя
     * @param $nSpecID integer ID категории
     */
    protected function specUserCheck($nUserID, $nSpecID)
    {
        $aData = $this->db->one_array('SELECT * FROM '.TABLE_PORTFOLIO_SPECS_USERS.' WHERE user_id = :user AND spec_id = :spec',
            array(
                'user' => $nUserID,
                'spec' => $nSpecID,
            ));
        $nItemCnt = $this->db->one_data('SELECT COUNT(ID) FROM '.TABLE_PORTFOLIO_ITEMS.'
                WHERE user_id = :user AND spec_id = :spec', array('user' => $nUserID, 'spec' => $nSpecID));
        $nActiveCnt = $this->db->one_data('SELECT COUNT(ID) FROM '.TABLE_PORTFOLIO_ITEMS.'
                WHERE user_id = :user AND spec_id = :spec AND status = :active '.(Portfolio::premoderation() ? ' AND moderated > 0 ' : '').'
                ', array('user' => $nUserID, 'spec' => $nSpecID, ':active' => Portfolio::STATUS_ACTIVE));
        if (empty($aData)) {
            $aData = array(
                'user_id'    => $nUserID,
                'spec_id'    => $nSpecID,
                'item_cnt'   => $nItemCnt,
                'active_cnt' => $nActiveCnt,
                'descr'      => '',
                'preview'    => 1, # preview по-умолчанию включаем, при активации PRO не будет необходимости включать принудительно
            );
            $nMaxNum = $this->db->one_data('SELECT MAX(num) FROM '.TABLE_PORTFOLIO_SPECS_USERS.' U
                WHERE U.user_id = :user', array(':user' => $nUserID));
            if ($nMaxNum) {
                $aData['num'] = $nMaxNum + 1;
            }
            $this->db->insert(TABLE_PORTFOLIO_SPECS_USERS, $aData);
        } else {
            if ($nItemCnt != $aData['item_cnt'] || $nActiveCnt != $aData['active_cnt']) {
                $this->db->update(TABLE_PORTFOLIO_SPECS_USERS,
                    array('item_cnt' => $nItemCnt, 'active_cnt' => $nActiveCnt),
                    array('user_id' => $nUserID, 'spec_id' => $nSpecID));
            }
        }
    }

    /**
     * Пересчет счетчика активных работ для разделов пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nSpecID ID раздела
     */
    public function specUserActiveCount($nUserID, $nSpecID)
    {
        $this->db->exec('
            UPDATE '.TABLE_PORTFOLIO_SPECS_USERS.' S
                LEFT JOIN (
                    SELECT I.user_id, I.spec_id, COUNT(I.id) AS cnt
                    FROM '.TABLE_PORTFOLIO_ITEMS.' I
                    WHERE I.status = :status '.(Portfolio::premoderation() ? ' AND I.moderated > 0 ' : '').'
                      AND I.user_id = :user AND I.spec_id = :spec
                    GROUP BY I.user_id, I.spec_id
                ) Ci ON S.user_id = Ci.user_id AND S.spec_id = Ci.spec_id
            SET S.active_cnt = Ci.cnt
            WHERE S.user_id = :user AND S.spec_id = :spec
        ', array(':status' => Portfolio::STATUS_ACTIVE, ':user' => $nUserID, ':spec' => $nSpecID));
    }

    /**
     * Получение данных о настройках раздела пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nSpecID ID раздела
     * @return mixed
     */
    public function specUserData($nUserID, $nSpecID)
    {
        return $this->db->one_array('
            SELECT *
            FROM '.TABLE_PORTFOLIO_SPECS_USERS.' U
            WHERE U.user_id = :user AND U.spec_id = :spec
            ', array(':user' => $nUserID, ':spec' => $nSpecID));
    }

    /**
     * Сохранение данных раздела пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nSpecID ID раздела
     * @param array $aData данные раздела
     * @return bool
     */
    public function specUserSave($nUserID, $nSpecID, $aData)
    {
        if(empty($aData)) return false;
        return $this->db->update(TABLE_PORTFOLIO_SPECS_USERS, $aData, array('user_id' => $nUserID, 'spec_id' => $nSpecID));
    }

    /**
     * Включение заданного порядка сортировки разделов пользователя
     * @param integer $nUserID ID пользователя
     * @return bool
     */
    public function specsUserCheckNum($nUserID)
    {
        $nMaxNum = $this->db->one_data('SELECT MAX(num) FROM '.TABLE_PORTFOLIO_SPECS_USERS.' U WHERE U.user_id = :user', array(':user' => $nUserID));
        if( ! $nMaxNum){
            $this->db->exec('
                UPDATE '.TABLE_PORTFOLIO_SPECS_USERS.' U
                    INNER JOIN '.TABLE_SPECIALIZATIONS.' S ON U.spec_id = S.id
                SET U.num = S.num
                WHERE U.user_id = :user
            ', array(':user' => $nUserID));
            $this->db->exec('
                UPDATE '.TABLE_PORTFOLIO_SPECS_USERS.' SET num = (SELECT @n:=@n+1 FROM (SELECT @n:=0) AS t)
                WHERE user_id = :user
                ORDER BY num;
            ', array(':user' => $nUserID));
            return true;
        }
        return false;
    }

    /**
     * Увеличения номера порядка сортировки разделов пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nNum номер раздела с которого увеличить
     * @return boolean|integer
     */
    public function specsUserNumInc($nUserID, $nNum = 0)
    {
        return $this->db->exec('
                UPDATE '.TABLE_PORTFOLIO_SPECS_USERS.' SET num = num + 1
                WHERE user_id = :user AND num > :num
                ORDER BY num;
            ', array(':user' => $nUserID, ':num' => $nNum));
    }

    /**
     * Расчет количества работ в разделах пользователей
     */
    public function userSpecsItemsCount()
    {
        $this->db->exec('
            UPDATE '.TABLE_PORTFOLIO_SPECS_USERS.' S LEFT JOIN (
                SELECT I.user_id, I.spec_id, COUNT(I.id) AS cnt
                FROM '.TABLE_PORTFOLIO_ITEMS.' I
                GROUP BY I.user_id, I.spec_id
                ) Ci ON S.user_id = Ci.user_id AND S.spec_id = Ci.spec_id
            SET S.item_cnt = Ci.cnt
        ');

        $this->db->exec('
            INSERT INTO '.TABLE_PORTFOLIO_SPECS_USERS.'(spec_id, user_id, num, item_cnt)
                SELECT I.spec_id, I.user_id, 0, COUNT(I.id) AS cnt
                FROM '.TABLE_PORTFOLIO_ITEMS.' I
                    LEFT JOIN '.TABLE_PORTFOLIO_SPECS_USERS.' U ON I.user_id = U.user_id AND I.spec_id = U.spec_id
                WHERE U.user_id IS NULL
                GROUP BY I.user_id, I.spec_id
        ');

        $this->db->exec('
            UPDATE '.TABLE_PORTFOLIO_SPECS_USERS.' S LEFT JOIN (
                SELECT I.user_id, I.spec_id, COUNT(I.id) AS cnt
                FROM '.TABLE_PORTFOLIO_ITEMS.' I
                WHERE I.status = :status '.(Portfolio::premoderation() ? ' AND I.moderated > 0 ' : '').'
                GROUP BY I.user_id, I.spec_id
                ) Ci ON S.user_id = Ci.user_id AND S.spec_id = Ci.spec_id
            SET S.active_cnt = Ci.cnt
        ', array(':status' => Portfolio::STATUS_ACTIVE));

        $this->db->exec('
            UPDATE '.TABLE_USERS_STAT.' S LEFT JOIN (
                SELECT I.user_id, COUNT(I.id) AS cnt
                FROM '.TABLE_PORTFOLIO_ITEMS.' I
                WHERE I.status = :status '.(Portfolio::premoderation() ? ' AND I.moderated > 0 ' : '').'
                GROUP BY I.user_id
                ) Ci ON S.user_id = Ci.user_id
            SET S.portfolio = Ci.cnt
        ', array(':status' => Portfolio::STATUS_ACTIVE));
    }

    public function makeUserWorksCache($nUserID)
    {
        if( ! $nUserID) return;
        $aFilter = array('user_id' => $nUserID);

        $aFilter['status'] = Portfolio::STATUS_ACTIVE;
        if (Portfolio::premoderation()) {
            $aFilter[':moderated'] = 'I.moderated > 0';
        }
        $aFilter[':us'] = 'US.user_id = I.user_id AND US.spec_id = I.spec_id';
        $aFilter[':s'] = 'I.spec_id = S.id';
        $aFilter = $this->prepareFilter($aFilter, 'I');

        $aItems = $this->db->select_key('
            SELECT I.id, I.keyword, I.preview, I.title
            FROM '.TABLE_PORTFOLIO_ITEMS.' I LEFT JOIN '.TABLE_PORTFOLIO_SPECS_USERS.' PSU ON I.user_id = PSU.user_id AND I.spec_id = PSU.spec_id
                , '.TABLE_SPECIALIZATIONS.' S,'.TABLE_USERS_SPECIALIZATIONS.' US
            '.$aFilter['where'].'
            ORDER BY US.main DESC, PSU.num, S.num, I.num
            LIMIT 3
        ', 'id', $aFilter['bind']);

        Users::model()->userSave($nUserID, array('portfolio_works' => serialize($aItems)));
    }

    /**
     * Изменение рейтинга пользователя
     * @param integer $nUserID ID пользователя
     * @param string $sType тип обновления (inc, dec)
     * @param integer $nItemID ID работы
     */
    protected function ratingUpdate($nUserID, $sType, $nItemID)
    {
        $nAmount = (int)$this->db->one_data('SELECT COUNT(*) FROM '.TABLE_PORTFOLIO_ITEMS.' WHERE user_id = :user', array(':user' => $nUserID));
        if ($sType == 'dec') $nAmount++;
        if ($nAmount > config::sysAdmin('users.rating.portfolio.limit', 50, TYPE_UINT)) {
            return;
        }
        $nRating = config::sysAdmin('users.rating.portfolio', 10);
        switch ($sType) {
            case 'inc': # добавили работу
            {
                Users::model()->ratingChange($nUserID, $nRating, 'portfolio-item-add', array('item_id'=>$nItemID));
            } break;
            case 'dec': # удалили работу
            {
                Users::model()->ratingChange($nUserID, -$nRating, 'portfolio-item-del', array('item_id'=>$nItemID));
            } break;
        }
    }

}