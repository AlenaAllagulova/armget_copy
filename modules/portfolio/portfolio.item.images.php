<?php

class PortfolioItemImages_ extends CImagesUploaderTable
{
    /**
     * Константы размеров
     */
    const szForm      = 'f'; # form - в форме редактирования
    const szView      = 'v'; # view - просмотр
    const szOrginal   = 'o'; # original - оригинальное изображение

    function initSettings()
    {
        $this->path    = bff::path('portfolio', 'images');
        $this->pathTmp = bff::path('tmp', 'images');
        $this->url     = bff::url('portfolio', 'images');
        $this->urlTmp  = bff::url('tmp', 'images');

        $this->tableRecords = TABLE_PORTFOLIO_ITEMS;
        $this->tableImages = TABLE_PORTFOLIO_ITEMS_IMAGES;

        $this->folderByID = config::sysAdmin('portfolio.item.images.folderbyid', true, TYPE_BOOL); # раскладываем файлы изображений по папкам (1+=>0, 1000+=>1, ...)
        $this->filenameLetters = config::sysAdmin('portfolio.item.images.filename.letters', 8, TYPE_UINT); # кол-во символов в названии файла
        $this->limit = Portfolio::imagesLimit(); # лимит фотографий у работы
        $this->maxSize = config::sysAdmin('portfolio.item.images.max.size', 10485760, TYPE_UINT); # 2мб (2мб: 2097152, 5мб: 5242880)


        # размеры изображений
        $this->sizes = bff::filter('portfolio.item.images.sizes', array(
            self::szForm   => array(
                'width'    => 122,
                'height'   => false,
                'vertical' => array('width' => false, 'height' => 100)
            ),
            self::szView    => array(
                'width'    => 838,
                'height'   => false
            ),
            self::szOrginal => array('o' => true),
        ));

        # настройки водяного знака
        $watermark = Site::i()->watermarkSettings('portfolio');
        if (!empty($watermark)) {
            $this->sizes[self::szView] += array(
                'watermark'       => true,
                'watermark_src'   => $watermark['file']['path'],
                'watermark_pos_x' => $watermark['pos_x'],
                'watermark_pos_y' => $watermark['pos_y'],
            );
        }

        # размеры изображений, полный URL которых необходимо кешировать
        $this->useFav = false;
    }

    public function urlDefault($sSizePrefix)
    {
        return '';
    }

    /**
     * Получаем дату самого последнего добавленного изображения
     * @param boolean $buildHash сформировать hash на основе даты
     * @return integer|string
     */
    public function getLastUploaded($buildHash = true)
    {
        $lastUploaded = $this->db->one_data('SELECT MAX(created) FROM ' . $this->tableImages . '
                    WHERE ' . $this->fRecordID . ' = :id
                    LIMIT 1', array(':id' => $this->recordID)
        );
        if (!empty($lastUploaded)) {
            $lastUploaded = strtotime($lastUploaded);
        } else {
            $lastUploaded = mktime(0, 0, 0, 1, 1, 2000);
        }

        return ($buildHash ? $this->getLastUploadedHash($lastUploaded) : $lastUploaded);
    }

    /**
     * Формируем hash на основе даты самого последнего добавленного изображения
     * @return integer
     */
    public function getLastUploadedHash($lastUploaded)
    {
        $base64 = base64_encode($lastUploaded);

        return md5(strval($lastUploaded - 1000) . SITEHOST . $base64) . '.' . $base64;
    }

    /**
     * Выполняем проверку, загружались ли новые изображения
     * @param string $lastUploaded hash даты последнего загруженного изображения
     * @return boolean
     */
    public function newImagesUploaded($lastUploaded)
    {
        # проверка hash'a
        if (empty($lastUploaded) || ($dot = strpos($lastUploaded, '.')) !== 32) {
            return true;
        }
        $date = intval(base64_decode(mb_substr($lastUploaded, $dot + 1)));
        if ($this->getLastUploadedHash($date) !== $lastUploaded) {
            return true;
        }
        # выполнялась ли загрузка новых изображений
        if ($this->getLastUploaded(false) > intval($date)) {
            return true;
        }

        return false;
    }

}