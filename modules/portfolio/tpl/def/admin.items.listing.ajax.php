<?php
/**
 * @var $this Portfolio
 */
$urlEdit = $this->adminLink(bff::$event.'&act=edit&id=');
foreach ($list as $k=>&$v): ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small<? if($v['status'] == Portfolio::STATUS_BLOCKED){ ?> clr-error<? } ?>"><?= $v['id'] ?></td>
        <td class="left"><?= $v['title'] ?></td>
        <td>
            <? # для списка "на модерации", указываем причину отправления на модерацию:
            if($f['tab'] == 2) {
                if($v['moderated'] == 0) {
                    if( $v['status'] == Portfolio::STATUS_BLOCKED ) {
                        ?><i class="icon-ban-circle disabled" title="отредактировано пользователем после блокировки"></i><?
                    } else {
                        ?><i class="disabled" title="новая работа"></i><?
                    }
                } elseif($v['moderated'] == 2) {
                    ?><i class="icon-pencil disabled" title="отредактировано пользователем"></i><?
                }
            } ?>
        </td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink"></a>&nbsp;
            <a class="but images<? if(!$v['imgcnt']){ ?> disabled<? } ?>" href="<?= $urlEdit.$v['id'].'&ftab=images' ?>" title="фото: <?= $v['imgcnt'] ?>"></a>
            <a class="but edit item-edit" title="<?= _t('', 'Edit') ?>" href="#" data-id="<?= $v['id'] ?>"></a>
            <a class="but del item-del" title="<?= _t('', 'Delete') ?>" href="#" data-id="<?= $v['id'] ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="5">
            ничего не найдено
        </td>
    </tr>
<? endif;