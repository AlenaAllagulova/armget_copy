<?php
/**
 * @var $this Portfolio
 */
$isOwner = User::isCurrent($user['id']);
$aTermMeas = Portfolio::aTermMeas();
?>
    <div class="p-profileContent">

        <div class="p-profile-title">
            <? if( ! empty($prev['link']) || ! empty($next['link'])): ?>
            <div class="p-prevnext-links">
                <? if( ! empty($prev['link'])): ?><a href="<?= $prev['link'] ?>" class="pull-left"><i class="fa fa-angle-left c-link-icon"></i><span><?= _t('portfolio', 'Предыдущая работа'); ?></span></a><? endif; ?>
                <? if( ! empty($next['link'])): ?><a href="<?= $next['link'] ?>" class="pull-right"><span><?= _t('portfolio', 'Следующая работа'); ?></span><i class="fa fa-angle-right c-link-icon"></i></a><? endif; ?>
            </div>
            <div class="clearfix"></div>
            <? endif; ?>
            <h5 class="text-center"><?= $title ?></h5>
            <div class="p-portfolioSection-opt text-center">
                <? if( ! empty($price)): ?><span><i class="fa fa-money"></i> <?= tpl::formatPrice($price) ?> <?= Site::currencyData($price_curr, 'title_short') ?><?= ! empty($price_rate_text[LNG]) ? ' '.$price_rate_text[LNG] : '' ?></span><? endif; ?>
                <? if( ! empty($term)): ?><span><i class="fa fa-clock-o"></i> <?= tpl::declension($term, $aTermMeas[ $term_meas ]['decl'] ) ?></span><? endif; ?>
            </div>
        </div>

        <div class="p-portfolioSection">

            <? if($isOwner):?>
                <?= $this->itemStatusBlock($aData); ?>

                <div class="p-profileOrder-controls-mobile">
                    <a href="<?= Portfolio::url('edit', array('id' => $id)) ?>"><i class="fa fa-edit c-link-icon"></i></a>
                    <a href="#" class="link-delete j-item-delete-<?= $id ?>"><i class="fa fa-times c-link-icon"></i></a>
                </div>

                <div class="p-profileOrder-controls">
                    <a href="<?= Portfolio::url('edit', array('id' => $id)) ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                    <a href="#" class="link-delete j-item-delete-<?= $id ?>"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
                </div>
                <script type="text/javascript">
                    <? js::start(); ?>
                    $(function(){
                        $('.j-item-delete-<?= $id ?>').click(function(){
                            if(confirm('<?= _t('portfolio', 'Удалить работу?') ?>')){
                                bff.ajax(bff.ajaxURL('portfolio', 'item-delete'), {id:<?= $id ?>, hash: app.csrf_token}, function(resp, errors){
                                    if(resp && resp.success) {
                                        document.location = '<?= Portfolio::url('my.portfolio'); ?>';
                                    } else {
                                        app.alert.error(errors);
                                    }
                                });
                            }
                            return false;
                        });
                    });
                    <? js::stop(); ?>
                </script>
            <? endif; ?>

            <p class="p-portfolioSection-description"><?= nl2br($descr)?></p>
            <? if( ! empty($link)): $aUrl = parse_url($link);
                ?><a href="<?= $link ?>" rel="nofollow noopener" target="_blank"><?= ! empty($aUrl['host']) ? $aUrl['host'] : $link ?></a><?
               endif; ?>

            <div class="p-work-snapshots">

                <? if( ! empty($video['html'])): ?>
                <div class="l-video-container">
                    <?= $video['html'] ?>
                </div>
                <? endif; ?>

                <? if( ! empty($images)): ?>
                <div class="p-work-item">
                    <? foreach($images as $k => $v): ?>
                        <img src="<?= $v['i']['v'] ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" /><br /><br />
                    <? endforeach; ?>
                </div>
                <? endif; ?>
            </div>

            <div class="p-portfolioSection-bottom">

                <div class="c-base-date p-portfolioSection-bottom-date">
                    <?= _t('portfolio', 'Работа добавлена:'); ?> <span><?= $created = tpl::date_format_pub($created, 'd.m.y') ?></span><?
                    $modified = tpl::date_format_pub($modified, 'd.m.y');
                    if($modified != $created): ?>,
                        <?= _t('portfolio', 'изменена:'); ?> <span><?= $modified ?></span>
                    <? endif; ?>
                </div>

                <?  $aConfig = $this->configLoad();
                if( ! empty($aConfig['share_code'])): ?>
                    <?= $aConfig['share_code'] ?>
                <?  endif; ?>
            </div>


        </div>

    </div>