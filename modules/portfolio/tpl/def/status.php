<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <? if (Portfolio::premoderation()) { # включена премодерация ?>
                    <h1 class="small"><?= _t('portfolio', 'Ваша работа будет опубликована после проверки менеджером') ?></h1>
                    <p>
                        <?= _t('portfolio', 'Ваша работа сохранена, но еще не опубликована.<br /> Вы можете увидеть и отредактировать ее в <a [link_cabinet]>вашем кабинете</a>.',
                                array('link_cabinet' => 'href="' . Portfolio::url('my.portfolio', array('spec' => $spec_id)) . '"')) ?><br />
                        <?= _t('portfolio', 'Как только наш менеджер проверит корректность введенных вами данных, работа появится в портфолио.') ?><br />
                    </p>
                    <p><a href="<?= Portfolio::url('add') ?>"><?= _t('portfolio', 'Добавить еще одну работу') ?></a></p>
                    <p><a href="<?= Portfolio::url('my.portfolio') ?>"><?= _t('portfolio', 'Перейти на страницу списка работ') ?></a></p>
                <? } else { ?>
                    <h1 class="small"><?= _t('portfolio', 'Ваша работа успешно опубликована') ?></h1>
                    <p><?= _t('portfolio', 'Чтобы увидеть ее в общем списке работ перейдите на <a [link_portfolio]>страницу портфолио</a>.', array('link_portfolio' => 'href="' . Portfolio::url('my.portfolio') . '"')) ?><br />
                    <?= _t('portfolio', 'Вы также можете отредактировать ее в <a [link_cabinet]>вашем кабинете</a>.',
                            array('link_cabinet' => 'href="' . Portfolio::url('my.portfolio', array('spec' => $spec_id)) . '"')
                        ) ?><br /></p>
                    <p><a href="<?= Portfolio::url('add') ?>"><?= _t('portfolio', 'Добавить еще одну работу') ?></a></p>
                <? } ?>
            </div>
        </div>
    </section>
</div>