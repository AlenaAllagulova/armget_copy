<?php
/**
 * @var $this Portfolio
 * @var $user array
 */
$bShowPreview = ($user['pro'] || ! bff::servicesEnabled() || ! Portfolio::previewOnlyPro());
$aTermMeas = Portfolio::aTermMeas();

if( ! empty($list)):
    foreach($list as $k => $v): ?>
        <div class="p-portfolioSection j-spec">
            <h5><?= $specs[$k]['title'] ?></h5>

            <? $bDescr = ! empty($specs[$k]['descr']); ?>
            <div class="<?= ! $bDescr ? 'hidden ' : '' ?>j-descr"><p> <?= $bDescr ? nl2br($specs[$k]['descr']) : '' ?> </p></div>

            <? if($user['my']): ?>
                <div class="p-profileOrder-controls-mobile j-edit">
                    <a href="#" class="j-spec-edit" data-id="<?= $k ?>"><i class="fa fa-edit c-link-icon"></i></a>
                </div>

                <div class="p-profileOrder-controls  j-edit">
                    <a href="#" class="j-spec-edit" data-id="<?= $k ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('portfolio', 'Редактировать раздел'); ?></a>
                </div>
            <? endif; ?>

            <? if($bShowPreview && $specs[$k]['preview']): ?>
                <div class="row">
                    <? $cnt = 0; foreach($v as $vv):
                        $bDisabled = ($vv['status'] != Portfolio::STATUS_ACTIVE || (Portfolio::premoderation() && !$vv['moderated']));
                        ?>
                        <div class="col-sm-4<?= $bDisabled ? ' disabled ' : '' ?>">
                            <a href="<?= Portfolio::url('user.view', array('id' => $vv['id'], 'keyword' => $vv['keyword'], 'login' => $vv['login'])) ?>" class="p-portfolioSection-work">
                                <div class="p-portfolioSection-img">
                                    <img src="<?= PortfolioItemPreview::url($vv['id'], $vv['preview']); ?>" alt="<?= tpl::imageAlt(array('t' => $vv['title'])); ?>" />
                                </div>
                                <?= $vv['title']; ?>
                                <div class="p-portfolioSection-opt">
                                    <? if($vv['price']): ?><span><i class="fa fa-money"></i> <?= tpl::formatPrice($vv['price']) ?> <?= Site::currencyData($vv['price_curr'], 'title_short') ?><?= ! empty($vv['price_rate_text'][LNG]) ? ' '.$vv['price_rate_text'][LNG] : '' ?></span><? endif; ?>
                                    <? if($vv['term']): ?><span><i class="fa fa-clock-o"></i> <?= tpl::declension($vv['term'], $aTermMeas[ $vv['term_meas'] ]['decl'] ) ?></span><? endif; ?>
                                </div>
                            </a>
                            <? if($user['my']): ?>
                                <a href="<?= Portfolio::url('edit', array('id' => $vv['id'])) ?>" class="p-edit-work-icon"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                                <?= $this->itemStatusBlock($vv); ?>
                            <? endif; ?>
                        </div>
                        <? $cnt++; if( ($cnt % 3) == 0 ):?></div><div class="row"><? endif; ?>
                    <? endforeach; ?>
                </div>
            <? else: ?>
                <ol class="p-portfolio-list">
                <?  foreach($v as $vv):
                    $bDisabled = ($vv['status'] != Portfolio::STATUS_ACTIVE || (Portfolio::premoderation() && !$vv['moderated']));
                    ?>
                    <li<?= $bDisabled ? ' class="disabled"' : '' ?>><a href="<?= Portfolio::url('user.view', array('id' => $vv['id'], 'keyword' => $vv['keyword'], 'login' => $vv['login'])) ?>"><?= $vv['title']; ?></a><?
                        if($vv['price']): ?><span class="c-highlited"><?= tpl::formatPrice($vv['price']) ?> <?= Site::currencyData($vv['price_curr'], 'title_short') ?><?= ! empty($vv['price_rate_text'][LNG]) ? ' '.$vv['price_rate_text'][LNG] : '' ?></span><? endif; ?><?
                        if($vv['term']): ?><span><?= tpl::declension($vv['term'], $aTermMeas[ $vv['term_meas'] ]['decl'] ) ?></span><? endif; ?><?
                        if($user['my']): ?><a href="<?= Portfolio::url('edit', array('id' => $vv['id'])) ?>" class="p-edit-work-icon"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a><?= $this->itemStatusBlock($vv); ?><? endif;
                    ?></li>
                <? endforeach; ?>
                </ol>
            <? endif; ?>
        </div>
    <? endforeach; ?>
<? endif;