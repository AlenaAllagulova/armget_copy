<?php
?>
<td class="row1"><span class="field-title"><?= ! empty($priceSett['price_title']) ? $priceSett['price_title'] : _t('portfolio', 'Стоимость'); ?>:</span></td>
<td class="row2">
    <input class="" type="text" name="price" value="<?= $price ?>" style="width: 50px;" maxlength="10" />
    <select name="price_curr" style="width: 70px;"><?= Site::currencyOptions( ! empty($price_curr) ? $price_curr : Site::currencyDefault('id')) ?></select>
    <? if( ! empty($priceSett['rates'])): ?>
        <select name="price_rate" style="width: 150px;"><?= HTML::selectOptions($priceSett['rates'], ! empty($price_rate) ? $price_rate : 0, false); ?></select>
    <? endif; ?>
</td>
