<?php
/**
 * @var $this Portfolio
 */

?>
<?= tplAdmin::blockStart(_t('portfolio', 'Portfolio').' / Работы / Добавление', false, array('id'=>'PortfolioItemsFormBlock','style'=>'display:none;')); ?>
<div id="PortfolioItemsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('portfolio', 'Portfolio').' / Работы', true, array('id'=>'PortfolioItemsListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array(),
    array(
        array('title'=>_t('', 'пересчет счетчиков работ пользователей'), 'onclick'=>"return bff.confirm('sure', {r:'".$this->adminLink(bff::$event.'&act=dev-users-counters-update')."'})", 'icon'=>'icon-refresh', 'debug-only'=>true),
    )
); ?>
<?
$aTabs = array(
    0 => array('t'=>_t('portfolio','Опубликованные')),
    2 => array('t'=>_t('portfolio','На модерации'), 'counter'=>config::get('portfolio_items_moderating', 0)),
    3 => array('t'=>_t('portfolio','Заблокированные')),
    4 => array('t'=>_t('portfolio','Все')),
);
?>
<div class="tabsBar">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab <? if($f['tab']==$k) { ?>tab-active<? } ?>"><a href="#" onclick="return jPortfolioItemsList.onTab(<?= $k ?>,this);" <?= (!empty($v['c']) ? $v['c'] : '') ?>><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
    <? } ?>
    <div id="PortfolioItemsProgress" class="progress" style="display: none;"></div>
</div>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="PortfolioItemsListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="order" value="<?= $f['order'] ?>" />
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>" />
        <input type="hidden" name="spec_id" value="<?= $f['spec_id'] ?>" />
        <input type="hidden" name="cat_id" value="<?= $f['cat_id'] ?>" />

        <div class="left">
            <div class="left" style="margin-left:8px;">
                <? foreach($specsOptions as $lvl => $v):
                ?><select class="spec-select" data-lvl="<?= $lvl ?>" style="margin-right: 5px; width: 150px; <? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jPortfolioItemsList.onSpecialization($(this))"><?= $v['categories'] ?></select><?
                endforeach; ?>
            </div>
            <div class="left" style="margin-left:8px;">
                <input style="width:175px;" type="text" maxlength="150" name="title" placeholder="<?= _t('portfolio', 'ID / Заголовок работы'); ?>" value="<?= HTML::escape($f['title']) ?>" />
                <input style="width:155px;" type="text" maxlength="150" name="user" placeholder="<?= _t('', 'ID / Логин / E-mail пользователя') ?>" value="<?= HTML::escape($f['user']) ?>" />
                <input type="button" class="btn btn-small button cancel" style="margin-left: 8px;" onclick="jPortfolioItemsList.submit(false);" value="<?= _t('', 'search') ?>" />
            </div>
            <div class="left" style="margin-left: 8px;"><a class="ajax cancel" onclick="jPortfolioItemsList.submit(true); return false;"><?= _t('', 'reset') ?></a></div>
            <div class="clear"></div>
        </div>
        <div class="right"></div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="PortfolioItemsListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">
            <a href="javascript: jPortfolioItemsList.onOrder('id');" class="ajax">ID</a>
            <div class="order-<?= $f['order_dir'] ?>" <? if($f['order_by']!='id') { ?>style="display:none;"<? } ?> id="items-order-id"></div>
        </th>
        <th class="left"><?= _t('', 'Title') ?></th>
        <th></th>
        <th width="105">
            <a href="javascript: jPortfolioItemsList.onOrder('created');" class="ajax"><?= _t('', 'Created') ?></a>
            <div class="order-<?= $f['order_dir'] ?>" <? if($f['order_by']!='created') { ?>style="display:none;"<? } ?> id="items-order-created"></div>
        </th>
        <th width="135"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="PortfolioItemsList">
    <?= $list ?>
    </tbody>
</table>
<div id="PortfolioItemsListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
var jPortfolioItemsFormManager = (function(){
    var $progress, $block, $blockCaption, $formContainer, process = false;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $formContainer = $('#PortfolioItemsFormContainer');
        $progress = $('#PortfolioItemsProgress');
        $block = $('#PortfolioItemsFormBlock');
        $blockCaption = $block.find('span.caption');

        <? if( ! empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jPortfolioItemsList.toggle(false);
            if(jPortfolioItemsForm) jPortfolioItemsForm.onShow();
        } else {
            jPortfolioItemsList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? '<?= _t('portfolio', 'Portfolio') ?> / Работы / Добавление' : '<?= _t('portfolio', 'Portfolio') ?> / Работы / Редактирование'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
            } else {
                jPortfolioItemsList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        <? $tab = $this->input->get('ftab', TYPE_NOTAGS); ?>
        params = $.extend(params || {}, {act:type<?= ! empty($tab) ? ',ftab:\''.$tab.'\'' : '' ?>});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jPortfolioItemsList =
    (function()
    {
        var $progress, $block, $list, $listTable, $listPgn, $filters, filters, processing = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';
        var tab = '<?= $f['tab'] ?>';
        var orders = <?= func::php2js($orders) ?>, orderby = '<?= $f['order_by'] ?>';
        var specCache = {};

        $(function(){
            $progress  = $('#PortfolioItemsProgress');
            $block     = $('#PortfolioItemsListBlock');
            $list      = $block.find('#PortfolioItemsList');
            $listTable = $block.find('#PortfolioItemsListTable');
            $listPgn   = $block.find('#PortfolioItemsListPgn');
            $filters   = $block.find('#PortfolioItemsListFilters');
            filters    = $filters.get(0);

            $list.delegate('a.item-edit', 'click', function(){
                var id = intval($(this).data('id'));
                if(id>0) jPortfolioItemsFormManager.action('edit',id);
                return false;
            });

            $list.delegate('a.item-toggle', 'click', function(){
                var id = intval($(this).data('id'));
                var type = $(this).data('type');
                if(id>0) {
                    var params = {progress: $progress, link: this};
                    bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                }
                return false;
            });

            $list.delegate('a.item-del', 'click', function(){
                var id = intval($(this).data('id'));
                if(id>0) del(id, this);
                return false;
            });

            $(window).bind('popstate',function(e){
                if('state' in window.history && window.history.state === null) return;
                var loc = document.location;
                var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                if( actForm!=null ) {
                    var actId = /id=([\d]+)/.exec(loc.search.toString());
                    jPortfolioItemsFormManager.action(actForm[1], actId && actId[1]);
                } else {
                    jPortfolioItemsFormManager.action('cancel');
                    updateList(false);
                }
            });

        });

        function isProcessing()
        {
            return processing;
        }

        function del(id, link)
        {
            bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
            return false;
        }

        function updateList(updateUrl)
        {
            if(isProcessing()) return;
            var f = $(filters).serialize();
            bff.ajax(ajaxUrl, f, function(data){
                if(data) {
                    $list.html( data.list );
                    $listPgn.html( data.pgn );
                    if(updateUrl !== false && bff.h) {
                        window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                    }
                }
            }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
        }

        function setPage(id)
        {
            filters.page.value = intval(id);
        }

        function specView(data, $select)
        {
            if(data === 'empty') {
                return;
            }

            if(data.subs>0) {
                $select.after('<select class="spec-select" data-lvl="'+data.lvl+'" style="margin-right: 5px; width:150px;" onchange="jPortfolioItemsList.onSpecialization($(this))">'+data.cats+'</select>').show();
                return;
            }
        }

        return {
            submit: function(resetForm)
            {
                if(isProcessing()) return false;
                setPage(1);
                if(resetForm) {
                    filters['title'].value = '';
                    filters['user'].value = '';
                    filters['cat_id'].value = '0';
                    filters['spec_id'].value = '0';
                    var $sel = $filters.find('.spec-select:first');
                    $sel.val(0);
                    jPortfolioItemsList.onSpecialization($sel);
                }
                updateList();
            },
            page: function (id)
            {
                if(isProcessing()) return false;
                setPage(id);
                updateList();
            },
            onOrder: function(by)
            {
                if(isProcessing() || !orders.hasOwnProperty(by))
                    return;

                orders[by] = (orders[by] == 'asc' ? 'desc' : 'asc');
                $('#items-order-'+orderby).hide();
                orderby = by;
                $('#items-order-'+orderby).removeClass('order-asc order-desc').addClass('order-'+orders[by]).show();

                filters.order.value = orderby+'-'+orders[by];
                setPage(1);

                updateList();
            },
            onTab: function(tabNew, link)
            {
                if(isProcessing() || tabNew == tab) return false;
                setPage(1);
                tab = filters.tab.value = tabNew;
                updateList();
                $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
                return false;
            },
            refresh: function(resetPage, updateUrl)
            {
                if(resetPage) setPage(0);
                updateList(updateUrl);
            },
            toggle: function(show)
            {
                if(show === true) {
                    $block.show();
                    if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                }
                else $block.hide();
                updateList(false);
            },
            onSpecialization: function($select)
            {
                specView('empty');

                var catID = intval($select.val());

                var lvl = $select.data('lvl');
                if(lvl == 'spec'){
                    filters['spec_id'].value = catID;
                } else {
                    filters['cat_id'].value = catID;
                    $select.nextAll().remove();

                    if(specCache.hasOwnProperty(catID)) {
                        specView( specCache[catID], $select );
                    } else {
                        bff.ajax('<?= $this->adminLink('specializations_list&act=category-data', 'specializations'); ?>', {'cat_id': catID, 'lvl':$select.data('lvl')}, function(data){
                            if(data && data.success) {
                                specView( (specCache[catID] = data), $select );
                            }
                        }, function(){
                            $progress.toggle();
                        });
                    }
                }
                updateList();

            }
        };
    }());
</script>