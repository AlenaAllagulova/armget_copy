<?php
if( ! empty($priceSett)):
?>
<div class="form-group o-propose-inputs">
    <label for="price" class="col-sm-3 control-label"><?= ! empty($priceSett['price_title']) ? $priceSett['price_title'] : _t('portfolio', 'Стоимость'); ?></label>
    <div class="col-sm-9">
        <div class="input-group">
            <input type="text" name="price" id="price" class="form-control input-sm" value="<?= $price ?>" maxlength="10" />
            <select class="form-control input-sm" name="price_curr"><?= Site::currencyOptions( ! empty($price_curr) ? $price_curr : ( ! empty($priceSett['curr']) ? $priceSett['curr'] : Site::currencyDefault('id') )) ?></select>
    <? if( ! empty($priceSett['rates'])): ?>
            <select class="form-control input-sm" name="price_rate"><?= HTML::selectOptions($priceSett['rates'], ! empty($price_rate) ? $price_rate : 0, false); ?></select>
    <? endif; ?>
        </div>
    </div>
</div>
<? endif;
