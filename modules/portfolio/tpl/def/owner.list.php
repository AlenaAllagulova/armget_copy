<?php
/**
 * Портфолио пользователя
 * @var $this Portfolio
 */
tpl::includeJS('portfolio.owner.list', false, 2);
?>
    <div class="p-profileContent" id="j-portfolio-owner-list">

        <form method="get" action="" id="j-portfolio-owner-list-form">
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            <input type="hidden" name="spec" value="<?= $f['spec'] ?>" />

            <div class="p-profile-title">
                <div class="dropdown pull-left">
                    <a href="#" class="ajax-link" data-toggle="dropdown"><span class="j-f-spec-title"><?= isset($specs[ $f['spec'] ]) ? $specs[ $f['spec'] ]['title'] : '' ?></span><b class="caret"></b></a>
                    <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                        <? foreach($specs as $v): ?>
                            <li><a href="#" class="j-f-spec-select" data-id="<?= $v['id'] ?>"><?= $v['title'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <? if($user['my']): ?>
                    <div class="pull-right">
                        <a class="btn btn-primary btn-sm" href="<?= Portfolio::url('add'); ?>"><i class="fa fa-plus-circle"></i> <?= _t('portfolio', 'Добавить работу') ?></a>
                        <a class="btn btn-default btn-sm" href="<?= Portfolio::url('my.settings'); ?>"><i class="fa fa-cog"></i></a>
                    </div>
                <? endif; ?>
                <div class="clearfix"></div>
            </div>
        </form>

        <? if( ! empty($portfolio_intro)): ?><p><?= nl2br($portfolio_intro) ?></p><? endif; ?>

        <div class="j-list"><?= $list ?></div>

        <div class="j-pagination"><?= $pgn ?></div>

        <? if( ! $count): ?><div class="alert alert-info"><?= _t('portfolio', 'Работы не найдены'); ?></div><? endif; ?>

    </div>
<script type="text/javascript">
    <? $aSpec = array();
        foreach($specs as $v){
            $aSpec[ $v['id'] ] = array( 'id' => $v['id'], 't' => $v['title']);
        }
        js::start(); ?>
    jPortfolioOwnerList.init(<?= func::php2js(array(
        'lang'  => array(
            'item_delete'   => _t('portfolio', 'Удалить работу?'),
            'spec_save_success' => _t('portfolio', 'Настройки раздела сохранены'),
        ),
        'specs' => $aSpec,
        'ajax'  => true,
    )) ?>);
    <? js::stop(); ?>
</script>