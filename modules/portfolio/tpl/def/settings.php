<?php
/**
 * Форма настроек магазина
 * @var $this Portfolio
 */
tpl::includeJS('portfolio.settings.form', false);
$aData = HTML::escape($aData, 'html', array('portfolio_intro'));
?>
<div class="container">

    <section class="l-mainContent">
        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <?= tpl::getBreadcrumbs(array(
                    array('title' => _t('','Profile'),'link'=>Users::url('my.profile')),
                    array('title' => _t('portfolio', 'Portfolio'),'link'=>Portfolio::url('my.portfolio')),
                    array('title' => _t('', 'Settings'),'active'=>true),
                )); ?>

                <div class="p-profileContent">

                    <div class="p-profile-title">
                        <h4><?= _t('', 'Settings'); ?></h4>
                    </div>


                    <form class="form-horizontal" action="" method="post" id="j-portfolio-settings-form">

                        <div class="p-portfolioSection">
                            <h5><?= _t('portfolio', 'Вводный текст'); ?></h5>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea rows="5" class="form-control" name="portfolio_intro"><?= $portfolio_intro ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mrgt30">
                            <div class="col-sm-9 c-formSubmit">
                                <button class="btn btn-primary c-formSuccess"><?= _t('form', 'Сохранить изменения'); ?></button>
                                <a class="c-formCancel" href="<?= Portfolio::url('my.portfolio'); ?>"><?= _t('form', 'Отмена'); ?></a>
                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    </section>

</div>
<script type="text/javascript">
    <? js::start() ?>
    jPortfolioSettingsForm.init(<?= func::php2js(array(
        'lang' => array(
            'saved_success' => _t('portfolio', 'Настройки успешно сохранены'),
        ),
        'url_settings' => Portfolio::url('my.settings'),
    )) ?>);
    <? js::stop() ?>
</script>