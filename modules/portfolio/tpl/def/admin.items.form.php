<?php
/**
 * @var $this Portfolio
 */
$aData = HTML::escape($aData, 'html', array('title', 'descr','link','price','term'));
$aData['edit'] = $edit = ! empty($id);
$tab = $this->input->getpost('ftab', TYPE_NOTAGS);
$aTabs = array(
    'info'   => _t('portfolio','Описание'),
    'images' => _t('portfolio','Изображения'),
);
if( ! isset($aTabs[$tab])) {
    $tab = 'info';
}

?>
<div class="tabsBar">
    <?php foreach($aTabs as $k=>$v): ?>
        <span class="tab<?= ($k==$tab ? ' tab-active' : '') ?>"><a href="#" onclick="jPortfolioItemsForm.onTab('<?= $k ?>', this); return false;"><?= $v ?></a></span>
    <?php endforeach; ?>
    <div class="progress" style="margin-left: 5px; display: none;" id="form-progress"></div>
</div>

<div id="portfolio-form-block-info" class="hidden">
<form name="PortfolioItemsForm" id="PortfolioItemsForm" action="<?= $this->adminLink(null) ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <table class="admtbl tbledit">
        <tr class="required check-select">
            <td class="row1 field-title" width="100"><?= _t('portfolio', 'Категория') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <select name="spec_id" style="width: 350px;"><?= HTML::selectOptions($specs, $spec_id, false, 'spec_id', 't') ?></select>
            </td>
        </tr>
        <tr class="required">
            <td class="row1" width="110"><span class="field-title"><?= _t('', 'Title') ?><span class="required-mark">*</span>:</span></td>
            <td class="row2">
                <input class="stretch" type="text" id="item-title" name="title" value="<?= $title ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1"><span class="field-title">Описание:</span></td>
            <td class="row2">
                <textarea id="item-descr" name="descr" rows="5"><?= $descr ?></textarea>
            </td>
        </tr>
        <tr>
            <td class="row1"><span class="field-title">Ссылка:</span></td>
            <td class="row2">
                <input class="stretch" type="text" name="link" value="<?= $link ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1"><span class="field-title">Ссылка на видео:</span></td>
            <td class="row2">
                <input class="stretch" type="text" name="video" value="<?= ! empty($video['orig_url']) ? HTML::escape($video['orig_url']) : '' ?>" />
            </td>
        </tr>
        <tr class="j-price"><?= $priceForm ?></tr>
        <tr>
            <td class="row1"><span class="field-title">Сроки:</span></td>
            <td class="row2">
                <input type="text" name="term" value="<?= $term ?>" style="width: 50px;" maxlength="5" />
                <select name="term_meas" style="width: 100px;"><?= HTML::selectOptions(Portfolio::aTermMeas(), ! empty($term_meas) ? $term_meas : 0, false, 'id', 't'); ?></select>
            </td>
        </tr>

        <? $oPreview = $this->itemPreview($id);
        foreach($oPreview->getVariants() as $iconField=>$v):
            $oPreview->setVariant($iconField);
            $icon = $v;
            $icon['uploaded'] = ! empty($aData[$iconField]);
        ?>
        <tr>
            <td class="row1" width="100">
                <span class="field-title"><?= $icon['title'] ?></span>:<? if(sizeof($v['sizes']) == 1) { $sz = current($v['sizes']); ?><br /><span class="desc"><?= ($sz['width'].'x'.$sz['height']) ?></span><? } ?>
            </td>
            <td class="row2">
                <input type="file" name="<?= $iconField ?>" <? if($icon['uploaded']){ ?>style="display:none;" <? } ?> />
                <? if($icon['uploaded']) { ?>
                    <div style="margin:5px 0;">
                        <input type="hidden" name="<?= $iconField ?>_del" class="del-icon" value="0" />
                        <img src="<?= $oPreview->getURL($aData[$iconField], PortfolioItemPreview::szOriginal) ?>" alt="" /><br />
                        <a href="#" class="ajax desc cross but-text" onclick="return jPortfolioItemsForm.previewDelete(this);">удалить</a>
                    </div>
                <? } ?>
            </td>
        </tr>
        <? endforeach; ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'User') ?></td>
            <td class="row2">
                <a href="#" class="ajax" onclick="return bff.userinfo(<?= $user_id ?>);"><?= $email ?></a>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2">
                <?= $this->viewPHP($aData, 'admin.items.form.status'); ?>
            </td>
        </tr>

        <tr class="footer">
            <td colspan="2">
                <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jPortfolioItemsForm.save(false);" />
                <? if ($edit) { ?><input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jPortfolioItemsForm.save(true);" /><? } ?>
                <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jPortfolioItemsFormManager.action('cancel');" />
            </td>
        </tr>
    </table>
</form>
</div>

<div id="portfolio-form-block-images" class="hidden"><?= $this->viewPHP($aData, 'admin.items.form.images'); ?></div>

<script type="text/javascript">
    var jPortfolioItemsForm =
        (function(){
            var $progress, $form, formChk, id = parseInt(<?= $id ?>), blocksPrefix = 'portfolio-form-block-';
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';
            var returnToList = false;

            $(function(){
                $progress = $('#PortfolioItemsFormProgress');
                $form = $('#PortfolioItemsForm');

                var $price = $form.find('.j-price');
                $form.on('change', '[name="spec_id"]', function(){
                    var $el = $(this);
                    bff.ajax(ajaxUrl+'&act=price-form', {id:$el.val()}, function(data){
                        if(data.hasOwnProperty('html')){
                            $price.html(data.html);
                        }
                    });
                });

                bff.iframeSubmit($form, function(data){
                    if(data && data.success) {
                        bff.success('Данные успешно сохранены');
                        if(returnToList || ! id) {
                            jPortfolioItemsFormManager.action('cancel');
                            jPortfolioItemsList.refresh( ! id);
                        }
                    }
                },{
                    url: ajaxUrl,
                    progress: $progress,
                    beforeSubmit:function(){
                        if( ! formChk.check(true) ) return false;
                        return true;
                    }
                });

                initBlock('<?= $tab ?>', false);
            });

            function initBlock(key, $block)
            {
                if($block === false) {
                    $block = $('#'+blocksPrefix+key);
                }
                $block.addClass('inited').removeClass('hidden');
            }

            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('Запись успешно удалена');
                                jPortfolioItemsFormManager.action('cancel');
                                jPortfolioItemsList.refresh();
                            }});
                    }
                },
                save: function(retToList)
                {
                    if( ! formChk.check(true) ) return false;
                    returnToList = retToList;
                },
                onShow: function ()
                {
                    formChk = new bff.formChecker($form);
                },
                previewDelete: function(link){
                    var $block = $(link).parent();
                    $block.hide().find('input.del-icon').val(1);
                    $block.prev().show();
                    return false;
                },
                onTab: function(key, tabLink)
                {
                    $('[id^="'+blocksPrefix+'"]').addClass('hidden');
                    var $block = $('#'+blocksPrefix+key).removeClass('hidden');
                    if( ! $block.hasClass('inited')) {
                        initBlock(key, $block);
                    }
                    $(tabLink).parent().addClass('tab-active').siblings().removeClass('tab-active');
                    if(bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act=<?= $act ?>&id=<?= $id ?>&ftab=' + key);
                    }
                }

            };
        }());
</script>