<?php
/**
 * Форма работы (добавление / редактирование)
 * @var $this Portfolio
 */
tpl::includeJS(array('autocomplete', 'qquploader', 'ui.sortable'), true);
tpl::includeJS('portfolio.item.form', false, 2);
$aData = HTML::escape($aData, 'html', array('title','descr','link','price','term'));

if($edit){
    $formTitle = _t('portfolio', 'Редактировать работу');
    $url = Portfolio::url('edit', array('id'=>$id));
} else {
    $formTitle = _t('portfolio', 'Добавить работу в портфолио');
    $url = Portfolio::url('add');
}
?>
    <div class="container">

        <section class="l-mainContent">
            <div class="row">

                <div class="col-md-8 col-md-offset-2">

                <?= tpl::getBreadcrumbs(array(
                    array('title' => _t('','Profile'),'link'=>Users::url('my.profile')),
                    array('title' => _t('portfolio', 'Portfolio'),'link'=>Portfolio::url('my.portfolio')),
                    array('title' => $formTitle,'active'=>true),
                )); ?>

                    <div class="p-profileContent">

                        <div class="p-profile-title">

                            <h4><?= $formTitle; ?></h4>

                        </div>

                        <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" id="j-item-form">

                            <div class="p-portfolioSection">

                                <div class="form-group j-required">
                                    <label for="pname" class="col-sm-3 control-label"><?= _t('portfolio', 'Название'); ?> <i class="text-danger">*</i></label>
                                    <div class="col-sm-7">
                                        <input type="text" name="title" class="form-control" id="pname"  value="<?= $title ?>" placeholder="<?= _t('portfolio', 'Введите название работы'); ?>" autocomplete="off" />
                                  </div>
                                </div>

                                <div class="form-group">
                                    <label for="pposition" class="col-sm-3 control-label"><?= _t('portfolio', 'Позиция'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="form-inline">

                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input name="order" value="1" type="radio" <?= ! $edit ? 'checked="checked"' : ''?> />
                                                    <?= _t('portfolio', 'Первая'); ?>
                                                </label>
                                                <label class="radio-inline">
                                                    <input name="order" value="2" type="radio" />
                                                    <?= _t('portfolio', 'Последняя'); ?>
                                                </label>
                                                <div class="visible-xs"></div>
                                                <label class="radio-inline p-position-last_radio">
                                                    <input name="order" value="3" type="radio" class="j-order-num" <?= $edit ? 'checked="checked"' : ''?>>
                                                </label>
                                                <label class="radio-inline p-input-sm">
                                                    <input name="order-num" type="text" class="form-control input-sm" value="<?= ! empty($num) ? $num : '' ?>"> <?= _t('portfolio', 'по счету'); ?>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= _t('portfolio', 'Раздел'); ?> <i class="text-danger">*</i></label>
                                    <div class="col-sm-9 p-form-noinput">
                                        <div class="dropdown">

                                          <a href="#" id="dLabel" class="dropdown-toggle ajax-link" data-toggle="dropdown"><span class="j-spec-title"><?= $spec_id ? $specs[ $spec_id ]['t'] : _t('portfolio', 'Выбрать раздел'); ?></span> <i class="fa fa-caret-down"></i></a>
                                          <input type="hidden" name="spec_id" value="<?= $spec_id ?>" class="j-spec-select" />
                                          <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                                            <? foreach($specs as $v): ?>
                                            <li<?= $v['a'] ? ' class="active"' : '' ?>><a href="#" class="j-spec" data-id="<?= $v['spec_id'] ?>"><?= $v['t'] ?></a></li>
                                            <? endforeach; ?>
                                          </ul>
                                      </div>
                                  </div>
                                </div>

                                <div class="form-group j-required">
                                    <label for="pdescr" class="col-sm-3 control-label"><?= _t('portfolio', 'Описание'); ?>  <i class="text-danger">*</i></label>
                                    <div class="col-sm-7">
                                        <textarea rows="10" class="form-control" name="descr" id="pdescr" placeholder="<?= _t('portfolio', 'Описание работы'); ?>"><?= $descr ?></textarea>
                                  </div>
                                </div>

                                <div class="form-group">
                                    <label for="plink" class="col-sm-3 control-label"><?= _t('portfolio', 'Ссылка'); ?></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="link" id="plink" placeholder="<?= _t('portfolio', 'Внешняя ссылка на работу'); ?>" value="<?= $link ?>" />
                                  </div>
                                </div>

                                <div class="form-group">
                                    <label for="pvideolink" class="col-sm-3 control-label"><?= _t('portfolio', 'Ссылка на видео'); ?></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="video" id="pvideolink" placeholder="<?= _t('portfolio', 'Внешняя ссылка на видео'); ?>"  value="<?= ! empty($video['orig_url']) ? HTML::escape($video['orig_url']) : '' ?>" />
                                        <div class="clearfix"></div>
                                        <div class="help-block"><?= _t('form', 'Youtube, Rutube, Vimeo'); ?></div>
                                    </div>
                                </div>

                            </div>

                            <div class="p-portfolioSection">
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <h5><?= _t('portfolio', 'Изображения'); ?></h5>
                                    </div>
                                </div>

                                <input type="file" name="preview" id="j-preview-file" value="" style="display: none;"/>
                                <? if( ! empty($preview)): ?>
                                    <div class="form-group">
                                        <label for="pvideolink" class="col-sm-3 control-label"><?= _t('portfolio', 'Превью'); ?></label>

                                        <div class="col-sm-3">
                                            <div class="o-propose-works p-propose-works">
                                                <div class="o-project-thumb p-portfolio-project-thumb">
                                                    <div class="o-inner added">
                                                        <img src="<?= PortfolioItemPreview::url($id, $preview) ?>" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 p-form-noinput">
                                            <button class="btn btn-default btn-sm j-preview-add"><?= _t('portfolio', 'Загрузить другое изображение'); ?></button>
                                            <span class="j-file-name"></span>

                                            <div class="help-block">
                                                <?= _t('portfolio', 'Максимальный размер изображения - [size] пикселей', array('size' => $preview_sizes)); ?>
                                            </div>
                                        </div>
                                    </div>
                                <? else: ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?= _t('portfolio', 'Превью'); ?></label>

                                        <div class="col-sm-6">
                                            <button class="btn btn-primary btn-sm j-preview-add"><?= _t('', 'Загрузить'); ?></button>
                                            <span class="j-file-name"></span>

                                            <span class="help-block">
                                                <?= _t('portfolio', 'Максимальный размер изображения - [size] пикселей', array('size' => $preview_sizes)); ?>
                                            </span>
                                        </div>
                                    </div>
                                <? endif; ?>

                                <div class="form-group j-images">
                                    <input type="hidden" name="images_type" value="ajax" class="j-images-type-value" />
                                    <input type="hidden" name="images_hash" value="<?= $imghash ?>" />
                                    <label class="col-sm-3 control-label nowrap"><?= _t('portfolio', 'Изображения работы'); ?></label>

                                    <div class="col-sm-9">

                                        <div id="p-gallery-show" class="j-images-type j-images-type-ajax">

                                            <ul class="p-addWork-gallery j-img-slots">

                                                <? for($i = 1; $i <= Portfolio::imagesLimit(); $i++): ?>
                                                    <li class="j-img-slot">
                                                        <a class="p-hasimg hidden j-img-preview">
                                                            <div class="p-galleryImg-container">
                                                                <img class="j-img-img" src="" alt="" />
                                                            </div>
                                                        </a>
                                                        <a href="#" class="p-galleryImg-remove link-delete hidden j-img-delete j-img-preview"><i class="fa fa-times-circle-o"></i></a>
                                                        <a class="j-img-upload">
                                                            <div class="p-galleryImg-container">
                                                                <i class="fa fa-plus-circle j-img-link"></i>
                                                            </div>
                                                        </a>
                                                        <input type="hidden" name="" value="" class="j-img-fn" />
                                                    </li>
                                                <? endfor; ?>

                                                <div class="clearfix"></div>
                                            </ul>

                                            <div class="help-block">
                                                <?= _t('form', 'Вы можете сортировать изображения просто перетаскивая их'); ?>
                                            </div>

                                            <?= _t('form', 'Если у вас возникли проблемы воспользуйтесь <a [a_simple]><span>альтернативной формой</span></a>', array('a_simple' => 'href="#" class="ajax-link j-images-toggler" data-type="simple"')); ?>

                                        </div>

                                        <div id="p-gallery" class="p-gallery-alternate hide j-images-type j-images-type-simple">
                                            <? for($i = 1; $i <= Portfolio::imagesLimit() - $imgcnt; $i++): ?>
                                                <div><input name="images_simple_<?= $i ?>" type="file" /></div>
                                            <? endfor; ?>

                                            <a href="#" class="ajax-link c-formCancel j-images-toggler" data-type="ajax"><span><?= _t('portfolio', 'Удобная форма загрузки изображений'); ?></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="p-portfolioSection">

                                <div class="j-price">
                                <?= $priceForm ?>
                                </div>

                                <div class="form-group o-propose-inputs">
                                    <label for="terms" class="col-sm-3 control-label"><?= _t('portfolio', 'Сроки'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="text" id="terms" name="term" class="form-control input-sm" value="<?= $term ?>" maxlength="5" />
                                            <select class="form-control input-sm" name="term_meas"><?= HTML::selectOptions(Portfolio::aTermMeas(), ! empty($term_meas) ? $term_meas : 0, false, 'id', 't'); ?></select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group c-formSubmit mrgt30">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button class="btn btn-primary c-formSuccess j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= $edit ? _t('form', 'Сохранить') : _t('portfolio', 'Добавить работу'); ?></button>
                                    <a href="#" class="c-formCancel j-cancel"><?= _t('form', 'Отмена'); ?></a>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
<script type="text/javascript">
<? js::start() ?>
jPortfolioItemForm.init(<?= func::php2js(array(
    'lang' => array(
        'saved_success'       => _t('portfolio', 'Работа успешно сохранена'),
        'spec_wrong'          => _t('portfolio', 'Укажите раздел'),
        'upload_typeError'    => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
        'upload_sizeError'    => _t('form', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
        'upload_minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_emptyError'   => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_limitError'   => _t('form', 'Вы можете загрузить не более {limit} файлов'),
        'upload_onLeave'      => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
    ),
    'url'         => $url,
    'itemID'      => $id,
    'imgLimit'    => Portfolio::imagesLimit(),
    'imgMaxSize'  => $img->getMaxSize(),
    'imgUploaded' => $imgcnt,
    'imgData'     => $images,
    'cancelUrl'   => Portfolio::url('my.portfolio'),
)) ?>);
<? js::stop() ?>
</script>