<?php
/**
 * Форма редактирования раздела
 * @var $this Portfolio
 */
$aData = HTML::escape($aData, 'html', array('descr'));
?>
<form class="p-portfolioSection-edit" action="" method="post" id="j-owner-spec-form-<?= $spec_id ?>">
    <input type="hidden" name="id" value="<?= $spec_id ?>" />
    <div class="form-group">
        <textarea class="form-control" name="descr" rows="3"><?= ! empty($descr) ? $descr : '' ?></textarea>
    </div>
    <? if( ! empty($specs)): ?>
        <div class="form-inline">
            <div class="form-group">
                <label class="radio">
                    <input type="radio" name="order" value="1"> <?= _t('portfolio', 'Первый по счету'); ?>
                </label>
            </div>
            <div class="form-group">
                <label class="radio">
                    <input type="radio" name="order" value="2" class="j-after-check"> <?= _t('portfolio', 'После другого раздела'); ?>
                </label>
            </div>
            <div class="form-group">
                <select class="form-control j-after-select" name="cat_after"><?= HTML::selectOptions($specs, 0, false, 'id', 'title'); ?></select>
            </div>
        </div>
    <? endif; ?>
    <? if( ! bff::servicesEnabled() || $pro || ! Portfolio::previewOnlyPro()): ?>
    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="preview" value="1" <?= $preview ? ' checked="checked"' : '' ?> />
                <?= _t('portfolio', 'Превью работ включены'); ?>
            </label>
        </div>
    </div>
    <? endif; ?>
    <div class="c-formSubmit">
        <button class="btn btn-primary c-formSuccess btn-sm j-submit"><?= _t('form', 'Сохранить'); ?></button>
        <a href="#" class="ajax-link j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
    </div>
</form>