<?php

class M_Portfolio_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('portfolio', 'Portfolio');
        $module = 'portfolio';

        # Список
        if ($security->haveAccessToModuleToMethod($module, 'items') ||
            $security->haveAccessToModuleToMethod($module, 'items-moderate')) {
            $menu->assign($menuTitle, 'Работы', $module, 'listing', true, 10,
                array( 'counter' => 'portfolio_items_moderating'));
            $menu->adminHeaderCounter($menuTitle, 'portfolio_items_moderating', $module, 'listing&tab=2', 5, '', array('parent'=>'moderation'));
        }
        # Настройки
        if ($security->haveAccessToModuleToMethod($module, 'settings')) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 20);
        }
    }
}