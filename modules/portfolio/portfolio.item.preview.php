<?php

class PortfolioItemPreview_ extends CImageUploader
{
    # варианты иконок
    const PREVIEW    = 'preview';

    # ключи размеров
    const szOriginal = 'o'; # оригинальный размер

    function initSettings()
    {
        $this->path    = bff::path('portfolio', 'images');
        $this->pathTmp = bff::path('tmp', 'images');
        $this->url     = bff::url('portfolio', 'images');
        $this->urlTmp  = bff::url('tmp', 'images');

        $this->table = TABLE_PORTFOLIO_ITEMS;
        $this->fieldID = 'id';
        $this->filenameLetters = config::sysAdmin('portfolio.item.preview.filename.letters', 4, TYPE_UINT);
        $aVariants = $this->getVariants();
        if( ! empty($aVariants) ) {
            $this->setVariant( key($aVariants) );
        }
    }

    static function url($nItemID, $sFilename, $sVariantKey = self::szOriginal)
    {
        static $i;
        if( ! isset($i)) {
            $i = new  PortfolioItemPreview();
        }
        $i->setRecordID($nItemID);

        if( empty($sFilename) ) {
            # иконка-заглушка
            return $i->url . 'default-'.$sVariantKey.'.png';
        } else {
            return $i->getURL($sFilename, $sVariantKey);
        }
    }

    function getVariants()
    {
        return bff::filter('portfolio.item.preview.sizes', array(
            self::PREVIEW => array(
                'title'=>_t('portfolio', 'Превью'),
                'key'=>self::PREVIEW,
                'sizes'=>array(
                    self::szOriginal => array('width'=>200, 'height'=>200, 'o'=>true),
                ),
            ),
        ));
    }

    function setVariant($sKey)
    {
        $aVariants = $this->getVariants();
        if( isset($aVariants[$sKey]) ) {
            $this->fieldImage = $sKey;
            $this->sizes = $aVariants[$sKey]['sizes'];
        }
    }
}