<?php

class Svc_ extends SvcBase
{
    /**
     * Страница с описанием всех услуг
     */
    public function index()
    {
        if ( ! bff::servicesEnabled()) {
            $this->errors->error404();
        }

        $aData = array(
            'svc' => array(),
        );

        $aSvc = Svc::model()->svcListing(Svc::TYPE_SERVICE);
        foreach ($aSvc as $v) {
            if ($v['on'] && bff::moduleExists($v['module'])) {
                $aData['svc'][ $v['module'] ][ $v['keyword'] ] = $v;
            }
        }

        # SEO: Услуги
        $url = static::url('list', array(), true);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $this->seo()->setPageMeta(Site::i(), 'services', array(), $aData);
        if (empty($aData['titleh1'])) $aData['titleh1'] = _t('svc', 'Платные сервисы');

        $aData['breadcrumbs'] = array();
        if (User::id()) {
            $aData['breadcrumbs'][] = array('title' => _t('','Profile'), 'link' => Users::url('my.profile'));
        }
        $aData['breadcrumbs'][] = array('title' => $aData['titleh1'], 'active' => true);

        return $this->viewPHP($aData, 'index');
    }

    /**
     * Страница с описанием / формой услуги
     */
    public function view()
    {
        $sKeyword = $this->input->get('keyword', TYPE_NOTAGS);
        switch ($sKeyword) {
            case 'pro':
                return Users::i()->svc_pro(); break;
            case 'carousel':
                return Users::i()->svc_carousel(); break;
            case 'stairs':
                return Users::i()->svc_stairs(); break;
        }

        $this->errors->error404();
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cron' => array('period' => '0 0 * * *'),
        );
    }

}