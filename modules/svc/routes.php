<?php

return [
    # главная
    'svc-index' => [
        'pattern'  => 'services(/|)',
        'callback' => 'svc/index',
        'priority' => 480,
    ],
    # просмотр
    'svc-view' => [
        'pattern'  => 'services/(.+)',
        'callback' => 'svc/view/keyword=$1',
        'priority' => 490,
    ],
];