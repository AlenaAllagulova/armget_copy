<?php

class Svc_ extends SvcBase
{
    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        $aLangFields = array(
            'description' => TYPE_STR,
        );

        if (Request::isAJAX()) {

            $aData = array();
            /*
            $this->input->postm(array(
            ), $aData);
            */


            $this->input->postm_lang($aLangFields, $aData);
            $this->db->langFieldsModify($aData, $aLangFields, $aData);
            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad();
        $this->db->langFieldsSelect($aData, $aLangFields);
        if( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }}