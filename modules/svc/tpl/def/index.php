<?php
    /**
     * @var $this Svc
     */
   $isUser = User::id();
   $useClient = Users::useClient();
   $isWorker = $isUser && User::isWorker();
?>
    <div class="container">

        <section class="l-mainContent">

            <?= tpl::getBreadcrumbs($breadcrumbs); ?>

            <h1 class="small"><?= $titleh1; ?></h1>

            <?= config::get('svc_description_'.LNG, '') ?>

            <? if($useClient): ?><h4><?= _t('svc', 'Услуги для исполнителя'); ?></h4><? endif; ?>

            <? if( ! empty($svc['users'])): $oIcon = Users::i()->svcIcon(); ?>
            <div class="se-services-list">
                <div class="row">
                    <? foreach($svc['users'] as $k => $v): if( ! $v['on']) continue;
                        $url = ( $isWorker ? Svc::url('view', array('keyword' => $v['keyword'])) : false); ?>
                        <div class="col-sm-4 se-services-item">
                            <a class="se-service-icon"<? if($url) { ?> href="<?= $url ?>"<? } ?>>
                                <img src="<?= $oIcon->url($v['id'], $v['icon_s']) ?>" alt="<?= tpl::imageAlt(array('t' => $v['title_view'][LNG])); ?>" />
                            </a>
                            <div class="media-body">
                                <? if ($url) { ?>
                                    <a href="<?= $url ?>" class="se-service-name"><?= $v['title_view'][LNG] ?></a>
                                <? } else { ?>
                                    <span class="se-service-name"><?= $v['title_view'][LNG] ?></span>
                                <? } ?>
                                <?= $v['description'][LNG] ?>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
            <? endif; ?>

            <? if( ! empty($svc['shop'])): $oIcon = Shop::i()->svcIcon(); ?>
            <h6><?= _t('svc', 'Продвижение товара в магазине'); ?></h6>
            <div class="se-services-list">
                <div class="row">
                    <? foreach($svc['shop'] as $k => $v): if( ! $v['on']) continue; ?>
                        <div class="col-sm-4 se-services-item">
                            <a class="se-service-icon">
                                <img src="<?= $oIcon->url($v['id'], $v['icon_s']) ?>" alt="<?= tpl::imageAlt(array('t' => $v['title_view'][LNG])); ?>" />
                            </a>
                            <div class="media-body">
                                <span class="se-service-name"><?= $v['title_view'][LNG] ?></span>
                                <?= $v['description'][LNG] ?>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
            <? endif; ?>


            <? if( ! empty($svc['orders'])): $oIcon = Orders::i()->svcIcon(); ?>

            <? if($useClient): ?><h4><?= _t('svc', 'Услуги для заказчика'); ?></h4><? endif; ?>

            <div class="se-services-list">
                <div class="row">

                    <? $i = 0; foreach($svc['orders'] as $k => $v): if( ! $v['on']) continue; ?>
                        <? if($i && ($i%3) == 0): ?>
                            </div>
                            <div class="row">
                        <? endif; $i++; ?>
                    <div class="col-sm-4 se-services-item">
                        <a class="se-service-icon">
                            <img src="<?= $oIcon->url($v['id'], $v['icon_s']) ?>" alt="<?= tpl::imageAlt(array('t' => $v['title_view'][LNG])); ?>" />
                        </a>
                        <div class="media-body">
                            <span class="se-service-name"><?= $v['title_view'][LNG] ?></span>
                            <?= $v['description'][LNG] ?>
                        </div>
                    </div>
                    <? endforeach; ?>
            </div>
            <? endif; ?>

        </section>

    </div>
