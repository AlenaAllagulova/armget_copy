<?php
$aTabs = array(
    'general'  => 'Общие',
);
?>
<?= tplAdmin::blockStart('Счет и услуги / '._t('','Settings'), false, array('id'=>'SvcSettingsFormBlock')); ?>

<form name="SvcSettingsForm" id="SvcSettingsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="edit" />
    <input type="hidden" name="save" value="1" />
    <div class="tabsBar" id="SvcSettingsFormTabs">
        <? foreach($aTabs as $k=>$v) { ?>
            <span class="tab<? if($k == 'general') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
        <? } ?>
    </div>
    <!-- таб: Поделиться -->
    <div class="j-tab j-tab-info">
        <table class="admtbl tbledit">
            <?= $this->locale->buildForm($aData, 'svcconfig', '
                <tr>
                    <td class="row1 field-title" width="100">Описание на странице услуг:</td>
                    <td class="row2"><?= tpl::jwysiwyg($aData[\'description\'][$key], \'description[\'.$key.\']\', 0, 115); ?></td>
                </tr>
            '); ?>
        </table>
    </div>

    <div style="margin-top: 10px;">
        <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jSvcSettingsForm.save(false);" />
        <div id="SvcSettingsFormProgress" class="progress" style="display: none;"></div>
        <div class="clearfix"></div>
    </div>

</form>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
    var jSvcSettingsForm =
        (function(){
            var $progress, $form, f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#SvcSettingsFormProgress');
                $form = $('#SvcSettingsForm');
                f = $form.get(0);

                // tabs
                $form.find('#SvcSettingsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });
            });

            return {
                save: function()
                {
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                        }
                    }, $progress);
                }
            };
        }());
</script>