<?php

abstract class SvcBase_ extends SvcModule
{
    /** @var SvcModel */
    public $model = null;

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Список услуг
            case 'list':
                $url .= '/services/'. (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Просмотр услуги
            case 'view':
                $index = static::url('list');
                if (empty($opts['keyword'])) {
                    $url = $index;
                    break;
                }
                $keyword = $opts['keyword'];
                unset($opts['keyword']);

                $url = $index.$keyword . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
        }

        return bff::filter('svc.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Активируем услугу/пакет услуг
     * @param string $sModuleName название модуля, в котором выполняется активация услуги
     * @param integer $nSvcID ID услуги / пакета услуг
     * @param array|mixed $aSvcData данные об активируемой услуге/пакете услуг
     * @param integer $nItemID ID записи
     * @param integer $nUserID ID пользователя
     * @param integer|boolean $nSvcPrice стоимость услуги / пакета услуг или FALSE (получить из настроек)
     * @param integer|boolean $fMoney стоимость услуги / пакета услуг для оплаты или FALSE (получить из настроек)
     * @param array $aSvcSettings
     * @return array
     */
    function activate($sModuleName, $nSvcID, $aSvcData = array(), $nItemID, $nUserID,
                      $nSvcPrice = false, $fMoney = false, array &$aSvcSettings = array())
    {
        $sUserError = _t('', 'Ошибка активации услуги');

        do {
            try {
                $oModule = bff::module($sModuleName);
            } catch (\Exception $e) {
                bff::log($e->getMessage());
                $this->errors->set($sUserError);
                break;
            }

            if (!$oModule instanceof IModuleWithSvc) {
                bff::log('Модуль "' . $sModuleName . '" должен реализовать интерфейс "IModuleWithSvc" [svc::activate]');
                $this->errors->set($sUserError);
                break;
            }

            if (empty($aSvcSettings) && empty($aSvcData)) {
                $aSvcData = $this->model->svcData($nSvcID);
            }

            if ((empty($aSvcData) || $aSvcData['module'] != $sModuleName) && empty($aSvcSettings)) {
                bff::log('Активируемая услуга не связана с модулем "' . $sModuleName . '" [svc::activate]');
                $this->errors->set($sUserError);
                break;
            }
            if ($nSvcPrice === false) {
                if( ! empty($aSvcData['price'])){
                    $nSvcPrice = $aSvcData['price'];
                }
                if( ! empty($aSvcSettings['price'])){
                    $nSvcPrice = $aSvcSettings['price'];
                }
            }
            if ($fMoney === false) {
                $fMoney = $nSvcPrice;
            }
            $nUserBalance = $this->users()->model->userBalance($nUserID);
            $mSuccess = $oModule->svcActivate($nItemID, $nSvcID, $aSvcData, $aSvcSettings);
            if ($mSuccess !== false) {
                if ($mSuccess === 2) {
                    # 1) создаем закрытый счет активации услуги без списывания средств
                    $this->bills()->createBill_OutService($nSvcID, $nItemID, $nUserID, $nUserBalance,
                        0, 0, Bills::STATUS_COMPLETED, $oModule->svcBillDescription($nItemID, $nSvcID, false, $aSvcSettings),
                        $aSvcSettings
                    );
                } else {
                    # проверка необходимости дополнительного анализа созданного счета
                    $afterBillCreate = false;
                    if (isset($aSvcSettings['afterBillCreate'])) {
                        $afterBillCreate = $aSvcSettings['afterBillCreate'];
                        unset($aSvcSettings['afterBillCreate']);
                    }

                    # 1) создаем закрытый счет активации услуги
                    $nBillID = $this->bills()->createBill_OutService($nSvcID, $nItemID, $nUserID, ($nUserBalance - $nSvcPrice),
                        $nSvcPrice, $fMoney, Bills::STATUS_COMPLETED, $oModule->svcBillDescription($nItemID, $nSvcID, false, $aSvcSettings),
                        $aSvcSettings
                    );
                    # 2) снимаем деньги со счета пользователя
                    $this->bills()->updateUserBalance($nUserID, $nSvcPrice, false);

                    # обновим рейтинг пользователю
                    if ($nBillID) {
                        Users::i()->svc_rating_money($nUserID, $nSvcPrice);
                    }
                    # дополнительный анализ созданного счета
                    if (is_callable($afterBillCreate)) {
                        $afterBillCreate($nBillID);
                    }
                }

                return true;
            }

        } while (false);

        return false;
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('svc', 'images') => 'dir', # изображения платных услуг
        ));
    }

}