<?php

class M_Site_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $module = 'site';
        $menuTitle = _t('menu','Настройки сайта');

        # Страницы
        if ($security->haveAccessToModuleToMethod('site-pages', 'listing')) {
            $menu->assign('Страницы', 'Список страниц', $module, 'pageslisting', true, 1,
                array('rlink' => array('event' => 'pagesAdd'))
            );
            $menu->assign('Страницы', 'Добавить cтраницу', $module, 'pagesAdd', false, 2);
            $menu->assign('Страницы', 'Редактирование cтраницы', $module, 'pagesEdit', false, 3);
        }

        # Настройки сайта
        if ($security->haveAccessToModuleToMethod($module, 'siteconfig')) {
            $menu->assign($menuTitle, 'Общие настройки', $module, 'siteconfig', true, 2);
        }

        # Системные настройки
        if (config::sysAdminEnabled() && $security->isSuperAdmin()) {
            $menu->assign($menuTitle, 'Системные настройки', $module, 'settingsSystemManager', true, 3);
        }

        # Дополнения (плагины и темы)
        $menu->assign($menuTitle, 'Дополнения', 'site', 'extensionsManager', true, 4, array(
            'access' => 'extensions',
        ));

        # Обновления
        $menu->assign($menuTitle, _t('dev', 'Обновления'), 'dev', 'updatesManager', true, 5, array(
            'access' => 'updates',
        ));

        # Инструкции
        //$menu->assign($menuTitle, 'Инструкции', $module, 'instructions', true, 5);

        # Счетчики
        if ($security->haveAccessToModuleToMethod($module, 'counters')) {
            $menu->assign($menuTitle, 'Счетчики', $module, 'counters', true, 10,
                array('rlink' => array('event' => 'counters&act=add'))
            );
        }

        # Валюты
        if ($security->haveAccessToModuleToMethod($module, 'currencies')) {
            $menu->assign($menuTitle, 'Валюты', $module, 'currencies', true, 15);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module,'seo')) {
            $menu->assign('SEO', 'Общие настройки', $module, 'seo_templates_edit', true, 5);
        }
    }
}