<section class="l-mainContent">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1><?= $title ?></h1>
                <?= $content ?>
            </div>
        </div>
    </div>
</section>