<?php
?>
<div class="in-slider hidden-xs">
    <? if( ! empty($images)): ?>
    <ul class="in-slider-slides" id="j-index-header-slider">
        <? foreach($images as $v): ?>
        <li style="background-image: url(<?= $v['link'] ?>)"></li>
        <? endforeach; ?>
    </ul>
    <? endif; ?>
    <div class="container">
        <div class="in-slider-box">
            <div class="in-slider-box-inside">
                <? $title = config::get('site_index_view_title_'.LNG, ''); ?>
                <? if(!empty($title)) { ?><p class="in-slider-box-title"><?= $title; ?></p><? } ?>
                <? $description = config::get('site_index_view_desc_'.LNG, ''); ?>
                <? if(mb_strlen(strip_tags(str_replace('&nbsp;', '', $description))) > 0) { ?>
                <div class="in-slider-box-descr"><?= $description ?></div>
                <? } ?>
                <a href="<?= Orders::url('add'); ?>" class="in-slider-box-btn">
                    <? $btn = config::get('site_index_view_button_'.LNG, false); echo (!empty($btn) ? $btn : _t('site', 'Опубликовать заказ')); ?>
                </a>
            </div>
            <? if( ! empty($stat)): ?>
            <div class="in-slider-box-inside hidden-xs">
                <p class="in-slider-box-subtitle"><?= _t('site', 'Статистика портала:'); ?></p>
                <ul class="in-slider-box-list">
                    <? foreach($stat as $v): ?>
                    <li><i class="<?= $v['icon'] ?>"></i> <?= $v['title'] ?>: <span><?= $v['data'] ?></span></li>
                    <? endforeach; ?>
                </ul>
            </div>
            <? endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
<? js::start() ?>
$(function(){
    var block = $('#j-index-header-slider');
    var period = 2000;
    if (block.find('li').length > 1) {
        block.find('li:gt(0)').hide();
        setInterval(function(){
            block.find('li:first')
                .fadeOut(period).next()
                .fadeIn(period).end()
                .appendTo(block);
        }, period*2);
    }
});
<? js::stop() ?>
</script>