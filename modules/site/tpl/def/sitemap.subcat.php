<?php
?>
    <div class="row">
    <? $cnt = 0; foreach ($cats as $v): ?>
        <? if ($cnt && $cnt >2): $cnt = 0; ?>
            </div><!-- /.row -->
            <div class="row">
        <? endif; $cnt++; ?>
        <div class="col-sm-4">
            <div class="l-sitemap">
                <? if ( ! empty($v['title'])):
                    if ( ! empty($v['url'])): ?>
                    <h4><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></h4>
                    <? else: ?>
                    <h4><?= $v['title'] ?></h4>
                <? endif;
                endif; ?>
                <ul>
                    <? foreach($v['sub'] as $vv): ?>
                    <li><a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a></li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    <? endforeach; ?>
    </div>
