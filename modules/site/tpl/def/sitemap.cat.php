<?php
?>
    <div class="row">
        <div class="col-sm-4">
            <div class="l-sitemap">
                <ul>
                    <? $count = count($cats)/3; $cnt = 0; foreach ($cats as $v): ?>
                    <? if ($cnt && $cnt > $count): $cnt = 0; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="l-sitemap">
                                <ul>
                    <? endif; $cnt++; ?>
                    <li><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    </div>