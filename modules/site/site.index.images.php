<?php

class SiteIndexImages_ extends CImagesUploaderField
{
    /**
     * Константы размеров
     */
    const szView    = 'v'; # view - просмотр
    const szOrginal = 'o'; # original - оригинальное изображение

    function initSettings()
    {
        $this->path    = bff::path('index', 'images');
        $this->pathTmp = bff::path('index', 'images');
        $this->url     = bff::url('index',  'images');
        $this->urlTmp  = bff::url('index',  'images');

        $this->folderByID = config::sysAdmin('site.index.images.folderbyid', true, TYPE_BOOL);
        $this->filenameLetters = config::sysAdmin('site.index.images.filename.letters', 10, TYPE_UINT);
        $this->maxSize = config::sysAdmin('site.index.images.max.size', 5242880, TYPE_UINT); # 2мб (2мб: 2097152, 5мб: 5242880)
        $this->limit = config::sysAdmin('site.index.images.limit', 15, TYPE_UINT);

        $this->sizes = bff::filter('site.index.images.sizes', array(
            self::szView    => array('width' => 100, 'height' => 100),
            self::szOrginal => array('o' => true),
        ));
    }

    /**
     * Получаем данные о записи
     * @param integer $recordID ID записи
     * @return array
     */
    protected function loadRecordData($recordID)
    {
        $data = array();
        $data[$this->field_images] = func::unserialize(config::get('site_index_images'));
        $data[$this->field_count]  = sizeof($data[$this->field_images]);
        return $data;
    }

    /**
     * Сохраняем данные о записи
     * @param integer $recordID ID записи
     * @param array $recordData данные
     * @return mixed
     */
    protected function saveRecordData($recordID, array $recordData)
    {
        if (isset($recordData[$this->field_images]) && is_array($recordData[$this->field_images])) {
            config::save('site_index_images', serialize($recordData[$this->field_images]), true);
        }
    }

}