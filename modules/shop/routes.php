<?php

return [
    # просмотр товара
    'shop-view' => [
        'pattern'  => 'shop/([\d]+)\-(.*).html',
        'callback' => 'shop/view/id=$1',
        'priority' => 60,
    ],
    # действия с товаром
    'shop-action' => [
        'pattern'  => 'shop/(add|edit|promote|status)',
        'callback' => 'shop/$1/',
        'priority' => 70,
    ],
    # поиск по категории
    'shop-search-cat' => [
        'pattern'  => 'shop/(.+)',
        'callback' => 'shop/search/cat=$1',
        'priority' => 80,
    ],
    # главная
    'shop-search' => [
        'pattern'  => 'shop(/|)',
        'callback' => 'shop/search',
        'priority' => 90,
    ],
];