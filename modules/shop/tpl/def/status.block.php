<?php
/**
 * @var $this Shop
 */
?>
<? if($moderated != 1 && Shop::premoderation()): ?><div class="alert alert-warning" role="alert"><?= _t('shop', 'Ожидает проверки менеджера') ?></div><? endif; ?>
<? if($status == Shop::STATUS_BLOCKED): ?>
    <div class="alert alert-danger" role="alert"><b><?= _t('shop', 'Заблокирован менеджером.') ?></b>
    <? if( ! empty($blocked_reason)): ?><br /><?= _t('', 'Причина блокировки:'); ?> <?= tpl::blockedReason($blocked_reason) ?><? endif; ?>
    </div>
<? endif; ?>