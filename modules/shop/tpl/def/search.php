<?php
/**
 * Список товаров
 * @var $this Shop
 */

tpl::includeJS('shop.search', false, 3);

?>
    <div class="container">

        <section class="l-mainContent" id="j-shop-search">
            <div class="row">

                <!-- Left Column -->
                <aside class="col-md-3">

                    <? if(User::id() && (User::isWorker() || !Users::useClient())): ?>
                    <div class="l-searchAdd">
                        <div class="add-button">
                            <a href="<?= Shop::url('add')?>" class="btn btn-primary btn-block"><i class="fa fa-plus-circle hidden-sm"></i> <?= _t('shop', 'Добавить товар'); ?></a>
                        </div>
                    </div>
                    <? endif; ?>

                    <?= $form ?>

                    <? # Баннер: Магазин: список ?>
                    <?= Banners::view('shop_list', array('pos'=>'left', 'cat'=>$cat_id)) ?>

                </aside>

                <div class="col-md-9 l-content-column" id="j-shop-search-list">

                    <div class="l-search-bar">
                        <form method="get" role="search" action="<?= Shop::url('list') ?>">
                            <input type="search" name="q" class="form-control" placeholder="<?= _t('shop', 'Найти товар'); ?>">
                            <button class="l-search-button"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <div class="l-list-heading">
                        <h6><?= _t('orders', 'Найдено'); ?> <span class="j-products-count"><?= $count ?></span></h6>
                        <div class="clearfix"></div>
                    </div>

                    <div class="j-list"><?= $list ?></div>
                    <div class="j-pagination"><?= $pgn ?></div>

                </div>

            </div>
        </section>

    </div>
<script type="text/javascript">
<? js::start() ?>
jShopSearch.init(<?= func::php2js(array(
    'lang'       => array(),
    'ajax'       => true,
    'cat_id'     => $cat_id,
    'defCountry' => Geo::defaultCountry(),
    'preSuggest' => (Geo::countrySelect() && $f['c'] ? Geo::regionPreSuggest($f['c'], 2) : ''),
)) ?>);
<? js::stop() ?>
</script>