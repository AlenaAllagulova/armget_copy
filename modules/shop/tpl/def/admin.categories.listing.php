<?php
$bRotate = true;
?>
<?= tplAdmin::blockStart(_t('shop', 'Shop').' / '._t('shop', 'Добавление категории'), false, array('id'=>'ShopCategoriesFormBlock','style'=>'display:none;')); ?>
<div id="ShopCategoriesFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('shop', 'Shop').' / '._t('shop', 'Категории'), true, array('id'=>'ShopCategoriesListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>_t('shop', '+ добавить категорию'),'class'=>'ajax','onclick'=>'return jShopCategoriesFormManager.action(\'add\',0);'),
    array(
        array('title'=>_t('', 'валидация nested-sets'), 'onclick'=>"return bff.confirm('"._t('', 'Длительная операция, продолжить?')."', {r:'".$this->adminLink(bff::$event.'&act=dev-treevalidate')."'})", 'icon'=>'icon-indent-left', 'debug-only'=>true),
    )); ?>
<div class="actionBar" style="min-height: 20px;">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="ShopCategoriesListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />

        <div class="left">
        </div>
        <div class="right">
            <div id="ShopCategoriesProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="ShopCategoriesListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="50"><?= _t('', 'ID') ?></th>
        <th class="left"><?= _t('', 'Title') ?></th>
        <th width="100"><?= _t('shop', 'Товары') ?></th>
        <th width="135"><?= _t('', 'Created') ?></th>
        <th width="135"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="ShopCategoriesList">
    <?= $list ?>
    </tbody>
</table>
<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">
        <? if($bRotate){ ?>&darr; &uarr;<? } ?>
    </div>
    <br />
</div>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
var jShopCategoriesFormManager = (function(){
    var $progress, $block, $blockCaption, $formContainer, process = false;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $formContainer = $('#ShopCategoriesFormContainer');
        $progress = $('#ShopCategoriesProgress');
        $block = $('#ShopCategoriesFormBlock');
        $blockCaption = $block.find('span.caption');

        <? if(!empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jShopCategoriesList.toggle(false);
            if(jShopCategoriesForm) jShopCategoriesForm.onShow();
        } else {
            jShopCategoriesList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? '<?= _t('shop', 'Shop') ?> / <?= _t('shop', 'Добавление категории') ?>' : '<?= _t('shop', 'Shop') ?> / <?= _t('shop', 'Редактирование категории') ?>'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
            } else {
                jShopCategoriesList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        params = $.extend(params || {}, {act:type});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jShopCategoriesList =
    (function()
    {
        var $progress, $block, $list, $listTable, $listPgn, filters, processing = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';

        $(function(){
            $progress  = $('#ShopCategoriesProgress');
            $block     = $('#ShopCategoriesListBlock');
            $list      = $block.find('#ShopCategoriesList');
            $listTable = $block.find('#ShopCategoriesListTable');
            $listPgn   = $block.find('#ShopCategoriesListPgn');
            filters    = $block.find('#ShopCategoriesListFilters').get(0);

            $list.delegate('a.category-edit', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) jShopCategoriesFormManager.action('edit',id);
                return false;
            });

            $list.delegate('a.category-toggle', 'click', function(){
                var id = intval($(this).attr('rel'));
                var type = $(this).data('type');
                if(id>0) {
                    bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, {
                        progress: $progress, link: this, complete: function(r){
                            if(r && r.success && r.refresh) updateList();
                        }
                    });
                }
                return false;
            });

            $list.delegate('a.category-del', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) del(id, this);
                return false;
            });

            $list.delegate('a.category-expand', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) {
                    bff.expandNS(id, ajaxUrl+'expand&id=', {cookie:app.cookiePrefix+'shop_categories_expand', progress:$progress});
                }
                return false;
            });

            $list.delegate('a.category-dynprops', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) {
                    bff.redirect('<?= $this->adminLink('dynprops_listing&owner='); ?>'+id);
                }
                return false;
            });

            $(window).bind('popstate',function(){
                if('state' in window.history && window.history.state === null) return;
                var loc = document.location;
                var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                if( actForm!=null ) {
                    var actId = /id=([\d]+)/.exec(loc.search.toString());
                    jShopCategoriesFormManager.action(actForm[1], actId && actId[1]);
                } else {
                    jShopCategoriesFormManager.action('cancel');
                    updateList(false);
                }
            });

            <? if($bRotate) { ?>bff.rotateTable($list, ajaxUrl+'rotate', $progress);<? } ?>
        });

        function isProcessing()
        {
            return processing;
        }

        function del(id, link)
        {
            bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
            return false;
        }

        function updateList(updateUrl)
        {
            if(isProcessing()) return;
            var f = $(filters).serialize();
            bff.ajax(ajaxUrl, f, function(data){
                if(data) {
                    $list.html( data.list );
                    $listPgn.html( data.pgn );
                    if(updateUrl !== false && bff.h) {
                        window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                    }
                    <? if($bRotate) { ?>$list.tableDnDUpdate();<? } ?>
                }
            }, function(p){ $progress.toggle(p); processing = p; $list.toggleClass('disabled'); });
        }

        function setPage(id)
        {
            // нет постраничности
        }

        return {
            submit: function(resetForm)
            {
                if(isProcessing()) return false;
                setPage(1);
                if(resetForm) {
                    //
                }
                updateList();
            },
            page: function (id)
            {
                if(isProcessing()) return false;
                setPage(id);
                updateList();
            },
            refresh: function(resetPage,updateUrl)
            {
                if(resetPage) setPage(0);
                updateList(updateUrl);
            },
            toggle: function(show)
            {
                if(show === true) {
                    $block.show();
                    if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                }
                else $block.hide();
            }
        };
    }());
</script>