<?php
if( ! isset($data['pex'])) $data['pex'] = 0;
$bOpenPrice =  ! empty($data['pf']) || ! empty($data['pt']) || ! empty($data['pex']);
if(empty($price_title_mod)){
    $price_title_mod = _t('shop', 'Торг возможен');
}
?>
<a href="#j-shop-price" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpenPrice ? ' active' : '' ?>"><i class="fa <?= $bOpenPrice ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('shop', 'Стоимость'); ?></a>
<div class="collapse<?= $bOpenPrice ? ' in' : '' ?> j-collapse" id="j-shop-price">
    <div class="l-inside l-budget">
        <div class="form-inline" role="form">
            <div class="form-group">
                <input type="text" name="pf" class="form-control input-sm" value="<?= ! empty($data['pf']) ? $data['pf'] : '' ?>" placeholder="<?= _t('', 'От'); ?>">
            </div>
            <div class="form-group">
                <input type="text" name="pt" class="form-control input-sm" value="<?= ! empty($data['pt']) ? $data['pt'] : '' ?>" placeholder="<?= _t('', 'До'); ?>">
            </div>
            <div class="form-group">
                <select class="form-control input-sm" name="pc"><?= Site::currencyOptions(isset($data['pc']) ? $data['pc'] : $price_curr) ?></select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <? if($price_ex & Shop::PRICE_EX_EXCHANGE): ?>
                    <div class="checkbox"> <label>
                            <input type="checkbox" value="<?= Shop::PRICE_EX_EXCHANGE ?>" autocomplete="off" name="pex[]" <?= $data['pex'] & Shop::PRICE_EX_EXCHANGE ? 'checked="checked"' : '' ?> />
                            <?= _t('shop', 'Обмен'); ?>
                        </label> </div>
                <? endif; ?>
                <? if($price_ex & Shop::PRICE_EX_FREE): ?>
                    <div class="checkbox"> <label>
                            <input type="checkbox" value="<?= Shop::PRICE_EX_FREE ?>" autocomplete="off" name="pex[]" <?= $data['pex'] & Shop::PRICE_EX_FREE ? 'checked="checked"' : '' ?> />
                            <?= _t('shop', 'Бесплатно'); ?>
                        </label></div>
                <? endif; ?>
                <? if($price_ex & Shop::PRICE_EX_MOD): ?>
                    <div class="checkbox"> <label>
                            <input type="checkbox" value="<?= Shop::PRICE_EX_MOD ?>" autocomplete="off" name="pex[]" <?= $data['pex'] & Shop::PRICE_EX_MOD ? 'checked="checked"' : '' ?> />
                            <?= $price_title_mod ?>
                        </label></div>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>
