<?php
if( ! $price) return;
$bRadio = $price_ex & Shop::PRICE_EX_EXCHANGE || $price_ex & Shop::PRICE_EX_FREE;
if( ! isset($data['price_ex'])) $data['price_ex'] = 0;
if(empty($price_title_mod)){
    $price_title_mod = _t('shop', 'Торг возможен');
}
?>
<div class="form-group o-propose-inputs">
    <label for="price" class="col-sm-3 control-label"><?= (!empty($price_title) ? $price_title : _t('shop', 'Цена')) ?></label>
    <div class="col-sm-9">
        <? if($price_ex & Shop::PRICE_EX_EXCHANGE): ?><div class="radio pdt5"> <label> <input type="radio" name="price_ex[]" value="<?= Shop::PRICE_EX_EXCHANGE ?>" <?= $data['price_ex'] & Shop::PRICE_EX_EXCHANGE ? ' checked="checked"' : '' ?>/> <?= _t('shop', 'Обмен'); ?> </label> </div> <? endif; ?>
        <? if($price_ex & Shop::PRICE_EX_FREE): ?><div class="radio pdt5"> <label> <input type="radio" name="price_ex[]" value="<?= Shop::PRICE_EX_FREE ?>" <?= $data['price_ex'] & Shop::PRICE_EX_FREE ? ' checked="checked"' : '' ?>/> <?= _t('shop', 'Бесплатно'); ?> </label> </div> <? endif; ?>
        <? if($bRadio): ?>
        <div class="radio radio-inputs">
            <label>
                <input type="radio" name="price_ex[]" id="j-price-val" value="0" />
            </label>
        <? endif; ?>
            <div class="input-group">
                <input type="text" name="price" class="form-control input-sm j-price-val-ch" maxlength="9" value="<?= isset($data['price']) ? $data['price'] : '' ?>" />
                <select class="form-control input-sm j-price-val-ch" name="price_curr"><?= Site::currencyOptions(isset($data['price_curr']) ? $data['price_curr'] : $price_curr) ?></select>
                <? if($price_ex & Shop::PRICE_EX_MOD): ?> <label class="checkbox-inline radio-inputs-checkbox"> <input class="j-price-val-ch" type="checkbox" name="price_ex[]" value="<?= Shop::PRICE_EX_MOD ?>" <?= $data['price_ex'] & Shop::PRICE_EX_MOD ? ' checked="checked"' : '' ?> /> <?= $price_title_mod ?> </label> <? endif; ?>
            </div>
        <? if($bRadio): ?>
        </div>
        <? endif; ?>
    </div>
</div>