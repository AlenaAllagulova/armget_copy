<?php
    tpl::includeJS('shop.search.keywords', false, 2);
?>
    <div class="container">
        <section class="l-searchAdd">
            <div class="row">
                <div class="col-sm-12">
                    <div class="l-search-bar">
                        <form role="search" action="" method="get" id="j-shop-search-keywords-form">
                            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
                            <? if($f['tag']) { ?><input type="hidden" name="tag" value="<?= HTML::escape($f['tag']) ?>" /><? } ?>
                            <input type="search" name="q" value="<?= HTML::escape($f['q']) ?>" class="form-control" placeholder="<?= _t('shop', 'Найти товар'); ?>">
                            <button class="l-search-button"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="l-mainContent" id="j-shop-search-keywords">
            <div class="l-list-heading l-search-heading">
                <h6><?= _t('shop', 'Найдено'); ?> <span class="j-shop-count"><?= $count ?></span></h6>
                <div class="clearfix"></div>
            </div>
            <div class="j-list"><?= $list ?></div>
            <div class="j-pagination"><?= $pgn ?></div>
        </section>
    </div>
<script type="text/javascript">
    <? js::start() ?>
    jShopSearchKeywords.init(<?= func::php2js(array(
        'lang' => array(),
        'ajax' => false,
    )) ?>);
    <? js::stop() ?>
</script>