<?php
$productsUrl = $this->adminLink('products&cat=');
foreach($list as $k=>$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>" id="dnd-<?= $id ?>" data-numlevel="<?= $v['numlevel'] ?>" data-pid="<?= $v['pid'] ?>">
        <td class="small"><?= $id ?></td>
        <td class="left" style="padding-left:<?= ($v['numlevel']*15-10) ?>px;">
            <a class="but category-expand folder<?= (!$v['node'] ? '_ua' : '') ?> but-text" rel="<?= $id ?>" href="#"><?= $v['title'] ?></a>
        </td>
        <td><a href="<?= $productsUrl.$id ?>"><?= $v['products'] ?></a></td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a class="but sett category-dynprops" title="<?= _t('shop', 'Дин.свойства') ?>" href="#" rel="<?= $id ?>"></a>
            <a class="but edit category-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
            <a class="but <?= ($v['enabled']?'un':'') ?>block category-toggle" title="<?= _t('', 'Enable') ?>" href="#" data-type="enabled" rel="<?= $id ?>"></a>
            <a class="but del category-del" title="<?= _t('', 'Delete') ?>" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="5">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;