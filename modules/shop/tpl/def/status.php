<?php

?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <? if (Shop::premoderation()) { # включена премодерация ?>
                    <h1 class="small"><?= _t('shop', 'Ваш товар будет опубликован после проверки менеджером') ?></h1>
                    <p><?= _t('shop', 'Ваш товар сохранен, но еще не опубликован.<br /> Вы можете увидеть и отредактировать его в <a [link_cabinet]>вашем кабинете</a>.', array(
                        'link_cabinet' => 'href="'.Shop::url('my.shop', array('cat' => $cat_id1)).'"'
                    )) ?><br />
                        <?= _t('shop', 'Как только наш менеджер проверит корректность введенных вами данных, товар появится в магазине.') ?><br />
                    </p>
                    <p><a href="<?= Shop::url('add') ?>"><?= _t('shop', 'Добавить еще один товар') ?></a></p>
                    <p><a href="<?= Shop::url('my.shop') ?>"><?= _t('shop','Перейти на страницу списка товаров') ?></a></p>
                <? } else { ?>
                    <h1 class="small"><?= _t('shop', 'Ваш товар успешно опубликован.') ?></h1>
                    <p><?= _t('shop', 'Чтобы увидеть его в общем списке товаров перейдите на <a [link_shop]>страницу магазина</a>.',
                            array('link_shop' => 'href="'.Shop::url('my.shop').'"')) ?><br />
                       <?= _t('shop', 'Вы также можете отредактировать его в <a [link_cabinet]>вашем кабинете</a>.',
                            array('link_cabinet' => 'href="'.Shop::url('my.shop', array('cat' => $cat_id1)).'"')) ?><br /></p>
                    <p><a href="<?= Shop::url('add') ?>"><?= _t('shop', 'Добавить еще один товар') ?></a></p>
                <? } ?>
            </div>
        </div>
    </section>
</div>