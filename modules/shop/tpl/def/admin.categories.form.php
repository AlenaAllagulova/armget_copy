<?php
    /**
     * @var $this Shop
     * @var $price_ex integer
     */
    $aData = HTML::escape($aData, 'html', array('keyword'));
    $edit = !empty($id);
    $aTabs = array(
        'info' => _t('shop','Основные'),
        'seo-shop' => _t('','SEO: Магазин'),
    );
    if (Orders::useProducts()) {
        $aTabs['seo-orders'] = _t('','SEO: Заказы');
    }

?>
<form name="ShopCategoriesForm" id="ShopCategoriesForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
<input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
<input type="hidden" name="save" value="1" />
<input type="hidden" name="id" value="<?= $id ?>" />
<? if(sizeof($aTabs) > 1) { ?>
<div class="tabsBar" id="ShopCategoriesTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab<? if($k == 'info') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
    <? } ?>
</div>
<? } ?>
<!-- таб: Основные -->
<div class="j-tab j-tab-info">
    <table class="admtbl tbledit">
        <tr class="required check-select">
            <td class="row1 field-title" width="110"><?= _t('shop', 'Основной раздел') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <? if ($edit) { ?>
                    <input type="hidden" name="pid" id="category-pid" value="<?= $pid ?>" />
                    <span class="bold"><?= $pid_path ?></span>
                <? } else { ?>
                    <select name="pid" id="category-pid"><?= $this->model->categoriesOptions($pid, false, 1) ?></select>
                <? } ?>
            </td>
        </tr>
        <?= $this->locale->buildForm($aData, 'categories-item','
        <tr class="required">
            <td class="row1 field-title">Название<span class="required-mark">*</span></td>
            <td class="row2">
                <input class="stretch <?= $key ?>" type="text" id="category-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
            </td>
        </tr>'); ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'URL-Keyword') ?><br /><a href="#" onclick="return bff.generateKeyword('#category-title-<?= LNG ?>', '#category-keyword');" class="ajax desc small"><?= _t('', 'generate') ?></a></td>
            <td class="row2">
                <input class="stretch" type="text" id="category-keyword" name="keyword" value="<?= $keyword ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('shop', 'Цена') ?></td>
            <td class="row2">
                <label class="checkbox">
                    <input type="checkbox" id="category-price-settings-toggler" name="price" <?php if ($price){ ?> checked="checked"<?php } ?> />
                </label>
            </td>
        </tr>
        <tr <?php if (!$price){ ?> class="hidden"<?php } ?> id="category-price-settings">
            <td class="row1"></td>
            <td class="row2" id="j-price-form"><?= $priceForm; ?></td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('', 'Enabled') ?></td>
            <td class="row2">
                <label class="checkbox"><input type="checkbox" id="category-enabled" name="enabled"<? if($enabled){ ?> checked="checked"<? } ?> /></label>
            </td>
        </tr>
    </table>
</div>
<!-- таб: SEO: Магазин -->
<div class="j-tab j-tab-seo-shop hidden">
    <?= SEO::i()->form($this, $aData, 'search-category', array('name_prefix'=>'shop_')); ?>
</div>
<!-- таб: SEO: Заказы -->
<div class="j-tab j-tab-seo-orders hidden">
    <?= SEO::i()->form(Orders::i(), $aData, 'search-product', array('name_prefix'=>'orders_')); ?>
</div>
<div style="margin-top: 10px;">
    <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jShopCategoriesForm.save(false);" />
    <? if($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jShopCategoriesForm.save(true);" /><? } ?>
    <? if($edit) { ?><input type="button" onclick="jShopCategoriesForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
    <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jShopCategoriesFormManager.action('cancel');" />
</div>
</form>

<script type="text/javascript">
var jShopCategoriesForm =
(function(){
    var $progress, $form, formChk, id = <?= $id ?>;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $progress = $('#ShopCategoriesFormProgress');
        $form = $('#ShopCategoriesForm');
        // tabs
        $form.find('#ShopCategoriesTabs .j-tab-toggler').on('click', function(e){ nothing(e);
            var key = $(this).data('key');
            $form.find('.j-tab').addClass('hidden');
            $form.find('.j-tab-'+key).removeClass('hidden');
            $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
        });

        <? if( ! $edit): ?>
            var prCache = {};
            var $priceForm = $('#j-price-form');
            var $price= $form.find('#category-price-settings-toggler');
            $form.find('#category-pid').change(function(){
                var id = intval($(this).val());
                if(prCache.hasOwnProperty(id)){
                    priceForm(prCache[id]);
                } else {
                    bff.ajax(ajaxUrl + '&act=price-form', {id:id}, function(data,errors){
                        if(data && data.form){
                            prCache[id] = {form:data.form, price:data.price};
                            priceForm(prCache[id]);
                        }
                    });
                }
            });

            function priceForm(o){
                $priceForm.html(o.form);
                if($price.is(':checked') != o.price){
                    $price.trigger('click');
                }
            }
        <? endif; ?>
    });
    return {
        del: function()
        {
            if( id > 0 ) {
                bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                    false, {progress: $progress, repaint: false, onComplete:function(){
                        bff.success('<?= _t('shop', 'Категория была успешно удалена') ?>');
                        jShopCategoriesFormManager.action('cancel');
                        jShopCategoriesList.refresh();
                    }});
            }
        },
        save: function(returnToList)
        {
            if( ! formChk.check(true) ) return;
            bff.ajax(ajaxUrl, $form.serialize(), function(data,errors){
                if(data && data.success) {
                    bff.success('<?= _t('', 'Данные успешно сохранены') ?>');
                    if(returnToList || ! id) {
                        jShopCategoriesFormManager.action('cancel');
                        jShopCategoriesList.refresh( ! id);
                    }
                }
            }, $progress);
        },
        onShow: function()
        {
            $form.find('#category-price-settings-toggler').change(function(){
                $form.find('#category-price-settings').toggle();
                return false;
            });
            formChk = new bff.formChecker($form);
        }
    };
}());
</script>