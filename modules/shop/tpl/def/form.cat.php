<?php
tpl::includeJS('cat.select', false);
if(empty($aParents)) {
    $aParents = array();
    $aData['aParents'] = $aParents;
}
if( ! isset($id)) $id = 0;
$path = '';
if($id){
    $path = $aParents;
    if($pid) $path[] = (int)$pid;
    if($id) $path[] = (int)$id;
    if( ! empty($cat_id)) $path[] = (int)$cat_id;
    $path = ', path:'.func::php2js($path, true);
}
?>
<div class="dropdown p-form-noinput j-cat-select" data="{count:<?= $count ?><?= $path ?>}">
    <input type="hidden" class="j-cat-value" name="<?= ! empty($inputName) ? $inputName : 'cats['.$count.']' ?>" value="<?= ! empty($cat_id) ? $cat_id : 0 ?>" autocomplete="off"/>
    <a href="#" class="dropdown-toggle ajax-link pull-left j-title-empty<?= $id ? ' hide' : '' ?>" data-toggle="dropdown"><span><?= _t('specs', 'Выбрать категорию'); ?></span> <i class="fa fa-caret-down"></i></a>
    <a href="#" class="dropdown-toggle ajax-link pull-left j-title-selected<?= ! $id ? ' hide' : '' ?>" data-toggle="dropdown"><span class="j-title"><?= ! empty($pid_title) ? $pid_title.' / ' : '' ?><?= ! empty($title) ? $title : '' ?></span> <i class="fa fa-caret-down"></i></a>
    <? if( ! empty($allowDelete)): ?>
        <div class="p-profileCabinet-delete">
            <a href="#" class="p-delete j-delete" title="<?= _t('form', 'Удалить'); ?>"><i class="fa fa-trash-o"></i></a>
        </div>
    <? endif; ?>
    <div class="clearfix"></div>
    <ul class="dropdown-menu c-dropdown-caret_left j-menu" role="menu">
        <?= $this->viewPHP($aData, 'form.cat.ajax'); ?>
    </ul>
</div>