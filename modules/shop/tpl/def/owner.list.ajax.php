<?php
/**
 * @var $this Shop
 * @var $user array
 */
    if( ! empty($list)):
        foreach($list as $k => $v): ?>
            <div class="p-portfolioSection j-cat">

                <h5><?= $cats[$k]['title'] ?></h5>

                <? $bDescr =  ! empty($cats[$k]['descr']); ?>
                <p class="<?= ! $bDescr ? 'hidden ' : '' ?>j-descr"> <?= $bDescr ? nl2br($cats[$k]['descr']) : '' ?> </p>

                <? if($user['my']): ?>
                <div class="p-profileOrder-controls-mobile j-edit">
                    <a href="#" class="j-cat-edit" data-id="<?= $k ?>"><i class="fa fa-edit c-link-icon"></i></a>
                </div>

                <div class="p-profileOrder-controls  j-edit">
                    <a href="#" class="j-cat-edit" data-id="<?= $k ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('shop', 'Редактировать категорию'); ?></a>
                </div>
                <? endif; ?>

                <!-- Shop List -->
                <ul class="sh-shopList p-shopList media-list">
                <?  foreach($v as $vv):
                    $bDisabled = $vv['status'] != Shop::STATUS_ACTIVE || ( Shop::premoderation() && $vv['moderated'] != 1);
                ?>
                    <li class="media j-product<?= $bDisabled ? ' disabled ' : '' ?><?= $vv['svc_marked'] ? ' sh-highlited' : '' ?>">
                        <? if($vv['img_s']): ?>
                        <a href="<?= $vv['url_view'] ?>" class="sh-shopList-img">
                            <img src="<?= $vv['img_s'] ?>" alt="<?= tpl::imageAlt(array('t' => $vv['title'])); ?>" />
                        </a>
                        <? endif; ?>
                        <div class="media-body">
                            <div class="sh-item-info">
                                <div class="sh-item-name">
                                    <? if($vv['svc_fixed']): ?><i class="fa fa-thumb-tack c-icon-fixed"></i><? endif; ?>
                                    <a href="<?= $vv['url_view'] ?>"><?= $vv['title'] ?></a>
                                </div>

                                <? if($user['my']): ?>
                                <div class="p-profileOrder-controls-mobile">
                                    <a href="<?= Shop::url('edit', array('id' => $vv['id'])) ?>"><i class="fa fa-edit c-link-icon"></i></a>
                                    <? if(bff::servicesEnabled()): ?><a href="<?= Shop::url('promote', array('id' => $vv['id'])); ?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i></a><? endif; ?>
                                    <a href="#" class="<?= $vv['status'] != Shop::STATUS_ACTIVE ? 'hidden ' : '' ?>j-hide" data-id="<?= $vv['id'] ?>"><i class="fa fa-lock c-link-icon"></i></a>
                                    <a href="#" class="<?= $vv['status'] != Shop::STATUS_NOT_ACTIVE ? 'hidden ' : '' ?>j-show" data-id="<?= $vv['id'] ?>"><i class="fa fa-unlock c-link-icon"></i></a>
                                    <a href="#" class="link-delete j-delete" data-id="<?= $vv['id'] ?>"><i class="fa fa-times c-link-icon"></i></a>
                                </div>
                                <? endif; ?>

                                <div class="l-project-head">
                                    <?= Shop::priceBlock($vv, $cats); ?>
                                    <? if( ! empty($vv['aTags'])): foreach($vv['aTags'] as $vvv):?>
                                        <a href="<?= Shop::url('search-tag', $vvv) ?>" class="l-tag"><?= $vvv['tag'] ?></a>
                                    <? endforeach; endif; ?>
                                </div>

                                <article class="sh-shopList-description">
                                    <?= nl2br(tpl::truncate($vv['descr'], config::sysAdmin('shop.owner.list.ajax.descr.truncate', 250, TYPE_UINT))); ?>
                                </article>

                                <ul class="l-item-features sh-project-features">
                                    <li><?= tpl::userLink($vv, 'icon', 'shop') ?></li>
                                    <li><a href="<?= Shop::url('search-cat', $vv) ?>"><i class="fa fa-th-list c-link-icon"></i><?= $vv['cat_title'] ?></a></li>
                                    <? if( ! empty($vv['city_data']['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $vv['city_data']['title']?></li><? endif; ?>
                                </ul>
                            </div>
                        </div>

                        <? if($user['my']): ?>
                        <div class="p-profileOrder-controls sh-item-controls">
                            <a href="<?= Shop::url('edit', array('id' => $vv['id'])) ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                            <? if(bff::servicesEnabled()): ?><a href="<?= Shop::url('promote', array('id' => $vv['id'])); ?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i><?= _t('form', 'Рекламировать'); ?></a><? endif; ?>
                            <a href="#" class="<?= $vv['status'] != Shop::STATUS_ACTIVE ? 'hidden ' : '' ?>j-hide" data-id="<?= $vv['id'] ?>"><i class="fa fa-lock c-link-icon"></i><?= _t('shop', 'Скрыть из магазина'); ?></a>
                            <a href="#" class="<?= $vv['status'] != Shop::STATUS_NOT_ACTIVE ? 'hidden ' : '' ?>j-show" data-id="<?= $vv['id'] ?>"><i class="fa fa-unlock c-link-icon"></i><?= _t('shop', 'Отобразить'); ?></a>
                            <a href="#" class="link-delete j-delete" data-id="<?= $vv['id'] ?>"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
                        </div>
                        <?= $this->productStatusBlock($vv); ?>
                        <? endif; ?>

                    </li>
                <? endforeach; ?>
                </ul>
            </div>
        <? endforeach;
    endif;
