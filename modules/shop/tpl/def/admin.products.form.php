<?php
    /**
     * @var $this Shop
     */
    $aData = HTML::escape($aData, 'html', array('cat_id','title','descr'));
    $aData['edit'] = $edit = ! empty($id);
    $tab = $this->input->getpost('tab', TYPE_NOTAGS);
    $aTabs = array(
        'info' => _t('shop','Описание'),
        'images' => _t('shop','Фото'),
    );
    if( ! isset($aTabs[$tab])) {
        $tab = 'info';
    }
?>
<div class="tabsBar">
    <?php foreach($aTabs as $k=>$v): ?>
        <span class="tab<?= ($k==$tab ? ' tab-active' : '') ?>"><a href="#" onclick="jShopProductsForm.onTab('<?= $k ?>', this); return false;"><?= $v ?></a></span>
    <?php endforeach; ?>
    <div class="progress" style="margin-left: 5px; display: none;" id="form-progress"></div>
</div>

<div id="product-form-block-info" class="hidden">
<form name="ShopProductsForm" id="ShopProductsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <input type="hidden" name="cat_id" class="j-cat-id" value="<?= $cat_id ?>" />
    <table class="admtbl tbledit">
        <tr class="required check-select">
            <td class="row1 field-title" width="100"><?= _t('shop', 'Категория') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <?php
                foreach($cats as $lvl=>$v) {
                    ?><select class="j-cat-select" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jShopProductsForm.onCategory($(this))"><?= $v['categories'] ?></select><?
                }
                ?>
            </td>
        </tr>
        <tr class="required">
            <td class="row1 field-title"><?= _t('shop', 'Заголовок') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <input class="stretch" type="text" name="title" value="<?= $title ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('shop', 'Описание') ?></td>
            <td class="row2">
                <textarea class="stretch" rows="5" name="descr"><?= $descr ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="j-cat-form" style="padding: 0;">
                <?= ( ! empty($cat['form']) ? $cat['form'] : '' ) ?>
            </td>
        </tr>
        <? $nRegionsLimit = Shop::regionsLimit();
        if($nRegionsLimit): ?>
        <tr id="product-city-block">
            <td class="row1 field-title"><?= _t('shop', 'Регион') ?></td>
            <td class="row2">
                <? $bAddRegions = ( ! empty($regions) && count($regions) < $nRegionsLimit ); ?>
                <div id="j-region-select-block" <? if( $bAddRegions ) { ?> style="display: none;"<? } ?>>
                    <? if(Geo::countrySelect()): ?>
                        <div id="j-country-input-block" class="left" style="margin-right:5px;">
                            <select id="j-product-country" onchange="jShopProductsForm.onCountry($(this));"> <?= HTML::selectOptions(Geo::countryList(), Geo::defaultCountry(), _t('', 'Select'), 'id', 'title') ?></select>
                        </div>
                    <? endif; ?>
                    <input type="hidden" id="product-city-id" />
                    <div class="relative left" id="j-region-input-block" style="margin-bottom: 3px;"><input type="text" id="product-city-ac" class="autocomplete" placeholder="<?= _t('orders', 'Введите название региона') ?>" style="width: 220px;" /></div>
                    <div class="clearfix"></div>
                </div>
                <div id="j-regions-selected" class="hide left"></div>
                <? if( $bAddRegions ) { ?><a href="#" onclick="$('#j-region-select-block').show(); $(this).hide(); return false;" class="left btn btn-mini" style="margin-left: 3px;" id="j-region-but-add"><?= _t('', '+ add') ?></a><? } ?>
                <div class="clearfix"></div>
            </td>
        </tr>
        <? endif; ?>

        <tr class="check-select">
            <td class="row1 field-title"><?= _t('', 'Tags') ?></td>
            <td class="row2">
                <?= $this->productTags()->tagsForm($id, $this->adminLink('ajax&act=tags-suggest', bff::$class), 725) ?>
            </td>
        </tr>
    <?php if($edit): ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'User') ?></td>
            <td class="row2">
                <a href="#" class="ajax" onclick="return bff.userinfo(<?= $user_id ?>);"><?= $email ?></a>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2">
                <?= $this->viewPHP($aData, 'admin.products.form.status'); ?>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'User') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <input type="hidden" name="user_id" value="0" id="j-product-user-id" />
                <input type="text" name="email" value="" id="j-product-user-email" class="autocomplete input-large" placeholder="<?= _t('', 'Enter user e-mail') ?>" />
            </td>
        </tr>
    <?php endif; ?>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jShopProductsForm.save(false);" />
                <?php if($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jShopProductsForm.save(true);" /><? } ?>
                <?php if($edit) { ?><input type="button" onclick="jShopProductsForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
                <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jShopProductsFormManager.action('cancel');" />
            </td>
        </tr>
    </table>
</form>
</div>

<div id="product-form-block-images" class="hidden"><?= $this->viewPHP($aData, 'admin.products.form.images'); ?></div>

<script type="text/javascript">
var jShopProductsForm =
(function(){
    var $progress, $form, formChk, $catForm, id = <?= $id ?>, blocksPrefix = 'product-form-block-';
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';
    var catCache = {};
    var $regionBlock, $regionsSelected, $country, $regionsSelect;
    var city = {$block:0,$ac:0, acApi:0, data:{}};

    $(function(){
        $progress = $('#ShopProductsFormProgress');
        $form = $('#ShopProductsForm');
        $catForm = $form.find('.j-cat-form');

        $regionBlock = $('#j-region-input-block');
        $regionsSelected = $('#j-regions-selected');
        $country = $('#j-product-country');
        $regionsSelect = $('#j-region-select-block');

        $regionsSelected.on('click', '.j-delete', function(){
            $('#j-region-but-add').remove();
            $(this).closest('.j-selected').remove();
            var cnt = $regionsSelected.find('.j-region-id').length;
            if(cnt < <?= $nRegionsLimit ?>){
                $regionsSelect.show();
            }
            return false;
        });

        city.$block = $form.find('#product-city-block');
        city.$ac = $form.find('#product-city-ac').autocomplete('<?= $this->adminLink('regionSuggest', 'geo') ?>',
            {valueInput: $form.find('#product-city-id'), params:{country:<?= Geo::defaultCountry() ?>}, suggest: <?= Geo::regionPreSuggest(Geo::defaultCountry()) ?>,
                onSelect: function(cityID, cityTitle, ex){
                    if( ! ex.changed ) return;
                    regionSelected(cityID, cityTitle, $country ? $country.find(':selected').text() : false);
                    city.acApi.reset();
                }}, function(){ city.acApi = this; });

        <? if( ! $edit): ?>
        $form.find('#j-product-user-email').autocomplete(ajaxUrl+'&act=user',
            {valueInput: $form.find('#j-product-user-id')});
        <? endif; ?>

        <? if($edit):
            if( ! empty($regions)): foreach($regions as $v):?>regionSelected(<?= $v['reg3_city']?>, '<?= HTML::escape($v['title'], 'js') ?>', <?= Geo::countrySelect() ? '\''.HTML::escape($v['country_title'], 'js').'\'': 'false' ?>);<? endforeach; endif;
        endif; ?>

        initBlock('<?= $tab ?>', false);
    });

    function catView(data, $select)
    {
        if(data === 'empty') {
            $catForm.empty();
            return;
        }

        if(data.subs>0) {
            $select.after('<select class="j-cat-select" style="margin-right: 5px;" onchange="jShopProductsForm.onCategory($(this))">'+data.cats+'</select>').show();
            return;
        }

        $catForm.html(data.form);
        formChk.check(false, true);
    }

    function initBlock(key, $block)
    {
        if($block === false) {
            $block = $('#'+blocksPrefix+key);
        }
        $block.addClass('inited').removeClass('hidden');
    }

    function regionSelected(region_id, title, country)
    {
        if(region_id > 0 && ! $regionsSelected.find('.j-region-id[value="'+region_id+'"]').length ){
            $regionsSelected.append(
                '<span class="label j-selected" style="margin:0 2px 2px 2px;" data-country="'+country+'" data-city="'+title+'">'+(country ? country + ' / ' : '') + title +'<a href="#" class="j-delete" style="margin-left: 3px;"><i class="icon-remove icon-white" style="margin-top: 0px;"></i></a><input type="hidden" name="regions[]" class="j-region-id" value="'+region_id+'" /></span>'
            ).removeClass('hide');
            if($regionsSelected.find('.j-region-id').length >= <?= $nRegionsLimit ?>){
                $regionsSelect.hide();
            }
        }
    }

    return {
        del: function()
        {
            if( id > 0 ) {
                bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                    false, {progress: $progress, repaint: false, onComplete:function(){
                        bff.success('<?= _t('shop', 'Товар был успешно удален') ?>');
                        jShopProductsFormManager.action('cancel');
                        jShopProductsList.refresh();
                    }});
            }
        },
        save: function(returnToList)
        {
            if( ! formChk.check(true) ) return false;
            // check selected cats
            var catsEmpty = $form.find('select.j-cat-select[value="0"]:visible');
            if(catsEmpty.length > 0) {
                bff.error('<?= _t('shop', 'Выберите категорию') ?>');
                return false;
            }
            var data = $form.serialize();
            <? if( ! $edit): ?>
                data = data + '&' + jShopProductImages.serialize();
            <? endif; ?>
            bff.ajax(ajaxUrl, data, function(data){
                if(data && data.success) {
                    bff.success('<?= _t('shop', 'Данные успешно сохранены') ?>');
                    if(returnToList || ! id) {
                        jShopProductsFormManager.action('cancel');
                        jShopProductsList.refresh( ! id);
                    }
                }
            }, $progress);
            return true;
        },
        onShow: function ()
        {
            formChk = new bff.formChecker( $form );
        },
        onCategory: function($select)
        {
            catView('empty');

            var catID = intval($select.val());
            $form.find('.j-cat-id').val(catID);
            $select.nextAll().remove();

            if( ! catID) return;

            if(catCache.hasOwnProperty(catID)) {
                catView( catCache[catID], $select );
            } else {
                bff.ajax('<?= $this->adminLink('products&act=category-data'); ?>', {cat_id: catID}, function(data){
                    if(data && data.success) {
                        catView( (catCache[catID] = data), $select );
                    }
                }, function(){
                    $progress.toggle();
                });
            }
        },
        onTab: function(key, tabLink)
        {
            $('[id^="'+blocksPrefix+'"]').addClass('hidden');
            var $block = $('#'+blocksPrefix+key).removeClass('hidden');
            if( ! $block.hasClass('inited')) {
                initBlock(key, $block);
            }
            $(tabLink).parent().addClass('tab-active').siblings().removeClass('tab-active');
            if(bff.h) {
                window.history.pushState({}, document.title, ajaxUrl + '&act=<?= $act ?>&id=<?= $id ?>&tab=' + key);
            }
        },
        onCountry:function($el)
        {
            var country = intval($el.val());
            if(country){
                $regionBlock.show();
                city.acApi.setParam('country', country);
                if( city.data.hasOwnProperty(country) ) {
                    city.acApi.setSuggest(city.data[country], true);
                } else {
                    bff.ajax('<?= $this->adminLink('ajax&act=country-presuggest', 'geo') ?>', {country:country}, function(data){
                        city.data[country] = data;
                        city.acApi.setSuggest(data, true);
                    });
                }
            } else {
                $regionBlock.hide();

            }
        }

    };
}());
</script>