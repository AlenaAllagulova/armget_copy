<?php
/**
 * Форма товара (добавление / редактирование)
 * @var $this Shop
 */
tpl::includeJS(array('autocomplete', 'qquploader', 'ui.sortable'), true);
tpl::includeJS('shop.product.form', false, 3);
$aData = HTML::escape($aData, 'html', array('title','descr'));

if($edit){
    $formTitle = _t('shop', 'Редактировать товар');
    $url = Shop::url('edit', array('id'=>$id));
} else {
    $formTitle = _t('shop', 'Добавить товар в магазин');
    $url = Shop::url('add');
}
foreach($aSvc as $k => $v){
    switch($v['id']){
        case Shop::SVC_FIX:  $aSvc[$k]['fa'] = 'fa-thumb-tack'; break;
        case Shop::SVC_UP:   $aSvc[$k]['fa'] = 'fa-arrow-up'; break;
        case Shop::SVC_MARK: $aSvc[$k]['fa'] = 'fa-flag'; break;
    }
}
?>
<div class="container">

    <section class="l-mainContent">
        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <?= tpl::getBreadcrumbs(array(
                    array('title' => _t('','Profile'),'link'=>Users::url('my.profile')),
                    array('title' => _t('shop', 'Shop'),'link'=>Shop::url('my.shop')),
                    array('title' => $formTitle,'active'=>true),
                )); ?>

                <div class="p-profileContent">

                    <div class="p-profile-title">
                        <h4><?= $formTitle; ?></h4>
                    </div>

                    <form class="form-horizontal"  action="" method="POST" enctype="multipart/form-data" id="j-product-form">

                        <div class="p-portfolioSection">

                            <!-- Title -->
                            <div class="form-group j-required">
                                <label for="pname" class="col-sm-3 control-label"><?= _t('shop', 'Заголовок'); ?> <i class="text-danger">*</i></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="pname" name="title" placeholder="<?= _t('shop', 'Введите название товара'); ?>" value="<?= $title ?>" autocomplete="off" />
                              </div>
                            </div>

                            <!-- Category -->
                            <div class="form-group j-cat">
                                <label for="inputSpec" class="col-sm-3 control-label"><?= _t('shop', 'Категория'); ?> <i class="text-danger">*</i></label>
                                <div class="col-sm-9">
                                    <?= $this->catSelect(1, $cat, array('inputName' => 'cat_id')); ?>
                                </div>
                            </div>

                            <div id="j-product-form-price"><?= ! empty($price) ? $price : '' ?></div>

                            <!-- Description -->
                            <div class="form-group j-required">
                                <label for="description" class="col-sm-3 control-label"><?= _t('shop', 'Описание'); ?> <i class="text-danger">*</i></label>
                                <div class="col-sm-8">
                                    <textarea rows="4" name="descr" class="form-control" id="description" placeholder="<?= _t('orders', 'Описание товара'); ?>"><?= $descr ?></textarea>
                                </div>
                            </div>

                            <div id="j-product-form-dp"><?= ! empty($dp) ? $dp : '' ?></div>

                            <?= $this->productTags()->formFront($id); ?>

                            <?  $region = array('reg1_country' => 0, 'reg3_city' => 0, 'title' => '');
                                if(Shop::regionsLimit()):
                                if (empty($regions)) {
                                    if (Geo::filterEnabled()) {
                                        $filter = Geo::filter();
                                        if (!empty($filter['id'])) {
                                            if ($filter['numlevel'] == Geo::lvlCity) {
                                                $region = array('reg1_country' => $filter['country'], 'reg3_city' => $filter['id'], 'title' => $filter['title']);
                                            } else {
                                                $region = array('reg1_country' => $filter['country'] ? $filter['country'] : $filter['id'], 'reg3_city' => 0, 'title' => '');
                                            }
                                        }
                                    }
                                } else {
                                    $region = reset($regions);
                                }
                            ?>
                            <div class="form-group">
                                <label for="country" class="col-sm-3 control-label o-control-label"><?= _t('', 'Регион'); ?></label>
                                <? if(Geo::countrySelect()): ?>
                                    <div class="col-sm-4 mrgb5">
                                        <select class="form-control" id="j-product-country"><?= HTML::selectOptions(Geo::countryList(), $region['reg1_country'], _t('', 'Выбрать'), 'id', 'title') ?></select>
                                    </div>
                                <? endif; ?>
                                <div class="col-sm-4 j-region">
                                    <input type="hidden" name="regions[]" value="<?= $region['reg3_city'] ?>" id="j-product-city-value" />
                                    <input type="text" class="form-control" id="j-product-city-select" value="<?= $region['title'] ?>" placeholder="<?= _t('', 'Город'); ?>" />
                                </div>
                            </div>
                            <? endif; ?>


                            <!-- Gallery -->
                            <div class="form-group j-images">
                                <input type="hidden" name="images_type" value="ajax" class="j-images-type-value" />
                                <input type="hidden" name="images_hash" value="<?= $imghash ?>" />
                                <label class="col-sm-3 control-label nowrap"><?= _t('shop', 'Изображения товара'); ?></label>

                                <div class="col-sm-9">

                                    <div id="p-gallery-show" class="j-images-type j-images-type-ajax">

                                        <ul class="p-addWork-gallery j-img-slots">

                                            <? for($i = 1; $i <= Shop::imagesLimit(); $i++): ?>
                                                <li class="j-img-slot">
                                                    <a class="p-hasimg hidden j-img-preview">
                                                        <div class="p-galleryImg-container">
                                                            <img class="j-img-img" src="" alt="" />
                                                        </div>
                                                    </a>
                                                    <a href="#" class="p-galleryImg-remove link-delete hidden j-img-delete j-img-preview"><i class="fa fa-times-circle-o"></i></a>
                                                    <a class="j-img-upload">
                                                        <div class="p-galleryImg-container">
                                                            <i class="fa fa-plus-circle j-img-link"></i>
                                                        </div>
                                                    </a>
                                                    <input type="hidden" name="" value="" class="j-img-fn" />
                                                </li>
                                            <? endfor; ?>

                                            <div class="clearfix"></div>
                                        </ul>

                                        <div class="help-block">
                                            <?= _t('form', 'Вы можете сортировать изображения просто перетаскивая их'); ?>
                                        </div>

                                        <?= _t('form', 'Если у вас возникли проблемы воспользуйтесь <a [a_simple]><span>альтернативной формой</span></a>', array('a_simple' => 'href="#" class="ajax-link j-images-toggler" data-type="simple"')); ?>

                                    </div>

                                    <div id="p-gallery" class="p-gallery-alternate hide j-images-type j-images-type-simple">
                                        <? for($i = 1; $i <= Shop::imagesLimit() - $imgcnt; $i++): ?>
                                            <div><input name="images_simple_<?= $i ?>" type="file" /></div>
                                        <? endfor; ?>

                                        <a href="#" class="ajax-link c-formCancel j-images-toggler" data-type="ajax"><span><?= _t('form', 'Удобная форма загрузки изображений'); ?></span></a>

                                    </div><!-- /#p-gallery -->

                                </div><!-- /.col-sm-8 -->

                            </div><!-- /.form-group -->

                            <div class="row">
                                <label class="col-sm-3 control-label o-control-label"><?= _t('', 'Продвижение товара'); ?></label>
                                <div class="col-sm-9">
                                    <div class="panel-group o-advertise-accordion">
                                        <?  $cur = Site::currencyDefault(); $svc = (int)$svc;
                                        foreach($aSvc as $v):
                                            if( ! $v['on']) continue;
                                            ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a class="j-accordion">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="svc[]" value="<?= $v['id'] ?>" <?= $svc & $v['id'] ? ' disabled="disabled" checked="checked" ': '' ?> />
                                                                <i class="fa <?= $v['fa'] ?>"></i><span><?= $v['title_view'][LNG] ?></span>
                                                                <div class="o-advertise-price"><span class="j-svc-price-<?= $v['id'] ?>"><?= round($v['price']) ?></span> <?= $cur; ?></div>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div id="j-svc-<?= $v['id'] ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <? if($v['id'] == Shop::SVC_FIX && ! empty($v['per_day'])): ?>
                                                            <div class="o-advertise-accordion-count form-inline">
                                                                <div class="form-group">
                                                                    <label for="j-fixed-count"><?= _t('', 'Закрепить на'); ?></label>
                                                                    <input type="text" name="svc_fixed_days" maxlength="3" class="form-control input-sm" id="j-fixed-count" value="<?= $v['period'] ?>">
                                                                    <?= _t('', 'дней'); ?>
                                                                </div>
                                                            </div>
                                                        <? endif; ?>
                                                        <?= $v['description_full'][LNG] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            </div>

                        </div><!-- /.p-portfolioSection -->

                        <div class="form-group c-formSubmit mrgt30">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary c-formSuccess j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= $edit ? _t('form', 'Сохранить') : _t('shop', 'Добавить товар'); ?></button>
                                <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                          </div><!-- /.col-sm-9 -->
                        </div><!-- /.form-group -->


                    </form>

                </div><!-- /.p-profile-content -->

            </div><!-- /.col-md-8 -->

        </div><!-- /.row -->
    </section><!-- /.main-content -->

</div><!-- /.container -->
<script type="text/javascript">
    <? js::start() ?>
    jShopProductForm.init(<?= func::php2js(array(
        'lang' => array(
            'saved_success'       => _t('shop', 'Товар успешно сохранен'),
            'cat_wrong'           => _t('shop', 'Укажите категорию'),
            'upload_typeError'    => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
            'upload_sizeError'    => _t('form', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
            'upload_minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
            'upload_emptyError'   => _t('form', 'Файл {file} имеет некорректный размер'),
            'upload_limitError'   => _t('form', 'Вы можете загрузить не более {limit} файлов'),
            'upload_onLeave'      => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
        ),
        'url'           => $url,
        'defCountry'    => Geo::defaultCountry(),
        'geoCountry'    => Geo::regionTitle( ! empty($reg1_country) ? $reg1_country : Geo::defaultCountry()),
        'regionPreSuggest' => Geo::regionPreSuggest( ! empty($reg1_country) ? $reg1_country : ( ! empty($region['reg1_country']) ? $region['reg1_country'] : 0), 2),
        'geoCity'       => $region['reg3_city'],
        'itemID'        => $id,
        'imgLimit'      => Shop::imagesLimit(),
        'imgMaxSize'    => $img->getMaxSize(),
        'imgUploaded'   => $imgcnt,
        'imgData'       => $images,
    )) ?>);
    <? js::stop() ?>
</script>