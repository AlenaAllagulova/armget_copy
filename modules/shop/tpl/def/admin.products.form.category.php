<?php

?>
<table class="admtbl tbledit">
<?

# Дин.свойства
if( ! empty($dp) ) {
    ?><tbody class="j-product-dp"><?
    echo $dp;
    ?></tbody><?
}

# Цена
if( ! empty($price) ) {
    $price_curr = ( $edit ? $product['price_curr'] : ( ! empty($price_curr) ? $price_curr : Site::currencyDefault('id') ) );
    $price_title = ( mb_strlen($price_title) > 0 ? $price_title : _t('shop', 'Цена') );
?>
<tr class="j-product-price">
    <td class="row1 field-title" width="100"><?= $price_title ?></td>
    <td class="row2">
        <? if( $price ) { ?>
            <? if($price_ex & Shop::PRICE_EX_FREE) { ?><label class="radio" style="margin-bottom: 4px;"><input type="radio" name="price_ex[]" value="<?= Shop::PRICE_EX_FREE ?>" <? if($product['price_ex'] & Shop::PRICE_EX_FREE) { ?> checked="checked" <? } ?> /> <?= _t('shop', 'Бесплатно') ?></label><? } ?>
            <? if($price_ex & Shop::PRICE_EX_EXCHANGE) { ?><label class="radio" style="margin-bottom: 4px;"><input type="radio" name="price_ex[]" value="<?= Shop::PRICE_EX_EXCHANGE ?>" <? if($product['price_ex'] & Shop::PRICE_EX_EXCHANGE) { ?> checked="checked" <? } ?> /> <?= _t('shop', 'Обмен') ?></label><? } ?>
            <label class="radio inline"<? if($price_ex <= Shop::PRICE_EX_MOD) { ?> style="display: none;" <? } ?>><input type="radio" name="price_ex[]" value="<?= Shop::PRICE_EX_PRICE ?>" <? if($product['price_ex'] <= Shop::PRICE_EX_MOD) { ?> checked="checked" <? } ?> /> <?= $price_title ?>&nbsp;</label>
        <? } ?>
        <input type="text" name="price" class="input-medium" value="<?= $product['price'] ?>" />
        <select name="price_curr" class="input-mini"><?= Site::currencyOptions($price_curr) ?></select>
        <span class="j-product-price-mod">
            <? if($price_ex & Shop::PRICE_EX_MOD) { ?><label class="checkbox inline"><input type="checkbox" name="price_ex[]" value="<?= Shop::PRICE_EX_MOD ?>" <? if($product['price_ex'] & Shop::PRICE_EX_MOD) { ?> checked="checked" <? } ?> /><?= ( mb_strlen($price_title_mod) > 0 ? $price_title_mod : _t('shop', 'Торг возможен') ) ?></label><? } ?>
        </span>
    </td>
</tr>
<? } ?>
</table>