<?php
?>
<tr>
    <td class="row1"><span class="field-title">Стоимость</span><span class="required-mark">*</span>:</td>
    <td class="row2">
        <input type="text" min="0" name="price" value="<?= $v['price'] ?>" class="input-mini" /><?= $pricePrefix; ?>
    </td>
</tr>
<tr>
    <td class="row1"><span class="field-title">Период действия услуги</span><span class="required-mark">*</span>:</td>
    <td class="row2">
        <input type="text" name="period" min="1" value="<?= $v['period'] ?>" class="input-mini"><div class="help-inline">дней</div>
    </td>
</tr>