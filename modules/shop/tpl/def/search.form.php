<?php
    if( ! isset($cat_id1)) $cat_id1 = 0;
    if( ! isset($cat_id2)) $cat_id2 = 0;
?>
    <button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#j-shop-search-form-block"><i class="fa fa-cog pull-right"></i><?= _t('', 'Фильтр'); ?> <span class="small">(<?= _t('shop', 'Найдено'); ?> <span class="j-orders-count"><?= $count ?></span>)</span></button>

    <div class="l-leftColumn l-borderedBlock collapse" id="j-shop-search-form-block">
        <form action="" method="get">
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />

        <a href="#j-shop-categories" data-toggle="collapse" data-parent="#accordion" class="h6 active"><i class="fa fa-chevron-down pull-right"></i><?= _t('shop', 'Категории'); ?></a>
        <div class="collapse in j-collapse" id="j-shop-categories">
            <ul class="l-left-categories l-inside visible-md visible-lg">
                <? foreach($cats as $v): $url = Shop::url('list').$v['keyword'].'/'; ?>
                    <li<?= $v['id'] == $cat_id1 ? ' class="opened"' : ''?>><a href="<?= $url; ?>" data-subs="<?= $v['subs'] ?>" class="j-cat-title"><? if ($v['subs']){ ?><i class="fa fa-caret-<?= $v['id'] == $cat_id1 ? 'down' : 'right'?>"></i> <? } ?><?= $v['title'] ?></a>
                    <? if( ! empty($v['sub'])): ?>
                        <ul<?= $v['id'] != $cat_id1 ? ' class="hidden"' : ''?>>
                            <li<?= $v['id'] == $cat_id1 && $cat_id2 == 0 ? ' class="checked"' : '' ?>><a href="<?= $url ?>"><?= _t('', 'Все'); ?></a></li>
                            <? foreach($v['sub'] as $vv): ?>
                                <li<?= $vv['id'] == $cat_id2 ? ' class="checked"' : '' ?>><a href="<?= $url.$vv['keyword'] ?>/"><?= $vv['title'] ?></a></li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>
                    </li>
                <? endforeach; ?>
            </ul>

            <div class="collapse in" id="j-mobile-cats">
                <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                    <? foreach($cats as $v): $url = Shop::url('list').$v['keyword'].'/'; ?>
                        <li><a href="<?= $url ?>" class="j-mobile-cat" data-id="<?= $v['id']; ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                    <? endforeach; ?>
                </ul>
            </div>

            <? foreach($cats as $v): $url = Shop::url('list').$v['keyword'].'/'; ?>
            <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                    <li class="active">
                        <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= _t('form', 'Вернуться назад'); ?></span><br>
                            <?= $v['title'] ?>
                        </a>
                        <ul>
                            <li<?= $v['id'] == $cat_id1 && $cat_id2 == 0 ? ' class="checked"' : '' ?>>
                                <a href="<?= $url ?>"><?= _t('', 'Все'); ?></a>
                            </li>
                            <? foreach($v['sub'] as $vv): ?>
                            <li<?= $vv['id'] == $cat_id2 ? ' class="checked"' : '' ?>>
                                <a href="<?= $url.$vv['keyword'] ?>/"><?= $vv['title'] ?></a>
                            </li>
                            <? endforeach; ?>
                        </ul>
                    </li>
                </ul>
            </div>
            <? endforeach; ?>

        </div>

        <div id="j-shop-search-form-dp"><?= ! empty($dp) ? $dp : '' ?></div>

        <? if( Shop::regionsLimit() && ! Geo::filterEnabled()): tpl::includeJS(array('autocomplete'), true);
        $bCountry = Geo::countrySelect(); $bOpenRegion = $f['ct'] || ($bCountry && $f['c']); ?>
        <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="h6<?= $bOpenRegion ? ' active' : '' ?>"><i class="fa <?= $bOpenRegion ? 'fa-chevron-down' : 'fa-chevron-right' ?> pull-right"></i><?= _t('shop', 'Регион'); ?></a>
        <div class="collapse <?= $bOpenRegion ? 'in' : '' ?> j-collapse" id="j-left-region">
            <div class="l-inside">
                <div class="form">
                    <? if($bCountry): ?>
                        <div class="form-group">
                            <select name="c" class="form-control" id="j-left-region-country" autocomplete="off"><?= HTML::selectOptions(Geo::countryList(), $f['c'], _t('', 'Все'), 'id', 'title') ?></select>
                        </div>
                    <? endif; ?>
                    <div class="relative">
                        <input type="hidden" name="r" value="<?= $f['r'] ?>" id="j-region-pid-value" />
                        <input type="hidden" name="ct" value="<?= $f['ct'] ?>" id="j-region-city-value" />
                        <input type="text" class="form-control input-sm<?= $bCountry && empty($f['c']) ? ' hidden' : '' ?>" id="j-region-city-select" value="<?= $region_title ?>" placeholder="<?= _t('shop', 'Или введите название региона'); ?>" />
                    </div>
                </div>
            </div>
        </div>
        <? endif; ?>

        <div id="j-shop-search-form-price"><?= ! empty($pr) ? $pr : '' ?></div>

        <div class="l-inside text-center">
            <a href="#" class="ajax-link j-clear-filter"><i class="fa fa-refresh c-link-icon"></i><span><?= _t('', 'Очистить фильтр'); ?></span></a>
        </div><!-- /.l-inside -->
        </form>


    </div><!-- /.l-left-column -->
