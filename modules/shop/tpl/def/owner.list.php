<?php
/**
 * Список товаров пользователя.
 * @var $this Shop
 * @var $user array
 */
 tpl::includeJS('shop.owner.list', false, 2);
?>

    <div class="p-profileContent" id="j-shop-owner-list">

        <form method="get" action="" id="j-shop-owner-list-form">
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            <input type="hidden" name="cat" value="<?= $f['cat'] ?>" />

            <div class="p-profile-title">
                <div class="dropdown pull-left">
                    <a href="#" class="ajax-link" data-toggle="dropdown"><span class="j-f-cat-title"><?= isset($cats[ $f['cat'] ]) ? $cats[ $f['cat'] ]['title'] : '' ?></span><b class="caret"></b></a>
                    <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                        <? foreach($cats as $v): ?>
                        <li><a href="#" class="j-f-cat-select" data-id="<?= $v['id'] ?>"><?= $v['title'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <? if($user['my']): ?>
                <div class="pull-right">
                    <a class="btn btn-primary btn-sm" href="<?= Shop::url('add'); ?>"><i class="fa fa-plus-circle"></i> <?= _t('shop', 'Добавить товар'); ?></a>
                    <a class="btn btn-default btn-sm" href="<?= Shop::url('my.settings'); ?>"><i class="fa fa-cog"></i></a>
                </div>
                <? endif; ?>
                <div class="clearfix"></div>
            </div>
        </form>

        <? if( ! empty($shop_intro)): ?><p><?= nl2br($shop_intro) ?></p><? endif; ?>

        <div class="j-list"><?= $list ?></div>

        <div class="j-pagination"><?= $pgn ?></div>

        <? if( ! $count): ?><div class="alert alert-info"><?= _t('shop', 'Товары не найдены'); ?></div><? endif; ?>

    </div>
<script type="text/javascript">
<?  $aCat = array();
    foreach ($cats as $v) {
        $aCat[ $v['id'] ] = array('id'=>$v['id'], 't'=>$v['title']);
    }
    js::start(); ?>
    jShopOwnerList.init(<?= func::php2js(array(
        'lang'   => array(
            'product_delete'   => _t('shop', 'Удалить продукт?'),
            'cat_save_success' => _t('shop', 'Настройки категории сохранены'),
        ),
        'cats' => $aCat,
        'ajax' => true,
    )) ?>);
<? js::stop(); ?>
</script>