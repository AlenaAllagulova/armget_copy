<?php
/**
 * Форма настроек магазина
 * @var $this Shop
 */
tpl::includeJS('shop.settings.form', false);
?>
<div class="container">

    <section class="l-mainContent">
        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <?= tpl::getBreadcrumbs(array(
                    array('title' => _t('','Profile'),'link'=>Users::url('my.profile')),
                    array('title' => _t('shop', 'Shop'),'link'=>Shop::url('my.shop')),
                    array('title' => _t('shop', 'Settings'),'active'=>true),
                )); ?>

                <div class="p-profileContent">

                    <div class="p-profile-title">
                        <h4><?= _t('', 'Settings'); ?></h4>
                    </div>


                    <form class="form-horizontal" action="" method="post" id="j-shop-settings-form">

                        <div class="p-portfolioSection">
                            <h5><?= _t('shop', 'Вводный текст'); ?></h5>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea rows="5" class="form-control" name="shop_intro"><?= $shop_intro ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mrgt30">
                            <div class="col-sm-9 c-formSubmit">
                                <button class="btn btn-primary c-formSuccess"><?= _t('form', 'Сохранить изменения'); ?></button>
                                <a class="c-formCancel" href="<?= Shop::url('my.shop'); ?>"><?= _t('form', 'Отмена'); ?></a>
                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    </section>

</div>
<script type="text/javascript">
    <? js::start() ?>
    jShopSettingsForm.init(<?= func::php2js(array(
        'lang' => array(
            'saved_success' => _t('shop', 'Настройки успешно сохранены'),
        ),
        'url_settings' => Shop::url('my.settings'),
    )) ?>);
    <? js::stop() ?>
</script>