<form class="p-portfolioSection-edit" action="" method="post" id="j-owner-cat-form-<?= $cat_id1 ?>">
    <input type="hidden" name="cat_id1" value="<?= $cat_id1 ?>" />
    <div class="form-group">
        <textarea class="form-control" name="descr" rows="3"><?= ! empty($descr) ? HTML::escape($descr) : '' ?></textarea>
    </div>
    <? if( ! empty($cats)): ?>
    <div class="form-inline">
        <div class="form-group">
            <label class="radio">
                <input type="radio" name="order" value="1"> <?= _t('shop', 'Первый по счету'); ?>
            </label>
        </div>
        <div class="form-group">
            <label class="radio">
                <input type="radio" name="order" value="2" class="j-after-check"> <?= _t('shop', 'После другого раздела'); ?>
            </label>
        </div>
        <div class="form-group">
            <select class="form-control j-after-select" name="cat_after"><?= HTML::selectOptions($cats, 0, false, 'id', 'title'); ?></select>
        </div>
    </div>
    <? endif; ?>
    <div class="c-formSubmit">
        <button class="btn btn-primary c-formSuccess btn-sm j-submit"><?= _t('form', 'Сохранить'); ?></button>
        <a href="#" class="ajax-link j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
    </div>
</form>
