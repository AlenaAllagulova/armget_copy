<?php
/**
 * @var $this Shop
 */
?>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        var id = intval(<?= $id ?>);
        $('.j-product-delete-'+id).click(function(){
            if(confirm('<?= _t('shop', 'Удалить товар?') ?>')){
                bff.ajax(bff.ajaxURL('shop', 'product-delete'), {id:id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        document.location = '<?= Shop::url('my.shop'); ?>';
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });
        $('.j-product-hide-'+id).click(function(){
            bff.ajax(bff.ajaxURL('shop', 'product-hide'), {id:id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $('.j-product-hide-'+id).addClass('hidden');
                    $('.j-product-show-'+id).removeClass('hidden');
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });
        $('.j-product-show-'+id).click(function(){
            bff.ajax(bff.ajaxURL('shop', 'product-show'), {id:id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $('.j-product-show-'+id).addClass('hidden');
                    $('.j-product-hide-'+id).removeClass('hidden');
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });
    });
    <? js::stop(); ?>
</script>
<div class="l-inside p-profileOrder-controls-mobile mrgt0">
    <a href="<?= Shop::url('edit', array('id' => $id))?>"><i class="fa fa-edit c-link-icon"></i></a>
    <? if(bff::servicesEnabled()): ?><a href="<?= Shop::url('promote', array('id' => $id))?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i></a><? endif; ?>
    <a href="#" class="<?= $status != Shop::STATUS_ACTIVE ? 'hidden ' : '' ?>j-product-hide-<?= $id ?>" data-id="<?= $id ?>"><i class="fa fa-lock c-link-icon"></i></a>
    <a href="#" class="<?= $status != Shop::STATUS_NOT_ACTIVE ? 'hidden ' : '' ?>j-product-show-<?= $id ?>" data-id="<?= $id ?>"><i class="fa fa-unlock c-link-icon"></i></a>
    <a href="#" class="link-delete j-product-delete-<?= $id ?>"><i class="fa fa-times c-link-icon"></i></a>
</div>

<div class="l-inside p-profileOrder-controls o-controls">
    <a href="<?= Shop::url('edit', array('id' => $id))?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
    <? if(bff::servicesEnabled()): ?><a href="<?= Shop::url('promote', array('id' => $id))?>" class="link-advertise"><i class="fa fa-bullhorn c-link-icon"></i><?= _t('form', 'Рекламировать'); ?></a><? endif; ?>
    <a href="#" class="<?= $status != Shop::STATUS_ACTIVE ? 'hidden ' : '' ?>j-product-hide-<?= $id ?>" data-id="<?= $id ?>"><i class="fa fa-lock c-link-icon"></i><?= _t('shop', 'Скрыть из магазина'); ?></a>
    <a href="#" class="<?= $status != Shop::STATUS_NOT_ACTIVE ? 'hidden ' : '' ?>j-product-show-<?= $id ?>" data-id="<?= $id ?>"><i class="fa fa-unlock c-link-icon"></i><?= _t('shop', 'Отобразить'); ?></a>
    <a href="#" class="link-delete j-product-delete-<?= $id ?>"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
</div>
