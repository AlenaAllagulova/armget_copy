<?php
$urlEdit = $this->adminLink(bff::$event.'&act=edit&id=');
foreach($list as $k=>$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small<? if($v['status'] == Shop::STATUS_BLOCKED){ ?> clr-error<? } ?>"><?= $id ?></td>
        <td class="left">
            <a href="<?= Shop::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword'])) ?>" class="but linkout"></a>
            <?= $v['title'] ?>
        </td>
        <td>
            <? # для списка "на модерации", указываем причину отправления на модерацию:
            if($f['tab'] == 2) {
                if($v['moderated'] == 0) {
                    if( $v['status'] == Shop::STATUS_BLOCKED ) {
                        ?><i class="icon-ban-circle disabled" title="отредактировано пользователем после блокировки"></i><?
                    } else {
                        ?><i class="disabled" title="новый товар"></i><?
                    }
                } elseif($v['moderated'] == 2) {
                    ?><i class="icon-pencil disabled" title="отредактировано пользователем"></i><?
                }
            } ?>
        </td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink"></a>&nbsp;
            <a class="but images<? if(!$v['imgcnt']){ ?> disabled<? } ?>" href="<?= $urlEdit.$id.'&tab=images' ?>" title="фото: <?= $v['imgcnt'] ?>"></a>
            <a class="but edit product-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
            <a class="but del product-del" title="<?= _t('', 'Delete') ?>" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="5">
            ничего не найдено
        </td>
    </tr>
<? endif;