<?php
$bPromote = ! empty($bPromote);
if( ! empty($list)):
    $tagsLimit = Shop::searchTagsLimit();
    ?>
    <? if($bPromote): ?><div class="sh-shopList mrgb30"><? else: ?><ul class="sh-shopList media-list"><? endif; ?>
    <? foreach($list as $v):
        $urlView = Shop::url('view', array('id' => $v['id'], 'keyword' => $v['keyword']));
        ?>
        <<?= $bPromote ? 'div' : 'li' ?> class="media j-product<?= $v['svc_marked'] ? ' sh-highlited' : '' ?>">
            <? if($v['img_s']): ?>
            <a href="<?= $urlView ?>" class="sh-shopList-img">
                <img src="<?= $v['img_s'] ?>" alt="<?= tpl::imageAlt(array('t' => $v['title'])); ?>" />
            </a>
            <? endif; ?>
            <div class="media-body">
                <div class="sh-item-info">

                    <div class="sh-item-name">
                        <? if($v['svc_fixed'] || $bPromote): ?><i class="fa fa-thumb-tack c-icon-fixed<?= ! $v['svc_fixed'] ? ' j-fixed hidden' : '' ?>"></i><? endif; ?>
                        <a href="<?= $urlView ?>"><?= $v['title'] ?></a>
                    </div>

                    <div class="l-project-head">
                        <?= Shop::priceBlock($v); ?>
                        <? if( ! empty($v['aTags'])):  $n = 0; foreach($v['aTags'] as $vv): if(!is_array($vv)) continue; ?>
                            <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                            <a href="<?= Shop::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
                        <? endforeach; endif; ?>
                    </div>

                    <article class="sh-shopList-description">
                        <?= tpl::truncate($v['descr'], config::sysAdmin('shop.search.list.descr.truncate', 250, TYPE_UINT)); ?>
                    </article>

                    <ul class="l-item-features sh-project-features">
                        <li><?= tpl::userLink($v, 'icon', 'shop'); ?></li>
                        <li><a href="<?= Shop::url('search-cat', $v) ?>"><i class="fa fa-th-list c-link-icon"></i><?= $v['cat_title'] ?></a></li>
                        <? if( ! empty($v['city_data']['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $v['city_data']['title']?></li><? endif; ?>
                    </ul>

                </div>

            </div>

        </<?= $bPromote ? 'div' : 'li' ?>>
    <? endforeach; ?>
    </<?= $bPromote ? 'div' : 'ul' ?>>
<? else: ?>
    <div class="alert alert-info"><?= _t('shop', 'По вашему запросу в магазине еще нет готовых работ'); ?></div>
<? endif; ?>




