<div class="well well-small">
    <table class="admtbl tbledit">
        <?php echo $this->locale->buildForm($aData, 'categories-item', '
                        <tr>
                            <td class="row1 field-title span3">Заголовок цены</td>
                            <td class="row2">
                                <input type="text" class="<?= $key ?>" name="price_title[<?= $key ?>]" value="<?= HTML::escape($aData[\'price_title\'][$key]); ?>" placeholder="Цена" maxlength="50" style="width: 225px;" />
                            </td>
                        </tr>
                        ');?>
        <tr>
            <td class="row1 field-title">Валюта по-умолчанию</td>
            <td class="row2">
                <select name="price_curr" class="span2">
                    <?= Site::currencyOptions($price_curr) ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="row1 field-title">Модификатор</td>
            <td class="row2">
                <input type="checkbox" name="price_ex[]" value="<?= Shop::PRICE_EX_MOD ?>"<?php if ($price_ex & Shop::PRICE_EX_MOD){ ?> checked="checked"<?php } ?> />&nbsp;
                <?= $this->locale->formField('price_title_mod', $price_title_mod, 'text', array('maxlength' => 50, 'placeholder'=>'Торг возможен')); ?>
            </td>
        </tr>
        <tr>
            <td class="row1 field-title">Обмен</td>
            <td class="row2">
                <input type="checkbox" name="price_ex[]" value="<?= Shop::PRICE_EX_EXCHANGE ?>"<?php if ($price_ex & Shop::PRICE_EX_EXCHANGE){ ?> checked="checked"<?php } ?> />
            </td>
        </tr>
        <tr>
            <td class="row1 field-title">Бесплатно</td>
            <td class="row2">
                <input type="checkbox" name="price_ex[]" value="<?= Shop::PRICE_EX_FREE ?>"<?php if ($price_ex & Shop::PRICE_EX_FREE){ ?> checked="checked"<?php } ?> />
            </td>
        </tr>
    </table>
</div>
