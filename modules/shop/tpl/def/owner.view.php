<?php
    /**
     * @var $this Shop
     */
    $owner = User::isCurrent($user_id);
    if( ! empty($regions)){
        $aFirstRegions = reset($regions);
    }

?>
    <div class="p-profileContent">

        <? if( ! empty($prev['link']) || ! empty($next['link'])): ?>
        <div class="p-profile-title">
            <div class="p-prevnext-links">
                <? if( ! empty($prev['link'])): ?><a href="<?= $prev['link'] ?>" class="pull-left"><i class="fa fa-angle-left c-link-icon"></i><span><?= _t('shop', 'Предыдущий товар'); ?></span></a><? endif; ?>
                <? if( ! empty($next['link'])): ?><a href="<?= $next['link'] ?>" class="pull-right"><span><?= _t('shop', 'Следующий товар'); ?></span><i class="fa fa-angle-right c-link-icon"></i></a><? endif; ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <? endif; ?>

        <div class="p-portfolioSection">

            <?php if($owner) echo $this->productStatusBlock($aData); ?>

            <h1 class="small"><?= $title ?></h1>

            <div>
                <?= Shop::priceBlock($aData, $cat, false) ?>
                <? if( ! empty($tags)): foreach($tags as $v):?>
                    <a href="<?= Shop::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                <? endforeach; endif; ?>
            </div>

            <article class="sh-shop-item-text">
                <p><?= nl2br($descr); ?></p>
            </article>

            <? if($imgcnt): ?>
            <div class="sh-item-gallery">
                <? foreach($images as $k => $v): ?>
                    <a href="<?= $v['i'][ ShopProductImages::szView ]; ?>" class="fancyzoom" rel="fancy-gallery-<?= $id ?>">
                        <img src="<?= $v['i'][ ShopProductImages::szThumbnail ]; ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" />
                    </a>
                <? endforeach ?>
                <script type="text/javascript">
                    <?
                        tpl::includeJS('fancybox2', true);
                        js::start(); ?>
                    $(function(){
                        $('.fancyzoom').fancybox({
                            openEffect	: 'none',
                            closeEffect	: 'none',
                            nextEffect  : 'fade',
                            prevEffect : 'fade',
                            helpers: {
                                overlay: {locked: false}
                            }
                        });

                    });
                    <? js::stop(); ?>
                </script>
            </div>
            <? endif; ?>

            <? if( ! empty($dynprops)): ?><div class="l-inside l-dymanic-features"><?= $dynprops ?></div><? endif; ?>

            <? if( ! empty($aFirstRegions['title'])): ?>
                <ul class="l-item-features sh-project-features">
                    <li><i class="fa fa-map-marker"></i> <?= $aFirstRegions['title']?></li>
                </ul>
            <? endif; ?>

        </div>

    </div>

    <?= $owner ? $this->viewPHP($aData, 'owner.view.controls') : '' ?>

    <a href="<?= Shop::url('user.listing', array('login' => $login)); ?>"><i class="fa fa-chevron-left c-link-icon mrgt30"></i><?= _t('shop', 'Вернуться в магазин'); ?></a>