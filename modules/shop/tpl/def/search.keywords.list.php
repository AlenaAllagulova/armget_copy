<?php
if( ! isset($nTagID)) $nTagID =0;
if( ! empty($list)):
    ?>
    <ul class="sh-shopList media-list l-search-results">
        <? foreach($list as $v):
            $urlView = Shop::url('view', array('id' => $v['id'], 'keyword' => $v['keyword']));
            ?>
            <li class="media">
                <div class="l-list-num"><?= $v['n'] ?>.</div>
                <? if($v['img_s']): ?>
                    <a href="<?= $urlView ?>" class="sh-shopList-img">
                        <img src="<?= $v['img_s'] ?>" alt="<?= tpl::imageAlt(array('t' => $v['title'])); ?>" />
                    </a>
                <? endif; ?>
                <div class="media-body">
                    <div class="sh-item-info">

                        <div class="sh-item-name">
                            <a href="<?= $urlView ?>"><?= $v['title'] ?></a>
                        </div>

                        <div class="l-project-head">
                            <?= Shop::priceBlock($v); ?>
                            <? if( ! empty($v['aTags'])): foreach($v['aTags'] as $vv): if(!is_array($vv)) continue;
                                $bSearch = $nTagID == $vv['id'];
                                if( ! $bSearch && $f['q']){
                                    $bSearch = $f['q'] == $vv['tag'];
                                }
                                ?>
                                <a href="<?= Shop::url('search-tag', $vv) ?>" class="l-tag<?= $bSearch ? ' l-search-tag' : '' ?>"><?= $vv['tag'] ?></a>
                            <? endforeach; endif; ?>
                        </div>

                        <article class="sh-shopList-description">
                            <?= nl2br(tpl::truncate($v['descr'], config::sysAdmin('shop.search.keywords.list.descr.truncate', 250, TYPE_UINT))); ?>
                        </article>

                        <ul class="l-item-features sh-project-features">
                            <li><?= tpl::userLink($v, 'icon', 'shop')?></li>
                            <li><a href="<?= Shop::url('search-cat', $v) ?>"><i class="fa fa-th-list c-link-icon"></i><?= $v['cat_title'] ?></a></li>
                        </ul>
                    </div>
                </div>
            </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('shop', 'Товары, отвечающие вашему запросу, не найдены'); ?></div>
<? endif;
