<?php

?>
<?= tplAdmin::blockStart(_t('shop', 'Shop').' / '._t('shop','Добавление товара'), false, array('id'=>'ShopProductsFormBlock','style'=>'display:none;')); ?>
<div id="ShopProductsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('shop', 'Shop').' / '._t('shop','Товары'), true, array('id'=>'ShopProductsListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>_t('shop','+ добавить товар'),'class'=>'ajax','onclick'=>'return jShopProductsFormManager.action(\'add\',0);'),
    array(
        array('title'=>_t('', 'пересчет счетчиков товаров пользователей'), 'onclick'=>"return bff.confirm('sure', {r:'".$this->adminLink(bff::$event.'&act=dev-users-counters-update')."'})", 'icon'=>'icon-refresh', 'debug-only'=>true),
)); ?>
<?
$aTabs = array(
    0 => array('t'=>_t('shop','Активные')),
    1 => array('t'=>_t('shop','Неактивные')),
    2 => array('t'=>_t('shop','На модерации'), 'counter'=>config::get('shop_products_moderating', 0)),
    3 => array('t'=>_t('shop','Заблокированные')),
    4 => array('t'=>_t('shop','Все')),
);
?>
<div class="tabsBar" id="OrdersOrdersListTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab <? if($f['tab']==$k) { ?>tab-active<? } ?>"><a href="#" onclick="return jShopProductsList.onTab(<?= $k ?>,this);" <?= (!empty($v['c']) ? $v['c'] : '') ?>><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
    <? } ?>
    <div id="ShopProductsProgress" class="progress" style="display: none;"></div>
</div>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="ShopProductsListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="order" value="<?= $f['order'] ?>" />
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>" />

        <div class="left">
            <div class="left">
                <select name="cat" style="width: 175px;" onchange="jShopProductsList.refresh();"><?= $cats ?></select>
                <input style="width:145px;" type="text" maxlength="150" name="title" placeholder="<?= _t('shop', 'ID / Заголовок товара') ?>" value="<?= HTML::escape($f['title']) ?>" />
                <input style="width:155px;" type="text" maxlength="150" name="user" placeholder="<?= _t('shop', 'ID / логин / E-mail пользователя') ?>" value="<?= HTML::escape($f['user']) ?>" />
                <input type="hidden" name="tag" value="<?= $f['tag'] ?>" />
                <div style="display: inline-block;"><input type="text" style="width:140px;" id="j-f-tag" class="autocomplete" placeholder="<?= _t('shop', 'Тег') ?>" value="<?= ( ! empty($tag) ? HTML::escape($tag['tag']) : '' ) ?>" /></div>
                <input type="button" class="btn btn-small button cancel" onclick="jShopProductsList.submit(false);" value="<?= _t('', 'search') ?>" />
                <a class="ajax cancel" onclick="jShopProductsList.submit(true); return false;"><?= _t('', 'reset') ?></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="ShopProductsListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70"><?= _t('', 'ID') ?></th>
        <th class="left"><?= _t('shop', 'Заголовок') ?></th>
        <th></th>
        <th width="120">
            <a href="javascript: jShopProductsList.onOrder('created');" class="ajax"><?= _t('', 'Created') ?></a>
            <div class="order-<?= $f['order_dir'] ?>" <? if($f['order_by']!='created') { ?>style="display:none;"<? } ?> id="products-order-created"></div>
        </th>
        <th width="135"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="ShopProductsList">
    <?= $list ?>
    </tbody>
</table>
<div id="ShopProductsListPgn"><?= $pgn ?></div>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
var jShopProductsFormManager = (function(){
    var $progress, $block, $blockCaption, $formContainer, process = false;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $formContainer = $('#ShopProductsFormContainer');
        $progress = $('#ShopProductsProgress');
        $block = $('#ShopProductsFormBlock');
        $blockCaption = $block.find('span.caption');

        <? if(!empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jShopProductsList.toggle(false);
            if(jShopProductsForm) jShopProductsForm.onShow();
        } else {
            jShopProductsList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? '<?= _t('shop', 'Shop') ?> / <?= _t('shop', 'Добавление товара') ?>' : '<?= _t('shop', 'Shop') ?> / <?= _t('shop', 'Редактирование товара') ?>'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
            } else {
                jShopProductsList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        <? $tab = $this->input->get('tab', TYPE_NOTAGS); ?>
        params = $.extend(params || {}, {act:type<?= ! empty($tab) ? ',tab:\''.$tab.'\'' : '' ?>});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
                jShopProductsList.refresh();
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jShopProductsList = (function(){
    var $progress, $block, $list, $listTable, $listPgn, filters, processing = false;
    var tab = <?= $f['tab'] ?>, tagAC;
    var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';
    var orders = <?= func::php2js($orders) ?>, orderby = '<?= $f['order_by'] ?>';

    $(function(){
        $progress  = $('#ShopProductsProgress');
        $block     = $('#ShopProductsListBlock');
        $list      = $block.find('#ShopProductsList');
        $listTable = $block.find('#ShopProductsListTable');
        $listPgn   = $block.find('#ShopProductsListPgn');
        filters    = $block.find('#ShopProductsListFilters').get(0);

        $list.delegate('a.product-edit', 'click', function(){
            var id = intval($(this).attr('rel'));
            if(id>0) jShopProductsFormManager.action('edit',id);
            return false;
        });

        $list.delegate('a.product-toggle', 'click', function(){
            var id = intval($(this).attr('rel'));
            var type = $(this).data('type');
            if(id>0) {
                var params = {progress: $progress, link: this};
                bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
            }
            return false;
        });

        $list.delegate('a.product-del', 'click', function(){
            var id = intval($(this).attr('rel'));
            if(id>0) del(id, this);
            return false;
        });

        tagAC = $.autocomplete($block.find('#j-f-tag'), '<?= $this->adminLink('ajax&act=tags-autocomplete') ?>',
            {valueInput: $block.find('[name="tag"]'),
                onSelect: function(id, title, ex){
                    jShopProductsList.submit();
                }});

        $(window).bind('popstate',function(){
            if('state' in window.history && window.history.state === null) return;
            var loc = document.location;
            var actForm = /act=(add|edit)/.exec( loc.search.toString() );
            if( actForm!=null ) {
                var actId = /id=([\d]+)/.exec(loc.search.toString());
                jShopProductsFormManager.action(actForm[1], actId && actId[1]);
            } else {
                jShopProductsFormManager.action('cancel');
                updateList(false);
            }
        });

    });

    function isProcessing()
    {
        return processing;
    }

    function del(id, link)
    {
        bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
        return false;
    }

    function updateList(updateUrl)
    {
        if(isProcessing()) return;
        var f = $(filters).serialize();
        bff.ajax(ajaxUrl, f, function(data){
            if(data) {
                $list.html( data.list );
                $listPgn.html( data.pgn );
                if(updateUrl !== false && bff.h) {
                    window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                }
            }
        }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
    }

    function setPage(id)
    {
        filters.page.value = intval(id);
    }

    return {
        submit: function(resetForm)
        {
            if(isProcessing()) return;
            setPage(1);
            if(resetForm) {
                filters['cat'].value = 0;
                filters['title'].value = '';
                filters['user'].value = '';
                tagAC.reset();
            }
            updateList();
        },
        page: function (id)
        {
            if(isProcessing()) return;
            setPage(id);
            updateList();
        },
        onTab: function(tabNew, link)
        {
            if(isProcessing() || tabNew == tab) return false;
            setPage(1);
            tab = filters.tab.value = tabNew;
            updateList();
            $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
            return false;
        },
        onOrder: function(by)
        {
            if(isProcessing() || !orders.hasOwnProperty(by))
                return;

            orders[by] = (orders[by] == 'asc' ? 'desc' : 'asc');
            $('#products-order-'+orderby).hide();
            orderby = by;
            $('#products-order-'+orderby).removeClass('order-asc order-desc').addClass('order-'+orders[by]).show();

            filters.order.value = orderby+'-'+orders[by];
            setPage(1);

            updateList();
        },
        refresh: function(resetPage,updateUrl)
        {
            if(resetPage) setPage(0);
            updateList(updateUrl);
        },
        toggle: function(show)
        {
            if(show === true) {
                $block.show();
                if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
            }
            else $block.hide();
        }
    };
}());

</script>