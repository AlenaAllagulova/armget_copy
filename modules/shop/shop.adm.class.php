<?php

/**
 * Права доступа группы:
 *  - shop: Магазин
 *      - products: Управление товарами (список, добавление, редактирование, удаление)
 *      - products-moderate: Модерация товаров
 *      - tags: Управление тегами (список, добавление, редактирование, удаление)
 *      - categories: Управление категориями (список, добавление, редактирование, удаление)
 *      - svc: Управление услугами
 *      - settings: Настройки
 *      - seo: SEO
 */
class Shop_ extends ShopBase
{
    #---------------------------------------------------------------------------------------
    # товары

    public function products()
    {
        if (!$this->haveAccessTo('products')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add': # добавление
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateProductData(0, $bSubmit);
                    if ($bSubmit) {

                        if ($this->errors->no()) {
                            $aData['moderated'] = 1;
                            $nProductID = $this->model->productSave(0, $aData, 'd');
                            if ($nProductID > 0) {
                                $this->productImages($nProductID)->saveTmp('img');
                            }
                            $this->productTags()->tagsSave($nProductID);
                        }
                        break;
                    }
                    $aData['id'] = 0;
                    $aData['cats'] = $this->model->categoriesOptionsByLevel($this->model->categoryParentsID($aData['cat_id']), array('empty' => 'Выбрать'));
                    $aData['images'] = array();
                    $aData['imgcnt'] = 0;
                    $aData['img'] = $this->productImages(0);
                    $aData['act'] = $sAct;

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.products.form');
                }
                break;
                case 'edit': # редактирование
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nProductID = $this->input->postget('id', TYPE_UINT);
                    if (!$nProductID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {

                        $aData = $this->validateProductData($nProductID, $bSubmit);
                        if ($this->errors->no()) {
                            $this->model->productSave($nProductID, $aData, 'd');
                            $this->productTags()->tagsSave($nProductID);
                        }
                        $aData['id'] = $nProductID;
                        break;
                    }
                    $aData = $this->model->productData($nProductID, array(), true);
                    if (empty($aData)) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData['cats'] = $this->model->categoriesOptionsByLevel($this->model->categoryParentsID($aData['cat_id']), array('empty' => 'Выбрать'));
                    $aData['cat'] = $this->productFormByCategory($aData['cat_id'], $aData);
                    $aData['img'] = $this->productImages($nProductID);
                    $aData['images'] = $aData['img']->getData($aData['imgcnt']);
                    $aData['act'] = $sAct;

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.products.form');
                    if (Request::isAJAX()) $this->ajaxResponseForm($aResponse, 2, true);
                }
                break;
                case 'toggle': # переключатели
                {
                    $nProductID = $this->input->postget('id', TYPE_UINT);
                    if (!$nProductID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);
                    $this->model->productToggle($nProductID, $sToggleType);
                }
                break;
                case 'category-data': # данные о категории
                {
                    $nCategoryID = $aResponse['id'] = $this->input->post('cat_id', TYPE_UINT);
                    if (empty($nCategoryID)) {
                        $this->errors->unknownRecord();
                        break;
                    }
                    $aResponse = $this->productFormByCategory($nCategoryID);
                }
                break;
                case 'delete': # удаление
                {

                    $nProductID = $this->input->postget('id', TYPE_UINT);
                    if (!$nProductID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->productData($nProductID, array(), true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->productDelete($nProductID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                break;
                case 'user': # пользователь
                {
                    $sEmail = $this->input->post('q', TYPE_NOTAGS);
                    $sEmail = $this->input->cleanSearchString($sEmail);
                    $aUsers = Users::model()->usersList(array(
                            ':email'    => array(
                                (Users::model()->userEmailCrypted() ? 'BFF_DECRYPT(email)' : 'email') . ' LIKE :email',
                                ':email' => $sEmail . '%'
                            ),
                            'blocked'   => 0,
                            'activated' => 1,
                            'type'      => Users::TYPE_WORKER,
                        ), array('user_id', 'email')
                    );
                    $this->autocompleteResponse($aUsers, 'user_id', 'email');

                }
                break;
                case 'img-upload': # изображения: загрузка
                {

                    $nProductID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->productImages($nProductID);
                    $mResult = $oImages->uploadQQ();
                    $aResponse = array('success' => ($mResult !== false && $this->errors->no()));

                    if ($mResult !== false) {
                        $aResponse = array_merge($aResponse, $mResult);
                        $aResponse = array_merge($aResponse,
                            $oImages->getURL($mResult,
                                array(ShopProductImages::szSmall, ShopProductImages::szView),
                                empty($nProductID)
                            )
                        );
                    }
                    $aResponse['errors'] = $this->errors->get();
                    $this->ajaxResponse($aResponse, true, false, true);
                }
                break;
                case 'img-saveorder': # изображения: сохранение порядка
                {

                    $nProductID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->productImages($nProductID);
                    $img = $this->input->post('img', TYPE_ARRAY);
                    if (!$oImages->saveOrder($img, false, true)) {
                        $this->errors->impossible();
                    }
                }
                break;
                case 'img-delete': # изображения: удаление
                {

                    $nProductID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->productImages($nProductID);
                    $nImageID = $this->input->post('image_id', TYPE_UINT);
                    $sFilename = $this->input->post('filename', TYPE_STR);
                    if (!$nImageID && empty($sFilename)) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($nImageID) {
                        $oImages->deleteImage($nImageID);
                    } else {
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'img-delete-all': # изображения: удаление всех изображений
                {

                    $nProductID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->productImages($nProductID);
                    if ($nProductID) {
                        $oImages->deleteAllImages(true);
                    } else {
                        $sFilename = $this->input->post('filenames', TYPE_ARRAY_STR);
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'dev-users-counters-update': # пересчет счетчиков товаров пользователей
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }

                    $this->model->userCatsProductsCount();
                    # обновляем счетчик товаров "на модерации"
                    $this->moderationCounterUpdate();

                    $this->adminRedirect(($this->errors->no() ? Errors::SUCCESS : Errors::IMPOSSIBLE), bff::$event);
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = $this->input->postgetm(array(
            'page'  => TYPE_UINT,
            'tab'   => TYPE_UINT,
            'cat'   => TYPE_UINT,
            'title' => TYPE_NOTAGS,
            'user'  => TYPE_NOTAGS,
            'tag'   => TYPE_UINT,
        ));

        # формируем фильтр списка товаров
        $sql = array();
        $mPerpage = 15;
        $aData['pgn'] = '';

        $aData['orders'] = array('created'=>'desc',);
        $f += $this->prepareOrder($orderBy, $orderDirection, 'created-desc', $aData['orders']);
        $f['order'] = $orderBy . '-' . $orderDirection;
        $sqlOrder = "$orderBy $orderDirection";

        switch ($f['tab']) {
            case 0: # Активные
            {
                $sql['status'] = static::STATUS_ACTIVE;
                if (static::premoderation()) {
                    $sql[':mod'] = 'P.moderated > 0';
                }
            }
            break;
            case 1: # Неактивные
            {
                $sql['status'] = static::STATUS_NOT_ACTIVE;
            }
            break;
            case 2: # На модерации
            {
                $sql[':mod'] = 'P.moderated != 1';
            }
            break;
            case 3: # Заблокированные
            {
                $sql['status'] = static::STATUS_BLOCKED;
            }
            break;
            case 4: # Все
            {
            }
            break;
        }

        if ($f['cat'] > 0) {
            $sql[':cat_id'] = array('(P.cat_id2 = :cat OR P.cat_id1 = :cat)', ':cat' => $f['cat']);
        }
        if (!empty($f['title'])) {
            $sql[':title'] = array(
                '(P.id = ' . intval($f['title']) . ' OR P.title LIKE :title)',
                ':title' => '%' . $f['title'] . '%'
            );
        }
        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(P.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }
        if ($f['tag']) {
            $aData['tag'] = $this->productTags()->tagData($f['tag']);
            if (!empty($aData['tag'])) {
                $sql[':tag'] = $f['tag'];
            }
        }

        $aData['f'] = $f;
        $nCount = $this->model->productsListing($sql, true);
        $oPgn = new Pagination($nCount, $mPerpage, '#', 'jShopProductsList.page('.Pagination::PAGE_ID.'); return false;');
        $oPgn->pageNeighbours = 6;
        $aData['pgn'] = $oPgn->view(array('arrows'=>false));
        $aData['list'] = $this->model->productsListing($sql, false, $oPgn->getLimitOffset(), $sqlOrder);
        $aData['list'] = $this->viewPHP($aData, 'admin.products.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                    'pgn'  => $aData['pgn'],
                ), 2, true
            );
        }

        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        $aData['cats'] = $this->model->categoriesOptions($f['cat'], array('Все категории'));
        tpl::includeJS(array('ui.sortable', 'qquploader', 'autocomplete', 'autocomplete.fb'), true);

        return $this->viewPHP($aData, 'admin.products.listing');
    }

    public function product_status()
    {
        if ( ! $this->haveAccessTo('products-moderate')) {
            $this->errors->accessDenied();
        }


        $productID = $this->input->post('id', TYPE_UINT);
        if (!$productID) {
            $this->errors->unknownRecord();
        } else {
            $aData = $this->model->productData($productID, array(), true);
            if (empty($aData)) {
                $this->errors->unknownRecord();
            }
        }
        if ( ! $this->errors->no()) {
            $this->ajaxResponseForm();
        }

        $aResponse = array();
        switch ($this->input->postget('act', TYPE_STR))
        {
            case 'approve': # модерация: одобрение
            {
                $aUpdate = array(
                    'moderated' => 1
                );

                if ($aData['status'] == self::STATUS_BLOCKED) {
                    /**
                     * В случае если "Одобряем" заблокированный товар
                     * => значит он после блокировки был отредактирован пользователем
                     */
                    $aUpdate[] = 'status_prev = status';
                    $aUpdate['status'] = ( in_array($aData['status_prev'], array(
                        self::STATUS_ACTIVE,
                        self::STATUS_NOT_ACTIVE,
                    )) ? $aData['status_prev'] : self::STATUS_ACTIVE );
                    $aUpdate['status_changed'] = $this->db->now();
                }

                $res = $this->model->productSave($productID, $aUpdate);
                $this->model->categoryUserActiveCount($aData['user_id'], $aData['cat_id1']);

                if (empty($res)) {
                    $this->errors->impossible();
                }

                # обновляем счетчик товаров "на модерации"
                $this->moderationCounterUpdate();

                # Отправим письмо исполнителю
                Users::sendMailTemplateToUser($aData['user_id'], 'shop_product_approved', array(
                    'product_id' => $productID,
                    'product_title' => $aData['title'],
                    'product_url' => static::url('view', array('id'=>$productID,'keyword'=>$aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            }
            break;
            case 'activate': # модерация: активация
            {
                if ($aData['status'] != self::STATUS_NOT_ACTIVE) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_ACTIVE,
                    'status_changed' => $this->db->now(),
                    'moderated' => 1,
                );

                $res = $this->model->productSave($productID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                $this->model->categoryUserActiveCount($aData['user_id'], $aData['cat_id1']);
                # обновляем счетчик товаров "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'deactivate': # модерация: деактивация
            {
                if ($aData['status'] != self::STATUS_ACTIVE) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_NOT_ACTIVE,
                    'status_changed' => $this->db->now(),
                    'moderated' => 1,
                );

                $res = $this->model->productSave($productID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                $this->model->categoryUserActiveCount($aData['user_id'], $aData['cat_id1']);
                # обновляем счетчик товаров "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'block': # модерация: блокировка / разблокировка
            {
                /**
                 * Блокировка товара (если уже заблокирован => изменение причины блокировки)
                 * @param string 'blocked_reason' причина блокировки
                 * @param integer 'id' ID товара
                 */

                $bUnblock = $this->input->post('unblock', TYPE_UINT);
                $sBlockedReason = $this->input->postget('blocked_reason', TYPE_NOTAGS, array('len' => 1000, 'len.sys' => 'shop.blocked_reason.limit'));
                $bBlocked = ($aData['status'] == self::STATUS_BLOCKED);

                if ($aData['user_id']) {
                    $aUserData = Users::model()->userData($aData['user_id'], array('user_id', 'blocked'));
                    if (!empty($aUserData['blocked'])) {
                        $this->errors->set('Для блокировки/разблокировки товара, разблокируйте аккаунт владельца данного товара');
                        break;
                    }
                }

                $aUpdate = array(
                    'moderated'      => 1,
                    'blocked_reason' => $sBlockedReason,
                    'status_changed' => $this->db->now(),
                );

                $bBlockedResult = $bBlocked;
                if (!$bBlocked) { # блокируем
                    $aUpdate['status_prev'] = $aData['status'];
                    $aUpdate['status'] = self::STATUS_BLOCKED;
                    $bBlockedResult = true;

                    # Отправим письмо исполнителю
                    Users::sendMailTemplateToUser($aData['user_id'], 'shop_product_blocked', array(
                        'product_id' => $productID,
                        'product_title' => $aData['title'],
                        'product_url' => static::url('view', array('id'=>$productID, 'keyword'=>$aData['keyword'])),
                        'blocked_reason' => nl2br($sBlockedReason),
                    ), Users::ENOTIFY_GENERAL);
                } else {
                    if ($bUnblock) {
                        # разблокируем
                        switch ($aData['status_prev']) {
                            case self::STATUS_ACTIVE:
                            case self::STATUS_NOT_ACTIVE:
                                $aUpdate['status'] = $aData['status_prev'];
                            break;
                            case self::STATUS_BLOCKED:
                                $aUpdate['status'] = self::STATUS_NOT_ACTIVE;
                            break;
                        }
                        $aUpdate['status_prev'] = self::STATUS_BLOCKED;
                        $bBlockedResult = false;

                        # Отправим письмо исполнителю
                        Users::sendMailTemplateToUser($aData['user_id'], 'shop_product_approved', array(
                            'product_id' => $productID,
                            'product_title' => $aData['title'],
                            'product_url' => static::url('view', array('id'=>$productID,'keyword'=>$aData['keyword'])),
                        ), Users::ENOTIFY_GENERAL);
                    }
                }

                $res = $this->model->productSave($productID, $aUpdate);
                if (!$res) {
                    $this->errors->impossible();
                    break;
                }
                $this->model->categoryUserActiveCount($aData['user_id'], $aData['cat_id1']);
                # обновляем счетчик товаров "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['blocked'] = $bBlockedResult;
                $aResponse['reason'] = $sBlockedReason;
            }
            break;
        }

        if ($this->errors->no()) {
            $aData = $this->model->productData($productID, array(), true);
            if ( ! empty($aData)) {
                $aData['only_form'] = true;
                $aResponse['html'] = $this->viewPHP($aData, 'admin.products.form.status');
            }
        }
        $this->ajaxResponseForm($aResponse);
    }

    #---------------------------------------------------------------------------------------
    # категории

    public function categories()
    {
        if (!$this->haveAccessTo('categories')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add': # добавление
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateCategoryData(0, $bSubmit);
                    if ($bSubmit) {

                        if ($this->errors->no()) {
                            $nCategoryID = $this->model->categorySave(0, $aData);
                            if ($nCategoryID > 0) {
                            }
                        }
                    }

                    $aData['id'] = 0;
                    $aData['priceForm'] = $this->viewPHP($aData, 'admin.categories.form.price');

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                }
                break;
                case 'edit': # редактирование
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {

                        $aData = $this->validateCategoryData($nCategoryID, $bSubmit);

                        if ($this->errors->no()) {
                            $this->model->categorySave($nCategoryID, $aData);
                        }
                        $aData['id'] = $nCategoryID;
                    } else {
                        $aData = $this->model->categoryData($nCategoryID, array(), true);
                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                    }

                    $aData['pid_path'] = $this->model->categoryParentsTitle($nCategoryID);
                    $aData['priceForm'] = $this->viewPHP($aData, 'admin.categories.form.price');

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                }
                break;
                case 'expand': # просмотр подкатегорий
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData['list'] = $this->model->categoriesListing(array('pid' => $nCategoryID));
                    $aData['skip_norecords'] = false;
                    $aResponse['list'] = $this->viewPHP($aData, 'admin.categories.listing.ajax');
                    $aResponse['cnt'] = sizeof($aData['list']);
                }
                break;
                case 'toggle': # переключатели
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);
                    $this->model->categoryToggle($nCategoryID, $sToggleType);
                    $aResponse['refresh'] = ($sToggleType == 'enabled' &&
                        $this->model->categorySubCount($nCategoryID) > 0);
                }
                break;
                case 'rotate': # изменение порядка
                {

                    $this->model->categoriesRotate();
                }
                break;
                case 'delete': # удаление
                {

                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->categoryData($nCategoryID, array());
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->categoryDelete($nCategoryID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    } else {
                    }
                }
                break;
                case 'dev-treevalidate': # проверка целосности NestedSets
                {
                    if (!FORDEV || !BFF_DEBUG) {
                        return $this->showAccessDenied();
                    }

                    set_time_limit(0);
                    ignore_user_abort(true);

                    return $this->model->treeCategories->validate(true);
                }
                break;
                case 'price-form':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nCategoryID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = $this->model->categoryData($nCategoryID, array(), true);
                    if (empty($aData)) {
                        $this->errors->unknownRecord();
                        break;
                    }
                    $aResponse['price'] = $aData['price'];
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form.price');
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = array();
        $this->input->postgetm(array(
                'page' => TYPE_UINT,
            ), $f
        );

        # формируем фильтр списка категорий
        $sql = array();
        $sqlOrder = 'numleft';
        $aData['pgn'] = '';

        $sExpandState = $this->input->cookie(bff::cookiePrefix() . 'shop_categories_expand', TYPE_STR);
        $aExpandID = (!empty($sExpandState) ? explode('.', $sExpandState) : array());
        $aExpandID = array_map('intval', $aExpandID);
        $aExpandID[] = ShopModel::CATEGORIES_ROOTID;
        $sql[] = 'pid IN (' . join(',', $aExpandID) . ')';

        $aData['list'] = $this->model->categoriesListing($sql, false, '', $sqlOrder);

        $aData['list'] = $this->viewPHP($aData, 'admin.categories.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                    'pgn'  => $aData['pgn'],
                )
            );
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        tpl::includeJS(array('tablednd','wysiwyg'), true);

        return $this->viewPHP($aData, 'admin.categories.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nCategoryID ID категории или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateCategoryData($nCategoryID, $bSubmit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langCategories, $aData);
        $this->input->postm(array(
                'pid'        => TYPE_UINT,       # Основной раздел
                'keyword'    => TYPE_NOTAGS,     # URL-Keyword
                'enabled'    => TYPE_BOOL,       # Включен
                'price'      => TYPE_BOOL,       # Цена: настройки
                'price_ex'   => TYPE_ARRAY_UINT, # Цена: настройки
                'price_curr' => TYPE_INT,        # Цена: валюта
                # SEO: Использовать общий шаблон
                'shop_mtemplate'   => TYPE_BOOL, # Магазин
                'orders_mtemplate' => TYPE_BOOL, # Заказы: тип "товар"
            ), $aData
        );

        $aData['price_ex'] = array_sum($aData['price_ex']);

        if ($bSubmit) {
            # URL-Keyword
            $aData['keyword'] = $this->db->getKeyword($aData['keyword'], $aData['title'][LNG], TABLE_SHOP_CATS, $nCategoryID, 'keyword', 'id');
        } else {
            if ( ! $nCategoryID) {
                $aData['shop_mtemplate'] = 1;
                $aData['orders_mtemplate'] = 1;
            }
        }

        return $aData;
    }

    #---------------------------------------------------------------------------------------
    # теги

    public function tags()
    {
        if (!$this->haveAccessTo('tags')) {
            return $this->showAccessDenied();
        }

        return $this->productTags()->manage();
    }

    public function ajax()
    {
        if (!$this->security->haveAccessToAdminPanel()) {
            return $this->showAccessDenied();
        }

        switch ($this->input->getpost('act', TYPE_STR))
        {
            case 'tags-suggest': # autocomplete.fb
            {
                $sQuery = $this->input->postget('tag', TYPE_NOTAGS);
                $this->productTags()->tagsAutocomplete($sQuery);
            }
            break;
            case 'tags-autocomplete': # autocomplete
            {
                $sQuery = $this->input->post('q', TYPE_NOTAGS);
                $this->productTags()->tagsAutocomplete($sQuery);
            }
            break;
        }

        $this->ajaxResponse(Errors::IMPOSSIBLE);
    }

    # ------------------------------------------------------------------------------------------------------------------------------
    # Услуги

    public function svc_services()
    {
        if (!$this->haveAccessTo('svc')) {
            return $this->showAccessDenied();
        }

        $svc = Svc::model();

        if (Request::isPOST()) {
            $aResponse = array();

            switch ($this->input->getpost('act')) {
                case 'update':
                {

                    $nSvcID = $this->input->post('id', TYPE_UINT);
                    if (!$nSvcID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData = $svc->svcData($nSvcID, array('id', 'type', 'keyword'));
                    if (empty($aData) || $aData['type'] != Svc::TYPE_SERVICE) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $this->svcValidateData($nSvcID, Svc::TYPE_SERVICE, $aDataSave);

                    if ($this->errors->no()) {
                        # загружаем иконки
                        $oIcon = self::svcIcon($nSvcID);
                        $oIcon->setAssignErrors(false);
                        foreach ($oIcon->getVariants() as $iconField => $v) {
                            $oIcon->setVariant($iconField);
                            $aIconData = $oIcon->uploadFILES($iconField, true, false);
                            if (!empty($aIconData)) {
                                $aDataSave[$iconField] = $aIconData['filename'];
                            } else {
                                if ($this->input->post($iconField . '_del', TYPE_BOOL)) {
                                    if ($oIcon->delete(false)) {
                                        $aDataSave[$iconField] = '';
                                    }
                                }
                            }
                        }

                        # сохраняем
                        $svc->svcSave($nSvcID, $aDataSave);
                    }

                }
                break;
                case 'reorder': # сортировка услуг
                {

                    $aSvc = $this->input->post('svc', TYPE_ARRAY_UINT);
                    $svc->svcReorder($aSvc, Svc::TYPE_SERVICE);
                }
                break;
                default:
                {
                    $this->errors->impossible();
                }
                break;
            }

            $this->iframeResponseForm($aResponse);
        }

        $aData = array(
            'svc' => $svc->svcListing(Svc::TYPE_SERVICE, $this->module_name),
        );
        return $this->viewPHP($aData, 'admin.svc.services');
    }

    /**
     * Проверка данных услуги
     * @param string $nSvcID ID услуги
     * @param integer $nType тип Svc::TYPE_
     * @param array $aData @ref проверенные данные
     */
    protected function svcValidateData($nSvcID, $nType, &$aData)
    {
        $aParams = array(
            'price' => TYPE_PRICE,
        );

        if ($nType == Svc::TYPE_SERVICE) {
            $aSettings = array(
                'on'     => TYPE_BOOL, # включена
            );

            switch($nSvcID){
                case static::SVC_MARK:
                    $aSettings['period'] = TYPE_UINT; # дней
                    break;
                case static::SVC_FIX:
                    $aSettings['per_day'] = TYPE_UINT; # цена за день
                    $aSettings['period']  = TYPE_UINT; # дней
                    break;
            }

            $aData = $this->input->postm($aParams);
            $aData['settings'] = $this->input->postm($aSettings);
            $this->input->postm_lang($this->model->langSvcServices, $aData['settings']);
            $aData['title'] = $aData['settings']['title_view'][LNG];

            switch($nSvcID){
                case static::SVC_MARK:
                case static::SVC_FIX:
                    if( ! $aData['settings']['period']){
                        $aData['settings']['period'] = 1;
                    }
                    break;
            }

        }
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

}