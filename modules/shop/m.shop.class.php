<?php

class M_Shop_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('shop', 'Shop');
        $module = 'shop';

        # Товары
        if ($security->haveAccessToModuleToMethod($module, 'products')) {
            $menu->assign($menuTitle, _t('shop','Товары'), $module, 'products', true, 10,
                array('rlink' => array('event' => 'products&act=add'), 'counter' => 'shop_products_moderating')
            );
            $menu->adminHeaderCounter($menuTitle, 'shop_products_moderating', $module, 'products&tab=2', 3, '', array('parent'=>'moderation'));
        }

        # Теги
        if ($security->haveAccessToModuleToMethod($module, 'tags')) {
            $menu->assign($menuTitle, _t('','Tags'), $module, 'tags', true, 15);
        }

        # Категории
        if ($security->haveAccessToModuleToMethod($module, 'categories')) {
            $menu->assign($menuTitle, _t('shop','Категории'), $module, 'categories', true, 11,
                array('rlink' => array('event' => 'categories&act=add'))
            );

            # Дин. свойства
            $menu->assign($menuTitle, _t('shop','Дин. св-ва категорий'), $module, 'dynprops_listing', false, 12);
            $menu->assign($menuTitle, _t('shop','Дин. св-ва категорий'), $module, 'dynprops_action', false, 13);
        }

        # Услуги
        if ($security->haveAccessToModuleToMethod($module, 'svc')) {
            $menu->assign('Счет и услуги', 'Услуги: '.$menuTitle, $module, 'svc_services', true, 20);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module, 'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 15);
        }
    }
}