<?php

use bff\db\Dynprops;

abstract class ShopBase_ extends Module
    implements IModuleWithSvc
{
    # Статус товара
    const STATUS_ACTIVE     = 1; # активен
    const STATUS_NOT_ACTIVE = 2; # неактивен
    const STATUS_BLOCKED    = 5; # заблокирован

    # Тип доп. модификаторов цены
    const PRICE_EX_PRICE    = 0;
    const PRICE_EX_MOD      = 1; # Модификатор
    const PRICE_EX_EXCHANGE = 2; # Обмен
    const PRICE_EX_FREE     = 4; # Бесплатно

    # ID Услуг
    const SVC_UP        = 8;  # поднятие
    const SVC_MARK      = 16; # выделение
    const SVC_FIX       = 32; # закрепление

    /** @var ShopModel */
    public $model = null;
    public $securityKey = '406e31169ff47bb1dbe552a4c208aca8';

    public function init()
    {
        parent::init();

        $this->module_title = _t('shop','Магазин');

        bff::autoloadEx(array(
            'ShopProductImages' => array('app', 'modules/shop/shop.product.images.php'),
            'ShopProductTags'   => array('app', 'modules/shop/shop.product.tags.php'),
            'ShopSvcIcon'       => array('app', 'modules/shop/shop.svc.icon.php'),
        ));

        # инициализируем модуль дин. свойств
        if (bff::adminPanel()) {
            if (strpos(bff::$event, 'dynprops') === 0) {
                $this->dp();
            }
        }
    }

    /**
     * @return Shop
     */
    public static function i()
    {
        return bff::module('Shop');
    }

    /**
     * @return ShopModel
     */
    public static function model()
    {
        return bff::model('Shop');
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'shop_product_blocked' => array(
                'title'       => 'Магазин: Товар заблокирован модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при блокировке товара модератором',
                'vars'        => array(
                    '{fio}'             => 'ФИО исполнителя',
                    '{name}'            => 'Имя исполнителя',
                    '{product_id}'      => 'ID товара',
                    '{product_title}'   => 'Заголовок товара',
                    '{product_url}'     => 'Ссылка для просмотра товара',
                    '{blocked_reason}'  => 'Причина блокировки',
                ),
                'impl'        => true,
                'priority'    => 35,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
            'shop_product_approved' => array(
                'title'       => 'Магазин: Товар одобрен модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при одобрении товара модератором',
                'vars'        => array(
                    '{fio}'             => 'ФИО исполнителя',
                    '{name}'            => 'Имя исполнителя',
                    '{product_id}'      => 'ID товара',
                    '{product_title}'   => 'Заголовок товара',
                    '{product_url}'     => 'Ссылка для просмотра товара',
                ),
                'impl'        => true,
                'priority'    => 36,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
        );
        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Список товаров
            case 'list':
                $url = Geo::url($opts, $dynamic) . 'shop/' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Просмотр товара из общего списка
            case 'view':
                $url .= '/shop/' . $opts['id'] . '-' . $opts['keyword'] .'.html'.
                    static::urlQuery($opts, array('id','keyword'));
                break;
            # Добавление товара
            case 'add':
                $url .= '/shop/add' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Инфо о статусе товара
            case 'status':
                # id - ID товара
                $url .= '/shop/status' . static::urlQuery($opts);
                break;
            # Редактирование товара
            case 'edit':
                # id - ID товара
                $url .= '/shop/edit' . static::urlQuery($opts);
                break;
            # Продвижение товара
            case 'promote':
                # id - ID товара
                $url .= '/shop/promote' . static::urlQuery($opts);
                break;
            # Кабинет: магазин
            case 'my.shop':
                $opts['login'] = User::data('login');
                $url = static::url('user.listing', $opts, $dynamic);
                break;
            # Кабинет: настройки магазина
            case 'my.settings':
                $url  = Users::url('profile', array('login'=>User::data('login'), 'tab'=>'shop/settings'), $dynamic);
                $url .= (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Профиль: магазин
            case 'user.listing':
                $url  = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'shop'), $dynamic);
                $url .= static::urlQuery($opts, array('login'));
                break;
            # Профиль: просмотр товара
            case 'user.view':
                $url  = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'shop'), $dynamic);
                $url .= $opts['id'] . '-' . $opts['keyword'] .'.html'. static::urlQuery($opts, array('id','keyword','login'));
                break;
            # Поиск по тегу
            case 'search-tag':
                $opts['tag'] = mb_strtolower($opts['tag']).'-'.$opts['id'];
                $url = Geo::url($opts, $dynamic).'shop/' . static::urlQuery($opts, array('id'));
                break;
            # Поиск по категории
            case 'search-cat':
                $url = Geo::url($opts, $dynamic).'shop/'.$opts['cat_keyword1'].'/'.( ! empty($opts['cat_keyword2']) ? $opts['cat_keyword2'].'/' : '');
                break;
        }
        return bff::filter('shop.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        $templates = array(
            'pages'  => array(
                'search' => array(
                    't'      => 'Список (все категории)',
                    'list'   => true,
                    'macros' => array(),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-category' => array(
                    't'       => 'Список (категория)',
                    'list'    => true,
                    'inherit' => true,
                    'macros'  => array(
                        'category' => array('t'=>'Название категории'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-keyword' => array(
                    't'      => 'Поиск товаров (по ключевому слову)',
                    'list'   => true,
                    'macros' => array(
                        'query' => array('t'=>'Строка запроса'),
                    ),
                ),
                'view' => array(
                    't'      => 'Просмотр товара',
                    'macros' => array(
                        'title'       => array('t' => 'Заголовок (до 50 символов)'),
                        'title.full'  => array('t' => 'Заголовок (полный)'),
                        'description' => array('t' => 'Описание (до 150 символов)'),
                        'tags'        => array('t' => 'Теги (перечисление)'),
                    ),
                    'fields' => array(
                        'share_title' => array(
                            't'    => 'Заголовок (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                        'share_description' => array(
                            't'    => 'Описание (поделиться в соц. сетях)',
                            'type' => 'textarea',
                        ),
                        'share_sitename' => array(
                            't'    => 'Название сайта (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                    ),
                ),
            ),
        );

        return $templates;
    }

    /**
     * Включен ли раздел магазин
     * @return boolean
     */
    public static function enabled()
    {
        return config::sysAdmin('shop.enabled', true, TYPE_BOOL);
    }

    /**
     * Использовать премодерацию товаров
     * @return bool
     */
    public static function premoderation()
    {
        return config::sysAdmin('shop.premoderation', true, TYPE_BOOL);
    }

    /**
     * Максимально допустимое кол-во фотографий прикрепляемых к товару
     * 0 - возможность прикрепления фотографий выключена (недоступна)
     * @return integer
     */
    public static function imagesLimit()
    {
        return config::sysAdmin('shop.images.limit', 8, TYPE_UINT);
    }

    /**
     * Максимальное кол-во регионов прикрепляемых к товару, 0 - нет регионов
     * По-умолчанию: 1
     * @return integer
     */
    public static function regionsLimit()
    {
        return config::sys('shop.regions.limit', 1, TYPE_UINT);
    }

    /**
     * Склеивать теги в "еще" когда их кол-во превышает допустимый лимит
     * @return integer
     */
    public static function searchTagsLimit()
    {
        return config::sysAdmin('shop.search.tags.limit', 0, TYPE_UINT);
    }

    /**
     * Инициализация компонента работы с дин. свойствами
     * @return \bff\db\Dynprops объект
     */
    public function dp()
    {
        static $oDp = null;
        if (isset($oDp)) {
            return $oDp;
        }

        # подключаем "Динамические свойства"
        $oDp = $this->attachComponent('dynprops',
            new Dynprops('owner_id', TABLE_SHOP_CATS,
                TABLE_SHOP_CATS_DP, TABLE_SHOP_CATS_DPM,
                1 # полное наследование
            )
        );

        $oDp->setSettings(array(
                'module_name'          => $this->module_name,
                'typesAllowed'         => array(
                    Dynprops::typeCheckboxGroup,
                    Dynprops::typeRadioGroup,
                    Dynprops::typeRadioYesNo,
                    Dynprops::typeCheckbox,
                    Dynprops::typeSelect,
                    Dynprops::typeInputText,
                    Dynprops::typeTextarea,
                    Dynprops::typeNumber,
                    Dynprops::typeRange,
                ),
                'langs'                => $this->locale->getLanguages(false),
                'langText'             => array(
                    'yes'    => _t('', 'Да'),
                    'no'     => _t('', 'Нет'),
                    'all'    => _t('', 'Все'),
                    'select' => _t('', 'Выбрать'),
                ),
                'ownerTable_Title'     => 'title',
                'cache_method'         => 'Shop_dpSettingsChanged',
                'typesAllowedParent'   => array(Dynprops::typeSelect),
                'datafield_int_last'   => 15,
                'datafield_text_first' => 16,
                'datafield_text_last'  => 20,
                'cacheKey'             => false,
            )
        );

        return $oDp;
    }

    /**
     * Получаем дин. свойства категории
     * @param integer $nCategoryID ID категории
     * @param boolean $bResetCache обнулить кеш
     * @return mixed
     */
    public function dpSettings($nCategoryID, $bResetCache = false)
    {
        if ($nCategoryID <= 0) {
            return array();
        }

        $cache = Cache::singleton($this->module_name, 'file');
        $cacheKey = 'categories-dynprops-' . $nCategoryID;
        if ($bResetCache) {
            # сбрасываем кеш настроек дин. свойств категории
            return $cache->delete($cacheKey);
        } else {
            if (($aSettings = $cache->get($cacheKey)) === false) { // ищем в кеше
                $aSettings = $this->dp()->getByOwner($nCategoryID, true, true, false);
                $cache->set($cacheKey, $aSettings); // сохраняем в кеш
            }

            return $aSettings;
        }
    }

    /**
     * Метод вызываемый модулем \bff\db\Dynprops, в момент изменения настроек дин. свойств категории
     * @param integer $nCategoryID ID категории
     * @param integer $nDynpropID ID дин.свойства
     * @param string $sEvent событие, генерирующее вызов метода
     * @return mixed
     */
    public function dpSettingsChanged($nCategoryID, $nDynpropID, $sEvent)
    {
        if (empty($nCategoryID)) {
            return false;
        }
        $this->dpSettings($nCategoryID, true);
    }

    /**
     * Формирование SQL запроса для сохранения дин.свойств
     * @param integer $nCategoryID ID категории
     * @param string $sFieldname ключ в $_POST массиве
     * @return array
     */
    public function dpSave($nCategoryID, $sFieldname = 'd')
    {
        $aData = $this->input->post($sFieldname, TYPE_ARRAY);

        $aDynpropsData = array();
        foreach ($aData as $props) {
            foreach ($props as $id => $v) {
                $aDynpropsData[$id] = $v;
            }
        }

        $aDynprops = $this->dp()->getByID(array_keys($aDynpropsData), true);

        return $this->dp()->prepareSaveDataByID($aDynpropsData, $aDynprops, 'update', true);
    }

    /**
     * Формирование формы редактирования / фильтра дин.свойств
     * @param integer $nCategoryID ID категории
     * @param boolean $bSearch формирование формы поиска
     * @param array|boolean $aData данные или FALSE
     * @param string $sKey ключ
     * @param string|boolean $sFormType тип формы: имя шаблона в модуле
     * @param string|boolean $sTemplateDir путь к шаблону (false - путь указанный компонентом)
     * @param array $aOnlyChild только указанные child свойства
     * @return string HTML template
     */
    public function dpForm($nCategoryID, $bSearch = true, $aData = false, $sKey = 'd', $sFormType = false, $sTemplateDir = false, $aOnlyChild = array())
    {
        if (empty($nCategoryID)) {
            return '';
        }

        if ($bSearch) {
            if (!bff::adminPanel()) {
                if (!$sFormType) {
                    $sFormType = 'dp.list';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->dp()->form($nCategoryID, $aData, true, true, $sKey, $sFormType, $sTemplateDir, array('filter'     => $aData,
                                                                                                                      'aOnlyChild' => $aOnlyChild
                    )
                );
            } else {
                $aForm = $this->dp()->form($nCategoryID, $aData, true, true, $sKey, 'search.inline');
            }
        } else {
            if (!bff::adminPanel()) {
                if (!$sFormType) {
                    $sFormType = 'dp.form';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->dp()->form($nCategoryID, $aData, true, false, $sKey, $sFormType, $sTemplateDir);
            } else {
                $aForm = $this->dp()->form($nCategoryID, $aData, true, false, $sKey, 'form.table');
            }
        }

        return (!empty($aForm['form']) ? $aForm['form'] : '');
    }

    /**
     * Отображение дин. свойств
     * @param integer $nCategoryID ID категории
     * @param array $aData данные
     * @param string $sKey ключ
     */
    public function dpView($nCategoryID, $aData, $sKey = 'd')
    {
        if (!bff::adminPanel()) {
            $aForm = $this->dp()->form($nCategoryID, $aData, true, false, $sKey, 'view.dp', $this->module_dir_tpl);
        } else {
            $aForm = $this->dp()->form($nCategoryID, $aData, true, false, $sKey, 'view.table');
        }

        return (!empty($aForm['form']) ? $aForm['form'] : '');
    }

    /**
     * Инициализация компонента ShopProductImages
     * @param integer $nItemID ID товара
     * @return ShopProductImages component
     */
    public function productImages($nItemID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new ShopProductImages();
        }
        $i->setRecordID($nItemID);

        return $i;
    }

    /**
     * Инициализация компонента работы с тегами товара
     * @return ShopProductTags
     */
    public function productTags()
    {
        static $i;
        if (!isset($i)) {
            $i = new ShopProductTags();
            if (!bff::adminPanel()) {
                $i->module_dir_tpl = $this->module_dir_tpl;
            }
        }
        return $i;
    }

    /**
     * Формирование формы редактирования / фильтра цены
     * @param integer $nCategoryID ID категории
     * @param boolean $bSearch формирование формы поиска
     * @param array|boolean $aData данные или FALSE
     * @param string|boolean $sFormType тип формы: имя шаблона в модуле
     * @param string|boolean $sTemplateDir путь к шаблону (false - путь указанный компонентом)
     * @return string HTML template
     */
    public function priceForm($nCategoryID, $bSearch = true, $aData = false, $sFormType = false, $sTemplateDir = false)
    {
        if (empty($nCategoryID)) {
            return '';
        }

        if ($bSearch) {
            if ( ! bff::adminPanel()) {

                if ( ! $sFormType) {
                    $sFormType = 'search.price';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->model->categoryData($nCategoryID);
                $aForm['data'] = $aData;

                return $this->viewPHP($aForm, $sFormType, $sTemplateDir);
            }
        } else {
            if ( ! bff::adminPanel()) {

                if ( ! $sFormType) {
                    $sFormType = 'form.price';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->model->categoryData($nCategoryID);
                $aForm['data'] = $aData;

                return $this->viewPHP($aForm, $sFormType, $sTemplateDir);
            }
        }

        return (!empty($aForm['form']) ? $aForm['form'] : '');
    }

    /**
     * Является ли текущий пользователь владельцем товара
     * @param integer $nProductID ID товара
     * @param integer|bool $nProductUserID ID пользователя товара или FALSE (получаем из БД)
     * @return boolean
     */
    public function isProductOwner($nProductID, $nProductUserID = false)
    {
        $nUserID = User::id();
        if (!$nUserID) {
            return false;
        }

        if ($nProductUserID === false) {
            $aData = $this->model->productData($nProductID, array('user_id'));
            if (empty($aData)) {
                return false;
            }

            $nProductUserID = $aData['user_id'];
        }

        return ($nProductUserID > 0 && $nUserID == $nProductUserID);
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nProductID ID товара или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateProductData($nProductID, $bSubmit)
    {
        $aData = array();
        $aParam = array(
            'cat_id'     => TYPE_UINT,       # Категория
            'title'      => array(TYPE_NOTAGS, 'len' => 200, 'len.sys' => 'shop.product.title.limit'),  # Название
            'descr'      => array(TYPE_TEXT,   'len' => 3000, 'len.sys' => 'shop.product.descr.limit'), # Описание
            'price'      => TYPE_PRICE,      # Цена: сумма
            'price_ex'   => TYPE_ARRAY_UINT, # Цена: доп. параметры
            'price_curr' => TYPE_UINT,       # Цена: валюта
            'regions'    => TYPE_ARRAY_UINT, # Регионы
        );
        if (!$nProductID) {
            if (bff::adminPanel()) {
                $aParam['user_id'] = TYPE_UNUM;
            }
        }

        $this->input->postm($aParam, $aData);

        if ($bSubmit) {
            # Заголовок
            if (!strlen($aData['title'])) {
                $this->errors->set(_t('shop', 'Введите название товара'), 'title');
            }
            # Описание
            if (mb_strlen($aData['descr']) < config::sysAdmin('shop.product.descr.min', 10, TYPE_UINT)) {
                $this->errors->set(_t('shop','Описание товара слишком короткое'), 'descr');
            }

            if ( ! bff::adminPanel()) {
                # антиспам фильтр
                Site::i()->spamFilter(array(
                    array('text' => & $aData['title'], 'error'=>_t('', 'В указанном вами заголовке присутствует запрещенное слово "[word]"')),
                    array('text' => & $aData['descr'], 'error'=>_t('', 'В указанном вами описании присутствует запрещенное слово "[word]"')),
                ));
            }

            # Категория
            $nCategoryID = $aData['cat_id'];
            if (!$nCategoryID) {
                $this->errors->set(_t('shop','Выберите категорию'));
            } else {
                # проверяем наличие подкатегорий
                $nSubsCnt = $this->model->categorySubCount($nCategoryID);
                if ($nSubsCnt > 0) {
                    $this->errors->set(_t('shop','Выбранная категория не должна содержать подкатегории'));
                } else {
                    # сохраняем ID категорий(parent и текущей), для возможности дальнейшего поиска по ним
                    $nParentsID = $this->model->categoryParentsID($nCategoryID, true);
                    foreach ($nParentsID as $lvl => $id) {
                        $aData['cat_id' . $lvl] = $id;
                    }
                }
            }

            if ( ! $nProductID) {
                if ( ! bff::adminPanel()) {
                    $aData['user_id'] = User::id();
                }
                if ( ! $aData['user_id']) {
                    $this->errors->set(_t('shop','Укажите пользователя'));
                }
            }

            $aData['price_ex'] = array_sum($aData['price_ex']);
            if ($aData['price'] && $aData['price_curr']) {
                $aData['price_search'] = Site::currencyPriceConvertToDefault($aData['price'], $aData['price_curr']);
            }
            # Город
            if (!empty($aData['regions'])) {
                foreach ($aData['regions'] as $k => $v) {
                    if (!$v) {
                        unset($aData['regions'][$k]);
                        continue;
                    }
                    if (!Geo::isCity($v)) {
                        $this->errors->set(_t('shop', 'Город указан некорректно'));
                    }
                }
            }

            # URL-Keyword
            $aData['keyword'] = mb_strtolower(func::translit($aData['title']));
            $aData['keyword'] = trim(preg_replace('/[^a-zA-Z0-9_\-]/', '', $aData['keyword']), '-');
        }

        return $aData;
    }

    /**
     * Получаем данные о категории для формы добавления/редактирования товара
     * @param int $nCategoryID ID категории
     * @param array $aProductData @ref параметры товара
     * @param array $aCategoryFieldsExtra дополнительно необходимые данные о категории
     * @return array
     */
    protected function productFormByCategory($nCategoryID, $aProductData = array(), $aCategoryFieldsExtra = array())
    {
        # получаем данные о категории:
        $aFields = array(
            'id',
            'pid',
            'subs',
            'price',
            'price_ex',
            'price_curr',
            'price_title',
            'price_title_mod',
        );
        if (!empty($aCategoryFieldsExtra)) {
            $aFields = array_merge($aFields, $aCategoryFieldsExtra);
            $aFields = array_unique($aFields);
        }
        $aData = $this->model->categoryData($nCategoryID, $aFields);
        if (empty($aData)) {
            return array();
        }

        if ($aData['subs'] > 0) {
            # есть подкатегории => формируем список подкатегорий
            if (bff::adminPanel()) {
                $aData['cats'] = $this->model->categorySubOptions($nCategoryID, array('sel'=>0, 'empty'=>'Выбрать'));
            }
        } else {
            # формируем форму дин. свойств:
            $aData['dp'] = $this->dpForm($nCategoryID, false, $aProductData);
        }

        # корректируем необходимые данные
        $aData['edit'] = !empty($aProductData);
        $aData['product'] = $this->input->clean_array($aProductData, array(
                'price'      => TYPE_PRICE,
                'price_ex'   => TYPE_UINT,
                'price_curr' => TYPE_UINT,
            )
        );

        if (bff::adminPanel()) {
            $aData['form'] = $this->viewPHP($aData, 'admin.products.form.category');
        } else {
            # ...
        }

        return $aData;
    }

    /**
     * Удаление товара
     * @param integer $nProductID ID товара
     * @return boolean
     */
    public function productDelete($nProductID)
    {
        if (empty($nProductID)) return false;

        $res = $this->model->productDelete($nProductID);
        if (!empty($res)) {
            $this->productImages($nProductID)->deleteAllImages(false);
            $this->productTags()->onItemDelete($nProductID);
            # обновляем счетчик товаров "на модерации"
            $this->moderationCounterUpdate();
            return true;
        }

        return false;
    }

    /**
     * Актуализация счетчика товаров ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->productsModeratingCounter();
            config::save('shop_products_moderating', $count, true);
        } else {
            config::saveCount('shop_products_moderating', $increment, true);
        }
    }

    /**
     * Формирование блока с ценой товара
     * @param array $data @ref
     * @param array $cats @ref данные о категориях товара
     * @param boolean $isList список товаров
     * @return string HTML
     */
    public static function priceBlock(array &$data, array &$cats = array(), $isList = true)
    {
        $ret = '';
        do {
            if (empty($data)) break;
            $catID = $data['cat_id'];
            if ( ! $isList) {
                if (empty($cats[$catID]['price'])) return '';
            }
            if(isset($data['cat_price']) && ! $data['cat_price']) return '';
            if(isset($cats[$catID]['price']) && ! $cats[$catID]['price']) return '';
            $price_view = '<span class="l-price">' . tpl::formatPrice($data['price']) . ' ' . Site::currencyData($data['price_curr'], 'title_short') . '</span>';
            if ($data['price_ex']) {
                $ret = '<span class="l-price_na">';
                if ($data['price_ex'] & static::PRICE_EX_EXCHANGE) {
                    $ret .= _t('shop', 'Обмен');
                }
                else if ($data['price_ex'] & static::PRICE_EX_FREE) {
                    $ret .= _t('shop', 'Бесплатно');
                }
                else if ($data['price_ex'] & static::PRICE_EX_MOD) {
                    if ($data['price'] > 0) {
                        $ret = $price_view.' '.$ret;
                    }
                    $lvl = ($data['cat_id'] == $data['cat_id2'] ? '2' : '');
                    if ( ! empty($data['price_title_mod'.$lvl])) {
                        $ret .= $data['price_title_mod'.$lvl];
                    } else if ( ! empty($cats[$catID]['price_title_mod'])) {
                        $ret .= $cats[$catID]['price_title_mod'];
                    } else {
                        $ret .= _t('shop', 'Торг возможен');
                    }
                }
                $ret .= '</span>';
            } else {
                $ret = $price_view;
            }
        } while (false);

        return $ret;
    }

    /**
     * Активация услуги для пользователя
     * @param integer $nProductID ID товара
     * @param integer $nSvcID ID услуги (услуг)
     * @param mixed $aSvcData данные об услуге(*)/пакете услуг или FALSE
     * @param array $aSvcSettings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return boolean true - успешная активация, false - ошибка активации
     */
    public function svcActivate($nProductID, $nSvcID, $aSvcData = false, array &$aSvcSettings = array())
    {
        if (!$nSvcID) {
            $this->errors->set(_t('svc', 'Неудалось активировать услугу'));

            return false;
        }
        $aData = $this->model->productData($nProductID, array('status', 'svc', 'svc_fixed_to', 'svc_marked_to', 'user_id', 'title', 'keyword'));
        if (empty($aData) || $aData['status'] != static::STATUS_ACTIVE) {
            $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
            return false;
        }

        $svcData = array();
        foreach(array(static::SVC_UP, static::SVC_MARK, static::SVC_FIX) as $v){
            if($nSvcID & $v){
                $svcData[] = $v;
            }
        }
        $svcData = Svc::model()->svcData($svcData, 'id, keyword, settings, type');
        foreach($svcData as $k => $v){
            $svcData[$v['id']] = $v;
            unset($svcData[$k]);
        }

        $notifyTitle = '';
        $notifyDescr = array();
        $notifyCnt = 0;


        $aUpdate = array('svc' => $aData['svc']);
        if($nSvcID & static::SVC_UP){
            $aUpdate['svc_order'] = $this->db->now();
            if($aData['svc'] & static::SVC_FIX){
                $aUpdate['svc_fixed_order'] = $this->db->now();
            }

            $notifyTitle = $svcData[static::SVC_UP]['title_view'][LNG];
            $notifyDescr[] = $notifyTitle;
            $notifyCnt++;
        }
        if ($nSvcID & static::SVC_MARK) {
            $aSvc = $svcData[static::SVC_MARK];
            $days = $aSvc['period'];
            $now = time();
            if ($aData['svc'] & static::SVC_MARK) {
                # продление
                $from = strtotime($aData['svc_marked_to']);
                if($from < $now){
                    $from = $now;
                }
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''), $from);
            } else {
                # активация новой
                $from = $now;
                $aUpdate['svc'] |= static::SVC_MARK;
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''));
            }
            $aSvcSettings[static::SVC_MARK]['from'] = $from;
            $aSvcSettings[static::SVC_MARK]['to'] = $to;
            $aUpdate['svc_marked_to'] = date('Y-m-d H:i:s', $to);

            $notifyTitle = $aSvc['title_view'][LNG];
            $notifyDescr[] = _t('svc', '[title] c [from] по [to]', array(
                'title' => $notifyTitle,
                'from' => tpl::date_format2($from),
                'to' => tpl::date_format2($to)));
            $notifyCnt++;
        }
        if ($nSvcID & static::SVC_FIX) {
            $aSvc = $svcData[static::SVC_FIX];
            $days = $aSvc['period'];
            if( ! empty($aSvc['per_day']) && ! empty($aSvcSettings['fixed_days'])){
                $days = $aSvcSettings['fixed_days'];
            }
            $now = time();
            if ($aData['svc'] & static::SVC_FIX) {
                # продление
                $from = strtotime($aData['svc_fixed_to']);
                if($from < $now){
                    $from = $now;
                }
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''), $from);
            } else {
                # активация новой
                $from = $now;
                $aUpdate['svc'] |= static::SVC_FIX;
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''));
            }
            $aSvcSettings[static::SVC_FIX]['from'] = $from;
            $aSvcSettings[static::SVC_FIX]['to'] = $to;
            $aUpdate['svc_fixed_to'] = date('Y-m-d H:i:s', $to);
            $aUpdate['svc_fixed_order'] = $this->db->now();

            $notifyTitle = $aSvc['title_view'][LNG];
            $notifyDescr[] = _t('svc', '[title] c [from] по [to]', array(
                'title' => $notifyTitle,
                'from' => tpl::date_format2($from),
                'to' => tpl::date_format2($to)));
            $notifyCnt++;
        }
        $result = $this->model->productSave($nProductID, $aUpdate);
        if ($result) {
            # Отправим уведомление пользователю
            $notifyDescr = join('<br />', $notifyDescr);
            if ($notifyCnt > 1) {
                Users::sendMailTemplateToUser($aData['user_id'], 'svc_activated_multiple', array(
                    'description' => $notifyDescr,
                    'item_id'     => $nProductID,
                    'item_title'  => $aData['title'],
                    'item_link'   => static::url('view', array('id' => $nProductID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            } else {
                Users::sendMailTemplateToUser($aData['user_id'], 'svc_activated_single', array(
                    'title'       => $notifyTitle,
                    'description' => $notifyDescr,
                    'item_id'     => $nProductID,
                    'item_title'  => $aData['title'],
                    'item_link'   => static::url('view', array('id' => $nProductID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            }
        }
        return $result;
    }

    /**
     * Формируем описание счета активации услуги (пакета услуг)
     * @param integer $nProductID ID товара
     * @param integer $nSvcID ID услуги
     * @param mixed $aData данные об услуге(*)/пакете услуг или FALSE
     * @param array $aSvcSettings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return string
     */
    public function svcBillDescription($nProductID, $nSvcID, $aData = false, array &$aSvcSettings = array())
    {
        $aData = $this->model->productData($nProductID, array('keyword', 'title'));

        $nSvcIDs = $nSvcID;
        if( ! empty($aSvcSettings['svc'])){
            foreach($aSvcSettings['svc'] as $v){
                $nSvcIDs |= $v;
            }
        }

        $sReturn = '';
        if ($nSvcIDs & static::SVC_UP) {
            $sReturn = _t('svc', 'Поднятие');
        }
        if ($nSvcIDs & static::SVC_MARK) {
            if ($sReturn) $sReturn .= ', '._t('svc', 'выделение');
            else $sReturn = _t('svc', 'Выделение');
            if( ! empty($aSvcSettings[static::SVC_MARK]['from']) && ! empty($aSvcSettings[static::SVC_MARK]['to'])) {
                $sReturn .= _t('scv', '(по [to])', array(
                                'to'   => tpl::date_format2($aSvcSettings[static::SVC_MARK]['to'])));
            }
        }
        if ($nSvcIDs & static::SVC_FIX) {
            if ($sReturn) $sReturn .= ', '._t('svc', 'закрепление');
            else $sReturn = _t('svc', 'Закрепление');
            if ( ! empty($aSvcSettings[static::SVC_FIX]['from']) && ! empty($aSvcSettings[static::SVC_FIX]['to'])) {
                $sReturn .= _t('scv', '(по [to])', array(
                                'to'   => tpl::date_format2($aSvcSettings[static::SVC_FIX]['to'])));
            }
        }

        $sReturn .= ' '._t('svc', 'товара');
        $sReturn .= ' <br /><a href="'.static::url('view', array('id' => $nProductID, 'keyword' => $aData['keyword'])).'">'.$aData['title'].'</a>';
        return $sReturn;
    }

    /**
     * Инициализация компонента обработки иконок услуг/пакетов услуг ShopSvcIcon
     * @param mixed $nSvcID ID услуги / пакета услуг
     * @return ShopSvcIcon component
     */
    public static function svcIcon($nSvcID = false)
    {
        static $i;
        if (!isset($i)) {
            $i = new ShopSvcIcon();
        }
        $i->setRecordID($nSvcID);

        return $i;
    }

    /**
     * Период: 1 раз в час
     */
    public function svcCron()
    {
        if (!bff::cron()) {
            return;
        }

        $this->model->svcCron();
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('products', 'images') => 'dir-split', # изображения товаров
            bff::path('tmp', 'images') => 'dir-only', # tmp
        ));
    }
}