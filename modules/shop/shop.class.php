<?php

class Shop_ extends ShopBase
{
    public function init()
    {
        parent::init();

        if (bff::$class == $this->module_name && Request::isGET()) {
            bff::setActiveMenu('//shop');
        }
    }

    /**
     * Добавление товара
     */
    public function add()
    {
        $this->checkEnabled();

        $this->security->setTokenPrefix('shop-product-form');

        $aData = $this->validateProductData(0, Request::isPOST());
        $nUserID = User::id();

        if (Request::isPOST()) {
            do {
                $aResponse = array();

                if ( ! User::isWorker()) {
                    $this->errors->set(_t('shop', 'Добавлять товары могут только исполнители'));
                    break;
                }

                if ( ! $this->errors->no()) {
                    break;
                }

                # доверенный пользователь: без модерации
                if ($trusted = User::isTrusted($this->module_name)){
                    $aData['moderated'] = 1;
                }

                # создаем продукт
                $nProductID = $this->model->productSave(0, $aData, 'd');
                if ( ! $nProductID) {
                    $this->errors->reloadPage();
                    break;
                }

                # загружаем изображения
                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->productImages($nProductID);
                    $oImages->setAssignErrors(false);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES)) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                            # удаляем загруженные через "удобный способ"
                            $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                            $oImages->deleteImages($aImages);
                        }
                    } else {
                        # перемещаем из tmp-директории в постоянную
                        $oImages->saveTmp('images');
                    }
                }

                # теги
                $this->productTags()->tagsSave($nProductID);

                # обновляем счетчик товаров "на модерации"
                if (!$trusted) {
                    $this->moderationCounterUpdate(1);
                }

                $svc = $this->input->post('svc', TYPE_ARRAY_UINT);
                $svc = array_sum($svc);
                if($svc){
                    $params = array('id' => $nProductID, 'preset' => $svc, 'status' => 1);
                    $fixedDays = $this->input->post('svc_fixed_days', TYPE_UINT);
                    if ($fixedDays) {
                        $params['svc_fixed_days'] = $fixedDays;
                    }
                    $aResponse['redirect'] = static::url('promote', $params);
                } else {
                    $aResponse['redirect'] = static::url('status', array('id' => $nProductID));
                }

            } while (false);
            $this->iframeResponseForm($aResponse);
        }

        if ($nUserID && Users::useClient() && ! User::isWorker()) {
            return $this->showForbidden('', _t('shop', 'Добавлять товары могут только исполнители'));
        }
        if( ! $nUserID){
            return $this->showForbiddenGuests();
        }

        bff::setMeta(_t('shop', 'Добавление товара'));
        return $this->form(0, $aData);
    }

    /**
     * Редактирование товара
     */
    public function edit()
    {
        $this->checkEnabled();

        $this->security->setTokenPrefix('shop-product-form');

        if ( ! User::id()) {
            $this->redirect(Users::url('login'), false, true);
        }

        $nProductID = $this->input->get('id', TYPE_UINT);
        if ( ! $nProductID) {
            $this->errors->error404();
        }
        $aProductData = $this->model->productData($nProductID, array(), true);
        if (empty($aProductData)) {
            $this->errors->error404();
        }

        if ( ! $this->isProductOwner($nProductID, $aProductData['user_id'])) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
                $this->iframeResponseForm();
            }
            return $this->showForbidden('', _t('shop', 'Вы не является владельцем данного товара'));
        }

        if (Request::isPOST()) {
            $aData = $this->validateProductData($nProductID, true);
            $aResponse = array();

            if ($this->errors->no()) {

                if ($aProductData['status'] == self::STATUS_BLOCKED) {
                    # товар заблокирован, помечаем на проверку модератору
                    $aData['moderated'] = 0;
                }

                # доверенный пользователь: без модерации
                $trusted = User::isTrusted($this->module_name);
                # помечаем на модерацию при изменении: названия, описания
                if ($aData['title'] != $aProductData['title'] || $aData['descr'] != $aProductData['descr']) {
                    if ($aProductData['moderated']!=0 && !$trusted) {
                        $aData['moderated'] = 2;
                    }
                }

                $this->model->productSave($nProductID, $aData, 'd');

                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->productImages($nProductID);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES) && $aProductData['imgcnt'] < $oImages->getLimit()) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                        }
                    } else {
                        # сохраняем порядок изображений
                        $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                        $oImages->saveOrder($aImages, false);
                    }

                    # помечаем на модерацию при изменении: фотографий
                    if ($oImages->newImagesUploaded($this->input->post('images_hash', TYPE_STR))
                        && $aProductData['moderated']!=0 && !$trusted) {
                        $this->model->productSave($nProductID, array('moderated' => 2));
                    }
                }

                # теги
                $this->productTags()->tagsSave($nProductID);

                # обновляем счетчик товаров "на модерации"
                $this->moderationCounterUpdate();

                $svc = $this->input->post('svc', TYPE_ARRAY_UINT);
                $svc = array_sum($svc);
                if($svc){
                    $params = array('id' => $nProductID, 'preset' => $svc);
                    $fixedDays = $this->input->post('svc_fixed_days', TYPE_UINT);
                    if ($fixedDays) {
                        $params['svc_fixed_days'] = $fixedDays;
                    }
                    $aResponse['redirect'] = static::url('promote', $params);
                } else {
                    $aResponse['redirect'] = static::url('user.view', array('id' => $nProductID, 'keyword' => $aData['keyword'], 'login'=>User::data('login')));
                }

            }
            $this->iframeResponseForm($aResponse);
        }

        bff::setMeta(_t('shop', 'Редактирование товара'));
        return $this->form($nProductID, $aProductData);
    }
    
    /**
     * Форма товара
     * @param integer $nProductID ID товара
     * @param array $aData @ref
     * @return string HTML
     */
    protected function form($nProductID, array &$aData)
    {
        $aData['id'] = $nProductID;
        $aData['img'] = $this->productImages($nProductID);
        $aData['aSvc'] = Svc::model()->svcListing(Svc::TYPE_SERVICE, $this->module_name);

        if ($nProductID) {
            if (static::imagesLimit()) {
                $aImages = $aData['img']->getData($aData['imgcnt']);
                $aData['images'] = array();
                foreach ($aImages as $v) {
                    $aData['images'][] = array(
                        'id'       => $v['id'],
                        'tmp'      => false,
                        'filename' => $v['filename'],
                        'i'        => $aData['img']->getURL($v, ShopProductImages::szForm, false)
                    );
                }
                $aData['imghash'] = $aData['img']->getLastUploaded();
            }
            $aData['dp'] = $this->dpForm($aData['cat_id'], false, $aData, 'd', 'form.dp', $this->module_dir_tpl);
            $aData['price'] = $this->priceForm($aData['cat_id'], false, $aData);

            # для селектора категории
            $aData['cat'] = array(
                'cat_id1' => $aData['cat_id1'],
                'cat_id2' => $aData['cat_id2'],
                'cat_id'  => $aData['cat_id'],
                'title'   => $this->model->categoryParentsTitle($aData['cat_id'], true, true, '/'));
            if($aData['cat_id'] == $aData['cat_id2']){
                $aStr = explode('/', $aData['cat']['title']);
                $aData['cat']['t1'] = trim($aStr[1]);
                $aData['cat']['t2'] = trim($aStr[0]);
            }
        } else {
            $aData['images'] = array();
            $aData['cat'] = array();
            $aData['imgcnt'] = 0;
            $aData['imghash'] = '';

            $aData['price'] = '';
            $aData['svc'] = 0;

            # поднятие не надо
            unset($aData['aSvc']['shop_up']);
        }

        $aData['edit'] = ! empty($nProductID);
        return $this->viewPHP($aData, 'form');
    }

    /**
     * Вывод сообщения о статусе товара
     */
    public function status()
    {
        $this->checkEnabled();

        $nProductID = $this->input->get('id', TYPE_UINT);
        if (!$nProductID) {
            $this->errors->error404();
        }
        $nUserID = User::id();
        if (!$nUserID) {
            return $this->showForbiddenGuests();
        }

        $aData = $this->model->productDataView($nProductID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        if ($aData['moderated'] > 0) {
            $this->redirect(static::url('view', array('id' => $nProductID, 'keyword' => $aData['keyword'])));
        }

        bff::setMeta(_t('shop', 'Публикация товара'));
        return $this->viewPHP($aData, 'status');
    }

    /**
     * Просмотр товара
     * @return string
     */
    public function view()
    {
        $this->checkEnabled();

        $userID = User::id();
        $nProductID = $this->input->get('id', TYPE_UINT);
        if (!$nProductID) {
            $this->errors->error404();
        }
        $aData = $this->model->productDataView($nProductID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        if ( ! User::isCurrent($aData['user_id'])) {
            if ($aData['status'] != static::STATUS_ACTIVE) {
                $this->errors->error404();
            }
            if (static::premoderation() && ! $aData['moderated']) {
                $this->errors->error404();
            }
        }

        $aData['avatar_small'] = UsersAvatar::url($aData['user_id'], $aData['avatar'], UsersAvatar::szSmall, $aData['sex']);

        $aData['images'] = array();
        if (static::imagesLimit() && $aData['imgcnt']) {
            $oImages = $this->productImages($nProductID);
            $aImages = $oImages->getData($aData['imgcnt']);
            foreach ($aImages as $v) {
                $aData['images'][] = array(
                    'id'       => $v['id'],
                    'filename' => $v['filename'],
                    'i'        => $oImages->getURL($v, array(
                            ShopProductImages::szThumbnail,
                            ShopProductImages::szView
                        ), false
                    )
                );
            }
        }

        # Теги
        $aData['tags'] = $this->productTags()->tagsGet($nProductID);

        # Дин. свойства
        $aData['dynprops'] = $this->dpView($aData['cat_id'], $aData);

        # Хлебные крошки
        $crumbs = array(
            array('title' => _t('shop', 'Shop'), 'link' => static::url('list')),
        );
        $aLinkData = array();
        foreach($aData['cat'] as $v){
            $aLinkData['cat_keyword'.$v['numlevel']] = $v['keyword'];
            $crumbs[] = array('title' => $v['title'], 'link' => static::url('search-cat', $aLinkData));
        }
        $crumbs[] = array('title' => $aData['title'], 'active' => true);
        $aData['breadcrumbs'] = &$crumbs;

        # Счетчик просмотров
        if ($userID != $aData['user_id'] && ! Request::isRefresh()) {
            $this->model->productSave($nProductID, array('views_total = views_total + 1'));
        }

        # SEO: Просмотр товара
        $seoTags = array(); foreach ($aData['tags'] as $v) $seoTags[] = $v['tag'];
        $url = static::url('view', array('id' => $nProductID, 'keyword' => $aData['keyword']), true);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $this->setMeta('view', array(
            'title'       => tpl::truncate($aData['title'], config::sysAdmin('shop.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full'  => $aData['title'],
            'description' => tpl::truncate(strip_tags($aData['descr']), config::sysAdmin('shop.view.meta.description.truncate', 150, TYPE_UINT), '...'),
            'tags'        => join(', ', $seoTags),
        ), $aData);
        # SEO: Open Graph
        $seoSocialImages = array();
        foreach ($aData['images'] as $v) {
            $seoSocialImages[] = $v['i'][ShopProductImages::szView];
        }
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], $seoSocialImages, $url, $aData['share_sitename']);

        return $this->viewPHP($aData, 'view');
    }

    /**
     * Поиск товаров
     */
    public function search()
    {
        $this->checkEnabled();

        $nPageSize = config::sysAdmin('shop.search.pagesize', 10, TYPE_UINT);
        $aFilter = array();
        $aData = array('list'=>array(), 'pgn'=>'', 'cat_id'=>0, 'cat_id1'=>0, 'cat_id2'=>0);

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT,  # № страницы
            'pf'   => TYPE_PRICE, # бюджет: от
            'pt'   => TYPE_PRICE, # бюджет: до
            'pc'   => TYPE_UINT,  # бюджет: валюта
            'pex'  => TYPE_ARRAY_UINT, # бюджет: экстра
            'q'    => TYPE_NOTAGS,# строка поиска
            'tag'  => TYPE_NOTAGS,# тег: "keyword-id"
            'd'    => TYPE_ARRAY, # дин. свойства
            'dc'   => TYPE_ARRAY, # дин. свойства child
            'c'    => TYPE_UINT,  # регион: страна
            'r'    => TYPE_UINT,  # регион: область / регион
            'ct'   => TYPE_UINT,  # регион: город
        ));
        $aData['f'] = &$f;
        $seoFilter = 0;

        # Фильтр по категории / подкатегории
        $f['cat'] = $this->input->getpost('cat', TYPE_NOTAGS); # keyword категории или категории/подкатегории
        $f['cat'] = trim($f['cat'], ' /');
        if ($f['cat']) {
            $sCat = ( ($pos = mb_strpos($f['cat'], '/')) ? mb_substr($f['cat'], $pos + 1) : $f['cat'] );
            $aCat = $aData['cat'] = $this->model->categoryMeta($sCat, $this->module_name);
            if (empty($aCat) && ! Request::isAJAX()) {
                $this->errors->error404();
            }
            $aFilter['cat_id'.$aCat['numlevel']] = $aData['cat_id'] = $aData['cat_id'.$aCat['numlevel']] = $aCat['id'];
            if ($aCat['numlevel'] == 2) $aData['cat_id1'] = $aCat['pid'];
        }
        $nCatID = $aData['cat_id'];
        if ($nCatID) {
            # Дин. свойства категории / подкатегории
            $dp = $this->dp()->prepareSearchQuery($f['d'], $f['dc'], $this->dpSettings($nCatID), 'P.');
            if (!empty($dp)) {
                $aFilter[':dp'] = $dp;
                $seoFilter++;
            }
        }

        $f['pex'] = array_sum($f['pex']);
        if ($f['pex']) { # флаги
            $aFilter[':price_ex'] = array('P.price_ex & :ex != 0', 'ex' => $f['pex']);
            $seoFilter++;
        }
        # Цена
        if ($f['pf']) { # от
            $aFilter[':price_from'] = array(
                'price_search >= :pf',
                'pf' => Site::currencyPriceConvertToDefault($f['pf'], $f['pc'])
            );
            $seoFilter++;
        }
        if ($f['pt']) { # до
            $aFilter[':price_to'] = array(
                'price_search <= :pt',
                'pt' => Site::currencyPriceConvertToDefault($f['pt'], $f['pc'])
            );
            $seoFilter++;
        }

        # Фильтр по тегу
        if ($f['tag']) {
            if (preg_match('/(.*)-(\d+)/', $f['tag'], $matches) && ! empty($matches[2])) {
                $aTagData = $this->productTags()->tagData($matches[2]);
                if (empty($aTagData) || mb_strtolower($aTagData['tag']) != $matches[1]) $this->errors->error404();
                $aFilter['tag_id'] = $aData['nTagID'] = $aTagData['id'];
                $aData['sTagTitle'] = $aTagData['tag'];
            } else {
                $this->errors->error404();
            }
        }

        if ($f['q']) {
            $aFilter['text'] = $f['q'];
        }

        if(static::regionsLimit()) {
            if(Geo::filterEnabled()) {   # включен фильтр в шапке
                $geoFilter = Geo::filter('all');
                if (Geo::countrySelect()&& ! empty($geoFilter['country'])) {
                    $aFilter['reg1_country'] = $geoFilter['country'];
                }
                if( ! empty($geoFilter['region'])) {
                    $aFilter['reg2_region'] = $geoFilter['region'];
                }
                if( ! empty($geoFilter['city'])) {
                    $aFilter['reg3_city'] = $geoFilter['city'];
                }
            } else {
                if (Geo::countrySelect()) { # включен выбор страны
                    if ($f['c']) {
                        $aFilter['reg1_country'] = $f['c'];
                    } else {
                        $f['ct'] = 0;
                        $f['r'] = 0;
                    }
                    if ($f['ct']) { # указан город или регион
                        if ($f['c'] == $f['r']) { # pid совпадает со страной, значит регион
                            $aFilter['reg2_region'] = $f['ct'];
                        } else { # иначе город
                            $aFilter['reg3_city'] = $f['ct'];
                        }
                    }
                } else {
                    if ($f['ct']) { # указан город или регион
                        if (Geo::defaultCountry() == $f['r']) { # pid совпадает со страной по умолчанию, значит регион
                            $aFilter['reg2_region'] = $f['ct'];
                        } else { # иначе город
                            $aFilter['reg3_city'] = $f['ct'];
                        }
                    }
                }
            }
        }

        # URL
        if ($nCatID) {
            if ($aCat['numlevel'] > 1) {
                $url = static::url('search-cat', array('cat_keyword1'=>$aCat['parent_keyword'], 'cat_keyword2'=>$aCat['keyword']), true);
            } else {
                $url = static::url('search-cat', array('cat_keyword1'=>$aCat['keyword']), true);
            }
        } else {
            $url = static::url('list', array(), true);
        }
        # Формирование списка товаров
        $nCount = $this->model->productsList($aFilter, true);
        if ($nCount) {
            $pgnQuery = $f; unset($pgnQuery['cat']);
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => static::urlDynamic($url),
                'query' => $pgnQuery,
            ));
            $aData['list'] = $this->model->productsList($aFilter, false, $pgn->getLimitOffset(), 'svc_fixed DESC, P.svc_fixed_order DESC, P.svc_order DESC');
            $bAddNumRow = ($f['q'] || $f['tag']);
            foreach ($aData['list'] as $k => &$v) {
                if ($bAddNumRow) {
                    $v['n'] = ($pgn->getCurrentPage() - 1) * $nPageSize + $k + 1;
                }
                if ($f['q']) {
                    foreach (explode(' ', $f['q']) as $vv) {
                        $v['title'] = str_replace($vv, '<em>' . $vv . '</em>', $v['title']);
                        $v['descr'] = str_replace($vv, '<em>' . $vv . '</em>', $v['descr']);
                    }
                }
            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        if ($f['q'] || $f['tag']) {
            $aData['list'] = $this->viewPHP($aData, 'search.keywords.list');
        } else {
            $aData['list'] = $this->viewPHP($aData, 'search.list');
        }
        $aData['cnt'] = $nCount;
        $aData['count'] = tpl::declension($nCount, _t('shop', 'товар;товара;товаров'));

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'   => $aData['pgn'],
                'list'  => $aData['list'],
                'count' => $aData['count'],
            ));
        }

        if ($f['q'] || $f['tag']) {
            # SEO: Поиск товаров по ключевому слову
            $this->seo()->robotsIndex(false);
            $this->setMeta('search-keyword', array(
                'query' => ( $f['tag'] ? ( ! empty($aData['sTagTitle']) ? $aData['sTagTitle'] : '') : tpl::truncate($f['q'], config::sysAdmin('shop.search.meta.query.truncate', 50, TYPE_UINT), '') ),
                'page'  => $f['page'],
            ));
            return $this->viewPHP($aData, 'search.keywords');
        }

        # SEO:
        $this->seo()->robotsIndex(!$seoFilter);
        $seoData = array('page' => $f['page']);
        if( ! Geo::filterEnabled()){
            if($f['c'])  $seoData['c'] = $f['c'];
            if($f['ct']) $seoData['ct'] = $f['ct'];
            if($f['r'])  $seoData['r'] = $f['r'];
        }
        if ($nCatID) {
            # SEO: Список (категория)
            $this->urlCorrection(static::urlDynamic($url));
            $this->seo()->canonicalUrl($url, $seoData);
            $this->setMeta('search-category', array(
                'category' => $aCat['title'],
                'page'     => $f['page'],
            ), $aCat);
            $this->seo()->setText($aCat, $f['page']);
        } else {
            # SEO: Список (все категории)
            $this->urlCorrection(static::urlDynamic($url));
            $this->seo()->canonicalUrl($url, $seoData);
            $this->setMeta('search', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        if ($nCatID) {
            $aData['dp'] = $this->dpForm($nCatID, true, $f, 'd', 'search.dp', $this->module_dir_tpl);
            $aData['pr'] = $this->priceForm($nCatID, true, $f);
        }

        # Фильтры поиска
        $aData['cats'] = $this->model->categoriesListAll(array('id', 'keyword', 'title'));

        $aData['region_title'] = ($aData['f']['ct'] ? Geo::i()->regionTitle($aData['f']['ct']) : '');
        $aData['form'] = $this->viewPHP($aData, 'search.form');

        return $this->viewPHP($aData, 'search');
    }

    /**
     * Настройка магазина пользователя
     * @return string HTML
     */
    public function my_settings()
    {
        $this->checkEnabled();

        $this->security->setTokenPrefix('shop-my-settings');
        $nUserID = User::id();

        $aParam = array(
            'shop_intro' => array(TYPE_NOTAGS, 'len' => 3000, 'len.sys' => 'shop.intro.limit'), # Описание
        );

        if (Request::isPOST()) {

            $aData = $this->input->postm($aParam);
            Users::model()->userSave($nUserID, $aData);
            $this->ajaxResponseForm(array());
        } else {
            $aData = Users::model()->userData($nUserID, array('shop_intro'));
        }

        if ($nUserID && Users::useClient() && ! User::isWorker()) {
            return $this->showForbidden('', _t('shop', 'Раздел "Магазин" доступен только исполнителям'));
        }
        if( ! $nUserID){
            return $this->showForbiddenGuests();
        }

        return $this->viewPHP($aData, 'settings');
    }

    /**
     * Кабинет: Магазин
     * @param array $userData данные о пользователе
     */
    public function my_shop(array $userData)
    {
        $this->checkEnabled();

        # просмотр товара
        $nProductID = $this->input->get('id', TYPE_UINT);
        if ($nProductID) {
            return $this->owner_view($nProductID, $userData);
        }

        # список товаров
        if (Users::useClient() && ! User::isWorker()) {
            $this->errors->error404();
        }
        return $this->owner_listing($userData['id'], $userData);
    }

    /**
     * Профиль: Магазин
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function user_shop(array $userData)
    {
        $this->checkEnabled();

        if (empty($userData['id'])) {
            $this->errors->error404();
        }

        # просмотр товара
        $nProductID = $this->input->get('id', TYPE_UINT);
        if ($nProductID) {
            return $this->owner_view($nProductID, $userData);
        }

        # список товаров
        return $this->owner_listing($userData['id'], $userData);
    }

    /**
     * Список товаров магазина пользователя
     * @param integer $userID ID пользователя
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    protected function owner_listing($userID, array $userData)
    {
        $this->checkEnabled();

        $pagination_on = false; # Использовать постраничную навигацию
        $nPageSize = config::sysAdmin('shop.profile.pagesize', 10, TYPE_UINT);

        $aData = Users::model()->userData($userID, array('shop_intro'));
        if (empty($aData)){
            $this->errors->error404();
        }

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            'cat'  => TYPE_UINT, # ID категории
        ));
        $aData['f'] = &$f;
        $aData['user'] = &$userData;

        $seoFilter = 0;
        $aFilter = array();

        if ($f['cat']) {
            $aFilter['cat_id1'] = $f['cat'];
            $seoFilter++;
        }

        $url = static::url('user.listing', array('login'=>$userData['login']), true);
        $aData['pgn'] = '';
        $aData['count'] = $this->model->productsOwnerList($userID, $aFilter, true);
        if ($aData['count']) {
            if ($pagination_on) {
                $pgn = new Pagination($aData['count'], $nPageSize, array(
                    'link' => static::urlDynamic($url),
                    'query' => $f,
                ));
                $aData['list'] = $this->model->productsOwnerList($userID, $aFilter, false, $pgn->getLimitOffset());
                $aData['pgn'] = $pgn->view();
                $f['page'] = $pgn->getCurrentPage();
            } else {
                $aData['list'] = $this->model->productsOwnerList($userID, $aFilter);
            }
            foreach ($aData['list'] as &$cat) {
                foreach ($cat as &$v) {
                    $v['url_view'] = static::url('user.view', array('id'=>$v['id'],'keyword'=>$v['keyword'],'login'=>$userData['login']));
                } unset($v);
            } unset($cat);
        }

        $aData['cats'] = array(
            0 => array(
                'id'    => 0,
                'title' => _t('shop', 'Все категории')
            ));
        $aData['cats'] += $this->model->categoriesOwnerList($userID);

        $aData['list'] = $this->viewPHP($aData, 'owner.list.ajax');
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'pgn'    => $aData['pgn'],
                    'list'   => $aData['list'],
                )
            );
        }

        # SEO: Магазин: список товаров
        $this->seo()->robotsIndex(!$seoFilter);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $this->seo()->setPageMeta($this->users(), 'profile-shop', array(
            'description' => tpl::truncate(strip_tags($aData['shop_intro']), config::sysAdmin('shop.intro.meta.description.truncate', 150, TYPE_UINT)),
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'region'  => $userData['region'],
            'country' => $userData['country'],
            'page'    => $f['page'],
        ), $aData);

        return $this->viewPHP($aData, 'owner.list');
    }

    /**
     * Просмотр товара в кабинете пользователя
     * @param integer $nProductID ID товара
     * @param array $userData данные о пользователе
     * @return string
     */
    protected function owner_view($nProductID, array $userData)
    {
        if ( ! $nProductID) {
            $this->errors->error404();
        }
        $aData = $this->model->productDataView($nProductID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        if ( ! $userData['my']) {
            if ($aData['status'] != static::STATUS_ACTIVE) {
                $this->errors->error404();
            }
            if (static::premoderation() && ! $aData['moderated']) {
                $this->errors->error404();
            }
        }

        # Изображения
        $aData['images'] = array();
        if (static::imagesLimit() && $aData['imgcnt']) {
            $oImages = $this->productImages($nProductID);
            $aImages = $oImages->getData($aData['imgcnt']);
            foreach ($aImages as $v) {
                $aData['images'][] = array(
                    'id'       => $v['id'],
                    'filename' => $v['filename'],
                    'i'        => $oImages->getURL($v, array(
                            ShopProductImages::szThumbnail,
                            ShopProductImages::szView
                        ), false
                    )
                );
            }
        }

        # Теги
        $aData['tags'] = $this->productTags()->tagsGet($nProductID);

        # Дин. свойства
        $aData['dynprops'] = $this->dpView($aData['cat_id'], $aData);

        # Следующий пост
        $aData['next'] = $this->model->productNext($nProductID, $userData['id'], $aData['created']);
        if ( ! empty($aData['next'])) {
            $aData['next']['link'] = static::url('user.view', array('id' => $aData['next']['id'], 'keyword' => $aData['next']['keyword'], 'login' => $userData['login']));
        }
        # Предыдущий пост
        $aData['prev'] = $this->model->productPrev($nProductID, $userData['id'], $aData['created']);
        if ( ! empty($aData['prev'])) {
            $aData['prev']['link'] = static::url('user.view', array('id' => $aData['prev']['id'], 'keyword' => $aData['prev']['keyword'], 'login' => $userData['login']));
        }

        # SEO: Магазин: просмотр товара (в профиле)
        $seoTags = array(); foreach ($aData['tags'] as $v) $seoTags[] = $v['tag'];
        $seoURL = static::url('view', array('id'=>$nProductID, 'keyword'=>$aData['keyword']), true);
        $this->urlCorrection(static::url('user.view', array('id'=>$nProductID, 'keyword'=>$aData['keyword'], 'login'=>$userData['login'])));
        $this->seo()->canonicalUrl($seoURL); // ссылка на товар в разделе "Магазин" (не в профиле)
        $this->seo()->setPageMeta($this->users(), 'profile-shop-view', array(
            'title' => tpl::truncate($aData['title'], config::sysAdmin('shop.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full' => $aData['title'],
            'description' => tpl::truncate(strip_tags($aData['descr']), config::sysAdmin('shop.view.meta.description.truncate', 150, TYPE_UINT)),
            'tags'    => join(', ', $seoTags),
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'country' => $userData['country'],
        ), $aData);
        # SEO: Open Graph
        $seoSocialImages = array();
        foreach ($aData['images'] as $v) $seoSocialImages[] = $v['i'][ShopProductImages::szView];
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], $seoSocialImages, $seoURL, $aData['share_sitename']);

        return $this->viewPHP($aData, 'owner.view');
    }

    public function productStatusBlock(array &$aData)
    {
        if(empty($aData['user_id'])) return '';
        if($aData['user_id'] != User::id()) return '';

        return $this->viewPHP($aData, 'status.block');
    }

    public function catSelect($nCount = 0, array $aData = array(), $aParam = array())
    {
        $aData = $aData + $aParam;
        $aData['count'] = $nCount;
        if (empty($aData['cat_id'])) {
            $aData['pid'] = $this->model()->categoryRootNodeID();
            $aData['pid_title'] = '';
            $nPid = $aData['pid'];
        } else {
            $nPid = ($aData['cat_id2'] == $aData['cat_id'] ? $aData['cat_id1'] : $this->model()->categoryRootNodeID());
            $aData = $aData + $this->model()->categoryData($nPid);
            $aData['pid_title'] = $this->model()->categoryParentsTitle($nPid, false, true, '/', $aData['aParents']);
        }
        $aData['aCats'] = $this->model()->categorySubOptions($nPid, false, array('enabled' => 1));

        return $this->viewPHP($aData, 'form.cat');
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            /**
             * Список подкатегорий для категории
             * @param int 'cat' ID категории
             */
            case 'form-menu':
            {
                $nCatID = $this->input->postget('cat', TYPE_UINT);
                $aData = $this->model()->categoryData($nCatID);
                $aData['pid_title'] = $this->model()->categoryParentsTitle($aData['id'], false, true, '/', $aData['aParents']);
                if ($aData['pid_title']) {
                    $aData['pid_title'] .= ' / ';
                }
                $aData['aCats'] = $this->model()->categorySubOptions($nCatID, false, array('enabled' => 1));
                $aResponse['menu'] = $this->viewPHP($aData, 'form.cat.ajax');
            } break;
            /**
             * Добавить новую
             */
            case 'add':
            {
                $nCount = $this->input->postget('cnt', TYPE_UINT);
                $aResponse['html'] = $this->catSelect($nCount, array(), array('allowDelete' => 1));

            } break;
            case 'tags-autocomplete':
            {
                $sQuery = $this->input->post('q', TYPE_NOTAGS);
                $this->productTags()->tagsAutocomplete($sQuery);
            } break;
            case 'dp-form':
            {
                $nID = $this->input->post('id', TYPE_UINT);
                $bSearch = $this->input->post('search', TYPE_BOOL);
                $sFormName = $bSearch ? 'search.dp' : 'form.dp';
                $aResponse['dp'] = $this->dpForm($nID, $bSearch, false, 'd', $sFormName, $this->module_dir_tpl);
                $aResponse['price'] = $this->priceForm($nID, $bSearch, false);
            } break;
            # дин. свойства: child-свойства
            case 'dp-child':
            {
                $p = $this->input->postm(array(
                        'dp_id'       => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value'    => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'name_prefix' => TYPE_NOTAGS, # Префикс для name
                    )
                );

                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                        $aData = $this->dp()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                                'name'  => $p['name_prefix'],
                                'class' => 'form-control'
                            ), false
                        );
                    $aResponse['form'] = $aData;
                }
            } break;
            case 'dp-child-search':
            {
                $p = $this->input->postm(array(
                        'dp_id'    => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value' => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'id'       => TYPE_UINT, # ID специализации или категории
                    )
                );

                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                    $aResponse['form'] = $this->dpForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                }
            } break;
            # форма редактирования категории пользователя
            case 'cat-edit-form':
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nCatID = $this->input->post('id', TYPE_UINT);
                $nUserID = User::id();
                $aData = $this->model->categoryUserData($nUserID, $nCatID);
                $aData['cat_id1'] = $nCatID;
                $aData['cats'] = $this->model->categoriesOwnerList($nUserID);
                unset($aData['cats'][$nCatID]);

                $aResponse['form'] = $this->viewPHP($aData, 'owner.form.cat');

            } break;
            # сохранение категории пользователя
            case 'cat-edit':
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nCatID = $this->input->post('cat_id1', TYPE_UINT);
                $nUserID = User::id();

                $aData = $this->input->postm(array(
                    'descr' => TYPE_NOTAGS,
                ));

                $nOrder = $this->input->post('order', TYPE_UINT);
                if($nOrder == 1){ # первый по счету;
                    $this->model->categoriesUserCheckNum($nUserID);
                    $aData['num'] = 1;
                    $this->model->categoriesUserNumInc($nUserID);
                }
                if($nOrder == 2) { # После другого раздела;
                    $this->model->categoriesUserCheckNum($nUserID);
                    $nCatAfterID = $this->input->post('cat_after', TYPE_UINT);
                    $aCatData = $this->model->categoryUserData($nUserID, $nCatAfterID);
                    $aData['num'] = $aCatData['num'] + 1;
                    $this->model->categoriesUserNumInc($nUserID, $aCatData['num']);
                }
                $this->model->categoryUserSave($nUserID, $nCatID, $aData);
                $aResponse['descr'] = nl2br($aData['descr']);
            } break;
            case 'product-delete': # удаление товара
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nProductID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->productData($nProductID);

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isProductOwner($nProductID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->productDelete($nProductID)) {
                    $this->errors->reloadPage();
                    break;
                }
            } break;
            case 'product-hide': # скрыть товар
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nProductID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->productData($nProductID, array('user_id', 'status', 'cat_id1'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isProductOwner($nProductID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_ACTIVE) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_NOT_ACTIVE;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->productSave($nProductID, $aUpdate);
                $this->model->categoryUserActiveCount($aData['user_id'], $aData['cat_id1']);
            } break;
            case 'product-show': # отобразить товар
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nProductID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->productData($nProductID, array('user_id', 'status', 'cat_id1'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isProductOwner($nProductID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_NOT_ACTIVE) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_ACTIVE;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->productSave($nProductID, $aUpdate);
                $this->model->categoryUserActiveCount($aData['user_id'], $aData['cat_id1']);
            } break;

            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Управление изображениями товара
     * @param getpost ::uint 'product_id' ID товара
     * @param getpost ::string 'act' действие
     */
    public function img()
    {
        $this->security->setTokenPrefix('shop-product-form');

        $nProductID = $this->input->getpost('item_id', TYPE_UINT);
        $oImages = $this->productImages($nProductID);
        $aResponse = array();

        switch ($this->input->getpost('act')) {
            case 'upload':
            {
                $aResponse = array('success' => false);
                do {
                    if (!$this->security->validateToken(true, false)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nProductID) {
                        if (!$this->isProductOwner($nProductID)) {
                            $this->errors->set(_t('shop', 'Вы не является владельцем данного товара'));
                            break;
                        }
                    }

                    $result = $oImages->uploadQQ();

                    $aResponse['success'] = ($result !== false && $this->errors->no());
                    if ($aResponse['success']) {
                        $aResponse = array_merge($aResponse, $result);
                        $aResponse['tmp'] = empty($nProductID);
                        $aResponse['i'] = $oImages->getURL($result, ShopProductImages::szForm, $aResponse['tmp']);
                        unset($aResponse['dir'], $aResponse['srv']);
                    }
                } while (false);

                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
            break;
            case 'delete':
            {
                $nImageID = $this->input->post('image_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_NOTAGS);

                # неуказан ID изображения ни filename временного
                if (!$nImageID && empty($sFilename)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->security->validateToken(true, false)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($nProductID) {
                    # проверяем доступ на редактирование
                    if (!$this->isProductOwner($nProductID)) {
                        $this->errors->set(_t('shop', 'Вы не является владельцем данного товара'));
                        break;
                    }
                }

                if ($nImageID) {
                    # удаляем изображение по ID
                    $oImages->deleteImage($nImageID);
                } else {
                    # удаляем временное
                    $oImages->deleteTmpFile($sFilename);
                }
            }
            break;
            case 'delete-tmp':
            {
                $aFilenames = $this->input->post('filenames', TYPE_ARRAY_STR);
                $oImages->deleteTmpFile($aFilenames);
            }
            break;
            default:
                $this->errors->reloadPage();
        }

        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Проверка, включен ли раздел "Магазин"
     */
    public function checkEnabled()
    {
        if (!static::enabled()) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
            } else {
                $this->errors->error404();
            }
        }
    }

    /**
     * Данные для карты сайта
     * @return array
     */
    public function getSitemapData()
    {
        $aCats = $this->model->categoriesListAll(array('id','keyword','title'));
        $url = static::url('list');
        if ($aCats) {
            foreach ($aCats as &$v) {
                $v['url'] = $url.$v['keyword'].'/';
                if( ! empty($v['sub'])){
                    foreach ($v['sub'] as &$vv) {
                        $vv['url'] = $v['url'].$vv['keyword'].'/';;
                    } unset($vv);
                }
            } unset($v);
        }

        return array(
            'cats' => $aCats,
            'link' => $url,
        );
    }

    # ------------------------------------------------------------------------------------------------------------------------------
    # Услуги

    public function promote()
    {
        $this->checkEnabled();

        if( ! bff::servicesEnabled()){
            $this->errors->error404();
        }

        $this->security->setTokenPrefix('shop-product-promote');

        $nProductID = $this->input->get('id', TYPE_UINT);
        if ( ! $nProductID) {
            $this->errors->error404();
        }

        if (Request::isAJAX()) {
            do {
                $aResponse = array();

                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->input->postm(array(
                    'svc' => TYPE_ARRAY_UINT,
                    'ps'  => TYPE_NOTAGS,
                    'fixed_days' => TYPE_UINT,
                ));
                extract($aData);

                $aProductData = $this->model->productData($nProductID, array('status', 'svc'));
                $nUserID = User::id();
                if ( ! $nUserID) {
                    break;
                }
                if (empty($aProductData)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($aProductData['status'] != static::STATUS_ACTIVE) {
                    $this->errors->reloadPage();
                    break;
                }

                $aSvcSettings = array(
                    'module' => $this->module_name,
                    'id'     => $nProductID,
                    'svc'    => array(),
                );
                if ( ! empty($fixed_days)) {
                    $aSvcSettings['fixed_days'] = $fixed_days;
                }
                $nSvcKeys = 0;
                $nSum = 0;
                foreach($svc as $v){
                    $aSvc = Svc::model()->svcData($v);
                    if(empty($aSvc)) continue;
                    if( ! $aSvc['on']) continue;
                    if($aSvc['module'] != $this->module_name) continue;
                    $sum = $aSvc['price'];
                    if ($v == static::SVC_FIX && ! empty($aSvc['per_day'])) {
                        if (empty($fixed_days)) {
                            $fixed_days = 1;
                        }
                        $sum = round($sum * $fixed_days, 2);
                    }
                    $nSum += $sum;
                    $nSvcKeys += $v;
                    $aSvcSettings['svc'][] = $v;
                }
                if( ! $nSum){
                    break;
                }
                $aSvcSettings['price'] = $nSum;
                if ($ps == 'balance') {
                    $nBalance = User::balance();
                    if ($nBalance >= $nSum) {
                        Svc::i()->activate($this->module_name, $nSvcKeys, array(), $nProductID, $nUserID, $nSum, false, $aSvcSettings);
                    } else {
                        $this->errors->set(_t('svc', 'Недостаточно средств. Пополните счет или выберите другой способ оплаты.'));
                        break;
                    }
                } else {
                    $aResponse['form'] = Bills::i()->createBill($ps, $nSum, $aSvcSettings, $nSvcKeys, $nProductID);
                }
                if($this->input->post('status', TYPE_UINT)){
                    $aResponse['redirect'] = static::url('status', array('id' => $nProductID));
                }

            } while (false);
            $this->ajaxResponseForm($aResponse);
        }

        $aData = $this->model->productsList(array('id' => $nProductID));
        $aData = reset($aData);

        $aData['bPromote'] = 1;
        $aData['list'] = array($aData);
        $aData['list'] = $this->viewPHP($aData, 'search.list');

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => $aData['title'], 'link' => static::url('user.view', array('id' => $aData['id'], 'keyword' => $aData['keyword'], 'login' => $aData['login']))),
            array('title' => _t('shop', 'Продвижение товара'), 'active' => true),
        );

        $aData['svc'] =  Svc::model()->svcListing(Svc::TYPE_SERVICE, $this->module_name);

        # определим возможность поднятия для товара
        if ($aData['svc_fixed']) {
            $sOrder = 'P.svc_fixed_order DESC';
        } else {
            $sOrder = 'P.svc_order DESC';
        }
        $aDataOrder = $this->model->productsList(array(), false, ' LIMIT 1 ', $sOrder);
        $aDataOrder = reset($aDataOrder);
        if($aDataOrder['id'] == $aData['id']){
            # в поднятии нет необходимости
            unset($aData['svc']['shop_up']);
        }

        $aData['preset'] = $this->input->get('preset', TYPE_UINT);
        $aData['svc_fixed_days'] = $this->input->get('svc_fixed_days', TYPE_UINT);
        $aData['bStatus'] = $this->input->get('status', TYPE_UINT);

        return $this->viewPHP($aData, 'promote');
    }

}