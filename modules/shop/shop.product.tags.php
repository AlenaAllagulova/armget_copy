<?php

class ShopProductTags_ extends bff\db\Tags
{
    protected function initSettings()
    {
        $this->tblTags = TABLE_TAGS;
        $this->postModeration = true;
        $this->tblTagsIn = TABLE_SHOP_PRODUCTS_IN_TAGS;
        $this->tblTagsIn_ItemID = 'product_id';
        $this->urlItemsListing = $this->adminLink('products&tag=', 'shop');
    }

    public function formFront($nItemID, $aParams = array())
    {
        $aData = array(
            'tags'   => ($nItemID > 0 ? $this->tagsGet($nItemID) : array()),
            'params' => $aParams,
        );

        return $this->viewPHP($aData, 'form.tags');
    }
}