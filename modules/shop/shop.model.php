<?php

define('TABLE_SHOP_PRODUCTS',         DB_PREFIX.'shop_products');
define('TABLE_SHOP_PRODUCTS_IMAGES',  DB_PREFIX.'shop_products_images');
define('TABLE_SHOP_PRODUCTS_IN_TAGS', DB_PREFIX.'shop_products_in_tags');
define('TABLE_SHOP_CATS_USERS',       DB_PREFIX.'shop_cats_users');
define('TABLE_SHOP_PRODUCTS_REGIONS', DB_PREFIX.'shop_products_regions');

class ShopModel_ extends Model
{
    /** @var ShopBase */
    protected $controller;

    /** @var bff\db\NestedSetsTree для категорий */
    public $treeCategories;
    public $langCategories = array(
        'title'               => TYPE_NOTAGS, # Название
        # Магазин
        'shop_mtitle'         => TYPE_NOTAGS, # Meta Title
        'shop_mkeywords'      => TYPE_NOTAGS, # Meta Keywords
        'shop_mdescription'   => TYPE_NOTAGS, # Meta Description
        'shop_seotext'        => TYPE_STR,    # SEO-Text
        # Заказы
        'orders_mtitle'       => TYPE_NOTAGS, # Meta Title
        'orders_mkeywords'    => TYPE_NOTAGS, # Meta Keywords
        'orders_mdescription' => TYPE_NOTAGS, # Meta Description
        'orders_seotext'      => TYPE_STR,    # SEO-Text
        # Цена
        'price_title'         => TYPE_NOTAGS, # Цена: название
        'price_title_mod'     => TYPE_NOTAGS, # Цена: название модификатора
    );

    public $langSvcServices = array(
        'title_view'       => TYPE_NOTAGS, # название
        'description'      => TYPE_STR, # описание (краткое)
        'description_full' => TYPE_STR, # описание (подробное)
    );

    public $langSvcPacks = array(
        'title_view'       => TYPE_NOTAGS, # название
        'description'      => TYPE_STR,    # описание (краткое)
        'description_full' => TYPE_STR,    # описание (подробное)
    );

    const CATEGORIES_ROOTID = 1;

    public function init()
    {
        parent::init();

        # подключаем nestedSets категории
        $this->treeCategories = new bff\db\NestedSetsTree(TABLE_SHOP_CATS);
        $this->treeCategories->init();
    }

    /**
     * Список категорий
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '') //admin
    {
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter[] = 'pid != 0';
        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM ' . TABLE_SHOP_CATS . ' C, ' . TABLE_SHOP_CATS_LANG . ' CL ' . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT C.id, C.created, CL.title, C.enabled, C.pid, C.numlevel,
                    COUNT(P.id) as products, ((C.numright-C.numleft)-1) as node
               FROM ' . TABLE_SHOP_CATS . ' C, ' . TABLE_SHOP_CATS_LANG . ' CL
                LEFT JOIN '.TABLE_SHOP_PRODUCTS.' P ON P.cat_id1 = CL.id OR P.cat_id2 = CL.id
               ' . $aFilter['where']
            . ' GROUP BY C.id '
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список категорий
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $aFilter[] = 'pid != 0';
        if (!$bCount) {
            $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        }
        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM ' . TABLE_SHOP_CATS . ' C ' . $aFilter['where'], $aFilter['bind']);
        }

        $aData = $this->db->select('SELECT C.id
                                  FROM ' . TABLE_SHOP_CATS . ' C, ' . TABLE_SHOP_CATS_LANG . ' CL
                                  ' . $aFilter['where'] . '
                                  ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
                                  ' . $sqlLimit, $aFilter['bind']
        );

        if (!empty($aData)) {
            //
        }

        return $aData;
    }

    /**
     * Список категорий пользователя
     * @param integer $nUserID ID пользователя
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesOwnerList($nUserID, array $aFilter = array(), $bCount = false, $sqlLimit = '') // frontend
    {
        $aFilter[] = 'pid != 0';
        $aFilter[':user'] = array('U.cat_id1 = C.id AND U.user_id = :user', ':user' => $nUserID);

        if($nUserID != User::id()){
            $aFilter[':active'] = 'U.active_cnt > 0';
        } else {
            $aFilter[':active'] = 'U.product_cnt > 0';
        }

        if ( ! $bCount) {
            $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        }

        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM ' . TABLE_SHOP_CATS_USERS . ' U, ' . TABLE_SHOP_CATS . ' C ' . $aFilter['where'], $aFilter['bind']);
        }

        $sqlOrder = 'U.num, C.numleft';
        $aData = $this->db->select_key('
            SELECT C.id, C.price, C.price_ex, CL.title, CL.price_title_mod, U.descr, U.product_cnt, U.active_cnt
            FROM ' . TABLE_SHOP_CATS_USERS . ' U, ' . TABLE_SHOP_CATS . ' C, ' . TABLE_SHOP_CATS_LANG . ' CL
            ' . $aFilter['where'] . '
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, 'id', $aFilter['bind']
        );

        if ( ! empty($aData)) {
            //
        }

        return $aData;
    }

    /**
     * Список всех категорий с подкатегориями
     * @param array $aFields - выбираемые поля
     * @return mixed
     */
    public function categoriesListAll(array $aFields = array()) // frontend
    {
        if (!in_array('id', $aFields)) {
            $aFields[] = 'id';
        }
        if (!in_array('pid', $aFields)) {
            $aFields[] = 'pid';
        }
        foreach ($aFields as $k => $v) {
            $aFields[$k] = (array_key_exists($v, $this->langCategories) ? 'CL.' : 'C.') . $v;
        }

        $aFilter = array('pid != 0', 'enabled'=>1);
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aData = $this->db->select('SELECT ' . join(',', $aFields) . ', ((C.numright-C.numleft)>1) as subs
                                  FROM ' . TABLE_SHOP_CATS . ' C, ' . TABLE_SHOP_CATS_LANG . ' CL
                                  ' . $aFilter['where'] . $this->db->langAnd(true, 'C', 'CL') . '
                                  ORDER BY C.numleft
                                  ', $aFilter['bind']
        );

        if (!empty($aData)) {
            $aData = $this->db->transformRowsToTree($aData, 'id', 'pid', 'sub');
        }

        return $aData;
    }

    /**
     * Получение данных категории
     * @param integer $nCategoryID ID категории
     * @param array $aFields необходимые поля
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function categoryData($nCategoryID, $aFields = array(), $bEdit = false)
    {
        if (empty($nCategoryID)) return array();

        return $this->categoryDataByFilter(array('id' => $nCategoryID), $aFields, $bEdit);
    }

    /**
     * Получение данных категории на основе фильтра
     * @param integer $nCategoryID ID категории
     * @param array $aFields необходимые поля
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function categoryDataByFilter($aFilter, $aFields = array(), $bEdit = false)
    {
        $aParams = array();
        if (empty($aFields) || $bEdit) $aFields = '*';
        if ($aFields == '*') {
            $aParams = array($aFields);
        } else {
            if (!is_array($aFields)) {
                $aFields = array($aFields);
            }
            foreach ($aFields as $v) {
                if (isset($this->langCategories[$v])) {
                    $v = 'CL.' . $v;
                } elseif ($v == 'subs') {
                    $v = '((C.numright-C.numleft)>1) as subs';
                } else {
                    $v = 'C.' . $v;
                }
                $aParams[] = $v;
            }
        }

        $aFilter[':lng'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aData = $this->db->one_array('SELECT ' . join(',', $aParams) . '
                       FROM ' . TABLE_SHOP_CATS . ' C,
                            ' . TABLE_SHOP_CATS_LANG . ' CL
                       ' . $aFilter['where'] . '
                       LIMIT 1', $aFilter['bind']
        );

        if ($bEdit) {
            $aData['node'] = ($aData['numright'] - $aData['numleft']);
            $this->db->langSelect($aData['id'], $aData, $this->langCategories, TABLE_SHOP_CATS_LANG);
        }

        return $aData;
    }

    /**
     * Получение данных категорий по keywords
     * @param array $aKeywords
     * @return array
     */
    public function categoriesDataByKeywords(array $aKeywords = array())
    {
        return $this->db->select_key('
            SELECT id, pid, keyword, numlevel
            FROM ' . TABLE_SHOP_CATS . '
            WHERE ' . $this->db->prepareIN('keyword', $aKeywords, false, true, false), 'keyword'
        );
    }

    /**
     * Получение meta данных категории
     * @param mixed $mCategory ID / Keyword категории
     * @param string $sModuleName название модуля
     * @return array
     */
    public function categoryMeta($mCategory, $sModuleName)
    {
        $prefix = mb_strtolower($sModuleName).'_';
        $aData = $this->db->one_array('SELECT
                    C.id, C.pid, C.keyword, CP.keyword as parent_keyword, C.numlevel, CL.title,
                    C.'.$prefix.'mtemplate as mtemplate,
                    CL.'.$prefix.'mtitle as mtitle,
                    CL.'.$prefix.'mkeywords as mkeywords,
                    CL.'.$prefix.'mdescription as mdescription,
                    CL.'.$prefix.'seotext as seotext
                FROM ' . TABLE_SHOP_CATS . ' C
                    LEFT JOIN '.TABLE_SHOP_CATS.' CP ON C.pid = CP.id,
                    ' . TABLE_SHOP_CATS_LANG . ' CL
                WHERE C.'.(is_int($mCategory) ? 'id' : 'keyword').' = :cat
                    ' . $this->db->langAnd(true, 'C', 'CL'),
            array(':cat' => $mCategory)
        );
        return $aData;
    }

    /**
     * Получение данных категории пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nCatID ID категории
     * @return mixed
     */
    public function categoryUserData($nUserID, $nCatID)
    {
        return $this->db->one_array('
            SELECT *
            FROM '.TABLE_SHOP_CATS_USERS.' U
            WHERE U.user_id = :user AND U.cat_id1 = :cat
            ', array(':user' => $nUserID, ':cat' => $nCatID));
    }

    /**
     * Сохранение данных категории пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nCatID ID категории
     * @param array $aData данные категории
     * @return bool
     */
    public function categoryUserSave($nUserID, $nCatID, $aData)
    {
        if(empty($aData)) return false;
        return $this->db->update(TABLE_SHOP_CATS_USERS, $aData, array('user_id' => $nUserID, 'cat_id1' => $nCatID));
    }

    /**
     * Включение заданного порядка сортировки категорий пользователя
     * @param integer $nUserID ID пользователя
     * @return bool
     */
    public function categoriesUserCheckNum($nUserID)
    {
        $nMaxNum = $this->db->one_data('SELECT MAX(num) FROM '.TABLE_SHOP_CATS_USERS.' U WHERE U.user_id = :user', array(':user' => $nUserID));
        if( ! $nMaxNum){
            $this->db->exec('
                UPDATE '.TABLE_SHOP_CATS_USERS.' U
                    INNER JOIN '.TABLE_SHOP_CATS.' C ON U.cat_id1 = C.id
                SET U.num = C.numleft
                WHERE U.user_id = :user
            ', array(':user' => $nUserID));
            $this->db->exec('
                UPDATE '.TABLE_SHOP_CATS_USERS.' SET num = (SELECT @n:=@n+1 FROM (SELECT @n:=0) AS t)
                WHERE user_id = :user
                ORDER BY num;
            ', array(':user' => $nUserID));
            return true;
        }
        return false;
    }

    /**
     * Увеличения номера порядка сортировки категорий пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nNum номер категории с которой увеличить
     * @return boolean|integer
     */
    public function categoriesUserNumInc($nUserID, $nNum = 0)
    {
        return $this->db->exec('
                UPDATE '.TABLE_SHOP_CATS_USERS.' SET num = num + 1
                WHERE user_id = :user AND num > :num
                ORDER BY num;
            ', array(':user' => $nUserID, ':num' => $nNum));

    }

    /**
     * Сохранение категории
     * @param integer $nCategoryID ID категории
     * @param array $aData данные категории
     * @return boolean|integer
     */
    public function categorySave($nCategoryID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }
        $aSave = array_diff_key($aData, $this->langCategories);
        if (isset($aData['title'])) {
            $aSave['title'] = $aData['title'][LNG];
        }

        if ($nCategoryID > 0) {
            $aSave['modified'] = $this->db->now(); # Дата изменения

            if (isset($aData['pid'])) {
                unset($aData['pid']);
            } # запрет изменения pid
            $res = $this->db->update(TABLE_SHOP_CATS, $aSave, array('id' => $nCategoryID));

            $this->db->langUpdate($nCategoryID, $aData, $this->langCategories, TABLE_SHOP_CATS_LANG);

            return !empty($res);
        } else {
            $aSave['created'] = $this->db->now(); # Дата создания
            $aSave['modified'] = $this->db->now(); # Дата изменения

            $nCategoryID = $this->treeCategories->insertNode($aSave['pid']);
            if ($nCategoryID > 0) {
                unset($aSave['pid']);
                $this->db->update(TABLE_SHOP_CATS, $aSave, 'id = :id', array(':id' => $nCategoryID));
                $this->db->langInsert($nCategoryID, $aData, $this->langCategories, TABLE_SHOP_CATS_LANG);
            }

            return $nCategoryID;
        }
    }

    /**
     * Переключатели категории
     * @param integer $nCategoryID ID категории
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function categoryToggle($nCategoryID, $sField)
    {
        switch ($sField) {
            case 'enabled': # Включен
            {
                $res = $this->toggleInt(TABLE_SHOP_CATS, $nCategoryID, 'enabled', 'id');
                if ($res) {
                    $aCategoryData = $this->categoryData($nCategoryID, array('enabled', 'numleft', 'numright'));
                    if (!empty($aCategoryData)) {
                        $this->db->update(TABLE_SHOP_CATS, array(
                                'enabled' => $aCategoryData['enabled'],
                            ), array(
                                'numleft > :left AND numright < :right'
                            ), array(
                                ':left'  => $aCategoryData['numleft'],
                                ':right' => $aCategoryData['numright'],
                            )
                        );
                    }
                }
                return $res;
            }
            break;
        }
    }

    /**
     * Является ли категория основной
     * @param integer $nCategoryID ID категории
     * @param integer $nParentID ID parent-категории (для избежания запроса к БД) или false
     * @return boolean true - основная, false - подкатегория
     */
    public function categoryIsMain($nCategoryID, $nParentID = false)
    {
        if (!empty($nParentID)) {
            return ($nParentID == self::CATEGORIES_ROOTID);
        } else {
            $nNumlevel = $this->treeCategories->getNodeNumlevel($nCategoryID);

            return ($nNumlevel == 1);
        }
    }

    /**
     * Перемещение категории
     * @return mixed @see rotateTablednd
     */
    public function categoriesRotate()
    {
        return $this->treeCategories->rotateTablednd();
    }

    /**
     * Удаление категории
     * @param integer $nCategoryID ID категории
     * @return boolean
     */
    public function categoryDelete($nCategoryID)
    {
        if (empty($nCategoryID)) {
            return false;
        }
        $nSubCnt = $this->categorySubCount($nCategoryID);
        if (!empty($nSubCnt)) {
            $this->errors->set('Невозможно выполнить удаление категории при наличии подкатегорий');

            return false;
        }

        $nItems = $this->db->one_data('SELECT COUNT(I.id) FROM ' . TABLE_SHOP_PRODUCTS . ' I WHERE I.cat_id = :id', array(':id' => $nCategoryID));
        if (!empty($nItems)) {
            $this->errors->set('Невозможно выполнить удаление категории при наличии вложенных элементов');

            return false;
        }

        $aDeletedID = $this->treeCategories->deleteNode($nCategoryID);
        $res = !empty($aDeletedID);
        if (!empty($res)) {
            $this->db->delete(TABLE_SHOP_CATS_LANG, array('id' => $nCategoryID));

            return true;
        }

        return false;
    }

    /**
     * Получаем кол-во вложенных категорий
     */
    public function categorySubCount($nCategoryID)
    {
        return $this->treeCategories->getChildrenCount($nCategoryID);
    }

    /**
     * Формирование списка подкатегорий
     * @param integer $nCategoryID ID категории
     * @param mixed $mOptions формировать select-options или FALSE
     * @param array $aFilter фильтр категорий
     * @return array|string
     */
    public function categorySubOptions($nCategoryID, $mOptions = false, $aFilter = array())
    {
        $aFilter['pid'] = $nCategoryID;
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aData = $this->db->select('SELECT C.id, CL.title, C.numlevel, ((C.numright-C.numleft)-1) as node
                    FROM ' . TABLE_SHOP_CATS . ' C, ' . TABLE_SHOP_CATS_LANG . ' CL
                    ' . $aFilter['where'] . '
                    ORDER BY C.numleft', $aFilter['bind']
        );

        if (empty($mOptions)) {
            return $aData;
        }

        return HTML::selectOptions($aData, $mOptions['sel'], $mOptions['empty'], 'id', 'title');
    }

    /**
     * Формирование списка основных категорий
     * @param integer $nSelectedID ID выбранной категории
     * @param mixed $mEmptyOpt невыбранное значение
     * @param integer $nType тип списка: 0 - все(кроме корневого), 1 - список при добавлении категории, 2 - список при добавлении записи
     * @param array $aOnlyID только список определенных категорий
     * @return string <option></option>...
     */
    public function categoriesOptions($nSelectedID = 0, $mEmptyOpt = false, $nType = 0, $aOnlyID = array())
    {
        $aFilter = array();
        if ($nType == 1) {
            $aFilter[] = 'numlevel < 2';
        } else {
            $aFilter[] = 'numlevel > 0';
        }
        if (!empty($aOnlyID)) {
            $aFilter[':only'] = '(C.id IN (' . join(',', $aOnlyID) . ') OR C.pid IN(' . join(',', $aOnlyID) . '))';
        }

        # Chrome не понимает style="padding" в option
        $bUsePadding = (mb_stripos(Request::userAgent(), 'chrome') === false);
        $bJoinItems = ($nType > 0);
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aCategories = $this->db->select('SELECT C.id, CL.title, C.numlevel, ((C.numright-C.numleft)-1) as node
                    ' . ($bJoinItems ? ', COUNT(I.id) as items ' : '') . '
               FROM ' . TABLE_SHOP_CATS . ' C
                    ' . ($bJoinItems ? ' LEFT JOIN ' . TABLE_SHOP_PRODUCTS . ' I ON C.id = I.cat_id ' : '') . '
                    , ' . TABLE_SHOP_CATS_LANG . ' CL
               ' . $aFilter['where'] . '
               GROUP BY C.id
               ORDER BY C.numleft', $aFilter['bind']
        );

        $sOptions = '';
        foreach ($aCategories as $v) {
            $nNumlevel = & $v['numlevel'];
            $bDisable = ($nType > 0 && ($nType == 2 ? $v['node'] > 0 : ($nNumlevel > 0 && $v['items'] > 0)));
            $sOptions .= '<option value="' . $v['id'] . '" ' .
                ($bUsePadding && $nNumlevel > 1 ? 'style="padding-left:' . ($nNumlevel * 10) . 'px;" ' : '') .
                ($v['id'] == $nSelectedID ? ' selected' : '') .
                ($bDisable ? ' disabled' : '') .
                '>' . (!$bUsePadding && $nNumlevel > 1 ? str_repeat('&nbsp;', $nNumlevel) : '') . $v['title'] . '</option>';
        }

        if ($mEmptyOpt !== false) {
            $nValue = 0;
            if (is_array($mEmptyOpt)) {
                $nValue = key($mEmptyOpt);
                $mEmptyOpt = current($mEmptyOpt);
            }
            $sOptions = '<option value="' . $nValue . '" class="bold">' . $mEmptyOpt . '</option>' . $sOptions;
        }

        return $sOptions;
    }

    /**
     * Формирование списков категорий (при добавлении/редактировании записи)
     * @param array $aCategoriesID ID категорий [lvl=>selectedID, ...]
     * @param mixed $mOptions формировать select-options или нет (false)
     * @return array [lvl=>[a=>selectedID, categories=>список категорий(массив или options)],...]
     */
    public function categoriesOptionsByLevel($aCategoriesID, $mOptions = false)
    {
        if (empty($aCategoriesID)) {
            return array();
        }

        # формируем список требуемых уровней категорий
        $aLevels = array();
        $bFill = true;
        $parentID = 1;
        foreach ($aCategoriesID as $lvl => $nCategoryID) {
            if ($nCategoryID || $bFill) {
                $aLevels[$lvl] = $parentID;
                if (!$nCategoryID) {
                    break;
                }
                $parentID = $nCategoryID;
            } else {
                break;
            }
        }

        if (empty($aLevels)) {
            return array();
        }

        $aData = $this->db->select('SELECT C.id, CL.title, C.numlevel as lvl
                    FROM ' . TABLE_SHOP_CATS . ' C, ' . TABLE_SHOP_CATS_LANG . ' CL
                    WHERE C.numlevel IN (' . join(',', array_keys($aLevels)) . ')
                      AND C.pid IN(' . join(',', $aLevels) . ')
                      ' . $this->db->langAnd(true, 'C', 'CL') . '
                    ORDER BY C.numleft'
        );
        if (empty($aData)) {
            return array();
        }

        $aLevels = array();
        foreach ($aData as $v) {
            $aLevels[$v['lvl']][$v['id']] = $v;
        }
        unset($aData);

        foreach ($aCategoriesID as $lvl => $nSelectedID) {
            if (isset($aLevels[$lvl])) {
                $aCategoriesID[$lvl] = array(
                    'a'          => $nSelectedID,
                    'categories' => (!empty($mOptions) ?
                            HTML::selectOptions($aLevels[$lvl], $nSelectedID, $mOptions['empty'], 'id', 'title') :
                            $aLevels[$lvl]),
                );
            } else {
                $aCategoriesID[$lvl] = array(
                    'a'          => $nSelectedID,
                    'categories' => false,
                );
            }
        }

        return $aCategoriesID;
    }

    /**
     * Получаем данные parent-категорий
     * @param integer $nCategoryID ID категории
     * @param bool $bIncludingSelf включать текущую в итоговых список
     * @param bool $bExludeRoot исключить корневой раздел
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsID($nCategoryID, $bIncludingSelf = true, $bExludeRoot = true)
    {
        if (empty($nCategoryID)) {
            return array(1 => 0);
        }
        $aData = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf, array(
                'id',
                'numlevel'
            )
        );
        $aParentsID = array();
        if (!empty($aData)) {
            foreach ($aData as $v) {
                $aParentsID[$v['numlevel']] = $v['id'];
            }
        }

        return $aParentsID;
    }

    /**
     * Получаем названия parent-категорий
     * @param integer $nCategoryID ID категории
     * @param boolean $bIncludingSelf включать текущую в итоговых список
     * @param boolean $bExludeRoot исключить корневой раздел
     * @param mixed $mSeparator объединить в одну строку или FALSE
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsTitle($nCategoryID, $bIncludingSelf = false, $bExludeRoot = false, $mSeparator = true, & $aParentsID = null)
    {
        $aParentsID = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf);
        if (empty($aParentsID)) {
            return ($mSeparator !== false ? '' : array());
        }

        $aData = $this->db->select_one_column('SELECT CL.title
                   FROM ' . TABLE_SHOP_CATS . ' C
                      , ' . TABLE_SHOP_CATS_LANG . ' CL
                   WHERE ' . $this->db->prepareIN('C.id', $aParentsID) . '' . $this->db->langAnd(true, 'C', 'CL') . '
                   ORDER BY C.numleft'
        );

        $aData = (!empty($aData) ? $aData : array());

        if ($mSeparator !== false) {
            return join('  ' . ($mSeparator === true ? '>' : $mSeparator) . '  ', $aData);
        } else {
            return $aData;
        }
    }

    /**
     * Смена parent-категории
     * @param integer $nCategoryID ID перемещаемой категории
     * @param integer $nNewParentID ID новой parent-категории
     * @return boolean
     */
    public function catChangeParent($nCategoryID, $nNewParentID)
    {
        return $this->treeCategories->changeParent($nCategoryID, $nNewParentID);
    }

    /**
     * Список категорий с родителями из массива
     * @param array $aCategories массив требуемых категория
     * @return array
     */
    public function categoriesListingInArray(array $aCategories)
    {
        if (empty($aCategories)) {
            return array();
        }
        $aData = $this->db->select_key('
            SELECT C.id, CL.title, C.enabled, C.pid, C.numlevel, ((C.numright-C.numleft)-1) as node, CL1.title as pid_title,
                   C.keyword,  C.price, C.price_ex, CL.price_title_mod
            FROM ' . TABLE_SHOP_CATS . ' C
                 LEFT JOIN ' . TABLE_SHOP_CATS . ' C1 ON C.pid = C1.id
                 LEFT JOIN ' . TABLE_SHOP_CATS_LANG . ' CL1 ON ' . $this->db->langAnd(false, 'C1', 'CL1') . '
                 ,
                 ' . TABLE_SHOP_CATS_LANG . ' CL
            WHERE ' . $this->db->langAnd(false, 'C', 'CL') . ' AND ' . $this->db->prepareIN('C.id', $aCategories) . '
            ORDER BY C.numleft
        ', 'id');
        if (empty($aData)) {
            $aData = array();
        }
        return $aData;
    }

    /**
     * Получим ID корневой категории
     * @return int
     */
    public function categoryRootNodeID()
    {
        return $this->treeCategories->getRootNodeID(0, $bCreated);
    }

    /**
     * Проверка существования прикрепления категории к пользователю. (при добавлении товара)
     * @param $nUserID integer ID пользователя
     * @param $nCategoryID integer ID категории
     */
    protected function categoryUserCheck($nUserID, $nCategoryID)
    {
        $aData = $this->db->one_array('SELECT * FROM '.TABLE_SHOP_CATS_USERS.' WHERE user_id = :user AND cat_id1 = :cat',
            array(
                'user' => $nUserID,
                'cat'  => $nCategoryID,
            ));
        $nProductCnt = $this->db->one_data('SELECT COUNT(ID) FROM '.TABLE_SHOP_PRODUCTS.'
                WHERE user_id = :user AND cat_id1 = :cat', array('user' => $nUserID, 'cat' => $nCategoryID));
        $nActiveCnt = $this->db->one_data('SELECT COUNT(ID) FROM '.TABLE_SHOP_PRODUCTS.'
                WHERE user_id = :user AND cat_id1 = :cat AND status = :active '.(Shop::premoderation() ? ' AND moderated = 1 ' : '').'
                ', array('user' => $nUserID, 'cat' => $nCategoryID, ':active' => Shop::STATUS_ACTIVE));
        if(empty($aData)){

            $aData = array(
                'user_id'     => $nUserID,
                'cat_id1'     => $nCategoryID,
                'product_cnt' => $nProductCnt,
                'active_cnt'  => $nActiveCnt,
            );
            $nMaxNum = $this->db->one_data('SELECT MAX(num) FROM '.TABLE_SHOP_CATS_USERS.' U WHERE U.user_id = :user', array(':user' => $nUserID));
            if($nMaxNum){
                $aData['num'] = $nMaxNum + 1;
            }
            $this->db->insert(TABLE_SHOP_CATS_USERS, $aData);
        } else {
            if($nProductCnt != $aData['product_cnt'] || $nActiveCnt != $aData['active_cnt']){
                $this->db->update(TABLE_SHOP_CATS_USERS, array('product_cnt' => $nProductCnt, 'active_cnt'  => $nActiveCnt),
                    array('user_id' => $nUserID, 'cat_id1' => $nCategoryID));
            }
        }
    }

    /**
     * Пересчет счетчика активных товаров для категории пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nCategoryID ID категории
     */
    public function categoryUserActiveCount($nUserID, $nCategoryID)
    {

        $this->db->exec('
            UPDATE '.TABLE_SHOP_CATS_USERS.' C LEFT JOIN (
                SELECT P.user_id, P.cat_id1, COUNT(P.id) AS cnt
                FROM '.TABLE_SHOP_PRODUCTS.' P
                WHERE P.status = :status '.(Shop::premoderation() ? ' AND P.moderated = 1 ' : '').'
                  AND P.user_id = :user AND P.cat_id1 = :cat
                GROUP BY P.user_id, P.cat_id1
                ) Cp ON C.user_id = Cp.user_id AND C.cat_id1 = Cp.cat_id1
            SET C.active_cnt = Cp.cnt
            WHERE C.user_id = :user AND C.cat_id1 = :cat
        ', array(':status' => Shop::STATUS_ACTIVE, ':user' => $nUserID, ':cat' => $nCategoryID));
    }

    # Товары

    /**
     * Список товаров
     * @param array $aFilter фильтр списка товаров
     * @param bool $bCount только подсчет кол-ва товаров
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function productsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '') //admin
    {
        $sFrom = TABLE_SHOP_PRODUCTS . ' P ';
        if (array_key_exists(':user', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS . ' U ';
            $aFilter[':users-join'] = ' P.user_id = U.user_id ';
        }
        if (isset($aFilter[':tag'])) {
            $sFrom .= ', ' . TABLE_SHOP_PRODUCTS_IN_TAGS . ' T ';
            $aFilter[':tag'] = array('P.id = T.product_id AND T.tag_id = :tag',
                                     ':tag'=>$aFilter[':tag']);
        }
        $aFilter = $this->prepareFilter($aFilter, 'P');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(P.id) FROM ' . $sFrom . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT P.id, P.created, P.keyword, P.title, P.user_id, P.imgcnt, P.status, P.moderated
               FROM ' . $sFrom . '
               ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список товаров
     * @param array $aFilter фильтр списка товаров
     * @param bool $bCount только подсчет кол-ва товаров
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function productsList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $sFrom = '';

        $bTagTT = false;
        # полнотекстовый
        if (array_key_exists('text', $aFilter)) {
            $aTagIDs = $this->db->select_one_column('SELECT id FROM '.TABLE_TAGS.' WHERE tag LIKE :tag', array(':tag' => trim($aFilter['text'])));
            $aFilter[':text'] = ' ( '.$this->db->prepareFulltextQuery($aFilter['text'], 'P.title, P.descr');
            if( ! empty($aTagIDs)){
                $aFilter[':text'] .= ' OR '.$this->db->prepareIN('TT.tag_id', $aTagIDs);
                $bTagTT = true;
            }
            $aFilter[':text'] .= ' ) ';
            unset($aFilter['text']);
        }

        # фильтр по тегу
        if (array_key_exists('tag_id', $aFilter)) {
            $bTagTT = true;
            $aFilter[':tag_id'] = array('TT.tag_id = :tag', 'tag' => $aFilter['tag_id']);
            unset($aFilter['tag_id']);
        }

        if($bTagTT){
            $sFrom .= ', ' . TABLE_SHOP_PRODUCTS_IN_TAGS . ' TT ';
            $aFilter[':jtags'] = 'P.id = TT.product_id ';
        }

        # фильтр по региону (если есть)
        $bRegions = Shop::regionsLimit();
        $bLjRegion = false;
        if($bRegions) {
            $bLjRegion = true;
            foreach (array('reg1_country', 'reg2_region', 'reg3_city') as $v) {
                if (!isset($aFilter[$v])) {
                    continue;
                }
                if (!isset($aFilter[':jregions'])) {
                    $sFrom .= ', ' . TABLE_SHOP_PRODUCTS_REGIONS . ' R ';
                    $aFilter[':jregions'] = 'P.id = R.product_id ';
                    $bLjRegion = false;
                }
                $aFilter[':' . $v] = array('R.' . $v . ' = :v' . $v, ':v' . $v => $aFilter[$v]);
                unset($aFilter[$v]);
            }
        }

        if (empty($aFilter['id'])) {
            $aFilter['status'] = Shop::STATUS_ACTIVE;
            if (Shop::premoderation()) {
                $aFilter['moderated'] = 1;
            }
        }

        $aFilter[':user'] = 'P.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'P');
            return $this->db->one_data('SELECT COUNT(id) FROM (
                SELECT P.id FROM ' . TABLE_SHOP_PRODUCTS . ' P ' . $sFrom .', '.TABLE_USERS.' U '.
                    $aFilter['where'] . ' GROUP BY P.id ) sl',
                    $aFilter['bind']);
        }

        $aFilter[':cat'] = 'P.cat_id1 = C.id';
        $aFilter[':cat_lang'] = $this->db->langAnd(false, 'C', 'CL');

        $aFilter = $this->prepareFilter($aFilter, 'P');

        $aData = $this->db->select('
            SELECT P.id, P.title, P.keyword, P.descr, P.cat_id1, P.cat_id2, P.cat_id, P.created, P.price, P.price_curr, P.price_ex,
                   P.imgcnt, P.img_s, P.status, P.moderated, P.user_id,
                   GROUP_CONCAT(T.tag_id) AS tags, '.($bRegions ? 'MAX(R.reg3_city) AS reg3_city, ' : '').'
                   C.keyword AS cat_keyword1, CL.title AS cat_title1, C.price AS cat_price, C.price_ex AS cat_price_ex, CL.price_title_mod,
                   U.login, U.name, U.surname, U.verified, U.pro,
                   ((P.svc & ' . Shop::SVC_MARK . ') > 0) as svc_marked,
                   ((P.svc & ' . Shop::SVC_FIX . ') > 0) as svc_fixed
            FROM ' . TABLE_SHOP_PRODUCTS . ' P
                LEFT JOIN ' . TABLE_SHOP_PRODUCTS_IN_TAGS . ' T ON P.id = T.product_id ' .
                ($bLjRegion ? ' LEFT JOIN ' . TABLE_SHOP_PRODUCTS_REGIONS . ' R ON P.id = R.product_id ' : '') .
            $sFrom . ' , '.TABLE_SHOP_CATS.' C, '.TABLE_SHOP_CATS_LANG.' CL, '.TABLE_USERS.' U '.
            $aFilter['where'] . '
            GROUP BY P.id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );

        if ( ! empty($aData)) {
            $this->productsListPrepare($aData);
        } else {
            $aData = array();
        }

        return $aData;
    }

    /**
     * Список товаров пользователя
     * @param int $nUserID ID пользователя
     * @param array $aFilter фильтр списка товаров
     * @param bool $bCount только подсчет кол-ва товаров
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function productsOwnerList($nUserID, array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        if ( ! isset($aFilter['user_id'])) {
            $aFilter['user_id'] = $nUserID;
        }

        if ($nUserID != User::id()) {
            $aFilter['status'] = Shop::STATUS_ACTIVE;
            if (Shop::premoderation()) {
                $aFilter[':moderated'] = 'P.moderated > 0';
            }
        }

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'P');
            return $this->db->one_data('SELECT COUNT(P.id) FROM ' . TABLE_SHOP_PRODUCTS . ' P ' . $aFilter['where'], $aFilter['bind']);
        }

        $aFilter[':cat'] = 'P.cat_id1 = C.id';
        $aFilter[':cat_lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter[':user'] = 'P.user_id = U.user_id';
        $aFilter = $this->prepareFilter($aFilter, 'P');

        if ( ! $sqlOrder) {
            $sqlOrder = 'CU.num, C.numleft, P.created DESC';
        }

        $bRegions = Shop::regionsLimit();

        $aData = $this->db->select('
            SELECT P.id, P.title, P.keyword, P.descr, P.cat_id1, P.cat_id2, P.cat_id, P.created, P.price, P.price_curr, P.price_ex,
                   P.imgcnt, P.img_s, P.status, P.user_id, P.status, P.moderated, P.blocked_reason,
                   ((P.svc & ' . Shop::SVC_MARK . ') > 0) as svc_marked,
                   ((P.svc & ' . Shop::SVC_FIX . ') > 0) as svc_fixed,
                   GROUP_CONCAT(T.tag_id) AS tags, '.($bRegions ? 'MAX(R.reg3_city) AS reg3_city, ' : '').'
                   C.keyword AS cat_keyword1, CL.title AS cat_title1, U.login, U.name, U.surname, U.verified, U.pro
            FROM ' . TABLE_SHOP_PRODUCTS . ' P
                LEFT JOIN '.TABLE_SHOP_PRODUCTS_IN_TAGS.' T ON P.id = T.product_id
                LEFT JOIN '.TABLE_SHOP_CATS_USERS.' CU ON P.user_id = CU.user_id AND P.cat_id1 = CU.cat_id1 '.
                ($bRegions ? ' LEFT JOIN ' . TABLE_SHOP_PRODUCTS_REGIONS . ' R ON P.id = R.product_id ' : '') .
                ', '.TABLE_SHOP_CATS.' C, '.TABLE_SHOP_CATS_LANG.' CL, '.TABLE_USERS.' U
            ' . $aFilter['where'] . '
            GROUP BY P.id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );

        if ( ! empty($aData)) {
            $this->productsListPrepare($aData);
        } else {
            $aData = array();
        }

        $aResult = array();
        foreach ($aData as $v) {
            if ( ! isset($aResult[ $v['cat_id1'] ])) {
                $aResult[ $v['cat_id1'] ] = array();
            }
            $aResult[ $v['cat_id1'] ][] = $v;
        }

        return $aResult;
    }

    /**
     * Подготовка данных списка товаров
     * @param array $aData @ref данные о товарах
     */
    protected function productsListPrepare(array &$aData = array())
    {
        if (empty($aData)) return;

        $aTags = array();
        $aSubCats = array();
        foreach ($aData as &$v) {
            if ($v['tags']) {
                $tags = array_unique(explode(',', $v['tags']));
                if (!empty($tags)) {
                    $v['aTags'] = $tags;
                    foreach ($tags as $t) {
                        if (!in_array($t, $aTags)) {
                            $aTags[] = $t;
                        }
                    }
                }
            }
            if( $v['cat_id2'] && ! in_array($v['cat_id2'], $aSubCats)){
                $aSubCats[] = $v['cat_id2'];
            }
            if ( ! empty($v['reg3_city'])) {
                $v['city_data'] = Geo::regionData($v['reg3_city']);
            }

        } unset($v);

        if (!empty($aTags)) {
            $aTags = $this->db->select_key('SELECT id, tag FROM ' . TABLE_TAGS . ' WHERE ' . $this->db->prepareIN('id', $aTags), 'id');
            foreach ($aData as &$v) {
                if (!empty($v['aTags'])) {
                    foreach ($v['aTags'] as $i => $tag_id) {
                        if (!empty($aTags[$tag_id])) {
                            $v['aTags'][$i] = $aTags[$tag_id];
                        }
                    }
                }
            } unset($v);
        }
        if( ! empty($aSubCats)){
            $aSubCats = $this->db->select_key('
                SELECT C.id, CL.title, C.keyword, C.price, C.price_ex, CL.price_title_mod
                FROM '.TABLE_SHOP_CATS.' C, '.TABLE_SHOP_CATS_LANG.' CL
                WHERE '.$this->db->langAnd(false, 'C', 'CL').' AND '.$this->db->prepareIN('C.id', $aSubCats),
                'id');
            foreach ($aData as &$v) {
                if ( ! empty($aSubCats[ $v['cat_id2'] ])) {
                    $cat = $aSubCats[ $v['cat_id2'] ];
                    $v['cat_keyword2'] = $cat['keyword'];
                    $v['cat_title2'] = $cat['title'];
                    $v['cat_price2'] = $cat['price'];
                    $v['cat_price_ex2'] = $cat['price_ex'];
                    $v['price_title_mod2'] = $cat['price_title_mod'];
                }
            } unset($v);
        }
        foreach ($aData as &$v) {
            $v['cat_title'] = $v['cat_title1'].( ! empty($v['cat_title2']) ? ' / '.$v['cat_title2'] : '');
        } unset($v);
    }

    /**
     * Получение данных товара
     * @param integer $nProductID ID товара
     * @param array $aFields
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function productData($nProductID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT P.*, U.email, U.blocked as user_blocked
                    FROM ' . TABLE_SHOP_PRODUCTS . ' P, ' . TABLE_USERS . ' U
                    WHERE P.user_id = U.user_id AND P.id = :id',
                array(':id' => $nProductID)
            );
            $aData['regions'] = $this->productRegions($nProductID);
        } else {
            if (empty($aFields)) {
                $aFields = array('P.*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM ' . TABLE_SHOP_PRODUCTS . ' P
                    WHERE P.id = :id',
                array(':id' => $nProductID)
            );
        }

        return $aData;
    }

    /**
     * Получение данных товара для просмотра
     * @param integer $nProductID ID товара
     * @return array
     */
    public function productDataView($nProductID)
    {
        $aData = $this->db->one_array('
            SELECT P.*, U.name, U.surname, U.avatar, U.sex, U.login, U.type, U.verified, U.pro, U.created AS user_created, U.opinions_cache, U.contacts,
                US.last_activity
            FROM ' . TABLE_SHOP_PRODUCTS . ' P, ' . TABLE_USERS . ' U, ' . TABLE_USERS_STAT . ' US
            WHERE P.user_id = U.user_id AND P.id = :id',
            array(':id' => $nProductID)
        );
        if(empty($aData)) {
            return array();
        }
        if( ! empty($aData['contacts'])) {
            $aData['contacts'] = func::unserialize($aData['contacts']);
        }
        $aData['cat'] = $this->db->select_key('
            SELECT C.id, C.keyword, CL.title, C.price, C.price_ex, CL.price_title_mod, C.numlevel
            FROM '.TABLE_SHOP_CATS.' C, '.TABLE_SHOP_CATS_LANG.' CL
            WHERE '.$this->db->prepareIN('C.id', array($aData['cat_id1'], $aData['cat_id2'])).
                  $this->db->langAnd(true, 'C', 'CL').'
            ORDER BY C.numleft
            ', 'id');
        $aData['regions'] = $this->productRegions($nProductID);

        return $aData;
    }

    /**
     * Данные для ссылки "следующий товар" (frontend)
     * @param integer $nProductID ID товара
     * @param integer $nUserID ID пользоателя
     * @param string $sCreated дата создания товара
     * @return mixed
     */
    public function productNext($nProductID, $nUserID, $sCreated)
    {
        $aFilter = array(
            'user_id' => $nUserID,
            ':id' => array('P.id != :id', ':id' => $nProductID),
        );

        if ( ! User::isCurrent($nUserID)) {
            $aFilter['status'] = Shop::STATUS_ACTIVE;
            if (Shop::premoderation()) {
                $aFilter[':moderated'] = 'P.moderated > 0';
            }
        }
        $aFilterFirst = $aFilter;

        $aFilter[':created'] = array('P.created > :created', ':created' => $sCreated);
        $aFilter = $this->prepareFilter($aFilter, 'P');

        $aData = $this->db->one_array('SELECT P.id, P.keyword
            FROM ' . TABLE_SHOP_PRODUCTS . ' P
            '.$aFilter['where'].'
            ORDER BY P.created ASC LIMIT 1', $aFilter['bind']
        );

        if (empty($aData)) {
            $aFilter = $this->prepareFilter($aFilterFirst, 'P');
            $aData = $this->db->one_array('SELECT P.id, P.keyword
                FROM ' . TABLE_SHOP_PRODUCTS . ' P
                '.$aFilter['where'].'
                ORDER BY P.created ASC LIMIT 1', $aFilter['bind']
            );
        }

        return $aData;
    }

    /**
     * Данные для ссылки "предыдущий товар" (frontend)
     * @param integer $nProductID ID товара
     * @param integer $nUserID ID пользоателя
     * @param string $sCreated дата создания товара
     * @return mixed
     */
    public function productPrev($nProductID, $nUserID, $sCreated)
    {
        $aFilter = array(
            'user_id' => $nUserID,
            ':id' => array('P.id != :id', ':id' => $nProductID),
        );

        if ( ! User::isCurrent($nUserID)) {
            $aFilter['status'] = Shop::STATUS_ACTIVE;
            if (Shop::premoderation()) {
                $aFilter[':moderated'] = 'P.moderated > 0';
            }
        }
        $aFilterLast = $aFilter;

        $aFilter[':created'] = array('P.created < :created', ':created' => $sCreated);
        $aFilter = $this->prepareFilter($aFilter, 'P');

        $aData = $this->db->one_array('SELECT P.id, P.keyword
            FROM ' . TABLE_SHOP_PRODUCTS . ' P
            '.$aFilter['where'].'
            ORDER BY P.created DESC LIMIT 1', $aFilter['bind']
        );

        if (empty($aData)) {
            $aFilter = $this->prepareFilter($aFilterLast, 'P');
            $aData = $this->db->one_array('SELECT P.id, P.keyword
                FROM ' . TABLE_SHOP_PRODUCTS . ' P
                '.$aFilter['where'].'
                ORDER BY P.created DESC LIMIT 1', $aFilter['bind']
            );
        }

        return $aData;
    }

    /**
     * Получение данных о регионых для товара
     * @param integer $nProductID ID товара
     * @return mixed
     */
    public function productRegions($nProductID)
    {
        if( ! Shop::regionsLimit()) return array();
        return $this->db->select('
            SELECT P.*, R.title_' . LNG . ' as title, C.title_' . LNG . ' as country_title
            FROM ' . TABLE_SHOP_PRODUCTS_REGIONS . ' P,
                 ' . TABLE_REGIONS . ' R,
                 ' . TABLE_REGIONS . ' C
            WHERE P.reg3_city = R.id AND P.product_id = :product_id AND P.reg1_country = C.id',
            array(':product_id' => $nProductID)
        );
    }

    /**
     * Сохранение товара
     * @param integer $nProductID ID товара
     * @param array $aData данные товара
     * @param mixed $sDynpropsDataKey ключ данных дин. свойств, например 'd' или FALSE
     * @return boolean|integer
     */
    public function productSave($nProductID, array $aSave, $sDynpropsDataKey = false)
    {
        if (empty($aSave)) {
            return false;
        }
        $aData = $aSave;
        unset($aData['regions']);

        if (!empty($sDynpropsDataKey) && !empty($aData['cat_id'])) {
            $aDataDP = $this->controller->dpSave($aData['cat_id'], $sDynpropsDataKey);
            $aData = array_merge($aData, $aDataDP);
        }
        if (isset($aData['status']) || isset($aData['status_prev']) || ! $nProductID) {
            $aData['status_changed'] = $this->db->now();
        }
        if ($nProductID > 0) {
            if (isset($aData['cat_id1'])) {
                $aOldData = $this->productData($nProductID, array('cat_id1', 'user_id'));
            }

            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_SHOP_PRODUCTS, $aData, array('id' => $nProductID));

            if (isset($aData['cat_id1'])) {
                if($aData['cat_id1'] != $aOldData['cat_id1']){
                    $this->categoryUserCheck($aOldData['user_id'], $aOldData['cat_id1']);
                    $this->categoryUserCheck($aOldData['user_id'], $aData['cat_id1']);
                }
            }

            $mRet = ! empty($res);
        } else {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $aData['created']; # Дата изменения
            if ( ! isset($aData['svc_order'])) {
                $aData['svc_order'] = $aData['created'];
            }

            $nProductID = $this->db->insert(TABLE_SHOP_PRODUCTS, $aData);
            if ($nProductID > 0) {
                $this->categoryUserCheck($aData['user_id'], $aData['cat_id1']);
            }

            $mRet = $nProductID;
        }

        if ($nProductID) {
            $this->productSaveRegions($nProductID, $aSave);
        }

        return $mRet;
    }

    /**
     * Сохранение регионов товара
     * @param integer $nProductID ID товара
     * @param array $aData ['regions'] массив регионов
     */
    public function productSaveRegions($nProductID, array $aData)
    {
        if (isset($aData['regions'])) {
            $this->db->delete(TABLE_SHOP_PRODUCTS_REGIONS, array('product_id' => $nProductID));
            $aInsert = array();
            foreach ($aData['regions'] as $v) {
                if (count($aInsert) < Shop::regionsLimit()) {
                    # разворачиваем данные о регионе: city_id => reg1_country, reg2_region, reg3_city
                    $aRegions = Geo::model()->regionParents($v);
                    $aRegions['db']['product_id'] = $nProductID;
                    $aInsert[] = $aRegions['db'];
                }
            }
            $this->db->multiInsert(TABLE_SHOP_PRODUCTS_REGIONS, $aInsert);
        }
    }

    /**
     * Переключатели товара
     * @param integer $nProductID ID товара
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function productToggle($nProductID, $sField)
    {
        switch ($sField) {
            case 'enabled': # Включен
            {
                return $this->toggleInt(TABLE_SHOP_PRODUCTS, $nProductID, $sField, 'id');
            }
            break;
        }
    }

    /**
     * Удаление товара
     * @param integer $nProductID ID товара
     * @return boolean
     */
    public function productDelete($nProductID)
    {
        if (empty($nProductID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_SHOP_PRODUCTS, array('id' => $nProductID));
        if (!empty($res)) {
            $this->userCatsProductsCount();
        }

        return $res;
    }

    /**
     * Получаем общее кол-во товаров, ожидающих модерации
     * @return integer
     */
    public function productsModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(P.id)
                FROM ' . TABLE_SHOP_PRODUCTS . ' P
                WHERE P.moderated != 1'
        );
    }

    /**
     * Расчет количества товаров в категориях пользователей
     */
    public function userCatsProductsCount()
    {
        $this->db->exec('
            UPDATE '.TABLE_SHOP_CATS_USERS.' C LEFT JOIN (
                SELECT P.user_id, P.cat_id1, COUNT(P.id) AS cnt
                FROM '.TABLE_SHOP_PRODUCTS.' P
                GROUP BY P.user_id, P.cat_id1
                ) Cp ON C.user_id = Cp.user_id AND C.cat_id1 = Cp.cat_id1
            SET C.product_cnt = Cp.cnt
        ');

        $this->db->exec('
            INSERT INTO '.TABLE_SHOP_CATS_USERS.'(cat_id1, user_id, num, product_cnt)
                SELECT P.cat_id1, P.user_id, 0, COUNT(P.id) AS cnt
                FROM '.TABLE_SHOP_PRODUCTS.' P
                    LEFT JOIN '.TABLE_SHOP_CATS_USERS.' U ON P.user_id = U.user_id AND P.cat_id1 = U.cat_id1
                WHERE U.user_id IS NULL
                GROUP BY P.user_id, P.cat_id1
        ');

        $this->db->exec('
            UPDATE '.TABLE_SHOP_CATS_USERS.' C LEFT JOIN (
                SELECT P.user_id, P.cat_id1, COUNT(P.id) AS cnt
                FROM '.TABLE_SHOP_PRODUCTS.' P
                WHERE P.status = :status '.(Shop::premoderation() ? ' AND P.moderated = 1 ' : '').'
                GROUP BY P.user_id, P.cat_id1
                ) Cp ON C.user_id = Cp.user_id AND C.cat_id1 = Cp.cat_id1
            SET C.active_cnt = Cp.cnt
        ', array(':status' => Shop::STATUS_ACTIVE));

    }

    # ----------------------------------------------------------------
    # Услуги

    /**
     * Данные об услугах (frontend)
     * @param integer $nTypeID ID типа Svc::type...
     * @return array
     */
    public function svcPromoteData($nTypeID)
    {
        if ($nTypeID == Svc::TYPE_SERVICE || empty($nTypeID)) {
            $aData = $this->db->select_key('SELECT id, keyword, price, settings
                            FROM ' . TABLE_SVC . ' WHERE type = :type',
                'keyword', array(':type' => Svc::TYPE_SERVICE)
            );

            if (empty($aData)) return array();

            foreach ($aData as $k => $v) {
                $sett = func::unserialize($v['settings']);
                unset($v['settings']);
                $aData[$k] = array_merge($v, $sett);
            }

            return $aData;

        } elseif ($nTypeID == Svc::TYPE_SERVICEPACK) {
            $aData = $this->db->select('SELECT id, keyword, price, settings
                                FROM ' . TABLE_SVC . ' WHERE type = :type ORDER BY num',
                array(':type' => Svc::TYPE_SERVICEPACK)
            );

            foreach ($aData as $k => $v) {
                $sett = func::unserialize($v['settings']);
                unset($v['settings']);
                # оставляем текущую локализацию
                foreach ($this->langSvcPacks as $lngK => $lngV) {
                    $sett[$lngK] = (isset($sett[$lngK][LNG]) ? $sett[$lngK][LNG] : '');
                }
                $aData[$k] = array_merge($v, $sett);
            }

            return $aData;
        }
    }

    /**
     * Данные об услугах для формы, страницы продвижения
     * @return array
     */
    public function svcData()
    {
        $aFilter = array('module' => 'users');
        $aFilter = $this->prepareFilter($aFilter, 'S');

        $aData = $this->db->select_key('SELECT S.*
                                    FROM ' . TABLE_SVC . ' S
                                    ' . $aFilter['where']
            . ' ORDER BY S.type, S.num',
            'id', $aFilter['bind']
        );
        if (empty($aData)) return array();

        $oIcon = Shop::svcIcon();
        foreach ($aData as $k => &$v) {
            $v['id'] = intval($v['id']);
            $v['disabled'] = false;
            $sett = func::unserialize($v['settings']);
            unset($v['settings']);
            if (!empty($sett)) {
                $v = array_merge($sett, $v);
            }
            $v['title_view'] = (isset($v['title_view'][LNG]) ? $v['title_view'][LNG] : '');
            $v['description'] = (isset($v['description'][LNG]) ? $v['description'][LNG] : '');
            $v['description_full'] = (isset($v['description_full'][LNG]) ? $v['description_full'][LNG] : '');
            $v['icon_s'] = $oIcon->url($v['id'], $v['icon_s'], ShopSvcIcon::SMALL);
            # исключаем выключенные услуги
            if (empty($v['on'])) unset($aData[$k]);
        }
        unset($v);

        return $aData;
    }

    public function svcCron()
    {
        $sNow = $this->db->now();

        # Деактивируем услугу "Выделение"
        $this->db->exec('UPDATE ' . TABLE_SHOP_PRODUCTS . '
            SET svc = svc - :mark
            WHERE ( svc & :mark) > 0 AND svc_marked_to <= :now',
            array(':now' => $sNow, ':mark' => Shop::SVC_MARK)
        );

        # Деактивируем услугу "Закрепление"
        $this->db->exec('UPDATE ' . TABLE_SHOP_PRODUCTS . '
            SET svc = svc - :fix, svc_fixed_order = "0000-00-00 00:00:00"
            WHERE ( svc & :fix) > 0 AND svc_fixed_to <= :now',
            array(':now' => $sNow, ':fix' => Shop::SVC_FIX)
        );

    }

    public function getLocaleTables()
    {
        return array(
            TABLE_SHOP_CATS          => array('type' => 'table', 'fields' => $this->langCategories, 'title' => _t('shop', 'Категории')),
            TABLE_SHOP_CATS_DP       => array(
                'type'   => 'fields',
                'fields' => array(
                    'title'       => TYPE_NOTAGS,
                    'description' => TYPE_NOTAGS
                ),
                'title' => _t('shop', 'Дин. свойства'),
            ),
            TABLE_SHOP_CATS_DPM => array(
                'type' => 'fields',
                'fields' => array('name' => TYPE_NOTAGS),
                'title' => _t('shop', 'Дин. свойства (значения)'),
                'id' => array('dynprop_id', 'value'),
            ),
        );
    }


    /**
     * Статистика
     * @return array
     */
    public function statisticData()
    {
        $data = array();

        # товаров
        $filter = array('status' => Shop::STATUS_ACTIVE);
        if (Shop::premoderation()) {
            $filter[':moderated'] = 'moderated > 0';
        }
        $filter[':user'] = 'P.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';
        $filter = $this->prepareFilter($filter, 'P');
        $data['products'] = (int)$this->db->one_data(' SELECT COUNT(P.id) FROM '.TABLE_SHOP_PRODUCTS.' P, '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);

        return $data;
    }

}