<?php

class ShopSvcIcon_ extends CImageUploader
{
    # варианты иконок
    const SMALL = 's'; # малая

    # ключи размеров
    const szOriginal = 'o'; # оригинальный размер

    function initSettings()
    {
        $this->path = bff::path('svc', 'images');
        $this->pathTmp = bff::path('tmp', 'images');
        $this->url = bff::url('svc', 'images');
        $this->urlTmp = bff::url('tmp', 'images');

        $this->table = TABLE_SVC;
        $this->fieldID = 'id';
        $this->filenameLetters = config::sysAdmin('shop.svc.icon.filename.letters', 4, TYPE_UINT);
        $aVariants = $this->getVariants();
        if (!empty($aVariants)) {
            $this->setVariant(key($aVariants));
        }
    }

    function url($nSvcID, $sFilename, $sVariantKey = self::SMALL)
    {
        $this->setRecordID($nSvcID);
        if (empty($sFilename)) {
            # иконка-заглушка
            return $this->url . 'default-' . $sVariantKey . '.png';
        } else {
            return $this->getURL($sFilename, self::szOriginal);
        }
    }

    function getVariants()
    {
        return bff::filter('shop.svc.icon.sizes', array(
            'icon_' . self::SMALL => array(
                'title' => 'Иконка',
                'key'   => self::SMALL,
                'sizes' => array(
                    self::szOriginal => array('width' => 70, 'height' => 70, 'o' => true),
                ),
            ),
        ));
    }

    function setVariant($sKey)
    {
        $aVariants = $this->getVariants();
        if (isset($aVariants[$sKey])) {
            $this->fieldImage = $sKey;
            $this->sizes = $aVariants[$sKey]['sizes'];
        }
    }
}