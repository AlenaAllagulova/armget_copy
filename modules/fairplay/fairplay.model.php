<?php

define('TABLE_FAIRPLAY_WORKFLOWS',               DB_PREFIX.'fairplay_workflows');            # ход работ
define('TABLE_FAIRPLAY_WORKFLOWS_USERS',         DB_PREFIX.'fairplay_workflows_users');      # ход работ (пользователи)
define('TABLE_FAIRPLAY_WORKFLOWS_STATUSES',      DB_PREFIX.'fairplay_workflows_statuses');   # изменение статусов хода работ
define('TABLE_FAIRPLAY_WORKFLOWS_HISTORY',       DB_PREFIX.'fairplay_workflows_history');    # история хода работ
define('TABLE_FAIRPLAY_WORKFLOWS_CHAT',          DB_PREFIX.'fairplay_workflows_chat');       # общение в ходе работ
define('TABLE_FAIRPLAY_WORKFLOWS_NOTIFICATIONS', DB_PREFIX.'fairplay_workflows_notifications'); # уведомления о новых сообщениях в чате или событиях в истории в рамках хода работ
define('TABLE_FAIRPLAY_WORKFLOWS_BILLS',         DB_PREFIX.'fairplay_workflows_bills');      # счета (резервирование)
define('TABLE_FAIRPLAY_WORKFLOWS_DOCS',          DB_PREFIX.'fairplay_workflows_docs');       # документы созданные в рамках хода работ
define('TABLE_FAIRPLAY_FINANCES_USERS',          DB_PREFIX.'fairplay_finances_users');       # финансовые данные пользователей
define('TABLE_FAIRPLAY_FINANCES_FILES',          DB_PREFIX.'fairplay_finances_files');       # финансовые данные пользователей - загруженные документы
define('TABLE_FAIRPLAY_DOCS',                    DB_PREFIX.'fairplay_docs');                 # документы
define('TABLE_FAIRPLAY_DOCS_LANG',               DB_PREFIX.'fairplay_docs_lang');            # документы мультиязычные данные

class FairplayModel_ extends Model
{
    /** @var FairplayBase */
    var $controller;

    public $financeFields = array('user_id', 'status', 'verified_reason', 'verified_images');
    protected $cryptKey = 'ywHR79?K$Fc+4dCV]9Hys{@TAN&*Jc3*7foL';

    public $langDocs = array(
        'title' => TYPE_STR,
    );

    #---------------------------------------------------------------------------------------
    # Ход работ

    /**
     * Сохранение хода работ
     * @param integer $workflowID ID хода работ
     * @param array $save данные хода работ
     * @return boolean|integer
     */
    public function workflowSave($workflowID, array $save)
    {
        if (empty($save)) {
            return false;
        }
        if (isset($save['extra'])) {
            $save['extra'] = serialize($save['extra']);
        }

        if ($workflowID > 0) {
            $save['modified'] = $this->db->now(); # Дата изменения
            if (isset($save['status'])) {
                $save['status_prev'] = $this->db->one_data('SELECT status FROM '.TABLE_FAIRPLAY_WORKFLOWS.' where id = :id', array(':id' => $workflowID));
            }
            $res = $this->db->update(TABLE_FAIRPLAY_WORKFLOWS, $save, array('id' => $workflowID));

            $ret = !empty($res);
        } else {
            $save['created'] = $this->db->now(); # Дата создания
            $save['modified'] = $this->db->now(); # Дата изменения

            $workflowID = $this->db->insert(TABLE_FAIRPLAY_WORKFLOWS, $save);
            $this->workflowUsersAdd($workflowID, $save['client_id'], array('owner' => 1));
            $this->workflowUsersAdd($workflowID, $save['worker_id'], array('owner' => 1));
            $ret = $workflowID;
        }

        if ($workflowID) {
            if (isset($save['status']) || isset($save['arbitrage']) || isset($save['fairplay'])) {
                $data = $this->workflowData($workflowID, array('status', 'arbitrage', 'fairplay'));
                $status = isset($save['status']) ? $save['status'] : $data['status'];
                $arbitrage = isset($save['arbitrage']) ? $save['arbitrage'] : $data['arbitrage'];
                $fairplay = isset($save['fairplay']) ? $save['fairplay'] : $data['fairplay'];
                $this->db->insert(TABLE_FAIRPLAY_WORKFLOWS_STATUSES, array(
                    'workflow_id' => $workflowID,
                    'user_id'     => User::id(),
                    'user_ip'     => Request::remoteAddress(),
                    'status'      => $status,
                    'arbitrage'   => $arbitrage,
                    'fairplay'    => $fairplay,
                    'created'     => $this->db->now(),
                    'extra'       => isset($save['extra']) ? $save['extra'] : '',
                ));
            }
        }

        return $ret;
    }

    /**
     * Добавление пользователя в ход работ
     * @param integer $workflowID ID хода работ
     * @param integer $userID ID пользователя
     * @param array $extra
     */
    public function workflowUsersAdd($workflowID, $userID, $extra = array())
    {
        if ( ! $workflowID || ! $userID) return;

        $extra += array(
            'workflow_id' => $workflowID,
            'user_id'     => $userID,
        );
        $this->db->insert(TABLE_FAIRPLAY_WORKFLOWS_USERS, $extra);
    }

    /**
     * Удаление пользователя из хода работ
     * @param integer $workflowID ID хода работ
     * @param integer $userID ID пользователя
     */
    public function workflowUsersDelete($workflowID, $userID)
    {
        if ( ! $workflowID || ! $userID) return;

        $this->db->delete(TABLE_FAIRPLAY_WORKFLOWS_USERS, array(
            'workflow_id' => $workflowID,
            'user_id'     => $userID,
        ));

        $this->resetCounters($workflowID, $userID);
    }

    /**
     * Получение данных о ходе работ
     * @param array|integer $filter ID хода работ или фильтр
     * @param array $fields
     * @return array
     */
    public function workflowData($filter, array $fields = array())
    {
        if ( ! is_array($filter)) {
            $filter = array('id' => $filter);
        }
        $filter = $this->prepareFilter($filter, 'W');

        if (empty($fields)) {
            $fields = array('W.*');
        }
        $data = $this->db->one_array('
            SELECT ' . join(',', $fields) . '
            FROM ' . TABLE_FAIRPLAY_WORKFLOWS . ' W '.
            $filter['where'].' 
            LIMIT 1', $filter['bind']
        );
        $opinions = array();
        if ( ! empty($data['client_opinion'])) {
            $opinions[] = $data['client_opinion'];
        }
        if ( ! empty($data['worker_opinion'])) {
            $opinions[] = $data['worker_opinion'];
        }
        if ( ! empty($opinions)) {
            $opinions = Opinions::model()->opinionsDataByFilter(array('id', 'type'), array('id' => array_unique($opinions)), false);
            $opinions = func::array_transparent($opinions, 'id', true);
            if ( ! empty($data['client_opinion']) && isset($opinions[ $data['client_opinion'] ])) {
                $data['client_opinion_type'] = $opinions[ $data['client_opinion'] ]['type'];
            }
            if ( ! empty($data['worker_opinion']) && isset($opinions[ $data['worker_opinion'] ])) {
                $data['worker_opinion_type'] = $opinions[ $data['worker_opinion'] ]['type'];
            }
        }
        if (isset($data['extra'])) {
            $data['extra'] = func::unserialize($data['extra']);
        }
        return $data;
    }

    /**
     * Массовое сохранение переписки к ходу работ
     * @param integer $workflowID ID хода работ
     * @param array $save данные
     * @return bool|int
     */
    public function workflowChatMultiSave($workflowID, array & $save)
    {
        if ( ! $workflowID || empty($save)) return;

        foreach ($save as & $v) {
            $v['workflow_id'] = $workflowID;
        } unset($v);

        $this->db->multiInsert(TABLE_FAIRPLAY_WORKFLOWS_CHAT, $save);
    }

    /**
     * Список ходов работ пользователя.
     * @param array $filter фильтр списка
     * @param bool $count только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function workflowList(array $filter = array(), $count = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        Orders::i();
        $group = false;
        $from = ' FROM '.TABLE_FAIRPLAY_WORKFLOWS . ' W ';
        $select = array();
        if (isset($filter['user'])) {
            $select[] = 'U.cnt_new';
            $from .= ', '. TABLE_FAIRPLAY_WORKFLOWS_USERS . ' U ';
            $filter[':ju'] = 'W.id = U.workflow_id';
            if (is_array($filter['user'])) {
                $filter[':u'] = $this->db->prepareIN('U.user_id', $filter['user']);
            } else {
                $filter[':u'] = array('U.user_id = :user', ':user' => $filter['user']);
            }
            unset($filter['user']);
        }

        if ($count) {
            $filter = $this->prepareFilter($filter, 'W');
            return $this->db->one_data('SELECT COUNT(W.id) '.$from.$filter['where'].( $group ? ' GROUP BY W.id ' : ''), $filter['bind']);
        }

        $from .= ', '. TABLE_ORDERS . ' O ';
        $filter[':jo'] = 'W.order_id = O.id';
        $filter = $this->prepareFilter($filter, 'W');

        $data = $this->db->select('
            SELECT W.id, W.status, W.client_id, W.worker_id, W.fairplay, W.client_opinion, W.worker_opinion, 
                   W.price, W.term, W.arbitrage, W.extra,
                   O.keyword, O.title, O.visibility '.
                   ( ! empty($select) ? ', '.join(', ', $select): '').
            $from.
            $filter['where'].
            ( $group ? ' GROUP BY W.id ' : '').
            (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $filter['bind']
        );
        $users = array();
        $opinions = array();
        foreach ($data as & $v) {
            $users[] = $v['client_id'];
            $users[] = $v['worker_id'];
            if ($v['client_opinion']) {
                $opinions[] = $v['client_opinion'];
            }
            if ($v['worker_opinion']) {
                $opinions[] = $v['worker_opinion'];
            }
            $v['extra'] = func::unserialize($v['extra']);
        } unset($v);
        if ( ! empty($users)) {
            $users = Users::model()->usersList(array('user_id' => array_unique($users)),
                array('user_id', 'name','surname','login','pro','verified'));
            foreach ($data as & $v) {
                if (isset($users[ $v['client_id'] ])) {
                    $v['client'] = $users[ $v['client_id'] ];
                }
                if (isset($users[ $v['worker_id'] ])) {
                    $v['worker'] = $users[$v['worker_id']];
                }
            } unset($v);
        }
        # Данные об оставленных отзывах
        if ( ! empty($opinions)) {
            $opinions = Opinions::model()->opinionsDataByFilter(array('id', 'type'), array('id' => array_unique($opinions)), false);
            $opinions = func::array_transparent($opinions, 'id', true);
            foreach ($data as & $v) {
                if ($v['client_opinion'] && isset($opinions[ $v['client_opinion'] ])) {
                    $v['client_opinion_type'] = $opinions[ $v['client_opinion'] ]['type'];
                }
                if ($v['worker_opinion'] && isset($opinions[ $v['worker_opinion'] ])) {
                    $v['worker_opinion_type'] = $opinions[ $v['worker_opinion'] ]['type'];
                }
            } unset($v);
        }
        return $data;
    }

    /**
     * Счетчики для списка ходов работ пользователя
     * @param array $filter фильтр списка
     * @return array
     */
    public function workflowListCounts(array $filter = array())
    {
        return array(0=>0);
    }

    /**
     * Список ходов работ (admin)
     * @param array $filter фильтр списка заказов
     * @param bool $count только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function workflowListing(array $filter, $count = false, $sqlLimit = '', $sqlOrder = '') //admin
    {
        Orders::i();
        $group = array();
        $from = TABLE_FAIRPLAY_WORKFLOWS . ' W
            LEFT JOIN '. TABLE_FAIRPLAY_WORKFLOWS_USERS . ' WUA ON W.id = WUA.workflow_id AND WUA.user_id = 1 
            , '.TABLE_ORDERS.' O ';
        $filter[':jo'] = 'W.order_id = O.id';

        if (array_key_exists(':juser', $filter)) {
            $from .= ', '. TABLE_FAIRPLAY_WORKFLOWS_USERS . ' WU, '.TABLE_USERS.' U ';
            $filter[':ju'] = 'W.id = WU.workflow_id AND WU.user_id = U.user_id';
            $group[] = 'W.id';
        }

        $filter = $this->prepareFilter($filter, 'W');
        $group = (!empty($group) ? join(',', array_unique($group)) : '');

        if ($count) {
            return $this->db->one_data('SELECT COUNT(W.id) FROM ' . $from . $filter['where'] . (!empty($group) ? ' GROUP BY ' . $group : ''), $filter['bind']);
        }

        return $this->db->select('
            SELECT W.id, W.status, W.order_id, W.client_id, W.worker_id, W.fairplay, W.created, W.price, W.term,
                   WUA.cnt_new,
                   O.keyword, O.title
               FROM ' . $from . $filter['where']
            . (!empty($group) ? ' GROUP BY ' . $group : '')
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $filter['bind']
        );
    }

    /**
     * Данные о ходах работ по фильтру
     * @param array $filter
     * @param bool|array $fields false - только подсчитать количество
     * @param bool $oneArray
     * @return mixed
     */
    public function workflowsDataByFilter(array $filter, $fields = false,  $oneArray = true)
    {
        $filter = $this->prepareFilter($filter, 'W');
        if (empty($fields)) {
            return (int)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_FAIRPLAY_WORKFLOWS . ' W ' . $filter['where'], $filter['bind']);
        } else {
            if ($oneArray) {
                return $this->db->one_array('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS . ' W ' . $filter['where'].' LIMIT 1', $filter['bind']);
            } else {
                return $this->db->select('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS . ' W ' . $filter['where'], $filter['bind']);
            }
        }
    }

    /**
     * Данные о пользователях в рамках ходах работ по фильтру
     * @param array $filter
     * @param bool|array $fields false - только подсчитать количество
     * @param bool $oneArray
     * @return mixed
     */
    public function workflowUsersByFilter(array $filter, $fields = false,  $oneArray = true)
    {
        $filter = $this->prepareFilter($filter, 'U');
        if (empty($fields)) {
            return (int)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_FAIRPLAY_WORKFLOWS_USERS . ' U ' . $filter['where'], $filter['bind']);
        } else {
            if ($oneArray) {
                return $this->db->one_array('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS_USERS . ' U ' . $filter['where'].' LIMIT 1', $filter['bind']);
            } else {
                return $this->db->select('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS_USERS . ' U ' . $filter['where'], $filter['bind']);
            }
        }
    }

    /**
     * Данные об изменениях статусов в пределах хода работ по фильтру
     * @param array $filter
     * @param bool|array $fields false - только подсчитать количество
     * @param bool $oneArray
     * @param string $sqlOrder
     * @return mixed
     */
    public function workflowsStatusesByFilter(array $filter, $fields = false,  $oneArray = true, $sqlOrder = 'S.created')
    {
        $filter = $this->prepareFilter($filter, 'S');
        if (empty($fields)) {
            return (int)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_FAIRPLAY_WORKFLOWS_STATUSES . ' S ' . $filter['where'], $filter['bind']);
        } else {
            if ($oneArray) {
                return $this->db->one_array('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS_STATUSES . ' S ' . $filter['where'].' LIMIT 1', $filter['bind']);
            } else {
                return $this->db->select('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS_STATUSES . ' S ' . $filter['where'].( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : ''), $filter['bind']);
            }
        }
    }

    /**
     * Количество ходов работ ожидающих арбитража
     * @return integer
     */
    public function arbitrageCounter()
    {
        return (int)$this->db->one_data('
            SELECT COUNT(id) FROM '.TABLE_FAIRPLAY_WORKFLOWS.'
            WHERE arbitrage = 1 ');
    }

    /**
     * Количество ходов работ ожидающих выплату
     * @return integer
     */
    public function paymentCounter()
    {
        return (int)$this->db->one_data('
            SELECT COUNT(id) FROM '.TABLE_FAIRPLAY_WORKFLOWS.'
            WHERE status = :status ', array(':status' => Fairplay::STATUS_PAYMENT));
    }

    #---------------------------------------------------------------------------------------
    # Фин. данные

    /**
     * Сохранение финансов пользователя
     * @param integer $userID ID пользователя
     * @param array $save данные
     * @return boolean|integer
     */
    public function financeSave($userID, array $save)
    {
        if ( ! $userID || empty($save)) {
            return false;
        }
        $extra = array();
        foreach ($save as $k => $v) {
            if ( ! in_array($k, $this->financeFields)) {
                $extra[$k] = $v;
                unset($save[$k]);
            }
        }
        if ( ! empty($extra)) {
            $save['extra'] = serialize($extra);
        }
        $prevKey = $this->db->crypt(true, $this->cryptKey);
        $exist = (int)$this->db->one_data('SELECT user_id FROM '.TABLE_FAIRPLAY_FINANCES_USERS.' WHERE user_id = :user', array(':user' => $userID));
        if ($exist) {
            $ret = $this->db->update(TABLE_FAIRPLAY_FINANCES_USERS, $save, array('user_id' => $userID), array(), array('extra'));
        } else {
            $save['user_id'] = $userID;
            $ret = $this->db->insert(TABLE_FAIRPLAY_FINANCES_USERS, $save, 'id', array(), array('extra'));
        }
        $this->db->crypt(true, $prevKey);
        return $ret;
    }

    /**
     * Получение финансовых данных пользователя
     * @param integer $userID ID пользователя
     * @return array
     */
    public function financeData($userID)
    {
        if ( ! $userID) return false;
        
        $prevKey = $this->db->crypt(true, $this->cryptKey);
        $data = $this->db->one_array('
                SELECT '.join(', ', $this->financeFields).', BFF_DECRYPT(extra) AS extra
                FROM ' . TABLE_FAIRPLAY_FINANCES_USERS . ' F
                WHERE F.user_id = :user',
            array(':user' => $userID)
        );
        $this->db->crypt(true, $prevKey);
        
        if (empty($data)) {
            $data = array(
                'user_id' => $userID,
                'status'  => Fairplay::VERIFIED_STATUS_NONE,
            );
        } else {
            $extra = func::unserialize($data['extra']);
            $data += $extra;
        }
        return $data;
    }

    /**
     * Список фин. данных пользователей (admin)
     * @param array $filter фильтр списка
     * @param bool $count только подсчет кол-ва заказов
     * @param array $fields
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function financesListing(array $filter, $count = false, $fields = array(), $sqlLimit = '', $sqlOrder = '') //admin
    {
        $crypt = Users::model()->cryptUsers;
        if ( ! empty($crypt)) {
            foreach ($filter as $k => $v) {
                if (in_array($k, $crypt)) {
                    unset($filter[$k]);
                    $aFilter[':' . $k] = array('BFF_DECRYPT(' . $k . ') = :' . $k, ':' . $k => $v);
                }
            }
        }

        $from = TABLE_USERS.' U LEFT JOIN '.TABLE_FAIRPLAY_FINANCES_USERS . ' F ON U.user_id = F.user_id';

        $filter = $this->prepareFilter($filter);

        if ($count) {
            return $this->db->one_data('SELECT COUNT(U.user_id) FROM ' . $from . $filter['where'], $filter['bind']);
        }

        if (empty($fields)) {
            $fields[] = 'F.*';
        } else {
            if ( ! empty($crypt)) {
                foreach ($fields as $k => $v) {
                    if (in_array($k, $crypt)) {
                        $fields[$k] = 'BFF_DECRYPT(' . $v . ') as ' . $v;
                    }
                }
            }
        }

        return $this->db->select('
            SELECT U.user_id, '.join(',', $fields).'
               FROM ' . $from . $filter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $filter['bind']
        );
    }

    /**
     * Статус проверки финансовых данных пользователя
     * @param integer $userID
     * @return int
     */
    public function financeVerified($userID)
    {
        if ( ! $userID) return Fairplay::VERIFIED_STATUS_NONE;

        static $cache;
        if ( ! isset($cache[$userID])) {
            $status = $this->db->one_data('SELECT status FROM '.TABLE_FAIRPLAY_FINANCES_USERS.' WHERE user_id = :user', array(':user' => $userID));
            if ( ! empty($status)) {
                $cache[$userID] = $status;
            } else {
                $cache[$userID] = Fairplay::VERIFIED_STATUS_NONE;
            }
        }
        return $cache[$userID];
    }

    /**
     * Удаление финансовых данных пользователя
     * @param $userID
     */
    public function financeDelete($userID)
    {
        if ( ! $userID) return;

        $this->db->delete(TABLE_FAIRPLAY_FINANCES_USERS, array('user_id' => $userID));
    }

    /**
     * Количество пользователей ожидающих проверки документов
     * @return integer
     */
    public function verifiedWaitingCounter()
    {
        return (int)$this->db->one_data('
            SELECT COUNT(user_id) FROM '.TABLE_FAIRPLAY_FINANCES_USERS.'
            WHERE status = :wait ', array(':wait' => Fairplay::VERIFIED_STATUS_WAIT));
    }

    #---------------------------------------------------------------------------------------
    # Движение средств

    /**
     * Сохранение счета
     * @param integer $billID ID счета
     * @param array $save данные счета
     * @return boolean|integer
     */
    public function billSave($billID, array $save)
    {
        if (empty($save)) {
            return false;
        }

        if (isset($save['extra']) && is_array($save['extra'])) {
            $save['extra'] = serialize($save['extra']);
        }

        if ($billID > 0) {
            $res = $this->db->update(TABLE_FAIRPLAY_WORKFLOWS_BILLS, $save, array('id' => $billID));

            $return = !empty($res);
        } else {
            $save['created'] = $this->db->now(); # Дата создания

            $billID = $this->db->insert(TABLE_FAIRPLAY_WORKFLOWS_BILLS, $save);
            $return = $billID;
        }

        return $return;
    }

    /**
     * Список счетов
     * @param array $filter фильтр списка
     * @param bool $count только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function billsListing(array $filter, $count = false, $sqlLimit = '', $sqlOrder = '') //admin
    {
        Orders::i();
        $from = TABLE_FAIRPLAY_WORKFLOWS_BILLS . ' B, 
            ' . TABLE_USERS . ' U,
            ' . TABLE_FAIRPLAY_WORKFLOWS.' W, 
            ' . TABLE_ORDERS . ' O';
        $filter[':ju'] = 'B.user_id = U.user_id';
        $filter[':jw'] = 'B.workflow_id = W.id';
        $filter[':jo'] = 'W.order_id = O.id';

        $aGroupBy = array();

        $filter = $this->prepareFilter($filter, 'B');
        $aGroupBy = (!empty($aGroupBy) ? join(',', array_unique($aGroupBy)) : '');

        if ($count) {
            return $this->db->one_data('SELECT COUNT(B.id) FROM ' . $from . $filter['where'] . (!empty($aGroupBy) ? ' GROUP BY ' . $aGroupBy : ''), $filter['bind']);
        }

        $data = $this->db->select('
            SELECT B.id, B.user_id, B.sum, B.commission, B.created, B.workflow_id, B.type, B.descr, B.extra, B.created_user_id, B.bill_id,
                   U.email, W.order_id, O.title
               FROM ' . $from . $filter['where']
            . (!empty($aGroupBy) ? ' GROUP BY ' . $aGroupBy : '')
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $filter['bind']
        );
        return $data;
    }

    /**
     * Данные о счетах по фильтру
     * @param array $filter
     * @param bool|array $fields false - только подсчитать количество
     * @param bool $oneArray
     * @return mixed
     */
    public function billsDataByFilter(array $filter, $fields = false,  $oneArray = true)
    {
        $filter = $this->prepareFilter($filter, 'B');
        if (empty($fields)) {
            return (int)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_FAIRPLAY_WORKFLOWS_BILLS . ' B ' . $filter['where'], $filter['bind']);
        } else {
            if ($oneArray) {
                return $this->db->one_array('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS_BILLS . ' B ' . $filter['where'].' LIMIT 1', $filter['bind']);
            } else {
                return $this->db->select('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_FAIRPLAY_WORKFLOWS_BILLS . ' B ' . $filter['where'], $filter['bind']);
            }
        }
    }

    #---------------------------------------------------------------------------------------
    # Уведомления о событиях

    /**
     * Расчет счетчиков новых событий для пользователей
     * @param array|integer $users пользователи
     */
    public function calcNewCounters($users)
    {
        if (empty($users)) return;
        if ( ! is_array($users)) {
            $users = array($users);
        }

        # список новых сообщениях в чате или событиях в истории в рамках хода работ для пользователей
        $notifications = $this->db->select('
            SELECT item_id, user_id, workflow_id
            FROM '.TABLE_FAIRPLAY_WORKFLOWS_NOTIFICATIONS.'
            WHERE '.$this->db->prepareIN('user_id', $users));
        # данные о ходах работ для пользователей
        $workflows = $this->workflowUsersByFilter(array('user_id' => $users), array('user_id', 'workflow_id', 'cnt_new'), false);
        $data = array();
        $usersCnt = array();
        # найдем сумму новых событий
        foreach ($notifications as $v) {
            if ( ! isset($data[ $v['user_id'] ][ $v['workflow_id'] ])) {
                $data[ $v['user_id'] ][ $v['workflow_id'] ] = 0;
            }
            $data[ $v['user_id'] ][ $v['workflow_id'] ]++; # для каждого хода работ

            if ( ! isset($usersCnt[ $v['user_id'] ])) {
                $usersCnt[ $v['user_id'] ] = 0;
            }
            $usersCnt[ $v['user_id'] ]++; # для каждого пользователя
        }
        $update = array();
        foreach ($workflows as $v) {
            if (empty($data[ $v['user_id'] ][ $v['workflow_id'] ])) {
                if ($v['cnt_new']) {
                    $update[] = 'WHEN workflow_id = ' . $v['workflow_id'] . ' AND user_id = ' . $v['user_id'] . ' THEN 0 ';
                }
            } else {
                if ($v['cnt_new'] != $data[ $v['user_id'] ][ $v['workflow_id'] ]) {
                    $update[] = 'WHEN workflow_id = ' . $v['workflow_id'] . ' AND user_id = ' . $v['user_id'] . ' THEN '.$data[ $v['user_id'] ][ $v['workflow_id'] ];
                }
            }
        }
        if ( ! empty($update)) { # обновим счетчик для каждого пользователя в пределах каждого хода работ
            $this->db->exec('
                UPDATE '.TABLE_FAIRPLAY_WORKFLOWS_USERS.' 
                SET cnt_new = CASE '.join(' ', $update).' ELSE cnt_new END
                WHERE '.$this->db->prepareIN('user_id', $users));
        }
        $update = array();
        foreach ($users as $u) {
            $update[] = 'WHEN user_id = ' . $u . ' THEN '.( ! empty($usersCnt[ $u ]) ? $usersCnt[ $u ] : '0');
        }
        if ( ! empty($update)) { # обновим счетчик для каждого пользователя
            $this->db->exec('
                UPDATE '.TABLE_USERS_STAT.' 
                SET fairplay_workflows = CASE '.join(' ', $update).' ELSE fairplay_workflows END
                WHERE '.$this->db->prepareIN('user_id', $users));
        }
    }

    /**
     * Сброс счетчиков новых событий для пользователя в пределах хода работ
     * @param integer $workflowID ID хода работ
     * @param integer $userID ID пользователя
     */
    public function resetCounters($workflowID, $userID)
    {
        if (empty($userID)) return;
        if (empty($workflowID)) return;

        $this->db->delete(TABLE_FAIRPLAY_WORKFLOWS_NOTIFICATIONS, array(
            'workflow_id' => $workflowID,
            'user_id'     => $userID,
        ));
        $this->calcNewCounters($userID);
    }

    /**
     * Формирование заданий на уведомление о новых событиях
     * @param integer $workflowID ID хода работ
     * @param integer $authorID ID автора события
     * @param integer $itemID ID события
     * @param integer $type тип события
     * @param bool $notified отправть почтовое уведомление о событии
     */
    public function addNotification($workflowID, $authorID, $itemID, $type, $notified = false)
    {
        if (empty($workflowID)) return;
        if (empty($authorID)) return;
        if (empty($itemID)) return;
        if (empty($type)) return;

        $data = $this->workflowUsersByFilter(array('workflow_id' => $workflowID), array('user_id'), false);
        $insert = array();
        $users = array();
        foreach ($data as $v) {
            if ($v['user_id'] == $authorID) continue;
            $insert[] = array(
                'workflow_id' => $workflowID,
                'item_id'     => $itemID,
                'type'        => $type,
                'user_id'     => $v['user_id'],
                'is_notified' => $notified ? 0 : 1,
            );
            $users[] = $v['user_id'];
        }
        if (empty($insert)) return;
        $this->db->multiInsert(TABLE_FAIRPLAY_WORKFLOWS_NOTIFICATIONS, $insert);
        $this->calcNewCounters($users);
    }

    /**
     * Удаление отметок о новых событиях
     * @param integer $workflowID ID хода работ
     * @param integer $userID ID пользователя
     * @param integer|array $items ID события или массив ID событий
     * @param integer $type тип события
     */
    public function dropNotification($workflowID, $userID, $items, $type)
    {
        if (empty($workflowID)) return;
        if (empty($userID)) return;
        if (empty($items)) return;
        if (empty($type)) return;

        $this->db->delete(TABLE_FAIRPLAY_WORKFLOWS_NOTIFICATIONS, array(
            'user_id'     => $userID,
            'workflow_id' => $workflowID,
            'item_id'     => $items,
            'type'        => $type,
        ));
    }

    #---------------------------------------------------------------------------------------
    # История событий

    /**
     * Добавление события в историю хода работ
     * @param integer $workflowID ID хода работ
     * @param array $data данные
     * @return integer
     */
    public function historyAdd($workflowID, array $data)
    {
        if (empty($workflowID)) return false;
        if (empty($data['type'])) return false;
        if (empty($data['user_id'])) {
            $data['user_id'] = User::id();
        };
        if (isset($data['extra'])) {
            $data['extra'] = serialize($data['extra']);
        }
        $data['workflow_id'] = $workflowID;
        $data['created'] = $this->db->now(); # Дата создания
        $historyID = $this->db->insert(TABLE_FAIRPLAY_WORKFLOWS_HISTORY, $data);
        if ($historyID) {
            $this->addNotification($workflowID, $data['user_id'], $historyID, Fairplay::NOTIFICATIONS_TYPE_HISTORY);
        }
        return $historyID;
    }

    /**
     * Список событий истории
     * @param array|integer $filter ID хода работ или фильтр
     * @param array $fields
     * @param string $sqlOrder
     * @return array
     */
    public function historyList($filter, $fields = array(), $sqlOrder = 'H.created DESC')
    {
        if ( ! is_array($filter)) {
            $filter = array('workflow_id' => $filter);
        }
        $filter = $this->prepareFilter($filter, 'H');
        if (empty($fields)) {
            $fields = array('H.*');
        }
        $data = $this->db->select('
            SELECT ' . join(',', $fields) . ' 
            FROM ' . TABLE_FAIRPLAY_WORKFLOWS_HISTORY . ' H 
            ' . $filter['where'] .'
            ' .(!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : ''), $filter['bind']);

        foreach ($data as & $v) {
            if (isset($v['extra'])) {
                $v['extra'] = func::unserialize($v['extra']);
            }
        } unset($v);
        return $data;
    }

    #---------------------------------------------------------------------------------------
    # Документы

    /**
     * Список документов (admin)
     * @param array $filter фильтр
     * @param array $fields столбцы
     * @param string $order
     * @return mixed
     */
    public function docsListing($filter = array(), $fields = array(), $order = 'D.name')
    {
        $filter[':l'] = $this->db->langAnd(false, 'D', 'DL');
        $filter = $this->prepareFilter($filter, 'D');

        foreach ($fields as & $v) {
            if (array_key_exists($v, $this->langDocs)) {
                $v = 'DL.'.$v;
            } else {
                $v = 'D.'.$v;
            }
        } unset($v);

        return $this->db->select('
            SELECT '.join(',', $fields).'
            FROM '.TABLE_FAIRPLAY_DOCS.' D, '.TABLE_FAIRPLAY_DOCS_LANG.' DL 
            ' . $filter['where'] .'
            ' .(!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : ''), $filter['bind']);
    }

    /**
     * Сохранение документа
     * @param integer $documentID ID документа или 0 (новый)
     * @param array $data данные о документе
     * @return bool|int
     */
    public function docSave($documentID, array $data)
    {
        if (empty($data)) {
            return false;
        }
        $save = array_diff_key($data, $this->langDocs);

        if ($documentID > 0) {
            $save['modified'] = $this->db->now(); # Дата изменения
            $res = $this->db->update(TABLE_FAIRPLAY_DOCS, $save, array('id' => $documentID));
            $this->db->langUpdate($documentID, $data, $this->langDocs, TABLE_FAIRPLAY_DOCS_LANG);
            return !empty($res);
        } else {
            $save['created'] = $this->db->now(); # Дата создания
            $save['modified'] = $this->db->now(); # Дата изменения
            $documentID = $this->db->insert(TABLE_FAIRPLAY_DOCS, $save);
            if ($documentID > 0) {
                $this->db->langInsert($documentID, $data, $this->langDocs, TABLE_FAIRPLAY_DOCS_LANG);
            }
            return $documentID;
        }
    }

    /**
     * Получение данных о документе
     * @param integer $documentID ID документа
     * @param array $fields столбцы
     * @param boolean $edit при редактировании
     * @return array
     */
    public function docData($documentID, $fields = array(), $edit = false)
    {
        if ($edit) {
            $data = $this->db->one_array('SELECT D.*
                    FROM ' . TABLE_FAIRPLAY_DOCS . ' D
                    WHERE D.id = :id',
                array(':id' => $documentID)
            );
            if ( ! empty($data)) {
                $this->db->langSelect($documentID, $data, $this->langDocs, TABLE_FAIRPLAY_DOCS_LANG);
            }
        } else {
            if (empty($fields)) {
                $fields = array('D.*','DL.*');
            } else {
                foreach ($fields as & $v) {
                    if (array_key_exists($v, $this->langDocs)) {
                        $v = 'DL.'.$v;
                    } else {
                        $v = 'D.'.$v;
                    }
                }
                unset($v);
            }
            return $this->db->one_array('
                SELECT '.join(',', $fields).' 
                FROM '.TABLE_FAIRPLAY_DOCS.' D, '.TABLE_FAIRPLAY_DOCS_LANG.' DL 
                WHERE '.$this->db->langAnd(false, 'D', 'DL').' AND D.id = :id', array(':id' => $documentID));
        }
        return $data;
    }

    /**
     * Удаление документа
     * @param integer $documentID ID документа
     * @return boolean
     */
    public function docDelete($documentID)
    {
        if (empty($documentID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_FAIRPLAY_DOCS, array('id' => $documentID));
        if ( ! empty($res)) {
            $this->db->delete(TABLE_FAIRPLAY_DOCS_LANG, array('id' => $documentID));
            return true;
        }
        return false;
    }

    public function getLocaleTables()
    {
        return array(
            TABLE_FAIRPLAY_DOCS => array('type' => 'table', 'fields' => $this->langDocs, 'title' => _t('fp', 'Категории')),
        );
    }

}