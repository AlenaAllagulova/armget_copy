<?php

class Fairplay_ extends FairplayBase
{
    /**
     * Кабинет: Ход работ
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function my_workflows(array $userData)
    {
        $userID = User::id();
        $filter = array('user' => $userID);

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            'st'   => TYPE_UINT, # Статус
        ));

        $filterTypes = array(
            0 => array('id' => 0, 't' => _t('orders', 'Все заказы')),
        );

        switch ($f['st']) {
            case 0: # все
                break;
        }

        $counts = $this->model->workflowListCounts($filter);
        foreach ($counts as $k => $v) {
            if ( ! isset($filterTypes[$k])) continue;
            $filterTypes[$k]['c'] = $v;
        }

        $data = array('types' => $filterTypes, 'f'=>&$f, 'user'=>&$userData, 'pgn'=>'');
        $count = $this->model->workflowList($filter, true);
        if ($count) {
            $pgn = new Pagination($count, config::sysAdmin('fairplay.workflow.pagesize', 5, TYPE_UINT), array(
                'link'  => static::url('my.workflow', array('login'=>$userData['login'])),
                'query' => $f,
            ));
            $data['list'] = $this->model->workflowList($filter, false, $pgn->getLimitOffset(), 'O.created DESC');
            foreach ($data['list'] as &$v) {
                $v['url_view'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword']));
                $v = Catcher::getOrderPrepay($v);
            } unset($v);
            $data['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }

        $data['list'] = $this->viewPHP($data, 'workflows.list.ajax');

        if (Request::isAJAX()) {
            $count = array();
            foreach ($filterTypes as $k => $v) {
                $count[$k] = $v['c'];
            }
            $this->ajaxResponseForm(array(
                'pgn'    => $data['pgn'],
                'list'   => $data['list'],
                'counts' => $count,
            ));
        }

        # SEO:
        $this->seo()->robotsIndex(false);
        $this->urlCorrection(static::url('my.workflow', array('login'=>$userData['login'])));
        $this->seo()->setPageMeta($this->users(), 'fairplay-workflows', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'country' => $userData['country'],
            'page'    => $f['page'],
        ), $data);
        
        return $this->viewPHP($data, 'workflows.list');
    }

    /**
     * Просмотр хода работ
     * @return string HTML
     */
    public function view()
    {
        $workflowID = $this->input->get('id', TYPE_UINT);
        if (!$workflowID) {
            $this->errors->error404();
        }
        $userID = User::id();
        if (!$userID) {
            $this->errors->error404();
        }
        $data = $this->model->workflowData($workflowID);
        if (empty($data)) {
            $this->errors->error404();
        }
        if ( ! in_array($userID, array($data['client_id'], $data['worker_id'],$data['client_expert_id'], $data['worker_expert_id']))) {
            $this->errors->error404();
        }

        $data['history'] = $this->model->historyList($workflowID, array('id', 'type', 'extra', 'created'));
        $hist = array();
        foreach ($data['history'] as & $v) {
            $hist[] = $v['id'];
            $v['descr'] = static::historyDescription($v['type'], $v['extra']);
        } unset($v);
        $this->model->dropNotification($workflowID, $userID, $hist, static::NOTIFICATIONS_TYPE_HISTORY);

        $data['order'] = Orders::model()->orderDataView($data['order_id']);
        Orders::i()->viewDataPrepare($data['order']);

        $data['worker'] = Users::model()->userData($data['worker_id'], array('user_id', 'login', 'name', 'surname', 'pro', 'sex', 'verified', 'avatar'));

        $data['users'] = array($data['worker_id'] => $data['worker']);

        $chat = $this->workflowChat($workflowID);
        $data['chat'] = $chat->listing();
        foreach ($data['chat']['list'] as $k => &$item) {
            $item['message'] = Catcher::i()->getMessageWithoutCatchedWords($item['author_id'], ($item['author_id'] == $data['client_id']?  $data['worker_id'] : $data['client_id'] ), $item['message']);
        }unset($item);
        $data['chat'] = $this->viewPHP($data, 'workflow.chat.ajax');
        $data['chat_size'] = $chat->getMaxSize(true);

        $docs = $this->workflowsDocs($workflowID);
        $data['documents'] = $docs->getData($userID);
        foreach ($data['documents'] as & $v) {
            $v['link'] = $docs->getURL($v);
        } unset($v);

        # SEO: Просмотр хода работ
        $order = & $data['order'];
        $seoURL = static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword']), true);
        $data['url_view'] = static::urlDynamic($seoURL);
        $seoData = array(
            'title'       => tpl::truncate($order['title'], config::sysAdmin('fairplay.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full'  => $order['title'],
            'description' => tpl::truncate(strip_tags($order['descr']), config::sysAdmin('fairplay.view.meta.description.truncate', 150, TYPE_UINT)),
        );
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->seo()->robotsIndex(false);
        $this->setMeta('view', $seoData, $order);

        return $this->viewPHP($data, 'view');
    }

    /**
     * Блок со статусом хода работ (ajax обновление)
     * @param integer $workflowID ID хода работ
     * @return string HTML
     */
    protected function viewStatus($workflowID)
    {
        if ( ! $workflowID) {
            return '';
        }
        $data = $this->model->workflowData($workflowID);
        if (empty($data)) {
            return '';
        }
        $order = Orders::model()->orderData($data['order_id'], array('keyword'));
        $data = Catcher::getOrderPrepay($data);
        $data['url_view'] = static::url('view', array('id' => $data['id'], 'keyword' => $order['keyword']));

        return $this->viewPHP($data, 'workflow.status');
    }

    /**
     * Таб финансы, в настройках пользователя
     * @param bool $active флаг активности таба
     * @return string HTML
     */
    public function my_settings_finance($active = false)
    {
        $userID = User::id();

        if ( ! $userID && Request::isAJAX()) {
            $this->showAccessDenied();
        }
        $this->security->setTokenPrefix('my-settings');
        $act = $this->input->getpost('act', TYPE_STR);
        if ($act)
        {
            if ( ! $this->security->validateToken() && ! in_array($act, array('finance-view'))) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            }

            $response = array();
            switch ($act) {
                case 'save': # сохранение данных
                {

                    $old = $this->model->financeData($userID);
                    $data = $this->validateFinancesData(true);
                    if ( ! $this->errors->no()) break;
                    $data['status'] = static::VERIFIED_STATUS_NONE;
                    if ( ! empty($data)) {
                        $this->model->financeSave($userID, $data);
                    }

                    $images = $this->verifiedImages($userID);
                    if ( ! empty($_FILES)) {
                        $i = 0; $limit = $images->getLimit();
                        foreach ($_FILES as $k => $v) {
                            if ($i >= $limit) break;
                            if (strpos($k, 'verified_') === 0) {
                                $images->uploadFILES($k);
                            }
                        }
                    }

                    $deleted = $this->input->post('verified_deleted', TYPE_ARRAY_STR);
                    if ( ! empty($deleted)) {
                        $images->deleteImages($deleted);
                    }

                    $imgData = $images->getData();
                    if ( ! empty($imgData)) {
                        $update = array('status' => static::VERIFIED_STATUS_WAIT);
                        if (isset($old['verified_reason'])) {
                            $reason = func::unserialize($old['verified_reason']);
                            if (isset($reason['cnt'])) {
                                $reason['cnt']++;
                                $update['verified_reason'] = serialize($reason);
                            }
                        }
                        $this->model->financeSave($userID, $update);
                        $this->verifiedCounterUpdate();
                    }
                } break;
                case 'finance-view': # просмотр
                {
                    $imageID = $this->input->get('id', TYPE_UINT);
                    $images = $this->verifiedImages($userID);
                    $data = $images->getImageData($imageID);
                    if (empty($data)) {
                        $this->errors->error404();
                        break;
                    }
                    if ($data['filename'] != $this->input->get('fn', TYPE_STR)) {
                        $this->errors->error404();
                        break;
                    }
                    $images->show($data);
                } break;
                case 'delete': # удаление
                {

                    if ( ! $this->security->validateToken()) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $this->verifiedImages($userID)->deleteAllImages($userID);
                    $this->model->financeDelete($userID);
                } break;
                default:
                {
                    $this->errors->impossible();
                }
                    break;
            }
            if (Request::isAJAX()) {
                $this->ajaxResponseForm($response);
            }
            if (Request::isPOST()) {
                $this->iframeResponseForm($response);
            }
        }

        $data = $this->model->financeData($userID);
        $data['active'] = $active;
        $user = Users::model()->userData($userID, array('name', 'surname', 'birthdate'));
        if ( ! isset($data['birthdate'])) {
            $data['birthdate'] = $user['birthdate'];
        }
        if (empty($data['fio'])) {
            $data['fio'] = $user['surname'].' '.$user['name'];
        }
        # причины отклонения
        if ($data['status'] == static::VERIFIED_STATUS_DECLINED) {
            $data['verified_reason'] = func::unserialize($data['verified_reason'], array('id' => 0, 'message' => '', 'cnt' => 0));
            $verifiedReasons = static::verifiedReasons();
            if ($data['verified_reason']['id'] > 0 && isset($verifiedReasons[$data['verified_reason']['id']])) {
                $data['verified_reason']['message'] = $verifiedReasons[$data['verified_reason']['id']]['t'];
            } else if ($data['verified_reason']['id'] == static::VERIFIED_REASON_OTHER) {
                $data['verified_reason']['message'] = nl2br($data['verified_reason']['message']);
            }
        }
        $images = $this->verifiedImages($userID);
        $data['verified_images'] = $images->getData();
        foreach ($data['verified_images'] as & $v) {
            $v['link'] = Users::url('my.settings', array('act' => 'finance-view', 'id' => $v['id'], 'fn' => $v['filename']));
        } unset($v);

        if ($data['status'] == static::VERIFIED_STATUS_APPROVED) {
            return $this->viewPHP($data, 'my.finance.approved');
        }

        return $this->viewPHP($data, 'my.finance');
    }

    /**
     * Дополнительные ajax запросы
     */
    public function ajax()
    {
        $userID = User::id();

        $response = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'chat-add': # добавление сообщения к обсуждению
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $workflowID = $this->input->post('workflow', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('order_id', 'client_id', 'worker_id', 'client_expert_id', 'worker_expert_id'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($userID, array($data['client_id'], $data['worker_id'], $data['client_expert_id'], $data['worker_expert_id']))) {
                    $this->errors->reloadPage();
                    break;
                }
                $chat = $this->workflowChat($workflowID);
                $message = $this->input->post('message', TYPE_NOTAGS);
                $attach = $chat->uploadAttachFILES('attach');
                if ( ! $this->errors->no()) {
                    break;
                }
                $save = array(
                    'message'   => $message,
                    'author_id' => $userID,
                );
                if ( ! empty($attach)) {
                    $save['extra'] = array('attach' => array($attach));
                }
                $chatID = $chat->append($save);
                if (empty($chatID)) {
                    $this->errors->reloadPage();
                    break;
                }
                $id = $this->input->post('id', TYPE_UINT);
                $data['chat'] = $chat->listing($id, 'next');
                if ( ! empty($data['chat']['list'])) {
                    $data['chat']['more'] = 0;
                    foreach ($data['chat']['list'] as $k => &$item) {
                        Catcher::i()->checkMessage($item['author_id'], ($item['author_id'] == $data['client_id']?  $data['worker_id']: $data['client_id'] ), $item['message'], Catcher::FROM_FIREPLAY_CHAT);
                    }unset($item);
                    $response['html'] = $this->viewPHP($data, 'workflow.chat.ajax');
                }
            } break;
            case 'chat-more': # просмотр предыдущих сообщений
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $workflowID = $this->input->post('workflow', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('order_id', 'client_id', 'worker_id', 'client_expert_id', 'worker_expert_id'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($userID, array($data['client_id'], $data['worker_id'], $data['client_expert_id'], $data['worker_expert_id']))) {
                    $this->errors->reloadPage();
                    break;
                }
                $id = $this->input->post('id', TYPE_UINT);
                $chat = $this->workflowChat($workflowID);
                $data['chat'] = $chat->listing($id);
                foreach ($data['chat']['list'] as $k => &$item) {
                    $item['message'] = Catcher::i()->getMessageWithoutCatchedWords($item['author_id'], ($item['author_id'] == $data['client_id']? $data['worker_id']: $data['client_id']), $item['message']);
                }unset($item);
                $response['more'] = $data['chat']['more'];
                $data['chat']['more'] = 0;
                $response['html'] = $this->viewPHP($data, 'workflow.chat.ajax');
            } break;
            case 'chat-refresh': # обновление чата
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('workflow', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('order_id', 'client_id', 'worker_id', 'client_expert_id', 'worker_expert_id'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($userID, array($data['client_id'], $data['worker_id'], $data['client_expert_id'], $data['worker_expert_id']))) {
                    $this->errors->reloadPage();
                    break;
                }
                $id = $this->input->post('id', TYPE_UINT);
                $chat = $this->workflowChat($workflowID);
                $data['chat'] = $chat->listing($id, 'next');
                if ( ! empty($data['chat']['list'])) {
                    foreach ($data['chat']['list'] as $k => &$item) {
                        $item['message'] = Catcher::i()->getMessageWithoutCatchedWords($item['author_id'], ($item['author_id'] == $data['client_id'] ?  $data['worker_id']: $data['client_id'] ), $item['message']);
                    }unset($item);
                    $data['chat']['more'] = 0;
                    $response['html'] = $this->viewPHP($data, 'workflow.chat.ajax');
                }
            } break;
            case 'status-close-modal': # модальное окно для закрытия хода работ 
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'fairplay'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['client_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($data['status'], array(static::STATUS_EXECUTION, static::STATUS_REWORK))) {
                    $this->errors->reloadPage();
                    break;
                }
                $response['html'] = $this->viewPHP($data, 'status.close.modal');
            } break;
            case 'status-close-opinion-add': # закрытие хода работ, добавление отзыва заказчиком
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'order_id', 'status', 'client_id', 'worker_id', 'fairplay'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['client_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($data['status'], array(static::STATUS_EXECUTION, static::STATUS_REWORK))) {
                    $this->errors->reloadPage();
                    break;
                }
                # валидация отзыва
                $opinion = Opinions::i()->validateOpinionData(0, true);
                if ( ! $this->errors->no()) {
                    break;
                }

                $opinion['order_id'] = $data['order_id'];
                $opinion['user_id'] = $data['worker_id'];
                $cnt = Opinions::model()->opinionsDataByFilter(array('COUNT(id) AS id'), array(
                    'order_id'  => $data['order_id'],
                    'author_id' => $userID,
                ), true);
                if ($cnt['id']) {
                    $this->errors->reloadPage();
                    break;
                }
                # публикация отзыва
                $opinionID = Opinions::i()->opinionAdd($opinion);
                if ($opinionID) {
                    $this->model->workflowSave($workflowID, array(
                        'status'                => $data['fairplay'] ? static::STATUS_PAYMENT : static::STATUS_CLOSED,
                        'client_opinion'        => $opinionID
                    ));
                    $this->model->historyAdd($workflowID, array(
                        'type'    => static::HISTORY_TYPE_OPINION_CLIENT,
                        'user_id' => $userID,
                    ));
                    if ($data['fairplay']) {
                        $this->workflowsDocs($workflowID)->fireEvent(static::DOCS_EVENT_COMPLETE);
                        $this->paymentCounterUpdate(1);
                    }
                }

                $opinionTypes = Opinions::aTypes();
                $order = Orders::model()->orderData($data['order_id'], array('title', 'keyword'));
                Users::sendMailTemplateToUser($data['worker_id'], 'workflow_close', array(
                    'workflow_id'   => $workflowID,
                    'order_title'   => $order['title'],
                    'workflow_url'  => static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword'])),
                    'opinion_type'  => $opinionTypes[ $opinion['type'] ]['t'],
                ));

                $response['html'] = $this->viewStatus($workflowID);
                $response['msg'] = _t('', 'Операция выполнена успешно.');
            } break;
            case 'opinion-add-modal': # модальное окно для добавление отзыва исполнителем
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'worker_id', 'fairplay', 'worker_opinion'));

                if (empty($data) || ! empty($data['worker_opinion'])) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['worker_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! in_array($data['status'], array(static::STATUS_CLOSED, static::STATUS_PAYMENT))) {
                    $this->errors->reloadPage();
                    break;
                }
                $response['html'] = $this->viewPHP($data, 'opinion.add.modal');
            } break;
            case 'opinion-add': # добавление отзыва исполнителем
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'order_id', 'status', 'worker_id', 'client_id', 'fairplay', 'worker_opinion'));
                if (empty($data) || ! empty($data['worker_opinion'])) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['worker_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($data['status'], array(static::STATUS_CLOSED, static::STATUS_PAYMENT))) {
                    $this->errors->reloadPage();
                    break;
                }

                # валидация отзыва
                $opinion = Opinions::i()->validateOpinionData(0, true);
                if ( ! $this->errors->no()) {
                    break;
                }

                $opinion['order_id'] = $data['order_id'];
                $opinion['user_id'] = $data['client_id'];
                $cnt = Opinions::model()->opinionsDataByFilter(array('COUNT(id) AS id'), array(
                    'order_id'  => $data['order_id'],
                    'author_id' => $userID,
                ), true);
                if ($cnt['id']) {
                    $this->errors->reloadPage();
                    break;
                }
                # публикация отзыва
                $opinionID = Opinions::i()->opinionAdd($opinion);
                if ($opinionID) {
                    $this->model->workflowSave($workflowID, array(
                        'worker_opinion'        => $opinionID
                    ));
                    $this->model->historyAdd($workflowID, array(
                        'type'    => static::HISTORY_TYPE_OPINION_WORKER,
                        'user_id' => $userID,
                    ));
                }

                $opinionTypes = Opinions::aTypes();
                $order = Orders::model()->orderData($data['order_id'], array('title', 'keyword'));
                Users::sendMailTemplateToUser($data['client_id'], 'worker_opinion_add', array(
                    'workflow_id'   => $workflowID,
                    'order_title'   => $order['title'],
                    'workflow_url'  => static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword'])),
                    'opinion_type'  => $opinionTypes[ $opinion['type'] ]['t'],
                ));
                $response['msg'] = _t('', 'Операция выполнена успешно.');
                $response['html'] = $this->viewStatus($workflowID);
            } break;
            case 'opinion-view': # просмотр отзыва
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('workflow', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'order_id', 'status', 'client_id', 'worker_id', 'client_opinion', 'worker_opinion'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                $id = $this->input->post('id', TYPE_UINT);
                if ( ! $id) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['client_id'] != $userID && $data['worker_id'] != $userID && $data['client_opinion'] != $id && $data['worker_opinion'] != $id) {
                    $this->errors->reloadPage();
                    break;
                }
                $data['opinion'] = Opinions::model()->opinionData($id);
                if (empty($data['opinion'])) {
                    $this->errors->reloadPage();
                    break;
                }
                $response['html'] = $this->viewPHP($data, 'opinion.view.modal');
            } break;
            case 'reserved-modal': # модальное окно для резервирования средств заказчиком
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'price'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['client_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['status'] != static::STATUS_RESERVATION) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = Catcher::getOrderPrepay($data);
                $finance = $this->model->financeData($userID);
                if ($finance['status'] != static::VERIFIED_STATUS_APPROVED) {
                    $this->errors->reloadPage();
                    break;
                }
                $data['finance'] = $finance;
                $data['commission'] = $this->commission((Catcher::isUseOrderPrepay() && isset($data['prepay']) && !empty($data['prepay']))? $data['prepay'] : $data['price'], $data['descr']);
                $sum = round((Catcher::isUseOrderPrepay() && isset($data['prepay']) && !empty($data['prepay']))? $data['prepay'] : $data['price'] + $data['commission'], 2);
                $data['sum'] = $sum;
                $balance = User::balance();

                $data['payList'] = $this->payMethodsList($balance >= $sum);
                $response['html'] = $this->viewPHP($data, 'reserved.modal');
            } break;
            case 'reserved': # резервирования средств заказчиком
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'price'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['client_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['status'] != static::STATUS_RESERVATION) {
                    $this->errors->reloadPage();
                    break;
                }
                $finance = $this->model->financeData($userID);
                if ($finance['status'] != static::VERIFIED_STATUS_APPROVED) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = Catcher::getOrderPrepay($data);
                $commission = $this->commission((Catcher::isUseOrderPrepay() && isset($data['prepay']) && !empty($data['prepay']))? $data['prepay'] : $data['price']);
                $sum = round((Catcher::isUseOrderPrepay() && isset($data['prepay']) && !empty($data['prepay']))? $data['prepay'] : $data['price'] + $commission, 2);
                $ps = $this->input->post('ps', TYPE_STR);
                $settings = array(
                    'module' => $this->module_name,
                );
                if ($ps == 'balance') {
                    $balance = User::balance();
                    if ($balance >= $sum) {
                        Svc::i()->activate($this->module_name, static::SVC_RESERVED, array(), $workflowID, $userID, $sum, false, $settings);
                        $response['msg'] = _t('', 'Операция выполнена успешно.');
                        $response['html'] = $this->viewStatus($workflowID);
                    } else {
                        $this->errors->set(_t('svc', 'Недостаточно средств. Пополните счет или выберите другой способ оплаты.'));
                        break;
                    }
                } else {
                    $response['form'] = Bills::i()->createBill($ps, $sum, $settings, static::SVC_RESERVED, $workflowID);
                }
            } break;
            case 'status-cancel': # отмена заказа заказчиком
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'worker_id', 'client_id'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['client_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['status'] != static::STATUS_RESERVATION) {
                    $this->errors->reloadPage();
                    break;
                }

                $this->model->workflowSave($workflowID, array(
                    'status'        => static::STATUS_CANCEL,
                ));
                $response['html'] = $this->viewStatus($workflowID);
                $response['msg'] = _t('', 'Операция выполнена успешно.');
            } break;
            case 'arbitrage-modal': # модальное окно для обращения в арбитраж
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'worker_id', 'arbitrage'));
                if (empty($data) || $data['arbitrage']) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($userID, array($data['client_id'], $data['worker_id']))) {
                    $this->errors->reloadPage();
                    break;
                }
                $response['html'] = $this->viewPHP($data, 'arbitrage.modal');
            } break;
            case 'arbitrage': # обращение в арбитраж
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'worker_id', 'arbitrage', 'extra'));
                if (empty($data) || $data['arbitrage']) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! in_array($userID, array($data['client_id'], $data['worker_id']))) {
                    $this->errors->reloadPage();
                    break;
                }
                $message = $this->input->post('message', TYPE_NOTAGS);
                if (empty($message)) {
                    $this->errors->set(_t('', 'Укажите текст сообщения'));
                }

                $chat = $this->workflowChat($workflowID);
                $chat->append(array(
                    'type' => static::CHAT_TYPE_ARBITRAGE_START,
                    'author_id' => $userID,
                ));
                $chatID = $chat->append(array(
                    'message'   => $message,
                    'author_id' => $userID,
                    'extra' => array('arbitrage' => 1)
                ));
                if (empty($chatID)) {
                    $this->errors->reloadPage();
                    break;
                }
                $data['extra']['arbitrage_chat_id'] = $chatID;
                $this->model->workflowSave($workflowID, array(
                    'arbitrage' => 1,
                    'extra'     => $data['extra'],
                ));
                $this->model->historyAdd($workflowID, array(
                    'type'    => static::HISTORY_TYPE_ARBITRAGE_START,
                    'user_id' => $userID,
                ));
                $this->model->workflowUsersAdd($workflowID, 1);

                $this->arbitrageCounterUpdate(1);
                $response['msg'] = _t('', 'Операция выполнена успешно.');
                $response['html'] = $this->viewStatus($workflowID);
            } break;
            case 'complete-modal': # модальное окно для информирования заказчика о завершении хода работ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'worker_id', 'extra'));
                if (empty($data) || ! in_array($data['status'], array(static::STATUS_EXECUTION, static::STATUS_REWORK))) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( $userID != $data['worker_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! empty($data['extra']['complete'])) {
                    $this->errors->reloadPage();
                    break;
                }
                $response['html'] = $this->viewPHP($data, 'complete.modal');
            } break;
            case 'complete': # информирование заказчика о завершении хода работ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'worker_id', 'order_id', 'client_id', 'extra'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if (empty($data) || ! in_array($data['status'], array(static::STATUS_EXECUTION, static::STATUS_REWORK))) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( $userID != $data['worker_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! empty($data['extra']['complete'])) {
                    $this->errors->reloadPage();
                    break;
                }

                $data['extra']['complete'] = 1;
                $this->model->workflowSave($workflowID, array(
                    'extra'     => $data['extra'],
                ));

                $order = Orders::model()->orderData($data['order_id'], array('title', 'keyword'));
                $message = $this->input->post('message', TYPE_NOTAGS);
                Users::sendMailTemplateToUser($data['client_id'], 'workflow_complete', array(
                    'workflow_id'   => $workflowID,
                    'order_title'   => $order['title'],
                    'workflow_url'  => static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword'])),
                    'message'       => $message,
                ));
                $chat = $this->workflowChat($workflowID);
                $chat->append(array(
                    'type' => static::CHAT_TYPE_COMPLETE,
                    'author_id' => $userID,
                ));

                if ( ! empty($message)) {
                    $chat->append(array(
                        'message' => $message,
                        'author_id' => $userID,
                    ), false);
                }
                $this->model->historyAdd($workflowID, array(
                    'type' => static::HISTORY_TYPE_COMPLETE,
                ));
                $response['msg'] = _t('', 'Операция выполнена успешно.');
                $response['html'] = $this->viewStatus($workflowID);
            } break;
            case 'continue-modal': # модальное окно для информирования исполнителя о продолжении (несогласие заказчика с завершением хода работ)
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'extra'));
                if (empty($data) || ! in_array($data['status'], array(static::STATUS_EXECUTION, static::STATUS_REWORK))) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( $userID != $data['client_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                if (empty($data['extra']['complete'])) {
                    $this->errors->reloadPage();
                    break;
                }
                $response['html'] = $this->viewPHP($data, 'continue.modal');
            } break;
            case 'continue': # информирования исполнителя о продолжении (несогласие заказчика с завершением хода работ)
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'order_id', 'worker_id', 'client_id', 'extra'));
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                if (empty($data) || ! in_array($data['status'], array(static::STATUS_EXECUTION, static::STATUS_REWORK))) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( $userID != $data['client_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                if (empty($data['extra']['complete'])) {
                    $this->errors->reloadPage();
                    break;
                }
                $message = $this->input->post('message', TYPE_NOTAGS);

                $data['extra']['complete'] = 0;
                $this->model->workflowSave($workflowID, array(
                    'extra'     => $data['extra'],
                    'status'    => static::STATUS_REWORK,
                ));

                $order = Orders::model()->orderData($data['order_id'], array('title', 'keyword'));
                Users::sendMailTemplateToUser($data['worker_id'], 'workflow_continue', array(
                    'workflow_id'   => $workflowID,
                    'order_title'   => $order['title'],
                    'workflow_url'  => static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword'])),
                    'message'   => $message,
                ));
                $chat = $this->workflowChat($workflowID);
                $chat->append(array(
                    'type' => static::CHAT_TYPE_REWORK,
                    'author_id' => $userID,
                ));
                if ( ! empty($message)) {
                    $chat->append(array(
                        'message' => $message,
                        'author_id' => $userID,
                    ), false);
                }

                $this->model->historyAdd($workflowID, array(
                    'type' => static::HISTORY_TYPE_REWORK,
                ));
                $response['msg'] = _t('', 'Операция выполнена успешно.');
                $response['html'] = $this->viewStatus($workflowID);
            } break;
            case 'change-modal': # модальное окно для изменения данных хода работ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'order_id', 'term', 'price', 'fairplay'));
                if (empty($data) || $data['status'] != static::STATUS_EXECUTION) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( $userID != $data['client_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['fairplay'] == static::FAIRPLAY_USE) {
                    $this->errors->reloadPage();
                    break;
                }
                $data += Orders::model()->orderData($data['order_id'], array('title', 'keyword'));
                $response['html'] = $this->viewPHP($data, 'change.modal');
            } break;
            case 'change': # изменения данных хода работ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $workflowID = $this->input->post('id', TYPE_UINT);
                if ( ! $workflowID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->workflowData($workflowID, array('id', 'status', 'client_id', 'term', 'price', 'fairplay'));
                if (empty($data) || $data['status'] != static::STATUS_EXECUTION) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( $userID != $data['client_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['fairplay'] == static::FAIRPLAY_USE) {
                    $this->errors->reloadPage();
                    break;
                }


                $save = $this->validateOfferWorkflowData();
                if ( ! $this->errors->no()) {
                    break;
                }
                if ($save['fairplay'] == static::FAIRPLAY_USE) {
                    $save['status'] = static::STATUS_RESERVATION;
                } else {
                    unset($save['fairplay']);
                }
                if ($this->model->workflowSave($workflowID, $save)) {
                    $this->workflowsDocs($workflowID)->fireEvent(static::DOCS_EVENT_CONFIRM);
                    $this->model->historyAdd($workflowID, array(
                        'type' => static::HISTORY_TYPE_FAIRPLAY,
                    ));
                }
            } break;
            default:
                $this->errors->impossible();
        }
        if (Request::isAJAX()) {
            $this->ajaxResponseForm($response);
        }
        $this->iframeResponseForm($response);
    }

    /**
     * Валидация данных хода работ для фомы выбора исполнителя заявки к заказу
     * @return array
     */
    public function validateOfferWorkflowData()
    {
        $data = $this->input->postm(array(
            'fairplay' => TYPE_UINT,  # Безопасная сделка
            'price'    => TYPE_PRICE, # Бюджет
            'term'     => TYPE_UINT,  # Срок
        ));
        if (empty($data['price'])) {
            $this->errors->set(_t('fp', 'Укажите бюджет'), 'price');
        }
        if (empty($data['term'])) {
            $this->errors->set(_t('fp', 'Укажите срок'), 'term');
        }
        if ( ! in_array($data['fairplay'], array(static::FAIRPLAY_USE, static::FAIRPLAY_NONE))) {
            $data['fairplay'] = static::FAIRPLAY_NONE;
        }
        return $data;
    }

    /**
     * Статус проверки финансовых данных пользователя
     * @param integer $userID ID ползователя
     * @return int
     */
    public static function financeVerified($userID)
    {
        return static::model()->financeVerified($userID);
    }

    /**
     * Список систем оплаты
     * @param bool $balanceUse использовать баланс пользователя
     * @return string
     */
    public function payMethodsList($balanceUse = true)
    {
        $data = array();
        $data['paySystems'] = Bills::getPaySystems($balanceUse);
        foreach ($data['paySystems'] as $k=>$v) {
            if (isset($v['enabled.fairplay']) && empty($v['enabled.fairplay'])) {
                unset($data['paySystems'][$k]);
            }
        }
        return $this->viewPHP($data, 'pay.methods');
    }

    /**
     * Обработка запроса от системы авторизации oAuth
     * @param string ::get 'system' система авторизации
     * @return string
     */
    public function oauth()
    {
        $system = trim($this->input->get('system', TYPE_NOTAGS), ' /');
        if (empty($system)) {
            exit;
        }
        $method = $system . '_request';
        if ( ! method_exists($this, $method)) {
            $this->log('Способ оплаты отключен: '.$system);
            exit;
        }
        return $this->$method();

    }

    /**
     * OAauth данные от Yandex.Money
     */
    protected function yandex_request()
    {
        $path = $this->security->sessionPath(true);
        $this->apiYandexMoney()->saveAccessToken(SITEURL.$path.'index.php?s=fairplay&ev=pay_systems&act=yandex-money-info');
    }

    /**
     * Проверка резервирования средств заказчика для вывода в статусе к заявки исполнителя.
     * Чтоб исполнитель не начал преждевременно выполнять работу.
     * Используется в /modules/orders/tpl/def/offers.list.php
     * @param integer $orderID ID заказа
     * @param string $orderKeyword keyword заказа
     * @param string $title @ref 'Вы согласились стать исполнителем на этот заказ.'
     */
    public function checkReserved($orderID, $orderKeyword, & $title)
    {
        if (empty($orderID)) return;

        $workflow = $this->model->workflowData(array('order_id' => $orderID), array('id', 'status'));
        if (empty($workflow)) return;
        if ($workflow['status'] == static::STATUS_RESERVATION) {
            $title .= _t('fp', '<br />Дождитесь пока заказчик зарезервирует сумму прежде чем приступить к работе.');
        }
        $title .= _t('fp', '<br />Дальнейшее общение по заказу вы можете продолжить в <a [link]>разделе “Ход работ”</a>', array(
            'link' => 'href="'.static::url('view', array('id' => $workflow['id'], 'keyword' => $orderKeyword)).'"'));
    }

    /**
     * Скачивание сгенерированого ранее документа
     */
    public function document()
    {
        $this->workflowsDocs(0)->download();
        header("HTTP/1.0 404 Not Found");
        exit;
    }

    /**
     * Cron: Уведомления о новых сообщениях в чате
     * Рекомендуемый интервал: раз в 5 минут
     */
    public function cronChatEmailNotification()
    {
        if ( ! bff::cron()) {
            return;
        }

        $this->workflowChat()->cronEmailNotification();
    }

    /**
     * Генерация документов по крону
     */
    public function cronDocsMake()
    {
        if ( ! bff::cron()) {
            return;
        }
        $this->workflowsDocs(0)->cronMake();
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            # Уведомления о сообщениях в чате хода работы
            'cronChatEmailNotification' => array('period' => '*/5 * * * *'),
            # Формирование документации
            'cronDocsMake' => array('period' => '* * * * *'),
        );
    }

}