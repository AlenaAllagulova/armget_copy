<?php

/**
 * Класс работы с документами хода работ
 */

use bff\utils\Files;

class FairplayWorkflowsDocs_ extends Component
{
    /** @var string Путь к документам */
    protected $path = '';

    /** @var string URL к документам */
    protected $url = '';

    /** @var integer ID записи */
    protected $workflowID = 0;

    /**
     * Раскладывать ли файлы в зависимости от recordID, по папкам folder = (recordID / recordsLimit)
     * @var boolean
     */
    protected $folderByID = true;
    protected $folderByID_RecordsLimit = 1000;

    public function __construct($workflowID = 0)
    {
        $this->init();
        $this->setRecordID($workflowID);
        $this->path = PATH_BASE . 'files' . DS . 'workflows'. DS;
        $this->url = Fairplay::url('document');
    }

    public function setRecordID($workflowID)
    {
        $this->workflowID = $workflowID;
    }

    /**
     * Генерация документов, по крону
     * Период: каждую минуту
     */
    public function cronMake()
    {
        $ts = time();
        do{
            $event = $this->seekEvent();
            if (empty($event)) return;

            $data = $this->prepareMakeData($event['workflow_id']);
            if (empty($data)) return;

            extract($data, EXTR_REFS);

            $documents = Fairplay::model()->docsListing(array('event' => $event['document_id']),
                                                        array('id', 'types', 'file_prefix', 'content', 'title'));
            foreach ($documents as $v) {
                $v['workflow_id'] = $workflow['id'];
                $v['content'] = strtr($v['content'], $macroses);
                if ($v['types'] & Fairplay::DOCS_TYPE_WORKER) {
                    $this->cronMakeSave($workflow['worker_id'], $v);
                }
                if ($v['types'] & Fairplay::DOCS_TYPE_CLIENT) {
                    $this->cronMakeSave($workflow['client_id'], $v);
                }
            }
            $this->db->delete(TABLE_FAIRPLAY_WORKFLOWS_DOCS, array('id' => $event['id']));
        } while((time() - $ts) < 55);
    }

    /**
     * Подготовка данных (макросов) для генерации документа
     * @param integer $workflowID ID хода работ
     * @return array|bool
     */
    protected function prepareMakeData($workflowID)
    {
        $workflow = Fairplay::model()->workflowData($workflowID, array('id', 'created', 'client_id', 'worker_id',
            'price', 'term', 'commission', 'reserved'));
        if (empty($workflow)) return false;
        $u = array(
            'client' => Fairplay::model()->financeData($workflow['client_id']),
            'worker' => Fairplay::model()->financeData($workflow['worker_id'])
        );
        include_once modification(PATH_MODULES.'fairplay/workflows.docs.number.php');
        $numbers = new FairplayWorkflowsDocsNumbers();

        $macroses = array(
            '{workflow_id}' => $workflow['id'],
            '{workflow_created}' => date('d.m.Y', strtotime($workflow['created'])),
            '{workflow_created_text}' => tpl::date_format2($workflow['created']),
            '{workflow_price}' => number_format($workflow['price'], 2, ',' ,' '),
            '{workflow_price_text}' => $numbers->priceToString($workflow['price']),
            '{workflow_commission}' => number_format($workflow['commission'], 2, ',' ,' '),
            '{workflow_commission_text}' => $numbers->priceToString($workflow['commission']),
            '{workflow_term}' => $workflow['term'],
            '{workflow_finish}' => date('d.m.Y', strtotime('+'.intval($workflow['term']).' days', strtotime($workflow['reserved']))),
        );
        $fields = Fairplay::financeFields();
        foreach ($u as $k => $v) {
            foreach ($fields as $kk => $vv) {
                foreach ($vv['fields'] as $kkk => $vvv) {
                    if ( ! isset($v[ $kkk ])) continue;
                    $macroses['{'.$k.'_'.($kk != 'general' ? $kk.'_' : '').$kkk.'}'] = $v[ $kkk ];
                }
            }
        }
        return array(
            'workflow' => $workflow,
            'macroses' => bff::filter('fairplay.docs.macroses.prepare', $macroses, array('workflow'=>&$workflow, 'u'=>&$u, 'numbers'=>&$numbers)),
        );
    }

    /**
     * Генерация документа для пользователя
     * @param integer $userID
     * @param array $doc @ref данные для генерации документа
     */
    protected function cronMakeSave($userID, & $doc)
    {
        $filename = $doc['file_prefix'].$doc['workflow_id'].'_'.$doc['id'].'_'.$userID.'_'.func::generator(8).'.pdf';
        $this->checkFolderByID();
        $path = $this->getPath($filename);
        Fairplay::i()->pdfGenerator()->output($doc['content'], array(
            'dest' => 'F',
            'filename' => $path,
        ));
        if ( ! file_exists($path)) {
            bff::log('Ошибка создания документа #'.$doc['id'].' для хода работ #'.$doc['workflow_id']);
            return;
        }
        $macro = array('{workflow_id}' => $doc['workflow_id']);
        $title = strtr($doc['title'], $macro);
        $this->db->insert(TABLE_FAIRPLAY_WORKFLOWS_DOCS, array(
            'workflow_id' => $doc['workflow_id'],
            'user_id'     => $userID,
            'created'     => $this->db->now(),
            'document_id' => $doc['id'],
            'title'       => $title,
            'filename'    => $filename,
        ));
    }

    /**
     * Генерация документа для пользователя в пределах хода работ
     * @param integer $userID
     * @param integer $documentID
     * @return bool -
     */
    public function makeSingle($userID, $documentID)
    {
        if ( ! $this->workflowID) return false;

        $data = $this->prepareMakeData($this->workflowID);
        if (empty($data)) return false;

        extract($data, EXTR_REFS);

        if ( ! in_array($userID, array($workflow['client_id'], $workflow['worker_id']))) {
            return false;
        }

        $doc = Fairplay::model()->docData($documentID);
        if (empty($doc)) return false;

        $doc['workflow_id'] = $this->workflowID;
        $doc['content'] = strtr($doc['content'], $macroses);
        $this->cronMakeSave($userID, $doc);

        return true;
    }

    /**
     * Проверка и создание в случае необходимости папки для хранения документов
     */
    protected function checkFolderByID()
    {
        # динамические папки используются
        if ($this->folderByID && ! empty($this->workflowID)) {
            $dirName = $this->getDir();
            if (!file_exists($dirName)) {
                Files::makeDir($dirName);
            }
        }
    }

    /**
     * Формирование названия каталога исходя из ID записи
     * @return string название директории
     */
    protected function getDir()
    {
        return (string)intval($this->workflowID / $this->folderByID_RecordsLimit);
    }

    /**
     * Формирование пути к файлу
     * @param string $filename имя файла
     * @return string Путь
     */
    protected function getPath($filename)
    {
        return $this->path . ($this->folderByID ? $this->getDir().DS : '') . $filename;
    }

    /**
     * Генерация события формирования документов
     * @param integer $eventID ID события
     */
    public function fireEvent($eventID)
    {
        if (empty($eventID)) return;
        if (empty($this->workflowID)) return;

        $this->db->insert(TABLE_FAIRPLAY_WORKFLOWS_DOCS, array(
            'workflow_id' => $this->workflowID,
            'document_id' => $eventID,
            'user_id'     => 0,
            'filename'    => '',
        ));
    }

    /**
     * Резервирование и поиск ближайшего события формирования документов
     * @return mixed
     */
    protected function seekEvent()
    {
        $pid = posix_getpid();
        $this->db->exec('
            UPDATE '.TABLE_FAIRPLAY_WORKFLOWS_DOCS.' SET 
            filename = :pid 
            WHERE user_id = 0 AND filename = :empty 
            ORDER BY id 
            LIMIT 1', array(':pid' => $pid, ':empty' => ''));
        return $this->db->one_array('
            SELECT id, workflow_id, document_id 
            FROM '.TABLE_FAIRPLAY_WORKFLOWS_DOCS.' 
            WHERE user_id = 0 AND filename = :pid 
            LIMIT 1', array(':pid' => $pid));
    }

    /**
     * Получаем данные о сформированных на текущий момент документах
     * @param integer $userID
     * @return array
     */
    public function getData($userID)
    {
        if (empty($this->workflowID)) return array();
        if (empty($userID)) return array();

        return $this->db->select_key('
            SELECT * 
            FROM ' . TABLE_FAIRPLAY_WORKFLOWS_DOCS . '
            WHERE workflow_id = :id AND user_id = :user 
            ORDER BY created', 'id',
            array(':id' => $this->workflowID, ':user' => $userID)
        );
    }

    /**
     * Получаем данные о документе
     * @param integer $docID ID документа
     * @return mixed
     */
    public function getDocData($docID)
    {
        return $this->db->one_array('SELECT * FROM ' . TABLE_FAIRPLAY_WORKFLOWS_DOCS . ' WHERE id = :id', array(':id' => $docID));
    }

    /**
     * Формирование URL документа
     * @param array $data : filename - название файла gen(N).ext, dir - # папки, srv - ID сервера
     * @return string URL
     */
    public function getURL($data)
    {
        if (empty($data)) return '';

        return $this->url.$data['id'].'-'.$this->hash($data);
    }

    /**
     * Формирование хеш ключа для защиты от просмотра
     * @param array $data
     * @return string
     */
    protected function hash($data)
    {
        return md5($data['id'].$data['workflow_id'].$data['user_id'].$data['document_id'].$data['filename']);
    }

    /**
     * Скачивание документа
     */
    public function download()
    {
        $id = $this->input->get('id', TYPE_UINT);
        $hash = $this->input->get('hash', TYPE_STR);
        if ( ! $id) return;

        $data = $this->getDocData($id);
        if (empty($data)) return;

        if ( ! $this->security->compareString($this->hash($data), $hash)) {
            return;
        }

        $this->setRecordID($data['workflow_id']);
        $path = $this->getPath($data['filename']);
        if ( ! file_exists($path)) return;

        $name = 'document-'.func::generator(4);
        $pos = strpos($data['filename'], $data['workflow_id'].'_'.$data['document_id'].'_'.$data['user_id'].'_');
        if ($pos > 0) {
            $name = substr($data['filename'], 0, $pos).'-'.$data['workflow_id'];
        }
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=".$name.".pdf");
        readfile($path);
        exit;
    }

    /**
     * Удаление документа
     * @param array|integer $data информация о документе или ID документа
     * @return boolean
     */
    public function deleteDoc($data)
    {
        if (empty($data)) {
            return false;
        }
        if (is_numeric($data)) {
            $data = $this->getDocData($data);
        }
        if (empty($data['id'])) {
            return false;
        }
        if ($this->workflowID && $data['workflow_id'] != $this->workflowID) {
            return false;
        }

        $this->setRecordID($data['workflow_id']);
        $path = $this->getPath($data['filename']);
        if (file_exists($path)) {
            @unlink($path);
        }
        $this->db->delete(TABLE_FAIRPLAY_WORKFLOWS_DOCS, array('id' => $data['id']));
        return true;
    }

}