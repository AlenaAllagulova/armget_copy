<?php

class Fairplay_ extends FairplayBase
{
    # Табы фильтрации списка ходов работ:
    const TAB_WF_ALL         = 0;  # Все
    const TAB_WF_PAYMENT     = 1;  # Выплата
    const TAB_WF_ARBITRAGE   = 2;  # Арбитраж
    const TAB_WF_RESERVATION = 3;  # Резервирование
    const TAB_WF_EXECUTING   = 4;  # Выполняются
    const TAB_WF_CLOSED      = 5;  # Закрытые
    const TAB_WF_CANCELED    = 6;  # Отменены

    #---------------------------------------------------------------------------------------
    # Ход работ

    /**
     * Список
     * @param int $forceTab
     * @return string
     */
    public function workflows($forceTab = 0)
    {
        if (! $this->haveAccessTo('workflows-moderate')) {
            return $this->showAccessDenied();
        }

        $act = $this->input->postget('act', TYPE_STR);
        if (!empty($act) || Request::isPOST()) {
            $response = array();
            switch ($act) {
                case 'edit': # редактирование
                {
                    $workflowID = $this->input->postget('id', TYPE_UINT);
                    if (!$workflowID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $data = $this->model->workflowData($workflowID, array());
                    if (empty($data)) {
                        $this->errors->unknownRecord();
                        break;
                    }
                    $data['client'] = Users::model()->userData($data['client_id'], array('email', 'name', 'surname'));
                    $data['worker'] = Users::model()->userData($data['worker_id'], array('email', 'name', 'surname'));
                    $data['order'] = Orders::model()->orderData($data['order_id'], array('id', 'title', 'keyword', 'visibility'));

                    $data['history'] = $this->model->historyList($workflowID, array('id', 'type', 'extra', 'created'));
                    $hist = array();
                    foreach ($data['history'] as & $v) {
                        $hist[] = $v['id'];
                        $v['descr'] = static::historyDescription($v['type'], $v['extra']);
                    } unset($v);
                    $this->model->dropNotification($workflowID, 1, $hist, static::NOTIFICATIONS_TYPE_HISTORY);

                    if ($data['arbitrage'] && $data['extra']['arbitrage_chat_id']) {
                        $cnt = $this->model->workflowUsersByFilter(array('workflow_id' => $workflowID, 'user_id' => 1), array('cnt_new'));
                        if ( ! empty($cnt['cnt_new'])) {
                            $data['cnt_new'] = $cnt['cnt_new'];
                        }
                    }

                    $docs = $this->workflowsDocs($workflowID);
                    $data['documents'] = array();

                    foreach (array('client', 'worker') as $f) {
                        if ( ! empty($data[$f.'_expert_id'])) {
                            $data[$f.'_expert'] = Users::model()->userData($data[$f.'_expert_id'], array('email'));
                        }
                        $data['documents'][$f] = $docs->getData($data[$f.'_id']);
                        foreach ($data['documents'][$f] as & $v) {
                            $v['link'] = $docs->getURL($v);
                        } unset($v);
                    }

                    $data['bills_list'] = $this->workflowBillList($workflowID);
                    $data['act'] = $act;
                    $data = Catcher::getOrderPrepay($data);
                    $response['form'] = $this->viewPHP($data, 'admin.workflow.form');
                    if (Request::isAJAX()) $this->ajaxResponseForm($response, 2, true);
                } break;
                case 'info': # краткая информация о ходе работ (popup)
                {
                    $workflowID = $this->input->getpost('id', TYPE_UINT);
                    if (!$workflowID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $data = $this->model->workflowData($workflowID, array(
                            'id',
                            'client_id',
                            'worker_id',
                            'order_id',
                            'created',
                            'status',
                            'fairplay',
                            'arbitrage',
                            'extra',
                        )
                    );
                    if (empty($data)) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($data['arbitrage'] && $data['extra']['arbitrage_chat_id']) {
                        $chat = $this->workflowChat($workflowID);
                        $data['chat'] = $chat->listing($data['extra']['arbitrage_chat_id'], 'current');
                        $msg = reset($data['chat']['list']);
                        $data['arbitrage_message'] = '<div>'.nl2br($msg['message']).'</div>';
                    }


                    $data['client'] = Users::model()->userData($data['client_id'], array('email', 'name', 'blocked'));
                    $data['worker'] = Users::model()->userData($data['worker_id'], array('email', 'name', 'blocked'));
                    $data['order'] = Orders::model()->orderData($data['order_id'], array('id', 'title', 'keyword', 'visibility'));
                    echo $this->viewPHP($data, 'admin.workflow.info');
                    exit;
                }  break;
                case 'chat-more': # предыдущие сообщения в чате
                {
                    $workflowID = $this->input->post('workflow', TYPE_UINT);
                    if ( ! $workflowID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $data = $this->model->workflowData($workflowID, array('order_id', 'client_id', 'worker_id', 'client_expert_id', 'worker_expert_id',
                                                                          'arbitrage', 'extra'));
                    if (empty($data)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $data['arbitrage_chat_id'] = 0;
                    if ($data['arbitrage'] && $data['extra']['arbitrage_chat_id']) {
                        $data['arbitrage_chat_id'] = $data['extra']['arbitrage_chat_id'];
                    }
                    $id = $this->input->post('id', TYPE_UINT);
                    $chat = $this->workflowChat($workflowID);
                    $data['chat'] = $chat->listing($id);
                    $response['more'] = $data['chat']['more'];
                    if ($id) $data['chat']['more'] = 0;
                    $response['html'] = $this->viewPHP($data, 'admin.workflow.chat.ajax');
                } break;
                case 'chat-add': # добавление сообщения к обсуждению
                {
                    $workflowID = $this->input->post('workflow', TYPE_UINT);
                    if ( ! $workflowID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $data = $this->model->workflowData($workflowID, array('order_id', 'client_id', 'worker_id', 'client_expert_id', 'worker_expert_id'));
                    if (empty($data)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $chat = $this->workflowChat($workflowID);
                    $message = $this->input->post('message', TYPE_NOTAGS);
                    $attach = $chat->uploadAttachFILES('attach');
                    if ( ! $this->errors->no()) {
                        break;
                    }
                    $save = array(
                        'message'   => $message,
                        'author_id' => User::id(),
                    );
                    if ( ! empty($attach)) {
                        $save['extra'] = array('attach' => array($attach));
                    }
                    $chatID = $chat->append($save);
                    if (empty($chatID)) {
                        $this->errors->impossible();
                        break;
                    }
                    $id = $this->input->post('id', TYPE_UINT);
                    $data['chat'] = $chat->listing($id, 'next');
                    $data['chat']['more'] = 0;
                    $response['html'] = $this->viewPHP($data, 'admin.workflow.chat.ajax');
                    $this->iframeResponseForm($response);
                } break;
                case 'user': # autocomplete: пользователь
                {
                    $sEmail = $this->input->post('q', TYPE_NOTAGS);
                    $sEmail = $this->input->cleanSearchString($sEmail);
                    $aFilter = array(
                        ':email'    => array(
                            (Users::model()->userEmailCrypted() ? 'BFF_DECRYPT(email)' : 'email') . ' LIKE :email',
                            ':email' => $sEmail . '%'
                        ),
                        'blocked'   => 0,
                        'activated' => 1,
                    );
                    $aUsers = Users::model()->usersList($aFilter, array('user_id', 'email'));
                    $this->autocompleteResponse($aUsers, 'user_id', 'email');
                } break;
                case 'expert-set': # назначение эксперта
                {
                    $workflowID = $this->input->post('id', TYPE_UINT);
                    $workflow = $this->model->workflowData($workflowID, array('order_id', 'client_id', 'worker_id', 'client_expert_id', 'worker_expert_id'));
                    if (empty($workflow)) {
                        $this->errors->impossible();
                        break;
                    }

                    $type = $this->input->post('type', TYPE_STR);
                    if ( ! $workflowID || ! in_array($type, array('client', 'worker'))) {
                        $this->errors->impossible();
                        break;
                    }
                    $userID = $this->input->post('expert', TYPE_UINT);
                    if ( ! $userID) {
                        $this->errors->set(_t('fp', 'Выберите пользователя'));
                        break;
                    }
                    $user = Users::model()->userData($userID, array('email', 'workflow'));
                    if (empty($user['email'])) {
                        $this->errors->set(_t('fp', 'Выберите пользователя'));
                        break;
                    }
                    $response['email'] = $user['email'];
                    if ($userID == $workflow['client_id']) {
                        $this->errors->set(_t('fp', 'Эксперт не может быть заказчиком'));
                        break;
                    }
                    if ($userID == $workflow['worker_id']) {
                        $this->errors->set(_t('fp', 'Эксперт не может быть исполнителем'));
                        break;
                    }
                    if ($userID == $workflow[$type.'_expert_id']) { # назначили того-же самого
                        break;
                    }
                    if (in_array($userID, array($workflow['client_expert_id'], $workflow['worker_expert_id']))) {
                        $this->errors->set(_t('fp', 'Пользователь уже назначен экспертом'));
                        break;
                    }
                    $this->model->workflowSave($workflowID, array(
                        $type.'_expert_id' => $userID,
                    ));
                    $this->model->workflowUsersAdd($workflowID, $userID);

                    $order = Orders::model()->orderData($workflow['order_id'], array('title', 'keyword'));

                    Users::sendMailTemplateToUser($userID, 'workflow_expert_assigned', array(
                        'workflow_id'   => $workflowID,
                        'order_title'   => $order['title'],
                        'workflow_url'  => static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword'])),
                    ));

                    if ( ! $user['workflow']) {
                        Users::model()->userSave($userID, array('workflow' => 1));
                    }
                } break;
                case 'expert-cancel': # отмена эксперта
                {
                    $workflowID = $this->input->post('id', TYPE_UINT);
                    $workflow = $this->model->workflowData($workflowID, array('client_expert_id', 'worker_expert_id'));
                    if (empty($workflow)) {
                        $this->errors->impossible();
                        break;
                    }

                    $type = $this->input->post('type', TYPE_STR);
                    if ( ! $workflowID || ! in_array($type, array('client', 'worker'))) {
                        $this->errors->impossible();
                        break;
                    }
                    $expertID = $workflow[$type.'_expert_id'];
                    $this->model->workflowSave($workflowID, array(
                        $type.'_expert_id' => 0,
                    ));
                    $this->model->workflowUsersDelete($workflowID, $expertID);
                } break;
                case 'arbitrage-close': # завершение арбитража
                {
                    $workflowID = $this->input->post('id', TYPE_UINT);
                    $workflow = $this->model->workflowData($workflowID, array('arbitrage', 'extra'));
                    if (empty($workflow)) {
                        $this->errors->impossible();
                        break;
                    }

                    unset($workflow['extra']['arbitrage_chat_id']);
                    $this->model->workflowSave($workflowID, array(
                        'arbitrage' => 0,
                        'extra'     => $workflow['extra'],
                    ));
                    $this->model->historyAdd($workflowID, array(
                        'type'    => static::HISTORY_TYPE_ARBITRAGE_STOP,
                        'user_id' => User::id(),
                    ));
                    $this->workflowChat($workflowID)->append(array(
                        'type' => static::CHAT_TYPE_ARBITRAGE_STOP,
                        'author_id' => User::id(),
                    ));
                    $this->model->workflowUsersDelete($workflowID, 1);
                    $this->arbitrageCounterUpdate();
                } break;
                case 'set-status': # установить статус
                {

                    $status = $this->input->post('status', TYPE_UINT);
                    $workflowID = $this->input->post('id', TYPE_UINT);
                    $this->setWorkflowsStatus($workflowID, $status);
                } break;
                case 'comission-calc': # рассчитать комиссию
                {
                    $price = $this->input->post('price', TYPE_NUM);
                    $response['commission'] = $this->commission($price);
                } break;
                case 'statuses-history': # отображение истории изменения статусов
                {
                    $workflowID = $this->input->post('id', TYPE_UINT);
                    $data = array('id' => $workflowID);
                    $data['list'] = $this->model->workflowsStatusesByFilter(array('workflow_id' => $workflowID), array(
                        'id', 'user_id', 'fairplay', 'status', 'arbitrage', 'created'), false);
                    $response['html'] = $this->viewPHP($data, 'admin.workflow.statuses.history');
                } break;
                case 'doc-delete': # удалить документ
                {
                    $workflowID = $this->input->post('workflow', TYPE_UINT);
                    if ( ! $workflowID) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $id = $this->input->post('id', TYPE_UINT);
                    if ( ! $id) {
                        $this->errors->impossible();
                        break;
                    }
                    $this->workflowsDocs($workflowID)->deleteDoc($id);
                } break;
                case 'doc-create-popup': # окно для создания документа в пределах хода работ
                {
                    do{
                        $workflowID = $this->input->get('workflow', TYPE_UINT);
                        if ( ! $workflowID) {
                            $this->errors->impossible();
                            break;
                        }
                        $userID = $this->input->get('user_id', TYPE_UINT);
                        if ( ! $userID) {
                            $this->errors->impossible();
                            break;
                        }

                        $data = $this->model->workflowData($workflowID, array('id', 'client_id', 'worker_id', 'order_id'));
                        if (empty($data)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                        if ( ! in_array($userID, array($data['client_id'], $data['worker_id']))) {
                            $this->errors->impossible();
                            break;
                        }
                        $data['user'] = Users::model()->userData($userID, array('user_id', 'email', 'name', 'blocked'));
                        $isClient = $userID == $data['client_id'];
                        $data['type'] = $isClient ? _t('', 'Заказчик') :  _t('', 'Исполнитель');
                        $data['order'] = Orders::model()->orderData($data['order_id'], array('id', 'title', 'keyword', 'visibility'));
                        $data['documents'] = $this->model->docsListing(array(
                            ':t' => 'D.types & '.($isClient ? static::DOCS_TYPE_CLIENT : static::DOCS_TYPE_WORKER).' != 0'
                            ), array('id', 'name'));
                        $data['tpl'] = 'create';
                        echo $this->viewPHP($data, 'admin.workflow.form.tpl');
                    } while (false);
                    if ( ! $this->errors->no()) {
                        $errors = $this->errors->get();
                        echo('<div class="alert alert-error">'.join('<br />', $errors).'</div>');
                    }
                    exit;
                } break;
                case 'doc-add': # создание документа в пределах хода работ
                {
                    $workflowID = $this->input->post('workflow', TYPE_UINT);
                    if ( ! $workflowID) {
                        $this->errors->impossible();
                        break;
                    }
                    $userID = $this->input->post('user_id', TYPE_UINT);
                    if ( ! $userID) {
                        $this->errors->impossible();
                        break;
                    }
                    $documentID = $this->input->post('doc_id', TYPE_UINT);
                    if ( ! $documentID) {
                        $this->errors->impossible();
                        break;
                    }

                    if ( ! $this->workflowsDocs($workflowID)->makeSingle($userID, $documentID)) {
                        $this->errors->set(_t('fp', 'Ошибка формирования документа'));
                    }
                } break;
                default:
                    $response = false;
            }

            if ($response !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($response);
            }
        }

        # формируем фильтр списка заказов
        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'tab'     => TYPE_UINT,
            'title'   => TYPE_NOTAGS,
            'user'    => TYPE_NOTAGS,
        ));
        $sql = array();

        if ( ! $f['tab'] && $forceTab) {
            $f['tab'] = $forceTab;
        }

        switch ($f['tab']) {
            case static::TAB_WF_ALL: # Все
            {
            } break;
            case static::TAB_WF_PAYMENT: # Выплата сумм
            {
                $sql['status'] = static::STATUS_PAYMENT;
            } break;
            case static::TAB_WF_ARBITRAGE: # Арбитраж
            {
                $sql['arbitrage'] = 1;
            } break;
            case static::TAB_WF_RESERVATION: # Резервирование
            {
                $sql['status'] = static::STATUS_RESERVATION;
            } break;
            case static::TAB_WF_EXECUTING: # Выполняются
            {
                $sql['status'] = array(static::STATUS_EXECUTION, static::STATUS_REWORK);
            } break;
            case static::TAB_WF_CLOSED: # Закрытые
            {
                $sql['status'] = static::STATUS_CLOSED;
            } break;
            case static::TAB_WF_CANCELED: # Отменены
            {
                $sql['status'] = static::STATUS_CANCEL;
            } break;
        }

        if (!empty($f['title'])) {
            if (is_numeric($f['title'])) {
                $sql[':title'] = array('W.id = :id', ':id' => $f['title']);
            }
        }

        if ( ! empty($f['user'])) {
            if (is_numeric($f['user'])) {
                $sql[':juser'] = array('U.user_id = :user', ':user' => $f['user']);
            } else {
                $sql[':juser'] = array('(U.email LIKE :user OR U.login LIKE :user)', ':user' => '%' . $f['user'] . '%');
            }
        }

        $data['f'] = &$f;

        $nCount = $this->model->workflowListing($sql, true);
        $oPgn = new Pagination($nCount, 15, '#', 'jFairplayWorkflowsList.page('.Pagination::PAGE_ID.'); return false;');
        $oPgn->pageNeighbours = 6;
        $data['pgn'] = $oPgn->view(array('arrows'=>false));
        $data['list'] = $this->model->workflowListing($sql, false, $oPgn->getLimitOffset(), 'id DESC');
        $data['list'] = $this->viewPHP($data, 'admin.workflows.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $data['list'],
                    'pgn'  => $data['pgn'],
                )
            );
        }

        $data['id'] = $this->input->get('id', TYPE_UINT);
        $data['act'] = $act;
        return $this->viewPHP($data, 'admin.workflows.listing');
    }

    public function workflows_arbitrage()
    {
        return $this->workflows(static::TAB_WF_ARBITRAGE);
    }

    public function workflows_payment()
    {
        return $this->workflows(static::TAB_WF_PAYMENT);
    }

    #---------------------------------------------------------------------------------------
    # Фин. данные

    public function finances()
    {
        if (!($this->haveAccessTo('finances-manage'))) {
            return $this->showAccessDenied();
        }

        $act = $this->input->postget('act', TYPE_STR);
        if (!empty($act) || Request::isPOST()) {
            $response = array();
            switch ($act) {
                case 'edit': # редактирование
                {
                    $submit = $this->input->post('save', TYPE_BOOL);
                    $userID = $this->input->postget('id', TYPE_UINT);
                    if (!$userID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($submit) {

                        $data = $this->validateFinancesData($submit);
                        $data['status'] = $this->input->post('status', TYPE_UINT);

                        if ($data['status'] == static::VERIFIED_STATUS_DECLINED) {
                            $verifiedReason = $this->input->post('verified_reason', TYPE_ARRAY);
                            $reason = $this->input->clean_array($verifiedReason, array(
                                'id'        => TYPE_INT,
                                'message'   => TYPE_STR,
                                'cnt'       => TYPE_UINT,
                            ));
                            $data['verified_reason'] = serialize($reason);
                        } else {
                            $data['verified_reason'] = '';
                        }

                        $deleted = $this->input->post('verified_deleted', TYPE_ARRAY_STR);
                        if ( ! empty($deleted)) {
                            $images = $this->verifiedImages($userID);
                            $images->deleteImages($deleted);
                        }

                        $old = $this->model->financeData($userID);
                        # отправим уведомление пользователю
                        if ($old['status'] != $data['status']) {
                            if ($data['status'] == static::VERIFIED_STATUS_DECLINED) {
                                $reasonMessage = '';
                                if ($reason['id'] > 0) {
                                    $verifiedReasons = static::verifiedReasons();
                                    if (isset($verifiedReasons[ $reason['id'] ])) {
                                        $reasonMessage = $verifiedReasons[ $reason['id'] ]['t'];
                                    }
                                } else {
                                    if (isset($reason['message'])) {
                                        $reasonMessage = $reason['message'];
                                    }
                                }
                                Users::sendMailTemplateToUser($userID, 'finance_verified_declined', array(
                                    'reason' => $reasonMessage,
                                ));
                            } else if ($data['status'] == static::VERIFIED_STATUS_APPROVED) {
                                Users::sendMailTemplateToUser($userID, 'finance_verified_approved', array());
                            }
                        }

                        if ($this->errors->no()) {
                            $this->model->financeSave($userID, $data);

                            # обновим счетчик ожидающих проверки верификации
                            $this->verifiedCounterUpdate();
                        }
                    } else {
                        $data = $this->model->financeData($userID);
                        if (empty($data)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                        $data += Users::model()->userData($userID, array('email'));
                        $images = $this->verifiedImages($userID);
                        $data['verifiedImages'] = $images->getData();
                        foreach ($data['verifiedImages'] as &$v) {
                            $v['link'] = $this->adminLink(bff::$event).'&act=verified-image-view&id='.$userID.'&image='.$v['id'].'&file='.$v['filename'];
                        } unset($v);
                        $reason_default = array('id' => 0, 'message' => '', 'cnt' => 0);
                        if (isset($data['verified_reason'])) {
                            $data['verified_reason'] = func::unserialize($data['verified_reason'], $reason_default);
                        } else {
                            $data['verified_reason'] = $reason_default;
                        }
                        $data['act'] = $act;
                        $response['form'] = $this->viewPHP($data, 'admin.finance.form');
                    }
                }  break;
                case 'verified-image-view': # просмотр документа загруженного пользователем
                {
                    $userID = $this->input->postget('id', TYPE_UINT);
                    $image = $this->input->get('image', TYPE_UINT);
                    $images = $this->verifiedImages($userID);
                    $data = $images->getImageData($image);
                    if (empty($data)) {
                        break;
                    }
                    $images->show($data);
                } break;
                case 'info': # просмотр фин. данных пользователя
                {
                    $userID = $this->input->postget('id', TYPE_UINT);
                    if (!$userID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $data = $this->model->financeData($userID);
                    if (empty($data)) {
                        $this->errors->unknownRecord();
                        break;
                    }
                    $data += Users::model()->userData($userID, array('email'));
                    $response['html'] = $this->viewPHP($data, 'admin.finance.info');
                } break;
                default:
                    $response = false;
            }

            if ($response !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($response);
            }
        }

        # формируем фильтр списка заказов
        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'tab'     => TYPE_UINT,
            'q'    => TYPE_NOTAGS,
        ));
        $sql = array();

        switch ($f['tab']) {
            case 0: # ожидает решения модератора
            {
                $sql[':status'] = array('F.status = :status', ':status' => static::VERIFIED_STATUS_WAIT);
            } break;
            case 1: # Одобрены
            {
                $sql[':status'] = array('F.status = :status', ':status' => static::VERIFIED_STATUS_APPROVED);
            } break;
            case 2: # Отклонены
            {
                $sql[':status'] = array('F.status = :status', ':status' => static::VERIFIED_STATUS_DECLINED);
            } break;
            case 3: # Все
            {
            } break;
        }

        if ($f['q'] != '') {
            $sql[':q'] = array(
                '( U.user_id = :q_user_id OR U.login LIKE :q_login OR U.email LIKE :q_email )',
                ':q_user_id' => intval($f['q']),
                ':q_login'   => '%' . $f['q'] . '%',
                ':q_email'   => $f['q'] . '%',
            );
        }

        $data['f'] = &$f;

        $nCount = $this->model->financesListing($sql, true);
        $oPgn = new Pagination($nCount, 15, '#', 'jFairplayFinancesList.page('.Pagination::PAGE_ID.'); return false;');
        $oPgn->pageNeighbours = 6;
        $data['pgn'] = $oPgn->view(array('arrows'=>false));
        $data['list'] = $this->model->financesListing($sql, false, array('U.email', 'U.name', 'U.surname', 'F.status'), $oPgn->getLimitOffset(), 'user_id');
        $data['list'] = $this->viewPHP($data, 'admin.finances.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $data['list'],
                    'pgn'  => $data['pgn'],
                )
            );
        }

        $data['id'] = $this->input->get('id', TYPE_UINT);
        $data['act'] = $act;
        return $this->viewPHP($data, 'admin.finances.listing');
    }

    #---------------------------------------------------------------------------------------
    # Настройки
    
    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        $act = $this->input->postget('act', TYPE_STR);
        if (!empty($act) || Request::isPOST()) {
            $response = array();
            switch ($act) {
                case 'save': # сохранение настроек
                {
                    $lang = array(
                        'verified_requirement' => TYPE_STR,
                    );

                    $data = $this->input->postm(array(
                        'commissions' => TYPE_ARRAY,
                    ));


                    $commissions = array();
                    foreach ($data['commissions'] as $v) {
                        $this->input->clean_array($v, array(
                            'from' => TYPE_UINT,
                            'to' => TYPE_UINT,
                            'sum' => TYPE_NUM,
                            'p' => TYPE_UINT,
                        ));
                        if (!$v['from'] && !$v['to']) {
                            continue;
                        }
                        $commissions[] = $v;
                    }

                    $data['commissions'] = serialize($commissions);

                    $this->input->postm_lang($lang, $data);
                    $this->db->langFieldsModify($data, $lang, $data);

                    $this->configSave($data);
                } break;
                case 'documents': # список шаблонов документов
                {
                    $data = array();
                    $data['items'] = $this->model->docsListing(array(), array('id', 'name'));
                    if ( ! empty($data['items'])) {
                        $fst = array_keys($data['items']);
                        $fst = reset($fst);
                        $id = $data['items'][$fst]['id'];
                        $data['active'] = $id;
                        $item = $this->model->docData($id, array(), true);
                        $item['tpl'] = 'document.form';
                        $data['form'] = $this->viewPHP($item, 'admin.settings.tpl');
                    }
                    $data['tpl'] = 'documents';
                    $response['html'] = $this->viewPHP($data, 'admin.settings.tpl');
                } break;
                case 'document-form': # форма редактирования шаблона документа
                {
                    $id = $this->input->post('id', TYPE_UINT);
                    if ($id) {
                        $data = $this->model->docData($id, array(), true);
                        if (empty($data)) {
                            $this->errors->impossible();
                            break;
                        }
                    } else {
                        $data = $this->validateDocData(0, false);
                        $data['id'] = 0;
                    }
                    $data['tpl'] = 'document.form';
                    $response['html'] = $this->viewPHP($data, 'admin.settings.tpl');
                } break;
                case 'document-save': # сохранение шаблона документа
                {

                    $id = $this->input->post('id', TYPE_UINT);
                    $data = $this->validateDocData($id, true);
                    if ( ! $this->errors->no()) {
                        break;
                    }
                    $fn = func::generator(10);
                    $path = bff::path('tmp', 'images').$fn.'.pdf';
                    $params = array(
                        'dest' => 'F',
                        'filename' => $path,
                    );
                    $this->pdfGenerator()->output($data['content'], $params, false);
                    if ( ! file_exists($path)) {
                        $this->errors->set(_t('fp', 'Ошибка генерации PDF-документа'));
                    } else {
                        @unlink($path);
                    }
                    if ( ! $this->errors->no()) {
                        break;
                    }
                    $res = $this->model->docSave($id, $data);
                    if ($res) {
                        $response['msg'] = _t('fp', 'Документ сохранен');
                        if ( ! $id) {
                            $response['id'] = $res;
                            $response['name'] = $data['name'];
                        }
                    }
                } break;
                case 'document-delete': # удаление шаблона документа
                {

                    $id = $this->input->post('id', TYPE_UINT);
                    $this->model->docDelete($id);
                } break;
                case 'document-preview': # предварительный просмотр шаблона документа
                {
                    $content = $this->input->post('content', TYPE_STR);
                    $this->pdfGenerator()->output($content);
                    exit;
                } break;
            }

            if ($response !== false) {
                if (Request::isAJAX()) {
                    $this->ajaxResponseForm($response);
                }
                $this->iframeResponseForm($response);
            }
        }

        $data = $this->configLoad(array(
            'commissions' => '',
        ));
        $data['commissions'] = func::unserialize($data['commissions']);
        if (empty($data['commissions'])) {
            $data['commissions'] = array(array('from' => 0, 'to' => 0, 'sum' => '10', 'p' => 0));
        }

        return $this->viewPHP($data, 'admin.settings');
    }

    /**
     * Проверка данных шаблона документа
     * @param integer $docID ID документа
     * @param bool $submit сабмит формы
     * @return array data
     */
    protected function validateDocData($docID, $submit)
    {
        $data = array();
        $this->input->postm_lang($this->model->langDocs, $data);
        $params = array(
            'name'  => TYPE_STR,
            'event' => TYPE_UINT,
            'types' => TYPE_ARRAY_UINT,
            'content' => TYPE_STR,
            'file_prefix' => TYPE_STR,
        );
        $this->input->postm($params, $data);

        if ($submit) {
            $data['types'] = array_sum($data['types']);
            if (empty($data['name'])) {
                $this->errors->set(_t('fp', 'Укажите название'), 'name');
            }

            if ( ! array_key_exists($data['event'], static::docsEvents())) {
                $this->errors->impossible();
            }
        }
        return $data;
    }

    /**
     * Системные настройки
     * @param array $options
     * @return string
     */
    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }
    
    #---------------------------------------------------------------------------------------
    # Движение средств

    public function bills()
    {
        if ( ! $this->haveAccessTo('bills-manage')) {
            return $this->showAccessDenied();
        }

        $act = $this->input->postget('act', TYPE_STR);
        if (!empty($act) || Request::isPOST()) {
            $response = array();
            switch ($act) {
                case 'create': # создание счета движения средств
                {
                    $data = $this->input->postm(array(
                        'type'           => TYPE_UINT,
                        'workflow_id'    => TYPE_UINT,
                        'user_id'        => TYPE_UINT,
                        'sum'            => TYPE_UNUM,
                        'commission'     => TYPE_UNUM,
                        'descr_reserved' => TYPE_NOTAGS,
                        'descr_paid'     => TYPE_NOTAGS,
                    ));

                    switch ($data['type']) {
                        case static::BILL_TYPE_RESERVED:
                            $data['descr'] = $data['descr_reserved'];
                            break;
                        case static::BILL_TYPE_PAID:
                            $data['descr'] = $data['descr_paid'];
                            $data['commission'] = 0;
                            break;
                        default:
                            $this->errors->impossible();
                            break 2;
                    }
                    unset($data['descr_reserved'], $data['descr_paid']);

                    if (empty($data['workflow_id'])) {
                        $this->errors->impossible();
                        break;
                    }
                    if (empty($data['user_id'])) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($data['sum'] <= 0) {
                        $this->errors->impossible();
                        break;
                    }


                    $pay = $this->input->post('pay', TYPE_STR);
                    switch ($pay) {
                        case 'yandex':
                            $finance = $this->model->financeData($data['user_id']);
                            if (empty($finance['yandex'])) {
                                $this->errors->impossible();
                                break 2;
                            }
                            $wallet = $finance['yandex'];
                            if ($finance['status'] != static::VERIFIED_STATUS_APPROVED) {
                                $this->errors->set(_t('fp', 'Фин.данные пользователя не проверенны.'));
                                break 2;
                            }
                            $workflow = $this->model->workflowData($data['workflow_id'], array('order_id', 'status'));
                            if (empty($workflow)) {
                                $this->errors->impossible();
                                break 2;
                            }
                            $order = Orders::model()->orderData($workflow['order_id'], array('title', 'keyword'));
                            if (empty($order)) {
                                $this->errors->impossible();
                                break 2;
                            }

                            $api = $this->apiYandexMoney();
                            if ( ! $api->isPassEnter()) {
                                $response['pass_required'] = 1;
                                $this->errors->set(_t('', 'Необходим пароль для проведения платежей'));
                                break 2;
                            }

                            $payInfo = $api->pay($wallet, $data['sum'],
                                _t('fp', 'Выплата суммы ход работ [id]', array('id' => '#'.$data['workflow_id'])),
                                $data['descr']);

                            if ( ! empty($payInfo['success'])) {
                                $data['extra'] = array(
                                    'system' => 'Yandex.Money',
                                    'payment_id' => $payInfo['payment_id'],
                                    'wallet' => $wallet);
                            }
                        break;
                    }
                    if ( ! $this->errors->no()) {
                        break;
                    }

                    $workflowID = $data['workflow_id'];
                    $data['created_user_id'] = User::id();
                    $billID = $this->model->billSave(0, $data);
                    if ($billID) {
                        $response['reload'] = $this->setWorkflowsStatus($data['workflow_id'], 0);
                    }
                    $response['list'] = $this->workflowBillList($workflowID);
                } break;
                default:
                    $response = false;
            }

            if ($response !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($response);
            }
        }

        # формируем фильтр списка заказов
        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'tab'     => TYPE_UINT,
            'user'    => TYPE_NOTAGS,
            'title'   => TYPE_NOTAGS,
        ));
        $sql = array();

        switch ($f['tab']) {
            case 0: # Все
            {
            } break;
            case 1: # Резервирование
            {
                $sql['type'] = static::BILL_TYPE_RESERVED;
            }break;
            case 2: # Выплата
            {
                $sql['type'] = static::BILL_TYPE_PAID;
            }break;
        }

        if (!empty($f['user'])) {
            if (is_numeric($f['user'])) {
                $sql['user_id'] = $f['user'];
            } else {
                $sql[':juser'] = array('(U.email LIKE :user OR U.login LIKE :user)', ':user' => '%' . $f['user'] . '%');
            }
        }

        if (!empty($f['title'])) {
            if (is_numeric($f['title'])) {
                $sql[':title'] = array('B.id = :id', ':id' => $f['title']);
            }
        }


        $data['f'] = &$f;

        $nCount = $this->model->billsListing($sql, true);
        $oPgn = new Pagination($nCount, 15, '#', 'jFairplayBillsList.page('.Pagination::PAGE_ID.'); return false;');
        $oPgn->pageNeighbours = 6;
        $data['pgn'] = $oPgn->view(array('arrows'=>false));
        $data['list'] = $this->model->billsListing($sql, false, $oPgn->getLimitOffset(), 'id DESC');
        foreach ($data['list'] as & $v) {
            $this->billDescr($v);
        }unset($v);
        $data['list'] = $this->viewPHP($data, 'admin.bills.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $data['list'],
                    'pgn'  => $data['pgn'],
                )
            );
        }

        $data['id'] = $this->input->get('id', TYPE_UINT);
        $data['act'] = $act;
        return $this->viewPHP($data, 'admin.bills.listing');
    }

    /**
     * Список счетов движения средств в пределах хода работ
     * @param integer $workflowID ID хода работ
     * @return string HTML
     */
    protected function workflowBillList($workflowID)
    {
        $data = array();
        $data['list'] = $this->model->billsListing(array(
            'workflow_id' => $workflowID,
        ), false, '', 'B.created');
        foreach ($data['list'] as & $v) {
            $this->billDescr($v);
        } unset($v);
        return $this->viewPHP($data, 'admin.workflow.bill.list');
    }

    /**
     * Формирование описания движения средств
     * @param array $data @ref данные о движении средств
     */
    protected function billDescr(& $data)
    {
        $data['info'] = '';
        if ($data['bill_id']) {
            $id = '<a href="'.$this->adminLink('listing&id='.$data['bill_id'], 'bills').'" target="_blank">#'.$data['bill_id'].'</a>';
            if ($data['type'] == static::BILL_TYPE_RESERVED) {
                $data['info'] = _t('fp', 'зарезервировано автоматически, счет [id] [description]', array('id' => $id, 'description' => $data['descr']));
            } else if ($data['type'] == static::BILL_TYPE_PAID) {
                $data['info'] = _t('fp', 'выплачено автоматически, счет [id] [description]', array('id' => $id, 'description' => $data['descr']));
            }
        } else {
            if ($data['type'] == static::BILL_TYPE_RESERVED) {
                $data['info'] = _t('fp', 'зарезервировано вручную');
            } else if ($data['type'] == static::BILL_TYPE_PAID) {
                if (!empty($data['extra'])) {
                    $extra = func::unserialize($data['extra']);
                    $data['info'] = _t('fp', 'выплачено [system] #[transaction]', array('system' => $extra['system'], 'transaction' => $extra['payment_id']));
                } else {
                    $data['info'] = _t('fp', 'выплачено вручную');
                }
            }
        }
    }

    /**
     * Назначение статуса для хода работ
     * @param integer $workflowID ID хода работ
     * @param integer $status ID статуса
     * @return bool true - статус установлен
     */
    protected function setWorkflowsStatus($workflowID, $status)
    {
        $workflow = $this->model->workflowData($workflowID, array('status', 'price', 'order_id'));
        if (empty($workflow)) {
            $this->errors->impossible();
            return false;
        }
        $workflow = Catcher::getOrderPrepay($workflow);

        $result = false;
        $error = $status ? true : false;
        switch ($workflow['status'])
        {
            case static::STATUS_RESERVATION: # текущий статус - резервирование суммы
            {
                $sum = $this->model->billsDataByFilter(array(
                    'type' => static::BILL_TYPE_RESERVED,
                    'workflow_id' => $workflowID,
                ), array('SUM(sum) AS sum'), false);
                if ( ! empty($sum)) {
                    $sum = reset($sum);
                } else {
                    $sum = array('sum' => 0);
                }

                $setExecution = false;
                $price = (Catcher::isUseOrderPrepay() && isset($workflow['prepay']) && !empty($workflow['prepay']))?
                    $workflow['prepay'] :
                    (Catcher::isHoldMinPrepay() ? ($workflow['price'] - Catcher::MIN_PREPAY)  : $workflow['price']);
                if ($status == 0) {
                    if ($sum['sum'] >= $price) {
                        $setExecution = true;
                    }
                } else if ($status == static::STATUS_EXECUTION) { # новый статус - выполнение работы
                    if ($sum['sum'] < $price) {
                        $this->errors->set(_t('fp', 'Зарезервированная сумма меньше бюджета'));
                        return false;
                    }
                    $setExecution = true;
                } else if ($status == static::STATUS_CANCEL) { # новый статус - отмена заказа
                    $this->model->workflowSave($workflowID, array(
                        'status' => static::STATUS_CANCEL,
                    ));
                    $error = false;
                    $result = true;
                }
                if ($setExecution) {
                    $this->model->workflowSave($workflowID, array(
                        'status' => static::STATUS_EXECUTION,
                    ));
                    $this->onReserved($workflowID);
                    $error = false;
                    $result = true;
                }
            } break;
            case static::STATUS_PAYMENT: # текущий статус - выплата суммы
            {
                $sum = $this->model->billsDataByFilter(array(
                    'type' => static::BILL_TYPE_PAID,
                    'workflow_id' => $workflowID,
                ), array('SUM(sum) AS sum'), false);
                if ( ! empty($sum)) {
                    $sum = reset($sum);
                    $sum = $sum['sum'];
                } else {
                    $sum = 0;
                }

                $setClosed = false;

                $price = (Catcher::isUseOrderPrepay() && isset($workflow['prepay']) && !empty($workflow['prepay']))?
                            $workflow['prepay'] :
                            (Catcher::isHoldMinPrepay() ? ($workflow['price'] - Catcher::MIN_PREPAY)  : $workflow['price']);
                if ($status == 0) {
                    if ($sum >= $price) {
                        $setClosed = true;
                    }
                } else if ($status == static::STATUS_CLOSED) { # новый статус - заказ закрыт
                    if ($sum < $price) {
                        $this->errors->set(_t('fp', 'Выплаченная сумма меньше бюджета'));
                        return false;
                    }
                    $setClosed = true;
                } else if ($status == static::STATUS_CANCEL) { # новый статус - отмена заказа
                    $this->model->workflowSave($workflowID, array(
                        'status' => static::STATUS_CANCEL,
                    ));
                    $error = false;
                    $result = true;
                }
                if ($setClosed) {
                    $this->model->workflowSave($workflowID, array(
                        'status' => static::STATUS_CLOSED,
                    ));
                    $this->workflowsDocs($workflowID)->fireEvent(static::DOCS_EVENT_PAID);
                    $this->model->historyAdd($workflowID, array(
                        'type'    => static::HISTORY_TYPE_PAID,
                        'user_id' => User::id(),
                        'extra'   => array('sum' => $sum),
                    ));
                    $error = false;
                    $result = true;
                }
            } break;
        }

        $this->paymentCounterUpdate();

        if ($error) {
            $this->errors->impossible();
        }
        return $result;
    }

    /**
     * Расчет счетчиков для шапки в админ. панели
     * @param integer $cnt @ref - сумма всех счетчиков для заголовка
     * @return array
     */
    public function adminHeaderCounters(& $cnt)
    {
        if (!$this->security->haveAccessToAdminPanel()) {
            $cnt = 0;
            return array();
        }
        $arbitrage = config::get('fairplay_arbitrage', 0, TYPE_UINT);
        $payment   = config::get('fairplay_payment', 0, TYPE_UINT);
        $finances  = config::get('fairplay_verified_waiting', 0, TYPE_UINT);
        $cnt = $arbitrage + $payment + $finances;
        return array(
            array(
                't'   => _t('fp', 'Арбитраж'),
                'url' => $this->adminLink('workflows_arbitrage'),
                'cnt' => $arbitrage,
             ),
            array(
                't'   => _t('fp', 'Выплата'),
                'url' => $this->adminLink('workflows_payment'),
                'cnt' => $payment,
            ),
            array(
                't'   => _t('fp', 'Фин. данные'),
                'url' => $this->adminLink('finances'),
                'cnt' => $finances,
            ),
        );
    }

    #---------------------------------------------------------------------------------------
    # Платежные системы

    public function pay_systems()
    {
        if ( ! $this->haveAccessTo('bills-manage')) {
            return $this->showAccessDenied();
        }

        $act = $this->input->postget('act', TYPE_STR);
        if (!empty($act) || Request::isPOST()) {
            $response = array();
            switch ($act) {
                case 'yandex-money-info': # Данные о кошельке (сумма денег в кошельке)
                {
                    $data = $this->apiYandexMoney()->info();
                    $data['pass'] = $this->input->get('pass', TYPE_STR);
                    return $this->viewPHP($data, 'admin.yandex.money.info');
                } break;
                case 'yandex-money-is-pay': # проверка возможности вывода средств пользователю
                {
                    $userID = $this->input->post('user', TYPE_UINT);
                    if (!$userID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $data = $this->model->financeData($userID);
                    if (empty($data)) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $response['allowed'] = 0;
                    if ($data['status'] != static::VERIFIED_STATUS_APPROVED) {
                        $response['msg'] = _t('fp', 'Не одобренны фин. данные пользователя');
                        break;
                    }

                    if (empty($data['yandex'])) {
                        $response['msg'] = _t('fp', 'Не указан кошелек Yandex.Money');
                        break;
                    }
                    $response['msg'] = _t('fp', 'Выплата на кошелек Yandex.Money: <b>[wallet]</b>', array('wallet' => $data['yandex']));
                    $response['allowed'] = 1;
                } break;
                case 'yandex-money-token-delete': # Удалить токена Yandex.Money (Деинсталяция приложения)
                {
                    if ( ! FORDEV) {
                        $this->errors->impossible();
                    }

                    $this->apiYandexMoney()->tokenDelete();
                } break;
                case 'yandex-money-pass-popup': # Окно для ввода пароля для проведения платежей
                {
                    $data = array(
                        'onSuccess' => $this->input->get('onSuccess', TYPE_STR),
                    );
                    echo $this->viewPHP($data, 'admin.yandex.money.pass');
                    exit;
                } break;
                case 'yandex-money-pass': # ввода пароля для проведения платежей
                {
                    $pass = $this->input->post('pass', TYPE_STR);
                    if (empty($pass)) {
                        $this->errors->set(_t('', 'Не верный пароль'));
                        break;
                    }
                    $this->apiYandexMoney()->checkPass($pass);
                } break;
                case 'yandex-money-pass-change': # изменение пароля для проведения платежей
                {
                    $pass = $this->input->post('pass', TYPE_STR);
                    if (empty($pass)) {
                        $this->errors->set(_t('', 'Не указан пароль'));
                        break;
                    }
                    $api = $this->apiYandexMoney();
                    $data = $api->info();
                    if ( ! isset($data['balance'])) {
                        $this->errors->set(_t('', 'Необходима авторизация Yandex.Money'));
                        break;
                    }
                    $api->changePass($pass);
                } break;
                case 'yandex-money-pass-forget': # забыть пароль для проведения платежей
                {
                    $this->apiYandexMoney()->forgetPass();
                } break;
                default:
                {
                    $response = false;
                }
            }

            if ($response !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($response);
            }
        }

        $this->redirect($this->adminLink(bff::$event.'&act=yandex-money-info'));
        return '';
    }
}