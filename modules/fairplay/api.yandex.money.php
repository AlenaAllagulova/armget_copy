<?php
/*
 * Класс работы с Yandex.Money API
 *
 * Привязка системы к API Yandex.Money
 * 1.  https://tech.yandex.ru/money/apps/
 *     Зарегистрируйте своё приложение в API Яндекс.Денег.
 *     https://money.yandex.ru/myservices/new.xml
 *     Адрес сайта - http://example.com
 *     Redirect URI - http://example.com/oauth/yandex
 *     Ставим чекет "Использовать проверку подлинности приложения (OAuth2 client_secret)"
 * 2.  Идентификатор приложения сохраняем в системную настройку 'fairplay.yandex.money.client_id'
 *     OAuth2 client_secret сохраняем в системную настройку 'fairplay.yandex.money.client_secret'
 * 3.  Необходимо получить токен Yandex.Money. В админ панели Безопасная сделка / Платежные системы / Yandex.Money
 *     ( http://example.com/admin/index.php?s=fairplay&ev=pay_systems&act=yandex-money-info)
 *     нажимаем "авторизоваться". Проходим процедуру авторизации Yandex.Money в открывшемся окне.
 * 4.  После завершения авторизации полученный токен будет зашифрован с помошью пароля для платежей.
 *     Пароль будет отображен только однажды. Его необходимо запомнить или изменить.
 * 
 *  https://github.com/yandex-money/yandex-money-sdk-php/
 */

class ApiYandexMoney_
{
    /** @var \Errors object */
    public $errors = null;
    /** @var \bff\base\Input object */
    public $input = null;

    protected $client_id;   # Идентификатор приложения Yandex.Money
    protected $client_secret; # OAuth2 secret Yandex.Money

    # https://tech.yandex.ru/money/doc/dg/concepts/protocol-rights-docpage/
    protected $scope; # права доступа приложения в системе  Yandex.Money

    const TOKEN = 'fairplay.yandex.money.token'; # имя для хранения токена в системных настройках
    const PASS_COOKIE = 'xhv9a';

    function __construct()
    {
        $this->client_id     = config::sys('fairplay.yandex.money.client_id', '', TYPE_STR);
        $this->client_secret = config::sys('fairplay.yandex.money.client_secret', '', TYPE_STR);
        $this->scope = array('account-info','operation-history','operation-details','payment-p2p','money-source("wallet","card")');

        $this->errors = bff::DI('errors');
        $this->input = bff::DI('input');

        require_once modification(PATH_MODULES.'fairplay/api/yandex/api.php');
    }

    /**
     * URL для установки приложения
     * @return string
     */
    protected static function redirect_uri()
    {
        return SITEURL.'/oauth/yandex/';
    }

    /**
     * Преобразование объекта в ассоциативный массив
     * @param object $object
     * @return array
     */
    public static function object2Array($object)
    {
        return json_decode(json_encode($object), true);
    }

    /**
     * URL для регистрации приложения в системе Yandex.Money
     * @return string|\YandexMoney\response
     */
    public function authUrl()
    {
        if (empty($this->client_id)) return '';

        return \YandexMoney\API::buildObtainTokenUrl($this->client_id, static::redirect_uri(), $this->scope);
    }

    /**
     * Сохранение и шифрование токена для доступа к системе Yandex.Money
     * @param $redirectUrl
     */
    public function saveAccessToken($redirectUrl)
    {
        do {
            if (empty($this->client_id)) break;
            if (empty($this->client_secret)) break;

            $code = $this->input->get('code', TYPE_STR);
            if (empty($code)) break;

            try {
                $access_token_response = \YandexMoney\API::getAccessToken($this->client_id, $code, static::redirect_uri(), $this->client_secret);
            } catch(Exception $e) {
                bff::log('Exception ' . __CLASS__ . '::' . __FUNCTION__ . ' ' . $e->getMessage());
                break;
            }
            if (empty($access_token_response)) {
                bff::log(__CLASS__ . '::' . __FUNCTION__ . ' no answer');
                break;
            }
            if (property_exists($access_token_response, "error")) {
                bff::log(__CLASS__ . '::' . __FUNCTION__ . ' ' . $access_token_response->error);
                break;
            }

            $access_token = $access_token_response->access_token;
            $pass = func::generator(4);
            $access_token = static::encrypt($access_token, $pass);
            config::save(static::TOKEN, $access_token);
            $redirectUrl .= '&pass='.$pass;
            $this->setPass($pass);
        } while (false);

        $this->redirectFromPopup($redirectUrl);
    }

    /**
     * Получение сохраненного токена
     * @param bool $pass
     * @return string
     */
    public function getSavedToken($pass = false)
    {
        if (empty($pass)) {
            $pass = $this->input->cookie(static::PASS_COOKIE, TYPE_STR);
            if (empty($pass)) {
                return '';
            }
        }
        $token = config::get(static::TOKEN, '', TYPE_STR);
        if (empty($token) || empty($this->client_id)) {
            return '';
        }
        return static::decrypt($token, $pass);
    }

    /**
     * Запомнить пароль для расшифровки токена, записать в куку
     * @param $pass
     */
    public function setPass($pass)
    {
        $expire = time() + 30*60;
        setcookie(static::PASS_COOKIE, $pass, $expire, '/', SITEHOST, false, true);
    }

    /**
     * Закрытие модального окна авторизации и редирект главного окна
     * @param bool $mURL
     */
    public function redirectFromPopup($mURL = false)
    {
        if ($mURL === false) {
            $mURL = bff::urlBase();
        }
        $mURL = addslashes($mURL);
        echo '<!DOCTYPE html>
            <html>
              <head>
                <script type="text/javascript">
                if (window.opener) {
                    window.close();
                    ' . (!empty($mURL) ? ' window.opener.location = \'' . $mURL . '\'; ' : '') . '
                } else {
                    window.location = \'' . $mURL . '\';
                }
                </script>
              </head>
              <body></body>
            </html>';
        exit;
    }

    /**
     * Удаление токена (Деинсталяция приложения Yandex.Money)
     */
    public function tokenDelete()
    {
        $token = $this->getSavedToken();
        if ( ! empty($token)) {
            try {
                \YandexMoney\API::revokeToken($token);
            } catch(Exception $e) {
                bff::log('Exception '.__CLASS__.'::'.__FUNCTION__.' '.$e->getMessage());
                $this->errors->set($e->getMessage());
            }
        }
        config::save(static::TOKEN, false);
    }

    /**
     * Оплата перечисление денег на другой кошелек Yandex.Money
     * @param string $wallet номер кошелька
     * @param float $amount сумма
     * @param string $comment примечание, сохраняется в истории отправителя
     * @param string $message сообщение для получателя
     * @return array|bool данные о платеже
     */
    public function pay($wallet, $amount, $comment = '', $message = '')
    {
        # http://api.yandex.com/money/doc/dg/reference/request-payment.xml
        if (empty($wallet)) {
            $this->errors->set(_t('yandex.money', 'Не указан кошелек'));
            return false;
        }
        if ($amount <= 0) {
            $this->errors->set(_t('yandex.money', 'Не указана сумма'));
            return false;
        }

        $token = $this->getSavedToken();
        if (empty($token)) {
            $this->errors->set(_t('yandex.money', 'Не настроены параметры платежной системы'));
            return false;
        }
        $result = array();
        $api = new \YandexMoney\API($token);
        $data = array(
            'pattern_id' => 'p2p',
            'to'         => $wallet,
            'amount'     => $amount,
            'comment'    => $comment,
            'message'    => $message,
         // 'label'      => $label,
        );

        try {
            $request = $api->requestPayment($data);
            if (isset($request->status) && $request->status == 'success') {
                $process = $api->processPayment(array(
                    'request_id' => $request->request_id,
                ));
                if (isset($process->status) && $process->status == 'success') {
                    $result['success'] = true;
                    $result['payment_id'] = $process->payment_id;
                } else {
                    $process = static::object2Array($process);
                    bff::log($data, 'yandex.log');
                    bff::log($process, 'yandex.log');
                    $this->errors->set(_t('yandex.money', 'При проведении платежа произошла ошибка.'));
                }
            } else {
                $request = static::object2Array($request);
                bff::log($data, 'yandex.log');
                bff::log($request, 'yandex.log');
                $this->errors->set(_t('yandex.money', 'При проведении платежа произошла ошибка.'));
            }
        } catch (Exception $e) {
            bff::log('Exception '.__CLASS__.'::'.__FUNCTION__.' '.$e->getMessage());
            $this->errors->set($e->getMessage());
        }

        return $result;
    }

    /**
     * Сумма доступных денег в кошельке
     * @return array
     */
    public function info()
    {
        $result = array();
        do{
            $token = config::get(static::TOKEN, '', TYPE_STR);
            if (empty($token) || empty($this->client_id)) {
                $result['token_required'] = $this->authUrl();
                break;
            }
            # http://api.yandex.com/money/doc/dg/reference/request-payment.xml
            $token = $this->getSavedToken();
            if (empty($token)) {
                $result['pass_required'] = true;
                break;
            }
            $api = new \YandexMoney\API($token);
            $result = array();
            try {
                $result = $api->accountInfo();
            } catch(Exception $e) {
                bff::log('Exception '.__CLASS__.'::'.__FUNCTION__.' '.$e->getMessage());
                $this->errors->set($e->getMessage());
            }
            $result = static::object2Array($result);
        } while (false);
        return $result;
    }

    /**
     * Проверка пароля для расшифровки токена
     * @param string $pass пароль
     * @return bool true - правильный пароль
     */
    public function checkPass($pass)
    {
        if (empty($pass)) {
            $this->errors->set(_t('', 'Неверный пароль'));
            return false;
        }
        $token = config::get(static::TOKEN, '', TYPE_STR);
        if (empty($token) || empty($this->client_id)) {
            $this->errors->set(_t('yandex.money', 'Не настроены параметры платежной системы'));
            return false;
        }
        $token = $this->getSavedToken($pass);
        if (empty($token)) {
            $this->errors->set(_t('', 'Неверный пароль'));
            return false;
        }
        $api = new \YandexMoney\API($token);
        try {
            $result = $api->accountInfo();
        } catch(Exception $e) {
            $this->errors->set(_t('', 'Неверный пароль'));
            return false;
        }
        if (isset($result->balance)) {
            $this->setPass($pass);
        }
        return true;
    }

    /**
     * Изменение пароля для расшифровки токена
     * @param string $pass новый пароль
     * @return bool
     */
    public function changePass($pass)
    {
        if (empty($pass)) {
            $this->errors->set(_t('', 'Неверный пароль'));
            return false;
        }
        $token = $this->getSavedToken();
        if (empty($token)) {
            $this->errors->set(_t('', 'Необходима авторизация Yandex.Money'));
            return false;
        }
        $token = static::encrypt($token, $pass);
        config::save(static::TOKEN, $token);
        $this->setPass($pass);
        return true;
    }

    /**
     * Забыть пароль для расшифровки токена (удалить куку)
     */
    public function forgetPass()
    {
        $expire = time() - 3600;
        setcookie(static::PASS_COOKIE, '', $expire, '/', SITEHOST, false, true);
    }

    /**
     * Проверим сохранен ли пароль для расшифровки токена
     * @return bool
     */
    public function isPassEnter()
    {
        $pass = $this->input->cookie(static::PASS_COOKIE, TYPE_STR);
        return ! empty($pass);
    }

    /**
     * Зашифровать строку с помощью ключа
     * @param string $data строка для шифрования
     * @param string $key ключ шифрования
     * @return string зашифрованная строка
     */
    public static function encrypt($data, $key)
    {
        $key = md5($key);
        return openssl_encrypt($data, 'aes-256-cbc', mb_strcut($key, 0, 32), false, mb_strcut($key, 0, 16));
    }

    /**
     * Расшифровать строку с помощью ключа
     * @param string $data зашифрованная строка
     * @param string $key ключ шифрования
     * @return string расшифрованная строка
     */
    public static function decrypt($data, $key)
    {
        $key = md5($key);
        return openssl_decrypt($data, 'aes-256-cbc', mb_strcut($key, 0, 32), false, mb_strcut($key, 0, 16));
    }

}