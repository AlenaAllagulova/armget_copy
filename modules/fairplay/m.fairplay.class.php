<?php

class M_Fairplay_
{
    static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $module = 'fairplay';
        $name = _t('fp','Безопасная сделка');

        # Ходы работ
        if ($security->haveAccessToModuleToMethod($module, 'workflows') ||
            $security->haveAccessToModuleToMethod($module, 'workflows-moderate')) {
            $menu->assign($name, _t('fp','Ход работ'), $module, 'workflows', true, 10);
            $menu->assign($name, _t('fp','Арбитраж'), $module, 'workflows_arbitrage', true, 20, array('counter' => 'fairplay_arbitrage'));
            $menu->assign($name, _t('fp','Выплата'), $module, 'workflows_payment', true, 30, array('counter' => 'fairplay_payment'));
        }

        # Фин. данные
        if ($security->haveAccessToModuleToMethod($module, 'finances')) {
            $menu->assign($name, _t('fp','Фин. данные'), $module, 'finances', true, 40,
                array('counter' => 'fairplay_verified_waiting')
            );
        }

        # Резервирование
        if ($security->haveAccessToModuleToMethod($module, 'bills')) {
            $menu->assign($name, _t('fp','Движение средств'), $module, 'bills', true, 50);
        }

        # Настройки
        if ($security->haveAccessToModuleToMethod($module, 'settings')) {
            $menu->assign($name, _t('','Settings'), $module, 'settings', true, 60);
            $menu->assign($name, _t('fp','Платежные системы'), $module, 'pay_systems', true, 70);
        }
    }
}