<?php

abstract class FairplayBase_ extends Module
    implements IModuleWithSvc
{
    /** @var FairplayModel */
    var $model = null;
    var $securityKey = '1ea6e26ed1a7bbb35aed713a826e5024';

    # Статус хода работ
    const STATUS_NEGOTIATION = 1; # обсуждение условий
    const STATUS_RESERVATION = 2; # резервирование суммы
    const STATUS_EXECUTION   = 3; # выполнение работы
    const STATUS_CLOSED      = 4; # заказ закрыт
    const STATUS_CANCEL      = 5; # отмена заказа
    const STATUS_PAYMENT     = 6; # выплата суммы
    const STATUS_REWORK      = 7; # доработка

    # Безопасная сделка
    const FAIRPLAY_NONE = 0; # не использовать
    const FAIRPLAY_USE  = 1; # использовать

    # ID Услуг
    const SVC_RESERVED  = 2048;  # резервирование

    # Верификация пользователей
    const VERIFIED_STATUS_NONE     = 0; # проверка не начата
    const VERIFIED_STATUS_WAIT     = 1; # документы загружены, ожидает решения модератора
    const VERIFIED_STATUS_DECLINED = 2; # модератор отклонил проверку
    const VERIFIED_STATUS_APPROVED = 4; # проверенный пользователь

    const VERIFIED_REASON_OTHER = -1; # другая причина отклонения

    # Типы счетов
    const BILL_TYPE_RESERVED    = 1; # платеж зарезервирован (получен)
    const BILL_TYPE_PAID        = 2; # платеж выплачен

    # Типы событий истории хода работ
    const HISTORY_TYPE_FAIRPLAY = 1; # переключение типа оплаты на Безопасную сделку
    const HISTORY_TYPE_RESERVED = 2; # Средства зарезервированны
    const HISTORY_TYPE_PAID     = 5; # Средства выплачены
    const HISTORY_TYPE_COMPLETE = 3; # Работа выполнена
    const HISTORY_TYPE_REWORK   = 4; # Вернули на доработку
    const HISTORY_TYPE_ARBITRAGE_START = 6; # Обращение в арбитраж
    const HISTORY_TYPE_ARBITRAGE_STOP  = 7; # Арбитраж завершен
    const HISTORY_TYPE_OPINION_CLIENT  = 8; # Добавлен отзыв от заказчика
    const HISTORY_TYPE_OPINION_WORKER  = 9; # Добавлен отзыв от исполнителя

    # Типы уведомлений
    const NOTIFICATIONS_TYPE_CHAT    = 1; # новое сообщение в чате
    const NOTIFICATIONS_TYPE_HISTORY = 2; # новое событие в истории

    # Типы сообщений в чате
    const CHAT_TYPE_MESSAGE         = 1; # Текстовое сообщение
    const CHAT_TYPE_START           = 2; # Начало работы
    const CHAT_TYPE_COMPLETE        = 3; # Работа выполнена
    const CHAT_TYPE_REWORK          = 4; # Вернули на доработку
    const CHAT_TYPE_ARBITRAGE_START = 6; # Обращение в арбитраж
    const CHAT_TYPE_ARBITRAGE_STOP  = 7; # Арбитраж завершен

    # События для создания документов
    const DOCS_EVENT_CONFIRM  = 1; # исполнитель подтвердил заказ
    const DOCS_EVENT_RESERVED = 2; # заказчик зарезервировал сумму
    const DOCS_EVENT_COMPLETE = 3; # заказчик принял работу (закрыл заказ, оставил отзыв)
    const DOCS_EVENT_PAID     = 4; # средства выплачены испонителю

    # Роль пользователя для кого создавать документ
    const DOCS_TYPE_WORKER = 1;
    const DOCS_TYPE_CLIENT = 2;

    /**
     * @return Fairplay
     */
    public static function i()
    {
        return bff::module('Fairplay');
    }

    /**
     * @return FairplayModel
     */
    public static function model()
    {
        return bff::model('Fairplay');
    }

    public function init()
    {
        parent::init();
        
        $this->module_title = _t('fp', 'Безопасная сделка');

        bff::autoloadEx(array(
            'WorkflowsChat'            => array('app', 'modules/fairplay/workflows.chat.php'),
            'FairplayVerifiedImages'   => array('app', 'modules/fairplay/verified.images.php'),
            'FairplayWorkflowsDocs'    => array('app', 'modules/fairplay/workflows.docs.php'),
            'ApiYandexMoney'           => array('app', 'modules/fairplay/api.yandex.money.php'),
            'PdfGenerator'             => array('app', 'app/pdf.php'),
        ));
    }

    /**
     * Почтовые шаблоны
     * @return array
     */
    public function sendmailTemplates()
    {
        $aTemplates = array(
            'finance_verified_approved' => array(
                'title'       => _t('fp','Фин. данные: Уведомление об одобрении верификации'),
                'description' => _t('fp','Уведомление, отправляемое <u>пользователю</u> при одобрении фин. данных модератором'),
                'vars'        => array(
                    '{fio}'   => _t('','ФИО пользователя'),
                    '{name}'  => _t('','Имя пользователя'),
                    '{email}' => _t('','Email'),
                ),
                'impl'        => true,
                'priority'    => 1,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'finance_verified_declined' => array(
                'title'       => _t('fp','Фин. данные: Уведомление об отклонении верификации'),
                'description' => _t('fp','Уведомление, отправляемое <u>пользователю</u> при отклонении фин. данных модератором'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{reason}' => _t('fp','Причина'),
                ),
                'impl'        => true,
                'priority'    => 2,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'workflow_money_reserved' => array(
                'title'       => _t('fp','Ход работ: Уведомление о резервировании средств'),
                'description' => _t('fp','Уведомление, отправляемое <u>исполнителю</u> при получении средств от заказчика'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{workflow_id}'   => _t('fp','ID хода работ'),
                    '{order_title}'   => _t('fp','Заголовок заказа'),
                    '{workflow_url}'  => _t('fp','Ссылка для просмотра хода работ'),
                    '{sum}'  => _t('fp','Зарезервированая сумма'),
                ),
                'impl'        => true,
                'priority'    => 3,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'workflow_expert_assigned' => array(
                'title'       => _t('fp','Ход работ: Назначение пользователя экспертом'),
                'description' => _t('fp','Уведомление, отправляемое <u>пользователю</u> при назначении его экспертом в ходе работ'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{workflow_id}'   => _t('fp','ID хода работ'),
                    '{order_title}'   => _t('fp','Заголовок заказа'),
                    '{workflow_url}'  => _t('fp','Ссылка для просмотра хода работ'),
                ),
                'impl'        => true,
                'priority'    => 4,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'workflow_chat_new' => array(
                'title'       => _t('fp','Ход работ: Уведомление о новом сообщении в чате'),
                'description' => _t('fp','Уведомление, отправляемое <u>пользователю</u> при добавлении нового сообщения в чат'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{workflows_list}' => _t('fp','Список договоров'),
                ),
                'impl'        => true,
                'priority'    => 5,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'workflow_complete' => array(
                'title'       => _t('fp','Ход работ: Уведомление о выполнении работы'),
                'description' => _t('fp','Уведомление, отправляемое <u>заказчику</u> при нажании исполнителем кнопки "Работа выполнена"'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{workflow_id}'   => _t('fp','ID хода работ'),
                    '{order_title}'   => _t('fp','Заголовок заказа'),
                    '{workflow_url}'  => _t('fp','Ссылка для просмотра хода работ'),
                    '{message}' => _t('fp','Текст сообщения (необязательно)'),
                ),
                'impl'        => true,
                'priority'    => 6,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'workflow_continue' => array(
                'title'       => _t('fp','Ход работ: Уведомление о продожении работы'),
                'description' => _t('fp','Уведомление, отправляемое <u>исполнителю</u> при нажании заказчиком кнопки "Вернуть заказ в работу"'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{workflow_id}'   => _t('fp','ID хода работ'),
                    '{order_title}'   => _t('fp','Заголовок заказа'),
                    '{workflow_url}'  => _t('fp','Ссылка для просмотра хода работ'),
                    '{message}' => _t('fp','Текст сообщения'),
                ),
                'impl'        => true,
                'priority'    => 7,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'workflow_close' => array(
                'title'       => _t('fp','Ход работ: Уведомление о завершении работы'),
                'description' => _t('fp','Уведомление, отправляемое <u>исполнителю</u> при нажании заказчиком кнопки "Завершить заказ"'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{workflow_id}'   => _t('fp','ID хода работ'),
                    '{order_title}'   => _t('fp','Заголовок заказа'),
                    '{workflow_url}'  => _t('fp','Ссылка для просмотра хода работ'),
                    '{opinion_type}'  => _t('fp','Тип отзыва (положительный, отрицательный, нейтральный)'),
                ),
                'impl'        => true,
                'priority'    => 8,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
            'worker_opinion_add' => array(
                'title'       => _t('fp','Ход работ: Добавление отзыва исполнителем'),
                'description' => _t('fp','Уведомление, отправляемое <u>заказчику</u> при добавлении отзыва исполнителем'),
                'vars'        => array(
                    '{fio}'    => _t('','ФИО пользователя'),
                    '{name}'   => _t('','Имя пользователя'),
                    '{email}'  => _t('','Email'),
                    '{workflow_id}'   => _t('fp','ID хода работ'),
                    '{order_title}'   => _t('fp','Заголовок заказа'),
                    '{workflow_url}'  => _t('fp','Ссылка для просмотра хода работ'),
                    '{opinion_type}'  => _t('fp','Тип отзыва (положительный, отрицательный, нейтральный)'),
                ),
                'impl'        => true,
                'priority'    => 9,
                'enotify'     => 0,
                'group'       => 'fairplay',
            ),
        );

        Sendmail::i()->addTemplateGroup('fairplay', 'Безопасная сделка', 5);
        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Информация при добавлении заказа
            case 'info.orders.add':
                # /
                break;
            # Список моих ход работ
            case 'my.workflow':
                $url = Users::url('profile', array('login'=>User::data('login'), 'tab'=>'workflows'), $dynamic);
                $url .= static::urlQuery($opts, array('login'));
                break;
            # Просмотр хода работ
            case 'view':
                $url .= '/workflows/' . $opts['id'] . '-' . $opts['keyword'] .'.html'.
                    static::urlQuery($opts, array('id', 'keyword'));
                break;
            # Скачивание документа
            case 'document':
                $url .= '/doc/';
                break;
        }
        return bff::filter('fairplay.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Создание хода работ
     * @param integer $orderID ID заказа
     * @return integer|bool
     */
    public function workflowCreate($orderID)
    {
        if ( ! $orderID) return false;

        $order = Orders::model()->orderData($orderID, array('user_id', 'performer_id', 'fairplay'));
        if (empty($order['performer_id'])) return false;

        $offer = Orders::model()->offersDataByFilter(array(
            'order_id' => $orderID,
            'user_id'  => $order['performer_id'],
        ), array('id', 'workflow'));
        if (empty($offer['workflow'])) return false;

        $save = func::unserialize($offer['workflow']);

        $status = static::STATUS_EXECUTION;
        if ($save['fairplay'] == static::FAIRPLAY_USE) {
            $status = static::STATUS_RESERVATION;
        }
        $save += array(
            'order_id'  => $orderID,
            'client_id' => $order['user_id'],
            'worker_id' => $order['performer_id'],
            'offer_id'  => $offer['id'],
            'status'    => $status,
        );
        $workflowID = $this->model->workflowSave(0, $save);
        if ( ! $workflowID) return false;

        if ($status == static::STATUS_RESERVATION) {
            $this->workflowsDocs($workflowID)->fireEvent(static::DOCS_EVENT_CONFIRM);
        }

        # Откроем исполнителю и заказчику в кабинете таб ход работ
        $users = array($order['user_id'] => 1, $order['performer_id'] => 1);
        $userID = User::id();
        if (array_key_exists($userID, $users)) {
            unset($users[$userID]);
            if ( ! User::data('workflow')) {
                Users::model()->userSave($userID, array('workflow' => 1));
                $this->security->expire();
            }
        }
        foreach ($users as $u => $v) {
            $user = Users::model()->userData($u, array('workflow'));
            if ( ! $user['workflow']) {
                Users::model()->userSave($u, array('workflow' => 1));
            }
        }

        # Копируем чат из заявки в ход работ
        $offerChat = Orders::model()->chatData($offer['id']);
        $chat = array();
        foreach ($offerChat as $v) {
            $s = array();
            foreach (array('author_id', 'created', 'message') as $f) {
                if (isset($v[$f])) {
                    $s[$f] = $v[$f];
                }
            }
            if (empty($s)) continue;
            $chat[] = $s;
        }
        if ( ! empty($chat)) {
            $this->model->workflowChatMultiSave($workflowID, $chat);
        }
        $this->workflowChat($workflowID)->append(array(
            'type' => static::CHAT_TYPE_START
        ));
        return $workflowID;
    }

    /**
     * Инициализация компонента WorkflowsChat
     * @param integer $workflowID ID хода работ
     * @return WorkflowsChat component
     */
    public function workflowChat($workflowID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new WorkflowsChat();
        }
        $i->setRecordID($workflowID);
        return $i;
    }

    /**
     * Шаблон для срока в ходе работ
     * @param integer $term срок в днях
     * @return string
     */
    public static function tplTerm($term)
    {
        return bff::filter('fairplay.tpl.term', tpl::declension($term, _t('', 'день;дня;дней')));
    }

    /**
     * Максимальное кол-во загружаемых документов для проверки пользователя
     * @return bool
     */
    public static function verifiedFilesLimit()
    {
        return config::sysAdmin('fairplay.verified.files.limit', 5, TYPE_UINT);
    }

    /**
     * Инициализация компонента обработки изображений для проверки пользователей
     * @param mixed $userID ID пользователя или FALSE
     * @return FairplayVerifiedImages component
     */
    public static function verifiedImages($userID = false)
    {
        static $i;
        if ( ! isset($i)) {
            $i = new FairplayVerifiedImages();
        }
        $i->setRecordID($userID);
        return $i;
    }

    /**
     * Актуализация счетчика документов пользователей ожидающих проверки
     * @param integer|null $increment
     */
    public function verifiedCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->verifiedWaitingCounter();
            config::save('fairplay_verified_waiting', $count, true);
        } else {
            config::saveCount('fairplay_verified_waiting', $increment, true);
        }
    }

    /**
     * Актуализация счетчика обращений в арбитраж
     * @param integer|null $increment
     */
    public function arbitrageCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->arbitrageCounter();
            config::save('fairplay_arbitrage', $count, true);
        } else {
            config::saveCount('fairplay_arbitrage', $increment, true);
        }
    }

    /**
     * Актуализация счетчика ходов работ ожидающих выплату
     * @param integer|null $increment
     */
    public function paymentCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->paymentCounter();
            config::save('fairplay_payment', $count, true);
        } else {
            config::saveCount('fairplay_payment', $increment, true);
        }
    }

    /**
     * Статусы ходов работ
     * @return array
     */
    public static function statuses()
    {
        return bff::filter('fairplay.statuses', array(
            static::STATUS_NEGOTIATION => array('id' => static::STATUS_NEGOTIATION, 't' => _t('fp', 'Oбсуждение условий'),    'icon' => 'icon-pencil'),
            static::STATUS_RESERVATION => array('id' => static::STATUS_RESERVATION, 't' => _t('fp', 'Резервирование суммы'),  'icon' => 'icon-arrow-down'),
            static::STATUS_EXECUTION   => array('id' => static::STATUS_EXECUTION,   't' => _t('fp', 'Выполнение работы'),     'icon' => 'icon-wrench'),
            static::STATUS_CLOSED      => array('id' => static::STATUS_CLOSED,      't' => _t('fp', 'Закрыт'),                'icon' => 'icon-ok'),
            static::STATUS_CANCEL      => array('id' => static::STATUS_CANCEL,      't' => _t('fp', 'Отменен'),               'icon' => 'icon-remove'),
            static::STATUS_PAYMENT     => array('id' => static::STATUS_PAYMENT,     't' => _t('fp', 'Выплата суммы'),         'icon' => 'icon-arrow-up'),
            static::STATUS_REWORK      => array('id' => static::STATUS_REWORK,      't' => _t('fp', 'Доработка'),             'icon' => 'icon-repeat'),
        ));
    }

    /**
     * Типы счетов
     * @return array
     */
    public static function billTypes()
    {
        return bff::filter('fairplay.bill.types', array(
            static::BILL_TYPE_RESERVED => array('id' => static::BILL_TYPE_RESERVED, 't' => _t('fp', 'резервирование'), 'icon' => 'icon icon-arrow-down disabled'),
            static::BILL_TYPE_PAID     => array('id' => static::BILL_TYPE_PAID,     't' => _t('fp', 'выплата'),        'icon' => 'icon icon-arrow-up disabled'),
        ));
    }

    /**
     * Типы событий истории хода работ
     * @return array
     */
    public static function historyTypes()
    {
        return bff::filter('fairplay.history.types', array(
            static::HISTORY_TYPE_FAIRPLAY        => array('id' => static::HISTORY_TYPE_FAIRPLAY,        't' => _t('fp', 'Тип оплаты Безопасная сделка')),
            static::HISTORY_TYPE_RESERVED        => array('id' => static::HISTORY_TYPE_RESERVED,        't' => _t('fp', 'Заказчик успешно зарезервировал сумму [sum]')),
            static::HISTORY_TYPE_PAID            => array('id' => static::HISTORY_TYPE_PAID,            't' => _t('fp', 'Сумма [sum] выплачена исполнителю')),
            static::HISTORY_TYPE_COMPLETE        => array('id' => static::HISTORY_TYPE_COMPLETE,        't' => _t('fp', 'Исполнитель уведомил о выполнении работы')),
            static::HISTORY_TYPE_REWORK          => array('id' => static::HISTORY_TYPE_REWORK,          't' => _t('fp', 'Заказчик вернул на доработку')),
            static::HISTORY_TYPE_ARBITRAGE_START => array('id' => static::HISTORY_TYPE_ARBITRAGE_START, 't' => _t('fp', 'Обращение в арбитраж')),
            static::HISTORY_TYPE_ARBITRAGE_STOP  => array('id' => static::HISTORY_TYPE_ARBITRAGE_STOP,  't' => _t('fp', 'Арбитраж завершен')),
            static::HISTORY_TYPE_OPINION_CLIENT  => array('id' => static::HISTORY_TYPE_OPINION_CLIENT,  't' => _t('fp', 'Добавлен отзыв от заказчика')),
            static::HISTORY_TYPE_OPINION_WORKER  => array('id' => static::HISTORY_TYPE_OPINION_WORKER,  't' => _t('fp', 'Добавлен отзыв от исполнителя')),
        ));
    }

    /**
     * Формирование описания события истории хода работ
     * @param integer $type тип события
     * @param array $extra доп. данные
     * @return string
     */
    public static function historyDescription($type, $extra = array())
    {
        $types = static::historyTypes();
        if ( ! isset($types[$type])) return '';

        $result = $types[$type]['t'];
        switch ($type) {
            case static::HISTORY_TYPE_RESERVED:
            case static::HISTORY_TYPE_PAID:
                $sum = isset($extra['sum']) ? $extra['sum'].' '.Site::currencyDefault('title_short') : '';
                $result = strtr($result, array('[sum]' => $sum));
                break;
        }
        return bff::filter('fairplay.history.description', $result, $extra);
    }

    /**
     * Статусы проверки финансовых документов пользователей
     * @return array
     */
    public static function verifiedStatuses()
    {
        return bff::filter('fairplay.verified.statuses', array(
            static::VERIFIED_STATUS_NONE => array(
                'id'    => static::VERIFIED_STATUS_NONE,
                'i'     => 'icon-warning-sign',
                'title' => _t('users', 'Не подтвержден'),
            ),
            static::VERIFIED_STATUS_WAIT => array(
                'id'    => static::VERIFIED_STATUS_WAIT,
                'i'     => 'icon-time',
                'title' => _t('users', 'Подал заявку'),
            ),
            static::VERIFIED_STATUS_DECLINED => array(
                'id'    => static::VERIFIED_STATUS_DECLINED,
                'i'     => 'icon-ban-circle',
                'title' => _t('users', 'Отклонен'),
            ),
            static::VERIFIED_STATUS_APPROVED => array(
                'id'    => static::VERIFIED_STATUS_APPROVED,
                'i'     => 'icon-ok',
                'title' => _t('users', 'Проверенный'),
            ),
        ));
    }

    /**
     * Причины отклонения проверки финансовых документов пользователей
     * @return array
     */
    public static function verifiedReasons()
    {
        $reasons = bff::filter('fairplay.verified.reasons', array(
            1 => array('t' => _t('users', 'Фотография не является сканом документа')),
            array('t' => _t('users', 'Фотография плохого качества')),
            array('t' => _t('users', 'ФИО автора документа не соответствует указанному в профиле')),
            array('t' => _t('users', 'Предоставьте, пожалуйста, документы подтверждающие вашу личность')),
            static::VERIFIED_REASON_OTHER => array('t' => _t('users', 'Другая причина')),
        ));
        foreach ($reasons as $k=>&$v) {
            $v['id'] = $k;
        } unset($v);
        return $reasons;
    }

    /**
     * Данные для конструктора полей формы на странице "Финансы"
     * @url /user/settings?tab=finance
     * @return array
     */
    public static function financeFields()
    {
        /*
          Первый уровень - блоки полей
            'general' => array( ключ используется при формировании названия макроса, 'general' меняется на ''
                't' => '',  Заголовок (выводим если не пустой)
                'fields' => array(  Второй уровень - массив полей в формате:
                    'key' => array('id' => 'key'  key - ключ - имя поля в БД и формах.
                        'type' =>  - тип поля возможные значения:
                            'text' - input text;
                            'text-addon' - тоже, что и text, используется для записи тескстовых инпутов в одну строку;
                            'textarea' - textarea;
                            'date' - 3 поля типа "input text" для выбора дня, месяца и года;
                            'radio' - radio button (обязательный параметр options)
                        'len' => максимальная длина поля для типов (textarea, text-addon, text) используется в валидаторе.
                        't' => Заголовок поля, тег label
                        'm' => Поле используется в макросах при генерации документов. Описание макроса.
                        'required' => true|false - Обязательно для заполнения
                        'sm' => Значения класса sm для верстки на фронтенде
                        'error' => Текст для вывода сообщения об ошибке (если не указан будет "Заполните все отмеченные поля")
                        'placeholder' => Значение тега placeholder
                        'options' => Набор значений в формате ключ => значение (используется для типа radio)
         */

        return bff::filter('fairplay.finance.fields', array(
            'general' => array(
                't' => '',
                'fields' => array(
                    'fio'       => array('id' => 'fio',       'type' => 'text',  'len' => 250, 't' => _t('fp', 'ФИО'),           'm' =>_t('fp', 'ФИО'),            'required' => true, 'sm' => 6, 'placeholder' => _t('', 'Ваши фамилия, имя и отчество')),
                    'birthdate' => array('id' => 'birthdate', 'type' => 'date',                't' => _t('fp', 'Дата рождения'), 'm' => _t('fp', 'Дата рождения'), 'required' => true, 'error' => _t('users', 'Дата рождения указана некорректно')),
                    'phone'     => array('id' => 'phone',     'type' => 'text',  'len' => 20,  't' => _t('fp', 'Телефон'),       'm' => _t('fp', 'Телефон'),       'required' => true, 'sm' => 4, 'placeholder' => _t('', 'Ваш номер телефона')),
                    'residence' => array('id' => 'residence', 'type' => 'radio', 't' => _t('fp', 'Резиденство'), 'm' => _t('fp', 'Резиденство'),                 'required' => true,
                        'error' => _t('fp', 'Укажите резиденство'),
                        'options' => array(
                             1 => _t('fp', 'Резидент'),
                             2 => _t('fp', 'Не резидент'),
                             3 => _t('fp', 'Беженец'),
                             4 => _t('fp', 'Вид на жительство'),
                        )),
                    'law_status' => array('id' => 'law_status', 'type' => 'radio', 't' => _t('fp', 'Юр. статус'), 'm' => _t('fp', 'Юр. статус'),                 'required' => true,
                        'error' => _t('fp', 'Укажите юр. статус'),
                        'options' => array(
                         1 => _t('fp', 'Физическое лицо'),
                         2 => _t('fp', 'Юридическое лицо'),
                        )),
                ),
            ),
            'document' => array(
                't' => _t('fp', 'Паспортные данные'),
                'fields' => array(
                    'number'    => array('id' => 'number',    'type' => 'text-addon', 'len' => 20,  'required' => true, 'm' => _t('fp', 'Номер паспорта'), 'sm' => 4, 'placeholder' => _t('fp', 'номер') ),
                    'series'    => array('id' => 'series',    'type' => 'text', 'len' => 5,   't' => _t('fp', 'Серия и номер паспорта'), 'm' => _t('fp', 'Серия паспорта'), 'required' => true, 'sm' => 2, 'placeholder' => _t('fp', 'серия')),
                    'date'      => array('id' => 'date',      'type' => 'text', 'len' => 20,  't' => _t('fp', 'Дата выдачи'), 'm' => _t('fp', 'Дата выдачи документа'), 'required' => true, 'sm' => 6, ),
                    'issued_by' => array('id' => 'issued_by', 'type' => 'text', 'len' => 200, 't' => _t('fp', 'Кем выдан'),   'm' => _t('fp', 'Кем выдан документ'), 'required' => true),
                    'register'  => array('id' => 'register',  'type' => 'text', 'len' => 200, 't' => _t('fp', 'Адрес регистрации'), 'm' => _t('fp', 'Адрес регистрации'), 'required' => true),
                    'post'      => array('id' => 'post',      'type' => 'text', 'len' => 200, 't' => _t('fp', 'Почтовый адрес'),'m' => _t('fp', 'Почтовый адрес'), 'required' => true),
                ),
            ),
            'pay' => array(
                't' => _t('fp', 'Платежные реквизиты'),
                'fields' => array(
                    'yandex'    => array('id' => 'yandex',    'type' => 'text',     'len' => 50,  't' => _t('fp', 'Яндекс.Деньги'), 'm' => _t('fp', 'Яндекс.Деньги'), 'sm' => 4,),
                    'iban'      => array('id' => 'iban',      'type' => 'text',     'len' => 200, 't' => _t('fp', 'Расчетный счет (IBAN)'), 'm' => _t('fp', 'Расчетный счет (IBAN)')),
                    'bank'      => array('id' => 'bank',      'type' => 'text',     'len' => 200, 't' => _t('fp', 'Название банка'), 'm' => _t('fp', 'Название банка')),
                    'bank_rf'   => array('id' => 'bank_rf',   'type' => 'text',     'len' => 200, 't' => _t('fp', 'Название банка уполномоченного банка в РФ'), 'm' => _t('fp', 'Название банка уполномоченного банка в РФ')),
                    'bank_korr' => array('id' => 'bank_korr', 'type' => 'text',     'len' => 200, 't' => _t('fp', 'Корр.счет Вашего банка в уполномоченном банке'), 'm' => _t('fp', 'Корр.счет Вашего банка в уполномоченном банке')),
                    'bank_bik'  => array('id' => 'bank_bik',  'type' => 'text',     'len' => 200, 't' => _t('fp', 'БИК уполномоченного банка'), 'm' => _t('fp', 'БИК уполномоченного банка')),
                    'bank_inn'  => array('id' => 'bank_inn',  'type' => 'text',     'len' => 200, 't' => _t('fp', 'ИНН уполномоченного банка'), 'm' => _t('fp', 'ИНН уполномоченного банка')),
                    'kor_name'  => array('id' => 'kor_name',  'type' => 'text',     'len' => 200, 't' => _t('fp', 'Название банка корреспондента'), 'm' => _t('fp', 'Название банка корреспондента')),
                    'acc_num'   => array('id' => 'acc_num',   'type' => 'text',     'len' => 200, 't' => _t('fp', 'Номер счета уполномоченного банка в РФ в ГУ ЦРБ'), 'm' => _t('fp', 'Номер счета уполномоченного банка в РФ в ГУ ЦРБ')),
                    'comment'   => array('id' => 'comment',   'type' => 'textarea', 'len' => 500, 't' => _t('fp', 'Комментарий к платежу')),
                ),
            ),
        ));
    }

    /**
     * События, по которым происходит генерация документов
     * @return array
     */
    public static function docsEvents()
    {
        return array(
            static::DOCS_EVENT_CONFIRM  => array('id' => static::DOCS_EVENT_CONFIRM,  't' => _t('fp', 'Исполнитель подтвердил заказ')),
            static::DOCS_EVENT_RESERVED => array('id' => static::DOCS_EVENT_RESERVED, 't' => _t('fp', 'Заказчик зарезервировал сумму')),
            static::DOCS_EVENT_COMPLETE => array('id' => static::DOCS_EVENT_COMPLETE, 't' => _t('fp', 'Заказчик принял работу (закрыл заказ, оставил отзыв)')),
            static::DOCS_EVENT_PAID     => array('id' => static::DOCS_EVENT_PAID,     't' => _t('fp', 'Средства выплачены исполнителю')),
        );
    }

    /**
     * Макросы для документов
     * @return array
     */
    public static function docsMacroses()
    {
        $general = array();
        $general['workflow_id'] = array('t' => _t('fp','ID Хода работ'), 'example' => 'XX');
        $general['workflow_created'] = array('t' => _t('fp','Дата создания хода работ'), 'example' => 'дд.мм.гггг');
        $general['workflow_created_text'] = array('t' => _t('fp','Дата создания прописью'), 'example' => '01 января 2016');
        $general['workflow_price'] = array('t' => _t('fp','Сумма'), 'example' => 'X XXX,XX');
        $general['workflow_price_text'] = array('t' => _t('fp','Сумма прописью'), 'example' => 'сто одиннадцать гривен');
        $general['workflow_commission'] = array('t' => _t('fp','Сумма комиссии'), 'example' => 'X XXX,XX');
        $general['workflow_commission_text'] = array('t' => _t('fp','Сумма комиссии прописью'), 'example' => 'сто одиннадцать гривен');
        $general['workflow_term'] = array('t' => _t('fp','Срок'), 'example' => 'XX');
        $general['workflow_finish'] = array('t' => _t('fp','Дата завершения работ'), 'example' => 'дд.мм.гггг');

        $data = array('general' => $general);

        $fields = static::financeFields();
        foreach (array('worker', 'client') as $k) {
            $tmp = array();
            foreach ($fields as $kk => $vv) {
                foreach ($vv['fields'] as $kkk => $vvv) {
                    if (empty($vvv['m'])) continue;
                    $tmp[$k.'_'.($kk != 'general' ? $kk.'_' : '').$kkk] = array('t' => $vvv['m']);
                }
            }
            $data[$k] = $tmp;
        }
        return bff::filter('fairplay.docs.macroses', $data);
    }

    /**
     * Инициализация API для работы с Yandex.Money
     * @return ApiYandexMoney
     */
    public static function apiYandexMoney()
    {
        static $i;
        if ( ! isset($i)) {
            $i = new ApiYandexMoney();
        }
        return $i;
    }

    /**
     * Проверка финансовых данных
     * @param boolean $submit сохранение
     * @return array
     */
    protected function validateFinancesData($submit)
    {
        $financeFields = static::financeFields();
        $fields = array();
        $mandatory = array();
        $params = array();
        $radio = array();
        $date = array();
        foreach ($financeFields as $block) {
            foreach ($block['fields'] as $k => $f) {
                $fields[$k] = $f;
                switch ($f['type']) {
                    case 'textarea':
                    case 'text-addon':
                    case 'text':
                        $params[ $f['id'] ] = array(TYPE_NOTAGS, 'len' => $f['len']);
                        if ( ! empty($f['required'])) {
                            $mandatory[] = $f['id'];
                        }
                        break;
                    case 'date':
                        $params[ $f['id'] ] = TYPE_ARRAY_UINT;
                        $date[] = $f['id'];
                        break;
                    case 'radio':
                        $params[ $f['id'] ] = TYPE_UINT;
                        $radio[] = $f['id'];
                        break;
                }
            }
        }
        $data = $this->input->postm($params);
        if ($submit) {
            foreach ($date as $v) {
                if ((empty($data[$v]) || array_sum($data[$v]) == 0) &&  ! empty($fields[$v]['required'])) {
                    if ( ! empty($fields[$v]['error'])) {
                        $this->errors->set($fields[$v]['error']);
                    } else {
                        $this->errors->set(_t('fp', 'Поле обязательно для заполнения'), $v);
                    }
                } else {
                    if ( ! checkdate($data[$v]['month'], $data[$v]['day'], $data[$v]['year'])) {
                        if ( ! empty($fields[$v]['error'])) {
                            $this->errors->set($fields[$v]['error']);
                        } else {
                            $this->errors->set(_t('fp', 'Некорректное значение'), $v);
                        }
                    } else {
                        $data[$v] = join('-', array($data[$v]['year'], $data[$v]['month'], $data[$v]['day']));
                    }
                }
            }
            foreach ($radio as $v) {
                if (empty($data[$v]) && ! empty($fields[$v]['required'])) {
                    if ( ! empty($fields[$v]['error'])) {
                        $this->errors->set($fields[$v]['error']);
                    } else {
                        $this->errors->set(_t('fp', 'Поле обязательно для заполнения'), $v);
                    }
                } else {
                    if ( ! array_key_exists($data[$v], $fields[$v]['options'])) {
                        $this->errors->reloadPage();
                    }
                }
            }
            foreach ($mandatory as $v) {
                if (empty($data[$v])) {
                    if ( ! empty($fields[$v]['error'])) {
                        $this->errors->set($fields[$v]['error']);
                    } else {
                        $this->errors->set(_t('fp', 'Поле обязательно для заполнения'), $v);
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Расчет комиссии при резервировании средств в зависимость от настроек
     * @param float $price цена
     * @param string $title @ref строка с объяснением расчета комиссии
     * @return float комиссия
     */
    public function commission($price, & $title = '')
    {
        $commissions = config::get('fairplay_commissions', false);
        $commissions = func::unserialize($commissions);

        $result = 0;
        $title = '';

        if (empty($commissions)) {
            $commissions = array(array('from' => 0, 'to' => 0, 'sum' => '10', 'p' => 0));
        }
        foreach ($commissions as $v) {
            if ($v['from'] && $price < $v['from']) continue;
            if ($v['to'] && $price > $v['to']) continue;
            if ($v['p']) {
                $result = $v['sum'];
                $title = $v['sum'].' '.Site::currencyDefault('title_short');
            } else {
                $result = round($v['sum'] * $price / 100, 2);
                $title = $v['sum'].'%';
            }
            break;
        }
        return bff::filter('fairplay.commission', $result, array('title' => & $title, 'price' => $price));
    }

    /**
     * Активация услуг для пользователя
     * @param integer $workflowID ID хода работ
     * @param integer $svcID ID услуги
     * @param mixed $svcData данные об услуге(*)/пакете услуг или FALSE
     * @param array $settings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return boolean true - успешная активация, false - ошибка активации
     */
    public function svcActivate($workflowID, $svcID, $svcData = false, array &$settings = array())
    {
        switch ($svcID) {
            case static::SVC_RESERVED: # Резервирование средств
                return $this->svcActivateReserved($workflowID, $settings);
                break;
            default:
                $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                return false;
        }
    }

    /**
     * Формируем описание счета активации услуг
     * @param integer $workflowID ID хода работ
     * @param integer $svcID ID услуги
     * @param mixed $aData данные об услуге(*)/пакете услуг или FALSE
     * @param array $settings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return string
     */
    public function svcBillDescription($workflowID, $svcID, $aData = false, array &$settings = array())
    {
        switch ($svcID) {
            case static::SVC_RESERVED: # Резервирование средств
                $workflow = $this->model->workflowData($workflowID, array('id', 'order_id', 'price'));
                if (empty($workflow['order_id'])) {
                    return '';
                }
                $order = Orders::model()->orderData($workflow['order_id'], array('keyword', 'title', 'prepay'));
                $result = _t('fp', 'Резервирование средств [sum], заказ: ', array(
                                'sum' => tpl::formatPrice(Catcher::isUseOrderPrepay() && (isset($order['prepay']) && !empty($order['prepay']))? $order['prepay'] : $workflow['price']).' '.Site::currencyDefault('title_short'))
                            );
                $result .= '<a href="'.static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword']), false).'">'.$order['title'].'</a>';
                return $result;
                break;
        }
        return '';
    }

    /**
     * Активация услуги резервирования средств
     * @param integer $workflowID ID хода работ
     * @param array $settings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return boolean true - успешная активация, false - ошибка активации
     */
    protected function svcActivateReserved($workflowID, & $settings)
    {
        if ( ! $workflowID) {
            $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
            return false;
        }

        # получаем данные о ходе работы
        $data = $this->model->workflowData($workflowID, array('id', 'status', 'price'));
        if ($data['status'] != static::STATUS_RESERVATION) {
            $this->errors->set(_t('svc', 'Неудалось активировать услугу. Некорректный статус.'));
            return false;
        }
        $this->onReserved($workflowID);

        $settings['afterBillCreate'] = function($billID)
        {
            $bill = Bills::model()->billData($billID, array('amount', 'item_id'));
            if (empty($bill['item_id'])) return;

            $workflow = $this->model->workflowData($bill['item_id'], array('id', 'price', 'client_id'));
            if (empty($workflow)) return;
            $workflow = Catcher::getOrderPrepay($workflow);

            $commission = $bill['amount'] - ((Catcher::isUseOrderPrepay() && isset($workflow['prepay']) && !empty($workflow['prepay'])) ? $workflow['prepay'] : $workflow['price']);
            $this->model->billSave(0, array(
                'bill_id'       => $billID,
                'svc_id'        => static::SVC_RESERVED,
                'commission'    => $commission,
                'user_id'       => $workflow['client_id'],
                'workflow_id'   => $bill['item_id'],
                'sum'           => (Catcher::isUseOrderPrepay() && isset($workflow['prepay']) && !empty($workflow['prepay']))? $workflow['prepay'] : $workflow['price'],
                'type'          => static::BILL_TYPE_RESERVED,
            ));

            $this->model->workflowSave($workflow['id'], array('commission' => $commission));
        };
        return true;
    }

    /**
     * Период: 1 раз в час
     */
    public function svcCron()
    {
        if (!bff::cron()) {
            return;
        }
    }

    /**
     * Обработка события резервирования средств
     * @param integer $workflowID ID хода работ
     */
    public function onReserved($workflowID)
    {
        # получаем данные о ходе работы
        $workflow = $this->model->workflowData($workflowID, array('order_id', 'worker_id', 'price', 'client_id'));
        $workflow = Catcher::getOrderPrepay($workflow);
        if (empty($workflow)) return;

        # переводим ход работы в статус "Выполнение работы"
        $this->model->workflowSave($workflowID, array(
            'status' => static::STATUS_EXECUTION,
            'reserved' => $this->db->now(),
        ));

        # генерируем документы
        $this->workflowsDocs($workflowID)->fireEvent(static::DOCS_EVENT_RESERVED);

        # помечаем в истории
        $this->model->historyAdd($workflowID, array(
            'type'    => static::HISTORY_TYPE_RESERVED,
            'user_id' => $workflow['client_id'],
            'extra'   => array('sum' => (Catcher::isUseOrderPrepay() && isset($workflow['prepay']) && !empty($workflow['prepay']))? $workflow['prepay'] : $workflow['price']),
        ));

        # отправляем уведомление на почту исполнителю
        $order = Orders::model()->orderData($workflow['order_id'], array('title', 'keyword'));
        Users::sendMailTemplateToUser($workflow['worker_id'], 'workflow_money_reserved', array(
            'workflow_id'   => $workflowID,
            'order_title'   => $order['title'],
            'workflow_url'  => static::url('view', array('id' => $workflowID, 'keyword' => $order['keyword'])),
            'sum'           => (Catcher::isUseOrderPrepay() && isset($workflow['prepay']) && !empty($workflow['prepay']))? $workflow['prepay'] : $workflow['price'].' '.Site::currencyDefault('title_short'),
        ));
    }

    /**
     * Логирование процесса оплаты
     * @param string|array $mMessage текст или данные для логирования
     * @return mixed
     */
    protected function log($mMessage)
    {
        return bff::log($mMessage, 'fairplay.log');
    }

    /**
     * Инициализация компонента генерации PDF-документов
     * @return PdfGenerator
     */
    public static function pdfGenerator()
    {
        static $i;
        if ( ! isset($i)) {
            $i = new PdfGenerator();
        }
        return $i;
    }

    /**
     * Инициализация компонента работы с документами хода работ
     * @param integer $workflowID ID хода работ
     * @return FairplayWorkflowsDocs
     */
    public static function workflowsDocs($workflowID = 0)
    {
        static $i;
        if ( ! isset($i)) {
            $i = new FairplayWorkflowsDocs($workflowID);
        }
        $i->setRecordID($workflowID);
        return $i;
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('attachments/fairplay') => 'dir-split', # прикреплений для истории сообщений
            PATH_BASE.'files'.DS.'fairplay'.DS => 'dir-split', # проверка финансовых данных
            PATH_BASE.'files'.DS.'workflows'.DS => 'dir-split', # документы ходов работ
        ));
    }

    /**
     * Проверка наличия компонента для генерации PDF файлов
     * @return string
     */
    public function pdfGeneratorCheck()
    {
        $class = $this->pdfGenerator()->generator();
        if(empty($class)){
            return _t('fp', 'PDF generator not found');
        }
        return _t('fp', 'PDF generator: ').$class;
    }

}