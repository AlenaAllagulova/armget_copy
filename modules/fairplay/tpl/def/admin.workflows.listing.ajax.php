<?php
$statuses = Fairplay::statuses();
$urlEdit = $this->adminLink(bff::$event.'&act=edit&id=');
foreach($list as $k=>$v):
    $id = $v['id'];
    $s = $statuses[ $v['status'] ];
    ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $id ?></td>
        <td class="left">
            <a href="<?= Orders::url('view', array('id' => $v['order_id'], 'keyword' => $v['keyword'])) ?>" class="but linkout" target="_blank"></a>
            <a href="#" class="nolink j-workflow-info" data-id="<?= $id ?>"><?= $v['title'] ?></a>
            <? if($v['fairplay'] == Fairplay::FAIRPLAY_USE): ?><i class="icon icon-fairplay" title="<?= _t('fp', 'Безопасная сделка'); ?>"></i><? endif; ?>
            <i class="icon <?= $s['icon'] ?> disabled" title="<?= $s['t'] ?>"></i>
        </td>
        <td><? if( ! empty($v['cnt_new'])): ?><a class="workflow-edit" href="#" rel="<?= $id ?>" data-tab="chat">+<?= $v['cnt_new'] ?></a><? endif; ?></td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a class="but edit workflow-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="5">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;