<?php
/**
 * @var $this Fairplay
 */
?>
<div id="popupOrderInfo" class="ipopup">
    <div class="ipopup-wrapper">
        <div class="ipopup-title"><?= _t('fp', 'Информация о ходе работ № [num]', array('num' => $id)); ?></div>
        <div class="ipopup-content" style="width:500px;">
            <table class="admtbl tbledit">
                <tr>
                    <td class="row1 field-title right" width="140" style="vertical-align: top;"><?= _t('', 'Заказ:'); ?></td>
                    <td class="row2">
                        <a href="#" onclick="return bff.orderinfo(<?= $order['id'] ?>);">#<?= $order['id'] ?></a>
                        <?= HTML::escape($order['title']) ?>
                        <? if($order['visibility'] == Orders::VISIBILITY_PRIVATE): ?><i class="icon icon-eye-close disabled" title="<?= _t('orders', 'Приватный заказ'); ?>"></i><? endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="row1 field-title right"><?= _t('fp', 'Способ оплаты:'); ?></td>
                    <td class="row2">
                        <? if($fairplay == Fairplay::FAIRPLAY_USE): ?>
                            <i class="icon icon-fairplay" title="<?= _te('fp', 'Безопасная сделка'); ?>"></i> <?= _t('fp', 'Безопасная сделка'); ?>
                        <? else: ?>
                            <?= _t('fp', 'Прямая оплата'); ?>
                        <? endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="row1 field-title right"><?= _t('', 'Заказчик:'); ?></td>
                    <td class="row2">
                        <a href="#" onclick="return bff.userinfo('<?= $client_id ?>');"><?= $client['email'] ?></a>
                    </td>
                </tr>
                <tr>
                    <td class="row1 field-title right"><?= _t('', 'Исполнитель:'); ?></td>
                    <td class="row2">
                        <a href="#" onclick="return bff.userinfo('<?= $worker_id ?>');"><?= $worker['email'] ?></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?
                        $aData['is_popup'] = true;
                        echo $this->viewPHP($aData, 'admin.workflow.form.status');
                        ?>
                    </td>
                </tr>

            </table>
            <div class="ipopup-content-bottom">
                <ul class="right">
                    <li><span class="post-date" title="<?= _te('', 'Created'); ?>"><?= tpl::date_format2($created); ?></span></li>
                </ul>
            </div>
        </div>
    </div>
</div>