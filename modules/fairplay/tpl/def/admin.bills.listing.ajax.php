<?php
$types = Fairplay::billTypes();
foreach($list as $k=>$v): ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $v['id'] ?></td>
        <td class="left">
            <a href="#" class="nolink j-workflow-info" data-id="<?= $v['workflow_id'] ?>"><?= $v['title'] ?></a><br />
            <span class="desc small"><?= $v['info'] ?></span>
        </td>
        <td><i class="<?= $types[ $v['type'] ]['icon'] ?>" title="<?= $types[ $v['type'] ]['t'] ?>"></i></td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);"><?= $v['email'] ?></a></td>
        <td class="right"><?= $v['sum'] ?></td>
        <td class="right"><?= $v['commission'] ?></td>
        <td><? if( ! empty($v['descr'])): ?>
            <div class="show-descr">
            <span class="icon-exclamation-sign disabled"></span>
            <div class="descr-hidden"><?= nl2br($v['descr']) ?></div>
            </div><? endif; ?>
        </td>
    </tr>
<? endforeach;

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="8">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;