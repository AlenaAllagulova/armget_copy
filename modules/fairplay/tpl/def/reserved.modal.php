<?php
$cur = ' '.Site::currencyDefault('title_short');
$fin_stat = '';
if(isset($finance['residence']) && isset($finance['law_status'])){
    $fields = Fairplay::financeFields();
    $fields = $fields['general']['fields'];
    $residences = $fields['residence']['options'];
    $law_statuses = $fields['law_status']['options'];
    $fin_stat = $residences[ $finance['residence'] ].', '.$law_statuses[ $finance['law_status'] ];
}
?>
<div class="modal fade" id="j-modal-fairplay-reserved-<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('fp', 'Зарезервировать [sum]', array('sum' => tpl::formatPrice($price).$cur)); ?></h4>
            </div>
            <div class="modal-body">
                <h5 class="mrgt0"><?= _t('fp', 'Сумма оплаты'); ?></h5>
                <ul class="list-unstyled">
                    <li><?= _t('fp', 'Бюджет заказа: [sum]', array('sum' => tpl::formatPrice($price).$cur)); ?></li>
                    <li><?= _t('fp', 'Комиссия сайта ([percent]): [sum]', array('percent' => $descr, 'sum' => tpl::formatPrice($commission).$cur)); ?></li>
                    <li><strong><?= _t('fp', 'Итого к оплате: [sum]', array('sum' => tpl::formatPrice($sum).$cur)); ?></strong></li>
                </ul>
                <h5><?= _t('fp', 'Способ резервирования'); ?></h5>
                <p>
                    <?= _t('fp', 'Ваш статус: [status] (<a [link]>изменить</a>)', array('status' => $fin_stat, 'link' => 'href="'.Users::url('my.settings', array('tab' => 'finance')).'"')); ?><br>
                    <?= _t('fp', 'Вам доступны следующие способы резервирования:'); ?>
                </p>
                <?= $payList ?>
            </div>
        </div>
    </div>
</div>

