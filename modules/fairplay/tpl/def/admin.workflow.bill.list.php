<?php
if(empty($list)): ?>
    <div class="alert alert-info" style="margin-bottom: 10px;">
        <?= _t('fp', 'На данный момент еще не было финансовых операций') ?>
    </div>
<? return; endif;
$types = Fairplay::billTypes();
?>
<table class="table table-condensed table-hover admtbl tblhover">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="40">ID</th>
        <th></th>
        <th width="100"><?= _t('', 'Created') ?></th>
        <th width="100"><?= _t('', 'Пользователь') ?></th>
        <th width="70" class="right"><?= _t('', 'Сумма').', '.Site::currencyDefault('title_short') ?></th>
        <th width="90" class="right"><?= _t('', 'Комиссия').', '.Site::currencyDefault('title_short') ?></th>
        <th class="left"><?= _t('', 'Описание') ?></th>
        <th width="20"></th>
    </tr>
    </thead>
    <tbody>
    <? foreach($list as $k => $v): ?>
        <tr class="row<?= ($k%2) ?>">
            <td class="small"><?= $v['id'] ?></td>
            <td><i class="<?= $types[ $v['type'] ]['icon'] ?>" title="<?= $types[ $v['type'] ]['t'] ?>"></i></td>
            <td><?= tpl::date_format2($v['created'], true, true) ?></td>
            <td><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);"><?= $v['email'] ?></a></td>
            <td class="right"><?= $v['sum'] ?></td>
            <td class="right"><?= $v['commission'] ?></td>
            <td class="left"><?= $v['info'] ?></td>
            <td><? if( ! empty($v['descr'])): ?>
                <div class="show-descr">
                    <span class="icon-exclamation-sign disabled"></span>
                    <div class="descr-hidden"><?= nl2br($v['descr']) ?></div>
                </div><? endif; ?>
            </td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>
