<?php
    $userID = User::id();
    $opinions = Opinions::aTypes();
    $opinion_title = '<strong class="'.$opinions[ $opinion['type'] ]['ct'].'">'.$opinions[ $opinion['type'] ]['tl'].'</strong>';
?>
<div class="modal fade" id="j-modal-opinion-view-<?= $opinion['id'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <? if($opinion['author_id'] == $userID):
                    $title = _t('fp', 'Вы оставили [opinion] отзыв:', array('opinion' => $opinion_title)); ?>
                    <h4 class="modal-title"><?= _t('fp', 'Ваш отзыв'); ?></h4>
                <? else: if($opinion['id'] == $client_opinion) :
                        $title = _t('fp', 'Заказчик оставил [opinion] отзыв:', array('opinion' => $opinion_title)); ?>
                        <h4 class="modal-title"><?= _t('fp', 'Отзыв заказчика'); ?></h4>
                    <? else:
                        $title = _t('fp', 'Исполнитель оставил [opinion] отзыв:', array('opinion' => $opinion_title)); ?>
                        <h4 class="modal-title"><?= _t('fp', 'Отзыв исполнителя'); ?></h4>
                <? endif; endif; ?>
            </div>
            <div class="modal-body">
                <p><?= $title ?></p>
                <?= nl2br($opinion['message']); ?>
            </div>
        </div>
    </div>
</div>

