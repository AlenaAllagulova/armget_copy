<?php
/**
 * @var $this Fairplay
 */
$curr = Site::currencyDefault('title_short');
$tab = $this->input->getpost('ftab', TYPE_NOTAGS);
$aTabs = array(
    'info'    => array('t' => _t('fp', 'Описание')),
    'bill'    => array('t' => _t('fp', 'Финансы')),
    'chat'    => array('t' => _t('fp', 'Чат')),
    'hist'    => array('t' => _t('fp', 'История')),
    'docs'    => array('t' => _t('fp', 'Документы')),
);
if( ! isset($aTabs[$tab])) {
    $tab = 'info';
}
if( ! empty($cnt_new)){
    $aTabs['chat']['counter'] = $cnt_new;
}
if($arbitrage):
    $aTabs['chat']['t'] = '<span class="icon-warning-sign disabled j-arbitrage-block"></span> '.$aTabs['chat']['t'];
    ?>
    <div class="alert alert-info j-arbitrage-block" style="margin-bottom: 10px;">
        <?= _t('fp', 'Идет арбитраж') ?>
    </div>
<? endif;
if(count($aTabs) > 1): ?>
    <div class="tabsBar">
        <? foreach($aTabs as $k=>$v): ?>
            <span class="tab<?= $k==$tab ? ' tab-active' : '' ?>"><a href="#" onclick="jFairplayWorkflowsForm.onTab('<?= $k ?>', this); return false;"><?= $v['t'] ?><? if(! empty($v['counter'])){ ?> (<?= $v['counter'] ?>)<? } ?></a></span>
        <? endforeach; ?>
        <div class="progress" style="margin-left: 5px; display: none;" id="fairplay-workflow-form-progress"></div>
    </div>
<? endif; ?>
<div id="fairplay-workflow-form-block-info" class="hidden">
    <form name="FairplayWorkflowsForm" id="FairplayWorkflowsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
        <input type="hidden" name="act" value="edit" />
        <input type="hidden" name="save" value="1" />
        <input type="hidden" name="id" value="<?= $id ?>" />
        <table class="admtbl tbledit">
            <tr>
                <td class="row1 field-title" width="145"><?= _t('fp', 'Заказ:'); ?></td>
                <td class="row2">
                    <a href="#" onclick="return bff.orderinfo(<?= $order['id'] ?>);">#<?= $order['id'] ?></a>
                    <?= HTML::escape($order['title']) ?>
                    <? if($order['visibility'] == Orders::VISIBILITY_PRIVATE): ?><i class="icon icon-eye-close disabled" title="Приватный заказ"></i><? endif; ?>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('fp', 'Способ оплаты:'); ?></td>
                <td class="row2">
                    <? if($fairplay == Fairplay::FAIRPLAY_USE): ?>
                        <i class="icon icon-fairplay" title="<?= _te('fp', 'Безопасная сделка'); ?>"></i> <?= _t('fp', 'Безопасная сделка'); ?>
                    <? else: ?>
                        <?= _t('fp', 'Прямая оплата'); ?>
                    <? endif; ?>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('', 'Заказчик:'); ?></td>
                <td class="row2">
                    <?= $client['name'] ?> <?= $client['surname'] ?> <a href="#" onclick="return bff.userinfo('<?= $client_id ?>');"><?= $client['email'] ?></a>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('', 'Исполнитель:'); ?></td>
                <td class="row2">
                    <?= $worker['name'] ?> <?= $worker['surname'] ?> <a href="#" onclick="return bff.userinfo('<?= $worker_id ?>');"><?= $worker['email'] ?></a>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('fp', 'Бюджет:'); ?></td>
                <td class="row2">
                    <?= tpl::formatPrice($price) ?> <?= $curr ?>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('fp', 'Срок:'); ?></td>
                <td class="row2">
                    <?= Fairplay::tplTerm($term) ?>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('fp', 'Эксперт от заказчика:'); ?></td>
                <td class="row2">
                    <div <?= $client_expert_id ? 'class="hide"' : '' ?>>
                        <input type="hidden" value="<?= $client_expert_id ?>" id="j-client-expert-id" />
                        <input type="text" id="j-client-expert-email" class="autocomplete" placeholder="<?= _t('', 'Enter user e-mail') ?>" style="width: 220px;" />
                        <a class="ajax j-client-expert-set" href="#"><?= _t('fp', 'закрепить'); ?></a>
                    </div>
                    <div <?= ! $client_expert_id ? 'class="hide"' : '' ?>>
                        <a href="#" class="ajax j-expert-view" data-id="<?= $client_expert_id ?>"><?= ! empty($client_expert['email']) ? $client_expert['email'] : '' ?></a>
                        <a class="ajax desc j-client-expert-cancel" href="#"><?= _t('fp', 'закрыть доступ'); ?></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('fp', 'Эксперт от исполнителя:'); ?></td>
                <td class="row2">
                    <div <?= $worker_expert_id ? 'class="hide"' : '' ?>>
                        <input type="hidden" value="<?= $worker_expert_id ?>" id="j-worker-expert-id" />
                        <input type="text" id="j-worker-expert-email" class="autocomplete" placeholder="<?= _t('', 'Enter user e-mail') ?>" style="width: 220px;" />
                        <a class="ajax j-worker-expert-set" href="#"><?= _t('fp', 'закрепить'); ?></a>
                    </div>
                    <div <?= ! $worker_expert_id ? 'class="hide"' : '' ?>>
                        <a href="#" class="ajax j-expert-view" data-id="<?= $worker_expert_id ?>"><?= ! empty($worker_expert['email']) ? $worker_expert['email'] : '' ?></a>
                        <a class="ajax desc j-worker-expert-cancel" href="#"><?= _t('fp', 'закрыть доступ'); ?></a>
                    </div>
                </td>
            </tr>
        </table>
    </form>
    <?= $this->viewPHP($aData, 'admin.workflow.form.status'); ?>
    <div style="margin-top: 5px;">
        <input type="button" class="btn button cancel" value="<?= _t('', 'Назад') ?>" onclick="jFairplayWorkflowsFormManager.action('cancel');" />
    </div>
</div>
<div id="fairplay-workflow-form-block-bill" class="hidden">
    <div id="j-bills-list"><?= $bills_list ?></div>
    <? include('admin.workflow.bill.form.php');  ?>
</div>

<div id="fairplay-workflow-form-block-chat" class="hidden">
    <div class="comments" id="j-workflow-chat" style="max-height: 540px; overflow-y: scroll;">
    </div>
    <div class="reply well well-small">
        <form id="j-chat-form" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="workflow" value="<?= $id ?>" />
            <input type="hidden" name="id" value="" />
            <textarea class="stretch" name="message" style="height: 65px; margin-bottom:3px;"></textarea>
            <input type="submit" value="<?= _te('', 'Отправить'); ?>" class="btn btn-success btn-small button submit j-submit">
            <input type="file" name="attach" value="" />
            <? if($arbitrage): ?><a onclick="return jFairplayWorkflowStatus.closeArbitrage();" class="btn btn-mini btn-warning j-arbitrage-block" href="#"><?= _t('fp', 'Завершить арбитраж') ?></a><? endif; ?>
        </form>
    </div>
</div>

<div id="fairplay-workflow-form-block-hist" class="hidden">
    <? if( ! empty($history)): ?>
        <table class="table table-condensed table-hover admtbl tblhover">
            <thead>
            <tr class="header nodrag nodrop">
                <th width="150"><?= _t('', 'Дата') ?></th>
                <th class="left"><?= _t('', 'Описание') ?></th>
            </tr>
            </thead>
            <tbody>
            <? foreach($history as $k => $v): ?>
                <tr class="row<?= ($k%2) ?>">
                    <td><?= tpl::date_format3($v['created']) ?></td>
                    <td class="left"><?= $v['descr'] ?></td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    <? endif; ?>
</div>

<div id="fairplay-workflow-form-block-docs" class="hidden">
    <? if(isset($documents['client'])){
        $doc = array('t' => _te('fp', 'Документы заказчика'), 'user' => $client_id);
        $docs = $documents['client'];
        $aData['tpl'] = 'docs';
        include('admin.workflow.form.tpl.php');
    } ?>
    <? if(isset($documents['worker'])){
        $doc = array('t' => _te('fp', 'Документы исполнителя'), 'user' => $worker_id);
        $docs = $documents['worker'];
        $aData['tpl'] = 'docs';
        include('admin.workflow.form.tpl.php');
    } ?>
</div>



<script type="text/javascript">
var jFairplayWorkflowsForm = (function(){
    var $progress, $form, formChk, id = <?= $id ?>, blocksPrefix = 'fairplay-workflow-form-block-';
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $progress = $('#FairplayWorkflowsFormProgress');
        $form = $('#FairplayWorkflowsForm');

        $form.find('#j-client-expert-email').autocomplete(ajaxUrl+'&act=user',{valueInput: $form.find('#j-client-expert-id')});

        $form.on('click', '.j-client-expert-set', function(e){
            e.preventDefault();
            var $el = $(this);
            expertSet('client', $el, $form.find('#j-client-expert-id').val());
        });

        $form.on('click', '.j-client-expert-cancel', function(e){
            e.preventDefault();
            var $el = $(this);
            expertCancel('client', $el);
        });

        $form.find('#j-worker-expert-email').autocomplete(ajaxUrl+'&act=user', {valueInput: $form.find('#j-worker-expert-id')});

        $form.on('click', '.j-worker-expert-set', function(e){
            e.preventDefault();
            var $el = $(this);
            expertSet('worker', $el, $form.find('#j-worker-expert-id').val());
        });

        $form.on('click', '.j-worker-expert-cancel', function(e){
            e.preventDefault();
            var $el = $(this);
            expertCancel('worker', $el);
        });

        $form.on('click', '.j-expert-view', function(e){
            e.preventDefault();
            bff.userinfo($(this).data('id'));
        });

        initBlock('<?= $tab ?>', false);
        initDocs();
    });

    function expertSet(type, $el, id)
    {
        bff.ajax(ajaxUrl+'&act=expert-set', {id:<?= $id ?>, type:type, expert:id}, function(data){
            if(data && data.success) {
                var $bl = $el.parent();
                $bl.addClass('hide');
                $bl = $bl.next();
                $bl.removeClass('hide');
                $bl = $bl.find('.j-expert-view');
                $bl.data('id', id);
                $bl.text(data.email);

            }
        }, $progress);
    }

    function expertCancel(type, $el)
    {
        bff.ajax(ajaxUrl+'&act=expert-cancel', {id:<?= $id ?>, type:type}, function(data){
            if(data && data.success) {
                $el.parent().addClass('hide').prev().removeClass('hide');
            }
        }, $progress);
    }

    function initBlock(key, $block)
    {
        if($block === false) {
            $block = $('#'+blocksPrefix+key);
        }
        $block.addClass('inited').removeClass('hidden');
        if(key == 'chat'){
            initChat();
        }
    }

    function initChat()
    {
        var $list = $('#j-workflow-chat');
        bff.ajax(ajaxUrl+'&act=chat-more', {workflow:<?= $id ?>, id:0}, function(resp){
            if (resp && resp.success) {
                $list.html(resp.html);

                var $more = $list.find('.j-more');
                $list.scrollTo($list.find('.j-message:last'), {duration:0, offset:50});
                $more.click(function(e){
                    e.preventDefault();
                    var $fst = $list.find('.j-message:first');
                    bff.ajax(ajaxUrl + '&act=chat-more',{workflow:<?= $id ?>, id:$fst.data('id')},function(resp) {
                        if (resp && resp.success) {
                            $more.parent().after(resp.html);
                            if( ! intval(resp.more)){
                                $more.parent().remove();
                            }
                            $list.scrollTo($fst, {duration:0, offset:-10});
                        }
                    });
                });

                var $f = $('#j-chat-form');
                var $message = $f.find('[name="message"]');
                var $attach = $f.find('[name="attach"]');
                bff.iframeSubmit($f, function(resp, errors){
                    if(resp && resp.success) {
                        if(resp.html){
                            $list.append(resp.html);
                            $list.scrollTo($list.find('.j-message:last'), {duration:0, offset:50});
                        }
                        $message.val('');
                        $attach.val('');
                    } else {
                        bff.error(errors);
                    }
                },{
                    beforeSubmit: function(){
                        $f.find('[name="id"]').val($list.find('.j-message:last').data('id'));
                        if( ! $message.val().length && ! $attach.val().length){
                            bff.error('<?= _te('fp', 'Введите текст сообщения'); ?>');
                            return false
                        }
                        return true;
                    },
                    button:$f.find('.j-submit'),
                    url:ajaxUrl + '&act=chat-add'
                });

            }
        });
    }

    function initDocs()
    {
        var $bl = $('#fairplay-workflow-form-block-docs');
        $bl.on('click', '.j-delete', function(e){
            e.preventDefault();
            var $el = $(this);
            if ( ! bff.confirm('sure')) return;
            bff.ajax(ajaxUrl+'&act=doc-delete', {workflow:<?= $id ?>, id:$el.data('id')}, function(resp) {
                if (resp && resp.success) {
                    $el.closest('tr').remove();
                }
            }, $progress);
        });

        $bl.on('click', '.j-add', function(e) {
            e.preventDefault();
            var $el = $(this);
            $.fancybox('', {ajax: true, href: ajaxUrl + '&act=doc-create-popup&workflow=<?= $id ?>&user_id='+ $el.data('user')});
        });
    }

    function docAdd(id, user)
    {
        var docID = $('#j-doc-add-id').val();
        bff.ajax(ajaxUrl+'&act=doc-add', {workflow:id, user_id:user, doc_id:docID}, function(resp) {
            $.fancybox.close();
            if (resp && resp.success) {
                location.reload();
            }
        }, $('#j-popupWorkflowDocAdd-progress'));

    }

    return {
        del: function()
        {
            if( id > 0 ) {
                bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                    false, {progress: $progress, repaint: false, onComplete:function(){
                        bff.success('<?= _te('', 'Запись успешно удалена'); ?>');
                        jFairplayWorkflowsFormManager.action('cancel');
                        jFairplayWorkflowsList.refresh();
                    }});
            }
        },
        save: function(returnToList)
        {
            if( ! formChk.check(true) ) return;
            var $bl = $types.filter(':visible');
            if($bl.find('.j-selected').length){
                $bl.removeClass('clr-error');
            } else {
                $bl.addClass('clr-error');
                return;
            }
            var data = $form.serialize();
            bff.ajax(ajaxUrl, data, function(data){
                if(data && data.success) {
                    bff.success('<?= _te('', 'Данные успешно сохранены'); ?>');
                    if(returnToList || ! id) {
                        jFairplayWorkflowsFormManager.action('cancel');
                        jFairplayWorkflowsList.refresh( ! id);
                    }
                }
            }, $progress);
        },
        onShow: function()
        {
            formChk = new bff.formChecker($form);
        },
        onTab: function(key, tabLink)
        {
            $('[id^="'+blocksPrefix+'"]').addClass('hidden');
            var $block = $('#'+blocksPrefix+key).removeClass('hidden');
            if( ! $block.hasClass('inited')) {
                initBlock(key, $block);
            }
            $(tabLink).parent().addClass('tab-active').siblings().removeClass('tab-active');
            if(bff.h) {
                window.history.pushState({}, document.title, ajaxUrl + '&act=<?= $act ?>&id=<?= $id ?>&ftab=' + key);
            }
        },
        docAdd:docAdd
    };
}());
</script>