<?php
/**
 * @var $this Fairplay
 */
tpl::includeJS(array('comments', 'autocomplete'), true);
?>
<?= tplAdmin::blockStart(_t('fp', 'Ход работ').' / '._t('fp', 'Редактирование'), false, array('id'=>'FairplayWorkflowsFormBlock','style'=>'display:none;')); ?>
<div id="FairplayWorkflowsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('fp', 'Ход работ').' / '._t('fp', 'Список'), true, array('id'=>'FairplayWorkflowsListBlock','class'=>(!empty($act) ? 'hidden' : ''))); ?>
<?
$aTabs = array(
    Fairplay::TAB_WF_ALL         => array('t'=>_t('fp', 'Все')),
    Fairplay::TAB_WF_PAYMENT     => array('t'=>_t('fp', 'Выплата'),  'counter' => config::get('fairplay_payment', 0)),
    Fairplay::TAB_WF_ARBITRAGE   => array('t'=>_t('fp', 'Арбитраж'), 'counter' => config::get('fairplay_arbitrage', 0)),
    Fairplay::TAB_WF_RESERVATION => array('t'=>_t('fp', 'Резервирование')),
    Fairplay::TAB_WF_EXECUTING   => array('t'=>_t('fp', 'Выполняются')),
    Fairplay::TAB_WF_CLOSED      => array('t'=>_t('fp', 'Закрытые')),
    Fairplay::TAB_WF_CANCELED    => array('t'=>_t('fp', 'Отменены')),
);
?>
<div class="tabsBar" id="FairplayWorkflowsListTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab <? if($f['tab']==$k) { ?>tab-active<? } ?>"><a href="#" onclick="return jFairplayWorkflowsList.onTab(<?= $k ?>,this);" <?= (!empty($v['c']) ? $v['c'] : '') ?>><?= $v['t'] ?><? if(! empty($v['counter'])){ ?> (<?= $v['counter'] ?>)<? } ?></a></span>
    <? } ?>
    <div id="FairplayWorkflowsProgress" class="progress" style="display: none;"></div>
</div>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="FairplayWorkflowsListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>" />

        <div class="left" style="margin-left:4px;">
            <input type="text" style="width:130px;" maxlength="150" name="title" placeholder="<?= _t('fp', 'ID / Ключевое слово') ?>" value="<?= HTML::escape($f['title']) ?>" />
        </div>
        <div class="left" style="margin-left:4px;">
            <input type="text" style="width:155px;" maxlength="150" name="user" placeholder="<?= _t('', 'ID / логин / E-mail пользователя') ?>" value="<?= HTML::escape($f['user']) ?>" />
        </div>

        <input type="button" class="left btn btn-small button cancel" style="margin-left: 4px;" onclick="jFairplayWorkflowsList.submit(false);" value="<?= _t('', 'search') ?>" />
        <div class="left" style="margin-left: 8px;"><a class="ajax cancel" onclick="jFairplayWorkflowsList.submit(true); return false;"><?= _t('', 'reset') ?></a></div>
        <div class="clearfix"></div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="FairplayWorkflowsListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">ID</th>
        <th class="left"><?= _t('fp', 'Заказ') ?></th>
        <th width="20"></th>
        <th width="110"><?= _t('', 'Created') ?></th>
        <th width="60"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="FairplayWorkflowsList">
    <?= $list ?>
    </tbody>
</table>
<div id="FairplayWorkflowsListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>
<div>
    <div class="left">

    </div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    var jFairplayWorkflowsFormManager = (function(){
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function(){
            $formContainer = $('#FairplayWorkflowsFormContainer');
            $progress = $('#FairplayWorkflowsProgress');
            $block = $('#FairplayWorkflowsFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if(!empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
        });

        function onFormToggle(visible)
        {
            if(visible) {
                jFairplayWorkflowsList.toggle(false);
                if(jFairplayWorkflowsForm) jFairplayWorkflowsForm.onShow();
            } else {
                jFairplayWorkflowsList.toggle(true);
            }
        }

        function initForm(type, id, params)
        {
            if( process ) return;
            bff.ajax(ajaxUrl,params,function(data){
                if(data && (data.success || intval(params.save)===1)) {
                    $blockCaption.html((type == 'add' ? '<?= _t('fp', 'Ход работ') ?> / <?= _t('fp', 'Добавление') ?>' : '<?= _t('fp', 'Ход работ') ?> / <?= _t('fp', 'Редактирование') ?>'));
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo( $blockCaption, {duration:500, offset:-300});
                    onFormToggle(true);
                    if(bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                    }
                } else {
                    jFairplayWorkflowsList.toggle(true);
                }
            }, function(p){ process = p; $progress.toggle(); });
        }

        function action(type, id, params)
        {
            <? $tab = $this->input->get('ftab', TYPE_NOTAGS); ?>
            params = $.extend(params || {}, {act:type<?= ! empty($tab) ? ',ftab:\''.$tab.'\'' : '' ?>});
            switch(type) {
                case 'add':
                {
                    if( id > 0 ) return action('edit', id, params);
                    if($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                } break;
                case 'cancel':
                {
                    $block.hide();
                    onFormToggle(false);
                } break;
                case 'edit':
                {
                    if( ! (id || 0) ) return action('add', 0, params);
                    params.id = id;
                    initForm(type, id, params);
                } break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jFairplayWorkflowsList = (function(){
        var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, tab = <?= $f['tab'] ?>, reg = {$ac:0,api:0, data:{}};
        var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';

        $(function(){
            $progress  = $('#FairplayWorkflowsProgress');
            $block     = $('#FairplayWorkflowsListBlock');
            $list      = $block.find('#FairplayWorkflowsList');
            $listTable = $block.find('#FairplayWorkflowsListTable');
            $listPgn   = $block.find('#FairplayWorkflowsListPgn');
            filters    = $block.find('#FairplayWorkflowsListFilters').get(0);

            $list.delegate('a.workflow-edit', 'click', function(){
                var $el = $(this);
                var id = intval($el.attr('rel'));
                var params = false;
                var tab = $el.data('tab');
                if(tab){
                    params = {ftab:tab};
                }
                if(id>0) jFairplayWorkflowsFormManager.action('edit',id, params);
                return false;
            });

            $list.delegate('a.workflow-del', 'click', function(){
                var id = intval($(this).attr('rel'));
                if(id>0) del(id, this);
                return false;
            });

            $list.on('click', '.j-workflow-info', function(e){
                e.preventDefault();
                $.fancybox('', {ajax:true, href:ajaxUrl+'info&id='+$(this).data('id')});
            });

            $(window).bind('popstate',function(){
                if('state' in window.history && window.history.state === null) return;
                var loc = document.location;
                var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                if( actForm!=null ) {
                    var actId = /id=([\d]+)/.exec(loc.search.toString());
                    jFairplayWorkflowsFormManager.action(actForm[1], actId && actId[1]);
                } else {
                    jFairplayWorkflowsFormManager.action('cancel');
                    updateList(false);
                }
            });
        });

        function isProcessing()
        {
            return processing;
        }

        function del(id, link)
        {
            bff.ajaxDelete('<?= _te('', 'Delete?'); ?>', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
            return false;
        }

        function updateList(updateUrl)
        {
            if(isProcessing()) return;
            var f = $(filters).serialize();
            bff.ajax(ajaxUrl, f, function(data){
                if(data) {
                    $list.html( data.list );
                    $listPgn.html( data.pgn );
                    if(updateUrl !== false && bff.h) {
                        window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                    }
                }
            }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
        }

        function setPage(id)
        {
            filters.page.value = intval(id);
        }

        return {
            submit: function(resetForm)
            {
                if(isProcessing()) return false;
                setPage(1);
                if(resetForm) {
                    filters['title'].value = '';
                    filters['user'].value = '';
                }
                updateList();
            },
            page: function (id)
            {
                if(isProcessing()) return false;
                setPage(id);
                updateList();
            },
            onTab: function(tabNew, link)
            {
                if(isProcessing() || tabNew == tab) return false;
                setPage(1);
                tab = filters.tab.value = tabNew;
                updateList();
                $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
                return false;
            },
            refresh: function(resetPage,updateUrl)
            {
                if(resetPage) setPage(0);
                updateList(updateUrl);
            },
            toggle: function(show)
            {
                if(show === true) {
                    $block.show();
                    if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                }
                else $block.hide();
            }
        };
    }());
</script>