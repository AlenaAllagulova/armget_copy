<?php
?>
<div class="modal fade" id="j-modal-change-<?= $id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('fp', 'Изменить условия заказа'); ?></h4>
            </div>
            <div class="modal-body">

                <p><strong><?= _t('fp', '[title] (обсуждение условий)', array('title' => $title)); ?></strong></p>

                <form class="form-horizontal" method="post" action="" role="form">
                    <input type="hidden" name="id" value="<?= $id ?>" />

                    <div class="form-group o-propose-inputs">
                        <label for="j-workflow-change-period" class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Срок'); ?></label>
                        <div class="col-sm-9">
                            <div class="input-group j-required">
                                <input type="text" name="term" id="j-workflow-change-period" class="form-control" value="<?= $term ?>">
                                <div class="o-propose-inputs-txt"><?= _t('fp', 'дней'); ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group o-propose-inputs">
                        <label for="j-workflow-change-price" class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Бюджет'); ?></label>
                        <div class="col-sm-9">
                            <div class="input-group j-required">
                                <input type="text" name="price" id="j-workflow-change-price" class="form-control input-sm" value="<?= $price ?>">
                                <div class="o-propose-inputs-txt"><?= Site::currencyDefault('title_short'); ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Способ оплаты'); ?></label>
                        <div class="col-sm-9">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_USE ?>" <?= $fairplay == Orders::FAIRPLAY_USE ? 'checked="checked"' : '' ?> />
                                    <?= _t('fp', 'Безопасная сделка (с резервированием бюджета) [link]', array('link' => '<a href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-question-circle"></i> </a>')); ?>
                                </label>
                                <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Безопасное сотрудничество с гарантией возврата денег. Вы резервируете бюджет заказа, а мы гарантируем вам возврат суммы, если работа выполнена некачественно или не в срок.'); ?></div>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_NONE ?>" <?= $fairplay == Orders::FAIRPLAY_NONE ? 'checked="checked"' : '' ?> />
                                    <?= _t('fp', 'Прямая оплата исполнителю на его кошелек/счет'); ?>
                                </label>
                                <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Сотрудничество без участия сайта в процессе оплаты. Вы сами договариваетесь с исполнителем о способе и порядке оплаты. И самостоятельно регулируете все претензии, связанные с качеством и сроками выполнения работы.'); ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3 o-control-label"></div>
                        <div class="col-sm-9 c-formSubmit">
                            <button class="btn btn-primary c-formSuccess j-submit"><?= _t('fp', 'Сохранить'); ?></button>
                            <a data-dismiss="modal" href="#" class="c-formCancel ajax-link"><span><?= _t('fp', 'Отмена'); ?></span></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
