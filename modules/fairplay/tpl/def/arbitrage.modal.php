<?php
?>
<div class="modal fade" id="j-modal-arbitrage-<?= $id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= _t('fp', 'Обратиться в Арбитраж'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= _t('fp', 'Пожалуйста, укажите по какой причине вы хотите обратиться в Арбитраж.'); ?></p>
                <form action="" method="post">
                    <input type="hidden" name="id" value="<?= $id ?>" />
                    <div class="form-group j-required">
                        <textarea rows="5" class="form-control" name="message" placeholder="<?= _t('fp', 'Причина обращения'); ?>"></textarea>
                        <div class="help-block j-help-block"></div>
                    </div>
                    <div class="form-group c-formSubmit">
                        <button type="submit" class="btn btn-success c-formSuccess j-submit"><?= _t('fp', 'Начать Арбитраж'); ?></button>
                        <a class="c-formCancel ajax-link" href="#" data-dismiss="modal"><span><?= _t('', 'Отмена'); ?></span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
