<?php
?>
<ul class="l-payment-methods">
    <? foreach($paySystems as $k => $v): ?>
    <li>
        <a href="#" class="j-ps" data-ps="<?= $k ?>">
            <div class="l-payment-methods-img">
                <span>
                    <img src="<?= $v['logo_desktop'] ?>" alt="<?= HTML::escape($v['title']); ?>" />
                </span>
            </div>
            <div class="l-payment-methods-title">
                <?= $v['title'] ?>
            </div>
        </a>
    </li>
    <? endforeach; ?>
</ul>

