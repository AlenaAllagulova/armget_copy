<?php
/** @var $this Fairplay */
$userID = User::id();
if( ! empty($list)): ?>
<ul class="l-projectList o-projectList">
    <? foreach($list as $v): ?>
        <li>
            <header class="l-project-title">
                <? if($v['visibility'] == Orders::VISIBILITY_PRIVATE): ?><i class="fa fa-eye-slash show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Приватный заказ'); ?>"></i><? endif; ?>
                <? if($v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
                <a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a>
                <? if( ! empty($v['cnt_new'])): ?><span class="label label-new">+<?= $v['cnt_new'] ?></span><? endif; ?>
            </header>

            <div class="l-project-terms">
                <span class="l-project-terms-budget"><?= _t('fp', 'Бюджет:'); ?> <strong><?= tpl::formatPrice($v['price']) ?> <?= Site::currencyDefault('title_short'); ?></strong></span>,
                <span class="l-project-terms-time"><?= _t('fp', 'Срок:'); ?> <strong><?= Fairplay::tplTerm($v['term']) ?></strong></span>
            </div>

            <? if($userID == $v['client_id']): ?>
                <div class="l-project-author">
                    <?= _t('fp', 'Исполнитель:'); ?> <?= tpl::userLink($v['worker'], 'no-login') ?>
                </div>
            <? else: ?>
                <div class="l-project-author">
                    <?= _t('fp', 'Заказчик:'); ?> <?= tpl::userLink($v['client'], 'no-login') ?>
                </div>
            <? endif; ?>
            <?= $this->viewPHP($v, 'workflow.status'); ?>
        </li>
    <? endforeach; ?>
</ul>
<? else: $v = array('jsOnly' => 1); ?>
    <div class="alert alert-info"><?= _t('fp', 'Заказы не найдены'); ?></div>
    <?= $this->viewPHP($v, 'workflow.status'); ?>
<? endif; ?>


