<?php
if(empty($chat['list'])): ?>
<div class="j-message"></div>
<? return; endif;
$userID = User::id();
$chat['list'] = array_reverse($chat['list']);
$day = '';
if( ! isset($users)) {
    $users = array();
}
if( ! empty($chat['more'])): ?><div class="text-center"><a class="j-more" href="#"><?= _t('', 'еще') ?></a></div><? endif;
foreach($chat['list'] as $v){
    $created = strtotime($v['created']); $dte = date('d.m.Y', $created);
    $authorID = $v['author_id'];
    if( ! isset($users[$authorID])){
        $users[$authorID] = Users::model()->userData($authorID, array('user_id', 'login', 'name', 'surname', 'pro', 'sex', 'verified', 'avatar'));
    }
    $user = $users[$authorID];
    $groupClient = array($client_id, $client_expert_id);
    $groupWorker = array($worker_id, $worker_expert_id);
    $userMain = in_array($userID, array($client_id, $worker_id));
    switch($v['type']){
        case Fairplay::CHAT_TYPE_MESSAGE:    
            $msgClass = 'i-message-third';
            if(in_array($authorID, array($client_id, $worker_id, $client_expert_id, $worker_expert_id))){
                $msgClass = 'i-message-others';
            }
            if((in_array($authorID, $groupClient) && in_array($userID, $groupClient))
            || (in_array($authorID, $groupWorker) && in_array($userID, $groupWorker))){
                $msgClass = 'i-message-self';
            }
            $label = '<div class="i-message-author-label"><span class="label label-arbitrator">'._t('fp', 'Арбитр').'</span></div>';
            if($authorID == $client_id){
                $label = $userMain ? '' : '<div class="i-message-author-label"><span class="label label-client">'._t('', 'Заказчик').'</span></div>';
            }else if($authorID == $worker_id){
                $label = $userMain ? '' : '<div class="i-message-author-label"><span class="label label-worker">'._t('', 'Исполнитель').'</span></div>';
            }else if($authorID == $client_expert_id){
                $label = '<div class="i-message-author-label"><span class="label label-client">'._t('fp', 'Эксперт от заказчика').'</span></div>';
            }else if($authorID == $worker_expert_id){
                $label = '<div class="i-message-author-label"><span class="label label-worker">'._t('fp', 'Эксперт от исполнителя').'</span></div>';
            }
        
            if($day != $dte): $day = $dte; ?><div class="i-message-date j-date" data-d="<?= date('Ymd', $created) ?>"><?= $dte ?></div><? endif; ?>
            <div class="i-message-box j-message" data-id="<?= $v['id'] ?>">
                <div class="<?= $msgClass ?>">
                    <div class="i-message">
                        <div class="i-message-avatar">
                            <?= tpl::userAvatar($user) ?>
                        </div>
                        <div class="i-message-content">
                            <div class="i-message-author">
                                <?= $label; ?>
                                <? if( ! empty($v['extra']['arbitrage'])): ?>
                                    <span class="i-message-author-arbitrage">
                                        <i class="fa fa-warning show-tooltip" title="" data-original-title="<?= _t('fp', 'обращение в арбитраж'); ?>"></i>
                                    </span>
                                <? endif; ?>
                                <?= tpl::userLink($user, 'no-login') ?>
                            </div>
                            <div><?= nl2br($v['message']) ?></div>
                            <? if( ! empty($v['extra']['attach'])): ?>
                                <div class="i-message-addition">
                                    <? foreach($v['extra']['attach'] as $vv): ?>
                                        <a href="<?= $vv['url'] ?>" target="_blank"><i class="fa fa-file-o c-link-icon"></i><?= $vv['rfilename'] ?></a>
                                    <? endforeach; ?>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="i-message-time"><?= date('H:i', $created); ?></div>
                </div>
            </div>
        <? break; # Fairplay::CHAT_TYPE_MESSAGE
        case Fairplay::CHAT_TYPE_START: ?>
            <div class="i-message-in j-message" data-id="<?= $v['id'] ?>">
                <div class="alert alert-start mrgb0">
                    <i class="fa fa-briefcase alert-ico-right"></i>
                    <?= _t('fp', 'Начало работы'); ?> <?= tpl::date_format3($v['created']); ?>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_COMPLETE: ?>
            <div class="i-message-in j-message" data-id="<?= $v['id'] ?>">
                <div class="alert alert-finished mrgb0">
                    <i class="fa fa-check alert-ico-right"></i>
                    <?= tpl::userLink($user, 'no-login text'); ?> <?= _t('fp', 'завершил работу'); ?> <?= tpl::date_format3($v['created']); ?>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_REWORK: ?>
            <div class="i-message-in j-message" data-id="<?= $v['id'] ?>">
                <div class="alert alert-return mrgb0">
                    <i class="fa fa-reply alert-ico-right"></i>
                    <?= tpl::userLink($user, 'no-login text'); ?> <?= _t('fp', 'вернул проект в работу'); ?> <?= tpl::date_format3($v['created']); ?>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_ARBITRAGE_START: ?>
            <div class="i-message-in j-message" data-id="<?= $v['id'] ?>">
                <div class="alert alert-arbitrage mrgb0">
                    <i class="fa fa-warning alert-ico-right"></i>
                    <?= tpl::userLink($user, 'no-login text'); ?> <?= _t('fp', 'начал арбитраж'); ?> <?= tpl::date_format3($v['created']); ?>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_ARBITRAGE_STOP: ?>
            <div class="i-message-in j-message" data-id="<?= $v['id'] ?>">
                <div class="alert alert-arbitrage alert-arbitrage-finished mrgb0">
                    <i class="fa fa-warning alert-ico-right"></i><?= _t('fp', 'Арбитраж окончен'); ?> <?= tpl::date_format3($v['created']); ?>
                </div>
            </div>
        <? break;
        }
    }
