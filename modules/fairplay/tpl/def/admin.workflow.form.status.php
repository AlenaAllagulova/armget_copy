<?php
/**
 * @var $this Fairplay
 */
$is_popup = ! empty($is_popup);
?>
<script type="text/javascript">
var jFairplayWorkflowStatus = (function(){
    var $block, url = '<?= $this->adminLink('workflows&act=', 'fairplay'); ?>';
    $(function(){
        $block = $('#j-workflow-status-block');
    });

    function _progress() {
        $block.toggleClass('disabled');
    }

    function statusesHistory()
    {
        bff.ajax(url+'statuses-history', {id:<?= $id ?>}, function(resp){
            if(resp && resp.success) {
                $('#j-statuses-history-<?= $id ?>').remove();
                $block.after(resp.html);
            }
        }, _progress);
        return false;
    }

    return {
        closeArbitrage: function(){
            bff.ajax(url+'arbitrage-close', {id:<?= $id ?>}, function(resp){
                if(resp && resp.success) {
                    $('.j-arbitrage-block').remove();
                }
            }, _progress);
            return false;
        },
        setStatus:function(status, confirm){
            if(confirm && ! bff.confirm('<?= _te('', 'Sure?'); ?>')) return false;
            bff.ajax(url+'set-status', {id:<?= $id ?>, status:status}, function(resp){
                if(resp && resp.success) {
                    <? if($is_popup): ?>
                        $.fancybox.close();
                    <? else: ?>
                        if(typeof(jFairplayWorkflowsFormManager) == 'object') {
                            jFairplayWorkflowsFormManager.action('cancel');
                            jFairplayWorkflowsFormManager.action('edit', <?= $id ?>);
                        }
                    <? endif; ?>
                    if(typeof(jFairplayWorkflowsList) == 'object'){
                        jFairplayWorkflowsList.refresh();
                    }
                }
            }, _progress);
            return false;
        },
        statusesHistory:statusesHistory
    };
}());
</script>
<div id="j-workflow-status-block">
<table class="admtbl tbledit">
    <tr>
        <td class="row1 field-title<? if($is_popup) { ?> right<? } ?>" style="width:<?= ( $is_popup ? 140 : 145 ) ?>px;"><?= _t('fp', 'Статус:'); ?></td>
        <td class="row2">
            <? $hist = ' <a href="#" onclick="return jFairplayWorkflowStatus.statusesHistory();"><i class="icon icon-time disabled" title="'._te('fp', 'История изменения статусов').'"></i></a>';
                switch($status){
                case Fairplay::STATUS_RESERVATION: ?>
                    <strong><?= _t('fp', 'Резервирование средств'); ?></strong><?= $hist; ?><br />
                    <a onclick="return jFairplayWorkflowStatus.setStatus(<?= Fairplay::STATUS_EXECUTION ?>);" class="btn btn-mini" href="#"><?= _t('fp', 'Выполнение работы') ?></a>
                    <a onclick="return jFairplayWorkflowStatus.setStatus(<?= Fairplay::STATUS_CANCEL ?>, 1);" class="btn btn-mini text-error" href="#"><?= _t('fp', 'Отменен') ?></a>
                <? break;
                case Fairplay::STATUS_EXECUTION: ?>
                    <strong><?= _t('fp', 'Выполнение работы'); ?></strong><?= $hist; ?>
                <? break;
                case Fairplay::STATUS_REWORK: ?>
                    <strong><?= _t('fp', 'Доработка'); ?></strong><?= $hist; ?>
                <? break;
                case Fairplay::STATUS_PAYMENT: ?>
                    <strong><?= _t('fp', 'Выплата суммы'); ?></strong><?= $hist; ?><br />
                    <a onclick="return jFairplayWorkflowStatus.setStatus(<?= Fairplay::STATUS_CLOSED ?>);" class="btn btn-mini" href="#"><?= _t('fp', 'Закрыт') ?></a>
                    <a onclick="return jFairplayWorkflowStatus.setStatus(<?= Fairplay::STATUS_CANCEL ?>, 1);" class="btn btn-mini text-error" href="#"><?= _t('fp', 'Отменен') ?></a>
                <? break;
                case Fairplay::STATUS_CLOSED: ?>
                    <strong><?= _t('fp', 'Закрыт'); ?></strong><?= $hist; ?>
                <? break;
                case Fairplay::STATUS_CANCEL: ?>
                    <strong><?= _t('fp', 'Отменен'); ?></strong><?= $hist; ?>
                <? break;
            } ?>

        </td>
    </tr>
    <? if($arbitrage && $is_popup): ?>
        <tr class="j-arbitrage-block">
            <td class="row1 field-title<? if($is_popup) { ?> right<? } ?>"><?= _t('fp', 'Обращение в арбитраж:'); ?></td>
            <td class="row2">
                <? if( ! empty($arbitrage_message)): ?>
                    <?= $arbitrage_message ?>
                <? endif; ?>
                <a onclick="return jFairplayWorkflowStatus.closeArbitrage();" class="btn btn-mini btn-success" href="#"><?= _t('fp', 'Завершить арбитраж') ?></a>
            </td>
        </tr>
    <? endif; ?>
</table>
</div>