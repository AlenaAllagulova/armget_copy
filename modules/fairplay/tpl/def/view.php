<?php
/**
 * Просмотр хода работ
 * @var $this Fairplay
 */
tpl::includeJS('fairplay.workflow.view', false, 1);
$userID = User::id();
?>
<div class="container">

    <?= tpl::getBreadcrumbs($order['breadcrumbs']); ?>

    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-9" id="j-workflow-view">
                <div class="l-borderedBlock">
                    <div class="l-inside o-clientInfo">
                            <a href="<?= Users::url('profile', array('login' => $order['login'])) ?>" class="o-client-avatar">
                                <img src="<?= $order['avatar_small'] ?>" alt="<?= tpl::avatarAlt($order); ?>" />
                                <?= tpl::userOnline($order) ?>
                            </a>
                            <div class="o-client-about">
                                <div>
                                    <?= tpl::userLink($order, 'no-login') ?>
                                </div>
                                <div class="visible-lg visible-md visible-sm">
                                    <?= _t('orders', 'Зарегистрирован на сайте') ?> <?= tpl::date_format_spent($order['user_created'], false, false)?>
                                </div>
                                <div class="o-feedbacks-inf">
                                    <?= _t('orders', 'Отзывы фрилансеров:') ?> <?= tpl::opinions($order['opinions_cache'], array('login' => $order['login'])) ?>
                                </div>
                            </div>
                            <?= Users::i()->note($order['user_id']); ?>
                    </div>

                    <div class="l-inside" id="j-fairplay-statuses-block">
                        <header class="title-type-2 j-title">
                            <div class="o-project-price o-project-price_hide-sm relative"><?= tpl::formatPrice($price) ?> <?= Site::currencyDefault('title_short'); ?></div>
                            <h1><? if($order['visibility'] == Orders::VISIBILITY_PRIVATE): ?><i class="fa fa-eye-slash show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Приватный заказ'); ?>"></i> <? endif;
                                if($fairplay == Fairplay::FAIRPLAY_USE): ?><i data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip" class="fa fa-shield c-safe-color show-tooltip"></i><? endif; ?>
                                <?= $order['title'] ?>
                            </h1>
                            <div class="clearfix"></div>
                            <div class="l-project-terms mrgt0">
                                <span class="l-project-terms-time"><?= _t('fp', 'Срок:'); ?> <strong><?= Fairplay::tplTerm($term) ?></strong></span>
                            </div>

                            <div class="l-project-author mrgt0">
                                <?= _t('orders', 'Исполнитель:'); ?> <span class="nowrap"><?= tpl::userLink($worker, 'no-login') ?></span>
                            </div>
                        </header>

                        <?= $this->viewPHP($aData, 'workflow.status'); ?>

                        <div class="l-project-descr">
                            <?= nl2br($order['descr']) ?>
                        </div>

                        <? if(Orders::imagesLimit() && $order['imgcnt']): ?>
                            <div id="j-workflow-view-carousel" class="owl-carousel l-carousel l-images-carousel">
                                <? foreach($order['images'] as $k => $v): ?>
                                    <div class="item">
                                        <a href="<?= $v['i'][ OrdersOrderImages::szView ]; ?>" class="fancyzoom" rel="fancy-gallery-<?= $id ?>">
                                            <img src="<?= $v['i'][ OrdersOrderImages::szSmall ]; ?>" alt="<?= tpl::imageAlt(array('t' => $order['title'], 'k' => $k)); ?>" />
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>

                        <? if(Orders::attachmentsLimit() && $order['attachcnt']): ?>
                            <ul class="o-order-files">
                                <? foreach($order['attachments'] as $v): ?>
                                    <li><a href="<?= $v['i'] ?>" target="_blank"><i class="fa fa-file-o c-li c-link-icon"></i><span><?= $v['origin'] ?></span></a> (<?= $v['size'] ?>)</li>
                                <? endforeach; ?>
                            </ul>
                        <? endif; ?>
                    </div>

                    <? if( ! empty($order['tags'])): ?>
                        <div class="l-inside">
                            <? foreach($order['tags'] as $v): ?>
                                <a href="<?= Orders::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                            <? endforeach; ?>
                        </div>
                    <?  endif; ?>

                    <? if( ! empty($order['dynprops'])): ?><div class="l-inside l-dymanic-features"><?= $order['dynprops'] ?></div><? endif; ?>
                </div>

                <? if( ! empty($history)): ?>
                <div class="l-project-history">
                    <h5><?= _t('fp', 'История изменений заказа'); ?></h5>
                    <ul class="list-unstyled">
                        <? foreach($history as $v): ?>
                        <li><?= tpl::date_format3($v['created']) ?> - <?= $v['descr'] ?></li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <? endif; ?>

                <? if( ! empty($documents)): ?>
                <div class="l-project-history">
                    <h5><?= _t('fp', 'Документы'); ?></h5>
                    <ul class="list-unstyled">
                        <? foreach($documents as $v): ?>
                        <li><a href="<?= $v['link'] ?>" target="_blank"><?= $v['title'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <? endif; ?>

                <a name="chat"></a>
                <div class="l-project-chat">
                    <h5><?= _t('fp', 'Обсуждение заказа'); ?></h5>

                    <div class="i-imailDialog-window l-project-chat-window" id="j-chat-list">
                        <?= $chat ?>
                    </div>

                    <form class="i-imailDialog-form form l-project-chat-form" method="post" enctype="multipart/form-data" action="" id="j-chat-form">
                        <input type="hidden" name="workflow" value="<?= $id ?>" />
                        <input type="hidden" name="id" value="0" />
                        <div class="form-group">
                            <textarea rows="4" class="form-control" name="message" placeholder="<?= _t('', 'Сообщение'); ?>"></textarea>
                        </div>
                        <button class="btn btn-sm btn-primary j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('', 'Отправить'); ?></button>
                        <div class="pull-right">
                            <button class="btn btn-sm btn-default pull-right j-attach"><?= _t('', 'Прикрепить файл'); ?></button>
                            <input type="file" class="hidden" name="attach" value="" />
                        </div>
                        <div class="clearfix"></div>

                        <div class="help-block text-right mrgb0"><?= _t('fp', 'Максимальный размер файла - [size]', array('size' => $chat_size)); ?></div>
                    </form>

                </div>

            </div>

            <div class="col-md-3 visible-lg visible-md">

                <div class="l-project-steps">
                    <ul>
                        <? if( ! $fairplay): ?>
                            <li class="active-ar">
                                <span><?= _t('fp', 'Обратите внимание'); ?></span>
                                <div class="l-project-steps-descr">
                                    <?= _t('fp', 'Тип оплаты в данном заказе - <strong>Прямая оплата</strong>, при которой вы самостоятельно регулируете все претензии по качеству, срокам и оплате работы.<br>
                                    Для безопасной работы в заказе рекомендуем оплату через <a [link]>Безопасную сделку</a>.', array(
                                        'link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"',
                                    )); ?>
                                </div>
                            </li>
                            <li class="done">
                                <span><?= _t('fp', 'Согласование условий'); ?></span>
                            </li>
                            <li class="<?= $status == Fairplay::STATUS_EXECUTION ? 'active' : 'done' ?>">
                                <i class="fa fa-wrench"></i>
                                <span><?= _t('fp', 'Выполнение работы'); ?></span>
                                <div class="l-project-steps-descr">
                                    <?= _t('fp', 'Процесс выполнения задания в заказе до получения заказчиком итогового результата.'); ?>
                                </div>
                            </li>
                            <? if($status != Fairplay::STATUS_CLOSED): ?>
                                <li>
                                    <i class="fa fa-check"></i>
                                    <span><?= _t('fp', 'Завершение заказа'); ?></span>
                                    <div class="l-project-steps-descr">
                                        <?= _t('fp', 'Описание шага завершения заказа.'); ?>
                                    </div>
                                </li>
                            <? else: ?>
                                <li class="active">
                                    <i class="fa fa-check"></i>
                                    <span><?= _t('fp', 'Заказ завершен'); ?></span>
                                </li>
                            <? endif; ?>
                        <? else: ?>
                            <li class="done">
                                <span><?= _t('fp', 'Согласование условий'); ?></span>
                            </li>
                            <? if($status == Fairplay::STATUS_CANCEL): ?>
                                <li class="active">
                                    <i class="fa fa-close"></i>
                                    <span><?= _t('fp', 'Заказ отменен'); ?></span>
                                </li>
                            <? else: ?>
                                <? if($status == Fairplay::STATUS_RESERVATION): ?>
                                    <li class="active">
                                        <i class="fa fa-money"></i>
                                        <span><?= _t('fp', 'Резервирование'); ?></span>
                                        <div class="l-project-steps-descr">
                                            <?= _t('fp', 'Резервирование заказчиком суммы оплаты по заказу. Деньги перечисляются и хранятся на сайте до завершения заказа.'); ?>
                                        </div>
                                    </li>
                                <? else: ?>
                                    <li class="done">
                                        <i class="fa fa-money"></i>
                                        <span><?= _t('fp', 'Резервирование'); ?></span>
                                    </li>
                                <? endif; ?>
                                <? $class = '';
                                if(in_array($status, array(Fairplay::STATUS_CLOSED, Fairplay::STATUS_PAYMENT))){
                                    $class = 'done';
                                }else{
                                    if($status == Fairplay::STATUS_EXECUTION){
                                        $class = 'active';
                                    }
                                }
                                ?>
                                <? if($status != Fairplay::STATUS_REWORK): ?>
                                <li class="<?= $class ?>">
                                    <i class="fa fa-wrench"></i>
                                    <span><?= _t('fp', 'Выполнение работы'); ?></span>
                                    <div class="l-project-steps-descr">
                                        <?= _t('fp', 'Процесс выполнения задания в заказе до получения заказчиком итогового результата.'); ?>
                                    </div>
                                </li>
                                <? else: ?>
                                    <li class="active">
                                        <i class="fa fa-repeat"></i>
                                        <span><?= _t('fp', 'Доработка'); ?></span>
                                        <div class="l-project-steps-descr">
                                            <?= _t('fp', 'Процесс выполнения задания в заказе до получения заказчиком итогового результата.'); ?>
                                        </div>
                                    </li>
                                <? endif; ?>
                                <? if( ! empty($arbitrage)): ?>
                                    <li class="active active-ar">
                                        <i class="fa fa-warning"></i>
                                        <span><?= _t('fp', 'Арбитраж'); ?></span>
                                        <div class="l-project-steps-descr">
                                            <?= _t('fp', 'В процессе выполнения работы возникли трудности и проблема решается с помощью арбитра.'); ?>
                                        </div>
                                    </li>
                                <? endif; ?>
                                <? $class = '';
                                if($status == Fairplay::STATUS_CLOSED){
                                    $class = 'done';
                                }else{
                                    if($status == Fairplay::STATUS_PAYMENT){
                                        $class = 'active';
                                    }
                                }
                                ?>
                                <li class="<?= $class ?>">
                                    <i class="fa fa-money"></i>
                                    <span><?= _t('fp', 'Выплата суммы'); ?></span>
                                    <div class="l-project-steps-descr">
                                        <?= _t('fp', 'Описание шага выплаты суммы.'); ?>
                                    </div>
                                </li>
                                <? $class = '';
                                if($status == Fairplay::STATUS_CLOSED){
                                    $class = 'done';
                                }
                                ?>
                                <li class="<?= $class ?>">
                                    <i class="fa fa-check"></i>
                                    <span><?= _t('fp', 'Заказ завершен'); ?></span>
                                </li>
                            <? endif ; ?>
                        <? endif; ?>
                    </ul>
                </div>

                <? # Баннер: Заказы: просмотр ?>
                <?= Banners::view('orders_view', array('pos'=>'right', 'spec'=>(!empty($order['specs'][0]['spec_id']) ? $order['specs'][0]['spec_id'] : 0))) ?>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
<? js::start() ?>
    jFairplayWorkflowView.init(<?= func::php2js(array(
        'lang' => array(
            'chat_empty' => _t('fp', 'Введите текст сообщения'),
        ),
        'id' => $id,
    )) ?>);
<? if(Orders::imagesLimit() && $order['imgcnt']):
    tpl::includeCSS(array('owl.carousel'), true);
    tpl::includeJS('owl.carousel.min', false);
    tpl::includeJS('fancybox2', true);
    ?>
    $(function(){
        var $carousel = $('#j-workflow-view-carousel');
        if($carousel.length){
            $carousel.owlCarousel({
                items : 5, //10 items above 1000px browser width
                itemsDesktop : [1000,4], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,3], // betweem 900px and 601px
                itemsTablet: [600,1], //2 items between 600 and 0
                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                navigation : true,
                navigationText : ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                pagination : true,
                autoPlay : false
            });
        }
        $('.fancyzoom').fancybox({
            openEffect	: 'none',
            closeEffect	: 'none',
            nextEffect  : 'fade',
            prevEffect : 'fade',
            fitToView: false,
            helpers: {
                overlay: {locked: false}
            }
        });
    });
<? endif; ?>
<? js::stop() ?>
</script>