<?php
/**
 * @var $this Fairplay
 */
$tabs = array(
    'general'    => array('t' => 'Общие'),
    'documents'  => array('t' => 'Документы'),
);
$tab = $this->input->get('tab', TYPE_STR);
if(empty($tab)){
    $tab = 'general';
}
$cnt = 1;
$commission = function($v = array()) use(& $cnt){
    $i = $cnt;
    if(empty($v)){
        $i = '__cnt__';
    }
    ob_start(); ob_implicit_flush(false);
?><div class="j-commission">
<input type="text" class="short" name="commissions[<?= $i ?>][from]" value="<?= ! empty($v['from']) ? $v['from'] : '' ?>" placeholder="от" />
<input type="text" class="short" name="commissions[<?= $i ?>][to]" value="<?= ! empty($v['to']) ? $v['to'] : '' ?>" placeholder="до" />
<input type="text" class="short" name="commissions[<?= $i ?>][sum]" value="<?= ! empty($v['sum']) ? $v['sum'] : '' ?>" placeholder="сумма" />
<select name="commissions[<?= $i ?>][p]" style="width: 55px;" >
    <option value="0"<?= isset($v['p']) && $v['p'] == 0 ? ' selected="selected"' : '' ?>>%</option>
    <option value="1"<?= isset($v['p']) && $v['p'] == 1 ? ' selected="selected"' : '' ?>><?= Site::currencyDefault('title_short'); ?></option>
</select>
<a class="but cross j-remove" href="#"></a>
</div><?
    $cnt++;
    return ob_get_clean();
};
?>
<?= tplAdmin::blockStart(_t('fp', 'Безопасная сделка').' / '._t('','Settings'), false, array('id'=>'FairplaySettingsFormBlock')); ?>
    <div class="tabsBar" id="FairplaySettingsFormTabs">
        <? foreach($tabs as $k=>$v) { ?>
            <span class="tab<? if($k == $tab) { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v['t'] ?></a></span>
        <? } ?>
        <div id="FairplaySettingsFormProgress" class="progress" style="display: none;"></div>
        <div class="clear"></div>
    </div>
    <div class="j-tab j-tab-general hidden">
        <form name="FairplaySettingsForm" id="FairplaySettingsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
            <input type="hidden" name="act" value="save" />
        <table class="admtbl tbledit">
            <tr>
                <td class="row1" width="100"><span class="field-title"><?= _t('fp', 'Комиссия:'); ?></span></td>
                <td class="row2">
                    <div class="j-commissions">
                        <? if( ! empty($commissions)): foreach($commissions as $v): ?>
                            <?= $commission($v); ?>
                        <? endforeach; endif; ?>
                    </div>
                    <a href="#" class="ajax desc j-commission-add" title="<?= _te('', '+ добавить'); ?>"><?= _t('', '+ добавить'); ?></a>
                </td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
            <?= $this->locale->buildForm($aData, 'fairplay-finance-requirement', '
                <tr>
                    <td class="row1" width="120"><?= _t(\'fp\', \'Финансовая проверка:\'); ?><br /><span class="desc small"><?= _t(\'fp\', \'(требования)\'); ?></span></td>
                    <td class="row2"><?= tpl::jwysiwyg((isset($aData[\'verified_requirement_\'.$key]) ? $aData[\'verified_requirement_\'.$key] : \'\'), \'verified_requirement[\'.$key.\']\', 0, 120); ?></td>
                </tr>
            '); ?>
        </table>
        <div style="margin-top: 10px;">
            <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jFairplaySettingsForm.save(false);" />
        </div>
        </form>

    </div>

    <div class="j-tab j-tab-documents hidden">
    </div>

<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
    var jFairplaySettingsForm =
        (function(){
            var $progress, $block, $form, f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#FairplaySettingsFormProgress');
                $block = $('#FairplaySettingsFormBlock');
                $form = $('#FairplaySettingsForm');
                f = $form.get(0);

                // tabs
                $block.find('#FairplaySettingsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    showTab(key);
                });

                if (bff.bootstrapJS()) {
                    $form.find('.j-contacts-view-tooltip').tooltip();
                }

                var $commissions = $form.find('.j-commissions');
                var cnt = <?= $cnt ?>;
                $form.on('click', '.j-commission-add', function(e){
                    e.preventDefault();
                    var html = <?= func::php2js($commission()) ?>;
                    html = html.replace(/__cnt__/g, cnt);
                    cnt++;
                    $commissions.append(html);
                });

                $commissions.on('click', '.j-remove', function(e){
                    e.preventDefault();
                    $(this).closest('.j-commission').remove();
                });

                showTab('<?= $tab ?>');

            });

            function showTab(key)
            {
                $block.find('.j-tab').addClass('hidden');
                var $tab = $block.find('.j-tab-'+key);
                $tab.removeClass('hidden');
                $block.find('.j-tab-toggler[data-key="'+key+'"]').parent().addClass('tab-active').siblings().removeClass('tab-active');
                window.history.pushState({}, document.title, ajaxUrl + '&tab=' + key);
                if($tab.hasClass('i')) return;
                $tab.addClass('i');
                switch(key){
                    case 'documents':
                        bff.ajax(ajaxUrl+'&act=documents', {}, function(data){
                            if(data && data.success){
                                $block.find('.j-tab-'+key).html(data.html);
                            }
                        }, $progress);
                        break;
                }
            }

            return {
                save: function()
                {
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('<?= _te('', 'Данные успешно сохранены'); ?>');
                        }
                    }, $progress);
                }
            };
        }());
</script>