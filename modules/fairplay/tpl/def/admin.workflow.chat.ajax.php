<?php
if(empty($chat['list'])): ?><div class="j-message"></div><? return; endif;
if( ! empty($chat['more'])): ?><div class="text-center"><a class="j-more" href="#"><?= _t('', 'еще') ?></a></div><? endif;
if( ! isset($arbitrage_chat_id)) $arbitrage_chat_id = 0;
$users = array();
$chat['list'] = array_reverse($chat['list']);
foreach($chat['list'] as $v){
    $authorID = $v['author_id'];
    if( ! isset($users[$authorID])){
        $users[$authorID] = Users::model()->userData($authorID, array('user_id', 'login', 'name', 'surname', 'email', 'blocked'));
    }
    $u = $users[$authorID];
    $label = '<li class="text-warning">'._te('fp', 'Арбитр').'</li>';
    if($authorID == $client_id){
        $label = '<li class="text-info">'._te('fp', 'Заказчик').'</li>';
    }else if($authorID == $worker_id){
        $label = '<li class="text-success">'._te('fp', 'Исполнитель').'</li>';
    }else if($authorID == $client_expert_id){
        $label = '<li class="text-info">'._te('fp', 'Эксперт от заказчика').'</li>';
    }else if($authorID == $worker_expert_id){
        $label = '<li class="text-success">'._te('fp', 'Эксперт от исполнителя').'</li>';
    }
    switch($v['type']){
        case Fairplay::CHAT_TYPE_MESSAGE:
            ?>
            <div class="comment j-message" data-id="<?= $v['id'] ?>">
                <div class="ccontent">
                    <div class="tb"><div class="tl"><div class="tr"></div></div></div>
                    <div class="ctext<?= $arbitrage_chat_id == $v['id'] ? ' text-error' : '' ?>">
                        <?= nl2br($v['message']) ?>
                        <? if( ! empty($v['extra']['attach'])): ?><br />
                            <? foreach($v['extra']['attach'] as $vv): ?>
                                <a href="<?= $vv['url'] ?>" target="_blank"><i class="icon icon-file disabled"></i><?= $vv['rfilename'] ?></a>
                            <? endforeach; ?>
                        <? endif; ?>
                    </div>
                    <div class="bl"><div class="bb"><div class="br"></div></div></div>
                </div>
                <div class="info" style="margin:0;">
                    <ul>
                        <li><p><a href="#" onclick="return bff.userinfo(<?= $u['user_id'] ?>);" class="userlink author<? if($u['blocked']){ ?> blocked<? } ?>"><?= ( ! empty($u['name']) ? $u['name'].' '.$u['surname'] : $u['login'] ) ?> (<?= $u['email'] ?>)</a></p></li>
                        <?= $label; ?>
                        <li class="date"><?= tpl::date_format2($v['created'], true); ?></li>
                        <? if( ! empty($v['extra']['arbitrage'])): ?><li><p class="text-error"><?= _t('fp', 'Обращение в арбитраж'); ?></p></li><? endif; ?>
                    </ul>
                </div>
            </div>
        <? break; # Fairplay::CHAT_TYPE_MESSAGE:
        case Fairplay::CHAT_TYPE_START: ?>
            <div class="alert alert-info comment j-message" data-id="<?= $v['id'] ?>" style="margin: 0 0 10px 15px;">
                <div class="info" style="margin-bottom:-8px">
                    <ul>
                        <li><p><?= _t('fp', 'Начало работы'); ?> <?= tpl::date_format3($v['created']); ?></p></li>
                    </ul>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_COMPLETE: ?>
            <div class="alert alert-success comment j-message" data-id="<?= $v['id'] ?>" style="margin: 0 0 10px 15px;">
                <div class="info" style="margin-bottom:-8px">
                    <ul>
                        <li><a href="#" onclick="return bff.userinfo(<?= $u['user_id'] ?>);" class="userlink author<? if($u['blocked']){ ?> blocked<? } ?>"><?= ( ! empty($u['name']) ? $u['name'].' '.$u['surname'] : $u['login'] ) ?> (<?= $u['email'] ?>)</a></li>
                        <?= $label; ?>
                        <li><p><?= _t('fp', 'завершил работу'); ?> <?= tpl::date_format3($v['created']); ?></p></li>
                    </ul>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_REWORK: ?>
            <div class="alert alert-warning comment j-message" data-id="<?= $v['id'] ?>" style="margin: 0 0 10px 15px;">
                <div class="info" style="margin-bottom:-8px">
                    <ul>
                        <li><a href="#" onclick="return bff.userinfo(<?= $u['user_id'] ?>);" class="userlink author<? if($u['blocked']){ ?> blocked<? } ?>"><?= ( ! empty($u['name']) ? $u['name'].' '.$u['surname'] : $u['login'] ) ?> (<?= $u['email'] ?>)</a></li>
                        <?= $label; ?>
                        <li><p><?= _t('fp', 'вернул проект в работу'); ?> <?= tpl::date_format3($v['created']); ?></p></li>
                    </ul>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_ARBITRAGE_START: ?>
            <div class="alert alert-error comment j-message" data-id="<?= $v['id'] ?>" style="margin: 0 0 10px 15px;">
                <div class="info" style="margin-bottom:-8px">
                    <ul>
                        <li><a href="#" onclick="return bff.userinfo(<?= $u['user_id'] ?>);" class="userlink author<? if($u['blocked']){ ?> blocked<? } ?>"><?= ( ! empty($u['name']) ? $u['name'].' '.$u['surname'] : $u['login'] ) ?> (<?= $u['email'] ?>)</a></li>
                        <?= $label; ?>
                        <li><p><?= _t('fp', 'начал арбитраж'); ?> <?= tpl::date_format3($v['created']); ?></p></li>
                    </ul>
                </div>
            </div>
        <? break;
        case Fairplay::CHAT_TYPE_ARBITRAGE_STOP: ?>
            <div class="alert alert-info comment j-message" data-id="<?= $v['id'] ?>" style="margin: 0 0 10px 15px;">
                <div class="info" style="margin-bottom:-8px">
                    <ul>
                        <li><p><?= _t('fp', 'Арбитраж окончен'); ?> <?= tpl::date_format3($v['created']); ?></p></li>
                    </ul>
                </div>
            </div>
        <? break;
    }
}


