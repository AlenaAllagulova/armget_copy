<?php
$statuses = Fairplay::verifiedStatuses();
$urlEdit = $this->adminLink(bff::$event.'&act=edit&id=');
foreach($list as $k=>$v):
    $id = $v['user_id']; ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $id ?></td>
        <td class="left">
            <a href="#" onclick="return bff.userinfo(<?= $id ?>);"><?= $v['email'] ?></a>
        </td>
        <td class="left"><?= $v['name'] ?> <?= $v['surname'] ?></td>
        <td><? if(isset($statuses[ $v['status'] ])): $s = $statuses[ $v['status'] ]; ?>
            <i class="<?= $s['i'] ?> disabled" title="<?= $s['title'] ?>"></i>
            <? endif; ?>
        </td>
        <td>
            <a class="but edit workflow-edit" title="<?= _t('', 'Edit') ?>" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach;
if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="5">
            <?= _t('', 'Nothing found') ?>
        </td>
    </tr>
<? endif;