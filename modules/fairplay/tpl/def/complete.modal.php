<?php
?>
<div class="modal fade" id="j-modal-complete-<?= $id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= _t('fp', 'Работа выполнена'); ?></h4>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <input type="hidden" name="id" value="<?= $id ?>" />
                    <div class="form-group">
                        <textarea rows="5" class="form-control" name="message" placeholder="<?= _t('fp', 'Комментарий для заказчика'); ?>"></textarea>
                        <div class="help-block j-help-block"></div>
                    </div>
                    <div class="form-group c-formSubmit">
                        <button type="submit" class="btn btn-success c-formSuccess j-submit"><?= _t('fp', 'Работа выполнена'); ?></button>
                        <a class="c-formCancel ajax-link" href="#" data-dismiss="modal"><span><?= _t('', 'Отмена'); ?></span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
