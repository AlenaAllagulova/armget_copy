<?php
$aData['tab'] = 'yandex-money';
echo $this->viewPHP($aData, 'admin.paysystems');
if(isset($token_required)){
    if(empty($token_required)){
        ?><div class="alert"><?= _t('fp', 'Не настроены параметры платежной системы'); ?></div><?
        return;
    }else{
        ?>
        <div class="alert"><?= _t('fp', 'Необходима авторизация Yandex.Money'); ?></div>
        <div style="margin-top: 10px;">
            <a href="#" class="btn" id="j-yandex-money-auth"><?= _t('fp', 'Авторизоваться'); ?></a>
        </div>
        <script type="text/javascript">
            $(function(){
                var popup;
                $('#j-yandex-money-auth').click(function(e){
                    e.preventDefault();
                    if(popup !== undefined) popup.close();
                    var o = $.extend({w: 650, h: 800}, o || {});
                    var centerWidth = ($(window).width() - o.w) / 2;
                    var centerHeight = ($(window).height() - o.h) / 2;
                    var popup = window.open('<?= $token_required ?>', "yandex_money_auth_popup", "width=" + o.w + ",height=" + o.h + ",left=" + centerWidth + ",top=" + centerHeight + ",resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes");
                    popup.focus();
                });
            });
        </script>
        <?
        return;
    }
}
?>
<table class="admtbl tbledit" id="j-yandex-money-info">
    <? if(isset($pass_required)): ?>
    <tr id="j-yandex-pass-required">
        <td class="row1 field-title"></td>
        <td class="row2">
            <p class="desc"><?= _t('fp', 'Необходим пароль для проведения платежей.'); ?></p>
            <script type="text/javascript">
                $(function(){
                    $.fancybox('', {ajax:true, href:'<?= $this->adminLink(bff::$event); ?>&act=yandex-money-pass-popup'});
                });
            </script>
        </td>
    </tr>
    <? endif; ?>
    <? if(isset($account)): ?>
    <tr>
        <td class="row1 field-title" width="100"><?= _t('fp', 'Кошелек:'); ?></td>
        <td class="row2">
            <?= $account ?>
        </td>
    </tr>
    <? endif; ?>
    <? if(isset($balance)): ?>
    <tr>
        <td class="row1 field-title"><?= _t('fp', 'Баланс:'); ?></td>
        <td class="row2">
            <?= $balance ?>
        </td>
    </tr>
    <? endif; ?>
    <? if(isset($balance_details['available'])): ?>
        <tr>
            <td class="row1 field-title"><?= _t('fp', 'Доступно:'); ?></td>
            <td class="row2">
                <?= $balance_details['available'] ?>
            </td>
        </tr>
    <? endif; ?>
    <? if( ! empty($pass)): ?>
        <tr>
            <td class="row1 field-title"><?= _t('fp', 'Пароль для платежей:'); ?></td>
            <td class="row2">
                <b><?= $pass ?></b>
                <a href="#" class="ajax" id="j-yandex-money-pass-change" style="margin-left: 10px;"><?= _t('fp', 'Изменить пароль'); ?></a>
                <p class="desc"><?= _t('fp', 'Согласно требованиям Yandex.Money полученный токен защищен паролем. Вам необходимо будет вводить этот пароль при проведении выплат.'); ?></p>
            </td>
        </tr>
    <? else: if(isset($balance)): ?>
        <tr>
            <td class="row1 field-title"></td>
            <td class="row2">
                <a href="#" class="ajax" id="j-yandex-money-pass-change"><?= _t('fp', 'Изменить пароль для соверешния платежей'); ?></a><br />
                <a href="#" class="ajax" id="j-yandex-money-pass-forget"><?= _t('fp', 'Забыть пароль для соверешния платежей'); ?></a>
            </td>
        </tr>
    <? endif; endif; ?>
    <tr class="hide" id="j-pass-cange">
        <td class="row1 field-title"><?= _t('fp', 'Новый пароль для платежей:'); ?></td>
        <td class="row2">
            <input type="text" id="j-yandex-money-pass-new" value="" />
            <a href="#" class="btn btn-small" id="j-yandex-money-pass-save"><?= _t('', 'Изменить'); ?></a>
        </td>
    </tr>
    <? if(FORDEV): ?>
        <tr>
            <td class="row1"></td>
            <td class="row2">
                <a href="#" class="btn btn-small btn-danger" id="j-yandex-money-token-delete"><?= _t('fp', 'Удалить токен'); ?></a>
                <script type="text/javascript">
                    $(function(){
                        $('#j-yandex-money-token-delete').click(function(e){
                            var $el = $(this);
                            e.preventDefault();
                            if( ! confirm('<?= _te('', 'Sure?'); ?>')) return;
                            bff.ajax('<?= $this->adminLink(bff::$event); ?>&act=yandex-money-token-delete', {}, function(data){
                                if (data && data.success) {
                                    bff.success('<?= _te('fp', 'Токен удален'); ?>');
                                    $el.remove();
                                }
                            });
                        });
                    });
                </script>
            </td>
        </tr>
    <? endif; ?>
</table>
<?= tplAdmin::blockStop(); ?>
<script type="text/javascript">
$(function(){
    $('#j-yandex-money-pass-change').click(function(e){
        e.preventDefault();
        $(this).closest('tr').remove();
        $('#j-pass-cange').removeClass('hide');
    });

    var process = false;
    $('#j-yandex-money-pass-save').click(function(e){
        e.preventDefault();
        var $pass = $('#j-yandex-money-pass-new');
        $pass.parent().removeClass('clr-error');
        var p = $pass.val();
        if( ! p.length){
            $pass.parent().addClass('clr-error');
            $pass.focus();
            return;
        }
        if(process) return;
        process = true;
        bff.ajax('<?= $this->adminLink(bff::$event); ?>&act=yandex-money-pass-change', {pass:p}, function(data){
            process = false;
            if (data && data.success) {
                <? if( ! empty($pass)): ?>
                    document.location = '<?= $this->adminLink(bff::$event.'&act=yandex-money-info'); ?>';
                <? else: ?>
                    location.reload();
                <? endif; ?>
            }
        });
    });

    $('#j-yandex-money-pass-forget').click(function(e){
        e.preventDefault();
        if(process) return;
        process = true;
        bff.ajax('<?= $this->adminLink(bff::$event); ?>&act=yandex-money-pass-forget', {}, function(data){
            process = false;
            if(data && data.success){
                $('#j-yandex-money-info').html('<div class="alert alert-warning"><?= _te('fp', 'Необходим пароль для проведения платежей.'); ?></div>');
            }
        });
    });
});
</script>
