<?php
?>
<div class="modal fade" id="j-modal-fairplay-opinion-add-<?= $id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= _t('fp', 'Оставить отзыв'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= _t('fp', 'Пожалуйста, оставьте ваш отзыв о сотрудничестве.'); ?></p>
                <form method="post" action="">
                    <input type="hidden" name="id" value="<?= $id ?>" />
                    <div class="form-group">
                        <? foreach(Opinions::aTypes() as $v): ?>
                            <label class="radio-inline radio-inline-sm radio-<?= $v['cf'] ?>">
                                <input type="radio" name="type" value="<?= $v['id'] ?>"> <?= $v['t'] ?>
                            </label>
                        <? endforeach; ?>
                    </div>
                    <div class="form-group j-required">
                        <textarea rows="5" class="form-control" name="message" placeholder="<?= _t('fp', 'Ваш отзыв о сотрудничестве'); ?>"></textarea>
                        <div class="help-block j-help-block"></div>
                    </div>
                    <div class="c-formSubmit">
                        <button type="submit" class="btn btn-success c-formSuccess j-submit"><?= _t('fp', 'Оставить'); ?></button>
                        <a class="c-formCancel ajax-link" href="#" data-dismiss="modal"><span><?= _t('form', 'Отмена'); ?></span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
