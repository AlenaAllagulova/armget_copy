<?php
/**
 * Список хода работ пользователя(layout)
 * @var $this Fairplay
 */
tpl::includeJS('fairplay.workflows.list', false, 1);
?>
<div class="p-profileContent pdt0" id="j-fairplay-workflows-list">

    <div class="p-profile-title hidden">
        <form method="get" action="" id="j-fairplay-workflows-list-form" class="mrgt10">
            <input type="hidden" name="st" value="<?= $f['st'] ?>" />
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            
            <div class="dropdown hidden-lg">
                <a href="#" class="ajax-link" data-toggle="dropdown"><span><span class="j-f-status-title"><?= $types[ $f['st'] ]['t'] ?></span> <small class="j-f-status-cnt">(<?= $types[ $f['st'] ]['c'] ?>)</small></span><b class="caret"></b></a>
                <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                    <? foreach($types as $v): ?>
                        <li<?= $v['id'] == $f['st'] ? ' class="active"' : '' ?>><a href="#" class="j-f-status" data-id="<?= $v['id']?>"><?= $v['t'] ?> <small class="j-cnt-status-<?= $v['id'] ?>">(<?= $v['c'] ?>)</small></a></li>
                    <? endforeach; ?>
                </ul>
            </div>

            <ul class="p-profile-submenu visible-lg">
                <? foreach($types as $v): ?>
                    <li<?= $v['id'] == $f['st'] ? ' class="active"' : '' ?>><a href="#" class="j-f-status" data-id="<?= $v['id']?>"><?= $v['t'] ?> <small class="j-cnt-status-<?= $v['id'] ?>">(<?= $v['c'] ?>)</small></a></li>
                <? endforeach; ?>
            </ul>

            <div class="clearfix"></div>
        </form>
    </div>
    <div class="j-list" id="j-fairplay-statuses-block"><?= $list ?></div>
    <div class="j-pagination"><?= $pgn ?></div>
</div>
<script type="text/javascript">
<? js::start() ?>
    jFairplayWorkflowsList.init(<?= func::php2js(array(
        'lang'  => array(
        ),
        'ajax'  => true,
    )) ?>);
<? js::stop() ?>
</script>