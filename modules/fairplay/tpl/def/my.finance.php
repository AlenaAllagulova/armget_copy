<?php
/** @var $this Fairplay */
tpl::includeJS('fairplay.my.finance', false, 1);
?>
<div class="p-profileContent j-settings-tab j-settings-tab-finance<?= ! $active ? ' displaynone' : ''?>" id="j-fairplay-finance">
    <form class="form-horizontal" role="form" method="post" action="">
        <section class="p-profileCabinet-section">

            <? $fields = Fairplay::financeFields(); $addon = '';
                foreach($fields as $block): ?>
                <? if( ! empty($block['t'])): ?>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <h5><?= $block['t'] ?></h5>
                        </div>
                    </div>
                <? endif; ?>
                <? foreach($block['fields'] as $f): $field = $f['id'];
                    switch($f['type']){
                        case 'text-addon': ob_start(); ob_implicit_flush(false); ?>
                            <div class="col-sm-<?= ! empty($f['sm']) ? $f['sm'] : '8' ?>">
                                <input type="text" name="<?= $field ?>" value="<?= isset($$field) ? $$field : '' ?>" class="form-control"<? if( ! empty($f['placeholder'])): ?> placeholder="<?= $f['placeholder'] ?>"<? endif; ?>>
                            </div>
                            <? $addon .= ob_get_clean(); break;
                        case 'text': ?>
                            <div class="form-group">
                                <label for="j-fp-<?= $field ?>" class="col-sm-4 control-label"><?= $f['t']; ?> <?= ! empty($f['required']) ? '<i class="text-danger">*</i>' : '' ?></label>
                                <div class="col-sm-<?= ! empty($f['sm']) ? $f['sm'] : '8' ?><?= ! empty($f['required']) ? ' j-required' : '' ?>">
                                    <input type="text" name="<?= $field ?>" value="<?= isset($$field) ? $$field : '' ?>" class="form-control" id="j-fp-<?= $field ?>"<? if( ! empty($f['placeholder'])): ?> placeholder="<?= $f['placeholder'] ?>"<? endif; ?>>
                                </div>
                                <?= $addon; ?>
                            </div>
                        <?  $addon = '';
                            break;
                        case 'date': $options = Users::i()->getBirthdateOptions(isset($$field) ? $$field : '', 1930, true); ?>
                            <div class="form-group p-cabinet-birthdate">
                                <label class="col-sm-4 control-label"><?= $f['t']; ?> <?= ! empty($f['required']) ? ' <i class="text-danger">*</i>' : '' ?></label>
                                <div class="col-sm-2">
                                    <select name="<?= $field ?>[day]" class="form-control">
                                        <?= $options['days'] ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <select name="<?= $field ?>[month]" class="form-control">
                                        <?= $options['months'] ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <select name="<?= $field ?>[year]" class="form-control">
                                        <?= $options['years'] ?>
                                    </select>
                                </div>
                            </div>
                        <? break;
                        case 'radio': ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $f['t']; ?><?= ! empty($f['required']) ? ' <i class="text-danger">*</i>' : '' ?></label>
                                <div class="col-sm-8">
                                    <? $val = isset($$field) ? $$field : 0;
                                    foreach($f['options'] as $k => $v): ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="<?= $field ?>" value="<?= $k ?>" <?= $val == $k ? 'checked="checked"' : '' ?>/>
                                                <?= $v ?>
                                            </label>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        <? break;
                        case 'textarea': ?>
                            <div class="form-group">
                                <label for="j-fp-<?= $field ?>" class="col-sm-4 control-label"><?= $f['t']; ?><?= ! empty($f['required']) ? ' <i class="text-danger">*</i>' : '' ?></label>
                                <div class="col-sm-<?= ! empty($f['sm']) ? $f['sm'] : '8' ?><?= ! empty($f['required']) ? ' j-required' : '' ?>">
                                    <textarea rows="3" name="<?= $field ?>" class="form-control" id="j-fp-<?= $field ?>"><?= isset($$field) ? $$field : '' ?></textarea>
                                </div>
                            </div>
                        <? break;
                    } ?>
                <? endforeach; ?>
            <? endforeach; ?>

            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4">
                    <? switch($status){case Fairplay::VERIFIED_STATUS_APPROVED: ?>
                        <div class="alert alert-success">
                            <?= _t('fp', 'Вы успешно подтвердили финансовую информацию.'); ?>
                        </div>
                        <? break; case Fairplay::VERIFIED_STATUS_WAIT: ?>
                        <div class="alert alert-warning">
                            <?= _t('fp', 'Заявка находится на обработке у модератора.'); ?>
                        </div>
                        <? break; case Fairplay::VERIFIED_STATUS_DECLINED: ?>
                        <div class="alert alert-danger">
                            <strong><?= _t('fp', 'Модератор отклонил вашу заявку по причине:'); ?></strong>
                            <div><?= $verified_reason['message'] ?></div>
                        </div>
                        <? break; default: ?>
                        <div class="alert alert-default"><?= _t('fp', 'Вы еще не подтвердили финансовую информацию.'); ?></div>
                        <? break; } ?>
                    <?= config::get('fairplay_verified_requirement_'.LNG, ''); ?>
                </div>
            </div>
            <?  $verifiedImages = $this->verifiedImages(User::id()); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label o-control-label"><?= _t('users', 'Документы'); ?> <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                    <div class="c-files-add">
                        <ul class="mrgb10 j-files">
                            <? if( ! empty($verified_images)): foreach($verified_images as $v): ?>
                                <li><a href="#" class="link-red j-delete" data-fn="<?= $v['filename'] ?>"><i class="fa fa-times"></i></a> <a href="<?= $v['link'] ?>" target="_blank" class="j-fancy-box" rel="fancy-gallery-finance" ><?= $v['filename'] ?></a> </li>
                            <? endforeach; endif; ?>
                        </ul>
                        <button type="button" class="btn btn-sm btn-default j-add"<?= count($verified_images) >= $verifiedImages->getLimit() ? ' style="display:none;"' : '' ?>><?= _t('', 'Добавить'); ?></button>
                    </div>
                    <div class="help-block">
                        <?= _t('', 'Формат: jpg, gif, png. Максимальный размер файла: [size].', array('size'=>$verifiedImages->getMaxSize(true))) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-4 c-formSubmit">
                    <button class="btn btn-primary c-formSuccess j-submit"><?= _t('', 'Сохранить'); ?></button>
                    <a class="c-formCancel j-cancel" href="<?= Users::url('my.profile'); ?>"><?= _t('form', 'Отмена'); ?></a>
                </div>
            </div>
        </section>
    </form>
</div>
<script type="text/javascript">
<? js::start() ?>
jFairplayMyFinance.init(<?= func::php2js(array(
    'lang' => array(
    ),
    'verified_limit' => isset($verifiedImages) ? $verifiedImages->getLimit() : 0
)) ?>);
<? if( ! empty($verified_images)):     tpl::includeJS('fancybox2', true); ?>
$(function() {
    $('.j-fancy-box').fancybox({
        openEffect	: 'none',
        closeEffect	: 'none',
        nextEffect  : 'fade',
        prevEffect : 'fade',
        fitToView: true,
        helpers: {
            overlay: {locked: false}
        }
    });
});
<? endif; ?>
<? js::stop() ?>
</script>