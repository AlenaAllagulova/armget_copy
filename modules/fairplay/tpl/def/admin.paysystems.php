<?php
$tabs = array(
    'yandex-money'  => array('t' => 'Yandex.Money', 'link' => $this->adminLink(bff::$event.'&act=yandex-money-info')),
);
?>
<?= tplAdmin::blockStart(_t('fp', 'Платежные системы'), true, array('id'=>'FairplayPaySystems')); ?>
<div class="tabsBar">
    <? foreach($tabs as $k=>$v) { ?>
        <span class="tab<? if($k == $tab) { ?> tab-active<? } ?>"><a href="<?= $v['link'] ?>"><?= $v['t'] ?></a></span>
    <? } ?>
    <div id="FairplayPaySystemsProgress" class="progress" style="display: none;"></div>
    <div class="clear"></div>
</div>

