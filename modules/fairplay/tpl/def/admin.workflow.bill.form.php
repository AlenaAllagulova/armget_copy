<?php
/**
 * @var $this Fairplay
 */
$billForm = 1;
$price = ( Catcher::isUseOrderPrepay() && isset($prepay) && !empty($prepay)) ? $prepay : $price; # переопределение суммы резерва/выплаты при установленной сумме предоплаты
?>
<form id="j-bill-create" method="post" action="">
    <?  $bill = array(
        'user_id'    => 0,
        'email'      => '',
        'type'       => Fairplay::BILL_TYPE_RESERVED,
        'price'      => 0,
        'sum'        => 0,
        'commission' => 0,
    );
    if($status == Fairplay::STATUS_RESERVATION) {
        $bill = array(
            'user_id'    => $client_id,
            'email'      => _t('fp', 'Заказчик ([email])', array('email' => $client['email'])),
            'type'       => Fairplay::BILL_TYPE_RESERVED,
            'price'      => $price,
            'commission' => $this->commission($price),
        );
        $bill['sum'] = round($price + $bill['commission'], 2);
    }else if($status == Fairplay::STATUS_PAYMENT){
        $bill = array(
            'user_id'    => $worker_id,
            'email'      => _t('fp', 'Исполнитель ([email])', array('email' => $worker['email'])),
            'type'       => Fairplay::BILL_TYPE_PAID,
            'price'      => ( Catcher::isHoldMinPrepay() ? $price - Catcher::MIN_PREPAY  : $price),
            'commission' => 0,
            'sum'        => ( Catcher::isHoldMinPrepay() ? $price - Catcher::MIN_PREPAY  : $price),
        );
    }
    $paySystems = array(
        'yandex' => array('t' => 'Yandex.Money'),
        'manual' => array('t' => 'Вручную'),
        );
    $typeReserved = 'j-type j-type-'.Fairplay::BILL_TYPE_RESERVED.( $bill['type'] != Fairplay::BILL_TYPE_RESERVED ? ' hidden' : '');
    $typePaid     = 'j-type j-type-'.Fairplay::BILL_TYPE_PAID.( $bill['type'] != Fairplay::BILL_TYPE_PAID ? ' hidden' : '');
    ?>
    <input type="hidden" name="workflow_id" value="<?= $id ?>" />
    <input type="hidden" name="pay" value="" />
    <div class="well well-small">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1 field-title" width="133"><?= _t('fp', 'Операция:'); ?></td>
                <td class="row2">
                    <select name="type"><?= html::selectOptions(Fairplay::billTypes(), $bill['type'], false, 'id', 't') ?></select>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"><?= _t('', 'Пользователь:'); ?></td>
                <td class="row2">
                    <input type="hidden" name="user_id" value="<?= $bill['user_id'] ?>" id="j-bill-user-id" />
                    <input type="text" id="j-bill-user-email" value="<?= $bill['email'] ?>" class="autocomplete" placeholder="<?= _t('', 'Выберите или введите e-mail') ?>" style="width: 220px;" />
                    <a class="userlink j-user-info" href="#"></a>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title">
                    <?= _t('fp', 'Сумма:'); ?>
                    <? if(Catcher::isHoldMinPrepay() && $bill['type'] == Fairplay::BILL_TYPE_PAID):?>
                        <br>
                        <span class="desc small">
                            <?=_t('', 'Автоматический вычет минимальной предоплаты [sum] [cur]',
                                [
                                    'sum' => Catcher::MIN_PREPAY,
                                    'cur' => Site::currencyDefault(),
                                ])?>
                        </span>
                    <? endif;?>
                </td>
                <td class="row2">
                    <input type="text" style="width: 70px;" value="<?= $bill['price']; ?>" name="sum" /> <?= $curr; ?>
                </td>
            </tr>
            <tr class="<?= $typeReserved ?>">
                <td class="row1 field-title"><?= _t('fp', 'Комиссия:'); ?></td>
                <td class="row2">
                    <input type="text" style="width: 70px;" value="<?= $bill['commission']; ?>" name="commission" /> <?= $curr; ?>
                    <a class="ajax desc j-calc-comission" href="#"><?= _t('fp', 'рассчитать'); ?></a>
                </td>
            </tr>
            <tr class="<?= $typeReserved ?>">
                <td class="row1 field-title"><?= _t('fp', 'Всего:'); ?></td>
                <td class="row2">
                    <span class="j-sum"><?= $bill['sum']; ?></span> <?= $curr; ?>
                </td>
            </tr>
            <tr class="<?= $typeReserved ?>">
                <td class="row1 field-title"><?= _t('fp', 'Примечание:'); ?></td>
                <td class="row2">
                    <textarea name="descr_reserved" class="stretch" rows="3"></textarea>
                </td>
            </tr>
            <tr class="<?= $typeReserved ?>">
                <td class="row1 field-title"></td>
                <td class="row2">
                    <a class="btn btn-success j-reserved" href="#"><?= _t('fp', 'Зарезервировать') ?></a>
                </td>
            </tr>

            <tr class="<?= $typePaid ?>">
                <td class="row1 field-title"><?= _t('fp', 'Способ оплаты:'); ?></td>
                <td class="row2">
                    <div class="btn-group">
                        <? foreach($paySystems as $k => $v): ?>
                        <a class="btn btn-mini j-select-pay-system" data-key="<?= $k ?>" href="#"><?= $v['t'] ?></a>
                        <? endforeach; ?>
                    </div>
                </td>
            </tr>
            <tr class="<?= $typePaid ?>">
                <td class="row1 field-title"><?= _t('fp', 'Примечание:'); ?></td>
                <td class="row2">
                    <textarea name="descr_paid" class="stretch" rows="3"><?= _t('fp', 'Выплата суммы по договору: [title]', array('title' => HTML::escape($order['title']))) ?></textarea>
                </td>
            </tr>
            <tr class="<?= $typePaid ?>">
                <td class="row1 field-title"></td>
                <td class="row2">
                    <div class="j-pay-info"></div>
                    <a class="btn btn-warning disabled j-pay" href="#"><?= _t('fp', 'Выплатить') ?></a>
                </td>
            </tr>
        </table>
    </div>
</form>
<script type="text/javascript">
var jFairplayWorkflowsBillForm = (function(){
    var $bill, $list, $pay, $userID, $progress, $payInfo;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';
    var ajaxBill = '<?= $this->adminLink('bills'); ?>';
    var verified = {}, $finance = false, financeCache = {};

    $(function(){

        $list = $('#j-bills-list');
        $bill = $('#j-bill-create');
        $pay = $bill.find('.j-pay');
        $userID = $bill.find('#j-bill-user-id');
        $progress = $('#fairplay-workflow-form-progress');
        $payInfo = $bill.find('.j-pay-info');

        $bill.find('#j-bill-user-email').autocomplete(ajaxUrl+'&act=user', {valueInput: $userID,
            suggest: {'<?= $client_id ?>':'<?= _te('', 'Заказчик'); ?> (<?= $client['email'] ?>)','<?= $worker_id ?>':'<?= _te('', 'Исполнитель'); ?> (<?= $worker['email'] ?>)'},
            onSelect:function(id){
                financeInfo(id);
                paySystems();
            }});

        $bill.on('change', '[name="sum"]', function(){
            calcSumm();
        });

        $bill.on('change', '[name="commission"]', function(){
            calcSumm();
        });

        $bill.on('change', '[name="type"]', function(){
            var id = $(this).val();
            $bill.find('.j-type').addClass('hidden');
            $bill.find('.j-type-' + id).removeClass('hidden');
            paySystems();
        });

        $bill.on('click', '.j-calc-comission', function(e){
            e.preventDefault();
            var price = floatval($bill.find('[name="sum"]').val());
            if(price <= 0){
                bff.error('<?= _te('fp', 'Сумма должна быть больше 0'); ?>');
                return;
            }
            bff.ajax(ajaxUrl+'&act=comission-calc', {price:price}, function(data){
                if (data && data.success) {
                    $bill.find('[name="commission"]').val(data.commission);
                    calcSumm();
                }
            });
        });


        $bill.on('click', '.j-reserved', function(e){
            e.preventDefault();
            $bill.find('[name="pay"]').val('');
            submit();
        });

        $pay.click(function(e){
            e.preventDefault();
            if($pay.hasClass('disabled')) return;
            submit();
        });

        $bill.on('click', '.j-user-info', function(e){
            e.preventDefault();
            var id = intval($userID.val());
            if( ! id){
                bff.error('<?= _te('fp', 'Выберите пользователя'); ?>');
                return;
            }
            bff.userinfo(id);
        });

        $bill.on('click', '.j-select-pay-system', function(e){
            e.preventDefault();
            var $el = $(this);
            $bill.find('[name="pay"]').val($el.data('key'));
            $el.addClass('active').siblings().removeClass('active');
            $pay.addClass('disabled');
            $payInfo.html('');
            if($el.data('key') == 'yandex'){
                var u = intval($userID.val());
                if( ! u) return;
                if(verified.hasOwnProperty(u)){
                    payInfo(u);
                }else{
                    bff.ajax('<?= $this->adminLink('pay_systems'); ?>&act=yandex-money-is-pay', {user:u}, function(data){
                        verified[u]={'allowed':0};
                        if (data && data.success) {
                            verified[u]['msg'] = data.msg;
                            verified[u]['allowed'] = intval(data.allowed);
                        }
                        payInfo(u);
                    }, $progress);
                }
            }else{
                $pay.removeClass('disabled');
            }
        });


        <? if($bill['user_id']): ?>financeInfo(<?= $bill['user_id'] ?>);<? endif; ?>
    });

    function calcSumm()
    {
        var s = floatval($bill.find('[name="sum"]').val());
        s += floatval($bill.find('[name="commission"]').val());
        $bill.find('.j-sum').text(s);
    }

    function floatval(str)
    {
        var fl = parseFloat((str+'').replace(',','.'));
        return isNaN(fl) ? 0 : fl;
    }

    function submit()
    {
        if( ! intval($bill.find('[name="user_id"]').val())){
            bff.error('<?= _te('fp', 'Укажите пользователя'); ?>');
            return;
        }
        var sum = floatval($bill.find('[name="sum"]').val());
        if(sum <= 0){
            bff.error('<?= _te('fp', 'Сумма должна быть больше 0'); ?>');
            return;
        }

        bff.ajax(ajaxBill + '&act=create', $bill.serialize(), function(data){
            if(data.pass_required){
                $.fancybox('', {ajax:true, href:'<?= $this->adminLink('pay_systems'); ?>&act=yandex-money-pass-popup&onSuccess=jFairplayWorkflowsBillForm.submit()'});
            }
            if (data && data.success) {
                reset();
                $list.html(data.list);
                bff.success('<?= _te('', 'Операция выполнена успешно'); ?>');
                if(data.reload){
                    jFairplayWorkflowsFormManager.action('cancel');
                    jFairplayWorkflowsFormManager.action('edit', <?= $id ?>);
                }
            }
        }, $progress);
    }

    function reset()
    {
        $bill.find('[name="user_id"]').val(0);
        $bill.find('[name="price"]').val(0);
        $bill.find('[name="commission"]').val(0);
        $bill.find('[name="descr"]').val('');
        $bill.find('#j-bill-user-email').val('');
        calcSumm();
    }

    function paySystems()
    {
        $bill.find('[name="pay"]').val('');
        $bill.find('.j-select-pay-system').removeClass('active');
        $payInfo.html('');
    }

    function payInfo(u)
    {
        if(verified[u]['allowed']){
            $pay.removeClass('disabled');
            $payInfo.html('<div class="alert alert-info" style="margin-bottom: 10px;">'+verified[u]['msg']+'</div>');
        }else{
            $payInfo.html('<div class="alert alert-error" style="margin-bottom: 10px;">'+verified[u]['msg']+'</div>');
        }
    }

    function financeInfo(id)
    {
        if($finance){
            $finance.remove();
        }
        id = intval(id);
        if( ! id) return;
        if(financeCache.hasOwnProperty(id)){
            $finance = $(financeCache[id]);
            $bill.after($finance);
        }else{
            bff.ajax('<?= $this->adminLink('finances'); ?>&act=info', {id:id}, function(data){
                if (data && data.success) {
                    financeCache[id] = data.html;
                    $finance = $(financeCache[id]);
                    $bill.after($finance);
                }
            });
        }
    }

    return{
        submit:submit
    };
}());
</script>


