<?php
/** @var $this Fairplay */
?>
<div class="p-profileContent j-settings-tab j-settings-tab-finance<?= ! $active ? ' displaynone' : ''?>" id="j-fairplay-finance">
    <div class="form-horizontal">
        <section class="p-profileCabinet-section">

            <div class="form-group">
                <div class="alert alert-success">
                    <?= _t('fp', 'Вы успешно подтвердили финансовую информацию.'); ?>
                </div>
            </div>

            <? $fields = Fairplay::financeFields(); $addon = '';
            foreach($fields as $block): ?>
                <? if( ! empty($block['t'])): ?>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <h5><?= $block['t'] ?></h5>
                        </div>
                    </div>
                <? endif; ?>
                <? foreach($block['fields'] as $f): $field = $f['id'];
                    switch($f['type']){
                        case 'text-addon': ob_start(); ob_implicit_flush(false); ?>
                            <div class="col-sm-<?= ! empty($f['sm']) ? $f['sm'] : '8' ?>">
                                <div class="form-control-static"><?= isset($$field) ? $$field : '' ?></div>
                            </div>
                            <? $addon .= ob_get_clean(); break;
                        case 'text': ?>
                            <div class="form-group">
                                <label for="j-fp-<?= $field ?>" class="col-sm-4 control-label"><?= $f['t']; ?></label>
                                <div class="col-sm-<?= ! empty($f['sm']) ? $f['sm'] : '8' ?>">
                                    <div class="form-control-static"><?= isset($$field) ? $$field : '' ?></div>
                                </div>
                                <?= $addon; ?>
                            </div>
                            <?  $addon = '';
                            break;
                        case 'date': ?>
                            <div class="form-group p-cabinet-birthdate">
                                <label class="col-sm-4 control-label"><?= $f['t']; ?></label>
                                <div class="col-sm-8">
                                    <div class="form-control-static"><?= isset($$field) ? tpl::date_format2($$field) : '' ?></div>
                                </div>
                            </div>
                            <? break;
                        case 'radio': ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $f['t']; ?></label>
                                <div class="col-sm-8">
                                    <? $val = isset($$field) ? $$field : 0; ?>
                                    <div class="form-control-static"><?= isset($f['options'][$val]) ? $f['options'][$val] : '' ?></div>
                                </div>
                            </div>
                            <? break;
                        case 'textarea': ?>
                            <div class="form-group">
                                <label for="j-fp-<?= $field ?>" class="col-sm-4 control-label"><?= $f['t']; ?></label>
                                <div class="form-control-static"><?= isset($$field) ? nl2br($$field) : '' ?></div>
                            </div>
                            <? break;
                    } ?>
                <? endforeach; ?>
            <? endforeach; ?>

            <?  $verifiedImages = $this->verifiedImages(User::id()); ?>
            <div class="form-group">
                <label class="col-sm-3 control-label o-control-label"><?= _t('users', 'Документы'); ?></label>
                <div class="col-sm-9">
                    <div class="c-files-add">
                        <ul class="mrgb10 j-files">
                            <? if( ! empty($verified_images)): foreach($verified_images as $v): ?>
                                <li><a href="<?= $v['link'] ?>" target="_blank" class="j-fancy-box" rel="fancy-gallery-finance" ><?= $v['filename'] ?></a> </li>
                            <? endforeach; endif; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9 col-sm-offset-3 c-formSubmit">
                    <button class="btn btn-danger c-formSuccess" data-toggle="modal" href="#j-modal-delete-confirm"><?= _t('fp', 'Удалить финансовые данные'); ?></button>
                    <a class="c-formCancel j-cancel" href="<?= Users::url('my.profile'); ?>"><?= _t('form', 'Отмена'); ?></a>
                </div>
            </div>

        </section>
    </div>
</div>
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="j-modal-delete-confirm" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="myModalLabel" class="modal-title"><?= _t('fp', 'Удалить финансовые данные'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= _t('fp', 'Подтвердите Ваше желание удалить финансовые данные.'); ?></p>
                <div class="form-group c-formSubmit">
                    <button class="btn btn-danger c-formSuccess" type="button" id="j-finance-delete"><?= _t('fp', 'Удалить финансовые данные'); ?></button>
                    <a data-dismiss="modal" href="#" class="c-formCancel ajax-link"><span><?= _t('', 'Отмена'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
<? js::start() ?>
$(function(){
    $('#j-finance-delete').click(function(e){
        var $el = $(this);
        e.preventDefault();
        bff.ajax(bff.ajaxURL('fairplay', 'delete&ev=my_settings_finance'), {hash: app.csrf_token}, function(data){
            if (data && data.success) {
                location.reload();
            }
        }, function(yes){
            if(yes){
                if( ! $el.is('[data-loading-text]')) $el.attr('data-loading-text', app.lang.form_btn_loading);
                $el.button('loading');
            }else{
                $el.button('reset');
            }
        });
    });
});
<? if( ! empty($verified_images)):     tpl::includeJS('fancybox2', true); ?>
$(function() {
    $('.j-fancy-box').fancybox({
        openEffect	: 'none',
        closeEffect	: 'none',
        nextEffect  : 'fade',
        prevEffect : 'fade',
        fitToView: true,
        helpers: {
            overlay: {locked: false}
        }
    });
});
<? endif; ?>
<? js::stop() ?>
</script>