<?php
if(empty($list)) return;
$statusPrev = 0;
$arbitragePrev = 0;
$fairplayPrev = reset($list);
$fairplayPrev = $fairplayPrev['fairplay'];
$statuses = Fairplay::statuses();
$users = array();
?>
<div class="well well-small" id="j-statuses-history-<?= $id ?>">
    <? foreach($list as $v):
        $changed = '';
        if($statusPrev != $v['status'] && isset($statuses[$v['status']])) {
            $statusPrev = $v['status'];
            $changed = $statuses[$v['status']]['t'];
        }
        if($arbitragePrev != $v['arbitrage']){
            $arbitragePrev = $v['arbitrage'];
            $changed = $v['arbitrage'] ? _t('fp', 'Обращение в арбитраж') : _t('fp', 'Арбитраж завершен');
        }
        if($fairplayPrev != $v['fairplay']) {
            $fairplayPrev = $v['fairplay'];
            if($v['fairplay']) $changed = _t('fp', 'Тип оплаты - безопасная сделка');
        }
        if(empty($changed)) continue;
        if( ! isset($users[ $v['user_id'] ])){
            $users[ $v['user_id'] ] = Users::model()->userData($v['user_id'], array('name', 'surname'));
        }
        $u = $users[ $v['user_id'] ];
        ?>
        <span class="desc"><?= tpl::date_format2($v['created'], true); ?>:</span>
        <?= $changed ?>
        <span class="desc"> - <?= $u['name']; ?> <?= $u['surname']; ?></span>
        <br />
    <? endforeach; ?>
</div>
