<?php

/**
 * Класс хранения прикреплений для истории сообщений в пределах хода работ
 */

use bff\utils\Files;
use bff\files\Attachment;

class WorkflowsChat_ extends Component
{
    /** @var integer ID записи */
    protected $recordID = 0;

    /** @var string Путь к файлам прикреплений */
    protected $path = '';
    /** @var string URL к файлам прикреплений*/
    protected $url = '';

    /**
     * Раскладывать ли файлы в зависимости от recordID, по папкам folder = (recordID / 1000)
     * @var boolean
     */
    protected $folderByID = false;

    /** @var string Название таблицы для хранения данных о записи */
    protected $tableRecords = '';

    /** @var string Название таблицы для хранения данных */
    protected $tableChat = '';

    /** @var string Название id-столбца в таблице для хранения данных о записи */
    protected $tableRecords_id = 'id';

    /** @var string Название поля для хранения ID записи (в таблице $tableAttachments) */
    protected $fRecordID = 'item_id';

    /**
     * Максимально допустимый размер файла
     * @example: 5242880 - 5мб, 4194304 - 4мб, 3145728 - 3мб, 2097152 - 2мб
     */
    protected $maxSize = 4194304;

    public function __construct($nRecordID = 0)
    {
        $this->init();
        $this->setRecordID($nRecordID);
        $this->initSettings();
    }

    function initSettings()
    {
        $this->path = bff::path('attachments/fairplay');
        $this->url = bff::url('attachments/fairplay');

        $this->tableRecords = TABLE_FAIRPLAY_WORKFLOWS;
        $this->tableChat = TABLE_FAIRPLAY_WORKFLOWS_CHAT;

        $this->fRecordID = 'workflow_id';

        $this->folderByID = config::sysAdmin('fairplay.workflows.chat.folderbyid', true, TYPE_BOOL); # раскладываем файлы прикреплений по папкам (id(5)=>0, id(1005)=>1, ...)
        $this->maxSize = config::sysAdmin('fairplay.workflows.chat.max.size', 5242880, TYPE_UINT); # 5мб
    }

    public function setRecordID($nRecordID)
    {
        $this->recordID = $nRecordID;
    }

    /**
     * Получение максимально допустимого размера файла
     * @param boolean $bFormat применить форматирование
     * @param boolean $bFormatExtTitle полное название объема данных (при форматировании)
     * @return mixed
     */
    public function getMaxSize($bFormat = false, $bFormatExtTitle = false)
    {
        return ($bFormat ? tpl::filesize($this->maxSize, $bFormatExtTitle) : $this->maxSize);
    }

    /**
     * Устанавливаем максимально допустимый размер
     * @param integer $nMaxSize размер в байтах
     */
    public function setMaxSize($nMaxSize)
    {
        $this->maxSize = $nMaxSize;
    }

    /**
     * Добавление сообщения
     * @param array $data данные о сообщении
     * @param bool $notified отправть почтовое уведомление о новом сообщении
     * @return bool|int
     */
    public function append($data, $notified = true)
    {
        if (empty($data['type']) && empty($data['message']) && empty($data['extra']['attach'])) {
            return false;
        }
        if (empty($data['author_id'])) {
            $data['author_id'] = User::id();
        }
        if (empty($data[ $this->fRecordID ])) {
            $data[ $this->fRecordID ] = $this->recordID;
        }
        $data['created'] = $this->db->now();
        if ( ! empty($data['extra'])) {
            if (is_array($data['extra'])) {
                $data['extra'] = serialize($data['extra']);
            }
        }
        if (empty($data['type'])) {
            $data['type'] = Fairplay::CHAT_TYPE_MESSAGE;
        }
        $messageID = $this->db->insert($this->tableChat, $data);
        $data['id'] = $messageID;
        if ($data['type'] == Fairplay::CHAT_TYPE_MESSAGE) {
            Fairplay::model()->addNotification($this->recordID, $data['author_id'], $messageID, Fairplay::NOTIFICATIONS_TYPE_CHAT, $notified);
        }
        return $messageID;
    }

    /**
     * Список сообщений
     * @param int $start с какого начать
     * @param string $mode = prev|next|current выборка до сообщения, после или указанное.
     * @param int $limit кол. сообшений
     * @return array
     */
    public function listing($start = 0, $mode = 'prev', $limit = 0)
    {
        if (empty($limit)) {
            $limit = config::sysAdmin('fairplay.workflow.chat.pagesize', 10, TYPE_UINT);
        }
        $filter = array($this->fRecordID => $this->recordID);
        if ($start) {
            switch($mode) {
                case 'prev':
                    $filter[':page'] = array('id < :start', ':start' => $start);
                    break;
                case 'next':
                    $filter[':page'] = array('id > :start', ':start' => $start);
                    break;
                case 'current':
                    $filter[':page'] = array('id = :start', ':start' => $start);
                    break;
            }
        }
        $filter = Model::filter($filter, 'C');
        $data = $this->db->select('
            SELECT * FROM '.$this->tableChat.' C '.$filter['where'].'
            ORDER BY id DESC
            LIMIT '.($limit + 1), $filter['bind']);
        $more = count($data) > $limit ? 1 : 0;
        if ($more) {
            array_pop($data);
        }
        $read = array();
        foreach($data as & $v) {
            $read[] = $v['id'];
            $v['extra'] = func::unserialize($v['extra']);
            if ( ! empty($v['extra']['attach'])) {
                foreach($v['extra']['attach'] as & $vv) {
                    if (empty($vv['filename'])) continue;
                    $vv['url'] = $this->getURL($vv['filename']);
                }
                unset($vv);
            }
        } unset($v);
        $userID = User::id();
        if (bff::adminPanel()) {
            $userID = 1;
        }
        Fairplay::model()->dropNotification($this->recordID, $userID, $read, Fairplay::NOTIFICATIONS_TYPE_CHAT);
        Fairplay::model()->calcNewCounters($userID);
        return array('list' => $data, 'more' => $more);
    }

    /**
     * Загрузка вложений методом FILES использоя компонент bff\files\Attachment;
     * @param string $field название поля
     * @return bool|mixed
     */
    public function uploadAttachFILES($field)
    {
        if (empty($_FILES[$field])) {
            return false;
        }
        if ($_FILES[$field]['error'] == UPLOAD_ERR_NO_FILE) {
            return false;
        }
        $this->checkFolderByID();
        $attach = new Attachment($this->getPath(), $this->maxSize);
        $attach->setAssignErrors(true);
        $attach->setFiledataAsString(false);
        return $attach->uploadFILES($field);
    }

    /**
     * Выполняем проверку существования папки (формируемую по ID)
     * Если нет - создаем.
     */
    protected function checkFolderByID()
    {
        if ($this->folderByID && ! empty($this->recordID)) {
            $dir = $this->path . intval($this->recordID / 1000);
            if ( ! file_exists($dir)) {
                Files::makeDir($dir);
            }
        }
    }

    /**
     * Формирование пути к файлу вложения
     * @param string $filename - название файла
     * @return string Путь
     */
    protected function getPath($filename = '')
    {
        return $this->path.($this->folderByID ? $this->getDir().DS : '').$filename;
    }

    /**
     * Формирование название директории исходя из ID записи
     * @return string название директории
     */
    protected function getDir()
    {
        return (string)intval($this->recordID / 1000);
    }

    /**
     * Формирование URL к файлу вложения
     * @param string $filename - название файла
     * @return string URL
     */
    public function getURL($filename = '')
    {
        return $this->url.($this->folderByID ? $this->getDir().'/' : '').$filename;
    }

    /**
     * Рассылка почтовых уведомлений о новых сообщениях
     */
    public function cronEmailNotification()
    {
        $timeoutSec = config::sysAdmin('fairplay.workflow.chat.notification.timeout', 60, TYPE_UINT);
        $users = $this->db->select('
            SELECT N.user_id, GROUP_CONCAT(N.workflow_id) AS workflow
            FROM '.TABLE_FAIRPLAY_WORKFLOWS_NOTIFICATIONS.' N, 
                 '.TABLE_FAIRPLAY_WORKFLOWS_CHAT.' C
            WHERE N.item_id = C.id AND N.is_notified = 0 AND N.type = :type AND C.created < :now
            GROUP BY N.user_id
            LIMIT 100
        ', array(
            ':now'  => date('Y-m-d H:i:s', time() - $timeoutSec),
            ':type' => Fairplay::NOTIFICATIONS_TYPE_CHAT,
        ));
        foreach ($users as $u)
        {
            if (empty($u['workflow'])) continue;
            $workflow = explode(',', $u['workflow']);
            $workflow = array_unique($workflow);

            $this->db->update(TABLE_FAIRPLAY_WORKFLOWS_NOTIFICATIONS, array(
                'is_notified'   => 1,
                'notified_date' => $this->db->now(),
            ), array(
                'user_id'     => $u['user_id'],
                'workflow_id' => $workflow,
                'is_notified' => 0,
                'type'        => Fairplay::NOTIFICATIONS_TYPE_CHAT,
            ));
            if ($u == 1) continue; # администратору на почту уведомления не отправляем

            $more = false;
            if (count($workflow) > 10) {
                $more = true;
                $workflow = array_slice($workflow, 0, 10);
            }
            $list = Fairplay::model()->workflowList(array('id' => $workflow));
            $workflow = array();
            foreach($list as $v) {
                $workflow[] = '<a href="'.Fairplay::url('view', array('id' => $v['id'], 'keyword' => $v['keyword'])).'">'.$v['title'].'</a>';
            }
            if (empty($workflow)) continue;
            if ($more) {
                $workflow[] = _t('fp', 'и др.');
            }
            Users::sendMailTemplateToUser($u['user_id'], 'workflow_chat_new', array(
                'workflows_list'   => join("<br />\n", $workflow),
            ));
        }
    }
}
