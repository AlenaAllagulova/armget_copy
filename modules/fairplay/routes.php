<?php

return [
    # просмотр хода работ
    'fairplay-view' => [
        'pattern'  => 'workflows/([\d]+)\-(.*)\.html',
        'callback' => 'fairplay/view/id=$1',
        'priority' => 250,
    ],
    # связь админ панели с системой оплаты
    'fairplay-oauth' => [
        'pattern'  => 'oauth/(.*)',
        'callback' => 'fairplay/oauth/system=$1',
        'priority' => 260,
    ],
    # просмотр документа хода работ
    'fairplay-document' => [
        'pattern'  => 'doc/([\d]+)\-(.*)',
        'callback' => 'fairplay/document/id=$1&hash=$2',
        'priority' => 270,
    ],
];