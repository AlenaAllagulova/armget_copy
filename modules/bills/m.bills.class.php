<?php

class M_Bills_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        if ($security->haveAccessToModuleToMethod('bills', 'listing')) {
            $menu->assign('Счет и услуги', 'Список счетов', 'bills', 'listing', true, 1);
        }
    }
}