<?php
tpl::includeJS('bills.my', false, 2);
?>
<div id="j-bills-select-pay-system">
<div class="row o-advertise-paymethods-list">

    <? $i = 0; foreach($psystems as $k => $v): ?>
    <? if($i && ($i % 4 == 0)): ?>
        </div>
        <div class="row o-advertise-paymethods-list">
    <? endif; $i++; ?>
    <div class="col-sm-3">
        <label class="o-advertise-paymethod j-ps-item<?= $i == 1 ? ' active' : '' ?>">
            <div class="o-paymethod-inner">
                <div class="o-paymethod-logo">
                    <div class="o-paymethod-logo-in">
                        <img src="<?= $v['logo_desktop'] ?>" alt="<?= HTML::escape($v['title']); ?>" />
                    </div>
                </div>
            </div>
            <div class="o-paymethod-inner">
                <input type="radio" name="ps" value="<?= $k ?>" class="j-ps-radio" <?= $i == 1 ? 'checked="checked"' : '' ?>>
                <span><?= $v['title'] ?></span>
            </div>
        </label>
        <div class="clearfix"></div>
    </div>
    <? endforeach; ?>

</div>
</div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jBillsSelectPay.init(<?= func::php2js(array(
            'lang' => array(),
        )) ?>);
    });
    <? js::stop(); ?>
</script>