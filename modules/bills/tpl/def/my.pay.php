<?php
    tpl::includeJS('bills.my', false, 2);
    $urlPay = Bills::url('my.pay');
?>
        <div class="container">

            <section class="l-mainContent">

                <div class="row">

                    <div class="col-md-8 col-md-offset-2">

                        <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                        <form method="post" action="<?= $urlPay ?>" id="j-my-pay-form">

                        	<h6 class="mrgt30"><?= _t('bills', 'На какую сумму вы желаете пополнить счет?'); ?></h6>
                            <div class="row">
                                <div class="col-sm-3">
                                <input type="text" name="amount" autocomplete="off" class="form-control" placeholder="<?= _t('bills', 'Сумма пополнения'); ?>" value="<?= $amount ?>">
                                </div>
                            </div>

                            <h6 class="mrgt40"><?= _t('bills', 'Выберите способ оплаты'); ?></h6>

                            <?= $payMethodsList ?>

                            <div class="c-formSubmit">
                                <button class="btn btn-primary c-formSuccess j-submit"><?= _t('bills', 'Пополнить'); ?></button>
                                <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                            </div>

                        </form>

					</div>

                </div>
            </section>
        </div>
    <script type="text/javascript">
        <? js::start(); ?>
        $(function(){
            jBillsMyPay.init(<?= func::php2js(array(
                'lang'       => array(),
                'url_submit' => $urlPay,
                'url_back'   => Bills::url('my.history'),
            )) ?>);
        });
        <? js::stop(); ?>
    </script>