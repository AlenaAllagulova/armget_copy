<?php
    tpl::includeJS('bills.my', false, 2);
    tpl::includeJS('datepicker', true);
?>
<div class="container">

    <section class="l-mainContent">

        <?= tpl::getBreadcrumbs($breadcrumbs); ?>

        <div class="row">

            <div class="col-md-8 col-md-offset-2" id="j-bills-history">

                <div class="p-profileBill-bill">
                    <div class="p-bill-heading"><?= _t('bills', 'На вашем счету:'); ?> <strong><?= $balance ?> <?= Site::currencyDefault(); ?></strong></div>
                    <a href="<?= Bills::url('my.pay') ?>" class="btn btn-secondary btn-sm"><?= _t('bills', 'Пополнить счет'); ?></a>
                </div>

                <form class="form-inline mrgt10 mrgb10" role="form" method="get" action="" id="j-bills-history-form">
                    <input type="hidden" name="page" value="<?= $f['page'] ?>" />
                    <div class="form-group">
                        <input type="text" name="fr" class="form-control input-sm j-datepicker" placeholder="<?= _t('', 'дд-мм-гггг'); ?>" value="<?= $f['fr'] ?>" autocomplete="off" />
                    </div>
                    <span class="hidden-xs">-</span>
                    <div class="form-group">
                        <input type="text" name="to" class="form-control input-sm j-datepicker" placeholder="<?= _t('', 'дд-мм-гггг'); ?>" value="<?= $f['to'] ?>" autocomplete="off" />
                    </div>
                    <button type="submit" class="btn btn-default btn-sm j-submit"><?= _t('bills', 'Показать'); ?></button>
                </form>

                <div class="j-list"><?= $list ?></div>
                <div class="j-pagination"><?= $pgn ?></div>

            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jBillsMyHistory.init(<?= func::php2js(array(
            'lang' => array(),
            'ajax' => true,
        )) ?>);
    });
    <? js::stop(); ?>
</script>