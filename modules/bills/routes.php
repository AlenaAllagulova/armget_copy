<?php

return [
    # список счетов пользователя
    'user-bill' => [
        'pattern'  => 'user/bill(/|)',
        'callback' => 'bills/my_bill',
        'priority' => 150,
    ],
    # процессинг оплаты услуг
    'bills-process' => [
        'pattern'  => 'bill/process/(.*)',
        'callback' => 'bills/processPayRequest/psystem=$1',
        'priority' => 480,
    ],
    # результат процессинга
    'bills-result' => [
        'pattern'  => 'bill/(success|fail|result)',
        'callback' => 'bills/$1/',
        'priority' => 490,
    ],
];