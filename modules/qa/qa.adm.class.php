<?php

/**
 * Права доступа группы:
 *  - qa: Ответы
 *      - questions: Управление вопросами
 *      - questions-moderate: Модерация вопросов
 *      - answers: Список ответов
 *      - answers-moderate: Модерация ответов
 *      - answers-comments: Управление комментариями к ответами
 *      - settings: Настройки
 *      - seo: SEO
 */
class Qa_ extends QaBase
{
    public function questions()
    {
        if (!($this->haveAccessTo('questions') || $this->haveAccessTo('questions-moderate')))
            return $this->showAccessDenied();

        $accessManage = $this->haveAccessTo('questions');

        $sAct = $this->input->postget('act',TYPE_STR);
        if ( ! empty($sAct) || Request::isPOST())
        {
            $aResponse = array();
            switch ($sAct)
            {
                case 'add':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateQuestionData(0, $bSubmit);
                    if ($bSubmit)
                    {

                        if (!$accessManage) {
                            $this->errors->accessDenied();
                            break;
                        }

                        if ($this->errors->no()) {
                            $aData['moderated'] = 1;
                            $aData['status'] = self::STATUS_OPENED;
                            $nQuestionID = $this->model->questionSave(0, $aData);
                            if ($nQuestionID > 0) {
                            }
                        }
                        break;
                    }

                    $aData['id'] = 0;
                    $aData['specs'] = Specializations::model()->specializationsOptions(0, 0, array(
                            'empty'     => 'Выбрать',
                            'emptyCats' => array('empty' => 'Выбрать'),
                            'noAllSpec' => 1
                        )
                    );
                    $aData['act'] = $sAct;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.questions.form');
                } break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nQuestionID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nQuestionID) { $this->errors->unknownRecord(); break; }

                    if ($bSubmit)
                    {

                        if (!$accessManage) {
                            $this->errors->accessDenied();
                            break;
                        }

                        $aData = $this->validateQuestionData($nQuestionID, $bSubmit);
                        if ($this->errors->no()) {
                            $this->model->questionSave($nQuestionID, $aData);
                        }
                        $aData['id'] = $nQuestionID;
                        break;
                    }

                    $aData = $this->model->questionData($nQuestionID, array(), true);
                    if (empty($aData)) { $this->errors->unknownRecord(); break; }
                    $aData['answers'] = $this->model->answersListing(array('question_id' => $nQuestionID));

                    $aData['specs'] = Specializations::model()->specializationsOptions(
                        $aData['spec_id'],
                        $aData['cat_id'],
                        array(
                            'empty'     => 'Выбрать',
                            'emptyCats' => array('empty' => 'Выбрать'),
                            'noAllSpec' => 1
                        )
                    );
                    $aData['act'] = $sAct;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.questions.form');
                } break;
                case 'toggle':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $nQuestionID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nQuestionID) { $this->errors->unknownRecord(); break; }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->questionToggle($nQuestionID, $sToggleType);
                } break;
                case 'delete':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $nQuestionID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nQuestionID) { $this->errors->unknownRecord(); break; }

                    $aData = $this->model->questionData($nQuestionID, array(), true);
                    if (empty($aData)) { $this->errors->impossible(); break; }

                    $res = $this->model->questionDelete($nQuestionID);
                    if ( ! $res) { $this->errors->impossible(); break; }
                    else {
                    }
                } break;
                case 'user': # autocomplete: пользователь
                {
                    $sEmail = $this->input->post('q', TYPE_NOTAGS);
                    $aFilter = array(
                        ':email'    => array(
                            (Users::model()->userEmailCrypted() ? 'BFF_DECRYPT(email)' : 'email') . ' LIKE :email',
                            ':email' => $sEmail . '%'
                        ),
                        'blocked'   => 0,
                        'activated' => 1,
                    );
                    $aUsers = Users::model()->usersList($aFilter, array('user_id', 'email'));
                    $this->autocompleteResponse($aUsers, 'user_id', 'email');
                }
                break;
                case 'dev-qa-counters-update':
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }
                    $this->moderationCounterUpdate();
                    $this->moderationAnswersCounterUpdate();
                    $this->model->questionsAnswersCounters();

                    $this->adminRedirect(($this->errors->no() ? Errors::SUCCESS : Errors::IMPOSSIBLE), bff::$event);
                } break;
                default: $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) $this->ajaxResponseForm($aResponse);
        }

        $f = array();
        $this->input->postgetm(array(
            'page' => TYPE_UINT,
            'tab'  => TYPE_INT,
            'q'    => TYPE_NOTAGS,
            'user' => TYPE_NOTAGS,
        ), $f);

        # формируем фильтр списка вопросов
        $sql = array();
        switch ($f['tab']) {
            case 0: { # Открытые
                $sql['status'] = self::STATUS_OPENED;
                if (self::premoderation()) {
                    $sql[':mod'] = 'Q.moderated > 0';
                }
            } break;
            case 1: { # Закрытые
                $sql['status'] = self::STATUS_CLOSED;
                if (self::premoderation()) {
                    $sql[':mod'] = 'Q.moderated > 0';
                }
            } break;
            case 2: { # На модерации
                $sql[':mod'] = 'Q.moderated != 1';
                $sql[':status'] = 'Q.status != '.self::STATUS_DELETED;
            } break;
            case 3: { $sql['status'] = self::STATUS_BLOCKED; } break; # Заблокированные
            case 4: { $sql['status'] = self::STATUS_DELETED; } break; # Удаленные
            case 5: { $sql['solved'] = 1; } break; # Решенные
            case 6: { $sql['status'] = self::STATUS_NOTACTIVATED; } break; # Неактивированные
        }

        if ( ! empty($f['q'])) {
            $sql[':q'] = array('(Q.id = '.intval($f['q']).' OR Q.title LIKE :q)', ':q' => '%'.$f['q'].'%');
        }

        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(Q.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }

        $nCount = $this->model->questionsListing($sql, true);
        $oPgn = new Pagination($nCount, 15, '#', 'jQaQuestionsList.page('.Pagination::PAGE_ID.'); return false;');
        $oPgn->pageNeighbours = 6;
        $aData['pgn'] = $oPgn->view(array('arrows'=>false));
        $aData['list'] = $this->model->questionsListing($sql, false, $oPgn->getLimitOffset(), 'created DESC');
        $aData['list'] = $this->viewPHP($aData, 'admin.questions.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list'=>$aData['list'],
                'pgn'=>$aData['pgn'],
            ));
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        return $this->viewPHP($aData, 'admin.questions.listing');
    }

    public function questions_status()
    {
        if ( ! $this->haveAccessTo('questions-moderate')) {
            $this->errors->accessDenied();
        }


        $questionID = $this->input->post('id', TYPE_UINT);
        if (!$questionID) {
            $this->errors->unknownRecord();
        } else {
            $aData = $this->model->questionData($questionID);
            if (empty($aData)) {
                $this->errors->unknownRecord();
            }
        }
        if ( ! $this->errors->no()) {
            $this->ajaxResponseForm();
        }

        $aResponse = array();
        switch ($this->input->postget('act', TYPE_STR))
        {
            case 'activate': # модерация: активация
            {
                if ($aData['status'] != self::STATUS_NOTACTIVATED) {
                    break;
                }

                # данные о владельце вопроса
                $aUserData = Users::model()->userData($aData['user_id'], array('user_id', 'activated'));
                if (empty($aUserData['activated'])) {
                    $this->errors->impossible();
                    break;
                }
                $this->onUserActivated($aData['user_id']);
            }
            break;
            case 'approve': # модерация: одобрение
            {
                $aUpdate = array(
                    'moderated' => 1
                );

                if ($aData['status'] == self::STATUS_BLOCKED) {
                    /**
                     * В случае если "Одобряем" заблокированный вопрос
                     * => значит он после блокировки был отредактирован пользователем
                     * => следовательно открываем
                     */
                    $newStatus = self::STATUS_OPENED;
                    $aUpdate[] = 'status_prev = status';
                    $aUpdate['status'] = $newStatus;
                }

                $res = $this->model->questionSave($questionID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик вопросов "на модерации"
                $this->moderationCounterUpdate();

                # Отправим письмо пользователю
                Users::sendMailTemplateToUser($aData['user_id'], 'qa_question_approved', array(
                    'question_id' => $questionID,
                    'question_title' => $aData['title'],
                    'question_url' => static::url('view', array('id' => $questionID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            }
            break;
            case 'close': # закрытие
            {
                if ($aData['status'] != self::STATUS_OPENED) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_CLOSED,
                    'moderated' => 1,
                    'expire'    => $this->db->now()
                );

                $res = $this->model->questionSave($questionID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик вопросов "на модерации"
                $this->moderationCounterUpdate();
            }
            break;
            case 'open': # открытие
            {
                if ($aData['status'] != self::STATUS_CLOSED) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_OPENED,
                    'moderated' => 1,
                    'solved'    => 0,
                );
                # время приема ответов
                $aTerms = static::aTerms($nDefTerm);
                if( ! array_key_exists($aData['term'], $aTerms)){
                    $aData['term'] = $nDefTerm;
                }
                $aUpdate['expire'] = date('Y-m-d H:i:s', time() + $aTerms[ $aData['term'] ]['days'] * 24*60*60);


                $res = $this->model->questionSave($questionID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик вопросов "на модерации"
                $this->moderationCounterUpdate();
            }
            break;
            case 'block': # блокировка
            {
                /**
                 * Блокировка (если уже заблокирован => изменение причины блокировки)
                 * @param string 'blocked_reason' причина блокировки
                 * @param integer 'id' ID вопрос
                 */

                $bUnblock = $this->input->post('unblock', TYPE_UINT);
                $sBlockedReason = $this->input->postget('blocked_reason', TYPE_NOTAGS, array('len' => 1000, 'len.sys' => 'qa.blocked_reason.limit'));
                $bBlocked = ($aData['status'] == self::STATUS_BLOCKED);

                $aUpdate = array(
                    'moderated'      => 1,
                    'blocked_reason' => $sBlockedReason,
                );

                $bBlockedResult = $bBlocked;
                if (!$bBlocked) { # блокируем
                    $aUpdate['status_prev'] = $aData['status'];
                    $aUpdate['status'] = self::STATUS_BLOCKED;
                    $bBlockedResult = true;

                    # Отправим письмо пользователю
                    Users::sendMailTemplateToUser($aData['user_id'], 'qa_question_blocked', array(
                        'question_id' => $questionID,
                        'question_title' => $aData['title'],
                        'question_url' => static::url('view', array('id' => $questionID, 'keyword' => $aData['keyword'])),
                        'blocked_reason' => nl2br($sBlockedReason),
                    ), Users::ENOTIFY_GENERAL);

                } else {
                    if ($bUnblock) {
                        # разблокируем
                        switch ($aData['status_prev']) {
                            case 0:
                            case self::STATUS_BLOCKED:
                                $aUpdate['status'] = self::STATUS_CLOSED;
                            break;
                            default:
                                $aUpdate['status'] = $aData['status_prev'];
                            break;
                        }
                        $aUpdate['status_prev'] = self::STATUS_BLOCKED;
                        $bBlockedResult = false;

                        # Отправим письмо пользователю
                        Users::sendMailTemplateToUser($aData['user_id'], 'qa_question_approved', array(
                            'question_id' => $questionID,
                            'question_title' => $aData['title'],
                            'question_url' => static::url('view', array('id' => $questionID, 'keyword' => $aData['keyword'])),
                        ), Users::ENOTIFY_GENERAL);
                    }
                }

                $res = $this->model->questionSave($questionID, $aUpdate);
                if (!$res) {
                    $this->errors->impossible();
                    break;
                }
                # обновляем счетчик вопросов "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['blocked'] = $bBlockedResult;
                $aResponse['reason'] = $sBlockedReason;
            }
            break;
        }

        if ($this->errors->no()) {
            $aData = $this->model->questionData($questionID);
            if ( ! empty($aData)) {
                $aData['only_form'] = true;
                $aData['is_popup'] = $this->input->post('is_popup', TYPE_BOOL);
                $aData['user'] = Users::model()->userData($aData['user_id'], array('blocked'));
                $aResponse['html'] = $this->viewPHP($aData, 'admin.questions.form.status');
            }
        }

        $this->ajaxResponseForm($aResponse);
    }

    public function answers()
    {
        if (!($this->haveAccessTo('answers') || $this->haveAccessTo('answers-moderate'))) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            $bAccessManage = $this->haveAccessTo('answers');
            $bAccessModerate = $this->haveAccessTo('answers-moderate');
            switch ($sAct) {
                case 'delete':
                {
                    if (!$bAccessManage) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nAnswerID = $this->input->postget('id', TYPE_UINT);
                    if (!$nAnswerID) {
                        $this->errors->impossible();
                        break;
                    }

                    $this->model->answerSave($nAnswerID, array('status' => static::STATUS_ANS_DEL_MODERATOR));
                    # обновляем счетчик ответов "на модерации"
                    $this->moderationAnswersCounterUpdate();
                    $aResponse['html'] = $this->answer_view($nAnswerID);
                }
                break;
                case 'approve':
                {
                    if (!$bAccessModerate) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nAnswerID = $this->input->postget('id', TYPE_UINT);
                    if (!$nAnswerID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->answerData($nAnswerID, array('question_id'));

                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $aUpdate = array(
                        'moderated' => 1
                    );

                    $res = $this->model->answerSave($nAnswerID, $aUpdate);
                    if (empty($res)) {
                        $this->errors->impossible();
                    }
                    # обновляем счетчик ответов "на модерации"
                    $this->moderationAnswersCounterUpdate();

                }
                break;
                case 'block':
                {
                    if (!$bAccessModerate) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nAnswerID = $this->input->postget('id', TYPE_UINT);
                    if (!$nAnswerID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->answerData($nAnswerID, array('question_id'));

                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $aUpdate = array(
                        'moderated' => 1,
                        'status'    => static::STATUS_ANS_BLOCK_MODERATOR,
                        'blocked_reason' => $this->input->post('reason', TYPE_NOTAGS),
                    );

                    $res = $this->model->answerSave($nAnswerID, $aUpdate);
                    if (empty($res)) {
                        $this->errors->impossible();
                    }
                    # обновляем счетчик ответов "на модерации"
                    $this->moderationAnswersCounterUpdate();
                    $aResponse['html'] = $this->answer_view($nAnswerID);
                }  break;
                case 'unblock':
                {
                    if (!$bAccessModerate) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nAnswerID = $this->input->postget('id', TYPE_UINT);
                    if (!$nAnswerID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->answerData($nAnswerID, array('status'));

                    if (empty($aData) || $aData['status'] != static::STATUS_ANS_BLOCK_MODERATOR) {
                        $this->errors->impossible();
                        break;
                    }

                    $aUpdate = array(
                        'status'    => static::STATUS_ANS_DEFAULT,
                    );

                    $res = $this->model->answerSave($nAnswerID, $aUpdate);
                    if (empty($res)) {
                        $this->errors->impossible();
                    }
                    $aResponse['html'] = $this->answer_view($nAnswerID);
                }  break;
                case 'form':
                {
                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = array(
                        'v' => $this->model->answerData($nID, array(), true),
                    );
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.answer.listing.list.form');

                }  break;
                case 'save':
                {
                    if (!$bAccessManage) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = $this->input->postm(array(
                        'description' => TYPE_NOTAGS,
                    ));

                    if (strlen($aData['description']) < 10) {
                        $this->errors->set(_t('qa', 'Опишите подробнее ответ'), 'description');
                    }
                    if ( ! $this->errors->no()) {
                        break;
                    }
                    $aData['moderated'] = 1;
                    $this->model->answerSave($nID, $aData);
                    $aResponse['html'] = $this->answer_view($nID);
                }  break;
                case 'block-form':
                {
                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = array(
                        'v' => $this->model->answerData($nID, array(), true),
                        'showBlockForm' => 1,
                        'bCommonListing' => 1,
                        'urlEdit'        => $this->adminLink('questions&act=edit&id=').$nID,
                    );
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.answer.listing.list.view');
                } break;
                case 'cancel':
                {
                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aResponse['html'] = $this->answer_view($nID);
                } break;
                case 'comments':
                {
                    if (!$bAccessModerate) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nAnswerID = $this->input->postget('id', TYPE_UINT);
                    if (!$nAnswerID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->answerData($nAnswerID, array('question_id'));
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $aResponse['html'] = $this->answerComments()->admListing($nAnswerID);
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'tab'     => TYPE_UINT,
            'perpage' => TYPE_UINT,
            'user'    => TYPE_NOTAGS,
            'best'    => TYPE_BOOL,
        ));

        $sql = array();
        $sqlOrder = 'A.created';
        $aData['pgn'] = '';
        $aData['f'] = $f;

        switch ($f['tab']) {
            case 1: # Все
            {
            }
            break;
            default: # На модерации
            {
                $sql[':mod'] = 'A.moderated != 1 AND A.status = '.static::STATUS_ANS_DEFAULT;
            }
            break;
        }

        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(U.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }
        if ($f['best']) {
            $sql[':best'] = 'A.best > 0';
        }

        $nCount = $this->model->answersListing($sql, true);
        $aPerpage = $this->preparePerpage($f['perpage'], array(20, 40, 60));
        $sFilter = http_build_query($f);
        unset($f['page']);
        $oPgn = new Pagination($nCount, $f['perpage'], $this->adminLink("answers&$sFilter&page=" . Pagination::PAGE_ID));
        $aData['pgn'] = $oPgn->view();

        $aData['answers'] = ($nCount > 0 ?
            $this->model->answersListing($sql, false, $oPgn->getLimitOffset(), $sqlOrder) :
            array());

        $aData['perpage'] = $aPerpage;

        return $this->viewPHP($aData, 'admin.answers.listing');
    }

    protected function answer_view($nAnswerID)
    {
        $aData = array(
            'v'              => $this->model->answerData($nAnswerID, array(), true),
            'bCommonListing' => 1,
            'urlEdit'        => $this->adminLink('questions&act=edit&id=').$nAnswerID,
        );
        return $this->viewPHP($aData, 'admin.answer.listing.list.view');
    }

    public function comments_ajax()
    {
        if (!$this->haveAccessTo('answers-comments')) {
            return $this->showAccessDenied();
        }

        $this->answerComments()->admAjax();
    }

    public function comments_mod()
    {
        if (!$this->haveAccessTo('answers-comments')) {
            return $this->showAccessDenied();
        }

        return $this->answerComments()->admListingModerate(15);
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'share_code' => TYPE_STR,
            ), $aData);

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad();
        if ( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

}