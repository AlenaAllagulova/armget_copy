<?php

abstract class QaBase_ extends Module
{
    /** @var QaModel */
    var $model = null;
    var $securityKey = '168070291b381e450ac32fbea0712a1e';

    var $userRegisterEmbedded; # параметры регистрации пользователя, если вопрос задает не авторизованный пользователь

    # Статус вопроса:
    const STATUS_OPENED  = 1; # открыт
    const STATUS_CLOSED  = 2; # закрыт
    const STATUS_BLOCKED = 5; # заблокирован
    const STATUS_DELETED = 6; # удален
    const STATUS_NOTACTIVATED = 7; # не активировано

    # Статус ответа:
    const STATUS_ANS_DEFAULT          = 0;
    const STATUS_ANS_DEL_OWNER_ANS    = 1; # удален владельцем ответа
    const STATUS_ANS_DEL_OWNER_QUEST  = 2; # удален владельцем вопроса
    const STATUS_ANS_DEL_MODERATOR    = 3; # удален модератором
    const STATUS_ANS_BLOCK_MODERATOR  = 4; # заблокирован модератором

    public function init()
    {
        parent::init();

        bff::autoloadEx(array(
            'QaAnswerComments' => array('app', 'modules/qa/qa.answer.comments.php'),
        ));

        $this->module_title = _t('qa','Ответы');

        # параметры регистрации пользователя
        $this->userRegisterEmbedded = array(
            'force_type'        => Users::TYPE_WORKER,
            'force_role'        => Users::ROLE_PRIVATE,
            'generate_password' => true,
            'no_captcha'        => true,
        );
    }

    /**
     * @return Qa
     */
    public static function i()
    {
        return bff::module('Qa');
    }

    /**
     * @return QaModel
     */
    public static function model()
    {
        return bff::model('Qa');
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'qa_answer_new' => array(
                'title'       => 'Ответы: Новый ответ',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при добавлении ответа на его вопрос',
                'vars'        => array(
                    '{fio}'            => 'ФИО пользователя',
                    '{name}'           => 'Имя пользователя',
                    '{question_id}'    => 'ID вопроса',
                    '{question_title}' => 'Заголовок вопроса',
                    '{question_url}'   => 'Ссылка для просмотра вопроса',
                ),
                'impl'        => true,
                'priority'    => 60,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
            'qa_question_blocked' => array(
                'title'       => 'Ответы: Вопрос заблокирован модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при блокировке вопроса модератором',
                'vars'        => array(
                    '{fio}'            => 'ФИО пользователя',
                    '{name}'           => 'Имя пользователя',
                    '{question_id}'    => 'ID вопроса',
                    '{question_title}' => 'Заголовок вопроса',
                    '{question_url}'   => 'Ссылка для просмотра вопроса',
                    '{blocked_reason}' => 'Причина блокировки',
                ),
                'impl'        => true,
                'priority'    => 61,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
            'qa_question_approved' => array(
                'title'       => 'Ответы: Вопрос одобрен модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при одобрении вопроса модератором',
                'vars'        => array(
                    '{fio}'            => 'ФИО пользователя',
                    '{name}'           => 'Имя пользователя',
                    '{question_id}'    => 'ID вопроса',
                    '{question_title}' => 'Заголовок вопроса',
                    '{question_url}'   => 'Ссылка для просмотра вопроса',
                ),
                'impl'        => true,
                'priority'    => 62,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
        );
        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Добавление
            case 'add':
                $url .= '/qa/add/' . ( ! empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Редактирование
            case 'edit':
                $url .= '/qa/edit/' . static::urlQuery($opts);
                break;
            # Информация о статусе вопроса
            case 'status':
                $url .= '/qa/status/' . static::urlQuery($opts);
                break;
            # Список
            case 'list':
                $url .= '/qa/' . ( ! empty($opts['keyword']) ? $opts['keyword'] . '/' : '') .
                    static::urlQuery($opts, array('keyword'));
                break;
            # Просмотр
            case 'view':
                if (empty($opts['id']) || empty($opts['keyword'])) {
                    $url .= '/qa/';
                    break;
                }
                $url .= '/qa/' . $opts['id'] . '-' . $opts['keyword'] . static::urlQuery($opts, array('id', 'keyword'));
                break;
            # Кабинет: Вопросы
            case 'my.list':
                $opts['login'] = User::data('login');
                $url = static::url('user.list', $opts, $dynamic);
                break;
            # Профиль: Вопросы пользователя
            case 'user.list':
                $url  = Users::url('profile', array('login' => $opts['login'], 'tab' => 'qa'), $dynamic);
                $url .= static::urlQuery($opts, array('login'));
                break;
        }
        return bff::filter('qa.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        $templates = array(
            'pages'  => array(
                'listing' => array(
                    't'      => 'Список',
                    'list'   => true,
                    'macros' => array(),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'listing-cat' => array(
                    't'       => 'Список (категория)',
                    'list'    => true,
                    'inherit' => true,
                    'macros'  => array(
                        'title' => array('t' => 'Название категории'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'listing-spec' => array(
                    't'       => 'Список (специализация)',
                    'list'    => true,
                    'inherit' => true,
                    'macros'  => array(
                        'title' => array('t' => 'Название специализации'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-keyword' => array(
                    't'      => 'Поиск по ключевому слову',
                    'list'   => true,
                    'macros' => array(
                        'query' => array('t'=>'Строка запроса'),
                    ),
                ),
                'view' => array(
                    't'       => 'Просмотр вопроса',
                    'macros'  => array(
                        'title'       => array('t' => 'Заголовок (до 50 символов)'),
                        'title.full'  => array('t' => 'Заголовок (полный)'),
                        'description' => array('t' => 'Краткое описание (до 150 символов)'),
                    ),
                    'fields' => array(
                        'share_title'       => array(
                            't'    => 'Заголовок (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                        'share_description' => array(
                            't'    => 'Описание (поделиться в соц. сетях)',
                            'type' => 'textarea',
                        ),
                        'share_sitename'    => array(
                            't'    => 'Название сайта (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                    ),
                ),
            ),
        );

        if ( ! Specializations::catsOn()) {
            unset($templates['pages']['listing-cat']);
        }

        return $templates;
    }

    /**
     * Включен ли раздел "Ответы"
     * @return bool true - включен
     */
    public static function enabled()
    {
        return config::sysAdmin('qa.enabled', true, TYPE_BOOL);
    }

    /**
     * Использовать премодерацию вопросов
     * @return bool
     */
    public static function premoderation()
    {
        return config::sysAdmin('qa.premoderation', true, TYPE_BOOL);
    }

    /**
     * Выводить блок "Лидеры раздела"
     * @return bool true - включен
     */
    public static function bestEnabled()
    {
        return config::sysAdmin('qa.best', false, TYPE_BOOL);
    }

    /**
     * Количество пользователей в блоке "Лидеры раздела"
     * @return bool true - включен
     */
    public static function bestLimit()
    {
        return config::sysAdmin('qa.best.limit', 5, TYPE_UINT);
    }

    /**
     * Варианты сроков приема ответов на вопрос
     * @param integer $nDef @ref ID варианта по-умолчанию
     * @return array
     */
    public static function aTerms(& $nDef = 0)
    {
        static $aData = false, $def = false;
        if (!$aData || !$def) {
            $aData = bff::filter('qa.terms', array(
                1 => array('id' => 1, 'days' => 3,  'def' => 0),
                2 => array('id' => 2, 'days' => 5,  'def' => 0),
                3 => array('id' => 3, 'days' => 7,  'def' => 0),
                4 => array('id' => 4, 'days' => 10, 'def' => 1),
                5 => array('id' => 5, 'days' => 15, 'def' => 0),
                6 => array('id' => 6, 'days' => 20, 'def' => 0),
            ));
            foreach ($aData as $k => $v) {
                $aData[$k]['t'] = tpl::declension($v['days'], _t('qa', 'день;дня;дней'));
                if ($v['def']) {
                    $def = $k;
                }
            }
        }
        $nDef = $def;
        return bff::filter('qa.terms.result', $aData, array('nDef'=>&$nDef));
    }

    /**
     * Варианты сроков приема ответов на вопрос в виде select::options
     * @param integer $nSelected выбранный вариант
     * @param integer $nDays @ref
     * @return string HTML
     */
    public static function aTermsOptions($nSelected = 0, & $nDays = 0)
    {
        $aData = static::aTerms();
        if (!array_key_exists($nSelected, array_keys($aData))) {
            $nSelected = 0;
        }

        $sRet = '';
        foreach ($aData as $v) {
            $attr = array('value' => $v['id']);
            if ($nSelected) {
                if ($nSelected == $v['id']) {
                    $attr[] = 'selected';
                    $nDays = $v['days'];
                }
            } else {
                if ($v['def']) {
                    $attr[] = 'selected';
                    $nDays = $v['days'];
                }
            }
            $sRet .= '<option' . HTML::attributes($attr) . '>' . $v['t'] . '</option>';
        }
        return $sRet;
    }

    /**
     * Варианты типа удаляющего ответ
     * @param bool $bOwnerQuestion при просмотре владелецем вопроса
     * @param bool $bOwnerAnswer при просмотре владелецем ответа
     * @return array
     */
    public static function aAnswerStatus($bOwnerQuestion = false, $bOwnerAnswer = false)
    {
        $aData = array(
            static::STATUS_ANS_DEL_OWNER_ANS   => array('id' => static::STATUS_ANS_DEL_OWNER_ANS, 't' => _t('qa', 'Удален владельцем ответа')),
            static::STATUS_ANS_DEL_OWNER_QUEST => array('id' => static::STATUS_ANS_DEL_OWNER_QUEST, 't' => _t('qa', 'Удален владельцем вопроса')),
            static::STATUS_ANS_DEL_MODERATOR   => array('id' => static::STATUS_ANS_DEL_MODERATOR, 't' => _t('qa', 'Удален модератором')),
            static::STATUS_ANS_BLOCK_MODERATOR => array('id' => static::STATUS_ANS_BLOCK_MODERATOR, 't' => _t('qa', 'Заблокирован модератором')),
        );
        if ($bOwnerQuestion) {
            $aData[static::STATUS_ANS_DEL_OWNER_QUEST]['t'] = _t('qa', 'Вы удалили этот ответ');
        }
        if ($bOwnerAnswer) {
            $aData[static::STATUS_ANS_DEL_OWNER_ANS]['t'] = _t('qa', 'Вы удалили этот ответ');
        }
        return $aData;
    }

    /**
     * Инициализация компонента работы с комментариями к ответам
     * @return QaAnswerComments component
     */
    public function answerComments()
    {
        static $i;
        if (!isset($i)) {
            $i = new QaAnswerComments();
        }

        return $i;
    }

    /**
     * Метод обрабатывающий ситуацию с блокировкой/разблокировкой пользователя
     * @param integer $nUserID ID пользователя
     * @param boolean $bBlocked true - заблокирован, false - разблокирован
     */
    public function onUserBlocked($nUserID, $bBlocked)
    {
        if ($bBlocked) {
            # при блокировке пользователя -> блокируем все его вопросы
            $aQuestions = $this->model->questionsDataByFilter(
                array(
                    'user_id' => $nUserID,
                    'status' => array(self::STATUS_OPENED, self::STATUS_CLOSED),
                ),
                array('id')
            );
            if (!empty($aQuestions)) {
                $this->model->questionsSave(array_keys($aQuestions), array(
                        'status_prev = status',
                        'status' => self::STATUS_BLOCKED,
                        'blocked_reason' => _t('users', 'Аккаунт пользователя заблокирован'),
                    )
                );
            }
        } else {
            # при разблокировке -> разблокируем
            $aQuestions = $this->model->questionsDataByFilter(
                array(
                    'user_id' => $nUserID,
                    'status' => self::STATUS_BLOCKED
                ),
                array('id')
            );
            if (!empty($aQuestions)) {
                $this->model->questionsSave(array_keys($aQuestions), array(
                        'status = (CASE status_prev WHEN ' . self::STATUS_BLOCKED . ' THEN ' . self::STATUS_CLOSED . ' ELSE status_prev END)',
                        'status_prev' => self::STATUS_BLOCKED,
                        //'blocked_reason' => '', # оставляем последнюю причину блокировки
                    )
                );
            }
        }
    }

    /**
     * Обрабатываем параметры вопроса
     * @param integer $nQuestionID ID вопроса или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateQuestionData($nQuestionID, $bSubmit)
    {
        $aParams = array(
            'title'       => array(TYPE_NOTAGS, 'len' => 200, 'len.sys' => 'qa.question.title.limit'), # Вопрос
            'description' => array(TYPE_NOTAGS, 'len' => 65536, 'len.sys' => 'qa.question.description.limit'), # Описание
            'term'        => TYPE_UINT,   # Срок
        );

        if (bff::adminPanel()) {
            $aParams = $aParams + array(
                'cat_id' => TYPE_UINT,  # Категория
                'spec_id' => TYPE_UINT, # Специализация
            );
        } else {
            $aParams['specs'] = TYPE_ARRAY; # Специализация
        }

        $aData = $this->input->postm($aParams);

        if ($bSubmit) {
            if (mb_strlen($aData['title']) < config::sysAdmin('qa.question.title.min', 10, TYPE_UINT)) {
                $this->errors->set(_t('qa', 'Заголовок вопроса слишком короткий'));
            }
            if (mb_strlen($aData['description']) < config::sysAdmin('qa.question.description.min', 10, TYPE_UINT)) {
                $this->errors->set(_t('qa', 'Опишите подробнее ваш вопрос'));
            }

            if (!bff::adminPanel()) {
                # проверим специализации
                $this->cleanSpecializations($aData);

                # антиспам фильтр
                Site::i()->spamFilter(array(
                    array('text' => & $aData['title'],       'error'=>_t('qa', 'В указанном вами вопросе присутствует запрещенное слово "[word]"')),
                    array('text' => & $aData['description'], 'error'=>_t('qa', 'В указанном вами уточнении присутствует запрещенное слово "[word]"')),
                ));
            }

            if (empty($aData['spec_id'])) {
                $this->errors->set(_t('qa', 'Укажите раздел вопроса'));
            }

            # Пользователь при добавлении
            if (!$nQuestionID) {
                if (bff::adminPanel()) {
                    $aData['user_id'] = $this->input->post('user_id', TYPE_UINT);
                    if (!$aData['user_id']) {
                        $this->errors->set(_t('users', 'Укажите пользователя'));
                    } else {
                        $aUserData = Users::model()->userData($aData['user_id'], array('email'));
                        if (empty($aUserData)) {
                            $this->errors->set(_t('users', 'Пользователь указан некорректно'));
                        }
                    }
                } else {
                    $aData['user_id'] = User::id();
                }
            }

            # Срок приема ответов
            $aTerms = static::aTerms($nDefTerm);
            if (!array_key_exists($aData['term'], $aTerms)) {
                $aData['term'] = $nDefTerm;
            }
            $aData['expire'] = date('Y-m-d H:i:s', time() + $aTerms[$aData['term']]['days'] * 86400);

            # Формируем URL keyword
            $aData['keyword'] = trim(mb_strtolower(tpl::truncate(func::translit($aData['title'], true), config::sysAdmin('qa.question.keyword.truncate', 80, TYPE_UINT), '')), '-');
        }

        return $aData;
    }

    /**
     * Обрабатываем параметры ответа на вопрос
     * @param integer $nAnswerID ID ответа или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateAnswerData($nAnswerID, $bSubmit)
    {
        $aParams = array(
            'question_id' => TYPE_UINT,   # ID вопроса
            'description' => array(TYPE_NOTAGS, 'len' => 65536, 'len.sys' => 'qa.answer.description.limit'), # Описание
        );
        $aData = $this->input->postm($aParams);

        if ($bSubmit) {
            if (!$aData['question_id']) {
                $this->errors->reloadPage();
            } else {
                if (mb_strlen($aData['description']) < config::sysAdmin('qa.answer.description.min', 10, TYPE_UINT)) {
                    $this->errors->set(_t('qa', 'Опишите подробнее ваш ответ'));
                }

                if ( ! bff::adminPanel()) {
                    # антиспам фильтр
                    Site::i()->spamFilter(array(
                        array('text' => & $aData['description'], 'error'=>_t('qa', 'В указанном вами ответе присутствует запрещенное слово "[word]"')),
                    ));
                }
            }
        }

        return $aData;
    }

    /**
     * Валидация специализаций вопроса
     * @param array $aData @ref данные
     */
    public function cleanSpecializations(array & $aData)
    {
        if (!isset($aData['specs'])) {
            return;
        }

        $aSpecs = array();
        $aCats = array();
        foreach ($aData['specs'] as $v) {
            if (!isset($v['spec'])) {
                continue;
            }
            $nSpecID = $v['spec'];
            unset($v['spec']);
            if (!empty($v)) {
                $nCatID = end($v);
            } else {
                $nCatID = 0;
            }
            $aSpecs[] = array('cat_id' => $nCatID, 'spec_id' => $nSpecID);
            if ($nCatID && !in_array($nCatID, $aCats)) {
                $aCats[] = $nCatID;
            }
        }
        unset($aData['specs']);
        $aAllowCats = Specializations::model()->specializationsInCategories($aCats, array());
        foreach ($aSpecs as $k => $v) {
            if (!isset($aAllowCats[$v['cat_id']][$v['spec_id']])) {
                unset($aSpecs[$k]);
            }
        }
        if (empty($aSpecs)) {
            return;
        }
        $aSpecs = reset($aSpecs);
        $aData['spec_id'] = $aSpecs['spec_id'];
        $aData['cat_id'] = $aSpecs['cat_id'];
    }

    /**
     * Является ли текущий пользователь владельцем вопроса
     * @param integer $nQuestionID ID вопроса
     * @param integer|bool $nQuestionUserID ID пользователя вопроса или FALSE (получаем из БД)
     * @return boolean
     */
    public function isQuestionOwner($nQuestionID, $nQuestionUserID = false)
    {
        $nUserID = User::id();
        if (!$nUserID) {
            return false;
        }

        if ($nQuestionUserID === false) {
            $aData = $this->model->questionData($nQuestionID, array('user_id'));
            if (empty($aData)) {
                return false;
            }

            $nQuestionUserID = $aData['user_id'];
        }

        return ($nQuestionUserID > 0 && $nUserID == $nQuestionUserID);
    }

    /**
     * Проверка возможности добавления ответа на вопрос
     * @param int $nQuestionID ID вопроса
     * @param int $nUserID ID пользователя
     * @param bool $bHidden @ref флаг, скрыть форму
     * @return bool
     */
    public function isAllowAddAnswer($nQuestionID, $nUserID, & $bHidden = false)
    {
        $bHidden = false;
        if (!$nQuestionID || !$nUserID) return false;

        $aData = $this->model->questionData($nQuestionID, array('status','moderated','user_id'));
        if (empty($aData)) return false;

        if (static::premoderation()) {
            if (!$aData['moderated']) return false;
        }
        if ($aData['status'] != static::STATUS_OPENED && $nUserID != $aData['user_id']) {
            return false;
        }

        $aDataAnswers = $this->model->answersList($nQuestionID, array('user_id' => $nUserID));
        if (!empty($aDataAnswers)) {
            return false;
        }

        if ($nUserID == $aData['user_id']) {
            if (in_array($aData['status'], array(static::STATUS_OPENED, static::STATUS_CLOSED))) {
                $bHidden = ($aData['status'] == static::STATUS_CLOSED);
                return true;
            }
            return false;
        }

        return true;
    }

    /**
     * Актуализация счетчика вопросов ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->questionsModeratingCounter();
            config::save('qa_questions_moderating', $count, true);
        } else {
            config::saveCount('qa_questions_moderating', $increment, true);
        }
    }

    /**
     * Актуализация счетчика ответов ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationAnswersCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->answersModeratingCounter();
            config::save('qa_answers_moderating', $count, true);
        } else {
            config::saveCount('qa_answers_moderating', $increment, true);
        }
    }

    /**
     * Метод обрабатывающий ситуацию с активацией пользователя
     * @param integer $userID ID пользователя
     */
    public function onUserActivated($userID)
    {
        # активируем вопросы пользователя
        $data = $this->model->questionsDataByFilter(
            array(
                'user_id' => $userID,
                'status'  => self::STATUS_NOTACTIVATED,
            ),
            array('id')
        );
        if (!empty($data)) {
            $res = (int)$this->model->questionsSave(array_keys($data), array(
                    'status_prev'      => self::STATUS_NOTACTIVATED,
                    'status'           => self::STATUS_OPENED,
                    'moderated'        => 0, # помечаем на модерацию
                )
            );
            if ($res > 0) {
                # обновляем счетчик "на модерации"
                $this->moderationCounterUpdate();
            }
        }
    }

}