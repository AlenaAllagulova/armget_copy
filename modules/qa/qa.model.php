<?php

define('TABLE_QA_QUESTIONS',        DB_PREFIX.'qa_questions');
define('TABLE_QA_ANSWERS',          DB_PREFIX.'qa_answers');
define('TABLE_QA_ANSWERS_VOTES',    DB_PREFIX.'qa_answers_votes');
define('TABLE_QA_ANSWERS_COMMENTS', DB_PREFIX.'qa_answers_comments');

class QaModel_ extends Model
{
    /** @var QaBase */
    var $controller;
    
    # --------------------------------------------------------------------
    # Вопросы

    /**
     * Список вопросов (admin)
     * @param array $aFilter фильтр списка вопросов
     * @param bool $bCount только подсчет кол-ва вопросов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function questionsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $sFrom = '';
        if(isset($aFilter[':user'])){
            $sFrom .= ', '.TABLE_USERS.' U ';
            $aFilter[':ju'] = 'Q.user_id = U.user_id';
        }

        $aFilter = $this->prepareFilter($aFilter, 'Q');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(Q.id) FROM '.TABLE_QA_QUESTIONS.' Q '.$sFrom.$aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT Q.id, Q.keyword, Q.created, Q.title, Q.status
               FROM '.TABLE_QA_QUESTIONS.' Q '.$sFrom.'
               '.$aFilter['where']
               .( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '')
               .$sqlLimit, $aFilter['bind']);
    }

    /**
     * Список вопросов (frontend)
     * @param array $aFilter фильтр списка вопросов
     * @param bool $bCount только подсчет кол-ва вопросов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function questionsList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        # полнотекстовый поиск
        if (array_key_exists('text', $aFilter)) {
            $aFilter[':text'] = $this->db->prepareFulltextQuery($aFilter['text'], 'Q.title, Q.description');
            unset($aFilter['text']);
        }

        $aFilter['status'] = array(Qa::STATUS_OPENED, Qa::STATUS_CLOSED);
        $aFilter[':user'] = 'Q.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0';

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'Q');
            return $this->db->one_data('SELECT COUNT(Q.id) FROM '.TABLE_QA_QUESTIONS.' Q, '.TABLE_USERS.' U '.$aFilter['where'], $aFilter['bind']);
        }

        $aFilter[':spec'] = 'Q.spec_id = S.id';
        $aFilter[':spec_lang'] = $this->db->langAnd(false, 'S', 'SL');
        $aFilter[':user_stat'] = 'U.user_id = US.user_id';

        $aFilter = $this->prepareFilter($aFilter, 'Q');
        $aData = $this->db->select('
            SELECT Q.id, Q.user_id, Q.cat_id, Q.spec_id, Q.title, Q.status, Q.solved, Q.keyword, Q.created, Q.answer_cnt, Q.views_total,
                   S.keyword AS spec_keyword, SL.title AS spec_title,
                   U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex, US.last_activity
            FROM '.TABLE_QA_QUESTIONS.' Q,
                 '.TABLE_SPECIALIZATIONS.' S, '.TABLE_SPECIALIZATIONS_LANG.' SL,
                 '.TABLE_USERS.' U,
                 '.TABLE_USERS_STAT.' US
            '.$aFilter['where'].'
            '.( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '').'
            '.$sqlLimit, $aFilter['bind']);

        if ( ! empty($aData)){
            if(Specializations::catsOn()){
                $aCats = array();
                foreach($aData as $v){
                    if ( ! in_array($v['cat_id'], $aCats)){
                        $aCats[] = $v['cat_id'];
                    }
                }
                $aCats = Specializations::model()->categoriesListingInArray($aCats);
                foreach($aData as $k => $v){
                    $aData[$k]['cat_keyword'] = ( isset($aCats[$v['cat_id']]) ? $aCats[ $v['cat_id'] ]['keyword'] : '' );
                }
            }
        }

        return $aData;
    }

    /**
     * Список вопросов пользователя в кабинете (frontend)
     * @param int $nUserID ID пользователя
     * @param array $aFilter фильтр списка вопросов
     * @param bool $bCount только подсчет кол-ва вопросов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function questionOwnerList($nUserID, array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        if (Qa::premoderation() && $nUserID != User::id()){
            $aFilter['moderated'] = 1;
        }
        $aFilter['status'] = array(Qa::STATUS_OPENED, Qa::STATUS_CLOSED);
        if (User::isCurrent($nUserID)) {
            $aFilter['status'][] = Qa::STATUS_BLOCKED;
        }

        $aFilter[':own'] = array(' (Q.user_id = :own OR A.user_id = :own) ', ':own' => $nUserID);

        if(isset($aFilter['answer'])){
            $aFilter[':answer'] = array('A.user_id = :answer', ':answer' => $aFilter['answer']);
            unset($aFilter['answer']);
        }

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'Q');

            return $this->db->one_data('
                SELECT COUNT(id) FROM (
                    SELECT Q.id
                    FROM '.TABLE_QA_QUESTIONS.' Q
                        LEFT JOIN '.TABLE_QA_ANSWERS.' A ON Q.id = A.question_id AND A.user_id = :own
                    '.$aFilter['where'].'
                    GROUP BY Q.id
                ) С', $aFilter['bind']);
        }

        $aFilter[':spec'] = 'Q.spec_id = S.id';
        $aFilter[':spec_lang'] = $this->db->langAnd(false, 'S', 'SL');
        $aFilter[':user'] = 'Q.user_id = U.user_id';

        $aFilter = $this->prepareFilter($aFilter, 'Q');

        $aData = $this->db->select('
            SELECT Q.id, Q.user_id, Q.cat_id, Q.spec_id, Q.title, Q.status, Q.solved, Q.keyword, Q.created, Q.answer_cnt, Q.views_total,
                   Q.moderated, Q.blocked_reason,
                   A.user_id AS answer_user_id, A.description AS answer,
                   S.keyword AS spec_keyword, SL.title AS spec_title,
                   U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex
            FROM '.TABLE_QA_QUESTIONS.' Q
                    LEFT JOIN '.TABLE_QA_ANSWERS.' A ON Q.id = A.question_id AND A.user_id = :own
                 ,'.TABLE_SPECIALIZATIONS.' S, '.TABLE_SPECIALIZATIONS_LANG.' SL,
                 '.TABLE_USERS.' U
            '.$aFilter['where'].'
            GROUP BY Q.id
            '.( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '').'
            '.$sqlLimit, $aFilter['bind']);

        if ( ! empty($aData)) {
            if (Specializations::catsOn()) {
                $aCats = array();
                foreach($aData as $v){
                    if ( ! in_array($v['cat_id'], $aCats)){
                        $aCats[] = $v['cat_id'];
                    }
                }
                $aCats = Specializations::model()->categoriesListingInArray($aCats);
                foreach($aData as $k => $v){
                    $aData[$k]['cat_keyword'] = $aCats[ $v['cat_id'] ]['keyword'];
                }
            }
        }

        return $aData;
    }

    /**
     * Получение данных вопроса
     * @param integer $nQuestionID ID вопроса
     * @param array $aFields список требуемых данных
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function questionData($nQuestionID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT Q.*, U.email, U.blocked AS user_blocked
                    FROM '.TABLE_QA_QUESTIONS.' Q, ' . TABLE_USERS . ' U
                    WHERE Q.id = :id AND Q.user_id = U.user_id',
                    array(':id' => $nQuestionID));
        } else {
            if (empty($aFields)) {
                $aFields = array('*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM ' . TABLE_QA_QUESTIONS . '
                    WHERE id = :id',
                array(':id' => $nQuestionID)
            );
        }
        return $aData;
    }

    /**
     * Получение данных вопроса для страницы просмотра
     * @param integer $nQuestionID ID вопроса
     * @return array
     */
    public function questionDataView($nQuestionID)
    {
        $aFilter['id'] = $nQuestionID;
        $aFilter[':spec'] = 'Q.spec_id = S.id';
        $aFilter[':spec_lang'] = $this->db->langAnd(false, 'S', 'SL');
        $aFilter[':user'] = 'Q.user_id = U.user_id';

        $sFrom = '';
        if (Specializations::catsOn()){
            $sFrom .= ', '.TABLE_SPECIALIZATIONS_CATS.' C, '.TABLE_SPECIALIZATIONS_CATS_LANG.' CL ';
            $aFilter[':cat'] = 'Q.cat_id = C.id';
            $aFilter[':cat_lang'] = $this->db->langAnd(false, 'C', 'CL');
        }

        $aFilter = $this->prepareFilter($aFilter, 'Q');
        $aData = $this->db->one_array('
            SELECT Q.*,
            S.keyword AS spec_keyword, SL.title AS spec_title, '.
            (Specializations::catsOn() ? 'C.keyword AS cat_keyword, CL.title AS cat_title, ' : '').'
            U.login, U.name, U.surname, U.pro, U.avatar, U.sex, U.verified, U.email, U.blocked AS user_blocked
            FROM '.TABLE_QA_QUESTIONS.' Q,
                 '.TABLE_SPECIALIZATIONS.' S, '.TABLE_SPECIALIZATIONS_LANG.' SL,
                 '.TABLE_USERS.' U '.$sFrom.
            $aFilter['where'], $aFilter['bind']);
        return $aData;
    }

    /**
     * Сохранение вопроса
     * @param integer $nQuestionID ID вопроса
     * @param array $aData данные вопроса
     * @return boolean|integer
     */
    public function questionSave($nQuestionID, array $aData)
    {
        if (empty($aData)) return false;

        if (isset($aData['status']) || isset($aData['status_prev']) || ! $nQuestionID) {
            $aData['status_changed'] = $this->db->now();
        }

        if ($nQuestionID > 0)
        {
            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_QA_QUESTIONS, $aData, array('id'=>$nQuestionID));

            return ! empty($res);
        }
        else
        {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nQuestionID = $this->db->insert(TABLE_QA_QUESTIONS, $aData);
            if ($nQuestionID > 0) {
                //
            }
            return $nQuestionID;
        }
    }

    /**
     * Переключатели вопроса
     * @param integer $nQuestionID ID вопроса
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function questionToggle($nQuestionID, $sField)
    {
        switch ($sField) {
            case '?': {
                // $this->toggleInt(TABLE_QA_QUESTIONS, $nQuestionID, $sField, 'id');
            } break;
        }
    }

    /**
     * Удаление вопроса
     * @param integer $nQuestionID ID вопроса
     * @return boolean
     */
    public function questionDelete($nQuestionID)
    {
        if (empty($nQuestionID)) return false;

        $this->ratingUpdate('dec', $nQuestionID); # обновим рейтинг

        $res = $this->db->delete(TABLE_QA_QUESTIONS, array('id'=>$nQuestionID));
        if (empty($res)) return false;

        $this->db->delete(TABLE_QA_ANSWERS_COMMENTS, array('question_id'=>$nQuestionID));
        $aAnswers = $this->answersListing(array('question_id' => $nQuestionID));
        $aAnswersIDs = array();
        foreach($aAnswers as $v){
            if( ! in_array($v['id'], $aAnswersIDs)){
                $aAnswersIDs[] = $v['id'];
            }
        }
        if( ! empty($aAnswersIDs)){
            $this->db->delete(TABLE_QA_ANSWERS_VOTES, array('answer_id'=>$aAnswersIDs));
        }
        $this->db->delete(TABLE_QA_ANSWERS, array('question_id'=>$nQuestionID));

        # обновляем счетчик вопросов "на модерации"
        $this->controller->moderationCounterUpdate();
        # обновляем счетчик ответов "на модерации"
        $this->controller->moderationAnswersCounterUpdate();

        return true;
    }

    /**
     * Данные о нескольких вопросах
     * @param array $aFilter фильтр
     * @param array $aFields список полей требуемых данных
     * @param string $sqlOrder
     * @param bool $nLimit
     * @return mixed
     */
    public function questionsDataByFilter(array $aFilter, array $aFields = array(), $sqlOrder = '', $nLimit = false)
    {
        $aFilter = $this->prepareFilter($aFilter);

        $aParams = array();
        if (empty($aFields)) {
            $aFields = array('*');
        }
        if (!is_array($aFields)) {
            $aFields = array($aFields);
        }
        foreach ($aFields as $v) {
            $aParams[] = $v;
        }

        return $this->db->select_key('SELECT ' . join(',', $aParams) . '
                                  FROM ' . TABLE_QA_QUESTIONS . '
                                  ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . (!empty($nLimit) ? $this->db->prepareLimit(0, $nLimit) : ''),
            'id',
            $aFilter['bind']
        );
    }

    /**
     * Обновляем данные о вопросах
     * @param array $aQuestionsID ID вопросов
     * @param array $aData данные
     * @return integer кол-во обновленных вопросов
     */
    public function questionsSave(array $aQuestionsID, array $aData)
    {
        if (empty($aQuestionsID) || empty($aData)) {
            return 0;
        }

        return $this->db->update(TABLE_QA_QUESTIONS, $aData, array('id' => $aQuestionsID));
    }

    /**
     * Обработка закрытия вопросов по крону
     */
    public function questionsCronStatus()
    {
        $this->db->exec('
            UPDATE '.TABLE_QA_QUESTIONS.'
                SET status = :closed, status_prev = :opened, status_changed = :now
            WHERE status = :opened AND expire < :now',
            array(
                ':closed' => Qa::STATUS_CLOSED,
                ':opened' => Qa::STATUS_OPENED,
                ':now'    => $this->db->now(),
            ));
    }

    /**
     * Получаем общее кол-во вопросов, ожидающих модерации
     * @return integer
     */
    public function questionsModeratingCounter()
    {
        $aFilter = $this->prepareFilter(array(
                ':mod'    => 'Q.moderated != 1',
                ':status'  => 'Q.status != '.Qa::STATUS_DELETED
            ), 'Q'
        );

        return (int)$this->db->one_data('SELECT COUNT(Q.id)
                FROM ' . TABLE_QA_QUESTIONS . ' Q
                ' . $aFilter['where'], $aFilter['bind']
        );
    }

    /**
     * Пересчет счетчиков для всех вопросов и ответов
     */
    public function questionsAnswersCounters()
    {
        // Количество сответов на вопросы
        $this->db->exec('
            UPDATE '.TABLE_QA_QUESTIONS.' Q, (
                SELECT A.question_id, COUNT(*) AS cnt
                FROM '.TABLE_QA_ANSWERS.' A
                GROUP BY A.question_id
                ) Sl
            SET Q.answer_cnt = Sl.cnt
            WHERE Q.id = Sl.question_id
        ');

        // Сумма голосов за ответы
        $this->db->exec('
            UPDATE '.TABLE_QA_ANSWERS.' A, (
                SELECT V.answer_id, SUM(V.vote) AS votes
                FROM '.TABLE_QA_ANSWERS_VOTES.' V
                GROUP BY V.answer_id
                ) Sl
            SET A.votes = Sl.votes
            WHERE A.id = Sl.answer_id
        ');

    }

    # --------------------------------------------------------------------
    # Ответы

    /**
     * Получение данных ответа
     * @param integer $nAnswerID ID ответа
     * @param array $aFields список требуемых данных
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function answerData($nAnswerID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT A.*, U.email, U.blocked AS ublocked, U.name, U.login
                    FROM '.TABLE_QA_ANSWERS.' A, ' . TABLE_USERS . ' U
                    WHERE A.id = :id AND A.user_id = U.user_id',
                array(':id' => $nAnswerID));
        } else {
            if (empty($aFields)) {
                $aFields = array('*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM ' . TABLE_QA_ANSWERS . '
                    WHERE id = :id',
                array(':id' => $nAnswerID)
            );
        }
        return $aData;
    }

    /**
     * Сохранение ответа
     * @param integer $nAnswerID ID ответа
     * @param array $aData данные ответа
     * @return boolean|integer
     */
    public function answerSave($nAnswerID, array $aData)
    {
        if (empty($aData)) return false;

        if ($nAnswerID > 0)
        {
            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_QA_ANSWERS, $aData, array('id'=>$nAnswerID));

            return ! empty($res);
        }
        else
        {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nAnswerID = $this->db->insert(TABLE_QA_ANSWERS, $aData);
            if ($nAnswerID > 0) {
                $this->questionSave($aData['question_id'], array('answer_cnt = answer_cnt + 1'));
            }
            return $nAnswerID;
        }
    }

    /**
     * Список ответов (admin)
     * @param array $aFilter фильтр списка ответов
     * @param bool $bCount только подсчет кол-ва ответов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function answersListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = 'A.created') //admin
    {
        $sFrom = TABLE_QA_ANSWERS . ' A, ' . TABLE_USERS . ' U ';
        $aFilter[':ujoin'] = ' A.user_id = U.user_id ';

        $aFilter = $this->prepareFilter($aFilter, 'A');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(A.id) FROM ' . $sFrom . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('
            SELECT A.id, A.created, A.description, A.moderated, A.user_id, A.question_id, A.status, A.blocked_reason,
                A.comments_cnt, U.name, U.email, U.blocked AS ublocked, U.login
            FROM ' . $sFrom . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список ответов (frontend)
     * @param integer $nQuestionID ID ответа
     * @param array $aFilter фильтр списка ответов
     * @return mixed
     */
    public function answersList($nQuestionID, array $aFilter = array())
    {
        $aFilter['question_id'] = $nQuestionID;
        $aFilter[':user'] = 'A.user_id = U.user_id AND U.user_id = US.user_id';

        $nUserID = User::id();
        $aFilter = $this->prepareFilter($aFilter, 'A');
        if ($nUserID){
            $aFilter['bind'][':user'] = $nUserID;
        }
        $aData = $this->db->select('
            SELECT A.id, A.user_id, A.description, A.votes, A.best, A.status, A.comments_cnt, A.created, A.blocked_reason,
                   U.login, U.name, U.surname, U.pro, U.avatar, U.verified, US.last_activity, U.sex'.($nUserID ? ', V.vote' : ', 0 AS vote').'
            FROM '.TABLE_QA_ANSWERS.' A
                 '.( $nUserID ? 'LEFT JOIN '.TABLE_QA_ANSWERS_VOTES.' V ON A.id = V.answer_id AND V.user_id = :user' : '').'
                 , '.TABLE_USERS.' U
                 , '.TABLE_USERS_STAT.' US
            '.$aFilter['where'].'
             ORDER BY A.best DESC, A.votes DESC, A.created DESC', $aFilter['bind']);

        return $aData;
    }

    /**
     * Установим лучший ответ
     * @param int $nQuestionID ID вопроса
     * @param int $nAnswerID ID ответа
     */
    public function answerBest($nQuestionID, $nAnswerID)
    {
        $this->db->exec('
            UPDATE '.TABLE_QA_ANSWERS.'
            SET best = CASE
                    WHEN id = :answer THEN 1
                    ELSE 0
                END
            WHERE question_id = :question', array(
                ':question' => $nQuestionID,
                ':answer' => $nAnswerID,
        ));
    }

    /**
     * Лучший ответ
     * @param integer $nQuestionID ID вопроса
     * @return mixed
     */
    function answerBestData($nQuestionID)
    {
        return $this->db->one_array('
            SELECT * FROM '.TABLE_QA_ANSWERS.' WHERE question_id = :question AND best = 1
            LIMIT 1 ', array(':question' => $nQuestionID));
    }

    /**
     * Получаем общее кол-во ответов, ожидающих модерации
     * @return integer
     */
    public function answersModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(A.id)
                FROM ' . TABLE_QA_ANSWERS . ' A
                WHERE A.moderated != 1 AND A.status = '.Qa::STATUS_ANS_DEFAULT
        );
    }

    /**
     * Изменение рейтинга пользователя
     * @param string $sType тип обновления ('inc', 'dec')
     * @param integer $nQuestionID ID вопроса
     */
    public function ratingUpdate($sType, $nQuestionID)
    {
        $aQuestionData = $this->questionData($nQuestionID, array('status', 'user_id'));

        # для удаленных вопросов рейтинг не меняется
        if ($aQuestionData['status'] == Qa::STATUS_DELETED) {
            return;
        }

        $aBest = $this->db->one_array('
            SELECT * FROM '.TABLE_QA_ANSWERS.' WHERE question_id = :question AND best = 1
            LIMIT 1 ', array(':question' => $nQuestionID));
        if (empty($aBest)) return;

        # свои ответы на свои вопросы рейтинг не изменяют
        if ($aQuestionData['user_id'] == $aBest['user_id']) return;

        $aBestUser = Users::model()->userData($aBest['user_id'], array('type'));
        if (empty($aBestUser)) return;

        $nUserID = $aBest['user_id'];

        # меняем рейтинг только для исполнителей
        if (Users::useClient() && $aBestUser['type'] != Users::TYPE_WORKER) {
            return;
        }

        $nRating = config::sysAdmin('users.rating.qa.best', 10, TYPE_UINT);
        switch ($sType) {
            case 'inc': # увеличим рейтинг
            {
                Users::model()->ratingChange($nUserID, $nRating, 'qa-best-add',
                    array('question_id'=>$nQuestionID));
            }
            break;
            case 'dec': # уменьшим рейтинг
            {
                Users::model()->ratingChange($nUserID, -$nRating, 'qa-best-del',
                    array('question_id'=>$nQuestionID));
            }
            break;
        }
    }

    # --------------------------------------------------------------------
    # Голоса за ответы

    /**
     * Сохранение голоса за ответ
     * @param array $aData данные о голосе
     * @return bool|int сумма голосов за ответ
     */
    public function voteSave(array $aData)
    {
        if (empty($aData) || empty($aData['answer_id'])) return false;

        $res = $this->db->insert(TABLE_QA_ANSWERS_VOTES, $aData, false);
        if (!$res) return false;

        # обновляем сумму голосов за ответ
        $nAnswerID = $aData['answer_id'];
        $nSum = $this->votesSum(array('answer_id' => $nAnswerID));
        $this->answerSave($nAnswerID, array('votes' => $nSum));

        return $nSum;
    }

    /**
     * Сумма голосов за ответ на основе фильтра
     * @param array $aFilter
     * @return integer (-6, 0, 4)
     */
    public function votesSum(array $aFilter = array())
    {
        if (empty($aFilter)) return 0;

        $aFilter = $this->prepareFilter($aFilter, 'V');
        return (int)$this->db->one_data('SELECT SUM(V.vote)
            FROM '.TABLE_QA_ANSWERS_VOTES.' V
            '.$aFilter['where'], $aFilter['bind']);
    }

    /**
     * Лидеры раздела
     * @param array $filter фильтр
     * @param int $limit лимит
     * @return mixed
     */
    public function bestList(array $filter = array(), $limit = 5)
    {
        $filter[':ju'] = 'A.user_id = U.user_id';
        $filter[':bl'] = array('U.blocked = :bl', ':bl' => 0);
        $filter[':del'] = array('U.deleted = :del', ':del' => 0);
        $from =  ' FROM '.TABLE_QA_ANSWERS.' A, '.TABLE_USERS.' U ';

        # фильтр по специализациям
        if (isset($filter['spec_id'])) {
            $from .= ', '.TABLE_QA_QUESTIONS.' Q ';
            $filter[':jq'] = 'A.question_id = Q.id';
            if (!is_array($filter['spec_id'])) {
                $filter[':sp'] = array('Q.spec_id = :sp', ':sp' => $filter['spec_id']);
            } else {
                $filter['Q.spec_id'] = $filter['spec_id'];
            }
            unset($filter['spec_id']);
        }

        $filter = $this->prepareFilter($filter);

        return $this->db->select('
            SELECT A.user_id, COUNT(A.id) AS cnt, SUM(A.best) AS best, U.login, U.name, U.surname, U.verified, U.pro, U.avatar, U.sex
            '.$from.$filter['where'].'
            GROUP BY A.user_id
            ORDER BY best DESC, cnt DESC
            LIMIT '.$limit, $filter['bind']);
    }

    /**
     * Статистика
     * @return array
     */
    public function statisticData()
    {
        $data = array();

        # вопросов
        $filter = array('status' => array(Qa::STATUS_OPENED, Qa::STATUS_CLOSED));
        if (Qa::premoderation()) {
            $filter[':moderated'] = 'moderated > 0';
        }
        $filter[':user'] = 'Q.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';
        $filter = $this->prepareFilter($filter, 'Q');
        $data['questions'] = (int)$this->db->one_data('SELECT COUNT(Q.id) FROM '.TABLE_QA_QUESTIONS.' Q, '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);

        return $data;
    }

}