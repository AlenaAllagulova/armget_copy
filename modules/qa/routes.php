<?php

return [
    # просмотр вопроса
    'qa-view' => [
        'pattern'  => 'qa/([\d]+)\-(.*)(/|)',
        'callback' => 'qa/view/id=$1',
        'priority' => 280,
    ],
    # добавление/редактирование/поиск/продвижение, ...
    'qa-action' => [
        'pattern'  => 'qa/(add|edit|promote|search|status)(/|)',
        'callback' => 'qa/$1/',
        'priority' => 290,
    ],
    # поиск по специализации
    'qa-search-spec' => [
        'pattern'  => 'qa/(.+)',
        'callback' => 'qa/search/spec=$1',
        'priority' => 300,
    ],
    # главная
    'qa-search' => [
        'pattern'  => 'qa(/|)',
        'callback' => 'qa/search',
        'priority' => 310,
    ],
];
