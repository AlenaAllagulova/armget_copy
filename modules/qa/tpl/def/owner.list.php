<?php
/**
 * Список вопросов пользователя.
 * @var $this Qa
 */
$bOwner = User::isCurrent($id);
tpl::includeJS('qa.owner.list', false, 2);
?>
    <div class="p-profileContent" id="j-qa-owner-list">

        <form method="get" action="" id="j-qa-owner-list-form">
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            <input type="hidden" name="qa" value="<?= $f['qa'] ?>" />
            <input type="hidden" name="st" value="<?= $f['st'] ?>" />

        <div class="p-profile-title">
            <ul class="p-profile-submenu visible-lg">
                <? foreach($qa as $v): ?>
                <li<?= $v['id'] == $f['qa'] ? ' class="active"' : ''?>><a href="#" class="j-f-qa" data-id="<?= $v['id'] ?>"><?= $v['t'] ?></a></li>
                <? endforeach; ?>
            </ul>

            <div class="dropdown pull-right p-profile-submenu-dropdowns">
                <a href="#" class="ajax-link" data-toggle="dropdown"><span class="j-f-st-title"><?= $st[ $f['st'] ]['t'] ?></span><b class="caret"></b></a>
                <ul class="dropdown-menu c-dropdown-caret_right" role="menu">
                    <? foreach($st as $v): ?>
                        <li<?= $v['id'] == $f['st'] ? ' class="active"' : ''?>><a href="#" class="j-f-st" data-id="<?= $v['id'] ?>"><?= $v['t'] ?></a></li>
                    <? endforeach; ?>
                </ul>
            </div>

            <div class="dropdown hidden-lg pull-left p-profile-submenu-dropdowns">
                <a href="#" class="ajax-link" data-toggle="dropdown"><span class="j-f-qa-title"><?= $qa[ $f['qa'] ]['t'] ?></span><b class="caret"></b></a>
                <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                    <? foreach($qa as $v): ?>
                        <li<?= $v['id'] == $f['qa'] ? ' class="active"' : ''?>><a href="#" class="j-f-qa" data-id="<?= $v['id'] ?>"><?= $v['t'] ?></a></li>
                    <? endforeach; ?>
                </ul>
            </div>
            <div class="clearfix"></div>

        </div>
        </form>

        <div class="j-list"><?= $list ?></div>

        <div class="j-pagination"><?= $pgn ?></div>

    </div>
<script type="text/javascript">
    <? js::start(); ?>
    jQaOwnerList.init(<?= func::php2js(array(
        'lang'   => array(
            'delete'   => _t('qa', 'Удалить вопрос?'),
        ),
        'qa'    => $qa,
        'st'    => $st,
        'owner' => $bOwner,
        'ajax'  => true,
    )) ?>);
    <? js::stop(); ?>
</script>