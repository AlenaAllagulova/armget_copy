<?php
    /**
     * @var $this Qa
     */
    $aData = HTML::escape($aData, 'html', array('cat_id','spec_id','title','description'));
    $edit = ! empty($id);

    $tab = $this->input->getpost('ftab', TYPE_NOTAGS);
    $aTabs = array(
        'info'    => _t('qa', 'Описание'),
    );
    if($edit){
        $aTabs['answers'] = _t('qa', 'Ответы');
    }
    if( ! isset($aTabs[$tab])) {
        $tab = 'info';
    }
if(count($aTabs) > 1): ?>
    <div class="tabsBar">
        <? foreach($aTabs as $k=>$v): ?>
            <span class="tab<?= $k==$tab ? ' tab-active' : '' ?>"><a href="#" onclick="jQaQuestionsForm.onTab('<?= $k ?>', this); return false;"><?= $v ?></a></span>
        <? endforeach; ?>
        <div class="progress" style="margin-left: 5px; display: none;" id="form-progress"></div>
    </div>
<? endif; ?>

<div id="qa-form-block-info" class="hidden">
<form name="QaQuestionsForm" id="QaQuestionsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
<input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
<input type="hidden" name="save" value="1" />
<input type="hidden" name="id" value="<?= $id ?>" />
<table class="admtbl tbledit">
<tr class="required">
    <td class="row1 field-title" width="100">Вопрос<span class="required-mark">*</span></td>
    <td class="row2">
        <input class="stretch" type="text" id="question-title" name="title" value="<?= $title ?>" maxlength="200" />
    </td>
</tr>
<tr class="required">
    <td class="row1 field-title">Описание<span class="required-mark">*</span></td>
    <td class="row2">
        <textarea class="stretch" id="question-description" rows="8" name="description"><?= $description ?></textarea>
    </td>
</tr>
<tr class="check-select">
    <td class="row1 field-title">Раздел<span class="required-mark">*</span></td>
    <td class="row2">
        <input type="hidden" name="cat_id" class="j-cat-id" value="<?= $cat_id ?>" />
        <input type="hidden" name="spec_id" class="j-spec-id" value="<?= $spec_id ?>" />
        <?
        foreach($specs as $lvl => $v) {
            ?><select class="spec-select" data-lvl="<?= $lvl ?>" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jQaQuestionsForm.onSpecialization($(this))"><?= $v['categories'] ?></select><?
        }
        ?>
    </td>
</tr>
<? if($edit): ?>
<tr>
    <td class="row1 field-title"><?= _t('', 'User') ?></td>
    <td class="row2">
        <a href="#" class="ajax" onclick="return bff.userinfo(<?= $user_id ?>);"><?= $email ?></a>
    </td>
</tr>
<tr>
    <td class="row1" colspan="2">
        <? $aData['user'] = array('blocked'=>$user_blocked); ?>
        <?= $this->viewPHP($aData, 'admin.questions.form.status'); ?>
    </td>
</tr>
<? else: ?>
    <tr>
        <td class="row1 field-title"><?= _t('', 'User') ?><span class="required-mark">*</span></td>
        <td class="row2">
            <input type="hidden" name="user_id" value="0" id="j-qa-question-user-id" />
            <input type="text" value="" id="j-qa-question-user-email" class="autocomplete" placeholder="<?= _t('', 'Enter user e-mail') ?>" style="width: 220px;" />
        </td>
    </tr>
<? endif; ?>
<tr class="footer">
    <td colspan="2">
        <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jQaQuestionsForm.save(false);" />
        <? if ($edit): ?>
            <input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jQaQuestionsForm.save(true);" />
            <input type="button" class="btn btn-danger button" value="<?= _t('', 'Delete') ?>" onclick="jQaQuestionsForm.del();" />
        <? endif; ?>
        <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jQaQuestionsFormManager.action('cancel');" />
    </td>
</tr>
</table>
</form>
</div>

<? if($edit): ?><div id="qa-form-block-answers" class="hidden"><?= $this->viewPHP($aData, 'admin.answers.listing.list'); ?></div><? endif; ?>

<script type="text/javascript">
var jQaQuestionsForm =
(function(){
    var $progress, $form, formChk, id = parseInt(<?= $id ?>), blocksPrefix = 'qa-form-block-';
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $progress = $('#QaQuestionsFormProgress');
        $form = $('#QaQuestionsForm');

        <? if( ! $edit): ?>
        $form.find('#j-qa-question-user-email').autocomplete(ajaxUrl+'&act=user',
            {valueInput: $form.find('#j-qa-question-user-id')});
        <? endif; ?>

        onTab('<?= $tab ?>');
    });

    function onTab(key, tabLink){
        $('[id^="'+blocksPrefix+'"]').addClass('hidden');
        $('#'+blocksPrefix+key).removeClass('hidden');
        $(tabLink).parent().addClass('tab-active').siblings().removeClass('tab-active');
        if(bff.h) {
            window.history.pushState({}, document.title, ajaxUrl + '&act=<?= $act ?>&id=<?= $id ?>&ftab=' + key);
        }
    }
    return {
        del: function()
        {
            if( id > 0 ) {
                bff.ajaxDelete('sure', id, ajaxUrl + '&act=delete&id=' + id,
                false, {
                    progress: $progress, repaint: false, onComplete: function () {
                        bff.success('Вопрос был успешно удален');
                        jQaQuestionsFormManager.action('cancel');
                        jQaQuestionsList.refresh();
                    }
                });
            }
        },
        save: function(returnToList)
        {
            if( ! formChk.check(true) ) return;
            bff.ajax(ajaxUrl, $form.serialize(), function(data){
                if(data && data.success) {
                    bff.success('Данные успешно сохранены');
                    if(returnToList || ! id) {
                        jQaQuestionsFormManager.action('cancel');
                        jQaQuestionsList.refresh( ! id);
                    }
                }
            }, $progress);
        },
        onShow: function ()
        {
            formChk = new bff.formChecker($form);
        },
        onSpecialization: function($select)
        {
            var val = intval($select.val());
            var lvl = $select.data('lvl');
            if(lvl == 'spec'){
                $form.find('.j-spec-id').val(val);
            } else {
                $form.find('.j-cat-id').val(val);
                $form.find('.j-spec-id').val(0);
                $select.nextAll().remove();
                bff.ajax('<?= $this->adminLink('specializations_list&act=category-data&noAllSpec=1', 'specializations'); ?>', {cat_id:val,lvl:lvl}, function(data){
                    if(data && data.success && data.subs > 0) {
                        $select.after('<select class="spec-select" data-lvl="'+data.lvl+'" style="margin-right: 5px;" onchange="jQaQuestionsForm.onSpecialization($(this))">'+data.cats+'</select>').show();
                    }
                });
            }
        },
        onTab: onTab
    };
}());
</script>