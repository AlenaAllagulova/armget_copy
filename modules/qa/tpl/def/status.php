<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <? if( ! empty($confirm)): ?>
            <div class="col-md-8 col-md-offset-2">
                <h1 class="small">
                    <?= _t('qa', 'Ваш вопрос будет опубликован после подтверждения аккаунта') ?>
                </h1>
                <?= $confirm ?>
            </div>
            <? else: ?>
            <div class="col-md-8 col-md-offset-2">
                <h1 class="small">
                    <? if(Qa::premoderation()) { ?>
                        <?= _t('qa', 'Ваш вопрос будет опубликован после проверки менеджером') ?>
                    <? } else { ?>
                        <?= _t('qa', 'Ваш вопрос успешно опубликован') ?>
                    <? } ?>
                </h1>
                <? if(Qa::premoderation()) { ?>
                    <p>
                        <?= _t('qa', 'Ваш вопрос сохранен, но еще не опубликован.<br> Вы можете увидеть и отредактировать его в <a [a_cabinet]>вашем кабинете</a>.<br />Как только наш менеджер проверит корректность введенных вами данных, вопрос появится в списке.',
                        array('a_cabinet' => 'href="'.Qa::url('my.list').'"')) ?>
                        <br />
                    </p>
                    <p><a href="<?= Qa::url('add') ?>"><?= _t('qa', 'Задать еще вопрос') ?></a></p>
                    <p><a href="<?= Qa::url('list') ?>"><?= _t('qa','Перейти на страницу списка вопросов') ?></a></p>
                <? } else { ?>
                    <p>
                        <?= _t('qa', 'Чтобы увидеть его в общем списке вопросов перейдите на <a [a_list]>список вопросов</a>.',
                            array('a_list' => 'href="'.Qa::url('list').'"')) ?><br />
                        <?= _t('qa', 'Вы также можете отредактировать его в <a [a_cabinet]>вашем кабинете</a>.',
                            array('a_cabinet' => 'href="'.Qa::url('my.list').'"')) ?>
                    </p>
                    <p><a href="<?= Qa::url('add') ?>"><?= _t('qa', 'Задать еще вопрос') ?></a></p>
                <? } ?>
            </div>
            <? endif; ?>
        </div>
    </section>
</div>