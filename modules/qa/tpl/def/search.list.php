<?php
/**
 * Поиск вопросов: список
 * @var $this Qa
 */
if( ! empty($list)):
    ?>
    <ul class="q-questionsList media-list">
        <? $urlList = Qa::url('list');
            foreach($list as $v):
                $urlView = Qa::url('view', array('id' => $v['id'], 'keyword' => $v['keyword'])); ?>
        <li class="media">
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                <?= tpl::userAvatar($v) ?>
            </a>
            <div class="media-body">
                <? if($v['solved']): ?>
                <div class="q-solvedLabel">
                    <span class="label label-success"><?= _t('qa', 'Вопрос решен'); ?></span>
                </div>
                <? else: if($v['status'] == Qa::STATUS_CLOSED): ?>
                    <div class="q-solvedLabel">
                        <span class="label label-closed"><?= _t('qa', 'Вопрос закрыт'); ?></span>
                    </div>
                <? endif; endif; ?>

                <div class="q-question-name">
                    <a href="<?= $urlView ?>"><?= $v['title'] ?></a>
                </div>
                <div class="clearfix"></div>
                <ul class="l-item-features">
                    <li><a href="<?= $urlList.(Specializations::catsOn() ? $v['cat_keyword'].'/' : '').$v['spec_keyword'] ?>"><i class="fa fa-th-list c-link-icon"></i><?= $v['spec_title'] ?></a></li>
                    <li><?= tpl::userLink($v, 'icon', 'qa'); ?></li>
                    <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($v['created'], true); ?></li>
                    <li><a href="<?= $urlView ?>#answers"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['answer_cnt'], _t('qa', 'ответ;ответа;ответов')) ?></span></a></li>
                </ul>
            </div>
        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('qa', 'Вопросы не найдены'); ?></div>
<? endif;
