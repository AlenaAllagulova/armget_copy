<?php
/**
 * Поиск вопросов: layout
 * @var $this Qa
 */
tpl::includeJS('qa.search', false, 2);
$nUserID = User::id();
?>
    <div class="container" id="j-qa-search">
        <section class="l-searchAdd">
            <div class="row">

                <div class="col-sm-3 add-button">
                    <a href="<?= Qa::url('add'); ?>" class="btn btn-primary btn-block"><i class="fa fa-question-circle hidden-sm"></i> <?= _t('qa', 'Задать вопрос'); ?></a>
                </div>

                <div class="col-sm-9 l-content-column">
                    <div class="l-search-bar">
                        <form role="search" method="get" action="<?= Qa::url('list') ?>">
                            <input type="search" name="q" class="form-control" placeholder="<?= _t('qa', 'Найти ответ'); ?>">
                            <button class="l-search-button"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>

            </div>
        </section>

        <section class="l-mainContent" id="j-qa-search-list">
            <div class="row">

                <aside class="col-md-3">
                    <?= $form ?>
                </aside>

                <div class="col-md-9 l-content-column">

                    <div class="l-menu-filter hidden-sm hidden-xs">
                        <button type="button" class="l-tabs-toggle btn btn-default" data-toggle="collapse" data-target="#menu-filter"><i class="fa fa-chevron-down pull-right"></i><span class="j-sl-title"><?= $solved[ $f['sl'] ]['t'] ?></span></button>
                        <ul class="nav nav-tabs collapse" id="menu-filter">
                            <? foreach($solved as $v): ?>
                                <li<?= $v['id'] == $f['sl'] ? ' class="active"' : '' ?>><a href="#" class="j-f-sl" data-id="<?= $v['id'] ?>"><?= $v['t'] ?></a></li>
                            <? endforeach; ?>
                        </ul>
                    </div>

                    <div class="l-list-heading">
                        <h6 class="visible-lg visible-md"><?= _t('qa', 'Найдено'); ?> <span class="j-qa-count"><?= $count ?></span></h6>

                        <div class="dropdown visible-sm visible-xs pull-left">
                            <a href="#" id="dLabel" class="dropdown-toggle ajax-link" data-toggle="dropdown"><span class="j-sl-title"><?= $solved[ $f['sl'] ]['t'] ?></span> <i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                                <? foreach($solved as $v): ?>
                                    <li<?= $v['id'] == $f['sl'] ? ' class="active"' : '' ?>><a href="#" class="j-f-sl" data-id="<?= $v['id'] ?>"><?= $v['t'] ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="j-list"><?= $list ?></div>
                    <div class="j-pagination"><?= $pgn ?></div>

                </div>
            </div>
        </section>
    </div>
<script type="text/javascript">
    <? js::start() ?>
    jQaSearch.init(<?= func::php2js(array(
        'lang'     => array(),
        'ajax'     => true,
        'rootSpec' => Specializations::ROOT_SPEC,
        'currSpec' => $spec_id,
        'currCat'  => $cat_id,
        'solved'   => $solved,
    )) ?>);
    <? js::stop() ?>
</script>