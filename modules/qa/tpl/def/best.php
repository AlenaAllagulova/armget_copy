<?php
if (!empty($best)): ?>
<h5 class="mrgb5"><?= _t('qa', 'Лидеры раздела'); ?></h5>
<ul class="l-leftCol-freelancers media-list">
    <? foreach ($best as $v): ?>
    <li class="media">
        <a href="<?= Users::url('profile', array('login' => $v['login'])) ?>" class="pull-left">
            <?= tpl::userAvatar($v) ?>
        </a>
        <div class="media-body">
            <?= tpl::userLink($v, 'no-login'); ?>
            <p>
                <?= tpl::declension($v['cnt'], _t('qa', 'ответ;ответа;ответов')) ?>
                <? if($v['best']) : ?>/ <?= tpl::declension($v['best'], _t('articles', 'лучший;лучших;лучших')) ?><? endif; ?>
            </p>
        </div>
    </li>
    <? endforeach; ?>
</ul>
<? endif;