<?php
/**
 * @var $this Qa
 *
 * Необходимые данные о Вопросе:
 * id, status, status_changed, moderated, blocked_reason
 *
 * Необходимыые данные пользователя:
 * user['blocked']
 */

$blocked = ($status == Qa::STATUS_BLOCKED);
$is_popup = ! empty($is_popup);
$only_form = ! empty($only_form);
?>
<? if ( ! $only_form) { ?>
<script type="text/javascript">
var jQaQuestionsStatus = (function(){
    var $block, url = '<?= $this->adminLink('questions_status&act=', 'qa'); ?>';
    var data = {id: <?= $id ?>, blocked: <?= ($blocked ? 1 : 0) ?>, is_popup: <?= ($is_popup ? 1 : 0) ?>};
    $(function(){
        $block = $('#j-i-status-block');
    });

    function _progress() {
        $block.toggleClass('disabled');
    }

    return {
        activate: function(user_id){
            if( ! bff.confirm('sure')) return;
            bff.ajax('<?= $this->adminLink('ajax&act=user-activate', 'users'); ?>', {id:user_id}, function(r){
                if(r && r.success) {
                    bff.success('<?= _t('', 'Операция выполнена успешно') ?>');
                }
                bff.ajax(url+'activate', data, function(resp){
                    if(resp && resp.success) {
                        $block.html(resp.html);
                    }
                }, _progress);
            }, _progress);
        },
        approve: function(){
            bff.ajax(url+'approve', data, function(resp){
                if(resp && resp.success) {
                    $block.html(resp.html);
                }
            }, _progress);
        },
        close: function(){
            if( ! bff.confirm('sure')) return;
            bff.ajax(url+'close', data, function(resp){
                if(resp && resp.success) {
                    $block.html(resp.html);
                }
            }, _progress);
        },
        open: function(){
            if( ! bff.confirm('sure')) return;
            bff.ajax(url+'open', data, function(resp){
                if(resp && resp.success) {
                    $block.html(resp.html);
                }
            }, _progress);
        },
        changeBlocked: function(step, block)
        {
            var $blocked_reason = $block.find('.j-i-blocked-reason');
            var $buttons = $block.find('.j-i-status-buttons');
            switch(step)
            {
                case 1: { // block/unblock, change reason
                    $block.find('#i_blocked').hide();
                    $block.find('#i_blocked_error, #i_blocking').show(0, function(){
                        $blocked_reason.focus();
                        $buttons.hide();
                    });
                } break;
                case 2: { // cancel
                    if(data.blocked == 1) {
                        $block.find('#i_blocking').hide();
                        $block.find('#i_blocked').show();
                    } else {
                        $block.find('#i_blocked_error').hide();
                    }
                    $buttons.show();
                } break;
                case 3: { // save
                    data.blocked_reason = $blocked_reason.val();
                    bff.ajax(url+'block', data, function(resp){
                        if(resp && resp.success) {
                            data.blocked = resp.blocked;
                            $block.html(resp.html);
                        }
                    }, _progress);
                } break;
                case 4: { // unblock
                    if (!bff.confirm('sure')) break;
                    data.unblock = 1;
                    jQaQuestionsStatus.changeBlocked(3);
                    data.unblock = 0;
                } break;
            }
            return false;
        }
    };
}());
</script>

<div class="<? if( ! $is_popup ) { ?>well well-small<? } ?>" id="j-i-status-block">
<? } ?>
    <table class="admtbl tbledit">
        <tr>
            <td class="row1 field-title<? if($is_popup) { ?> right<? } ?>" style="width:<?= ( $is_popup ? 133 : 88 ) ?>px;">Статус:</td>
            <td class="row2"><strong><?
                if( $user['blocked'] ) {
                    ?>Аккаунт пользователя был заблокирован<?
                } else {
                    switch($status) {
                        case Qa::STATUS_NOTACTIVATED: { echo 'Не активирован'; } break;
                        case Qa::STATUS_OPENED: { echo 'Открыт'; } break;
                        case Qa::STATUS_CLOSED: { echo 'Закрыт'; } break;
                        case Qa::STATUS_BLOCKED: { echo ($moderated == 0?'Ожидает проверки (был заблокирован)':'Заблокирован'); } break;
                    }
                }
                ?></strong><? if($status_changed != '0000-00-00 00:00:00'){ ?>&nbsp;&nbsp;<span class="desc">(<?= tpl::date_format2($status_changed, true, true); ?>)</span><? } ?>
            </td>
        </tr>
        <? if ( $status != Qa::STATUS_NOTACTIVATED && ! $user['blocked'] ) { ?>
            <tr>
                <td class="row1" colspan="2">
                    <div class="alert alert-danger <?= (!$blocked ? 'hidden':'') ?>" id="i_blocked_error">
                        <div><?= _t('', 'Причина блокировки:') ?><div class="right desc" id="i_blocked_reason_warn" style="display:none;"></div></div>
                        <div class="clear"></div>
                        <div id="i_blocked">
                            <span id="i_blocked_text"><?= (!empty($blocked_reason) ? tpl::blockedReason($blocked_reason) :'?') ?></span> - <a href="#" onclick="jQaQuestionsStatus.changeBlocked(1,0); return false;" class="ajax desc">изменить</a>
                        </div>
                        <div id="i_blocking" style="display: none;">
                            <textarea name="blocked_reason" class="autogrow j-i-blocked-reason" style="height:60px; min-height:60px;"><?= $blocked_reason; ?></textarea>
                            <a onclick="return jQaQuestionsStatus.changeBlocked(3, 1);" class="btn btn-mini btn-success" href="#"><?= (!$blocked ? _t('', 'continue'):_t('qa', 'изменить причину')) ?></a>
                            <? if($blocked) { ?><a onclick="return jQaQuestionsStatus.changeBlocked(4);" class="btn btn-success btn-mini" href="#"><?= _t('', 'unblock') ?></a><? } ?>
                            <a onclick="return jQaQuestionsStatus.changeBlocked(2);" class="btn btn-mini" href="#"><?= _t('', 'cancel') ?></a>
                        </div>
                    </div>
                </td>
            </tr>
           <? if( ! ($moderated && $blocked) ) { ?>
                <tr class="j-i-status-buttons">
                    <td class="row1" colspan="2" <? if( $is_popup ) { ?> style="padding-left: 90px;" <? } ?>>
                        <?
                        if ($moderated == 0) { ?>
                            <input class="btn btn-mini btn-success success button" type="button" onclick="jQaQuestionsStatus.approve();" value="<?= ($blocked ? 'проверено, все впорядке' : 'проверено') ?>" />
                        <? } else {
                            if ( $moderated == 2 ) {
                                ?><input class="btn btn-mini btn-success success button" type="button" onclick="jQaQuestionsStatus.approve();" value="<?= 'проверено' ?>" /> <?
                            }
                            if ($status ==  Qa::STATUS_CLOSED) {
                                ?><input class="btn btn-mini submit button" type="button" onclick="jQaQuestionsStatus.open();" value="открыть" /> <?
                            } else if ($status == Qa::STATUS_OPENED) {
                                ?><input class="btn btn-mini submit button" type="button" onclick="jQaQuestionsStatus.close();" value="закрыть" /> <?
                            }
                        }
                        if ( ! $blocked) { ?>
                            <a class="btn btn-mini text-error" onclick="jQaQuestionsStatus.changeBlocked(1); return false;" id="i_block_lnk"><?= _t('', 'block') ?></a>
                        <? } ?>
                    </td>
                </tr>
            <? }
        } # endif: ! $user['blocked']
        else if(intval($status) === Qa::STATUS_NOTACTIVATED) { ?>
            <tr>
                <td class="row1" colspan="2">
                    <input class="btn btn-mini submit button" type="button" onclick="jQaQuestionsStatus.activate(<?= $user_id ?>);" value="активировать" />
                </td>
            </tr>
        <? } ?>
    </table>
<? if( ! $only_form) { ?>
</div>
<? } ?>