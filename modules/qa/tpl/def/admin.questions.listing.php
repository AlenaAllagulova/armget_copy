<?php
    /**
     * @var $this Qa
     */
    tpl::includeJS(array('autocomplete','comments') , true);
?>
<?= tplAdmin::blockStart('Ответы / Добавление вопроса', false, array('id'=>'QaQuestionsFormBlock','style'=>'display:none;')); ?>
    <div id="QaQuestionsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart('Ответы / Вопросы', true, array('id'=>'QaQuestionsListBlock','class'=>(!empty($act) ? 'hidden' : '')),
        array('title'=>'+ добавить вопрос','class'=>'ajax','onclick'=>'return jQaQuestionsFormManager.action(\'add\',0);'),
        array(
            array('title'=>_t('', 'пересчет счетчиков'), 'onclick'=>"return bff.confirm('sure', {r:'".$this->adminLink(bff::$event.'&act=dev-qa-counters-update')."'})", 'icon'=>'icon-refresh', 'debug-only'=>true),
        )
    ); ?>
            <?
                $aTabs = array(
                    0 => array('t'=>'Открытые'),
                    1 => array('t'=>'Закрытые'),
                    5 => array('t'=>'Решенные'),
                    2 => array('t'=>'На модерации', 'counter'=>config::get('qa_questions_moderating', 0)),
                    3 => array('t'=>'Заблокированные'),
                    6 => array('t'=>'Неактивированные', 'c'=>' class="disabled" '),
                    4 => array('t'=>'Удаленные'),
                );
            ?>
            <div class="tabsBar" id="QaQuestionsListTabs">
                <? foreach($aTabs as $k=>$v) { ?>
                    <span class="tab <? if($f['tab']==$k) { ?>tab-active<? } ?>"><a href="#" onclick="return jQaQuestionsList.onTab(<?= $k ?>,this);" <?= (!empty($v['c']) ? $v['c'] : '') ?>><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
                <? } ?>
                <div id="QaQuestionsProgress" class="progress" style="display: none;"></div>
            </div>
            <div class="actionBar">
                <form method="get" action="<?= $this->adminLink(NULL) ?>" id="QaQuestionsListFilters" onsubmit="return false;" class="form-inline">
                <input type="hidden" name="s" value="<?= bff::$class ?>" />
                <input type="hidden" name="ev" value="<?= bff::$event ?>" />
                <input type="hidden" name="page" value="<?= $f['page'] ?>" />
                <input type="hidden" name="tab" value="<?= $f['tab'] ?>" />
                <div class="left">
                    <input style="width:175px;" type="text" maxlength="150" name="q" placeholder="ID / Название вопроса" value="<?= HTML::escape($f['q']) ?>" />
                </div>
                <div class="left" style="margin-left:4px;">
                    <input type="text" style="width:175px;" maxlength="150" name="user" placeholder="<?= _t('', 'ID / логин / E-mail пользователя') ?>" value="<?= HTML::escape($f['user']) ?>" />
                </div>
                <div class="left"  style="margin-left:4px;">
                    <input type="button" class="btn btn-small button cancel" onclick="jQaQuestionsList.submit(false);" value="<?= _t('', 'search') ?>" />
                    <a class="ajax cancel" onclick="jQaQuestionsList.submit(true); return false;"><?= _t('', 'reset') ?></a>
                </div>
                <div class="right">
                    
                </div>
                <div class="clear"></div>
                </form>
            </div>

            <table class="table table-condensed table-hover admtbl tblhover" id="QaQuestionsListTable">
                <thead>
                    <tr class="header nodrag nodrop">
                        <th width="70">ID</th>
                        <th class="left">Вопрос</th>
                        <th width="110"><?= _t('', 'Created') ?></th>
                        <th width="135"><?= _t('', 'Action') ?></th>
                    </tr>
                </thead>
                <tbody id="QaQuestionsList">
                    <?= $list ?>
                </tbody>
            </table>
            <div id="QaQuestionsListPgn"><?= $pgn ?></div>
            
<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">
        
    </div>
</div>

<script type="text/javascript">
var jQaQuestionsFormManager = (function(){
    var $progress, $block, $blockCaption, $formContainer, process = false;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $formContainer = $('#QaQuestionsFormContainer');
        $progress = $('#QaQuestionsProgress');
        $block = $('#QaQuestionsFormBlock');
        $blockCaption = $block.find('span.caption');

        <? if( ! empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jQaQuestionsList.toggle(false);
            if(jQaQuestionsForm) jQaQuestionsForm.onShow();
        } else {
            jQaQuestionsList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? 'Ответы / Добавление вопроса' : 'Ответы / Редактирование вопроса'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
            } else {
                jQaQuestionsList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        params = $.extend(params || {}, {act:type});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jQaQuestionsList =
(function()
{
    var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, tab = <?= $f['tab'] ?>;
    var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';
    
    $(function(){
        $progress  = $('#QaQuestionsProgress');
        $block     = $('#QaQuestionsListBlock');
        $list      = $block.find('#QaQuestionsList');
        $listTable = $block.find('#QaQuestionsListTable');
        $listPgn   = $block.find('#QaQuestionsListPgn');
        filters    = $block.find('#QaQuestionsListFilters').get(0);

        $list.delegate('a.question-edit', 'click', function(){
            var id = intval($(this).data('id'));
            if(id>0) jQaQuestionsFormManager.action('edit',id);
            return false;
        });

        $list.delegate('a.question-toggle', 'click', function(){
            var id = intval($(this).data('id'));
            var type = $(this).data('type');
            if(id>0) {
                var params = {progress: $progress, link: this};
                bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
            }
            return false;
        });

        $(window).bind('popstate',function(){
            if('state' in window.history && window.history.state === null) return;
            var loc = document.location;
            var actForm = /act=(add|edit)/.exec( loc.search.toString() );
            if( actForm!=null ) {
                var actId = /id=([\d]+)/.exec(loc.search.toString());
                jQaQuestionsFormManager.action(actForm[1], actId && actId[1]);
            } else {
                jQaQuestionsFormManager.action('cancel');
                updateList(false);
            }
        });

    });

    function isProcessing()
    {
        return processing;
    }

    function updateList(updateUrl)
    {
        if(isProcessing()) return;
        var f = $(filters).serialize();
        bff.ajax(ajaxUrl, f, function(data){
            if(data) {
                $list.html( data.list );
                $listPgn.html( data.pgn );
                if(updateUrl !== false && bff.h) {
                    window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                }
            }
        }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
    }

    function setPage(id)
    {
        filters.page.value = intval(id);
    }

    return {
        submit: function(resetForm)
        {
            if(isProcessing()) return false;
            setPage(1);
            if(resetForm) {
                filters['q'].value = '';
                filters['user'].value = '';
                //
            }
            updateList();
        },
        page: function (id)
        {
            if(isProcessing()) return false;
            setPage(id);
            updateList();
        },
        onTab: function(tabNew, link)
        {
            if(isProcessing() || tabNew == tab) return false;
            setPage(1);
            tab = filters.tab.value = tabNew;
            updateList();
            $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
            return false;
        },
        refresh: function(resetPage, updateUrl)
        {
            if(resetPage) setPage(0);
            updateList(updateUrl);
        },
        toggle: function(show)
        {
            if(show === true) {
                $block.show();
                if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
            }
            else $block.hide();
        }
    };
}());
</script>