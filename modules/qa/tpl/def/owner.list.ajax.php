<?php
/**
 * @var $this Qa
 */
$bOwner = User::isCurrent($id);
if( ! empty($list)): ?>
    <ul class="q-questionsList media-list">
    <?  $urlList = Qa::url('list');
        foreach($list as $v):
            $urlView = Qa::url('view', array('id' => $v['id'], 'keyword' => $v['keyword']));
            $bDisabled = $v['status'] == Qa::STATUS_BLOCKED || ( Qa::premoderation() && $v['moderated'] != 1);
            ?>
        <li class="media<?= $bDisabled ? ' disabled' : '' ?>">
            <div class="media-body">

                <? if($v['solved']): ?>
                    <div class="q-solvedLabel">
                        <span class="label label-success"><?= _t('qa', 'Вопрос решен'); ?></span>
                    </div>
                <? else: if($v['status'] == Qa::STATUS_CLOSED): ?>
                    <div class="q-solvedLabel">
                        <span class="label label-closed"><?= _t('qa', 'Вопрос закрыт'); ?></span>
                    </div>
                <? endif; endif; ?>
                <div class="q-question-name">
                    <a href="<?= $urlView ?>"><?= $v['title'] ?></a>
                </div>
                <div class="clearfix"></div>

                <? if($v['answer_user_id']): ?>
                    <article class="q-question-myanswer">
                        <strong><?= $bOwner ? _t('qa', 'Мой ответ:') : _t('qa', 'Ответ:'); ?></strong> <?= $v['answer'] ?>
                    </article>
                <? endif; ?>

                <? if($v['user_id'] == $id && $bOwner): ?>
                <div class="p-profileOrder-controls-mobile">
                    <a href="<?= Qa::url('edit', array('id' => $v['id'])) ?>"><i class="fa fa-edit c-link-icon"></i></a>
                    <a href="#" class="link-delete j-delete" data-id="<?= $v['id'] ?>"><i class="fa fa-times c-link-icon"></i></a>
                </div>

                <div class="p-profileOrder-controls sh-item-controls">
                    <a href="<?= Qa::url('edit', array('id' => $v['id'])) ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                    <a href="#" class="link-delete j-delete" data-id="<?= $v['id'] ?>"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
                </div>
                <? endif; ?>

                <ul class="l-item-features">
                    <li><a href="<?= $urlList.(Specializations::catsOn() ? $v['cat_keyword'].'/' : '').$v['spec_keyword'] ?>"><i class="fa fa-th-list c-link-icon"></i><?= $v['spec_title'] ?></a></li>
                    <? if($v['user_id'] != $id): ?><li><?= tpl::userLink($v, 'icon', 'qa'); ?></li><? endif; ?>
                    <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($v['created'], true); ?></li>
                    <li><a href="<?= $urlView ?>#answers"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['answer_cnt'], _t('qa', 'ответ;ответа;ответов')) ?></span></a></li>
                </ul>
                <?= $this->questionStatusBlock($v); ?>
            </div>
        </li>
    <? endforeach;
    else: ?>
        <div class="alert alert-info"><?= _t('qa', 'Вопросы не найдены'); ?></div>
<? endif;