<?php
/**
 * @var $this Qa
 */
$aTabs = array(
    0 => array('t' => _t('orders', 'На модерации'), 'counter'=>config::get('qa_answers_moderating', 0)),
    1 => array('t' => _t('orders', 'Все')),
);
?>
    <form action="" name="filter" class="form-inline" id="j-qa-answers-filter">
        <div class="tabsBar" id="j-qa-answers-tabs">
            <?php foreach($aTabs as $k=>$v) { ?>
                <span class="tab<?= $k== $f['tab'] ? ' tab-active' : '' ?>"><a href="#" class="j-tab" data-id="<?= $k ?>"><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
            <?php } ?>
            <label class="pull-right"><?= _t('', 'по') ?><select name="perpage" class="j-perpage" style="width: 45px; height: 20px !important; border:0;"><?= $perpage ?></select></label>
        </div>

        <div class="actionBar">
            <input type="hidden" name="s" value="<?= bff::$class ?>" />
            <input type="hidden" name="ev" value="<?= bff::$event ?>" />
            <input type="hidden" name="tab" value="<?= $f['tab'] ?>" class="j-tab-id" />
            <input type="hidden" name="page" value="<?= $f['page'] ?>"/>

            <div class="controls controls-row">
                <input type="text" name="user" value="<?= $f['user'] ?>"  placeholder="ID / логин / e-mail" style="width: 200px;" />
                <label class="checkbox inline" style="margin-right: 10px;"><input type="checkbox" name="best" value="1" class="j-change" <?= $f['best'] ? 'checked="checked"' : '' ?>/> лучшие</label>
                <input type="submit" value="<?= _t('', 'search') ?>" class="btn btn-small button submit" />
                <a class="cancel" id="j-filter-cancel"><?= _t('', 'reset') ?></a>
                <div class="clearfix"></div>
            </div>
        </div>
    </form>

<?= $this->viewPHP($aData, 'admin.answers.listing.list'); ?>

<?= $pgn; ?>

<script type="text/javascript">
    $(function(){
        var $filter = $('#j-qa-answers-filter');

        $('#j-filter-cancel').on('click', function(e){ nothing(e);
            var filter = $filter.get(0);
            filter.elements.page.value = 1;
            filter.elements.user.value = '';
            $(filter.elements.best).prop('checked', false);
            filter.submit();
        });

        $('.j-change').change(function(){
            $filter.get(0).elements.page.value = 1;
            $filter.submit();
        });
    });
</script>
