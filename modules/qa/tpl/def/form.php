<?php
/**
 * Форма добавления / редактирования вопроса
 * @var $this Qa
 */
tpl::includeJS('qa.question.form', false, 2);
$aData = HTML::escape($aData, 'html', array('title','descr'));

if($edit){
    $formTitle = _t('qa', 'Редактировать вопрос');
    $url = Qa::url('edit', array('id'=>$id));
} else {
    $formTitle = _t('qa', 'Задать вопрос');
    $url = Qa::url('add');
}
$modified = time();
$userID = User::id();
?>
        <div class="container">

            <section class="l-mainContent">
                <div class="row">

                    <div class="col-md-8 col-md-offset-2">

                        <?= tpl::getBreadcrumbs(array(
                            array('title' => _t('qa','Ответы'),'link'=>Qa::url('list')),
                            array('title' => $formTitle,'active'=>true),
                        )); ?>

						<div class="p-profileContent">

                        	<div class="p-profile-title">

                                <h4><?= $formTitle ?></h4>

                            </div>


                            <form class="form-horizontal" id="j-question-form" method="post" action="">

                                <div class="form-group j-required">
                                    <label for="question" class="col-sm-3 control-label"><?= _t('qa', 'Вопрос'); ?> <i class="text-danger">*</i></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="title" class="form-control" id="question" placeholder="<?= _t('qa', 'Краткое описание вопроса.'); ?>" value="<?= $title ?>" autocomplete="off" />
                                  </div>
                                </div>

                                <!-- Detailed question -->
                                <div class="form-group j-required">
                                    <label for="detailed" class="col-sm-3 control-label"><?= _t('qa', 'Уточнение'); ?> <i class="text-danger">*</i></label>
                                    <div class="col-sm-9">
                                        <textarea rows="7" name="description" class="form-control" id="detailed" placeholder="<?= _t('qa', 'Опишите подробнее вопрос на интересующую Вас тему.'); ?>"><?= $description ?></textarea>
                                  </div>
                                </div>

                                <div class="form-group j-specs">
                                    <label for="inputSpec" class="col-sm-3 control-label o-control-label"><?= _t('', 'Категория'); ?> <i class="text-danger">*</i></label>
                                    <div class="col-sm-9">
                                        <?= Specializations::i()->specSelect(1, $spec, array('inputName' => 'specs', 'emptyTitle' => _t('qa', 'Выбрать категорию'))); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="takeanws" class="col-sm-3 control-label"><?= _t('qa', 'Прием ответов'); ?></label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="takeanws" name="term"><?= Qa::aTermsOptions($term, $days) ?></select>
                                        <div class="help-block mrgb0"><?= _t('qa', 'до'); ?> <span id="j-term-title"><?= tpl::date_format2($modified + $days * 24*60*60, false, true) ?></div>
                                    </div>
                                </div>
                                
                                <? if( ! $userID): ?><?= Users::i()->register($this->userRegisterEmbedded + array('form' => '#j-question-form')) ?><? endif; ?>

                                <!-- Publicate button -->
                                <div class="form-group mrgt30">
                                    <div class="col-sm-9 col-sm-offset-3 c-formSubmit">
                                        <button class="btn btn-primary c-formSuccess j-submit"><?= $edit ? _t('form', 'Сохранить изменения') : _t('qa', 'Опубликовать вопрос'); ?></button>
                                        <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                                    </div>
                                </div>


                            </form>


                        </div><!-- /.p-profileContent -->


                    </div><!-- /.col-md-8 -->

                </div><!-- /.row -->
            </section><!-- /.main-content -->

        </div><!-- /.container -->
<script type="text/javascript">
    <? js::start(); $aTerms = Qa::aTerms(); $aTextTerms = array();
        foreach($aTerms as $v){
            $aTextTerms[ $v['id'] ] = tpl::date_format2($modified + $v['days'] * 24*60*60, false, true);
        }
     ?>
    jQaQuestionForm.init(<?= func::php2js(array(
        'lang' => array(
            'saved_success' => _t('qa', 'Вопрос успешно сохранен'),
            'spec_wrong'    => _t('qa', 'Укажите категорию'),
        ),
        'url'    => $url,
        'itemID' => $id,
        'terms'  => $aTextTerms,
        'checkUser' => ! $userID,
    )) ?>);
    <? js::stop() ?>
</script>