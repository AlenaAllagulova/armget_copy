<?php
if( ! isset($aAnswerStatus)){
    $aAnswerStatus = Qa::aAnswerStatus();
}
?>
<a name="j-answer-<?= $v['id'] ?>" ></a>
<div class="ccontent">
    <div class="tb"><div class="tl"><div class="tr"></div></div></div>
    <div class="ctext">
        <div class="j-answer-view"><?= nl2br($v['description']) ?></div>
        <? if($v['status'] == Qa::STATUS_ANS_BLOCK_MODERATOR || ! empty($showBlockForm)){ echo $this->viewPHP($aData, 'admin.answer.listing.list.block'); } ?>
    </div>
    <div class="bl"><div class="bb"><div class="br"></div></div></div>
</div>
<div class="info" style="margin:0;">
    <ul>
        <li><p><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink author<? if($v['ublocked']){ ?> blocked<? } ?>"><?= ( ! empty($v['name']) ? $v['name'] : $v['login'] ) ?> (<?= $v['email'] ?>)</a></p></li>
        <li class="date"><?= tpl::date_format2($v['created'], true); ?></li>
        <? if($v['status'] == Qa::STATUS_ANS_DEFAULT && $v['moderated'] != 1){ ?><li><a href="#" class="ajax text-success" onclick="jQaAnswers.approve(<?= $v['id'] ?>, this); return false;"><?= _t('', 'Approve') ?></a></li><? } ?>
        <? if($v['status'] == Qa::STATUS_ANS_DEFAULT || $v['status'] == Qa::STATUS_ANS_BLOCK_MODERATOR): ?>
            <li><a href="#" class="ajax text-success" onclick="jQaAnswers.edit(<?= $v['id'] ?>, this); return false;"><?= _t('', 'Edit') ?></a></li>
            <li><a href="#" class="text-error delete ajax" onclick="jQaAnswers.del(<?= $v['id'] ?>); return false;"><?= _t('', 'Delete') ?></a></li>
        <? endif; ?>
        <? if($v['status'] == Qa::STATUS_ANS_DEFAULT): ?>
            <li><a href="#" class="text-error delete ajax" onclick="jQaAnswers.block(<?= $v['id'] ?>); return false;"><?= _t('', 'Block') ?></a></li>
        <? endif; ?>
        <? if($v['comments_cnt']): ?><li><a href="#" class="ajax text-success" onclick="jQaAnswers.comments(<?= $v['id'] ?>, this); return false;"><?= _t('', 'Comments') ?></a></li><?endif; ?>
        <? if($bCommonListing){ ?><li><a href="<?= $urlEdit.$v['question_id'] ?>"><?= _t('qa', 'Вопрос #[id]', array('id'=>$v['question_id'])) ?></a></li><? } ?>
        <? if($v['status'] != Qa::STATUS_ANS_DEFAULT): ?><li class="date"><?= $aAnswerStatus[ $v['status'] ]['t'] ?></li><? endif; ?>
    </ul>
</div>
