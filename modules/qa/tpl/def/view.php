<?php
/**
 * Просмотр вопроса
 * @var $this Qa
 */
tpl::includeJS('qa.view', false);
$isOwner = User::isCurrent($user_id);
$isClosed = ($status == Qa::STATUS_CLOSED);
$isOpened = ($status == Qa::STATUS_OPENED);
$urlEdit = Qa::url('edit', array('id' => $id));

?>
    <div class="container">

        <?= tpl::getBreadcrumbs($breadcrumbs); ?>

        <section class="l-mainContent" id="j-qa-question-view">
            <div class="row">

            <div class="col-md-9">

                <div class="l-borderedBlock">
                    <div class="l-inside">
                        <header class="title-type-2">
                            <div class="row">
                                <div class="col-sm-10">
                                    <h1><?= $title ?></h1>
                                </div>
                                <div class="col-sm-2 q-question-solved j-question-status">
                                <? if($solved): ?>
                                    <span class="label label-success"><?= _t('qa', 'Вопрос решен'); ?></span>
                                <? else: if($isClosed): ?>
                                    <span class="label label-closed"><?= _t('qa', 'Вопрос закрыт'); ?></span>
                                <? endif; endif; ?>
                                </div>
                            </div>
                        </header>

                        <article>
                            <?= nl2br($description); ?>
                        </article>

                    </div>

                    <? if($isOwner): ?>
                    <div class="l-inside p-profileOrder-controls-mobile">
                        <a href="#" class="link-add <?= $solved ? ' hidden ' : '' ?>j-solved"><i class="fa fa-thumbs-up c-link-icon"></i></a>
                        <a href="#" class="j-close<?= $isClosed ? ' hidden' : '' ?>"><i class="fa fa-lock c-link-icon"></i></a>
                        <a href="#" class="j-open<?= $isOpened ? ' hidden' : '' ?>"><i class="fa fa-unlock c-link-icon"></i></a>
                        <a href="<?= $urlEdit ?>"><i class="fa fa-edit c-link-icon"></i></a>
                        <a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i></a>
                    </div>

                    <div class="l-inside p-profileOrder-controls">
                        <a href="#" class="link-add <?= $solved ? ' hidden ' : '' ?>j-solved"><i class="fa fa-thumbs-up c-link-icon"></i><?= _t('qa', 'Вопрос решен'); ?></a>
                        <a href="#" class="j-close<?= $isClosed ? ' hidden' : '' ?>"><i class="fa fa-lock c-link-icon"></i><?= _t('qa', 'Закрыть'); ?></a>
                        <a href="#" class="j-open<?= $isOpened ? ' hidden' : '' ?>"><i class="fa fa-unlock c-link-icon"></i><?= _t('qa', 'Открыть'); ?></a>
                        <a href="<?= $urlEdit ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                        <a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
                    </div>
                    <? endif; ?>

                    <div class="l-inside">
                        <ul class="l-item-features">
                            <li><a href="<?= Qa::url('list').(Specializations::catsOn() ? $cat_keyword.'/' : '').$spec_keyword; ?>"><i class="fa fa-th-list c-link-icon"></i><?= $spec_title ?></a></li>
                            <li><?= tpl::userLink($aData, 'icon', 'qa'); ?></li>
                            <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($created, true); ?></li>
                            <li><span><i class="fa fa-eye"></i> <?= _t('', 'Просмотров: [total]', array('total'=>$views_total)); ?></span></li>
                        </ul>
                    </div>

                </div>

                <?  $aConfig = $this->configLoad();
                if( ! empty($aConfig['share_code'])): ?>
                    <?= $aConfig['share_code'] ?>
                <?  endif; ?>

                <?= $isOwner ? $this->questionStatusBlock($aData) : '' ?>

                <div id="j-answers-block">
                    <a name="answers"></a>
                    <?= $answers; ?>
                </div>

            </div>

            <div class="col-md-3 visible-lg visible-md">
                <? # Баннер: Ответы: просмотр ?>
                <?= Banners::view('qa_view', array('pos'=>'right', 'spec'=>$spec_id)) ?>
            </div>

            </div>
        </section>

    </div>

<script type="text/javascript">
    <? js::start() ?>
    jQaView.init(<?= func::php2js(array(
        'lang'   => array(
            'delete' => _t('qa', 'Удалить вопрос?'),
            'answer_delete' => _t('qa', 'Удалить ответ?'),
        ),
        'status' => array(
            'solve' => '<span class="label label-success">'._t('qa', 'Вопрос решен').'</span>',
            'closed'  => '<span class="label label-closed">'._t('qa', 'Вопрос закрыт').'</span>',
        ),
        'delete_answer' => '<div class="alert alert-deleted">'._t('qa', 'Вы удалили этот ответ.').'</div>',
        'owner' => $isOwner,
        'id'    => $id,
    )) ?>);
    <? js::stop() ?>
</script>