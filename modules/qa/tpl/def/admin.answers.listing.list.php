<?php
/**
 * @var $this Qa
 */
tpl::includeJS('comments', true);
$aData['bCommonListing'] = $bCommonListing = (bff::$event == 'answers');
if($bCommonListing){
    $aData['urlEdit'] = $urlEdit = $this->adminLink('questions&act=edit&id=');
}
$aAnswerStatus = Qa::aAnswerStatus();
?>
<div class="comments" id="j-answers">
    <? foreach($answers as $v): $aData['v'] = $v; ?>
        <div class="comment j-answer" id="j-answer-<?= $v['id'] ?>" data-id="<?= $v['id'] ?>">
            <?= $this->viewPHP($aData, 'admin.answer.listing.list.view'); ?>
        </div>
    <? endforeach;
    if(empty($answers)): ?>
        <div class="alignCenter valignMiddle" style="height:30px; padding-top:15px;">
            <span class="desc"><?= _t('qa', 'нет ответов') ?></span>
        </div>
    <? endif; ?>
</div>

<script type="text/javascript">
    var jQaAnswers = (function() {
        var $filter, processing = false;
        var ajax_url = '<?= $this->adminLink('answers&act='); ?>';
        var $answers;

        $(function(){
            $filter = $('#j-qa-answers-filter');

            if($filter && $filter.length) {
                $filter.find('.j-perpage').on('change', function(){
                    $filter.submit();
                });
                $('#j-qa-answers-tabs').on('click', '.j-tab', function(e){ nothing(e);
                    $filter.find('.j-tab-id').val( $(this).data('id') );
                    $filter.submit();
                });
            }
        });

        return {
            del: function(id)
            {
                if( ! bff.confirm('sure')) return;
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'delete', {id: id}, function(data) {
                    if(data) {
                        $('#j-answer-'+id).html(data.html);
                    }
                    processing = false;
                });
            },
            approve: function(id, link)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'approve', {id: id}, function(data) {
                    if(data) {
                        $(link).remove();
                    }
                    processing = false;
                });
            },
            comments: function(id, link)
            {
                var $l = $(link);
                if( ! $l.hasClass('c')){
                    $l.addClass('c');
                    bff.ajax(ajax_url+'comments', {id: id}, function(data) {
                        if(data && data.html) {
                            $l.closest('.info').after('<div class="well well-small" style="margin: 0 10px 20px 40px;">'+data.html+'</div>');
                        }
                    });
                }
            },
            block:function(id)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'block-form', {id: id}, function(data) {
                    if(data) {
                        $('#j-answer-'+id).html(data.html).find('textarea').focus();
                    }
                    processing = false;
                });
            },
            edit:function(id, link)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'form', {id: id}, function(data) {
                    if(data) {
                        $('#j-answer-'+id).html(data.html).find('textarea').focus();
                    }
                    processing = false;
                });
            },
            save:function(id, link)
            {
                if(processing) return;
                processing = true;
                var $el = $(link);
                var $f = $el.closest('form');
                bff.ajax(ajax_url+'save', $f.serialize(), function(data) {
                    if(data) {
                        $('#j-answer-'+id).html(data.html);
                    }
                    processing = false;
                });
            },
            cancel:function(id)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'cancel', {id: id}, function(data) {
                    if(data) {
                        $('#j-answer-'+id).html(data.html);
                    }
                    processing = false;
                });

            },
            block_save:function(id, link)
            {
                if(processing) return;
                processing = true;
                var $f = $(link).closest('.j-block-form');

                bff.ajax(ajax_url+'block', {id: id, reason:$f.find('.j-reason').val()}, function(data) {
                    if(data) {
                        $('#j-answer-'+id).html(data.html);
                    }
                    processing = false;
                });
            },
            unblock:function(id)
            {
                if(processing) return;
                processing = true;
                bff.ajax(ajax_url+'unblock', {id: id}, function(data) {
                    if(data) {
                        $('#j-answer-'+id).html(data.html);
                    }
                    processing = false;
                });
            }
        };
    }());
</script>