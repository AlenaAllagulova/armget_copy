<?php
/** @var $this Qa */
    $nUserID = User::id();
    if($this->isAllowAddAnswer($id, $nUserID, $bHidden)): ?>
    <div class="l-borderedBlock no-color<?= $bHidden ? ' hidden' : '' ?>" id="j-add-answer-block">
        <header class="l-inside">
            <a href="#j-add-answer" class="btn btn-link ajax-link o-btn-propose" data-toggle="collapse" data-parent="#accordion"><span><?= _t('qa', 'Опубликовать свой ответ'); ?></span></a>
        </header>
        <div class="collapse" id="j-add-answer">
            <form class="form" role="form" method="post" action="">
                <input type="hidden" name="question_id" value="<?= $id ?>" />
                <div class="l-inside">
                    <div class="form-group j-required">
                        <label for="j-description" class="control-label"><?= _t('qa', 'Ваш ответ:'); ?></label>
                        <textarea rows="5" name="description" id="j-description" class="form-control"></textarea>
                    </div>
                    <button class="btn btn-primary mrgr10 j-submit"><i class="fa fa-check"></i> <?= _t('qa', 'Опубликовать'); ?></button>
                    <a href="#addcomment" class="ajax-link" data-toggle="collapse" data-parent="#accordion"><span><?= _t('form', 'Отмена'); ?></span></a>
                </div>
            </form>
        </div>
    </div>
<? endif; ?>

    <header class="title">
        <h4><i class="fa fa-comments-o"></i> <?= _t('qa', 'Ответы'); ?> <span class="o-count-proposals"><?= count($answers) ?></span></h4>
    </header>


<? if( ! empty($answers)): ?>
    <ul class="o-freelancersList media-list">

        <? foreach($answers as $v):
           $bAllowVoting = ! (! $nUserID || $nUserID == $v['user_id'] || $v['vote']);
        ?>
        <li class="media<?= $v['best'] ? ' q-best-answer' : ''?>">
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="o-freelancer-avatar">
                <?= tpl::userAvatar($v) ?>
            </a>
            <div class="media-body">
                <div class="o-freelancer-info">
                    <div>
                        <strong><?= tpl::userLink($v); ?></strong>
                        <? if($bQuestionOwner && $v['status'] == Qa::STATUS_ANS_DEFAULT): ?>
                        <div class="o-freelancer-buttons">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm j-best<?= $v['best'] ? ' active' : '' ?>" data-id="<?= $v['id'] ?>"><i class="fa fa-check hidden-xs"></i> <?= _t('qa', 'Лучший ответ'); ?></button>
                                <button type="button" class="btn btn-default btn-sm j-delete-answer" data-id="<?= $v['id'] ?>"><i class="fa fa-times hidden-xs"></i> <?= _t('form', 'Удалить'); ?></button>
                            </div>
                        </div>
                        <? else:
                            if($nUserID == $v['user_id'] && $nQuestionStatus == Qa::STATUS_OPENED && $v['status'] == Qa::STATUS_ANS_DEFAULT && ! $v['best']): ?>
                            <div class="o-freelancer-buttons">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm j-delete-answer-owner" data-id="<?= $v['id'] ?>"><i class="fa fa-times hidden-xs"></i> <?= _t('form', 'Удалить'); ?></button>
                                </div>
                            </div>
                            <? endif;
                            if($v['best']): ?>
                            <div class="q-best-answer-note">
                                <i class="fa fa-check"></i> <?= _t('qa', 'Лучший ответ'); ?>
                            </div>
                            <? endif;
                        endif; ?>
                    </div>

                    <span><?= tpl::date_format3($v['created'])?></span>
                </div>

                <? if( $v['status'] == Qa::STATUS_ANS_DEFAULT): ?>
                <div class="p-infoText j-answer-text">
                    <?= nl2br($v['description']); ?>

                    <div class="q-answer-comments">
                        <? if($nUserID): ?>
                        <div class="pull-left">
                            <a href="#o-subcomments-<?= $v['id'] ?>" class="ajax-link j-comment-block" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-comment c-link-icon"></i><span><?= _t('', 'Комментарии'); ?></span> [<?= $v['comments_cnt'] ?>]</a>
                        </div>
                        <? endif; ?>
                        <div class="q-answer-rating pull-right j-vote-block<?= ! $bAllowVoting ? ' q-answer-rating_na' : '' ?>">
                            <a href="#"><i class="fa fa-thumbs-up q-answer-good j-vote" data-id="<?= $v['id'] ?>" data-t="good"></i></a>
                            <span class="q-answer-<?= $v['votes'] < 0 ? 'bad' : 'good' ?> j-votes"><?= $v['votes'] ?></span>
                            <a href="#"><i class="fa fa-thumbs-down q-answer-bad j-vote" data-id="<?= $v['id'] ?>" data-t="bad"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="collapse" id="o-subcomments-<?= $v['id'] ?>">
                        <div class="q-answers-list">
                            <ul>
                            <? if( ! empty($comments[ $v['id'] ])): 
                                foreach($comments[ $v['id'] ] as $vv):
                                    ?><?= $this->viewPHP($vv, 'view.answers.comment.ajax'); ?><?
                                endforeach;
                            endif; ?>
                            </ul>

                            <? if($nUserID): ?>
                            <form role="form" method="post" action="" class="j-comment-form">
                                <input type="hidden" name="id" value="<?= $v['id'] ?>" />
                                <div class="form-group j-required">
                                   <textarea name="message" class="form-control" rows="3"></textarea>
                                </div>
                                <button class="btn btn-primary btn-sm j-submit"><?= _t('form', 'Комментировать'); ?></button>
                            </form>
                            <? endif; ?>

                        </div>
                    </div>
                </div>
                <? else: $aAnswerStatus = Qa::aAnswerStatus($bQuestionOwner, $nUserID == $v['user_id']); ?>
                    <div class="alert alert-deleted"><?= $aAnswerStatus[ $v['status'] ]['t']; ?></div>
                    <? if( $v['status'] == Qa::STATUS_ANS_BLOCK_MODERATOR && $nUserID == $v['user_id'] && $v['blocked_reason']): ?>
                        <div role="alert" class="alert alert-danger">
                            <b><?= _t('Qa', 'Причина блокировки:')?></b>
                            <br><?= nl2br($v['blocked_reason']) ?>
                        </div>
                    <? endif; ?>
                <? endif; ?>
            </div>
        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info mrgt20">
        <?= _t('qa', 'Ответов пока нет'); ?>
    </div>
<? endif;