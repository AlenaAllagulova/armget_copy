<?php
$bBlocked = $v['status'] == Qa::STATUS_ANS_BLOCK_MODERATOR;
?>
<div class="alert alert-danger j-block-form"<? if(!empty($noChange)){?> style="margin-top: 5px;" <? } ?>>
    <div><?= _t('','Причина блокировки:') ?></div>
    <div class="clear"></div>
    <? if($bBlocked && empty($showBlockForm)): ?>
    <div>
        <span><?= nl2br($v['blocked_reason']) ?></span><? if(empty($noChange)){ ?> - <a class="ajax desc j-change" onclick="jQaAnswers.block(<?= $v['id'] ?>); return false;" href="#"><?= _t('','change')?></a><? } ?>
    </div>
    <? else: ?>
    <div>
        <textarea class="autogrow j-reason" style="height:50px; min-height:50px;"><?= $v['blocked_reason'] ?></textarea>
        <a onclick="jQaAnswers.block_save(<?= $v['id'] ?>, this); return false;" class="btn btn-mini btn-success" href="#"><?= _t('','continue') ?></a>
        <a onclick="jQaAnswers.unblock(<?= $v['id'] ?>); return false;" class="btn btn-mini btn-success j-unblock" href="#"<?= ! $bBlocked ? 'style="display: none;"' : ''?>><?= _t('','unblock') ?></a>
        <a onclick="jQaAnswers.cancel(<?= $v['id'] ?>); return false;" class="btn btn-mini" href="#"><?= _t('','cancel') ?></a>
    </div>
    <? endif; ?>
</div>
