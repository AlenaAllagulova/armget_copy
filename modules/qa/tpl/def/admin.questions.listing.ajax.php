<?php
    /**
     * @var $this Qa
     */
    foreach ($list as $k=>&$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?><?= $v['status'] == Qa::STATUS_NOTACTIVATED ? ' disabled' : '' ?>">
        <td class="small"><?= $id ?></td>
        <td class="left">
            <a href="<?= Qa::url('view', array('id'=>$id, 'keyword'=>$v['keyword'])) ?>" class="but linkout" target="_blank"></a>
            <?= $v['title'] ?>
        </td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a class="but edit question-edit" title="<?= _t('', 'Edit') ?>" href="#" data-id="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="4">
            ничего не найдено
        </td>
    </tr>
<? endif;