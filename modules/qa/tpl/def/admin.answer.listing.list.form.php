<?php
if( ! isset($aAnswerStatus)){
    $aAnswerStatus = Qa::aAnswerStatus();
}
?>
<form method="post" action="">
    <input type="hidden" name="id" value="<?= $v['id'] ?>" />
    <a name="j-answer-<?= $v['id'] ?>" ></a>
    <div class="ccontent self">
        <div class="tb"><div class="tl"><div class="tr"></div></div></div>
        <div class="ctext">
            <textarea name="description" rows="6"><?= $v['description'] ?></textarea>
        </div>
        <div class="bl"><div class="bb"><div class="br"></div></div></div>
    </div>
    <div class="info" style="margin:0;">
        <ul>
            <li><p><a href="#" onclick="return bff.userinfo(<?= $v['user_id'] ?>);" class="userlink author<? if($v['ublocked']){ ?> blocked<? } ?>"><?= ( ! empty($v['name']) ? $v['name'] : $v['login'] ) ?> (<?= $v['email'] ?>)</a></p></li>
            <li class="date"><?= tpl::date_format2($v['created'], true); ?></li>
            <li>
                <? if($v['status'] == Qa::STATUS_ANS_DEFAULT || $v['status'] == Qa::STATUS_ANS_BLOCK_MODERATOR): ?>
                    <a href="#" class="btn btn-mini btn-success" onclick="jQaAnswers.save(<?= $v['id'] ?>, this); return false;"><?= _t('', 'Save') ?></a>
                    <a href="#" class="btn btn-mini btn-danger" onclick="jQaAnswers.del(<?= $v['id'] ?>); return false;"><?= _t('', 'Delete') ?></a>
                <? endif; ?>
                <a href="#" class="btn btn-mini" onclick="jQaAnswers.cancel(<?= $v['id'] ?>); return false;"><?= _t('', 'Cancel') ?></a>
            </li>
            <? if($v['status'] != Qa::STATUS_ANS_DEFAULT): ?><li class="date"><?= $aAnswerStatus[ $v['status'] ]['t'] ?></li><? endif; ?>
        </ul>
    </div>
</form>