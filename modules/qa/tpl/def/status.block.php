<?php
/**
 * @var $this Qa
 */
?>
<? if($moderated != 1 && Qa::premoderation()): ?><div class="alert alert-warning" role="alert"><?= _t('qa', 'Вопрос ожидает проверки модератора') ?></div><? endif; ?>
<? if($status == Qa::STATUS_BLOCKED): ?>
    <div class="alert alert-danger" role="alert"><b><?= _t('qa', 'Вопрос был заблокирован модератором.') ?></b>
    <? if( ! empty($blocked_reason)): ?><br /><?= _t('', 'Причина блокировки:'); ?> <?= tpl::blockedReason($blocked_reason) ?><? endif; ?>
    </div>
<? endif; ?>