<?php
    /**
     * @var $this Qa
     */
    $lng_all = _t('', 'Все');
    $lng_back = _t('form', 'Вернуться назад');
?>
    <button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#j-qa-search-form-block"><i class="fa fa-cog pull-right"></i><?= _t('qa', 'Темы вопросов'); ?> <span class="small hidden-sm">(<?= _t('orders', 'Найдено'); ?> <span class="j-qa-count"><?= $count ?></span>)</span></button>

    <div class="l-leftColumn l-borderedBlock collapse" id="j-qa-search-form-block">
        <form action="" method="get">
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            <input type="hidden" name="sl" value="<?= $f['sl'] ?>" />

            <a href="#j-qa-specs" data-toggle="collapse" data-parent="#accordion" class="h6 active"><i class="fa fa-chevron-down pull-right"></i><?= _t('qa', 'Темы вопросов'); ?></a>
            <div class="collapse in j-collapse" id="j-qa-specs">
                <? if(Specializations::catsOn()): ?>
                    <ul class="l-left-categories l-inside visible-md visible-lg">
                        <? foreach($specs as &$v): if(empty($v['specs'])) continue; ?>
                            <li<? if($v['a']){ ?> class="opened"<? } ?>><a href="<?= $v['url'] ?>" class="j-cat-title"><i class="fa fa-caret-<?= $v['a'] ? 'down' : 'right'?>"></i> <?= $v['title'] ?></a>
                                <ul<? if(!$v['a']){ ?> class="hidden"<? } ?>>
                                    <li<? if($v['a'] && ! $spec_id){ ?> class="checked"<? } ?>><a href="<?= $v['url'] ?>"><?= $lng_all; ?></a></li>
                                    <? foreach($v['specs'] as &$vv): ?>
                                        <li<? if($vv['a']){ ?> class="checked"<? } ?>><a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a></li>
                                    <? endforeach; unset($vv); ?>
                                </ul>
                            </li>
                        <? endforeach; unset($v); ?>
                    </ul>

                    <div class="collapse in" id="j-mobile-cats">
                        <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                            <? foreach($specs as &$v): ?>
                                <li><a href="<?= $v['url'] ?>" class="j-mobile-cat" data-id="<?= $v['id']; ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                            <? endforeach; unset($v); ?>
                        </ul>
                    </div>

                    <? foreach($specs as &$v): if(empty($v['specs'])) continue; ?>
                        <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                            <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                                <li class="active">
                                    <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= $lng_back; ?></span><br>
                                        <?= $v['title'] ?>
                                    </a>
                                    <ul>
                                        <li<? if($v['a'] && ! $spec_id){ ?> class="checked"<? } ?>>
                                            <a href="<?= $v['url'] ?>"><?= $lng_all; ?></a>
                                        </li>
                                        <? foreach($v['specs'] as &$vv): ?>
                                            <li<? if($vv['a']){ ?> class="checked"<? } ?>>
                                                <a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a>
                                            </li>
                                        <? endforeach; unset($vv); ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <? endforeach; unset($v); ?>

                <? else: ?>

                    <ul class="l-left-categories l-inside visible-md visible-lg">
                        <? foreach($specs as &$v): ?>
                            <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url']; ?>"><?= $v['title'] ?></a></li>
                        <? endforeach; unset($v); ?>
                    </ul>

                    <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                        <? foreach($specs as &$v): ?>
                            <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></li>
                        <? endforeach; unset($v); ?>
                    </ul>

                <? endif; ?>
            </div>

        </form>
    </div>

    <div class="visible-md visible-lg">
        <?= $best ?>

        <? # Баннер: Ответы: список ?>
        <div class="l-left-banner"><?= Banners::view('qa_list', array('pos'=>'left', 'spec'=>$spec_id)) ?></div>

    </div>