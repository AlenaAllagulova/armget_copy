<?php

class QaAnswerComments_ extends bff\db\Comments
{
    function initSettings()
    {
        $this->tblComments = TABLE_QA_ANSWERS_COMMENTS;
        $this->tblItems = TABLE_QA_ANSWERS;
        $this->tblItemsID = 'id';

        if( ! bff::adminPanel()){
            $this->tblCommentsGroupID = 'question_id';
        }

        $this->preModeration = false; # постмодерация
        $this->commentsTree = false;
        # config-ключ для хранения общего счетчика непромодерированных комментариев
        $this->counterKey_UnmoderatedAll = 'qa_comments_mod';
        $this->counterKey_ItemComments = 'comments_cnt';
        $this->commentHideReasons[self::commentDeletedByModerator] = _t('', 'Удален модератором');
        $this->commentHideReasons[self::commentDeletedByCommentOwner] = _t('', 'Удален автором комментария');
        $this->commentHideReasons[self::commentDeletedByItemOwner] = _t('qa', 'Удален владельцем ответа');
        $this->commentHideReasons[self::commentFromBlockedUser] = _t('', 'Комментарий от заблокированного пользователя');

        $this->urlListing = false;
        $this->urlListingAjax = $this->adminLink('comments_ajax', 'qa');
    }

    public function commentsGroup($nGroupID = 0)
    {
        if( ! $nGroupID) return array();

        $aData = $this->db->select('SELECT C.*, U.name as uname, U.surname, U.blocked as ublocked, U.login, U.verified, U.pro,
                    US.last_activity
                FROM '.$this->tblComments.' C
                    LEFT JOIN '.TABLE_USERS.' U ON C.user_id = U.user_id
                    LEFT JOIN '.TABLE_USERS_STAT.' US ON C.user_id = US.user_id
                WHERE C.'.$this->tblCommentsGroupID.' = :groupID
                ORDER BY C.id ASC', array(':groupID' => $nGroupID));

        $aResult = array();
        foreach ($aData as $v) {
            if ($v['ublocked'] && !$v['deleted']) {
                $v['deleted'] = self::commentFromBlockedUser;
            }

            if (!isset($aResult[ $v['item_id'] ])){
                $aResult[ $v['item_id'] ] = array();
            }
            $aResult[ $v['item_id'] ][] = $v;
        }

        return $aResult;
    }

}