<?php

class M_Qa_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('qa','Ответы');
        $module = 'qa';

        # Вопросы
        if ($security->haveAccessToModuleToMethod($module, 'questions') ||
            $security->haveAccessToModuleToMethod($module, 'questions-moderate')) {
            $menu->assign($menuTitle, 'Вопросы', $module, 'questions', true, 10,
                    array('rlink'=>array('event'=>'questions&act=add'), 'counter' => 'qa_questions_moderating'));
            $menu->adminHeaderCounter('Вопросы', 'qa_questions_moderating', $module, 'questions&tab=2', 6, '', array('parent'=>'moderation'));
        }

        # Ответы
        if ($security->haveAccessToModuleToMethod($module, 'answers') ||
            $security->haveAccessToModuleToMethod($module, 'answers-moderate')) {
            $menu->assign($menuTitle, $menuTitle, $module, 'answers', true, 20,
                array( 'counter' => 'qa_answers_moderating'));
            $menu->adminHeaderCounter('Вопросы'.' - '.$menuTitle, 'qa_answers_moderating', $module, 'answers', 7, '', array('parent'=>'moderation'));
        }

        # Комментарии
        if ($security->haveAccessToModuleToMethod($module, 'answer-comments')) {
            $menu->assign($menuTitle, 'Комментарии', $module, 'comments_mod', true, 30, array('counter' => 'qa_comments_mod'));
            $menu->adminHeaderCounter($menuTitle, 'qa_comments_mod', $module, 'comments_mod', 7, '', array('parent'=>'comments'));
        }

        # Настройки
        if ($security->haveAccessToModuleToMethod($module, 'settings')) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 50);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module, 'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 30);
        }
    }
}