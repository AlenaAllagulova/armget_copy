<?php
/**
 * Помощь: просмотр вопроса, содержание
 * @var $this Help
 */
 use bff\db\Publicator;

$galleryIndex = 0;
if( ! empty($aData['b']))
{
    foreach($aData['b'] as $v)
    {
        switch($v['type'])
        {
            case Publicator::blockTypeText:
            {
                ?><?= $v['text'][LNG]; ?><?
            } break;
            case Publicator::blockTypeSubtitle:
            {
                $size = 'h' . $v['size'];
                ?><<?= $size ?>><?= $v['text'][LNG]; ?></<?= $size ?>><?
            }break;
            case Publicator::blockTypePhoto:
            {
                ?><p> <img src="<?= $v['url'][Publicator::szView] ?>" alt="<?= $v['alt'][LNG] ?>"> </p><?
                echo ( ! empty($v['text'][LNG]) ? '<div>'.$v['text'][LNG].'</div>' : '');
            } break;
            case Publicator::blockTypeGallery:
            {
                $galleryIndex++;
                ?>
                <p>
                    <div class="fotorama">
                        <? foreach($v['p'] as $gp){ ?><img src="<?= $gp['url'][Publicator::szView] ?>" data-caption="<?= $gp['alt'][LNG] ?>" alt="<?= $gp['alt'][LNG] ?>" /><? } ?>
                    </div>
                </p>
                <?
            } break;
            case Publicator::blockTypeVideo:
            {
                ?><p>
                <object width="<?= $this->sett['video_width']; ?>" height="<?= $this->sett['video_height']; ?>">
                    <param name="allowfullscreen" value="true" />
                    <param name="allowscriptaccess" value="always" />
                    <param name="wmode" value="transparent" />
                    <param name="movie" value="<?= $v['video']; ?>" />
                    <embed src="<?= $v['video']; ?>" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" width="<?= $this->sett['video_width']; ?>" height="<?= $this->sett['video_height']; ?>"></embed>
                </object>
                <p><?
            } break;
        }
    }
}
if($galleryIndex){
    tpl::includeJS('fotorama/fotorama');
    tpl::includeCSS('/js/fotorama/fotorama', false);
}
