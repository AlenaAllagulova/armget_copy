<?php
/**
 * Помощь: поиск вопроса
 * @var $this Help
 * @var $questions array
 */
?>
    <div class="container">
        <section class="l-searchAdd">
            <div class="row">

                <div class="col-sm-12">
                    <div class="l-search-bar">
                        <form role="search" action="<?= Help::url('search') ?>" method="get">
                            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
                            <input type="text" name="q" class="form-control" placeholder="<?= _t('help', 'Найти вопрос'); ?>" autocomplete="off" value="<?= HTML::escape($f['q']) ?>" maxlength="80" />
                            <button class="l-search-button"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>

            </div>

        </section>


        <section class="l-mainContent">

            <? if( ! empty($questions)): ?>
            <div class="l-list-heading">
                <h6><?= _t('help', 'Найдено [n]', array('n' => tpl::declension($total, _t('help', 'совпадение;совпадения;совпадений')))); ?></h6>
                <div class="clearfix"></div>
            </div>

            <ul class="b-blogList media-list l-search-results">
                <? foreach($questions as $v): ?>
                <li class="media">
                    <div class="l-list-num"><?= $num++ ?>.</div>
                    <div class="media-body">
                        <div class="b-item-info">
                            <a href="<?= $v['link'] ?>"><span><?= $v['title'] ?></span></a>

                            <div class="fa-faqQuestion-short">
                                    <div>
                                        <?= $v['textshort'] ?>
                                    </div>
                                    <a href="<?= $v['link'] ?>"><span><?= _t('help', 'Подробнее'); ?></span><i class="fa fa-angle-right c-link-icon"></i></a>
                                </div>

                            <ol class="breadcrumb mrgt10">
                              <li><a href="<?= Help::url('cat', array('keyword'=>$v['cat1_keyword'])) ?>"><?= $v['cat1_title'] ?></a></li>
                              <? if( ! empty($v['cat2_keyword'])): ?>
                              <li><a href="<?= Help::url('cat', array('keyword'=>$v['cat2_keyword'])) ?>"><?= $v['cat2_title'] ?></a></li>
                                <? endif; ?>
                            </ol>

                        </div>

                    </div>

                </li>
                <? endforeach; ?>
            </ul>

            <?= $pgn ?>
            <? else: ?>
                <div class="alert alert-info"><?= _t('help', 'Вопросы не найдены'); ?></div>
            <? endif ;?>

        </section>

    </div>

