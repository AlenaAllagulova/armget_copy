<?php
/**
 * Помощь: вопросы категории
 * @var $this Help
 */
?>
    <div class="container">

        <?= $this->searchForm() ?>

        <section class="l-mainContent">

            <div class="row">

                <aside class="col-md-3 visible-lg visible-md">
                    <? # Баннер: Помощь: список ?>
                    <?= Banners::view('help_list', array('pos'=>'left', 'cat'=>$id)) ?>
                </aside>

                <div class="col-md-9 l-content-column">

                    <?= tpl::getBreadcrumbs($breadCrumbs); ?>

                    <div class="fa-faqCategory fa-faqCategory-inside">
                        <h2><?= $title ?></h2>
                        <ul>
                            <? if( ! empty($subcats_list)): foreach($subcats_list as $v): ?>
                                <li><a href="<?= Help::url('cat', array('keyword'=>$v['keyword'])) ?>"><?= $v['title'] ?></a></li>
                            <? endforeach; endif; ?>
                            <? if( ! empty($questions_list)): foreach($questions_list as $k => $v): ?>
                            <li><a href="#j-faq-<?= $k ?>" data-toggle="collapse" class="ajax-link"><span><?= $v['title'] ?></span></a>
                                <div class="collapse fa-faqQuestion-short" id="j-faq-<?= $k ?>">
                                    <div>
                                        <?= $v['textshort'] ?>
                                    </div>
                                    <a href="<?= $v['link'] ?>"><span><?= _t('', 'Подробнее'); ?></span><i class="fa fa-angle-right c-link-icon"></i></a>
                                </div>
                            </li>
                            <? endforeach; endif; ?>
                        </ul>
                    </div>

                </div>

            </div>

        </section>

    </div>
