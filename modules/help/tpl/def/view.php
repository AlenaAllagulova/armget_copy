<?php
/**
 * Помощь: просмотр вопроса
 * @var $this Help
 */
?>
    <div class="container">

        <?= tpl::getBreadcrumbs($breadCrumbs); ?>

        <section class="l-mainContent">

            <div class="row">
                <div class="col-md-9">

                    <div class="a-articleView">
                        <h1><?= $title ?></h1>

                        <?= $content ?>

                    </div>

                    <? if ( ! empty($questions_other)): ?>
                    <div class="fa-faqCategory">
                        <h2><?= _t('help', 'Другие вопросы из этого раздела'); ?></h2>
                        <ul>
                            <? foreach($questions_other as $v): ?>
                            <li><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                    <? endif; ?>

                </div>

                <div class="col-md-3 visible-lg visible-md">
                    <? # Баннер: Помощь: просмотр ?>
                    <?= Banners::view('help_view', array('pos'=>'right')) ?>
                </div>

            </div>

        </section>

    </div>
