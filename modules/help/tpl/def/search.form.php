<?php

/**
 * Помощь: форма поиска
 * @var $this Help
 */

extract($f, EXTR_REFS | EXTR_PREFIX_ALL, 'f');

?>
<section class="l-searchAdd">
    <div class="row">

        <div class="col-sm-3 add-button">
            <a href="<?= Contacts::url('write') ?>" class="btn btn-primary btn-block"><i class="fa fa-question-circle hidden-sm"></i> <?= _t('help', 'Задать вопрос'); ?></a>
        </div>

        <div class="col-sm-9 l-content-column">
            <div class="l-search-bar">
                <form role="search" action="<?= Help::url('search') ?>" method="get">
                    <input type="text" name="q" class="form-control" placeholder="<?= _t('help', 'Найти вопрос'); ?>" autocomplete="off" value="<?= HTML::escape($f_q) ?>" maxlength="80" >
                    <button class="l-search-button"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>

    </div>
</section>