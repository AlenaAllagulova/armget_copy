<?php
/**
 * Помощь: главная
 * @var $this Help
 */
?>
    <div class="container">

        <?= $this->searchForm() ?>

        <section class="l-mainContent">

            <div class="row">

                <aside class="col-md-3 visible-lg visible-md">
                    <? # Баннер: Помощь: список ?>
                    <?= Banners::view('help_list', array('pos'=>'left', 'cat'=>0)) ?>
                </aside>

                <div class="col-md-9 l-content-column">
                    <? foreach($items  as $v):
                        if( ! empty($v['subcats'])): ?>
                            <div class="fa-faqCategory">
                                <h2><?= $v['title'] ?></h2>
                                <ul>
                                    <? foreach($v['subcats'] as $vv): ?>
                                    <li><a href="<?= Help::url('cat', array('keyword'=>$vv['keyword'])) ?>"><?= $vv['title'] ?></a></li>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        <? else: ?>
                            <div class="fa-faqCategory">
                                <a href="<?= Help::url('cat', array('keyword'=>$v['keyword'])) ?>"><?= $v['title'] ?></a>
                            </div>
                        <? endif;
                    endforeach; ?>


                    <? if( ! empty($favs)): ?>
                    <div class="fa-faqCategory">
                        <h2><?= _t('help', 'Частые вопросы'); ?></h2>
                        <ul>
                            <? foreach($favs as $v): ?>
                            <li><a href="<?= Help::urlDynamic($v['link']) ?>"><?= $v['title'] ?></a></li>
                            <? endforeach; ?>
                        </ul>

                    </div>
                    <? endif; ?>

                </div>

            </div>

        </section>

    </div>
