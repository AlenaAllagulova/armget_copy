<?php

return [
    # просмотр вопроса
    'help-view' => [
        'pattern'  => 'help/(.*)\-([\d]+)\.html',
        'callback' => 'help/view/id=$2',
        'priority' => 440,
    ],
    # поиск
    'help-search' => [
        'pattern'  => 'help/search/',
        'callback' => 'help/search/',
        'priority' => 450,
    ],
    # список вопросов (в категории)
    'blog-list' => [
        'pattern'  => 'help/(.*)',
        'callback' => 'help/listing/cat=$1',
        'priority' => 460,
    ],
];