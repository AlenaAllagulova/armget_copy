<?php

class M_Help_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('help', 'Help');
        $module = 'help';
        # Вопросы
        if ($security->haveAccessToModuleToMethod($module, 'questions')) {
            $menu->assign($menuTitle, 'Вопросы', $module, 'questions', true, 1,
                array('rlink' => array('event' => 'questions&act=add'))
            );
        }
        # Категории
        if ($security->haveAccessToModuleToMethod($module, 'categories')) {
            $menu->assign($menuTitle, 'Категории', $module, 'categories', true, 5,
                array('rlink' => array('event' => 'categories&act=add'))
            );
        }
        # SEO
        if ($security->haveAccessToModuleToMethod($module,'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 50);
        }
    }
}