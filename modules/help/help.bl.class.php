<?php

abstract class HelpBase_ extends Module
{
    /** @var HelpModel */
    public $model = null;
    public $securityKey = '7a6479879566d66524eeea5e38589b28';

    public function init()
    {
        parent::init();
        $this->module_title = _t('help', 'Help');
    }

    /**
     * Shortcut
     * @return Help
     */
    public static function i()
    {
        return bff::module('help');
    }

    /**
     * Shortcut
     * @return HelpModel
     */
    public static function model()
    {
        return bff::model('help');
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts доп. параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic) . '/help/';
        switch ($key) {
            # Главная
            case 'index':
                # /help/
                break;
            # Поиск вопросов
            case 'search':
                $url .= 'search/' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Список вопросов по категории
            case 'cat':
                $url .= (isset($opts['keyword']) ? $opts['keyword'] . '/' : '');
                break;
            # Просмотр вопроса
            case 'view':
                $url .= mb_substr(mb_strtolower(func::translit($opts['title'])), 0, 100) . '-' . $opts['id'] . '.html';
                break;
        }
        return bff::filter('help.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        return array(
            'pages'  => array(
                'listing' => array(
                    't'      => 'Главная',
                    'macros' => array(),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'listing-category' => array(
                    't'       => 'Список в категории',
                    'list'    => true,
                    'inherit' => true,
                    'macros'  => array(
                        'category'           => array('t' => 'Название категории (текущая)'),
                        'categories'         => array('t' => 'Название всех категорий'),
                        'categories.reverse' => array('t' => 'Название всех категорий<br />(обратный порядок)'),
                    ),
                ),
                'search' => array(
                    't'      => 'Поиск вопроса',
                    'list'   => true,
                    'macros' => array(
                        'query' => array('t' => 'Строка поиска'),
                    ),
                ),
                'view' => array(
                    't'       => 'Просмотр вопроса',
                    'macros'  => array(
                        'title'              => array('t' => 'Заголовок вопроса'),
                        'textshort'          => array('t' => 'Краткое описание (до 150 символов)'),
                        'category'           => array('t' => 'Категория вопроса (текущая)'),
                        'categories'         => array('t' => 'Название всех категорий'),
                        'categories.reverse' => array('t' => 'Название всех категорий<br />(обратный порядок)'),
                    ),
                ),
            ),
        );
    }

    /**
     * Инициализируем компонент bff\db\Publicator
     * @return bff\db\Publicator компонент
     */
    public function initPublicator()
    {
        $aSettings = array(
            'title'           => false,
            'langs'           => $this->locale->getLanguages(),
            'images_path'     => bff::path('help', 'images'),
            'images_path_tmp' => bff::path('tmp', 'images'),
            'images_url'      => bff::url('help', 'images'),
            'images_url_tmp'  => bff::url('tmp', 'images'),
            # photo
            'photo_sz_view'   => array('width' => 800),
            # gallery
            'gallery_sz_view' => array(
                'width'    => 800,
                'height'   => false,
                'vertical' => array('width' => false, 'height' => 400),
                'quality'  => 95,
                'sharp'    => array() // no sharp
            ),
            'video_width'          => 560,
            'video_height'         => 315,
            'use_wysiwyg'          => true,
        );

        # настройки водяного знака
        $watermark = Site::i()->watermarkSettings('help');
        if (!empty($watermark)) {
            $aWatermark = array(
                'watermark'       => true,
                'watermark_src'   => $watermark['file']['path'],
                'watermark_pos_x' => $watermark['pos_x'],
                'watermark_pos_y' => $watermark['pos_y'],
            );
            $aSettings['gallery_sz_view'] += $aWatermark;
            $aSettings['photo_sz_view'] += $aWatermark;
        }

        return $this->attachComponent('publicator', new bff\db\Publicator($this->module_name, $aSettings));
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('help', 'images') => 'dir-only', # изображения
            bff::path('tmp', 'images')  => 'dir-only', # tmp
        ));
    }
}