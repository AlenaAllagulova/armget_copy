<?php
define('TABLE_ARTICLES',                  DB_PREFIX.'articles');
define('TABLE_ARTICLES_CATEGORIES',       DB_PREFIX.'articles_categories');
define('TABLE_ARTICLES_CATEGORIES_LANG',  DB_PREFIX.'articles_categories_lang');
define('TABLE_ARTICLES_TAGS',             DB_PREFIX.'articles_tags');
define('TABLE_ARTICLES_ARTICLES_TAGS',    DB_PREFIX.'articles_articles_tags');
define('TABLE_ARTICLES_VOTES',            DB_PREFIX.'articles_votes');
define('TABLE_ARTICLES_COMMENTS',         DB_PREFIX.'articles_comments');
define('TABLE_ARTICLES_USERS',            DB_PREFIX.'articles_users');
define('TABLE_ARTICLES_IMAGES',           DB_PREFIX.'articles_images');

class ArticlesModel_ extends Model
{
    /** @var ArticlesBase */
    var $controller;

    /** @var bff\db\NestedSetsTree для категорий */
    public $treeCategories;
    public $langCategories = array(
        'title'        => TYPE_NOTAGS, # Название
        'mtitle'       => TYPE_NOTAGS, # Meta Title
        'mkeywords'    => TYPE_NOTAGS, # Meta Keywords
        'mdescription' => TYPE_NOTAGS, # Meta Description
        'seotext'      => TYPE_STR,    # SEO-Text
    );

    public function init()
    {
        parent::init();

        # подключаем nestedSets категории
        $this->treeCategories = new bff\db\NestedSetsTree(TABLE_ARTICLES_CATEGORIES);
        $this->treeCategories->init();
    }

    # --------------------------------------------------------------------
    # Статьи

    /**
     * Список cтатей (admin)
     * @param array $aFilter фильтр списка cтатей
     * @param bool $bCount только подсчет кол-ва cтатей
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function articlesListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $sJoin = '';

        if (isset($aFilter[':tag'])) {
            $sJoin .= ', '.TABLE_ARTICLES_ARTICLES_TAGS.' T ';
            $aFilter[':tag'] = array('A.id = T.article_id AND T.tag_id = :tag', ':tag' => $aFilter[':tag']);
        }

        if (isset($aFilter[':user'])) {
            $sJoin .= ', ' . TABLE_USERS . ' U ';
            $aFilter[':ju'] = ' A.user_id = U.user_id ';
        }

        $aFilter = $this->prepareFilter($aFilter, 'A');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(A.id) FROM '.TABLE_ARTICLES.' A '.$sJoin.$aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT A.id, A.created, A.title, A.enabled, A.user_id, A.keyword
               FROM '.TABLE_ARTICLES.' A'.$sJoin.'
               '.$aFilter['where']
            .( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '')
            .$sqlLimit, $aFilter['bind']);
    }

    /**
     * Список cтатей (frontend)
     * @param array $aFilter фильтр списка cтатей
     * @param bool $bCount только подсчет кол-ва cтатей
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function articlesList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $nUserID = User::id();
        $sqlJoin = '';
        if (isset($aFilter['tag'])) {
            $sqlJoin = ', ' . TABLE_ARTICLES_ARTICLES_TAGS . ' TT ';
            $aFilter[':tag'] = array('TT.article_id = A.id AND TT.tag_id = :tag', ':tag' => $aFilter['tag']);
            unset($aFilter['tag']);
        }

        $aFilter['status'] = Articles::STATUS_ACTIVE;
        if (Articles::premoderation()) {
            $aFilter[':moderated'] = 'moderated > 0';
        }
        $aFilter[':user'] = 'A.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'A');
            return $this->db->one_data('SELECT COUNT(id) FROM ( SELECT A.id FROM ' . TABLE_ARTICLES . ' A ' . $sqlJoin . ', '.TABLE_USERS.' U
                    ' . $aFilter['where'].' GROUP BY A.id ) sl', $aFilter['bind']
            );
        }

        $aFilter = $this->prepareFilter($aFilter, 'A');
        if ($nUserID) {
            $aFilter['bind'][':userID'] = $nUserID;
        }

        $aData = $this->db->select('
            SELECT A.id, A.user_id, A.keyword, A.created, A.title, A.content_preview,
                   A.comments_enabled, A.comments_cnt, A.views_total, A.moderated, A.status, A.votes,
                   A.imgcnt, A.img_v, GROUP_CONCAT(T.tag_id) AS tags,
                   U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex'.($nUserID ? ', V.vote' : ', 0 AS vote').'
            FROM ' . TABLE_ARTICLES . ' A
                 '.( $nUserID ? 'LEFT JOIN '.TABLE_ARTICLES_VOTES.' V ON A.id = V.article_id AND V.user_id = :userID' : '').'
                 LEFT JOIN ' . TABLE_ARTICLES_ARTICLES_TAGS . ' T ON A.id = T.article_id
                 ' . $sqlJoin . '
                 , '.TABLE_USERS.' U
            ' . $aFilter['where'] . '
            GROUP BY A.id
            ' . 'ORDER BY ' . $sqlOrder . ' ' . $sqlLimit, $aFilter['bind']);
        if (empty($aData)) $aData = array();
        return $aData;
    }

    /**
     * Получение данных cтатьи
     * @param integer $nArticleID ID cтатьи
     * @param array $aFields необходимые поля
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function articleData($nArticleID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT A.*, U.email, U.blocked as user_blocked
                    FROM '.TABLE_ARTICLES.' A, ' . TABLE_USERS . ' U
                    WHERE A.user_id = U.user_id AND A.id = :id',
                array(':id'=>$nArticleID));

        } else {
            if (empty($aFields)) {
                $aFields = array('A.*');
            } else {
                foreach ($aFields as $k => $v) {
                    $aFields[$k] = 'A.'.$v;
                }
            }
            $aData = $this->db->one_array('SELECT '.join(', ', $aFields).'
                    FROM '.TABLE_ARTICLES.' A
                    WHERE A.id = :id ',
                array(':id' => $nArticleID)
            );
        }
        return $aData;
    }

    /**
     * Сохранение cтатьи
     * @param integer $nArticleID ID cтатьи
     * @param array $aData данные cтатьи
     * @return boolean|integer
     */
    public function articleSave($nArticleID, array $aData)
    {
        if (empty($aData)) return false;

        if ($nArticleID > 0)
        {
            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_ARTICLES, $aData, array('id'=>$nArticleID));

            return ! empty($res);
        }
        else
        {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nArticleID = $this->db->insert(TABLE_ARTICLES, $aData);
            if ($nArticleID > 0) {
                $aBest = $this->db->one_array('SELECT * FROM '.TABLE_ARTICLES_USERS.' WHERE user_id = :user', array(':user' => $aData['user_id']));
                $aCounts = $this->db->one_array('
                    SELECT  COUNT(A.id) AS articles_cnt, SUM(A.votes) AS votes
                    FROM '.TABLE_ARTICLES.' A
                    WHERE A.user_id = :user', array(':user' => $aData['user_id']));

                if (empty($aBest)) {
                    $aCounts['user_id'] = $aData['user_id'];
                    $this->db->insert(TABLE_ARTICLES_USERS, $aCounts);
                } else {
                    $this->db->update(TABLE_ARTICLES_USERS, $aCounts, array('user_id' => $aData['user_id']));
                }
            }
            return $nArticleID;
        }
    }

    /**
     * Переключатели cтатьи
     * @param integer $nArticleID ID cтатьи
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function articleToggle($nArticleID, $sField)
    {
        switch ($sField) {
            case 'enabled': { # Включен
                return $this->toggleInt(TABLE_ARTICLES, $nArticleID, $sField, 'id');
            } break;
        }
    }

    /**
     * Удаление cтатьи
     * @param integer $nArticleID ID cтатьи
     * @return boolean
     */
    public function articleDelete($nArticleID)
    {
        if (empty($nArticleID)) return false;
        $res = $this->db->delete(TABLE_ARTICLES, array('id'=>$nArticleID));
        if ( ! empty($res)) {
            return true;
        }
        return false;
    }

    /**
     * Получаем общее кол-во статей, ожидающих модерации
     * @return integer
     */
    public function articlesModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(A.id)
                FROM ' . TABLE_ARTICLES . ' A
                WHERE A.moderated != 1'
        );
    }

    # --------------------------------------------------------------------
    # Категории

    /**
     * Список категорий (admin)
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter[] = 'pid != 0';
        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM '.TABLE_ARTICLES_CATEGORIES.' C, '.TABLE_ARTICLES_CATEGORIES_LANG.' CL '.$aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT C.id, C.created, CL.title, C.enabled, C.pid, C.numlevel, ((C.numright-C.numleft)-1) as node
               FROM '.TABLE_ARTICLES_CATEGORIES.' C, '.TABLE_ARTICLES_CATEGORIES_LANG.' CL
               '.$aFilter['where']
            .( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '')
            .$sqlLimit, $aFilter['bind']);
    }

    /**
     * Список категорий (frontend)
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter[] = 'pid != 0';
        if ( ! $bCount) $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM '.TABLE_ARTICLES_CATEGORIES.' C '.$aFilter['where'], $aFilter['bind']);
        }

        $aData = $this->db->select('SELECT C.id
                                  FROM '.TABLE_ARTICLES_CATEGORIES.' C, '.TABLE_ARTICLES_CATEGORIES_LANG.' CL
                                  '.$aFilter['where'].'
                                  '.( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '').'
                                  '.$sqlLimit, $aFilter['bind']);

        if ( ! empty($aData))
        {
            //
        }

        return $aData;
    }

    /**
     * Список всех категорий с подкатегориями
     * @param array $aFields - выбираемые поля
     * @return mixed
     */
    public function categoriesListAll(array $aFields = array())
    {
        if (!in_array('id', $aFields)) {
            $aFields[] = 'id';
        }
        if (!in_array('pid', $aFields)) {
            $aFields[] = 'pid';
        }
        foreach ($aFields as $k => $v) {
            $aFields[$k] = (array_key_exists($v, $this->langCategories) ? 'CL.' : 'C.') . $v;
        }

        $aFilter = array('pid != 0', 'enabled'=>1);
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aData = $this->db->select('SELECT ' . join(',', $aFields) . ', ((C.numright-C.numleft)>1) as subs
                                  FROM ' . TABLE_ARTICLES_CATEGORIES . ' C, ' . TABLE_ARTICLES_CATEGORIES_LANG . ' CL
                                  ' . $aFilter['where'] . $this->db->langAnd(true, 'C', 'CL') . '
                                  ORDER BY C.numleft
                                  ', $aFilter['bind']
        );

        if (!empty($aData)) {
            $aData = $this->db->transformRowsToTree($aData, 'id', 'pid', 'sub');
        }

        return $aData;
    }

    /**
     *  Список категорий из массива
     * @param array $aCats ID категорий
     * @return array
     */
    public function categoriesListInArray($aCats = array())
    {
        if(empty($aCats)) return array();
        return $this->db->select_key('
            SELECT C.id, C.keyword, CL.title, C.numlevel
            FROM '.TABLE_ARTICLES_CATEGORIES.' C, '.TABLE_ARTICLES_CATEGORIES_LANG.' CL
            WHERE '.$this->db->prepareIN('C.id', $aCats).
            $this->db->langAnd(true, 'C', 'CL').'
            ORDER BY C.numleft
            ', 'id');
    }

    /**
     * Получение данных категории
     * @param integer $nCategoryID ID категории
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function categoryData($nCategoryID, $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT C.*, ((C.numright-C.numleft)-1) as node
                    FROM '.TABLE_ARTICLES_CATEGORIES.' C
                    WHERE C.id = :id',
                array(':id'=>$nCategoryID));
            if ( ! empty($aData)) {
                $this->db->langSelect($nCategoryID, $aData, $this->langCategories, TABLE_ARTICLES_CATEGORIES_LANG);
            }
        } else {
            //
        }
        return $aData;
    }

    /**
     * Получение meta данных категории
     * @param mixed $mCategory ID/Keyword категории
     * @return array
     */
    public function categoryMeta($mCategory)
    {
        $aData = $this->db->one_array('SELECT
                    C.id, C.pid, C.keyword, C.numlevel, CL.title,
                    C.mtemplate, CL.mtitle, CL.mkeywords, CL.mdescription, CL.seotext
                FROM ' . TABLE_ARTICLES_CATEGORIES . ' C, ' . TABLE_ARTICLES_CATEGORIES_LANG . ' CL
                WHERE C.'.(is_int($mCategory) ? 'id' : 'keyword').' = :cat '
                    . $this->db->langAnd(true, 'C', 'CL'),
            array(':cat' => $mCategory)
        );
        if (empty($aData)) $aData = array();
        return $aData;
    }

    /**
     * Сохранение категории
     * @param integer $nCategoryID ID категории
     * @param array $aData данные категории
     * @return boolean|integer
     */
    public function categorySave($nCategoryID, array $aData)
    {
        if (empty($aData)) return false;

        if ($nCategoryID > 0)
        {
            $aData['modified'] = $this->db->now(); # Дата изменения

            if (isset($aData['pid'])) unset($aData['pid']); # запрет изменения pid
            $res = $this->db->update(TABLE_ARTICLES_CATEGORIES, array_diff_key($aData, $this->langCategories), array('id'=>$nCategoryID));

            $this->db->langUpdate($nCategoryID, $aData, $this->langCategories, TABLE_ARTICLES_CATEGORIES_LANG);

            return ! empty($res);
        }
        else
        {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nCategoryID = $this->treeCategories->insertNode($aData['pid']);
            if ($nCategoryID > 0) {
                unset($aData['pid']);
                $this->db->update(TABLE_ARTICLES_CATEGORIES, array_diff_key($aData, $this->langCategories), 'id = :id', array(':id'=>$nCategoryID));
                $this->db->langInsert($nCategoryID, $aData, $this->langCategories, TABLE_ARTICLES_CATEGORIES_LANG);
            }
            return $nCategoryID;
        }
    }

    /**
     * Переключатели категории
     * @param integer $nCategoryID ID категории
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function categoryToggle($nCategoryID, $sField)
    {
        switch ($sField) {
            case 'enabled': { # Включен
                return $this->toggleInt(TABLE_ARTICLES_CATEGORIES, $nCategoryID, $sField, 'id');
            } break;
        }
    }

    /**
     * Перемещение категории
     * @return mixed @see rotateTablednd
     */
    public function categoriesRotate()
    {
        return $this->treeCategories->rotateTablednd();
    }

    /**
     * Удаление категории
     * @param integer $nCategoryID ID категории
     * @return boolean
     */
    public function categoryDelete($nCategoryID)
    {
        if (empty($nCategoryID)) return false;
        $nSubCnt = $this->categorySubCount($nCategoryID);
        if ( ! empty($nSubCnt)) {
            $this->errors->set('Невозможно выполнить удаление категории при наличии подкатегорий');
            return false;
        }

        $nItems = $this->db->one_data('SELECT COUNT(I.id) FROM '.TABLE_ARTICLES.' I WHERE I.cat_id = :id', array(':id'=>$nCategoryID));
        if ( ! empty($nItems)) {
            $this->errors->set('Невозможно выполнить удаление категории при наличии вложенных элементов');
            return false;
        }

        $aDeletedID = $this->treeCategories->deleteNode($nCategoryID);
        $res = ! empty($aDeletedID);
        if ( ! empty($res)) {
            $this->db->delete(TABLE_ARTICLES_CATEGORIES_LANG, array('id'=>$nCategoryID));
            return true;
        }
        return false;
    }

    /**
     * Получаем кол-во вложенных категорий
     */
    public function categorySubCount($nCategoryID)
    {
        return $this->treeCategories->getChildrenCount($nCategoryID);
    }

    /**
     * Формирование списка подкатегорий
     * @param integer $nCategoryID ID категории
     * @param mixed $mOptions формировать select-options или FALSE
     * @param array $aFilter фильтр категорий
     * @return array|string
     */
    public function categorySubOptions($nCategoryID, $mOptions = false, $aFilter = array())
    {
        $aFilter['pid'] = $nCategoryID;
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aData = $this->db->select('SELECT C.id, CL.title
                    FROM '.TABLE_ARTICLES_CATEGORIES.' C, '.TABLE_ARTICLES_CATEGORIES_LANG.' CL
                    ' . $aFilter['where'] . '
                    ORDER BY C.numleft', $aFilter['bind']);

        if (empty($mOptions)) return $aData;

        return HTML::selectOptions($aData, $mOptions['sel'], $mOptions['empty'], 'id', 'title');
    }

    /**
     * Формирование списка основных категорий
     * @param integer $nSelectedID ID выбранной категории
     * @param mixed $mEmptyOpt невыбранное значение
     * @param integer $nType тип списка: 0 - все(кроме корневого), 1 - список при добавлении категории, 2 - список при добавлении записи
     * @param array $aOnlyID только список определенных категорий
     * @return string <option></option>...
     */
    public function categoriesOptions($nSelectedID = 0, $mEmptyOpt = false, $nType = 0, $aOnlyID = array())
    {
        $aFilter = array();
        if ($nType == 1) {
            $aFilter[] = 'numlevel < 2';
        } else {
            $aFilter[] = 'numlevel > 0';
        }
        if ($nType == 2) {
            $aFilter[] = 'enabled = 1';
        }
        if ( ! empty($aOnlyID)) {
            $aFilter[':only'] = '(C.id IN ('.join(',', $aOnlyID).') OR C.pid IN('.join(',', $aOnlyID).'))';
        }

        # Chrome не понимает style="padding" в option
        $bUsePadding = (mb_stripos(Request::userAgent(), 'chrome') === false);
        $bJoinItems = ($nType > 0);
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aCategories = $this->db->select('SELECT C.id, CL.title, C.numlevel, ((C.numright-C.numleft)-1) as node
                    '.( $bJoinItems ? ', COUNT(I.id) as items ' : '' ).'
               FROM '.TABLE_ARTICLES_CATEGORIES.' C
                    '.( $bJoinItems ? ' LEFT JOIN '.TABLE_ARTICLES.' I ON C.id = I.cat_id ' : '').'
                    , '.TABLE_ARTICLES_CATEGORIES_LANG.' CL
               '.$aFilter['where'].'
               GROUP BY C.id
               ORDER BY C.numleft', $aFilter['bind']);

        $sOptions = '';
        foreach ($aCategories as &$v) {
            $nNumlevel = &$v['numlevel'];
            $bDisable = ( $nType>0 && ($nType == 2 ? $v['node'] > 0 : ( $nNumlevel>0 && $v['items']>0 ) ) );
            $sOptions .= '<option value="'.$v['id'].'" '.
                ($bUsePadding && $nNumlevel>1 ? 'style="padding-left:'.($nNumlevel*10).'px;" ':'').
                ($v['id'] == $nSelectedID?' selected':'').
                ($bDisable ? ' disabled' : '').
                '>'.( ! $bUsePadding && $nNumlevel>1 ? str_repeat('  ', $nNumlevel) :'').$v['title'].'</option>';
        } unset($v);

        if ($mEmptyOpt !== false) {
            $nValue = 0;
            if (is_array($mEmptyOpt)) {
                $nValue = key($mEmptyOpt);
                $mEmptyOpt = current($mEmptyOpt);
            }
            $sOptions = '<option value="'.$nValue.'" class="bold">'.$mEmptyOpt.'</option>'.$sOptions;
        }

        return $sOptions;
    }

    /**
     * Формирование списков категорий (при добавлении/редактировании записи)
     * @param array $aCategoriesID ID категорий [lvl=>selectedID, ...]
     * @param mixed $mOptions формировать select-options или нет (false)
     * @return array [lvl=>[a=>selectedID, categories=>список категорий(массив или options)],...]
     */
    public function categoriesOptionsByLevel($aCategoriesID, $mOptions = false)
    {
        if (empty($aCategoriesID)) return array();

        # формируем список требуемых уровней категорий
        $aLevels = array(); $bFill = true; $parentID = 1;
        foreach ($aCategoriesID as $lvl=>$nCategoryID) {
            if ($nCategoryID || $bFill) {
                $aLevels[$lvl] = $parentID;
                if ( ! $nCategoryID) break;
                $parentID = $nCategoryID;
            } else {
                break;
            }
        }

        if (empty($aLevels)) return array();

        $aData = $this->db->select('SELECT C.id, CL.title, C.numlevel as lvl
                    FROM '.TABLE_ARTICLES_CATEGORIES.' C, '.TABLE_ARTICLES_CATEGORIES_LANG.' CL
                    WHERE C.numlevel IN ('.join(',', array_keys($aLevels)).')
                      AND C.pid IN('.join(',', $aLevels).')
                      '.$this->db->langAnd(true, 'C', 'CL').'
                    ORDER BY C.numleft');
        if (empty($aData)) return array();

        $aLevels = array();
        foreach ($aData as $v) {
            $aLevels[$v['lvl']][$v['id']] = $v;
        } unset($aData);

        foreach ($aCategoriesID as $lvl=>$nSelectedID)
        {
            if (isset($aLevels[$lvl])) {
                $aCategoriesID[$lvl] = array(
                    'a' => $nSelectedID,
                    'categories' => ( ! empty($mOptions) ?
                        HTML::selectOptions($aLevels[$lvl], $nSelectedID, $mOptions['empty'], 'id', 'title') :
                        $aLevels[$lvl] ),
                );
            } else {
                $aCategoriesID[$lvl] = array(
                    'a' => $nSelectedID,
                    'categories' => false,
                );
            }
        }

        return $aCategoriesID;
    }

    /**
     * Получаем данные parent-категорий
     * @param integer $nCategoryID ID категории
     * @param bool $bIncludingSelf включать текущую в итоговых список
     * @param bool $bExludeRoot исключить корневой раздел
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsID($nCategoryID, $bIncludingSelf = true, $bExludeRoot = true)
    {
        if (empty($nCategoryID)) return array(1=>0);
        $aData = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf, array('id','numlevel'));
        $aParentsID = array();
        if ( ! empty($aData)) {
            foreach ($aData as $v) {
                $aParentsID[ $v['numlevel'] ] = $v['id'];
            }
        }
        return $aParentsID;
    }

    /**
     * Получаем названия parent-категорий
     * @param integer $nCategoryID ID категории
     * @param boolean $bIncludingSelf включать текущую в итоговых список
     * @param boolean $bExludeRoot исключить корневой раздел
     * @param mixed $mSeparator объединить в одну строку или FALSE
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsTitle($nCategoryID, $bIncludingSelf = false, $bExludeRoot = false, $mSeparator = true)
    {
        $aParentsID = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf);
        if (empty($aParentsID)) return ($mSeparator !== false ? '' : array());

        $aData = $this->db->select_one_column('SELECT CL.title
                   FROM '.TABLE_ARTICLES_CATEGORIES.' C, '.TABLE_ARTICLES_CATEGORIES_LANG.' CL
                   WHERE '.$this->db->prepareIN('C.id', $aParentsID).''.$this->db->langAnd(true, 'C', 'CL').'
                   ORDER BY C.numleft');

        $aData = ( ! empty($aData) ? $aData : array());

        if ($mSeparator !== false) {
            return join('  '.($mSeparator === true ? '>' : $mSeparator).'  ', $aData);
        } else {
            return $aData;
        }
    }

    # --------------------------------------------------------------------
    # Голосование за статью

    /**
     * Сохранение голоса за статью
     * @param array $aData данные о голосе
     * @return bool|int сумма голосов за статью
     */
    public function voteSave(array $aData)
    {
        if (empty($aData) || empty($aData['article_id'])) return false;

        $res = $this->db->insert(TABLE_ARTICLES_VOTES, $aData, false);
        if (!$res) return false;

        # обновляем сумму голосов за ответ
        $nAnswerID = $aData['article_id'];
        $nSum = $this->votesSum(array('article_id' => $nAnswerID));
        $this->articleSave($nAnswerID, array('votes' => $nSum));

        return $nSum;
    }

    /**
     * Сумма голосов за статью на основе фильтра
     * @param array $aFilter
     * @return integer (-6, 0, 4)
     */
    public function votesSum(array $aFilter = array())
    {
        if (empty($aFilter)) return 0;

        $aFilter = $this->prepareFilter($aFilter, 'V');
        return (int)$this->db->one_data('SELECT SUM(V.vote)
            FROM '.TABLE_ARTICLES_VOTES.' V
            '.$aFilter['where'], $aFilter['bind']);
    }

    /**
     * Список пользователей для блока популярных авторов статей
     * @param int $nLimit количество пользователей
     * @return array
     */
    public function bestList($nLimit = 4)
    {
        return $this->db->select('
            SELECT A.user_id, A.articles_cnt, A.votes, U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex
            FROM '.TABLE_ARTICLES_USERS.' A,
                 '.TABLE_USERS.' U
            WHERE A.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0
            ORDER BY A.average DESC, A.votes DESC, A.articles_cnt DESC
            LIMIT '.$nLimit);
    }

    /**
     * Формирование списка популярных авторов
     */
    public function bestGenerate()
    {
        # удалим пользователей у которых нет статей. (статьи были, но их удалили)
        $this->db->exec('
            DELETE U
            FROM '.TABLE_ARTICLES_USERS.' U
                LEFT JOIN '.TABLE_ARTICLES.' A ON A.user_id = U.user_id
            WHERE A.id IS NULL
        ');

        # добавим пользователей, которые создали первые статьи. (ранише сатей не было и их добавили)
        $this->db->exec('
            INSERT INTO '.TABLE_ARTICLES_USERS.'(user_id, articles_cnt, votes)
            SELECT  A.user_id, COUNT(A.id), SUM(A.votes)
            FROM '.TABLE_ARTICLES.' A
                LEFT JOIN '.TABLE_ARTICLES_USERS.' U ON A.user_id = U.user_id
            WHERE U.id IS NULL
            GROUP BY A.user_id
        ');

        # актуализируем счетчики количества статей и голосов для старых пользователей (которые были в списке)
        $this->db->exec('
            UPDATE '.TABLE_ARTICLES_USERS.' U, (
                SELECT  A.user_id, COUNT(A.id) AS cnt, SUM(A.votes) AS votes
                FROM '.TABLE_ARTICLES.' A
                GROUP BY A.user_id
                ) S
            SET U.articles_cnt = S.cnt, U.votes = S.votes
            WHERE U.user_id = S.user_id
        ');

        # расчитаем рейтинг - среднее количество голосов за статью, старше одной недели
        $this->db->exec('
            UPDATE '.TABLE_ARTICLES_USERS.' U, (
                SELECT  A.user_id, COUNT(A.id) AS cnt, SUM(A.votes) AS votes
                FROM '.TABLE_ARTICLES.' A
                WHERE A.created < :created AND A.moderated = 1 AND A.status = :active
                GROUP BY A.user_id
                ) S
            SET U.average = round(S.votes / S.cnt)
            WHERE U.user_id = S.user_id
        ', array(
            ':created' => date('Y-m-d H:i:s', strtotime('-1 week')),
            ':active'  => Articles::STATUS_ACTIVE,
        ));
    }

    public function getLocaleTables()
    {
        return array(
            TABLE_ARTICLES_CATEGORIES => array('type' => 'table', 'fields' => $this->langCategories, 'title' => _t('articles', 'Категории')),
        );
    }

    /**
     * Статистика
     * @return array
     */
    public function statisticData()
    {
        $data = array();

        # статей
        $filter = array('status' => Articles::STATUS_ACTIVE);
        if (Articles::premoderation()) {
            $filter[':moderated'] = 'moderated > 0';
        }
        $filter[':user'] = 'A.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';
        $filter = $this->prepareFilter($filter, 'A');
        $data['articles'] = (int)$this->db->one_data(' SELECT COUNT(A.id) FROM '.TABLE_ARTICLES.' A, '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);

        return $data;
    }

}