<?php

class M_Articles_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('articles','Articles');
        $module = 'articles';

        # Статьи
        if ($security->haveAccessToModuleToMethod($module, 'items') ||
            $security->haveAccessToModuleToMethod($module, 'items-moderate')) {
            $menu->assign($menuTitle, 'Список', $module, 'listing', true, 10,
                array('rlink'=>array('event'=>'listing&act=add'), 'counter' => 'articles_items_moderating')
            );
            $menu->adminHeaderCounter($menuTitle, 'articles_items_moderating', $module, 'listing&tab=2', 10, '', array('parent'=>'moderation'));
        }

        # Комментарии
        if ($security->haveAccessToModuleToMethod($module, 'items-comments')) {
            $menu->assign($menuTitle, 'Комментарии', $module, 'comments_mod', true, 15, array('counter' => 'articles_comments_mod'));
            $menu->adminHeaderCounter($menuTitle, 'articles_comments_mod', $module, 'comments_mod', 10, '', array('parent'=>'comments'));
        }

        # Категории
        if ($security->haveAccessToModuleToMethod($module,'categories')) {
            $menu->assign($menuTitle, 'Категории', $module, 'categories', true, 20,
                array('rlink'=>array('event'=>'categories&act=add') ));
        }

        # Теги
        if ($security->haveAccessToModuleToMethod($module, 'tags')) {
            $menu->assign($menuTitle, _t('','Tags'), $module, 'tags', true, 30);
        }

        # Настройки
        if ($security->haveAccessToModuleToMethod($module, 'settings')) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 50);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module, 'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 40);
        }
    }
}