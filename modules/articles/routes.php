<?php

return [
    # просмотр
    'articles-view' => [
        'pattern'  => 'articles/([\d]+)-(.+)\.html',
        'callback' => 'articles/view/id=$1',
        'priority' => 380,
    ],
    # добавление, редактирование
    'articles-action' => [
        'pattern'  => 'articles/(add|edit|status)(/|)',
        'callback' => 'articles/$1/',
        'priority' => 390,
    ],
    # поиск по категории
    'articles-search-cat' => [
        'pattern'  => 'articles/(.+)',
        'callback' => 'articles/search/cat=$1',
        'priority' => 400,
    ],
    # главная
    'articles-search' => [
        'pattern'  => 'articles(/|)',
        'callback' => 'articles/search',
        'priority' => 410,
    ],
];