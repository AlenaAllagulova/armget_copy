<?php

class ArticlesTags_ extends bff\db\Tags
{
    protected function initSettings()
    {
        $this->tblTags = TABLE_ARTICLES_TAGS;
        $this->postModeration = true;
        $this->tblTagsIn = TABLE_ARTICLES_ARTICLES_TAGS;
        $this->tblTagsIn_ItemID = 'article_id';
        $this->urlItemsListing = $this->adminLink('listing&tag=', 'articles');
    }

    public function formFront($nItemID, $aParams = array())
    {
        $aData = array(
            'tags'   => ($nItemID > 0 ? $this->tagsGet($nItemID) : array()),
            'params' => $aParams,
        );

        return $this->viewPHP($aData, 'form.tags');
    }
}