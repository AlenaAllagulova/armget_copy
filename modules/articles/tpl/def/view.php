<?php
/**
 * @var $this Articles
 */
tpl::includeJS('articles.view', false, 2);
$nUserID = User::id();
$aUser = $user;
$aUser['pro'] = 0;
$bAllowVoting = ! (! $nUserID || $nUserID == $user_id || $vote);
$owner = User::isCurrent($user_id);

?>
<div class="container">

    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

    <section class="l-mainContent">
        <div class="row">

            <!-- Project-->
            <div class="col-md-9" id="j-article-view">
                <div class="a-articleView">

                    <h1><?= $title ?></h1>

                    <?= $content  ?>

                    <? if($imgcnt): ?>
                        <div class="b-blogList-img">
                            <? foreach($images as $k => $v): ?>
                                <p><img src="<?= $v['i'][ ArticlesImages::szView ]; ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" /></p>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>

                    <div class="row mrgt20">

                        <div class="col-sm-8">
                            <? if( ! empty($tags)): foreach($tags as $v):?>
                                <a href="<?= Articles::url('tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                            <? endforeach; endif; ?>
                        </div>

                        <div class="col-sm-4">
                            <div class="a-social-icons text-right">
                                <? $aConfig = $this->configLoad(); ?>
                                <?= ! empty($aConfig['share_code']) ? $aConfig['share_code'] : '' ?>
                            </div>
                        </div>

                    </div>

                    <div class="a-user-info">
                        <div class="q-answer-rating a-answer-rating j-vote-block<?= ! $bAllowVoting ? ' q-answer-rating_na' : '' ?>">
                            <a href="#" class="j-vote" data-id="<?= $id ?>" data-t="good"><i class="fa fa-thumbs-up q-answer-good"></i></a>
                            <span class="q-answer-<?= $votes < 0 ? 'bad' : 'good' ?> j-votes"><?= $votes ?></span>
                            <a href="#" class="j-vote" data-id="<?= $id ?>" data-t="bad"><i class="fa fa-thumbs-down q-answer-bad"></i></a>
                        </div>
                        <ul class="l-item-features a-item-features">
                            <li><a href="<?= Users::url('profile', array('login' => $user['login'])) ?>"><img src="<?= UsersAvatar::url($user['user_id'], $user['avatar'], UsersAvatar::szSmall, $user['sex']) ?>" class="a-author-img" alt="<?= tpl::avatarAlt($aUser); ?>" /><?= tpl::userLink($aUser, 'text') ?></a><? if($user['pro']):?> <span class="pro">pro</span><? endif; ?></li>
                            <li><span class="b-date"><i class="fa fa-calendar-o"></i> <?= tpl::dateFormat($created) ?></span></li>
                            <li><span class="c-viewed"><i class="fa fa-eye"></i> <?= _t('', 'Просмотров: [total]', array('total'=>$views_total)); ?></span></li>
                        </ul>

                        <div class="clearfix"></div>
                    </div>

                </div>

                <? if($owner && $status != Articles::STATUS_DRAFT): ?>
                    <div class="l-spacer"></div>
                    <?= $this->articleStatusBlock($aData); ?>
                    <div class="p-profileOrder-controls-mobile">
                        <a href="<?= Articles::url('edit', array('id' => $id)) ?>"><i class="fa fa-edit c-link-icon"></i></a>
                        <a href="#" class="<?= $status != Articles::STATUS_ACTIVE ? 'hidden ' : '' ?>j-hide"><i class="fa fa-lock c-link-icon"></i></a>
                        <a href="#" class="<?= $status != Articles::STATUS_HIDDEN ? 'hidden ' : '' ?>j-show"><i class="fa fa-unlock c-link-icon"></i></a>
                        <a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i></a>
                    </div>

                    <div class="p-profileOrder-controls">
                        <a href="<?= Articles::url('edit', array('id' => $id)) ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                        <a href="#" class="<?= $status != Articles::STATUS_ACTIVE ? 'hidden ' : '' ?>j-hide"><i class="fa fa-lock c-link-icon"></i><?= _t('articles', 'Скрыть'); ?></a>
                        <a href="#" class="<?= $status != Articles::STATUS_HIDDEN ? 'hidden ' : '' ?>j-show"><i class="fa fa-unlock c-link-icon"></i><?= _t('articles', 'Показать'); ?></a>
                        <a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
                    </div>
                <? endif; ?>

                <? if($owner && $status == Articles::STATUS_DRAFT): ?>
                    <div class="l-funcBar">
                        <div class="container">
                            <div class="l-funcBar-in">
                                <div class="l-funcBar-l">
                                    <strong><?= _t('form', 'Данная статья находится в черновике'); ?></strong>
                                </div>
                                <div class="l-funcBar-r">
                                    <a href="#" class="btn btn-primary j-show"><?= _t('form', 'Опубликовать'); ?></a>
                                    <a href="<?= Articles::url('edit', array('id' => $id)) ?>" class="btn btn-info"><?= _t('form', 'Редактировать'); ?></a>
                                    <a href="#" class="btn btn-danger j-delete"><?= _t('form', 'Удалить'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endif; ?>

                <a name="comments"></a>
                <? if($status != Articles::STATUS_DRAFT):?>
                <div id="j-comments"><?= $this->viewPHP($aData, 'view.article.comments') ?></div>
                <? endif; ?>

            </div>

            <div class="col-md-3 visible-lg visible-md">
                <? # Баннер: Блоги: просмотр ?>
                <?= Banners::view('articles_view', array('pos'=>'right', 'cat'=>$cat_id)) ?>
            </div>

        </div>
    </section>

</div>
<script type="text/javascript">
    <? js::start() ?>
    jArticlesView.init(<?= func::php2js(array(
        'lang'   => array(
            'delete' => _t('articles', 'Удалить статью?'),
        ),
        'owner' => $owner,
        'id'    => $id,
    )) ?>);
    <? js::stop() ?>
</script>