<?php
/**
 * @var $this Articles
 */
tpl::includeJS('articles.search', false, 2);
?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <aside class="col-md-3">
                <?= $form ?>
            </aside>
            <div class="col-md-9 l-content-column" id="j-articles-search">
                <div class="j-list"><?= $list ?></div>
                <div class="j-pagination"><?= $pgn ?></div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    <? js::start() ?>
    jArticlesSearch.init(<?= func::php2js(array(
        'lang'    => array(),
        'ajax'    => true,
    )) ?>);
    <? js::stop() ?>
</script>