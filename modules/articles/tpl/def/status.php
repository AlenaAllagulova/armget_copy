<?php
?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <? if ($status == Articles::STATUS_DRAFT) { ?>
                    <h1 class="small"><?= _t('articles', 'Ваша статья сохранена в черновики.') ?></h1>
                    <p><a href="<?= Articles::url('view', array('id' => $id, 'keyword' => $keyword)) ?>"><?= _t('articles', 'Просмотреть статью') ?></a></p>
                    <p><a href="<?= Articles::url('add') ?>"><?= _t('articles', 'Добавить еще одну статью') ?></a></p>
                <? } else if (Articles::premoderation()) { # включена премодерация ?>
                    <h1 class="small"><?= _t('articles', 'Ваша статья будет опубликована после проверки модератором') ?></h1>
                    <p>
                       <?= _t('articles', 'Ваша статья сохранена, но еще не опубликована.') ?><br />
                       <?= _t('articles', 'Как только модератор проверит корректность введенных вами данных, статья появится в списке.') ?><br />
                       <?= _t('articles', '<a [link_view]>Посмотреть статью</a>', array('link_view' =>'href="'.Articles::url('view', array('id'=>$id, 'keyword'=>$keyword )).'"')) ?>
                    </p>
                    <p><a href="<?= Articles::url('add') ?>"><?= _t('articles', 'Добавить еще одну статью') ?></a></p>
                <? } else { ?>
                    <h1 class="small"><?= _t('articles', 'Ваша статья успешно опубликована.') ?></h1>
                    <p><a href="<?= Articles::url('add') ?>"><?= _t('articles', 'Добавить еще одну статью') ?></a></p>
                <? } ?>
            </div>
        </div>
    </section>
</div>