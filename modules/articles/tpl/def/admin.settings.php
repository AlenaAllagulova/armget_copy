<?php
$aTabs = array(
    'social'  => 'Поделиться',
);
?>
<?= tplAdmin::blockStart(_t('articles','Articles').' / '._t('','Settings'), false, array('id'=>'ArticlesSettingsFormBlock')); ?>

<form name="ArticlesSettingsForm" id="ArticlesSettingsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="edit" />
    <input type="hidden" name="save" value="1" />
    <div class="tabsBar" id="ArticlesSettingsFormTabs">
        <? foreach($aTabs as $k=>$v) { ?>
            <span class="tab<? if($k == 'social') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
        <? } ?>
    </div>
    <!-- таб: Поделиться -->
    <div class="j-tab j-tab-info">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1"><span class="field-title">Блок поделиться:</span></td>
            </tr>
            <tr>
                <td class="row2">
                    <textarea name="share_code" rows="10"><?= ! empty($aData['share_code']) ? HTML::escape($aData['share_code']) : '' ?></textarea>
                </td>
            </tr>
        </table>
    </div>

    <div style="margin-top: 10px;">
        <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jArticlesSettingsForm.save(false);" />
        <div id="ArticlesSettingsFormProgress" class="progress" style="display: none;"></div>
        <div class="clearfix"></div>
    </div>

</form>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
    var jArticlesSettingsForm =
        (function(){
            var $progress, $form, f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#ArticlesSettingsFormProgress');
                $form = $('#ArticlesSettingsForm');
                f = $form.get(0);

                // tabs
                $form.find('#ArticlesSettingsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });
            });

            return {
                save: function()
                {
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                        }
                    }, $progress);
                }
            };
        }());
</script>