<?php
/** @var $this Articles */
if( ! isset($cat_id1)) $cat_id1 = 0;
if( ! isset($cat_id2)) $cat_id2 = 0;
?>
    <? if(User::id()): ?>
    <div class="pdb30">
         <a href="<?= Articles::url('add') ?>" class="btn btn-primary btn-block"><i class="fa fa-plus-circle hidden-sm"></i> <?= _t('articles', 'Добавить статью'); ?></a>
    </div>
    <? endif; ?>
    <button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#j-articles-search-form-block"><i class="fa fa-cog pull-right"></i><?= _t('', 'Фильтр'); ?></span></button>
    <div class="l-leftColumn l-borderedBlock collapse" id="j-articles-search-form-block">
        <form action="" method="get" id="j-articles-search-form">
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
    
            <a href="#j-articles-categories" data-toggle="collapse" data-parent="#accordion" class="h6 active"><i class="fa fa-chevron-down pull-right"></i><?= _t('articles', 'Категории'); ?></a>
            <div class="collapse in j-collapse" id="j-articles-categories">
                <ul class="l-left-categories l-inside visible-md visible-lg">
                    <? foreach($cats as $v): ?>
                        <li<?= $v['id'] == $cat_id1 ? ' class="opened"' : ''?>><a href="<?= $v['url']; ?>" data-subs="<?= $v['subs'] ?>" class="j-cat-title"><? if ($v['subs']){ ?><i class="fa fa-caret-<?= $v['id'] == $cat_id1 ? 'down' : 'right'?>"></i> <? } ?><?= $v['title'] ?></a>
                            <? if( ! empty($v['sub'])): ?>
                                <ul<?= $v['id'] != $cat_id1 ? ' class="hidden"' : ''?>>
                                    <li<?= $v['id'] == $cat_id1 && $cat_id2 == 0 ? ' class="checked"' : '' ?>><a href="<?= $v['url'] ?>"><?= _t('', 'Все'); ?></a></li>
                                    <? foreach($v['sub'] as $vv): ?>
                                        <li<?= $vv['id'] == $cat_id2 ? ' class="checked"' : '' ?>><a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a></li>
                                    <? endforeach; ?>
                                </ul>
                            <? endif; ?>
                        </li>
                    <? endforeach; ?>
                </ul>
    
                <div class="collapse in" id="j-mobile-cats">
                    <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                        <? foreach($cats as $v): ?>
                            <li><a href="<?= $v['url'] ?>" class="j-mobile-cat" data-id="<?= $v['id']; ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
    
                <? foreach($cats as $v): ?>
                    <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                        <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                            <li class="active">
                                <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= _t('form', 'Вернуться назад'); ?></span><br>
                                    <?= $v['title'] ?>
                                </a>
                                <ul>
                                    <li<?= $v['id'] == $cat_id1 && $cat_id2 == 0 ? ' class="checked"' : '' ?>>
                                        <a href="<?= $v['url'] ?>"><?= _t('', 'Все'); ?></a>
                                    </li>
                                    <? foreach($v['sub'] as $vv): ?>
                                        <li<?= $vv['id'] == $cat_id2 ? ' class="checked"' : '' ?>>
                                            <a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                <? endforeach; ?>
    
            </div>
        </form>
    </div>

    <div class="visible-md visible-lg">
        <? if( ! empty($tags)): ?>
        <h5><?= _t('articles', 'Темы статей'); ?></h5>
        <div class="a-article-menu">
            <? foreach($tags as $v): if( ! $v['items']) continue; ?>
                <a href="<?= Articles::url('tag', array('tag' => $v['tag'], 'id' => $v['id'])) ?>" class="l-tag<?= ($tag_id == $v['id'] ? ' active' : '') ?>"><?= $v['tag'] ?> <small><?= $v['items'] ?></small></a>
            <? endforeach; ?>
        </div>
        <? endif; ?>

        <?= $this->best(); ?>

        <? # Баннер: Статьи: список ?>
        <?= Banners::view('articles_list', array('pos'=>'left', 'cat'=>$cat_id1)) ?>

    </div>