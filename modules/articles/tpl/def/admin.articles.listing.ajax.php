<?php
/**
 * @var $this Articles
 */
foreach ($list as $k=>&$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $id ?></td>
        <td class="left">
            <a href="<?= Articles::url('view', array('id' => $id, 'keyword' => $v['keyword'], 'adminView' => md5($v['user_id'].$v['id'].$v['keyword']))) ?>" class="but linkout" target="_blank"></a>
            <?= $v['title'] ?>
        </td>
        <td><?= tpl::date_format2($v['created'], true, true) ?></td>
        <td>
            <a class="but edit article-edit" title="<?= _t('', 'Edit') ?>" href="#" data-id="<?= $id ?>"></a>
            <a class="but del article-del" title="<?= _t('', 'Delete') ?>" href="#" data-id="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="4">
            ничего не найдено
        </td>
    </tr>
<? endif;