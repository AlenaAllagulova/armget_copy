<?php
if( ! empty($list)): ?>
<h5 class="mrgb5"><?= _t('articles', 'Популярные авторы'); ?></h5>
<ul class="l-leftCol-freelancers media-list">
    <? foreach($list as $v):?>
    <li class="media">
        <a href="<?= Users::url('profile', array('login' => $v['login'])) ?>" class="pull-left">
            <?= tpl::userAvatar($v) ?>
        </a>
        <div class="media-body">
            <?= tpl::userLink($v, 'no-login'); ?>
            <p>
                <strong><?= tpl::declension($v['articles_cnt'], _t('articles', 'статья;статьи;статей')) ?></strong>
                <?= tpl::declension($v['votes'], _t('articles', 'лайк;лайка;лайков')) ?>
            </p>
        </div>
    </li>
    <? endforeach; ?>
</ul>
<? endif;

