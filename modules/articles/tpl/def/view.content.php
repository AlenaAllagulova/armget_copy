<?php
/**
 * @var $this Articles
 */
use bff\db\Publicator;
$galleryIndex = 0;
$useMap = false;
if( ! empty($aData['b']))
{
    foreach($aData['b'] as $v)
    {
        switch($v['type'])
        {
            case Publicator::blockTypeText:
            {
                ?><?= $v['text'][LNG]; ?><?
            } break;
            case Publicator::blockTypeSubtitle:
            {
                $size = 'h' . $v['size'];
                ?><<?= $size ?>><?= $v['text'][LNG]; ?></<?= $size ?>><?
            }break;
            case Publicator::blockTypePhoto:
            {
                ?><p> <img src="<?= $v['url'][Publicator::szView] ?>" <?= ! empty($v['alt'][LNG]) ? 'alt="'.$v['alt'][LNG].'" title="'.$v['alt'][LNG].'"' : '' ?>> </p><?
            } break;
            case Publicator::blockTypeGallery:
            {
                $galleryIndex++;
                ?>
                <p>
                <div class="fotorama">
                    <? foreach($v['p'] as $gp){ ?><img src="<?= $gp['url'][Publicator::szView] ?>" <?= ! empty($gp['alt'][LNG]) ? 'alt="'.$gp['alt'][LNG].'" title="'.$gp['alt'][LNG].'"' : '' ?>/><? } ?>
                </div>
                </p>
                <?
            } break;
            case Publicator::blockTypeVideo:
            {
                ?><p>
                <object width="<?= $this->sett['video_width']; ?>" height="<?= $this->sett['video_height']; ?>">
                    <param name="allowfullscreen" value="true" />
                    <param name="allowscriptaccess" value="always" />
                    <param name="wmode" value="transparent" />
                    <param name="movie" value="<?= $v['video']; ?>" />
                    <embed src="<?= $v['video']; ?>" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" width="<?= $this->sett['video_width']; ?>" height="<?= $this->sett['video_height']; ?>"></embed>
                </object>
                </p><?
            } break;
            case Publicator::blockTypeMap:{
                $useMap = true;
                ?>
                <div class="l-mapView mrgt15">
                    <div class="map-google j-map-publicator-view" data-lat="<?= (!empty($v['lat']) ? $v['lat'] : ''); ?>" data-lon="<?= (!empty($v['lon']) ? $v['lon'] : ''); ?>" data-ballon="<?= HTML::escape($v['text'][LNG]) ?>" style="height: 350px;"></div>
                </div>
                <?
            } break;
        }
    }
}
if ($galleryIndex) {
    tpl::includeJS('fotorama/fotorama');
    tpl::includeCSS('/js/fotorama/fotorama', false);
}
if ($useMap) {
    Geo::mapsAPI();
?>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    $('.j-map-publicator-view').each(function(){
        var $bl = $(this);
        if($bl.hasClass('i')) return;
        $bl.addClass('i');
        var lat = $bl.data('lat');
        var lon = $bl.data('lon');
        app.map($bl.get(0), [lat, lon], function(map){
            if (this.isYandex()) {
                var myPlacemark = new ymaps.Placemark(map.getCenter(), {
                    draggable: false,
                    balloonContent: '<div><i class="fa fa-map-marker"></i> '+$bl.data('ballon')+'</div>'
                });
                map.geoObjects.add(myPlacemark);
            } else if (this.isGoogle()) {
                var pos = new google.maps.LatLng(lat, lon);
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    draggable: false
                });
                var infoWindow = new google.maps.InfoWindow({
                    position: pos,
                    content: '<div><i class="fa fa-map-marker"></i> '+$bl.data('ballon')+'</div>'
                });
                marker.addListener('click', function(){
                    infoWindow.open(map);
                });
            }
        }, {zoom: 12});
    });
});
<? js::stop(); ?>
</script>
<? }