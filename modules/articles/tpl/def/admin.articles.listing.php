<?php
/**
 * @var $this Articles
 */

?>
<?= tplAdmin::blockStart('Добавление статьи', false, array('id'=>'ArticlesArticlesFormBlock','style'=>'display:none;')); ?>
<div id="ArticlesArticlesFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('articles','Articles').' / Список', true, array('id'=>'ArticlesArticlesListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>'+ добавить статью','class'=>'ajax','onclick'=>'return jArticlesArticlesFormManager.action(\'add\',0);'),
    array(
        array('title'=>_t('', 'пересчет счетчиков'), 'onclick'=>"return bff.confirm('sure', {r:'".$this->adminLink(bff::$event.'&act=dev-counters-update')."'})", 'icon'=>'icon-refresh', 'debug-only'=>true),
    )
); ?>
<?
$aTabs = array(
    0 => array('t'=>'Активные'),
    1 => array('t'=>'Скрытые'),
    2 => array('t'=>'На модерации', 'counter'=>config::get('articles_items_moderating', 0)),
    3 => array('t'=>'Черновики'),
    4 => array('t'=>'Заблокированные'),
    5 => array('t'=>'Все'),
);
?>
<div class="tabsBar" id="ArticlesArticlesListTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab <? if($f['tab']==$k) { ?>tab-active<? } ?>"><a href="#" onclick="return jArticlesArticlesList.onTab(<?= $k ?>,this);"><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
    <? } ?>
    <div id="ArticlesArticlesProgress" class="progress" style="display: none;"></div>
</div>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="ArticlesArticlesListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>" />
        <input type="hidden" name="order" value="<?= $f['order'] ?>" />
        <input type="hidden" name="tag" id="ArticlesListFiltersTagId" value="<?= $f['tag'] ?>" />

        <div class="left">
            <div class="left">
                <select name="cat" style="width:170px;"  onchange="jArticlesArticlesList.refresh();"><?= $cats ?></select>
                <label class="relative" style="margin-left: 10px;">
                    <input type="text" class="autocomplete input-medium" id="ArticlesListFiltersTagTitle" placeholder="тег" value="<?= ($f['tag']>0 ? $tag['tag'] : '') ?>" />
                </label>
            </div>
            <div class="left" style="margin-left:8px;">
                <input style="width:150px;" type="text" maxlength="150" name="title" placeholder="Название или ID" value="<?= HTML::escape($f['title']) ?>" />
                <input type="text" style="width:155px;" maxlength="150" name="user" placeholder="<?= _t('', 'ID / логин / E-mail пользователя') ?>" value="<?= HTML::escape($f['user']) ?>" />
                <input type="button" class="btn btn-small button cancel" style="margin-left: 8px;" onclick="jArticlesArticlesList.submit(false);" value="<?= _t('', 'search') ?>" />
            </div>
            <div class="left" style="margin-left: 8px;"><a class="ajax cancel" onclick="jArticlesArticlesList.submit(true); return false;"><?= _t('', 'reset') ?></a></div>
            <div class="clear"></div>
        </div>
        <div class="right">

        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="ArticlesArticlesListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">
            <a href="javascript: jArticlesArticlesList.onOrder('id');" class="ajax">ID</a>
            <div class="order-<?= $f['order_dir'] ?>" <? if($f['order_by']!='id') { ?>style="display:none;"<? } ?> id="articles-order-id"></div>
        </th>
        <th class="left"><?= _t('', 'Title') ?></th>
        <th width="135">
            <a href="javascript: jArticlesArticlesList.onOrder('created');" class="ajax"><?= _t('', 'Created') ?></a>
            <div class="order-<?= $f['order_dir'] ?>" <? if($f['order_by']!='created') { ?>style="display:none;"<? } ?> id="articles-order-created"></div>
        </th>
        <th width="135"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="ArticlesArticlesList">
    <?= $list ?>
    </tbody>
</table>
<div id="ArticlesArticlesListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
var jArticlesArticlesFormManager = (function(){
    var $progress, $block, $blockCaption, $formContainer, process = false;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $formContainer = $('#ArticlesArticlesFormContainer');
        $progress = $('#ArticlesArticlesProgress');
        $block = $('#ArticlesArticlesFormBlock');
        $blockCaption = $block.find('span.caption');

        <? if( ! empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jArticlesArticlesList.toggle(false);
            if(jArticlesArticlesForm) jArticlesArticlesForm.onShow();
        } else {
            jArticlesArticlesList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? 'Добавление статьи' : 'Редактирование статьи'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
            } else {
                jArticlesArticlesList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        <? $tab = $this->input->get('ftab', TYPE_NOTAGS); ?>
        params = $.extend(params || {}, {act:type<?= ! empty($tab) ? ',ftab:\''.$tab.'\'' : '' ?>});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jArticlesArticlesList =
    (function()
    {
        var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, tab = <?= $f['tab'] ?>;
        var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';
        var orders = <?= func::php2js($orders) ?>, orderby = '<?= $f['order_by'] ?>';

        $(function(){
            $progress  = $('#ArticlesArticlesProgress');
            $block     = $('#ArticlesArticlesListBlock');
            $list      = $block.find('#ArticlesArticlesList');
            $listTable = $block.find('#ArticlesArticlesListTable');
            $listPgn   = $block.find('#ArticlesArticlesListPgn');
            filters    = $block.find('#ArticlesArticlesListFilters').get(0);

            $list.delegate('a.article-edit', 'click', function(){
                var id = intval($(this).data('id'));
                if(id>0) jArticlesArticlesFormManager.action('edit',id);
                return false;
            });

            $list.delegate('a.article-toggle', 'click', function(){
                var id = intval($(this).data('id'));
                var type = $(this).data('type');
                if(id>0) {
                    var params = {progress: $progress, link: this};
                    bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                }
                return false;
            });

            $list.delegate('a.article-del', 'click', function(){
                var id = intval($(this).data('id'));
                if(id>0) del(id, this);
                return false;
            });

            $block.find('#ArticlesListFiltersTagTitle').autocomplete( ajaxUrl+'tags-autocomplete',
                {valueInput: '#ArticlesListFiltersTagId', placeholder: 'Тег', onSelect: function(){
                    jArticlesArticlesList.submit(false);
                }});

            $(window).bind('popstate',function(){
                if('state' in window.history && window.history.state === null) return;
                var loc = document.location;
                var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                if( actForm!=null ) {
                    var actId = /id=([\d]+)/.exec(loc.search.toString());
                    jArticlesArticlesFormManager.action(actForm[1], actId && actId[1]);
                } else {
                    jArticlesArticlesFormManager.action('cancel');
                    updateList(false);
                }
            });

        });

        function isProcessing()
        {
            return processing;
        }

        function del(id, link)
        {
            bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
            return false;
        }

        function updateList(updateUrl)
        {
            if(isProcessing()) return;
            var f = $(filters).serialize();
            bff.ajax(ajaxUrl, f, function(data){
                if(data) {
                    $list.html( data.list );
                    $listPgn.html( data.pgn );
                    if(updateUrl !== false && bff.h) {
                        window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                    }
                }
            }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
        }

        function setPage(id)
        {
            filters.page.value = intval(id);
        }

        return {
            submit: function(resetForm)
            {
                if(isProcessing()) return false;
                setPage(1);
                if(resetForm) {
                    filters['cat'].value = 0;
                    filters['title'].value = '';
                    filters['tag'].value = 0;
                    filters['user'].value = '';
                    $block.find('#ArticlesListFiltersTagTitle').val('');
                }
                updateList();
            },
            page: function (id)
            {
                if(isProcessing()) return false;
                setPage(id);
                updateList();
            },
            onOrder: function(by)
            {
                if(isProcessing() || !orders.hasOwnProperty(by))
                    return;

                orders[by] = (orders[by] == 'asc' ? 'desc' : 'asc');
                $('#articles-order-'+orderby).hide();
                orderby = by;
                $('#articles-order-'+orderby).removeClass('order-asc order-desc').addClass('order-'+orders[by]).show();

                filters.order.value = orderby+'-'+orders[by];
                setPage(1);

                updateList();
            },
            onTab: function(tabNew, link)
            {
                if(isProcessing() || tabNew == tab) return false;
                setPage(1);
                tab = filters.tab.value = tabNew;
                updateList();
                $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
                return false;
            },
            refresh: function(resetPage, updateUrl)
            {
                if(resetPage) setPage(0);
                updateList(updateUrl);
            },
            toggle: function(show)
            {
                if(show === true) {
                    $block.show();
                    if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                }
                else $block.hide();
            }
        };
    }());
</script>