<?php
/** @var $this Articles */
?>
<? if(!$moderated && Articles::premoderation()): ?><div class="alert alert-warning" role="alert"><?= _t('articles', 'Ожидает проверки модератора') ?></div><? endif; ?>
<? if($status == Articles::STATUS_BLOCKED): ?>
    <div class="alert alert-danger" role="alert"><b><?= _t('articles', 'Заблокирована модератором.') ?></b>
    <? if( ! empty($blocked_reason)): ?><br /><?= _t('', 'Причина блокировки:'); ?> <?= tpl::blockedReason($blocked_reason) ?><? endif; ?>
    </div>
<? endif; ?>