<?php
$nUserID = User::id();
if( ! empty($list)): ?>
    <ul class="a-articlesList media-list">
        <? foreach($list as $v):
            $bAllowVoting = ! (! $nUserID || $nUserID == $v['user_id'] || $v['vote']);
            ?>
        <li class="media">
            <div class="media-body">
                <div class="b-item-info">
                    <header class="a-item-name">
                        <h1><a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a></h1>
                    </header>

                    <? if($v['tags']): $t = explode(',', $v['tags']); ?>
                        <div>
                            <? foreach($t as $vv): if(empty($tags[ $vv ])) continue;?>
                                <a href="<?= Articles::url('tag', array('id' => $tags[ $vv ]['id'], 'tag' => $tags[ $vv ]['tag'])) ?>" class="l-tag<?= ($tag_id == $vv ? ' active' : '') ?>"><?= $tags[ $vv ]['tag'] ?></a>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>

                    <article class="b-blogList-description">
                        <?= $v['content_preview'] ?>
                    </article>

                    <? if($v['imgcnt']): ?>
                        <div class="b-blogList-img">
                            <img src="<?= $v['img_v'] ?>" alt="<?= tpl::imageAlt(array('t' => $v['title'])); ?>" />
                        </div>
                    <? endif; ?>

                    <ul class="l-item-features a-item-features">
                        <li><?= tpl::userLink($v) ?></li>
                        <li><span class="b-date"><i class="fa fa-calendar-o"></i> <?= tpl::dateFormat($v['created']) ?></span></li>
                        <? if($v['comments_enabled']): ?><li><a href="<?= $v['url_view'].'#comments' ?>"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['comments_cnt'], _t('', 'комментарий;комментария;комментариев'))?></span></a></li><? endif; ?>
                        <li class="q-answer-rating j-vote-block<?= ! $bAllowVoting ? ' q-answer-rating_na' : '' ?>">
                            <a href="#" class="j-vote" data-id="<?= $v['id'] ?>" data-t="good"><i class="fa fa-thumbs-up q-answer-good"></i></a>
                            <span class="q-answer-<?= $v['votes'] < 0 ? 'bad' : 'good' ?> j-votes"><?= $v['votes'] ?></span>
                            <a href="#" class="j-vote" data-id="<?= $v['id'] ?>" data-t="bad"><i class="fa fa-thumbs-down q-answer-bad"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('articles', 'Статьи не найдены'); ?></div>
<? endif;