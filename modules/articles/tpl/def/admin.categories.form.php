<?php
    /**
     * @var $this Articles
     */
    $aData = HTML::escape($aData, 'html', array('keyword'));
    $aTabs = array(
        'info' => 'Основные',
        'seo' => 'SEO',
    );
    $edit = ! empty($id);
?>
<form name="ArticlesCategoriesForm" id="ArticlesCategoriesForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
<input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
<input type="hidden" name="save" value="1" />
<input type="hidden" name="id" value="<?= $id ?>" />
<div class="tabsBar" id="ArticlesCategoriesFormTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab<? if($k == 'info') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
    <? } ?>
</div>
<div class="j-tab j-tab-info">
    <table class="admtbl tbledit">
        <tr class="required check-select">
            <td class="row1 field-title" width="120">Основной раздел<span class="required-mark">*</span>:</td>
            <td class="row2">
                <? if ($edit) { ?>
                    <input type="hidden" name="pid" id="category-pid" value="<?= $pid ?>" />
                    <span class="bold"><?= $pid_path ?></span>
                <? } else { ?>
                    <select name="pid" id="category-pid"><?= $this->model->categoriesOptions($pid, false, 1) ?></select>
                <? } ?>
            </td>
        </tr>
        <?= $this->locale->buildForm($aData, 'categories-item','
<tr class="required">
    <td class="row1"><span class="field-title">Название<span class="required-mark">*</span>:</span></td>
    <td class="row2">
        <input class="stretch <?= $key ?>" type="text" id="category-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
    </td>
</tr>
'); ?>
        <tr>
            <td class="row1 field-title">URL-Keyword<span class="required-mark">*</span>:<br /><a href="#" onclick="return bff.generateKeyword('#category-title-<?= LNG ?>', '#category-keyword');" class="ajax desc small"><?= _t('', 'generate') ?></a></td>
            <td class="row2">
                <input class="stretch" type="text" id="category-keyword" name="keyword" value="<?= $keyword ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('', 'Enabled') ?>:</span></td>
            <td class="row2">
                <label class="checkbox"><input type="checkbox" id="category-enabled" name="enabled"<? if($enabled){ ?> checked="checked"<? } ?> /></label>
            </td>
        </tr>
    </table>
</div>
<div class="j-tab j-tab-seo hidden">
    <?= SEO::i()->form($this, $aData, 'listing-category'); ?>
</div>
<div style="margin-top: 10px;">
    <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jArticlesCategoriesForm.save(false);" />
    <? if ($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jArticlesCategoriesForm.save(true);" /><? } ?>
    <? if ($edit) { ?><input type="button" onclick="jArticlesCategoriesForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
    <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jArticlesCategoriesFormManager.action('cancel');" />
</div>
</form>

<script type="text/javascript">
    var jArticlesCategoriesForm =
        (function(){
            var $progress, $form, formChk, id = parseInt(<?= $id ?>);
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#ArticlesCategoriesFormProgress');
                $form = $('#ArticlesCategoriesForm');

                // tabs
                $form.find('#ArticlesCategoriesFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });
            });
            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('Запись успешно удалена');
                                jArticlesCategoriesFormManager.action('cancel');
                                jArticlesCategoriesList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    if( ! formChk.check(true) ) return;
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                            if(returnToList || ! id) {
                                jArticlesCategoriesFormManager.action('cancel');
                                jArticlesCategoriesList.refresh( ! id);
                            }
                        }
                    }, $progress);
                },
                onShow: function ()
                {
                    formChk = new bff.formChecker($form);
                }
            };
        }());
</script>