<?php

/**
 * Права доступа группы:
 *  - articles: Статьи
 *      - items: Управление статьями (список, добавление, редактирование, удаление)
 *      - items-moderate: Модерация статей (блокирование/одобрение/продление/активация)
 *      - items-comments: Управление комментариями
 *      - tags: Управление тегами (список, добавление, редактирование, удаление)
 *      - categories: Управление категориями (список, добавление, редактирование, удаление)
 *      - settings: Настройки
 *      - seo: SEO
 */
class Articles_ extends ArticlesBase
{
    public function listing()
    {
        if (!($this->haveAccessTo('items') || $this->haveAccessTo('items-moderate')))
            return $this->showAccessDenied();

        $accessManage = $this->haveAccessTo('items');

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
        }

        $sAct = $this->input->postget('act',TYPE_STR);
        if ( ! empty($sAct) || Request::isPOST())
        {
            $aResponse = array();
            switch ($sAct)
            {
                case 'add':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateArticleData(0, $bSubmit, $publicator);
                    if ($bSubmit)
                    {
                        if ($this->errors->no()) {

                            $aData['moderated'] = 1;
                            $nArticleID = $this->model->articleSave(0, $aData);
                            if ($nArticleID > 0) {
                                $this->articleTags()->tagsSave($nArticleID);
                                if ($publicator) {
                                    # переносим фотографии в постоянную папку
                                    $publicator->dataUpdate($aData['content'], $nArticleID);
                                }
                                # загружаем изображения
                                if (static::imagesLimit()) {
                                    # сохраняем / загружаем изображения
                                    # перемещаем из tmp-директории в постоянную
                                    $this->articlesImages($nArticleID)->saveTmp('img');
                                }
                            }
                        }
                    } else {
                        $aData['id'] = 0;
                        $aData['cats'] = $this->model->categoriesOptionsByLevel( $this->model->categoryParentsID($aData['cat_id']), array('empty'=>'Выбрать') );
                        $aData['images'] = array();
                        $aData['imgcnt'] = 0;
                        $aData['img'] = $this->articlesImages(0);
                        $aData['act'] = $sAct;
                        $aData['publicator'] = $publicator;
                        $aResponse['form'] = $this->viewPHP($aData, 'admin.articles.form');
                    }

                } break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nArticleID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nArticleID) { $this->errors->unknownRecord(); break; }

                    if ($bSubmit)
                    {
                        if (!$accessManage) {
                            $this->errors->accessDenied(); break;
                        }
                        $aData = $this->validateArticleData($nArticleID, $bSubmit, $publicator);
                        if ($this->errors->no()) {

                            $this->model->articleSave($nArticleID, $aData);
                            $this->articleTags()->tagsSave($nArticleID);
                        }
                        $aData['id'] = $nArticleID;
                    } else {
                        $aData = $this->model->articleData($nArticleID, array(), true);
                        if (empty($aData)) { $this->errors->unknownRecord(); break; }
                        $aData['cats'] = $this->model->categoriesOptionsByLevel( $this->model->categoryParentsID($aData['cat_id']), array('empty'=>'Выбрать') );
                        $aData['img'] = $this->articlesImages($nArticleID);
                        $aData['images'] = $aData['img']->getData($aData['imgcnt']);
                        if ($publicator && empty($aData['content_search'])) {
                            $aData['content'] = $publicator->textConvert($aData['content']);
                        }
                        $aData['act'] = $sAct;
                        $aData['publicator'] = $publicator;
                        $aResponse['form'] = $this->viewPHP($aData, 'admin.articles.form');
                    }

                } break;
                case 'toggle':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $nArticleID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nArticleID) { $this->errors->unknownRecord(); break; }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->articleToggle($nArticleID, $sToggleType);
                } break;
                case 'category-data':
                {
                    $nCategoryID = $aResponse['id'] = $this->input->post('cat_id', TYPE_UINT);
                    if (empty($nCategoryID)) { $this->errors->unknownRecord(); break; }

                    $aResponse['subs'] = $this->model->categorySubCount($nCategoryID);
                    if ($aResponse['subs'] > 0) {
                        $aResponse['cats'] = $this->model->categorySubOptions($nCategoryID, array('sel'=>0, 'empty'=>'Выбрать'));
                    }

                } break;
                case 'delete':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $nArticleID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nArticleID) { $this->errors->impossible(); break; }

                    $aData = $this->model->articleData($nArticleID, array(), true);
                    if (empty($aData)) { $this->errors->impossible(); break; }

                    $res = $this->articleDelete($nArticleID);
                    if ( ! $res) { $this->errors->impossible(); break; }

                } break;
                case 'user': # пользователь
                {
                    $sEmail = $this->input->post('q', TYPE_NOTAGS);
                    $sEmail = $this->input->cleanSearchString($sEmail);
                    $aUsers = Users::model()->usersList(array(
                            ':email'    => array(
                                (Users::model()->userEmailCrypted() ? 'BFF_DECRYPT(email)' : 'email') . ' LIKE :email',
                                ':email' => $sEmail . '%'
                            ),
                            'blocked'   => 0,
                            'activated' => 1,
                        ), array('user_id', 'email')
                    );
                    $this->autocompleteResponse($aUsers, 'user_id', 'email');
                } break;
                case 'img-upload': # изображения: загрузка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        $this->ajaxResponse(array('success' => false, 'errors' => $this->errors->get()), true, false, true);
                    }

                    $nArticleID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->articlesImages($nArticleID);
                    $mResult = $oImages->uploadQQ();
                    $aResponse = array('success' => ($mResult !== false && $this->errors->no()));

                    if ($mResult !== false) {
                        $aResponse = array_merge($aResponse, $mResult);
                        $aResponse = array_merge($aResponse,
                            $oImages->getURL($mResult,
                                array(ArticlesImages::szForm, ArticlesImages::szView),
                                empty($nArticleID)
                            )
                        );
                    }
                    $aResponse['errors'] = $this->errors->get();
                    $this->ajaxResponse($aResponse, true, false, true);
                }  break;
                case 'img-saveorder': # изображения: сохранение порядка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nArticleID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->articlesImages($nArticleID);
                    $img = $this->input->post('img', TYPE_ARRAY);
                    if (!$oImages->saveOrder($img, false, true)) {
                        $this->errors->impossible();
                    }
                }  break;
                case 'img-delete': # изображения: удаление
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nArticleID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->articlesImages($nArticleID);
                    $nImageID = $this->input->post('image_id', TYPE_UINT);
                    $sFilename = $this->input->post('filename', TYPE_STR);
                    if (!$nImageID && empty($sFilename)) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($nImageID) {
                        $oImages->deleteImage($nImageID);
                    } else {
                        $oImages->deleteTmpFile($sFilename);
                    }
                }  break;
                case 'img-delete-all': # изображения: удаление всех изображений
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nArticleID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->articlesImages($nArticleID);
                    if ($nArticleID) {
                        $oImages->deleteAllImages(true);
                    } else {
                        $sFilename = $this->input->post('filenames', TYPE_ARRAY_STR);
                        $oImages->deleteTmpFile($sFilename);
                    }
                }  break;
                case 'tags-suggest': # autocomplete.fb
                {
                    $sQuery = $this->input->postget('tag', TYPE_STR);
                    $this->articleTags()->tagsAutocomplete($sQuery);
                } break;
                case 'tags-autocomplete': # autocomplete
                {
                    $sQuery = $this->input->post('q', TYPE_STR);
                    $this->articleTags()->tagsAutocomplete($sQuery);
                } break;
                case 'dev-counters-update':
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }
                    $this->model->bestGenerate();
                    $this->moderationCounterUpdate();
                    $this->adminRedirect(($this->errors->no() ? Errors::SUCCESS : Errors::IMPOSSIBLE), bff::$event);
                } break;
                default: $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) $this->ajaxResponseForm($aResponse);
        }

        $f = array();
        $this->input->postgetm(array(
            'page'   => TYPE_UINT,
            'cat'    => TYPE_UINT,
            'tab'    => TYPE_INT,
            'tag'    => TYPE_UINT,
            'title'  => TYPE_NOTAGS,
            'user'   => TYPE_NOTAGS,
        ), $f);

        # формируем фильтр списка cтатьей
        $sql = array();
        $mPerpage = 15;
        $aData['pgn'] = '';

        $aData['orders'] = array('id'=>'asc','created'=>'desc',);
        $f += $this->prepareOrder($orderBy, $orderDirection, 'id-asc', $aData['orders']);
        $f['order'] = $orderBy.'-'.$orderDirection; $sqlOrder = "$orderBy $orderDirection";

        switch ($f['tab']) {
            case 0: # Активные
            {
                $sql['status'] = static::STATUS_ACTIVE;
                if (static::premoderation()) {
                    $sql[':mod'] = 'A.moderated > 0';
                }
            } break;
            case 1: # Скрытые
            {
                $sql['status'] = static::STATUS_HIDDEN;
            } break;
            case 2: # На модерации
            {
                $sql[':mod'] = 'A.moderated != 1';
            } break;
            case 3: # Черновики
            {
                $sql['status'] = static::STATUS_DRAFT;
            } break;
            case 4: # Заблокированные
            {
                $sql['status'] = static::STATUS_BLOCKED;
            } break;
            case 5: # Все
            {
            } break;
        }

        if ($f['cat'] > 0) {
            $sql[':cat_id'] = array('(A.cat_id2 = :cat OR A.cat_id1 = :cat)', ':cat'=>$f['cat']);
        }

        if ( ! empty($f['title'])) {
            $sql[':title'] = array('(A.id = '.intval($f['title']).' OR A.title LIKE :title)', ':title' => '%'.$f['title'].'%');
        }

        if ($f['tag'] > 0) {
            $aData['tag'] = $this->articleTags()->tagData($f['tag']);
            if (empty($aData['tag'])) {
                $f['tag'] = 0;
            } else {
                $sql[':tag'] = $f['tag'];
            }
        }

        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(A.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }

        if ($mPerpage!==false) {
            $nCount = $this->model->articlesListing($sql, true);
            $oPgn = new Pagination($nCount, $mPerpage, '#', 'jArticlesArticlesList.page('.Pagination::PAGE_ID.'); return false;');
            $oPgn->pageNeighbours = 6;
            $aData['pgn'] = $oPgn->view(array('arrows'=>false));
            $aData['list'] = $this->model->articlesListing($sql, false, $oPgn->getLimitOffset(), $sqlOrder);
        } else {
            $aData['list'] = $this->model->articlesListing($sql, false, '', $sqlOrder);
        }

        $aData['list'] = $this->viewPHP($aData, 'admin.articles.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list'=>$aData['list'],
                'pgn'=>$aData['pgn'],
            ));
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        $aData['cats'] = $this->model->categoriesOptions($f['cat'], array('Все категории'));
        tpl::includeJS(array('ui.sortable', 'qquploader', 'wysiwyg', 'autocomplete', 'autocomplete.fb'), true);

        $this->articleComments()->admListingIncludes();

        return $this->viewPHP($aData, 'admin.articles.listing');
    }

    public function article_status()
    {
        if ( ! $this->haveAccessTo('items-moderate')) {
            $this->errors->accessDenied();
        }


        $nArticleID = $this->input->post('id', TYPE_UINT);
        if (!$nArticleID) {
            $this->errors->unknownRecord();
        } else {
            $aData = $this->model->articleData($nArticleID, array(), true);
            if (empty($aData)) {
                $this->errors->unknownRecord();
            }
        }
        if ( ! $this->errors->no()) {
            $this->ajaxResponseForm();
        }

        $aResponse = array();
        switch ($this->input->postget('act', TYPE_STR))
        {
            case 'approve': # модерация: одобрение
            {
                $aUpdate = array(
                    'moderated' => 1
                );

                if ($aData['status'] == self::STATUS_BLOCKED) {
                    /**
                     * В случае если "Одобряем" заблокированную статью
                     * => значит она после блокировки была отредактирована пользователем
                     */
                    $aUpdate[] = 'status_prev = status';
                    $aUpdate['status'] = ( in_array($aData['status_prev'], array(
                        self::STATUS_ACTIVE,
                        self::STATUS_HIDDEN,
                    )) ? $aData['status_prev'] : self::STATUS_ACTIVE );
                    $aUpdate['status_changed'] = $this->db->now();
                }

                $res = $this->model->articleSave($nArticleID, $aUpdate);

                if (empty($res)) {
                    $this->errors->impossible();
                }

                # отправим пользователю письмо
                Users::sendMailTemplateToUser($aData['user_id'], 'article_approve', array(
                    'id' => $nArticleID,
                    'title' => $aData['title'],
                    'view_link' => static::url('view', array('id' => $nArticleID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);

                # обновляем счетчик статей "на модерации"
                $this->moderationCounterUpdate();
            }
            break;
            case 'activate': # модерация: активация
            {
                if ($aData['status'] != self::STATUS_HIDDEN) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_ACTIVE,
                    'status_changed' => $this->db->now(),
                    'moderated' => 1,
                );

                $res = $this->model->articleSave($nArticleID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик статей "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'deactivate': # модерация: деактивация
            {
                if ($aData['status'] != self::STATUS_ACTIVE) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_HIDDEN,
                    'status_changed' => $this->db->now(),
                    'moderated' => 1,
                );

                $res = $this->model->articleSave($nArticleID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик статей "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'block': # модерация: блокировка / разблокировка
            {
                /**
                 * Блокировка статьи (если уже заблокирована => изменение причины блокировки)
                 * @param string 'blocked_reason' причина блокировки
                 * @param integer 'id' ID статьи
                 */

                $bUnblock = $this->input->post('unblock', TYPE_UINT);
                $sBlockedReason = $this->input->postget('blocked_reason', TYPE_NOTAGS, array('len' => 1000, 'len.sys' => 'articles.blocked_reason.limit'));
                $bBlocked = ($aData['status'] == self::STATUS_BLOCKED);

                if ($aData['user_id']) {
                    $aUserData = Users::model()->userData($aData['user_id'], array('user_id', 'blocked'));
                    if (!empty($aUserData['blocked'])) {
                        $this->errors->set('Для блокировки/разблокировки статьи, разблокируйте аккаунт владельца данной статьи');
                        break;
                    }
                }

                $aUpdate = array(
                    'moderated'      => 1,
                    'blocked_reason' => $sBlockedReason,
                    'status_changed' => $this->db->now(),
                );

                $bBlockedResult = $bBlocked;
                if (!$bBlocked) { # блокируем
                    $aUpdate['status_prev'] = $aData['status'];
                    $aUpdate['status'] = self::STATUS_BLOCKED;
                    $bBlockedResult = true;
                } else {
                    if ($bUnblock) {
                        # разблокируем
                        switch ($aData['status_prev']) {
                            case self::STATUS_ACTIVE:
                            case self::STATUS_HIDDEN:
                                $aUpdate['status'] = $aData['status_prev'];
                                break;
                            case self::STATUS_BLOCKED:
                                $aUpdate['status'] = self::STATUS_HIDDEN;
                                break;
                        }
                        $aUpdate['status_prev'] = self::STATUS_BLOCKED;
                        $bBlockedResult = false;
                    }
                }

                $res = $this->model->articleSave($nArticleID, $aUpdate);
                if (!$res) {
                    $this->errors->impossible();
                    break;
                }
                # обновляем счетчик статей "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['blocked'] = $bBlockedResult;
                $aResponse['reason'] = $sBlockedReason;

                if ($bUnblock) {
                    # отправим пользователю письмо об одобрении
                    Users::sendMailTemplateToUser($aData['user_id'], 'article_approve', array(
                        'id' => $nArticleID,
                        'title' => $aData['title'],
                        'view_link' => static::url('view', array('id'=>$nArticleID, 'keyword'=>$aData['keyword'])),
                    ), Users::ENOTIFY_GENERAL);
                } else {
                    # отправим пользователю письмо о блокировке
                    Users::sendMailTemplateToUser($aData['user_id'], 'article_block', array(
                        'id' => $nArticleID,
                        'title' => $aData['title'],
                        'view_link' => static::url('view', array('id'=>$nArticleID, 'keyword'=>$aData['keyword'])),
                        'reason' => $sBlockedReason,
                    ), Users::ENOTIFY_GENERAL);
                }

            }
            break;
        }

        if ($this->errors->no()) {
            $aData = $this->model->articleData($nArticleID, array(), true);
            if ( ! empty($aData)) {
                $aData['only_form'] = true;
                $aResponse['html'] = $this->viewPHP($aData, 'admin.articles.form.status');
            }
        }
        $this->ajaxResponseForm($aResponse);
    }

    public function tags()
    {
        if (!$this->haveAccessTo('tags')) {
            return $this->showAccessDenied();
        }

        return $this->articleTags()->manage();
    }

    public function categories()
    {
        if ( ! $this->haveAccessTo('categories'))
            return $this->showAccessDenied();

        $sAct = $this->input->postget('act',TYPE_STR);
        if ( ! empty($sAct) || Request::isPOST())
        {
            $aResponse = array();
            switch ($sAct)
            {
                case 'add':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateCategoryData(0, $bSubmit);
                    if ($bSubmit)
                    {
                        if ($this->errors->no()) {

                            $nCategoryID = $this->model->categorySave(0, $aData);
                            if ($nCategoryID > 0) {
                            }
                        }
                    }

                    $aData['id'] = 0;

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                } break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nCategoryID) { $this->errors->unknownRecord(); break; }

                    if ($bSubmit)
                    {
                        $aData = $this->validateCategoryData($nCategoryID, $bSubmit);
                        if ($this->errors->no()) {

                            $this->model->categorySave($nCategoryID, $aData);
                        }
                        $aData['id'] = $nCategoryID;
                    } else {
                        $aData = $this->model->categoryData($nCategoryID, true);
                        if (empty($aData)) { $this->errors->unknownRecord(); break; }
                    }

                    $aData['pid_path'] = $this->model->categoryParentsTitle($nCategoryID);
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                } break;
                case 'expand':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nCategoryID) { $this->errors->unknownRecord(); break; }

                    $aData['list'] = $this->model->categoriesListing( array('pid'=>$nCategoryID) );
                    $aData['skip_norecords'] = false;
                    $aResponse['list'] = $this->viewPHP( $aData, 'admin.categories.listing.ajax' );
                    $aResponse['cnt']  = sizeof($aData['list']);
                } break;
                case 'toggle':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nCategoryID) { $this->errors->unknownRecord(); break; }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->categoryToggle($nCategoryID, $sToggleType);
                } break;
                case 'rotate':
                {
                    $this->model->categoriesRotate();
                } break;
                case 'delete':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nCategoryID) { $this->errors->impossible(); break; }

                    $aData = $this->model->categoryData($nCategoryID, true);
                    if (empty($aData)) { $this->errors->impossible(); break; }

                    $res = $this->model->categoryDelete($nCategoryID);
                    if ( ! $res) { $this->errors->impossible(); break; }
                    else {
                    }
                } break;
                default: $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) $this->ajaxResponseForm($aResponse);
        }

        $f = array();
        $this->input->postgetm(array(
            'page'   => TYPE_UINT,
        ), $f);

        # формируем фильтр списка категорий
        $sql = array();
        $sqlOrder = 'numleft';
        $aData['pgn'] = '';

        $sExpandState = $this->input->cookie(bff::cookiePrefix().'articles_categories_expand', TYPE_STR);
        $aExpandID = ( ! empty($sExpandState) ? explode('.', $sExpandState) : array());
        $aExpandID = array_map('intval', $aExpandID); $aExpandID[] = 1;
        $sql[] = 'pid IN ('.join(',', $aExpandID).')';

        $aData['list'] = $this->model->categoriesListing($sql, false, '', $sqlOrder);
        $aData['list'] = $this->viewPHP($aData, 'admin.categories.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list'=>$aData['list'],
                'pgn'=>$aData['pgn'],
            ));
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        return $this->viewPHP($aData, 'admin.categories.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nCategoryID ID категории или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateCategoryData($nCategoryID, $bSubmit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langCategories, $aData);
        $this->input->postm(array(
            'pid'       => TYPE_UINT,   # Основной раздел
            'keyword'   => TYPE_NOTAGS, # URL-Keyword
            'enabled'   => TYPE_BOOL,   # Включена
            'mtemplate' => TYPE_BOOL,   # Использовать базовый шаблон SEO
        ), $aData);

        if ($bSubmit)
        {
            # URL-Keyword
            $aData['keyword'] = $this->db->getKeyword($aData['keyword'], $aData['title'][LNG], TABLE_ARTICLES_CATEGORIES, $nCategoryID, 'keyword', 'id');
        } else {
            if (!$nCategoryID) {
                $aData['mtemplate'] = 1;
            }
        }
        return $aData;
    }

    /**
     * Валидация целостности NestedSets категорий
     */
    public function categories_nsvalid()
    {
        if ( ! FORDEV || ! BFF_DEBUG) return $this->showAccessDenied();
        return $this->model->treeCategories->validate(true);
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'share_code' => TYPE_STR,
            ), $aData);

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad();
        if( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

    public function comments_ajax()
    {
        if (!$this->haveAccessTo('items-comments')) {
            return $this->showAccessDenied();
        }

        $this->articleComments()->admAjax();
    }

    public function comments_mod()
    {
        if (!$this->haveAccessTo('items-comments')) {
            return $this->showAccessDenied();
        }

        return $this->articleComments()->admListingModerate(15);
    }

}