<?php
    /**
     * Кабинет пользователя: Сообщения
     * @var $this InternalMail
     */
    tpl::includeJS('internalmail.my', false, 2);
    tpl::includeJS('users.note', false, 2);
    $f['qq'] = HTML::escape($f['qq']);
?>
    <div class="container" id="j-my-messages-block">
        <section class="l-mainContent">
            <div class="row">
                <form action="" class="form-search" id="j-my-messages-form-list">
                    <input type="hidden" name="f" value="<?= $f['f'] ?>" id="j-my-messages-folder-value" />
                    <input type="hidden" name="page" value="<?= $f['page'] ?>" />
                    <input type="hidden" name="qq" value="<?= HTML::escape($f['qq']) ?>" />
                </form>
                <form action="" method="post" id="j-my-messages-form-act">
                    <aside class="col-md-3 hidden-xs hidden-sm">
                        <div class="l-borderedBlock p-profileColumn">
                            <div class="p-profile-collapse">
                                <div class="l-inside">
                                    <div class="input-group">
                                         <input type="text" name="new_folder_name" maxlength="25" class="form-control input-sm"  value="" placeholder="<?= _t('internalmail', 'Новая папка'); ?>" />
                                         <span class="input-group-btn">
                                             <button class="btn btn-primary btn-sm j-make-folder" type="button"><?= _t('', 'Создать'); ?></button>
                                         </span>
                                    </div>
                                </div>

                                <div class="l-inside">
                                    <ul class="i-imail-folders">
                                        <? $bCustom = false; foreach($folders as $k => $v): if( ! empty($v['custom'])) { $bCustom = true; continue; } ?>
                                            <li<?= $f['f'] == $k ? ' class="active"' : '' ?>><a href="#" class="<?= ! empty($v['class']) ? $v['class'] : '' ?> j-folder-select" data-id="<?= $k ?>"><i class="fa <?= $f['f'] == $k ? 'fa-folder-open' : 'fa-folder' ?>"></i><?= $v['title'] ?> </a></li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>

                                <? if($bCustom): ?>
                                <div class="l-inside">
                                    <ul class="i-imail-folders">
                                        <? foreach($folders as $k => $v): if(empty($v['custom'])) continue; ?>
                                        <li<?= $f['f'] == $k ? ' class="active"' : '' ?>><a href="#" class="<?= ! empty($v['class']) ? $v['class'] : '' ?> j-folder-select" data-id="<?= $k ?>"><i class="fa <?= $f['f'] == $k ? 'fa-folder-open' : 'fa-folder' ?>"></i><span class="j-title"><?= HTML::escape($v['title']) ?></span></a>
                                        <a href="#" class="j-edit-folder" data-id="<?= $k ?>"><i class="fa fa-edit show-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?= _t('form', 'Редактировать'); ?>" data-original-title="<?= _t('form', 'Редактировать'); ?>"></i></a>
                                        <a href="#" class="link-delete j-delete-folder" data-id="<?= $k ?>"><i class="fa fa-times show-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?= _t('form', 'Удалить'); ?>" data-original-title="<?= _t('form', 'Удалить'); ?>"></i></a>
                                        </li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </aside>
                </form>

                <div class="col-md-9 l-content-column">
                    <div class="p-profileContent">
                        <div class="i-topTitle">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>
                                </div>
                                <form action="" method="get">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                      <input name="qq" value="<?= $f['qq'] ?>" type="search" class="form-control input-sm j-q" placeholder="<?= _t('internalmail', 'Поиск сообщений'); ?>">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm j-q-submit" type="submit"><i class="fa fa-search"></i></button>
                                      </span>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>

                        <form action="" method="post" id="j-my-messages-mobile-form-act">
                        <div class="visible-sm visible-xs">
                            <div class="l-borderedBlock p-profileColumn">
                                <a class="p-profile-collapse-toggle text-center ajax-link" data-toggle="collapse" href="#profile-collapse"><span><?= _t('internalmail', 'Показать папки'); ?></span></a>
                                <div class="p-profile-collapse collapse" id="profile-collapse">
                                    <div class="l-inside">
                                        <div class="input-group">
                                          <input type="text" class="form-control input-sm" name="new_folder_name" value="" placeholder="<?= _t('internalmail', 'Новая папка'); ?>">
                                          <span class="input-group-btn">
                                            <button class="btn btn-primary btn-sm j-make-folder" type="button"><?= _t('', 'Создать'); ?></button>
                                          </span>
                                        </div>
                                    </div>

                                    <div class="l-inside">
                                        <ul class="i-imail-folders">
                                            <? foreach($folders as $k => $v): if( ! empty($v['custom'])) continue; ?>
                                                <li<?= $f['f'] == $k ? ' class="active"' : '' ?>><a href="#" class="<?= ! empty($v['class']) ? $v['class'] : '' ?> j-folder-select" data-id="<?= $k ?>"><i class="fa <?= $f['f'] == $k ? 'fa-folder-open' : 'fa-folder' ?>"></i><?= $v['title'] ?> </a></li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>

                                    <? if($bCustom): ?>
                                    <div class="l-inside">
                                        <ul class="i-imail-folders">
                                            <? foreach($folders as $k => $v): if(empty($v['custom'])) continue; ?>
                                                <li<?= $f['f'] == $k ? ' class="active"' : '' ?>><a href="#" class="<?= ! empty($v['class']) ? $v['class'] : '' ?> j-folder-select" data-id="<?= $k ?>"><i class="fa <?= $f['f'] == $k ? 'fa-folder-open' : 'fa-folder' ?>"></i><span class="j-title"><?= HTML::escape($v['title']) ?></span></a>
                                                    <a href="#" class="j-edit-folder" data-id="<?= $k ?>"><i class="fa fa-edit show-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?= _t('form', 'Редактировать'); ?>" data-original-title="<?= _t('form', 'Редактировать'); ?>"></i></a>
                                                    <a href="#" class="link-delete j-delete-folder" data-id="<?= $k ?>"><i class="fa fa-times show-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?= _t('form', 'Удалить'); ?>" data-original-title="<?= _t('form', 'Удалить'); ?>"></i></a>
                                                </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>
                                    <? endif; ?>

                                </div>
                            </div>
                        </div>
                        </form>

                        <div class="j-list"><?= $list ?></div>
                        <div class="j-pgn"><?= $pgn ?></div>

                    </div>

                </div>
            </div>

        </section>
    </div>
<script type="text/javascript">
<? js::start() ?>
    $(function(){
        jMyMessages.init(<?= func::php2js(array(
            'lang' => array(
                'move_folder' => _t('internalmail', 'Поместить в папку'),
                'set_new_folder_name' => _t('internalmail', 'Укажите название папки'),
                'ok' => _t('internalmail', 'ok'),
                'cancel' => _t('form', 'Отмена'),
            ),
            'folders' => $folders,
            'conv_url' => InternalMail::url('my.messages', array('i'=>'')),
            'ajax' => true,
        )) ?>);
    });
<? js::stop() ?>
</script>