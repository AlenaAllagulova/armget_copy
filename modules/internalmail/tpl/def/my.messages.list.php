<?php

$lng_fav = _t('internalmail', 'Избранные');
$lng_ignore = _t('internalmail', 'Игнорирую');
$lng_move_f = _t('internalmail', 'Поместить в папку');
$conversation_url = InternalMail::url('my.messages', array('i'=>''));

$inFolder = function(&$userFolders, $folderID){
    return ( ! empty($userFolders) && in_array($folderID, $userFolders) );
};

if( ! empty($list)): ?>
    <ul class="i-imailList media-list">
<? foreach($list as $v):
    $message = &$v['message'];
    ?>
        <li class="media">
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                <img src="<?= $v['avatar'] ?>" alt="<?= tpl::avatarAlt($v); ?>" />
                <?= tpl::userOnline($v) ?>
            </a>
            <div class="media-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="f-freelancer-name"><?= tpl::userLink($v) ?></div>
                        <div>
                            <a href="<?= InternalMail::url('my.chat', array('user' => $v['login'])) ?>"><?= _t('internalmail', 'Сообщения'); ?> [<?= $v['msgs_total'] ?>]<? if($v['msgs_new']): ?><span class="i-new-message">+<?= $v['msgs_new'] ?></span><? endif; ?></a> <small><?= tpl::date_format3($message['created']) ?></small>
                        </div>
                        <? if( InternalMail::FOLDERS_ENABLED ):
                            $title = $lng_move_f;
                            if( ! empty($v['folders'])){
                                $title = array(); foreach($v['folders'] as $vv) { $title[] = $folders[ $vv ]['title']; }
                                $title = join(', ', $title);
                            } ?>
                        <div class="dropdown">
                            <a href="#" id="dLabel" class="dropdown-toggle ajax-link" data-toggle="dropdown"><span class="j-f-title"><?= $title ?></span> <i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                                <? foreach($folders as $kk => $vv): if( ! $kk) continue; ?>
                                    <li class="<?= ! empty($vv['class']) ? $vv['class'] : '' ?><?= in_array($kk, $v['folders']) ? ' active' : '' ?>"><a href="#" data-user-id="<?= $v['user_id'] ?>" data-folder-id="<?= $kk ?>" class="j-f-action"><?= $vv['title'] ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                        <? endif; ?>
                    </div>

                    <div class="col-sm-5 i-your-note">
                        <?  $aNote = array('id' => $v['note_id'], 'user_id' => $v['user_id'], 'note' => $v['note'], 'bFav' => 1);
                        echo(Users::i()->noteBlock($aNote));
                        ?>
                    </div>
                </div>
                <? if($qq): ?>
                <div class="i-search-result">
                    <?= tpl::truncate( $message['message'], config::sysAdmin('internalmail.my.messages.list.message.truncate', 200, TYPE_UINT)) ?>
                </div>
                <? endif; ?>
            </div>
            <div class="clearfix"></div>
        </li>
    <? endforeach; ?>
    </ul>
<? unset($message);
else:
    ?><div class="alert alert-info"><?= _t('internalmail', 'Список сообщений пуст') ?></div><?
endif;