<?php

return [
    # переписка
    'internalmail-chat' => [
        'pattern'  => 'user/messages/chat(/|)',
        'callback' => 'internalmail/my_chat',
        'priority' => 110,
    ],
    # список собеседников
    'internalmail-messages' => [
        'pattern'  => 'user/messages(/|)',
        'callback' => 'internalmail/my_messages',
        'priority' => 120,
    ],
];