<?php

class Blog_ extends BlogBase
{
    public function init()
    {
        parent::init();

        if (bff::$class == $this->module_name && Request::isGET()) {
            bff::setActiveMenu('//blog');
        }
    }

    /**
     * Добавление поста
     */
    public function add()
    {
        bff::setMeta(_t('blog', 'Добавление записи в блог'));
        $this->security->setTokenPrefix('blog-post-form');

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
        }

        $aData = $this->validatePostData(0, Request::isPOST(), $publicator);
        $nUserID = User::id();

        $this->checkEnabled();

        if (Request::isPOST()) {
            do {
                $aResponse = array();

                if ( ! $this->errors->no()) {
                    break;
                }

                $func = $this->input->post('func', TYPE_STR);
                if ($func == 'preview' || $func == 'draft') {
                    $aData['status'] = static::STATUS_DRAFT;
                } else {
                    $aData['status'] = static::STATUS_ACTIVE;
                }

                # доверенный пользователь: без модерации
                if ($trusted = User::isTrusted($this->module_name)) {
                    $aData['moderated'] = 1;
                }

                # создаем пост
                $nPostID = $this->model->postSave(0, $aData);
                if ( ! $nPostID) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($publicator) {
                    # переносим фотографии в постоянную папку
                    $publicator->dataUpdate($aData['content'], $nPostID);
                }

                # загружаем изображения
                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->postImages($nPostID);
                    $oImages->setAssignErrors(false);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES)) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                            # удаляем загруженные через "удобный способ"
                            $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                            $oImages->deleteImages($aImages);
                        }
                    } else {
                        # перемещаем из tmp-директории в постоянную
                        $oImages->saveTmp('images');
                    }
                }

                # теги
                if (static::tagsOn()) {
                    $this->postTags()->tagsSave($nPostID);
                }

                # обновляем счетчик постов "на модерации"
                if (!$trusted &&$aData['status'] == static::STATUS_ACTIVE) {
                    $this->moderationCounterUpdate(1);
                }

                $aResponse['redirect'] = static::url('status', array('id' => $nPostID));
                if ($func == 'preview') {
                    $aResponse['redirect'] = static::url('view', array('id' => $nPostID, 'keyword' => $aData['keyword']));
                }

            } while (false);
            $this->iframeResponseForm($aResponse);
        }

        if ( ! $nUserID) {
            return $this->showForbiddenGuests();
        }

        return $this->form(0, $aData);
    }

    /**
     * Редактирование поста
     */
    public function edit()
    {
        bff::setMeta(_t('blog', 'Редактирование записи'));
        $this->security->setTokenPrefix('blog-post-form');

        if ( ! User::id()) {
            $this->redirect(Users::url('login'), false, true);
        }

        $this->checkEnabled();

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
        }

        $nPostID = $this->input->get('id', TYPE_UINT);
        if ( ! $nPostID) {
            $this->errors->error404();
        }

        $aPostData = $this->model->postData($nPostID, array(), true);
        if (empty($aPostData)) {
            $this->errors->error404();
        }

        if ( ! $this->isPostOwner($nPostID, $aPostData['user_id'])) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
                $this->iframeResponseForm();
            }
            return $this->showForbidden('', _t('blog', 'Вы не являетесь автором данной записи'));
        }

        if (Request::isPOST()) {
            $aData = $this->validatePostData($nPostID, true, $publicator);
            $aResponse = array();

            if ($this->errors->no()) {

                $func = $this->input->post('func', TYPE_STR);

                if ($aPostData['status'] == static::STATUS_BLOCKED) {
                    # пост заблокирован, помечаем на проверку модератору
                    $aData['moderated'] = 0;
                }

                # доверенный пользователь: без модерации
                $trusted = User::isTrusted($this->module_name);
                # помечаем на модерацию при изменении: названия, описания
                if ($aPostData['status'] == static::STATUS_ACTIVE) {
                    if ($aData['title'] != $aPostData['title'] || $aData['content'] != $aPostData['content']) {
                        if ($aPostData['moderated'] != 0 && !$trusted) {
                            $aData['moderated'] = 2;
                        }
                    }
                }

                # проверим необходимость публикации черновика
                if ($aPostData['status'] == static::STATUS_DRAFT) {
                    if ($func == 'publicate') {
                        $aData['status'] = static::STATUS_ACTIVE;
                        if ($trusted) {
                            $aData['moderated'] = 1;
                        } else {
                            $aData['moderated'] = 0;
                        }
                    }
                }

                $this->model->postSave($nPostID, $aData, 'd');

                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->postImages($nPostID);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES) && $aPostData['imgcnt'] < $oImages->getLimit()) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                        }
                    } else {
                        # сохраняем порядок изображений
                        $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                        $oImages->saveOrder($aImages, false);
                    }

                    # помечаем на модерацию при изменении изображений
                    if ($oImages->newImagesUploaded($this->input->post('images_hash', TYPE_STR))
                        && $aPostData['moderated']!=0 && !$trusted) {
                        $this->model->postSave($nPostID, array('moderated' => 2));
                    }
                }

                # теги
                if (static::tagsOn()) {
                    $this->postTags()->tagsSave($nPostID);
                }

                # обновляем счетчик постов "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['redirect'] = static::url('user.view', array('id' => $nPostID, 'keyword' => $aData['keyword'], 'login'=>User::data('login')));
                if ($func == 'preview') {
                    $aResponse['redirect'] = static::url('view', array('id' => $nPostID, 'keyword' => $aData['keyword']));
                }

            }
            $this->iframeResponseForm($aResponse);
        }

        return $this->form($nPostID, $aPostData);
    }

    /**
     * Форма поста
     * @param integer $nPostID ID поста
     * @param array $aData @ref
     * @return string HTML
     */
    protected function form($nPostID, array &$aData)
    {
        $aData['id'] = $nPostID;
        $aData['img'] = $this->postImages($nPostID);

        if ($nPostID) {
            if (static::imagesLimit()) {
                $aImages = $aData['img']->getData($aData['imgcnt']);
                $aData['images'] = array();
                foreach ($aImages as $v) {
                    $aData['images'][] = array(
                        'id'       => $v['id'],
                        'tmp'      => false,
                        'filename' => $v['filename'],
                        'i'        => $aData['img']->getURL($v, BlogPostImages::szForm, false)
                    );
                }
                $aData['imghash'] = $aData['img']->getLastUploaded();
            }

            # для селектора категории
            $aData['cat'] = array(
                'cat_id1' => $aData['cat_id1'],
                'cat_id2' => $aData['cat_id2'],
                'cat_id'  => $aData['cat_id'],
                'title'   => $this->model->categoryParentsTitle($aData['cat_id'], true, true, '/'));
            if ($aData['cat_id'] == $aData['cat_id2']) {
                $aStr = explode('/', $aData['cat']['title']);
                $aData['cat']['t1'] = trim($aStr[1]);
                $aData['cat']['t2'] = trim($aStr[0]);
            }
        } else {
            $aData['images'] = array();
            $aData['cat'] = array();
            $aData['imgcnt'] = 0;
            $aData['imghash'] = '';

            $aData['price'] = '';
        }

        $aData['edit'] = ! empty($nPostID);
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
            if ($aData['edit'] && empty($aData['content_search'])) {
                $aData['content'] = $publicator->textConvert($aData['content']);
            }
            $aData['publicator'] = $publicator->formFrontend($aData['content'], $nPostID, 'content', 'jBlogFormPublicator');
        }

        return $this->viewPHP($aData, 'form');
    }

    /**
     * Вывод сообщения о статусе поста
     */
    public function status()
    {
        $this->checkEnabled();

        $nPostID = $this->input->get('id', TYPE_UINT);
        if ( ! $nPostID) {
            $this->errors->error404();
        }
        $aData = $this->model->postDataView($nPostID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        if ($aData['moderated'] > 0) {
            $this->redirect(static::url('view', array('id' => $nPostID, 'keyword' => $aData['keyword'])));
        }
        if ( ! User::id()) {
            return $this->showForbiddenGuests();
        }

        return $this->viewPHP($aData, 'status');
    }

    /**
     * Выбор категории
     * @param int $nCount
     * @param array $aData
     * @param array $aParam
     * @return string
     */
    public function catSelect($nCount = 0, array $aData = array(), $aParam = array())
    {
        $aData = $aData + $aParam;
        $aData['count'] = $nCount;
        if (empty($aData['cat_id'])) {
            $aData['pid'] = $this->model()->categoryRootNodeID();
            $aData['pid_title'] = '';
            $nPid = $aData['pid'];
        } else {
            $nPid = ($aData['cat_id2'] == $aData['cat_id'] ? $aData['cat_id1'] : $this->model()->categoryRootNodeID());
            $aData = $aData + $this->model()->categoryData($nPid);
            $aData['pid_title'] = $this->model()->categoryParentsTitle($nPid, false, true, '/', $aData['aParents']);
        }
        $aData['aCats'] = $this->model()->categorySubOptions($nPid, false, array('enabled' => 1));

        return $this->viewPHP($aData, 'form.cat');
    }
    
    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR))
        {
            case 'form-menu': # список подкатегорий для категории
            {
                $nCatID = $this->input->postget('cat', TYPE_UINT); # ID категории
                $aData = $this->model()->categoryData($nCatID);
                $aData['pid_title'] = $this->model()->categoryParentsTitle($aData['id'], false, true, '/', $aData['aParents']);
                if ($aData['pid_title']) {
                    $aData['pid_title'] .= ' / ';
                }
                $aData['aCats'] = $this->model()->categorySubOptions($nCatID, false, array('enabled' => 1));
                $aResponse['menu'] = $this->viewPHP($aData, 'form.cat.ajax');
            } break;
            case 'add': # добавить новую категорию
            {
                $nCount = $this->input->postget('cnt', TYPE_UINT);
                $aResponse['html'] = $this->catSelect($nCount, array(), array('allowDelete' => 1));
            } break;
            case 'tags-autocomplete': # теги: autocomplete
            {
                $sQuery = $this->input->post('q', TYPE_STR);
                $this->postTags()->tagsAutocomplete($sQuery);
            } break;
            case 'post-delete': # удаление поста
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nPostID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->postData($nPostID, array('user_id'));
                if (empty($aData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isPostOwner($nPostID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->postDelete($nPostID)) {
                    $this->errors->reloadPage();
                    break;
                }

                $aResponse['redirect'] = static::url('my.list');

            } break;
            case 'post-hide': # скрыть пост
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nPostID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->postData($nPostID, array('user_id', 'status'));
                if (empty($aData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isPostOwner($nPostID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_ACTIVE) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_HIDDEN;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->postSave($nPostID, $aUpdate);
            } break;
            case 'post-show': # отобразить пост
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nPostID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->postData($nPostID, array('user_id', 'status'));
                if (empty($aData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isPostOwner($nPostID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if( ! in_array($aData['status'], array(static::STATUS_HIDDEN, static::STATUS_DRAFT))) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_ACTIVE;
                $aUpdate['status_changed'] = $this->db->now();

                if ($aData['status'] == static::STATUS_DRAFT) {
                    $aUpdate['moderated'] = User::isTrusted($this->module_name) ? 1 : 0;
                }

                $this->model->postSave($nPostID, $aUpdate);

                if ($aData['status'] == static::STATUS_DRAFT) {
                    $this->moderationCounterUpdate();
                    $aResponse['redirect'] = static::url('status', array('id' => $nPostID));
                }
            } break;
            case 'comment-add': # комментарии: добавление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $sMessage = $this->input->post('message', TYPE_TEXT, array('len' => 2500, 'len.sys' => 'blog.comment.message.limit'));
                $min = config::sys('blog.comment.message.min', 10, TYPE_UINT);
                if (mb_strlen($sMessage) < $min) {
                    $this->errors->set(_t('', 'Комментарий не может быть короче [min] символов', array('min' => $min)), 'message');
                    break;
                }

                # антиспам фильтр
                if(Site::i()->spamFilter(array(
                    array('text' => & $sMessage,   'error'=>_t('', 'В указанном вами комментарии присутствует запрещенное слово "[word]"')),
                ))) {
                    break;
                }

                $nPostID = $this->input->post('id', TYPE_UINT);
                if ( ! $nPostID) {
                    $this->errors->reloadPage(); break;
                }

                $aPostData = $this->model->postData($nPostID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aPostData)) {
                    $this->errors->reloadPage();
                }

                if ($aPostData['status'] != static::STATUS_ACTIVE) {
                    $this->errors->impossible(); break;
                }
                if ( ! $aPostData['comments_enabled']) {
                    $this->errors->set(_t('blog', 'Добавление комментариев запрещено автором записи'), 'message');
                    break;
                }

                # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                if (Site::i()->preventSpam('blog-comment', config::sysAdmin('blog.prevent.spam', 10, TYPE_UINT))) {
                    break;
                }

                $nParent = $this->input->post('parent', TYPE_UINT);

                $aData = array(
                    'message' => $sMessage,
                    'name' => User::data('name'),
                );

                $nCommentID = $this->postComments()->commentInsert($nPostID, $aData, $nParent);
                if ($nCommentID) {
                    # оставили комментарий
                    $aCommentsData = array(
                        'aComments' => array(array(
                            'created'  => $this->db->now(),
                            'message'  => nl2br($aData['message']),
                            'name'     => User::data('name'),
                            'surname'  => User::data('surname'),
                            'pro'      => User::data('pro'),
                            'login'    => User::data('login'),
                            'sex'      => User::data('sex'),
                            'avatar'   => User::data('avatar'),
                            'last_activity' => User::data('last_activity'),
                            'deleted'  => 0,
                            'numlevel' => $nParent ? 2 : 1,
                            'user_id'  => User::id(),
                            'id'       => $nCommentID,
                        )),
                        'nPostOwnerID' => $aPostData['user_id'],
                        'nItemID'      => $nPostID,
                    );
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                }

            } break;
            case 'comment-delete': # комментарии: удаление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $nCommentID = $this->input->post('id', TYPE_UINT);
                if ( ! $nCommentID) {
                    $this->errors->reloadPage();
                    break;
                }

                $nPostID = $this->input->post('post_id', TYPE_UINT);
                if ( ! $nPostID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aPostData = $this->model->postData($nPostID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aPostData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $aPostData['comments_enabled']) {
                    $this->errors->reloadPage();
                    break;
                }

                $oComments = $this->postComments();
                $aCommentData = $oComments->commentData($nPostID, $nCommentID);
                if (empty($aCommentData)) {
                    $this->errors->reloadPage();
                    break;
                }
                $nUserID = User::id();
                if ($aCommentData['user_id'] == $nUserID) { # владелец комментария
                    $oComments->commentDelete($nPostID, $nCommentID, BlogPostComments::commentDeletedByCommentOwner);
                } else {
                    if ($aPostData['user_id'] == $nUserID) { # владелец поста
                        $oComments->commentDelete($nPostID, $nCommentID, BlogPostComments::commentDeletedByItemOwner);
                    } else {
                        $this->errors->reloadPage();
                        break;
                    }
                }

                $aCommentsData = $oComments->commentsData($nPostID, 0, true, $nCommentID);
                if ( ! empty($aCommentsData)) {
                    $aCommentsData['nPostOwnerID'] = $aPostData['user_id'];
                    $aCommentsData['nItemID'] = $nPostID;
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                } else {
                    $aResponse['html'] = '';
                }
            } break;
            case 'comment-restore': # комментарии: восстановление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $nCommentID = $this->input->post('id', TYPE_UINT);
                if ( ! $nCommentID) {
                    $this->errors->reloadPage();
                    break;
                }

                $nPostID = $this->input->post('post_id', TYPE_UINT);
                if ( ! $nPostID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aPostData = $this->model->postData($nPostID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aPostData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $aPostData['comments_enabled']) {
                    $this->errors->reloadPage();
                    break;
                }

                $oComments = $this->postComments();
                $aCommentData = $oComments->commentData($nPostID, $nCommentID);
                if (empty($aCommentData)) {
                    $this->errors->reloadPage();
                    break;
                }
                $nUserID = User::id();
                switch ($aCommentData['deleted']) {
                    case BlogPostComments::commentDeletedByItemOwner:
                        if ($aPostData['user_id'] == $nUserID) {
                            $oComments->restore($nCommentID);
                        } else {
                            $this->errors->reloadPage();
                            break 2;
                        } break;
                    case BlogPostComments::commentDeletedByCommentOwner:
                        if ($aCommentData['user_id'] == $nUserID) {
                            $oComments->restore($nCommentID);
                        } else {
                            $this->errors->reloadPage();
                            break 2;
                        } break;
                    default:
                        $this->errors->reloadPage();
                        break 2;
                }

                $aCommentsData = $oComments->commentsData($nPostID, 0, true, $nCommentID);
                if ( ! empty($aCommentsData)) {
                    $aCommentsData['nPostOwnerID'] = $aPostData['user_id'];
                    $aCommentsData['nItemID'] = $nPostID;
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                } else {
                    $aResponse['html'] = '';
                }

            } break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Управление изображениями поста
     * @param getpost ::uint 'post_id' ID поста
     * @param getpost ::string 'act' действие
     */
    public function img()
    {
        $this->security->setTokenPrefix('blog-post-form');

        $nPostID = $this->input->getpost('post_id', TYPE_UINT);
        $oImages = $this->postImages($nPostID);
        $aResponse = array();

        switch ($this->input->getpost('act'))
        {
            case 'upload': # загрузка
            {
                $aResponse = array('success' => false);
                do {
                    if (!$this->security->validateToken(true, false)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nPostID) {
                        if (!$this->isPostOwner($nPostID)) {
                            $this->errors->set(_t('blog', 'Вы не являетесь автором данной записи'));
                            break;
                        }
                    }

                    $result = $oImages->uploadQQ();

                    $aResponse['success'] = ($result !== false && $this->errors->no());
                    if ($aResponse['success']) {
                        $aResponse = array_merge($aResponse, $result);
                        $aResponse['tmp'] = empty($nPostID);
                        $aResponse['i'] = $oImages->getURL($result, BlogPostImages::szForm, $aResponse['tmp']);
                        unset($aResponse['dir'], $aResponse['srv']);
                    }
                } while (false);

                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
            break;
            case 'delete': # удаление
            {
                $nImageID = $this->input->post('image_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_STR);

                # неуказан ID изображения ни filename временного
                if (!$nImageID && empty($sFilename)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->security->validateToken(true, false)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($nPostID) {
                    # проверяем доступ на редактирование
                    if (!$this->isPostOwner($nPostID)) {
                        $this->errors->set(_t('blog', 'Вы не являетесь автором данной записи'));
                        break;
                    }
                }

                if ($nImageID) {
                    # удаляем изображение по ID
                    $oImages->deleteImage($nImageID);
                } else {
                    # удаляем временное
                    $oImages->deleteTmpFile($sFilename);
                }
            }
            break;
            case 'delete-tmp': # удаление временного
            {
                $aFilenames = $this->input->post('filenames', TYPE_ARRAY_STR);
                $oImages->deleteTmpFile($aFilenames);
            }
            break;
            default:
                $this->errors->reloadPage();
        }

        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Поиск постов и просмотр общего списка
     */
    public function search()
    {
        $this->checkEnabled();

        $nPageSize = config::sysAdmin('blog.search.pagesize', 10, TYPE_UINT);
        $aData = array('list'=>array(), 'pgn'=>'', 'cat_id'=>0, 'cat_id1'=>0, 'cat_id2'=>0);
        $aFilter = array();

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT,   # № страницы
            'q'    => TYPE_NOTAGS, # строка поиска
            'tag'  => TYPE_NOTAGS, # тег: "keyword-id"
        ));
        $aData['f'] = &$f;

        # Фильтр по категории / подкатегории
        $f['cat'] = $this->input->getpost('cat', TYPE_STR); # keyword категории или категории/подкатегории
        $f['cat'] = trim($f['cat'], ' /');
        if ($f['cat']) {
            $sCat = ( ($pos = mb_strpos($f['cat'], '/')) ? mb_substr($f['cat'], $pos + 1) : $f['cat'] );
            $aCat = $aData['cat'] = $this->model->categoryMeta($sCat);
            if (empty($aCat) && ! Request::isAJAX()) {
                $this->errors->error404();
            }
            $aFilter['cat_id'.$aCat['numlevel']] = $aData['cat_id'] = $aData['cat_id'.$aCat['numlevel']] = $aCat['id'];
            if ($aCat['numlevel'] == 2) $aData['cat_id1'] = $aCat['pid'];
        }

        # Фильтр по тегу
        if ($f['tag']) {
            if (preg_match('/(.*)-(\d+)/', $f['tag'], $matches) && ! empty($matches[2])) {
                $aTagData = $this->postTags()->tagData($matches[2]);
                if (empty($aTagData) || mb_strtolower($aTagData['tag']) != $matches[1]) $this->errors->error404();
                $aFilter['tag'] = $aData['nTagID'] = $aTagData['id'];
                $aData['sTagTitle'] = $aTagData['tag'];
            } else {
                $this->errors->error404();
            }
        }

        if ($f['q']) {
            $aFilter['text'] = $f['q'];
        }

        # Категории
        $aData['cats'] = $this->model->categoriesListAll(array('id','pid','keyword','title'));
        foreach ($aData['cats'] as &$v) {
            $v['url'] = static::url('list', array('keyword'=>$v['keyword']));
            if ( ! empty($v['sub'])) {
                foreach ($v['sub'] as &$vv) {
                    $vv['keyword'] = $v['keyword'].'/'.$vv['keyword'];
                    $vv['url'] = static::url('list', array('keyword'=>$vv['keyword']));
                    if ($vv['id'] == $aData['cat_id']) $aData['cat']['keyword'] = $vv['keyword'];
                } unset($vv);
            }
        } unset($v);

        $nCount = $this->model->postsList($aFilter, true);
        if ($nCount) {
            $pgnQuery = $f; unset($pgnQuery['cat']);
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => static::url('list', array('keyword'=>($aData['cat_id'] ? $aData['cat']['keyword'] : ''))),
                'query' => $pgnQuery,
            ));
            $bAddNumRow = ($f['q'] || $f['tag']);
            $aData['list'] = $this->model->postsList($aFilter, false, $pgn->getLimitOffset(), 'P.created DESC', $bAddNumRow);
            foreach ($aData['list'] as $k => &$v) {
                $v['url_view'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword']));
                if ($bAddNumRow) {
                    $v['n'] = ($pgn->getCurrentPage() - 1) * $nPageSize + $k + 1;
                }
                if ($f['q']) {
                    foreach (explode(' ', $f['q']) as $vv) {
                        $v['title'] = str_replace($vv, '<em>' . $vv . '</em>', $v['title']);
                        $v['content_preview'] = str_replace($vv, '<em>' . $vv . '</em>', $v['content_preview']);
                    }
                }
            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        if ($f['q'] || $f['tag']) {
            $aData['list'] = $this->viewPHP($aData, 'search.keywords.list');
        } else {
            $aData['list'] = $this->viewPHP($aData, 'search.list');
        }
        $aData['cnt'] = $nCount;
        $aData['count'] = tpl::declension($nCount, _t('blog', 'запись;записи;записей'));
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'   => $aData['pgn'],
                'list'  => $aData['list'],
                'count' => $aData['count'],
            ));
        }

        if ($f['q'] || $f['tag']) {
            # SEO: Поиск по ключевому слову
            $this->seo()->robotsIndex(false);
            $this->setMeta('search-keyword', array(
                'query' => ( $f['tag'] ? ( ! empty($aData['sTagTitle']) ? $aData['sTagTitle'] : '') : tpl::truncate($f['q'], config::sysAdmin('blog.search.query.truncate', 50, TYPE_UINT), '') ),
                'page'  => $f['page'],
            ));
            return $this->viewPHP($aData, 'search.keywords');
        }

        $seoData = array('page' => $f['page']);
        if ($aData['cat_id']) {
            # SEO: Список постов в категории
            $url = static::url('list', array('keyword'=>$aData['cat']['keyword']), true);
            $this->urlCorrection(static::urlDynamic($url));
            $this->seo()->canonicalUrl($url, $seoData);
            $this->setMeta('listing-category', array(
                'category' => $aData['cat']['title'],
                'page'     => $f['page'],
            ), $aData['cat']);
            $this->seo()->setText($aData['cat'], $f['page']);
        } else {
            # SEO: Список постов
            $this->urlCorrection(static::url('list'));
            $this->seo()->canonicalUrl(static::url('list', array(), true), $seoData);
            $this->setMeta('listing', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        # Форма поиска
        $aData['form'] = $this->viewPHP($aData, 'search.form');

        return $this->viewPHP($aData, 'search');
    }

    /**
     * Кабинет: Блог
     * @param array $userData данные о пользователе
     */
    public function my_blog(array $userData)
    {
        return $this->user_blog($userData);
    }

    /**
     * Профиль: Блог
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function user_blog(array $userData)
    {
        if (empty($userData['id'])) {
            $this->errors->error404();
        }

        # просмотр поста
        $postID = $this->input->get('id', TYPE_UINT);
        if ($postID) {
            return $this->owner_view($postID, $userData);
        }

        # список постов
        return $this->owner_listing($userData['id'], $userData);
    }

    /**
     * Список постов пользователя (в профиле)
     * @param integer $userID ID пользователя
     * @param array $userData данные пользователя
     * @return string HTML
     */
    protected function owner_listing($userID, array $userData)
    {
        $this->checkEnabled();

        $nPageSize = config::sysAdmin('blog.profile.pagesize', 10, TYPE_UINT);

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT,
            'cat'  => TYPE_UINT,
        ));

        $aFilter = array();

        if ($f['cat']) {
            $aFilter['cat_id1'] = $f['cat'];
        }

        $aData = array('pgn'=>'', 'f'=>&$f, 'user'=>&$userData);
        $aData['count'] = $this->model->postsOwnerList($userID, $aFilter, true);
        if ($aData['count']) {
            $pgn = new Pagination($aData['count'], $nPageSize, array(
                'link'  => static::url('user.listing', array('login' => $userData['login'])),
                'query' => $f,
            ));
            $aData['list'] = $this->model->postsOwnerList($userID, $aFilter, false, $pgn->getLimitOffset());
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }

        $aData['cats'] = array(0 => array(
            'id'    => 0,
            'title' => _t('blog', 'Все категории')
        ));
        $aData['cats'] += $this->model->categoriesOwnerList($userID);

        $aData['list'] = $this->viewPHP($aData, 'owner.list.ajax');
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'    => $aData['pgn'],
                'list'   => $aData['list'],
            ));
        }

        # SEO: Профиль: Блог - список
        $this->seo()->robotsIndex(!$f['cat']);
        $seoURL = static::url('user.listing', array('login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL, array('page'=>$f['page']));
        $this->seo()->setPageMeta($this->users(), 'profile-blog', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'country' => $userData['country'],
            'region'  => $userData['region'],
            'page'    => $f['page'],
        ), $aData);

        return $this->viewPHP($aData, 'owner.list');
    }

    /**
     * Блок статуса поста
     * @param array $aData @ref
     * @return string
     */
    public function postStatusBlock(array &$aData)
    {
        if (empty($aData['user_id']) || ! User::isCurrent($aData['user_id'])) {
            return '';
        }
        if ($aData['status'] == static::STATUS_DRAFT) {
            return '';
        }
        return $this->viewPHP($aData, 'status.block');
    }

    /**
     * Просмотр поста пользователя (в профиле)
     * @param integer $nPostID ID поста
     * @param array $userData данные пользователя
     * @return string
     */
    protected function owner_view($nPostID = 0, array $userData)
    {
        $this->checkEnabled();

        if ( ! $nPostID) {
            $this->errors->error404();
        }
        $aData = $this->model->postDataView($nPostID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        # Статус:
        if (!$userData['my']) {
            # Неактивен
            if ($aData['status'] != static::STATUS_ACTIVE) {
                $this->errors->error404();
            }
            # Непромодерирован
            if (static::premoderation() && ! $aData['moderated']) {
                $this->errors->error404();
            }
        }

        # Следующий пост
        $aData['next'] = $this->model->postNext($nPostID, $aData['user_id'], $aData['created']);
        if ( ! empty($aData['next'])) {
            $aData['next']['link'] = static::url('user.view', array('id' => $aData['next']['id'], 'keyword' => $aData['next']['keyword'], 'login' => $userData['login']));
        }
        # Предыдущий пост
        $aData['prev'] = $this->model->postPrev($nPostID, $aData['user_id'], $aData['created']);
        if ( ! empty($aData['prev'])) {
            $aData['prev']['link'] = static::url('user.view', array('id' => $aData['prev']['id'], 'keyword' => $aData['prev']['keyword'], 'login' => $userData['login']));
        }

        if (static::publicatorEnabled() && ! empty($aData['content_search'])) {
            # содержание
            $aData['content'] = $this->initPublicator()->view($aData['content'], $nPostID, 'view.content', $this->module_dir_tpl);
        }

        # Содержание поста
        $aData['view'] = $this->post_view($aData);

        # SEO: Профиль: Блог - просмотр поста
        $seoURL = static::url('view', array('id' => $nPostID, 'keyword' => $aData['keyword']), true);
        $this->urlCorrection(static::url('user.view', array('id' => $nPostID, 'keyword' => $aData['keyword'], 'login' => $userData['login'])));
        $this->seo()->canonicalUrl($seoURL); // ссылка на пост в разделе "Блог" (не в профиле)
        $this->seo()->setPageMeta($this->users(), 'profile-blog-view', array(
            'title'      => tpl::truncate($aData['title'], config::sysAdmin('blog.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full' => $aData['title'],
            'description'=> tpl::truncate(trim(strip_tags($aData['content_preview']), '.,'), config::sysAdmin('blog.view.meta.description.truncate', 150, TYPE_UINT)),
            'tags'       => $aData['tags_seo'],
            'name'       => $userData['name'],
            'surname'    => $userData['surname'],
            'login'      => $userData['login'],
            'city'       => $userData['city'],
            'country'    => $userData['country'],
        ), $aData);
        # SEO: Open Graph
        $seoSocialImages = array();
        foreach ($aData['images'] as $v) $seoSocialImages[] = $v['i'][BlogPostImages::szView];
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], $seoSocialImages, $seoURL, $aData['share_sitename']);

        return $this->viewPHP($aData, 'owner.view');
    }

    /**
     * Просмотр поста в разделе "Блог"
     * @return string
     */
    public function view()
    {
        $this->checkEnabled();

        $userID = User::id();
        $nPostID = $this->input->get('id', TYPE_UINT);
        if (!$nPostID) {
            $this->errors->error404();
        }
        $aData = $this->model->postDataView($nPostID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        $adminView = $this->input->get('adminView', TYPE_STR);
        if ($adminView != md5($aData['user_id'].$aData['id'].$aData['keyword'])) {
            $adminView = false;
        }

        # Статус
        if (!User::isCurrent($aData['user_id']) && !$adminView) {
            # Неактивен
            if ($aData['status'] != static::STATUS_ACTIVE) {
                $this->errors->error404();
            }
            # Ожидает проверки
            if (static::premoderation() && ! $aData['moderated']) {
                $this->errors->error404();
            }
        }
        $aData['avatar_small'] = UsersAvatar::url($aData['user_id'], $aData['avatar'], UsersAvatar::szSmall, $aData['sex']);

        if (static::publicatorEnabled() && ! empty($aData['content_search'])) {
            # содержание
            $aData['content'] = $this->initPublicator()->view($aData['content'], $nPostID, 'view.content', $this->module_dir_tpl);
        }

        # Хлебные крошки
        $crumbs = array(
            array('title' => _t('blog','Blog'), 'link' => static::url('list')),
        );
        $aLinkData = array();
        foreach ($aData['cat'] as $v) {
            $aLinkData['cat'.$v['numlevel'].'_keyword'] = $v['keyword'];
            $crumbs[] = array('title' => $v['title'], 'link' => static::url('search-cat', $aLinkData));
        }
        $crumbs[] = array('title' => $aData['title'], 'active' => true);
        $aData['breadcrumbs'] = &$crumbs;

        # Содержание поста
        $aData['view'] = $this->post_view($aData);

        # Счетчик просмотров
        if ($userID != $aData['user_id'] && ! $adminView && ! Request::isRefresh()) {
            $this->model->postSave($nPostID, array('views_total = views_total + 1'));
        }

        # SEO: Просмотр поста
        $seoURL = static::url('view', array('id'=>$nPostID, 'keyword'=>$aData['keyword']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->setMeta('view', array(
            'title'      => tpl::truncate($aData['title'], config::sysAdmin('blog.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full' => $aData['title'],
            'description'=> tpl::truncate(trim(strip_tags($aData['content_preview']), '.,'), config::sysAdmin('blog.view.meta.description.truncate', 150, TYPE_UINT)),
            'tags'       => $aData['tags_seo'],
        ), $aData);
        # SEO: Open Graph
        $seoSocialImages = array();
        foreach ($aData['images'] as $v) $seoSocialImages[] = $v['i'][BlogPostImages::szView];
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], $seoSocialImages, $seoURL, $aData['share_sitename']);

        return $this->viewPHP($aData, 'view');
    }

    /**
     * Просмотр поста: содержание
     * @param array $aData @ref
     * @return string HTML
     */
    protected function post_view(array &$aData)
    {
        $nPostID = $aData['id'];

        $aData['images'] = array();
        if (static::imagesLimit() && $aData['imgcnt']) {
            $oImages = $this->postImages($nPostID);
            $aImages = $oImages->getData($aData['imgcnt']);
            foreach ($aImages as $v) {
                $aData['images'][] = array(
                    'id'       => $v['id'],
                    'filename' => $v['filename'],
                    'i'        => $oImages->getURL($v, array(
                            BlogPostImages::szView
                        ), false
                    )
                );
            }
        }
        $aData['tags_seo'] = array();
        if (static::tagsOn()) {
            $aData['tags'] = $this->postTags()->tagsGet($nPostID);
            foreach ($aData['tags'] as $v) {
                $aData['tags_seo'][] = $v['tag'];
            }
            $aData['tags_seo'] = join(', ', $aData['tags_seo']);
        }
        if ($aData['comments_enabled']) {
            $aData['comments'] = $this->postComments()->commentsData($nPostID, 0 ,true);
            $aData['comments']['nPostOwnerID'] = $aData['user_id'];
            $aData['comments']['nItemID'] = $nPostID;
            $aData['comments_list'] = $this->commentsList($aData['comments']);
        }

        return $this->viewPHP($aData, 'view.post');
    }

    /**
     * Вывод списка комментариев, рекурсивно
     * @param array $aData
     * @return string
     */
    public function commentsList($aData)
    {
        return $this->viewPHP($aData, 'view.post.comments.ajax');
    }

    /**
     * Проверка, включен ли раздел "Блоги"
     */
    public function checkEnabled()
    {
        if (!static::enabled()) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
            } else {
                $this->errors->error404();
            }
        }
    }

    /**
     * Данные для карты сайта
     * @return array
     */
    public function getSitemapData()
    {
        $aCats = $this->model->categoriesListAll(array('id','pid','keyword','title'));
        foreach ($aCats as &$v) {
            $v['url'] = static::url('list', array('keyword'=>$v['keyword']));
            if ( ! empty($v['sub'])) {
                foreach ($v['sub'] as &$vv) {
                    $vv['keyword'] = $v['keyword'].'/'.$vv['keyword'];
                    $vv['url'] = static::url('list', array('keyword'=>$vv['keyword']));
                } unset($vv);
            }
        } unset($v);

        return array(
            'cats' => $aCats,
            'link' => static::url('list'),
        );
    }
}