<?php

return [
    # просмотр поста
    'blog-view' => [
        'pattern'  => 'blog/([\d]+)\-(.*)(/|)',
        'callback' => 'blog/view/id=$1',
        'priority' => 320,
    ],
    # действие с постом
    'blog-action' => [
        'pattern'  => 'blog/(add|edit|status)(/|)',
        'callback' => 'blog/$1/',
        'priority' => 330,
    ],
    # поиск
    'blog-search' => [
        'pattern'  => 'blog/(.*)',
        'callback' => 'blog/search/cat=$1',
        'priority' => 340,
    ],
];
