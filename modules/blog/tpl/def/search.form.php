<?php

?>
<button type="button" class="l-column-toggle btn btn-default btn-block" data-toggle="collapse" data-target="#j-blog-search-form-block"><i class="fa fa-cog pull-right"></i><?= _t('', 'Фильтр'); ?> <span class="small">(<?= _t('blog', 'Найдено'); ?> <span class="j-posts-count"><?= $count ?></span>)</span></button>

<div class="l-leftColumn l-borderedBlock collapse" id="j-blog-search-form-block">
    <form action="" method="get">
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />

        <h6><?= _t('blog', 'Категории'); ?></h6>
        <div id="j-blog-categories">
            <ul class="l-left-categories l-inside visible-md visible-lg">
                <? foreach($cats as $v): ?>
                    <li<?= $v['id'] == $cat_id1 ? ' class="opened"' : ''?>><a href="<?= $v['url']; ?>" data-subs="<?= $v['subs'] ?>" class="j-cat-title"><? if ($v['subs']){ ?><i class="fa fa-caret-<?= $v['id'] == $cat_id1 ? 'down' : 'right'?>"></i> <? } ?><?= $v['title'] ?></a>
                        <? if( ! empty($v['sub'])): ?>
                            <ul<?= $v['id'] != $cat_id1 ? ' class="hidden"' : ''?>>
                                <li<?= $v['id'] == $cat_id1 && $cat_id2 == 0 ? ' class="checked"' : '' ?>><a href="<?= $v['url'] ?>"><?= _t('', 'Все'); ?></a></li>
                                <? foreach($v['sub'] as $vv): ?>
                                    <li<?= $vv['id'] == $cat_id2 ? ' class="checked"' : '' ?>><a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        <? endif; ?>
                    </li>
                <? endforeach; ?>
            </ul>

            <div class="collapse in" id="j-mobile-cats">
                <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                    <? foreach($cats as $v): ?>
                        <li><a href="<?= $v['url'] ?>" class="j-mobile-cat" data-id="<?= $v['id']; ?>"><i class="fa fa-chevron-right pull-right"></i><?= $v['title'] ?></a></li>
                    <? endforeach; ?>
                </ul>
            </div>

            <? foreach($cats as $v): ?>
                <div class="collapse j-mobile-cat-block" id="j-mobile-cat-<?= $v['id'] ?>">
                    <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                        <li class="active">
                            <a href="#" class="ajax-link j-mobile-cat-back"><span>&laquo; <?= _t('form', 'Вернуться назад'); ?></span><br>
                                <?= $v['title'] ?>
                            </a>
                            <ul>
                                <li<?= $v['id'] == $cat_id1 && $cat_id2 == 0 ? ' class="checked"' : '' ?>>
                                    <a href="<?= $v['url'] ?>"><?= _t('', 'Все'); ?></a>
                                </li>
                                <? foreach($v['sub'] as $vv): ?>
                                    <li<?= $vv['id'] == $cat_id2 ? ' class="checked"' : '' ?>>
                                        <a href="<?= $vv['url'] ?>"><?= $vv['title'] ?></a>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            <? endforeach; ?>

        </div>
    </form>
</div>