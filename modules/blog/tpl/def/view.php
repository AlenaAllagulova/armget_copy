<?php
/**
 * @var $this Blog
 */
?>
<div class="container">

    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

    <section class="l-mainContent">
        <div class="row">

        <div class="col-md-9">
            <?= $view ?>
        </div>

        <div class="col-md-3 visible-lg visible-md">
            <? # Баннер: Блоги: просмотр ?>
            <?= Banners::view('blog_view', array('pos'=>'right', 'cat'=>$cat_id)) ?>
        </div>

        </div>
    </section>

</div>