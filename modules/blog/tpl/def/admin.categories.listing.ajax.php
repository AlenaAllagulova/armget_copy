<?php
/**
 * @var $this Blog
 */
$urlPosts = $this->adminLink('posts&cat=');
foreach ($list as $k=>&$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>" id="dnd-<?= $id ?>" data-numlevel="<?= $v['numlevel'] ?>" data-pid="<?= $v['pid'] ?>">
        <td class="left" style="padding-left:<?= ($v['numlevel']*15-10) ?>px;">
            <a class="but category-expand folder<?= (!$v['node'] ? '_ua' : '') ?> but-text" data-id="<?= $id ?>" href="#"><?= $v['title'] ?></a>
        </td>
        <td><a href="<?= $urlPosts.$id.'&tab=4'.($v['node']?'&cat_sub=1':'') ?>"><?= $v['cnt_posts'] ?></a></td>
        <td><?= tpl::date_format2($v['created'], false, true) ?></td>
        <td>
            <a class="but <?= ($v['enabled']?'un':'') ?>block category-toggle" title="Включен" href="#" data-type="enabled" data-id="<?= $id ?>"></a>
            <a class="but edit category-edit" title="<?= _t('', 'Edit') ?>" href="#" data-id="<?= $id ?>"></a>
            <a class="but del category-del" title="<?= _t('', 'Delete') ?>" href="#" data-id="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="4">
            ничего не найдено
        </td>
    </tr>
<? endif;