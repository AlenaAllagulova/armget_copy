<?php
/**
 * @var $this Blog
 *
 * Необходимые данные о посте:
 * id, status, status_changed, moderated, blocked_reason
 *
 * Необходимыые данные пользователя:
 * user['blocked']
 */

$blocked = ($status == Blog::STATUS_BLOCKED);
$is_popup = ! empty($is_popup);
$only_form = ! empty($only_form);
?>
<? if ( ! $only_form) { ?>
    <script type="text/javascript">
        var jBlogPostStatus = (function(){
            var $block, url = '<?= $this->adminLink('post_status&act=', 'blog'); ?>';
            var data = {id: <?= $id ?>, blocked: <?= ($blocked?1:0) ?>};
            $(function(){
                $block = $('#j-post-status-block');
            });

            function _progress() {
                $block.toggleClass('disabled');
            }

            return {
                approve: function(){
                    bff.ajax(url+'approve', data, function(resp){
                        if(resp && resp.success) {
                            $block.html(resp.html);
                        }
                    }, _progress);
                },
                activate: function(){
                    if( ! bff.confirm('sure')) return;
                    bff.ajax(url+'activate', data, function(resp){
                        if(resp && resp.success) {
                            $block.html(resp.html);
                        }
                    }, _progress);
                },
                deactivate: function(){
                    if( ! bff.confirm('sure')) return;
                    bff.ajax(url+'deactivate', data, function(resp){
                        if(resp && resp.success) {
                            $block.html(resp.html);
                        }
                    }, _progress);
                },
                changeBlocked: function(step, block)
                {
                    var $blocked_reason = $block.find('.j-post-blocked-reason');
                    var $buttons = $block.find('.j-post-status-buttons');
                    switch(step)
                    {
                        case 1: { // block/unblock, change reason
                            $block.find('.j-blocked').hide();
                            $block.find('.j-blocked-error, .j-blocking').show(0, function(){
                                $blocked_reason.focus();
                                $buttons.hide();
                            });
                        } break;
                        case 2: { // cancel
                            if(data.blocked == 1) {
                                $block.find('.j-blocked').show();
                                $block.find('.j-blocking').hide();
                            } else {
                                $block.find('.j-blocked-error').hide();
                                $buttons.show();
                            }
                        } break;
                        case 3: { // save
                            data.blocked_reason = $blocked_reason.val();
                            bff.ajax(url+'block', data, function(resp){
                                if(resp && resp.success) {
                                    data.blocked = resp.blocked;
                                    $block.html(resp.html);
                                }
                            }, _progress);
                        } break;
                        case 4: { // unblock
                            if (!bff.confirm('sure')) break;
                            data.unblock = 1;
                            jBlogPostStatus.changeBlocked(3);
                            data.unblock = 0;
                        } break;
                    }
                    return false;
                }
            };
        }());
    </script>

    <div id="j-post-status-block" class="well well-small">
<? } ?>
    <table class="admtbl tbledit">
        <tr>
            <td class="row1 field-title<? if($is_popup) { ?> right<? } ?>" style="width:<?= ( $is_popup ? 133 : 88 ) ?>px;"><?= _t('blog', 'Статус') ?></td>
            <td class="row2"><strong><?
                    if( $user_blocked ) {
                        echo _t('users', 'Аккаунт пользователя был заблокирован');
                    } else {
                        switch($status) {
                            case Blog::STATUS_ACTIVE: { echo _t('blog', 'Активен'); } break;
                            case Blog::STATUS_HIDDEN: { echo _t('blog', 'Скрыт'); } break;
                            case Blog::STATUS_BLOCKED: { echo ($moderated == 0?_t('blog', 'Ожидает проверки (был заблокировано)'):_t('blog', 'Заблокирован')); } break;
                        }
                    }
                    ?></strong><? if($status_changed != '0000-00-00 00:00:00'){ ?>&nbsp;&nbsp;<span class="desc">(<?= tpl::date_format2($status_changed, true, true); ?>)</span><? } ?>
            </td>
        </tr>
        <? if ( ! $user_blocked ) { ?>
            <tr>
                <td class="row1" colspan="2">
                    <div class="alert alert-danger <?= (!$blocked ? 'hidden':'') ?> j-blocked-error">
                        <div><?= _t('blog', 'Причина блокировки:') ?></div>
                        <div class="clear"></div>
                        <div class="j-blocked">
                            <span class="j-blocked-text"><?= (!empty($blocked_reason) ? tpl::blockedReason($blocked_reason) :'?') ?></span> - <a href="#" onclick="jBlogPostStatus.changeBlocked(1,0); return false;" class="ajax desc">изменить</a>
                        </div>
                        <div class="j-blocking" style="display: none;">
                            <textarea name="blocked_reason" class="autogrow j-post-blocked-reason" style="height:60px; min-height:60px;"><?= $blocked_reason; ?></textarea>
                            <a onclick="return jBlogPostStatus.changeBlocked(3, 1);" class="btn btn-mini btn-success" href="#"><?= (!$blocked ? _t('', 'continue'):_t('blog', 'изменить причину')) ?></a>
                            <? if($blocked) { ?><a onclick="return jBlogPostStatus.changeBlocked(4);" class="btn btn-success btn-mini" href="#"><?= _t('', 'unblock') ?></a><? } ?>
                            <a onclick="return jBlogPostStatus.changeBlocked(2);" class="btn btn-mini" href="#"><?= _t('', 'cancel') ?></a>
                        </div>
                    </div>
                </td>
            </tr>
            <? if( ! ($moderated && $blocked) ) { ?>
                <tr class="j-post-status-buttons">
                    <td class="row1" colspan="2" <? if( $is_popup ) { ?> style="padding-left: 90px;" <? } ?>>
                        <?
                        if ($moderated == 0) { ?>
                            <input class="btn btn-mini btn-success success button" type="button" onclick="jBlogPostStatus.approve();" value="<?= ($blocked ? _t('blog', 'проверено, все впорядке') : _t('blog', 'проверено')) ?>" />
                        <? } else {
                            if ( $moderated == 2 ) {
                                ?><input class="btn btn-mini btn-success success button" type="button" onclick="jBlogPostStatus.approve();" value="<?= _t('blog', 'проверено') ?>" /> <?
                            }
                            if ($status ==  Blog::STATUS_ACTIVE) {
                                ?><input class="btn btn-mini submit button" type="button" onclick="jBlogPostStatus.deactivate();" value="<?= _t('blog', 'деактивировать') ?>" /> <?
                            } else if ($status ==  Blog::STATUS_HIDDEN) {
                                ?><input class="btn btn-mini submit button" type="button" onclick="jBlogPostStatus.activate();" value="<?= _t('blog', 'активировать') ?>" /> <?
                            }
                        }
                        if ( ! $blocked) { ?>
                            <a class="btn btn-mini text-error" onclick="jBlogPostStatus.changeBlocked(1); return false;"><?= _t('', 'block') ?></a>
                        <? } ?>
                    </td>
                </tr>
            <? }
        } # endif: ! $user_blocked ?>
    </table>
<? if( ! $only_form) { ?>
    </div>
<? } ?>