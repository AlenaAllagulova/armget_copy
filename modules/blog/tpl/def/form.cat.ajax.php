<?php
if( ! isset($id)) $id = 0;
if( ! isset($title)) $title = '';
if( ! isset($cat_id)) $cat_id = 0;
if($id && ! empty($pid)):
    $path = $aParents;
    if($pid) $path[] = (int)$pid;
    if($id) $path[] = (int)$id;
    $path = ', path:'.func::php2js($path, true);
    ?>
    <li class="p-categoriesList_active">
        <? /*  <a href="#" class="j-cat" data="{id:<?= $id; ?>, pid:<?= $pid; ?><?= $path ?>, pid_title:'<?= ! empty($pid_title) ? $pid_title : '' ?>'}"><h6><?= ! empty($t2) ? $t2 : $title ?></h6></a> */ ?>
        <h6><?= ! empty($t2) ? $t2 : $title ?></h6>
        <a href="#" class="small ajax-link j-back" data="{pid:<?= $pid; ?>, id:<?= $id; ?>}"><span><?= _t('form', 'Вернуться назад'); ?></span></a>
    </li>
<? endif;
if( ! empty($aCats)):
    foreach($aCats as $k => $v):
        $path = '';
        if( ! $v['node']){
            $path = $aParents;
            if($pid) $path[] = (int)$pid;
            if($id) $path[] = (int)$id;
            $path[] = (int)$v['id'];
            $path = ', path:'.func::php2js($path, true);
        } ?>
        <li<?= $v['id'] == $cat_id ? ' class="active"' : '' ?>><a href="#" class="<?= $v['node'] ? 'j-sub' : 'j-cat' ?>" data="{id:<?= $v['id']; ?>, pid:<?= $pid; ?><?= $path ?>, pid_title:'<?= ! empty($pid_title) ? HTML::escape($pid_title, 'js') : '' ?><?= $pid ? HTML::escape(!empty($t2) ? $t2 : $title, 'js') : '' ?>'}"><?= $v['title'] ?><?= $v['node'] ? ' &raquo;' : '' ?></a></li>
    <? endforeach;
endif;
