<?php
/** @var $this Blog */
?>
<div class="p-profileContent">

    <? if( ! empty($prev['link']) || ! empty($next['link'])): ?>
    <div class="p-profile-title">
        <div class="p-prevnext-links">
            <? if( ! empty($prev['link'])): ?><a href="<?= $prev['link'] ?>" class="pull-left"><i class="fa fa-angle-left c-link-icon"></i><span><?= _t('blog', 'Предыдущая запись'); ?></span></a><? endif; ?>
            <? if( ! empty($next['link'])): ?><a href="<?= $next['link'] ?>" class="pull-right"><span><?= _t('blog', 'Следующая запись'); ?></span><i class="fa fa-angle-right c-link-icon"></i></a><? endif; ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <? endif; ?>

    <?= $view ?>

</div>