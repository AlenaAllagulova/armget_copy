<?php
/**
 * Форма поста (добавление / редактирование)
 * @var $this Blog
 * @var $img BlogPostImages
 */
tpl::includeJS(array('autocomplete', 'qquploader', 'ui.sortable'), true);
tpl::includeJS('blog.post.form', false, 2);
$aData = HTML::escape($aData, 'html', array('title','content'));

if ($edit) {
    $formTitle = _t('blog', 'Редактировать запись');
    $url = Blog::url('edit', array('id'=>$id));
} else {
    $formTitle = _t('blog', 'Добавить запись в блог');
    $url = Blog::url('add');
}
?>
    <div class="container">

        <section class="l-mainContent">
            <div class="row">

                <div class="col-md-8 col-md-offset-2">

                    <?= tpl::getBreadcrumbs(array(
                        array('title' => _t('','Profile'),'link'=>Users::url('my.profile')),
                        array('title' => _t('blog','Blog'),'link'=>Blog::url('my.blog')),
                        array('title' => $formTitle,'active'=>true),
                    )); ?>

                    <div class="p-profileContent">
                        <div class="p-profile-title">
                            <h4><?= $formTitle; ?></h4>
                        </div>

                        <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" id="j-post-form">
                            <input type="hidden" name="func" value="" />
                            <div class="form-group j-required">
                                <label for="pname" class="col-sm-3 control-label"><?= _t('blog', 'Заголовок'); ?> <i class="text-danger">*</i></label>
                                <div class="col-sm-9">
                                    <input name="title" type="text" class="form-control" id="pname" placeholder="<?= _t('blog', 'Введите название записи'); ?>" value="<?= $title ?>" autocomplete="off" />
                              </div>
                            </div>

                            <? if(empty($publicator)): ?>
                            <div class="form-group">
                                <label for="pdescr" class="col-sm-3 control-label"><?= _t('blog', 'Текст'); ?> <i class="text-danger">*</i></label>
                                <div class="col-sm-9">
                                    <?= tpl::jwysiwyg($content, 'content', 0, 300, array('controls' => array(
                                        'insertImageSimple' => array('visible' => false),
                                        'fullscreen'        => array('visible' => false),
                                        'fontSize'          => array('visible' => false),
                                        'fontColor'         => array('visible' => false),
                                        'blockquote'        => array('visible' => false),
                                        'html'              => array('visible' => false),
                                    ))) ?>
                              </div>
                            </div>
                            <? endif; ?>

                            <?= $this->postTags()->formFront($id); ?>

                            <div class="form-group j-images">
                                <input type="hidden" name="images_type" value="ajax" class="j-images-type-value" />
                                <input type="hidden" name="images_hash" value="<?= $imghash ?>" />
                                <label class="col-sm-3 control-label"><?= _t('blog', 'Изображения'); ?></label>

                                <div class="col-sm-9">
                                    <div id="p-gallery-show" class="j-images-type j-images-type-ajax">
                                        <ul class="p-addWork-gallery j-img-slots">

                                            <? for($i = 1; $i <= $img->getLimit(); $i++): ?>
                                                <li class="j-img-slot">
                                                    <a class="p-hasimg hidden j-img-preview">
                                                        <div class="p-galleryImg-container">
                                                            <img class="j-img-img" src="" alt="" />
                                                        </div>
                                                    </a>
                                                    <a href="#" class="p-galleryImg-remove link-delete hidden j-img-delete j-img-preview"><i class="fa fa-times-circle-o"></i></a>
                                                    <a class="j-img-upload">
                                                        <div class="p-galleryImg-container">
                                                            <i class="fa fa-plus-circle j-img-link"></i>
                                                        </div>
                                                    </a>
                                                    <input type="hidden" name="" value="" class="j-img-fn" />
                                                </li>
                                            <? endfor; ?>

                                            <div class="clearfix"></div>
                                        </ul>
                                        <div class="help-block">
                                            <?= _t('form', 'Вы можете сортировать изображения просто перетаскивая их'); ?>
                                        </div>
                                        <?= _t('form', 'Если у вас возникли проблемы воспользуйтесь <a [a_simple]><span>альтернативной формой</span></a>', array('a_simple' => 'href="#" class="ajax-link j-images-toggler" data-type="simple"')); ?>
                                    </div>

                                    <div id="p-gallery" class="p-gallery-alternate hide j-images-type j-images-type-simple">
                                        <? for($i = 1; $i <= $img->getLimit() - $imgcnt; $i++): ?>
                                            <div><input name="images_simple_<?= $i ?>" type="file" /></div>
                                        <? endfor; ?>
                                        <a href="#" class="ajax-link c-formCancel j-images-toggler" data-type="ajax"><span><?= _t('form', 'Удобная форма загрузки изображений'); ?></span></a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group j-cat">
                                <label for="inputSpec" class="col-sm-3 control-label"><?= _t('blog', 'Категория'); ?> <i class="text-danger">*</i></label>
                                <div class="col-sm-9 p-form-noinput">
                                    <?= $this->catSelect(1, $cat, array('inputName' => 'cat_id')); ?>
                                </div>
                            </div>

                            <? if( ! empty($publicator)): ?>
                                <div class="p-portfolioSection">
                                    <?= $publicator ?>
                                </div>
                            <? endif; ?>

                            <!-- comments -->
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <div class="checkbox">
                                        <label>
                                            <input name="comments_enabled" type="checkbox" <?= $comments_enabled ? ' checked="checked"' : '' ?>> <?= _t('', 'Комментарии разрешены'); ?>
                                        </label>
                                    </div>
                              </div>
                            </div>

                            <div class="form-group c-formSubmit mrgt30">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <? if($edit): ?>
                                        <? if($status == Blog::STATUS_DRAFT): ?>
                                            <button class="btn btn-primary j-submit" data-func="publicate" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('form', 'Опубликовать'); ?></button>
                                            <button class="btn btn-info j-submit" data-func="preview" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('form', 'Предпросмотр'); ?></button>
                                            <button class="btn btn-default c-formSuccess j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('form', 'Сохранить'); ?></button>
                                        <? else: ?>
                                            <button class="btn btn-primary c-formSuccess j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('form', 'Сохранить'); ?></button>
                                        <? endif; ?>
                                    <? else: ?>
                                        <button class="btn btn-primary c-formSuccess j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('blog', 'Добавить запись'); ?></button>
                                        <button class="btn btn-info j-submit" data-func="preview" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('form', 'Предпросмотр'); ?></button>
                                        <button class="btn btn-default j-submit" data-func="draft" data-loading-text="<?= _t('form', 'Подождите...') ?>"><?= _t('form', 'Сохранить в черновик'); ?></button>
                                    <? endif; ?>
                                    <a href="#" class="c-formCancel j-cancel"><?= _t('form', 'Отмена'); ?></a>
                              </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
<script type="text/javascript">
    <? js::start() ?>
    jBlogPostForm.init(<?= func::php2js(array(
        'lang' => array(
            'saved_success'       => _t('blog', 'Запись успешно сохранена'),
            'cat_wrong'           => _t('blog', 'Укажите категорию'),
            'upload_typeError'    => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
            'upload_sizeError'    => _t('form', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
            'upload_minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
            'upload_emptyError'   => _t('form', 'Файл {file} имеет некорректный размер'),
            'upload_limitError'   => _t('form', 'Вы можете загрузить не более {limit} файлов'),
            'upload_onLeave'      => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
        ),
        'url'           => $url,
        'itemID'        => $id,
        'imgLimit'      => $img->getLimit(),
        'imgMaxSize'    => $img->getMaxSize(),
        'imgUploaded'   => $imgcnt,
        'imgData'       => $images,
    )) ?>);
    <? js::stop() ?>
</script>