<?php
/**
 * Список постов пользователя
 * @var $this Blog
 * @var $user array
 */
tpl::includeJS('blog.owner.list', false, 3);
?>
    <div class="p-profileContent" id="j-blog-owner-list">

        <form method="get" action="" id="j-blog-owner-list-form">
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            <input type="hidden" name="cat" value="<?= $f['cat'] ?>" />

            <div class="p-profile-title">
                <? if($user['my']): ?>
                <div class="pull-right">
                    <a class="btn btn-primary btn-sm" href="<?= Blog::url('add'); ?>"><i class="fa fa-plus-circle"></i> <?= _t('blog', 'Добавить запись'); ?></a>
                </div>
                <? endif; ?>
                <div class="dropdown pull-left">
                    <a href="#" class="ajax-link" data-toggle="dropdown"><span class="j-f-cat-title"><?= isset($cats[ $f['cat'] ]) ? $cats[ $f['cat'] ]['title'] : '' ?></span><b class="caret"></b></a>
                    <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                        <? foreach($cats as $v): ?>
                            <li><a href="#" class="j-f-cat-select" data-id="<?= $v['id'] ?>"><?= $v['title'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>

        <div class="j-list"><?= $list ?></div>

        <div class="j-pagination"><?= $pgn ?></div>

    </div>
<script type="text/javascript">
    <? $aCat = array();
        foreach ($cats as $v) {
            $aCat[ $v['id'] ] = array( 'id' => $v['id'], 't' => $v['title']);
        }
        js::start(); ?>
    jBlogOwnerList.init(<?= func::php2js(array(
        'lang'   => array(
            'post_delete' => _t('blog', 'Удалить запись?'),
        ),
        'cats' => $aCat,
        'ajax' => true,
    )) ?>);
    <? js::stop(); ?>
</script>