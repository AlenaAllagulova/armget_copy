<?php
if( ! isset($nTagID)) $nTagID =0;
if( ! empty($list)):
?>
    <ul class="b-blogList media-list l-search-results">
    <? foreach($list as $v): ?>
        <li class="media">
            <div class="l-list-num"><?= $v['n'] ?>.</div>
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                <?= tpl::userAvatar($v) ?>
            </a>
            <div class="media-body">
                <div class="f-freelancer-name">
                    <?= tpl::userLink($v, '', 'blog'); ?>
                </div>
                <div class="b-item-info">
                    <header class="b-item-name">
                        <h2><a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a></h2>
                    </header>
                    <div class="b-date"><?= tpl::date_format_pub($v['created'], 'd.m.Y') ?></div>

                    <article class="b-blogList-description">
                        <?= $v['content_preview'] ?>
                    </article>

                    <? if( ! empty($v['aTags']) && Blog::tagsOn()):?>
                        <div class="l-project-head">
                             <? foreach($v['aTags'] as $vv):?>
                                <a href="<?= Blog::url('search-tag', $vv) ?>" class="l-tag<?= $nTagID == $vv['id'] ? ' l-search-tag' : '' ?>"><?= $vv['tag'] ?></a>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>

                    <ul class="l-item-features b-project-features">
                        <li><a href="<?= Blog::url('search-cat', $v) ?>"><i class="fa fa-th-list c-link-icon"></i><?= $v['cat1_title'] ?><?= $v['cat2_title'] ? ' / '.$v['cat2_title'] : ''?></a></li>
                        <? if($v['comments_enabled']): ?><li><a href="<?= $v['url_view'].'#comments' ?>"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['comments_cnt'], _t('', 'комментарий;комментария;комментариев'))?></span></a></li><? endif; ?>
                    </ul>
                </div>
            </div>
        </li>
    <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('blog', 'Записи не найдены'); ?></div>
<? endif;