<?php
$nUserID = User::id();
if( ! empty($list)):  ?>
    <ul class="b-blogList media-list">
        <? foreach($list as $v): ?>
        <li class="media j-post">
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                <?= tpl::userAvatar($v) ?>
            </a>
            <div class="media-body">
                <div class="f-freelancer-name">
                    <?= tpl::userLink($v, '', 'blog'); ?>
                </div>
                <div class="b-item-info">
                    <header class="b-item-name">
                        <h2><a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a></h2>
                    </header>
                    <div class="b-date"><?= tpl::date_format_pub($v['created'], 'd.m.Y') ?></div>

                    <? if($nUserID == $v['user_id']): ?>
                    <div class="p-profileOrder-controls-mobile">
                        <a href="<?= Blog::url('edit', array('id' => $v['id'])) ?>"><i class="fa fa-edit c-link-icon"></i></a>
                        <a href="#" class="j-hide" data-id="<?= $v['id'] ?>"><i class="fa fa-lock c-link-icon"></i></a>
                        <a href="#" class="link-delete j-delete" data-id="<?= $v['id'] ?>"><i class="fa fa-times c-link-icon"></i></a>
                    </div>

                    <div class="p-profileOrder-controls">
                        <a href="<?= Blog::url('edit', array('id' => $v['id'])) ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                        <a href="#" class="j-hide" data-id="<?= $v['id'] ?>"><i class="fa fa-lock c-link-icon"></i><?= _t('blog', 'Скрыть'); ?></a>
                        <a href="#" class="link-delete j-delete" data-id="<?= $v['id'] ?>"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
                    </div>
                    <? endif; ?>

                    <article class="b-blogList-description">
                        <?= $v['content_preview'] ?>
                    </article>

                    <? if($v['imgcnt']): ?>
                    <div class="b-blogList-img">
                        <img src="<?= $v['img_v'] ?>" alt="<?= tpl::imageAlt(array('t' => $v['title'])); ?>" />
                    </div>
                    <? endif; ?>

                    <ul class="l-item-features b-project-features">
                        <li><a href="<?= Blog::url('search-cat', $v) ?>"><i class="fa fa-th-list c-link-icon"></i><?= $v['cat1_title'] ?><?= $v['cat2_title'] ? ' / '.$v['cat2_title'] : ''?></a></li>
                        <? if($v['comments_enabled']): ?><li><a href="<?= $v['url_view'].'#comments' ?>"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['comments_cnt'], _t('', 'комментарий;комментария;комментариев'))?></span></a></li><? endif; ?>
                    </ul>
                </div>
            </div>
        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('blog', 'Записи не найдены'); ?></div>
<? endif;
