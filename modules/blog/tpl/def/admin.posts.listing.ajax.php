<?php
    /**
     * @var $this Blog
     */
    foreach($list as $k=>&$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>" id="dnd-<?= $id ?>">
        <td class="small"><?= $id ?></td>
        <td class="left">
            <a href="<?= Blog::url('view', array('id' => $id, 'keyword' => $v['keyword'], 'adminView' => md5($v['user_id'].$v['id'].$v['keyword']))) ?>" class="but linkout" target="_blank"></a>
            <?= $v['title'] ?>
        </td>
        <td>
            <? # для списка "на модерации", указываем причину отправления на модерацию:
            if($f['tab'] == 2) {
                if($v['moderated'] == 0) {
                    if( $v['status'] == Blog::STATUS_BLOCKED ) {
                        ?><i class="icon-ban-circle disabled" title="отредактировано пользователем после блокировки"></i><?
                    } else {
                        ?><i class="disabled" title="новая запись"></i><?
                    }
                } elseif($v['moderated'] == 2) {
                    ?><i class="icon-pencil disabled" title="отредактировано пользователем"></i><?
                }
            } ?>
        </td>
        <td><? if($v['cat_id']) { ?><a href="#" class="small desc post-category" data-id="<?= $v['cat_id'] ?>" onclick="return false;"><?= $v['cat_title'] ?></a><? } else { ?>?<? } ?></td>
        <td><?= tpl::date_format2($v['created'], false, true) ?></td>
        <td>
            <a class="but edit post-edit" title="<?= _t('', 'Edit') ?>" href="#" data-id="<?= $id ?>"></a>
            <a class="but del post-del" title="<?= _t('', 'Delete') ?>" href="#" data-id="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if( empty($list) && !isset($skip_norecords) ): ?>
    <tr class="norecords">
        <td colspan="<?= 6 ?>">
            ничего не найдено
        </td>
    </tr>
<? endif;