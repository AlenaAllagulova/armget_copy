<?php
/** @var $this Blog */
?>
<? if(!$moderated && Blog::premoderation()): ?><div class="alert alert-warning" role="alert"><?= _t('blog', 'Ожидает проверки модератора') ?></div><? endif; ?>
<? if($status == Blog::STATUS_BLOCKED): ?>
    <div class="alert alert-danger" role="alert"><b><?= _t('blog', 'Заблокирован модератором.') ?></b>
    <? if( ! empty($blocked_reason)): ?><br /><?= _t('', 'Причина блокировки:'); ?> <?= tpl::blockedReason($blocked_reason) ?><? endif; ?>
    </div>
<? endif; ?>