<?php
/** @var $this Blog */
tpl::includeJS('blog.view', false, 2);
$owner = User::isCurrent($user_id);
$bAvatar = ! empty($avatar_small);
?>
<div class="b-blog-article" id="j-post-view">
    <? if($bAvatar): ?>
        <div class="media">
            <a href="<?= Users::url('profile', array('login' => $login)); ?>" class="f-freelancer-avatar">
                <img src="<?= $avatar_small ?>" alt="<?= tpl::avatarAlt($aData); ?>" /><?= tpl::userOnline($aData) ?>
            </a>
            <div class="media-body">

            <div class="f-freelancer-name">
                <?= tpl::userLink($aData) ?>
            </div>

            <header class="b-item-name">
                <h1><? if($owner): ?><i class="fa fa-lock <?= $status != Blog::STATUS_HIDDEN ? 'hidden' : ''?> j-fa-hide"></i><? endif; ?> <?= $title ?></h1>
            </header>
            <div class="b-item-info">
                <span class="b-date"><i class="fa fa-calendar"></i> <?= tpl::date_format_pub($created, 'd.m.Y') ?></span>,
                <span class="c-viewed"><i class="fa fa-eye"></i> <?= _t('', 'Просмотров: [total]', array('total'=>$views_total)); ?></span>
            </div>
    <? else: ?>
            <div class="b-item-info">
                <span class="b-date"><i class="fa fa-calendar"></i> <?= tpl::date_format_pub($created, 'd.m.Y') ?></span>,
                <span class="c-viewed"><i class="fa fa-eye"></i> <?= _t('', 'Просмотров: [total]', array('total'=>$views_total)); ?></span>
            </div>
            <h1><? if($owner): ?><i class="fa fa-lock <?= $status != Blog::STATUS_HIDDEN ? 'hidden' : ''?> j-fa-hide"></i><? endif; ?> <?= $title ?></h1>
    <? endif; ?>


            <?= $content ?>

            <? if($imgcnt): ?>
                <div class="b-blogList-img">
                    <? foreach($images as $k => $v): ?>
                        <p><img src="<?= $v['i'][ BlogPostImages::szView ]; ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" /></p>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
    <? if($bAvatar): ?>
        </div>
    <? endif; ?>
        <div class="row">

            <div class="col-sm-8">
                <? if( ! empty($tags)): foreach($tags as $v):?>
                    <a href="<?= Blog::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                <? endforeach; endif; ?>
            </div>

            <div class="col-sm-4">
                <div class="b-social-icons text-right pdr10">
                    <? $aConfig = $this->configLoad(); ?>
                    <?= ! empty($aConfig['share_code']) ? $aConfig['share_code'] : '' ?>
                </div>
            </div>

        </div>

        <? if($owner && $status != Blog::STATUS_DRAFT): ?>
            <div class="l-spacer"></div>
            <?= $this->postStatusBlock($aData); ?>
            <div class="p-profileOrder-controls-mobile">
                <a href="<?= Blog::url('edit', array('id' => $id)) ?>"><i class="fa fa-edit c-link-icon"></i></a>
                <a href="#" class="<?= $status != Blog::STATUS_ACTIVE ? 'hidden ' : '' ?>j-hide"><i class="fa fa-lock c-link-icon"></i></a>
                <a href="#" class="<?= $status != Blog::STATUS_HIDDEN ? 'hidden ' : '' ?>j-show"><i class="fa fa-unlock c-link-icon"></i></a>
                <a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i></a>
            </div>

            <div class="p-profileOrder-controls">
                <a href="<?= Blog::url('edit', array('id' => $id)) ?>"><i class="fa fa-edit c-link-icon"></i><?= _t('form', 'Редактировать'); ?></a>
                <a href="#" class="<?= $status != Blog::STATUS_ACTIVE ? 'hidden ' : '' ?>j-hide"><i class="fa fa-lock c-link-icon"></i><?= _t('blog', 'Скрыть'); ?></a>
                <a href="#" class="<?= $status != Blog::STATUS_HIDDEN ? 'hidden ' : '' ?>j-show"><i class="fa fa-unlock c-link-icon"></i><?= _t('blog', 'Показать'); ?></a>
                <a href="#" class="link-delete j-delete"><i class="fa fa-times c-link-icon"></i><?= _t('form', 'Удалить'); ?></a>
            </div>
        <? endif; ?>

    <? if($owner && $status == Blog::STATUS_DRAFT): ?>
        <div class="l-funcBar">
            <div class="container">
                <div class="l-funcBar-in">
                    <div class="l-funcBar-l">
                        <strong><?= _t('form', 'Данная запись находится в черновике'); ?></strong>
                    </div>
                    <div class="l-funcBar-r">
                        <a href="#" class="btn btn-primary j-show"><?= _t('form', 'Опубликовать'); ?></a>
                        <a href="<?= Blog::url('edit', array('id' => $id)) ?>" class="btn btn-info"><?= _t('form', 'Редактировать'); ?></a>
                        <a href="#" class="btn btn-danger j-delete"><?= _t('form', 'Удалить'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <? endif; ?>

</div>

<a name="comments"></a>
<div id="j-comments"><?= $this->viewPHP($aData, 'view.post.comments'); ?></div>

<script type="text/javascript">
    <? js::start() ?>
    jBlogPostView.init(<?= func::php2js(array(
        'lang'   => array(
            'delete' => _t('blog', 'Удалить запись?'),
        ),
        'owner' => $owner,
        'id'    => $id,
    )) ?>);
    <? js::stop() ?>
</script>