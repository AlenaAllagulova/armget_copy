<?php
/**
 * Список постов
 * @var $this Blog
 */

tpl::includeJS('blog.search', false, 2);
$nUserID = User::id();
?>
    <div class="container">

        <section class="l-searchAdd">
            <div class="row">
                <? if($nUserID): ?>
                <div class="col-sm-3 add-button">
                    <a href="<?= Blog::url('add')?>" class="btn btn-primary btn-block"><i class="fa fa-plus-circle hidden-sm"></i> <?= _t('blog', 'Добавить запись'); ?></a>
                </div>
                <? endif; ?>
                <div class="col-sm-<?= $nUserID ? 9 : 12 ?> l-content-column">
                    <div class="l-search-bar">
                        <form role="search" method="get" action="<?= Blog::url('list') ?>">
                            <input name="q" type="search" class="form-control" placeholder="<?= _t('', 'Найти запись'); ?>">
                            <button class="l-search-button"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="l-mainContent" id="j-blog-search">
            <div class="row">
                <aside class="col-md-3">
                    <?= $form ?>
                    <? # Баннер: Блоги: список ?>
                    <?= Banners::view('blog_list', array('pos'=>'left', 'cat'=>$cat_id)) ?>
                </aside>
                <div class="col-md-9 l-content-column" id="j-blog-search-list">
                    <div class="j-list"><?= $list ?></div>
                    <div class="j-pagination"><?= $pgn ?></div>
                </div>
            </div>
        </section>

    </div>
<script type="text/javascript">
    <? js::start() ?>
    jBlogSearch.init(<?= func::php2js(array(
        'lang'    => array(
            'post_delete'   => _t('blog', 'Удалить запись?'),
        ),
        'ajax'    => false,
        'cat_id'  => $cat_id,
    )) ?>);
    <? js::stop() ?>
</script>