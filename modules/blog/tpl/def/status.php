<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <? if ($status == Blog::STATUS_DRAFT) { # черновик ?>
                    <h1 class="small"><?= _t('blog', 'Ваша запись сохранена в черновики.') ?></h1>
                    <p>
                        <?= _t('blog', 'Ваша запись сохранена, но еще не опубликована.<br> Вы можете увидеть и опубликовать ее в <a [link_cabinet]>вашем кабинете</a>.',
                            array('link_cabinet' => 'href="'.Blog::url('my.blog', array('cat' => $cat_id1)).'"')) ?><br />
                        <?= _t('blog', 'После публикации, запись появится в блоге.') ?><br />
                    </p>
                    <p><a href="<?= Blog::url('view', array('id' => $id, 'keyword' => $keyword)) ?>"><?= _t('blog', 'Просмотреть запись') ?></a></p>
                    <p><a href="<?= Blog::url('add') ?>"><?= _t('blog', 'Добавить еще одну запись') ?></a></p>
                    <p><a href="<?= Blog::url('my.blog') ?>"><?= _t('blog','Перейти на страницу списка записей') ?></a></p>
                <? }else if (Blog::premoderation()) { # включена премодерация ?>
                    <h1 class="small"><?= _t('blog', 'Ваша запись будет опубликована после проверки модератором') ?></h1>
                    <p>
                        <?= _t('blog', 'Ваша запись сохранена, но еще не опубликована.<br> Вы можете увидеть и отредактировать ее в <a [link_cabinet]>вашем кабинете</a>.',
                            array('link_cabinet' => 'href="'.Blog::url('my.blog', array('cat' => $cat_id1)).'"')) ?><br />
                        <?= _t('blog', 'Как только модератор проверит корректность введенных вами данных, запись появится в блоге.') ?><br />
                    </p>
                    <p><a href="<?= Blog::url('add') ?>"><?= _t('blog', 'Добавить еще одну запись') ?></a></p>
                    <p><a href="<?= Blog::url('my.blog') ?>"><?= _t('blog','Перейти на страницу списка записей') ?></a></p>
                <? } else { ?>
                    <h1 class="small"><?= _t('blog', 'Ваша запись успешно опубликована.') ?></h1>
                    <p>
                    <?= _t('blog', 'Чтобы увидеть ее в общем списке, перейдите на <a [link_list]>страницу блога</a>.',
                            array('link_list' => 'href="'.Blog::url('my.blog').'"')) ?><br />
                    <?= _t('blog', 'Вы также можете отредактировать ее в <a [link_cabinet]>вашем кабинете</a>.',
                            array('link_cabinet' => 'href="'.Blog::url('my.blog', array('cat' => $cat_id1)).'"')) ?><br />
                    </p>
                    <p><a href="<?= Blog::url('add') ?>"><?= _t('blog', 'Добавить еще одну запись') ?></a></p>
                <? } ?>
            </div>
        </div>
    </section>
</div>
