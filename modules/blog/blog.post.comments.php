<?php

class BlogPostComments_ extends bff\db\Comments
{
    function initSettings()
    {
        $this->tblComments = TABLE_BLOG_COMMENTS;
        $this->tblItems = TABLE_BLOG_POSTS;
        $this->tblItemsID = 'id';

        $this->preModeration = false; # постмодерация
        $this->commentsTree = true;
        # config-ключ для хранения общего счетчика непромодерированных комментариев
        $this->counterKey_UnmoderatedAll = 'blog_comments_mod';
        $this->counterKey_ItemComments = 'comments_cnt';
        $this->commentHideReasons[self::commentDeletedByModerator] = _t('', 'Удален модератором');
        $this->commentHideReasons[self::commentDeletedByCommentOwner] = _t('', 'Удален автором комментария');
        $this->commentHideReasons[self::commentDeletedByItemOwner] = _t('blog', 'Удален владельцем записи');
        $this->commentHideReasons[self::commentFromBlockedUser] = _t('', 'Комментарий от заблокированного пользователя');

        $this->urlListing = $this->adminLink('posts&act=edit&ftab=comments&id=', 'blog');
        $this->urlListingAjax = $this->adminLink('comments_ajax', 'blog');
    }

    /**
     * Получение данных обо всех комментариях к записи
     * - в случае с frontend'ом и включенной preModeration: исключаем непромодерированные
     * @param integer $nItemID ID записи
     * @param integer $nCommentLastID ID комментария, с которого следует начать выборку
     * @param boolean $bSimpleTree простое дерево (выполняем только transformRowsToTree)
     * @param integer $nCommentTreeID ID комментария, с которого начать выбор ветки
     * @return array
     */
    public function commentsData($nItemID, $nCommentLastID = 0, $bSimpleTree = false, $nCommentTreeID = 0)
    {
        $sql = array();
        $bind = array(':itemID'=>$nItemID);
        if ( $nCommentLastID > 0 ) {
            $sql[] = 'C.id > :lastID';
            $bind[':lastID'] = $nCommentLastID;
        }
        if ($nCommentTreeID > 0) {
            $sql[] = 'C.id >= :treeID';
            $bind[':treeID'] = $nCommentTreeID;
        }

        # в случае с frontend'ом и включенной preModeration: исключаем непромодерированные
        if ( ! \bff::adminPanel() ) {
            if ( $this->preModeration ) {
                $sql[] = 'C.moderated = 1';
            }
        }
        # учитываем ID группы
        if ( $this->isGroupUsed() ) {
            $sql[] = 'C.'.$this->tblCommentsGroupID.' = :groupID';
            $bind[':groupID'] = $this->groupID;
        }
        $aData = $this->db->select('SELECT C.*, U.login, U.name AS uname, U.surname, U.pro, U.avatar, U.verified, U.sex, U.blocked as ublocked,
                    US.last_activity
                FROM '.$this->tblComments.' C
                    LEFT JOIN '.TABLE_USERS.' U ON C.user_id = U.user_id
                    LEFT JOIN '.TABLE_USERS_STAT.' US ON C.user_id = US.user_id
                WHERE C.item_id = :itemID'.( ! empty($sql) ? ' AND '.join(' AND ', $sql) : ' ' ).'
                ORDER BY C.id ASC', $bind);

        if( ! empty($aData) ) {
            foreach ($aData as $k=>$v) {
                if($v['ublocked'] && ! $v['deleted']) {
                    $aData[$k]['deleted'] = self::commentFromBlockedUser;
                }
            }
            if ($bSimpleTree) {
                if ($nCommentTreeID) {
                    $aData = $this->db->transformRowsToTree($aData, 'id', 'pid', 'sub');
                    if (isset($aData[$nCommentTreeID])) {
                        return array(
                            'aComments' => array($nCommentTreeID => $aData[$nCommentTreeID]),
                        );
                    }
                }
                return array(
                    'total' => sizeof($aData),
                    'aComments' => $this->db->transformRowsToTree($aData, 'id', 'pid', 'sub'),
                );
            }
            return $this->buildCommentsRecursive($aData);
        }
        return array('aComments'=>array(), 'nMaxIdComment'=>0, 'total'=>0);
    }

    /**
     * Восстановление комментария
     * @param $nCommentID
     */
    public function restore($nCommentID)
    {
        $aUpdate = array(
            'deleted' => 0,
            'moderated' => 0,
        );
        $this->db->update($this->tblComments, $aUpdate, array('id'=>$nCommentID));
        $this->updateUnmoderatedAllCounter(1);
    }
}