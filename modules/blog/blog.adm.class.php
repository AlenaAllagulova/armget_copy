<?php

/**
 * Права доступа группы:
 *  - blog: Блог
 *      - posts: Управление постами (список, добавление, редактирование, удаление)
 *      - posts-moderate: Модерация постов (блокирование/одобрение/продление/активация)
 *      - posts-comments: Управление комментариями
 *      - tags: Управление тегами (список, добавление, редактирование, удаление)
 *      - categories: Управление категориями (список, добавление, редактирование, удаление)
 *      - settings: Настройки
 *      - seo: SEO
 */
class Blog_ extends BlogBase
{
    public function posts()
    {
        if (!($this->haveAccessTo('posts') || $this->haveAccessTo('posts-moderate'))) {
            return $this->showAccessDenied();
        }

        $accessManage = $this->haveAccessTo('posts');

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validatePostData(0, $bSubmit, $publicator);
                    if ($bSubmit) {
                        if ($this->errors->no())
                        {

                            $aData['moderated'] = 1;
                            $nPostID = $this->model->postSave(0, $aData);
                            if ($nPostID > 0) {
                                # теги
                                if (static::tagsOn()) {
                                    $this->postTags()->tagsSave($nPostID);
                                }
                                if ($publicator) {
                                    # переносим фотографии в постоянную папку
                                    $publicator->dataUpdate($aData['content'], $nPostID);
                                }
                                # загружаем изображения
                                if (static::imagesLimit()) {
                                    # сохраняем / загружаем изображения
                                    # перемещаем из tmp-директории в постоянную
                                    $this->postImages($nPostID)->saveTmp('img');
                                }
                            }
                        }
                        break;
                    } else {
                        $aData['id'] = 0;
                        $aData['cats'] = $this->model->categoriesOptions($aData['cat_id'], array('empty' => 'Выбрать'));
                        $aData['images'] = array();
                        $aData['imgcnt'] = 0;
                        $aData['img'] = $this->postImages(0);
                        $aData['act'] = $sAct;
                        $aData['publicator'] = $publicator;
                        $aResponse['form'] = $this->viewPHP($aData, 'admin.posts.form');
                    }
                }
                break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nPostID = $this->input->postget('id', TYPE_UINT);
                    if (!$nPostID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        if (!$accessManage) {
                            $this->errors->accessDenied(); break;
                        }
                        $aData = $this->validatePostData($nPostID, $bSubmit, $publicator);
                        if ($this->errors->no()) {

                            $this->model->postSave($nPostID, $aData);
                            # теги
                            if (static::tagsOn()) {
                                $this->postTags()->tagsSave($nPostID);
                            }
                        }
                        $aData['id'] = $nPostID;
                        break;
                    } else {
                        $aData = $this->model->postData($nPostID, array(), true);
                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                        $aData['cats'] = $this->model->categoriesOptions($aData['cat_id'],
                            (!$aData['cat_id'] ? array('empty' => 'Выбрать') : false)
                        );
                        $aData['img'] = $this->postImages($nPostID);
                        $aData['images'] = $aData['img']->getData($aData['imgcnt']);
                        if ($publicator && empty($aData['content_search'])) {
                            $aData['content'] = $publicator->textConvert($aData['content']);
                        }
                        $aData['act'] = $sAct;
                        $aData['publicator'] = $publicator;
                        $aResponse['form'] = $this->viewPHP($aData, 'admin.posts.form');
                    }

                }
                break;
                case 'delete':
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nPostID = $this->input->postget('id', TYPE_UINT);
                    if (!$nPostID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->postData($nPostID, array(), true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->postDelete($nPostID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                break;
                case 'tags-suggest': # autocomplete.fb
                {
                    $sQuery = $this->input->postget('tag', TYPE_STR);
                    $this->postTags()->tagsAutocomplete($sQuery);
                }
                break;
                case 'tags-autocomplete': # autocomplete
                {
                    $sQuery = $this->input->post('q', TYPE_STR);
                    $this->postTags()->tagsAutocomplete($sQuery);
                }
                break;
                case 'user': # пользователь
                {
                    $sEmail = $this->input->post('q', TYPE_NOTAGS);
                    $sEmail = $this->input->cleanSearchString($sEmail);
                    $aUsers = Users::model()->usersList(array(
                            ':email'    => array(
                                (Users::model()->userEmailCrypted() ? 'BFF_DECRYPT(email)' : 'email') . ' LIKE :email',
                                ':email' => $sEmail . '%'
                            ),
                            'blocked'   => 0,
                            'activated' => 1,
                        ), array('user_id', 'email')
                    );
                    $this->autocompleteResponse($aUsers, 'user_id', 'email');
                }
                break;
                case 'img-upload': # изображения: загрузка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        $this->ajaxResponse(array('success' => false, 'errors' => $this->errors->get()), true, false, true);
                    }

                    $nPostID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->postImages($nPostID);
                    $mResult = $oImages->uploadQQ();
                    $aResponse = array('success' => ($mResult !== false && $this->errors->no()));

                    if ($mResult !== false) {
                        $aResponse = array_merge($aResponse, $mResult);
                        $aResponse = array_merge($aResponse,
                            $oImages->getURL($mResult,
                                array(BlogPostImages::szForm, BlogPostImages::szView),
                                empty($nPostID)
                            )
                        );
                    }
                    $aResponse['errors'] = $this->errors->get();
                    $this->ajaxResponse($aResponse, true, false, true);
                }
                break;
                case 'img-saveorder': # изображения: сохранение порядка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nPostID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->postImages($nPostID);
                    $img = $this->input->post('img', TYPE_ARRAY);
                    if (!$oImages->saveOrder($img, false, true)) {
                        $this->errors->impossible();
                    }
                }
                break;
                case 'img-delete': # изображения: удаление
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nPostID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->postImages($nPostID);
                    $nImageID = $this->input->post('image_id', TYPE_UINT);
                    $sFilename = $this->input->post('filename', TYPE_STR);
                    if (!$nImageID && empty($sFilename)) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($nImageID) {
                        $oImages->deleteImage($nImageID);
                    } else {
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'img-delete-all': # изображения: удаление всех изображений
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nPostID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->postImages($nPostID);
                    if ($nPostID) {
                        $oImages->deleteAllImages(true);
                    } else {
                        $sFilename = $this->input->post('filenames', TYPE_ARRAY_STR);
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'dev-posts-counters-update':
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }
                    $this->moderationCounterUpdate();
                    $this->adminRedirect(($this->errors->no() ? Errors::SUCCESS : Errors::IMPOSSIBLE), bff::$event);
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isPOST()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'cat'     => TYPE_UINT,
            'cat_sub' => TYPE_BOOL,
            'tab'     => TYPE_UINT,
            'tag'     => TYPE_UINT,
            'user'    => TYPE_NOTAGS,
            'q'       => TYPE_NOTAGS,
        ));

        # формируем фильтр списка постов
        $sql = array();
        $sqlOrder = 'created DESC';
        $mPerpage = 15;
        $aData['pgn'] = '';

        switch ($f['tab'])
        {
            case 0: # Активные
            {
                $sql['status'] = static::STATUS_ACTIVE;
                if (static::premoderation()) {
                    $sql[':mod'] = 'P.moderated > 0';
                }
            } break;
            case 1: # Скрытые
            {
                $sql['status'] = static::STATUS_HIDDEN;
            } break;
            case 2: # На модерации
            {
                $sql[':mod'] = 'P.moderated != 1';
            } break;
            case 3: # Черновики
            {
                $sql['status'] = static::STATUS_DRAFT;
            } break;
            case 4: # Заблокированные
            {
                $sql['status'] = static::STATUS_BLOCKED;
            } break;
            case 5: # Все
            {
            } break;
        }

        if ($f['cat'] > 0) {
            if ($f['cat_sub']) {
                $nCatLevel = $this->model->categoryData($f['cat'], false, array('numlevel'));
                $sql[':cat_id'] = array('(P.cat_id'.$nCatLevel.' = :cat)', ':cat' => $f['cat']);
            } else {
                $sql[':cat_id'] = array('(P.cat_id = :cat)', ':cat' => $f['cat']);
            }
        }

        if ($f['tag'] > 0) {
            $aData['tag'] = $this->postTags()->tagData($f['tag']);
            if (empty($aData['tag'])) {
                $f['tag'] = 0;
            }
        }

        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(P.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }

        if (!empty($f['q'])) {
            $sql[':q'] = array(
                '(P.id = ' . intval($f['q']) . ' OR P.title LIKE :q)',
                ':q' => '%' . $f['q'] . '%'
            );
        }

        $aData['f'] = $f;
        if ($mPerpage !== false) {
            $nCount = $this->model->postsListing($sql, $f['tag'], true);
            $oPgn = new Pagination($nCount, $mPerpage, '#', 'jBlogPostsList.page('.Pagination::PAGE_ID.'); return false;');
            $oPgn->pageNeighbours = 6;
            $aData['pgn'] = $oPgn->view(array('arrows'=>false));
            $aData['list'] = $this->model->postsListing($sql, $f['tag'], false, $oPgn->getLimitOffset(), $sqlOrder);
        } else {
            $aData['list'] = $this->model->postsListing($sql, $f['tag'], false, '', $sqlOrder);
        }

        $aData['list'] = $this->viewPHP($aData, 'admin.posts.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                    'pgn'  => $aData['pgn'],
                )
            );
        }

        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        $aData['cats'] = $this->model->categoriesOptions($f['cat'], array('Все категории'));

        tpl::includeJS(array('ui.sortable', 'qquploader', 'autocomplete', 'autocomplete.fb'), true);
        $this->postComments()->admListingIncludes();

        return $this->viewPHP($aData, 'admin.posts.listing');
    }

    public function post_status()
    {
        if ( ! $this->haveAccessTo('posts-moderate')) {
            $this->errors->accessDenied();
        }


        $nPostID = $this->input->post('id', TYPE_UINT);
        if (!$nPostID) {
            $this->errors->unknownRecord();
        } else {
            $aData = $this->model->postData($nPostID, array(), true);
            if (empty($aData)) {
                $this->errors->unknownRecord();
            }
        }
        if ( ! $this->errors->no()) {
            $this->ajaxResponseForm();
        }

        $aResponse = array();
        switch ($this->input->postget('act', TYPE_STR))
        {
            case 'approve': # модерация: одобрение
            {
                $aUpdate = array(
                    'moderated' => 1
                );

                if ($aData['status'] == self::STATUS_BLOCKED) {
                    /**
                     * В случае если "Одобряем" заблокированный пост
                     * => значит он после блокировки был отредактирован пользователем
                     */
                    $aUpdate[] = 'status_prev = status';
                    $aUpdate['status'] = ( in_array($aData['status_prev'], array(
                        self::STATUS_ACTIVE,
                        self::STATUS_HIDDEN,
                    )) ? $aData['status_prev'] : self::STATUS_ACTIVE );
                    $aUpdate['status_changed'] = $this->db->now();
                }

                $res = $this->model->postSave($nPostID, $aUpdate);

                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик постов "на модерации"
                $this->moderationCounterUpdate();

                # Отправим письмо пользователю
                Users::sendMailTemplateToUser($aData['user_id'], 'blog_post_approved', array(
                    'post_id' => $nPostID,
                    'post_title' => $aData['title'],
                    'post_url' => static::url('view', array('id' => $nPostID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            }
            break;
            case 'activate': # модерация: активация
            {
                if ($aData['status'] != self::STATUS_HIDDEN) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_ACTIVE,
                    'status_changed' => $this->db->now(),
                    'moderated' => 1,
                );

                $res = $this->model->postSave($nPostID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик постов "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'deactivate': # модерация: деактивация
            {
                if ($aData['status'] != self::STATUS_ACTIVE) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_HIDDEN,
                    'status_changed' => $this->db->now(),
                    'moderated' => 1,
                );

                $res = $this->model->postSave($nPostID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик постов "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'block': # модерация: блокировка / разблокировка
            {
                /**
                 * Блокировка поста (если уже заблокирован => изменение причины блокировки)
                 * @param string 'blocked_reason' причина блокировки
                 * @param integer 'id' ID поста
                 */

                $bUnblock = $this->input->post('unblock', TYPE_UINT);
                $sBlockedReason = $this->input->postget('blocked_reason', TYPE_NOTAGS, array('len' => 1000, 'len.sys' => 'blog.blocked_reason.limit'));
                $bBlocked = ($aData['status'] == self::STATUS_BLOCKED);

                if ($aData['user_id']) {
                    $aUserData = Users::model()->userData($aData['user_id'], array('user_id', 'blocked'));
                    if (!empty($aUserData['blocked'])) {
                        $this->errors->set('Для блокировки/разблокировки поста, разблокируйте аккаунт владельца данного поста');
                        break;
                    }
                }

                $aUpdate = array(
                    'moderated'      => 1,
                    'blocked_reason' => $sBlockedReason,
                    'status_changed' => $this->db->now(),
                );

                $bBlockedResult = $bBlocked;
                if (!$bBlocked) { # блокируем
                    $aUpdate['status_prev'] = $aData['status'];
                    $aUpdate['status'] = self::STATUS_BLOCKED;
                    $bBlockedResult = true;

                    # Отправим письмо пользователю
                    Users::sendMailTemplateToUser($aData['user_id'], 'blog_post_blocked', array(
                        'post_id' => $nPostID,
                        'post_title' => $aData['title'],
                        'post_url' => static::url('view', array('id' => $nPostID, 'keyword' => $aData['keyword'])),
                        'blocked_reason' => nl2br($sBlockedReason),
                    ), Users::ENOTIFY_GENERAL);

                } else {
                    if ($bUnblock) {
                        # разблокируем
                        switch ($aData['status_prev']) {
                            case self::STATUS_ACTIVE:
                            case self::STATUS_HIDDEN:
                                $aUpdate['status'] = $aData['status_prev'];
                                break;
                            case self::STATUS_BLOCKED:
                                $aUpdate['status'] = self::STATUS_HIDDEN;
                                break;
                        }
                        $aUpdate['status_prev'] = self::STATUS_BLOCKED;
                        $bBlockedResult = false;

                        # Отправим письмо пользователю
                        Users::sendMailTemplateToUser($aData['user_id'], 'blog_post_approved', array(
                            'post_id' => $nPostID,
                            'post_title' => $aData['title'],
                            'post_url' => static::url('view', array('id' => $nPostID, 'keyword' => $aData['keyword'])),
                        ), Users::ENOTIFY_GENERAL);
                    }
                }

                $res = $this->model->postSave($nPostID, $aUpdate);
                if (!$res) {
                    $this->errors->impossible();
                    break;
                }
                # обновляем счетчик постов "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['blocked'] = $bBlockedResult;
                $aResponse['reason'] = $sBlockedReason;
            }
            break;
        }

        if ($this->errors->no()) {
            $aData = $this->model->postData($nPostID, array(), true);
            if ( ! empty($aData)) {
                $aData['only_form'] = true;
                $aResponse['html'] = $this->viewPHP($aData, 'admin.posts.form.status');
            }
        }
        $this->ajaxResponseForm($aResponse);
    }
    
    public function tags()
    {
        if (!$this->haveAccessTo('tags')) {
            return $this->showAccessDenied();
        }

        return $this->postTags()->manage();
    }

    public function categories()
    {
        if (!$this->haveAccessTo('categories')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateCategoryData(0, $bSubmit);
                    if ($bSubmit) {
                        if ($this->errors->no()) {

                            $this->model->categorySave(0, $aData);
                        }
                        break;
                    }

                    $aData['id'] = 0;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                }
                break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        $aData = $this->validateCategoryData($nCategoryID, $bSubmit);
                        if ($this->errors->no()) {

                            $this->model->categorySave($nCategoryID, $aData);
                        }
                        $aData['id'] = $nCategoryID;
                        break;
                    } else {
                        $aData = $this->model->categoryData($nCategoryID, true);
                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                        $aData['pid_path'] = $this->model->categoryParentsTitle($nCategoryID);
                    }

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                }
                break;
                case 'expand':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nCategoryID) { $this->errors->unknownRecord(); break; }

                    $aData['list'] = $this->model->categoriesListing( array('pid'=>$nCategoryID) );
                    $aData['skip_norecords'] = false;
                    $aResponse['list'] = $this->viewPHP( $aData, 'admin.categories.listing.ajax' );
                    $aResponse['cnt']  = sizeof($aData['list']);
                } break;
                case 'toggle':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->categoryToggle($nCategoryID, $sToggleType);
                }
                break;
                case 'rotate':
                {

                    $this->model->categoriesRotate();
                }
                break;
                case 'dev-delete-all':
                {
                    if (!FORDEV || !BFF_DEBUG) {
                        return $this->showAccessDenied();
                    }

                    $this->model->categoriesDeleteAll();
                    $this->adminRedirect(Errors::SUCCESS, 'categories');
                }
                break;
                case 'dev-treevalidate':
                {
                    # валидация целостности NestedSets категорий
                    if (!FORDEV || !BFF_DEBUG) {
                        return $this->showAccessDenied();
                    }

                    return $this->model->treeCategories->validate(true);
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = array();
        # формируем фильтр списка категорий
        $sql = array();
        $sqlOrder = 'numleft';
        $aData['pgn'] = '';

        $sExpandState = $this->input->cookie(bff::cookiePrefix().'blog_categories_expand', TYPE_STR);
        $aExpandID = ( ! empty($sExpandState) ? explode('.', $sExpandState) : array());
        $aExpandID = array_map('intval', $aExpandID); $aExpandID[] = 1;
        $sql[] = 'pid IN ('.join(',', $aExpandID).')';

        $aData['list'] = $this->model->categoriesListing($sql, false, '', $sqlOrder);
        $aData['list'] = $this->viewPHP($aData, 'admin.categories.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list'=>$aData['list'],
            ));
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        return $this->viewPHP($aData, 'admin.categories.listing');
    }

    public function categories_delete()
    {
        if (!$this->haveAccessTo('categories')) {
            return $this->showAccessDenied();
        }

        $nCategoryID = $this->input->getpost('id', TYPE_UINT);
        if (!$nCategoryID) {
            $this->adminRedirect(Errors::IMPOSSIBLE, 'categories');
        }

        $aData = $this->model->categoryData($nCategoryID, true);
        if (!$aData) {
            $this->adminRedirect(Errors::IMPOSSIBLE, 'categories');
        }

        if (Request::isPOST())
        {

            $nNextCategoryID = $this->input->post('next', TYPE_UINT);
            if ($nNextCategoryID > 0) {
                # проверяем наличие категории
                $aDataNext = $this->model->categoryData($nNextCategoryID);
                if (empty($aDataNext) || $nNextCategoryID == $nCategoryID) {
                    $this->adminRedirect(Errors::IMPOSSIBLE, 'categories');
                }

                # перемещаем посты
                $bResult = $this->model->postsMoveToCategory($nNextCategoryID, $nCategoryID);
                if (!empty($bResult)) {
                    # удаляем категорию
                    $this->model->categoryDelete($nCategoryID);
                }
            } else {
                if (!$aData['posts']) {
                    # удаляем категорию
                    $this->model->categoryDelete($nCategoryID);
                }
            }

            $this->adminRedirect(Errors::SUCCESS, 'categories');
        }

        $aData['categories'] = $this->model->categoriesOptions(0, 'Выбрать');

        return $this->viewPHP($aData, 'admin.categories.delete');
    }

    /**
     * Валидация данных категории
     * @param integer $nCategoryID ID категории или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateCategoryData($nCategoryID, $bSubmit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langCategories, $aData);
        $this->input->postm(array(
            'pid'       => TYPE_UINT,   # Основной раздел
            'keyword'   => TYPE_NOTAGS, # URL-Keyword
            'enabled'   => TYPE_BOOL,   # Включена
            'mtemplate' => TYPE_BOOL,   # Использовать базовый шаблон SEO
        ), $aData);

        if ($bSubmit) {
            if ( ! $aData['pid']) {
                $this->errors->set(_t('blog', 'Укажите категорию'));
            }
            # URL-Keyword
            $aData['keyword'] = $this->db->getKeyword($aData['keyword'], $aData['title'][LNG], TABLE_BLOG_CATEGORIES, $nCategoryID, 'keyword', 'id');
        } else {
            if (!$nCategoryID) {
                $aData['mtemplate'] = 1;
            }
        }

        return $aData;
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'share_code' => TYPE_STR,
            ), $aData);

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad();
        if( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

    public function comments_ajax()
    {
        if (!$this->haveAccessTo('posts-comments')) {
            return $this->showAccessDenied();
        }

        $this->postComments()->admAjax();
    }

    public function comments_mod()
    {
        if (!$this->haveAccessTo('posts-comments')) {
            return $this->showAccessDenied();
        }

        return $this->postComments()->admListingModerate(15);
    }

}