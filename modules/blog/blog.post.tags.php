<?php
class BlogPostTags_ extends bff\db\Tags
{
    protected function initSettings()
    {
        $this->tblTags = TABLE_BLOG_TAGS;
        $this->postModeration = true;
        $this->tblTagsIn = TABLE_BLOG_POSTS_TAGS;
        $this->tblTagsIn_ItemID = 'post_id';
        $this->urlItemsListing = $this->adminLink('posts&tag=', 'blog');
    }

    public function formFront($nItemID, $aParams = array())
    {
        if ( ! Blog::tagsOn()) return '';

        $aData = array(
            'tags'   => ($nItemID > 0 ? $this->tagsGet($nItemID) : array()),
            'params' => $aParams,
        );

        return $this->viewPHP($aData, 'form.tags');
    }
}