<?php

class M_Blog_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = _t('blog','Blog');
        $module = 'blog';
        # Посты
        if ($security->haveAccessToModuleToMethod($module, 'posts') ||
            $security->haveAccessToModuleToMethod($module, 'posts-moderate')) {
            $menu->assign($menuTitle, 'Посты', $module, 'posts', true, 10,
                array('rlink' => array('event' => 'posts&act=add'), 'counter' => 'blog_posts_moderating')
            );
            $menu->adminHeaderCounter($menuTitle, 'blog_posts_moderating', $module, 'posts&tab=2', 9, '', array('parent'=>'moderation'));
        }

        # Комментарии
        if ($security->haveAccessToModuleToMethod($module, 'posts-comments')) {
            $menu->assign($menuTitle, 'Комментарии', $module, 'comments_mod', true, 15, array('counter' => 'blog_comments_mod'));
            $menu->adminHeaderCounter($menuTitle, 'blog_comments_mod', $module, 'comments_mod', 9, '', array('parent'=>'comments'));
        }

        # Категории
        if ($security->haveAccessToModuleToMethod($module, 'categories')) {
            $menu->assign($menuTitle, 'Категории', $module, 'categories', true, 20,
                array('rlink' => array('event' => 'categories&act=add'))
            );
        }

        # Теги
        if (Blog::tagsOn() && $security->haveAccessToModuleToMethod($module, 'tags')) {
            $menu->assign($menuTitle, _t('','Tags'), $module, 'tags', true, 30);
        }

        # Настройки
        if ($security->haveAccessToModuleToMethod($module, 'settings')) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 50);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module, 'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 35);
        }

    }
}