<?php

define('TABLE_BLOG_POSTS',           DB_PREFIX.'blog_posts');
define('TABLE_BLOG_POSTS_IMAGES',    DB_PREFIX.'blog_posts_images');
define('TABLE_BLOG_TAGS',            DB_PREFIX.'blog_tags');
define('TABLE_BLOG_POSTS_TAGS',      DB_PREFIX.'blog_posts_tags');
define('TABLE_BLOG_CATEGORIES',      DB_PREFIX.'blog_categories');
define('TABLE_BLOG_CATEGORIES_LANG', DB_PREFIX.'blog_categories_lang');
define('TABLE_BLOG_COMMENTS',        DB_PREFIX.'blog_comments');

class BlogModel_ extends Model
{
    /** @var BlogBase */
    var $controller;

    /** @var bff\db\NestedSetsTree для категорий */
    public $treeCategories;
    public $langCategories = array(
        'title'        => TYPE_NOTAGS, # Название
        'mtitle'       => TYPE_NOTAGS, # Meta Title
        'mkeywords'    => TYPE_NOTAGS, # Meta Keywords
        'mdescription' => TYPE_NOTAGS, # Meta Description
        'seotext'      => TYPE_STR,    # SEO-Text
    );

    const CATS_ROOTID = 1; # ID "Корневой категории" (изменять не рекомендуется)

    public function init()
    {
        parent::init();

        # подключаем nestedSets категории
        $this->treeCategories = new bff\db\NestedSetsTree(TABLE_BLOG_CATEGORIES);
        $this->treeCategories->init();
    }

    # --------------------------------------------------------------------
    # Посты

    /**
     * Список постов (admin)
     * @param array $aFilter фильтр списка постов
     * @param integer $nTagID ID тега
     * @param bool $bCount только подсчет кол-ва постов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function postsListing(array $aFilter, $nTagID, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $sqlJoin = ' LEFT JOIN ' . TABLE_BLOG_CATEGORIES_LANG . ' C ON C.id = P.cat_id ';

        if (isset($aFilter[':user'])) {
            $sqlJoin .= ' INNER JOIN ' . TABLE_USERS . ' U ON P.user_id = U.user_id ';
        }

        $aFilter = $this->prepareFilter($aFilter, 'P');

        if ($nTagID > 0) {
            $sqlJoin .= ' INNER JOIN ' . TABLE_BLOG_POSTS_TAGS . ' T ON T.post_id = P.id AND T.tag_id = :tag ';
            $aFilter['bind'][':tag'] = $nTagID;
        }

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(*) FROM (
                SELECT P.id
                FROM ' . TABLE_BLOG_POSTS . ' P ' . $sqlJoin . '
                     ' . $aFilter['where'] .
                'GROUP BY P.id ) sl', $aFilter['bind']
            );
        }

        return $this->db->select('SELECT P.id, P.keyword, P.created, P.title, P.cat_id, C.title as cat_title, P.status, 
                        P.moderated, P.user_id
               FROM ' . TABLE_BLOG_POSTS . ' P ' . $sqlJoin . '
               ' . $aFilter['where'] .
            ' GROUP BY P.id '
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список постов (frontend)
     * @param array $aFilter фильтр списка постов
     * @param bool $bCount только подсчет кол-ва постов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @param bool $bTags
     * @return mixed
     */
    public function postsList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = 'P.created DESC', $bTags = false)
    {
        $sqlJoin = '';

        # поиск по тегу
        if (isset($aFilter['tag']) && Blog::tagsOn()) {
            $bTags = true;
            $sqlJoin = ', ' . TABLE_BLOG_POSTS_TAGS . ' TT ';
            $aFilter[':tag'] = array('TT.post_id = P.id AND TT.tag_id = :tag', ':tag' => $aFilter['tag']);
            unset($aFilter['tag']);
        }

        # полнотекстовый поиск
        if (array_key_exists('text', $aFilter)) {
            $aFilter[':text'] = $this->db->prepareFulltextQuery($aFilter['text'], 'P.title, P.content');
            unset($aFilter['text']);
        }

        $aFilter['status'] = Blog::STATUS_ACTIVE;
        if (Blog::premoderation()) {
            $aFilter[':moderated'] = 'moderated > 0';
        }
        $aFilter[':user'] = 'P.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'P');
            return $this->db->one_data('SELECT COUNT(id) FROM ( SELECT P.id FROM ' . TABLE_BLOG_POSTS . ' P ' . $sqlJoin . ', '.TABLE_USERS.' U
                    ' . $aFilter['where'].' GROUP BY P.id ) sl', $aFilter['bind']
            );
        }

        $aFilter[':cat1'] = 'P.cat_id1 = C1.id '.$this->db->langAnd(true, 'C1', 'CL1');
        $aFilter[':user_stat'] = 'U.user_id = US.user_id';

        $aFilter = $this->prepareFilter($aFilter, 'P');
        $aData = $this->db->select('
            SELECT P.id, P.user_id, P.keyword, P.created, P.img_v, P.imgcnt, P.title, P.content_preview,
                   P.comments_enabled, P.comments_cnt, P.views_total, P.moderated, P.status,
                   C1.keyword AS cat1_keyword, CL1.title AS cat1_title, C2.keyword AS cat2_keyword, CL2.title AS cat2_title,
                   '.($bTags ? 'GROUP_CONCAT(T.tag_id) AS tags,' : '').'
                   U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex, US.last_activity
            FROM ' . TABLE_BLOG_POSTS . ' P
                 '.($bTags ? ' LEFT JOIN ' . TABLE_BLOG_POSTS_TAGS . ' T ON P.id = T.post_id ' : '').'
                 LEFT JOIN '.TABLE_BLOG_CATEGORIES.' C2 ON P.cat_id2 = C2.id
                 LEFT JOIN '.TABLE_BLOG_CATEGORIES_LANG.' CL2 ON '.$this->db->langAnd(false, 'C2', 'CL2').'
                 ' . $sqlJoin . '
                 , '.TABLE_BLOG_CATEGORIES.' C1, '.TABLE_BLOG_CATEGORIES_LANG.' CL1, '.TABLE_USERS.' U, '.TABLE_USERS_STAT.' US
            ' . $aFilter['where'] . '
            GROUP BY P.id
            ' . 'ORDER BY ' . $sqlOrder . ' ' . $sqlLimit, $aFilter['bind']);

        # Теги
        if( ! empty($aData)) {
            if($bTags) {
                $aTags = array();
                foreach ($aData as &$v) {
                    if ($v['tags']) {
                        $tags = explode(',', $v['tags']);
                        if (!empty($tags)) {
                            $v['aTags'] = $tags;
                            foreach ($tags as $t) {
                                if (!in_array($t, $aTags)) {
                                    $aTags[] = $t;
                                }
                            }
                        }
                    }
                }
                unset($v);

                if (!empty($aTags)) {
                    $aTags = $this->db->select_key('SELECT id, tag FROM ' . TABLE_BLOG_TAGS . ' WHERE ' . $this->db->prepareIN('id', $aTags), 'id');
                    foreach ($aData as &$v) {
                        if (!empty($v['aTags'])) {
                            foreach ($v['aTags'] as $i => $tag_id) {
                                if (!empty($aTags[$tag_id])) {
                                    $v['aTags'][$i] = $aTags[$tag_id];
                                }
                            }
                        }
                    }
                    unset($v);
                }
            }
        }

        return $aData;
    }

    /**
     * Список постов пользователя
     * @param int $nUserID ID пользователя
     * @param array $aFilter фильтр списка постов
     * @param bool $bCount только подсчет кол-ва постов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function postsOwnerList($nUserID, array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $aFilter['user_id'] = $nUserID;

        if ($nUserID != User::id()) {
            $aFilter['status'] = Blog::STATUS_ACTIVE;
            if (Blog::premoderation()) {
                $aFilter['moderated'] = 1;
            }
        }

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'P');
            return $this->db->one_data('SELECT COUNT(P.id) FROM ' . TABLE_BLOG_POSTS . ' P ' . $aFilter['where'], $aFilter['bind']);
        }

        $aFilter[':cat'] = 'P.cat_id1 = C1.id';
        $aFilter[':cat_lang'] = $this->db->langAnd(false, 'C1', 'CL1');
        $aFilter[':user'] = 'P.user_id = U.user_id';
        $aFilter = $this->prepareFilter($aFilter, 'P');

        if ( ! $sqlOrder) {
            $sqlOrder = 'C1.numleft, P.created DESC';
        }

        $aData = $this->db->select('
            SELECT P.id, P.user_id, P.keyword, P.created, P.img_v, P.imgcnt, P.title, P.content_preview,
                   P.comments_enabled, P.comments_cnt, P.status, P.moderated, P.blocked_reason,
                   C1.keyword AS cat1_keyword, CL1.title AS cat1_title, C2.keyword AS cat2_keyword, CL2.title AS cat2_title,
                   U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex
            FROM ' . TABLE_BLOG_POSTS . ' P
                 LEFT JOIN '.TABLE_BLOG_CATEGORIES.' C2 ON P.cat_id2 = C2.id
                 LEFT JOIN '.TABLE_BLOG_CATEGORIES_LANG.' CL2 ON '.$this->db->langAnd(false, 'C2', 'CL2').'
                 , '.TABLE_BLOG_CATEGORIES.' C1, '.TABLE_BLOG_CATEGORIES_LANG.' CL1, '.TABLE_USERS.' U
            ' . $aFilter['where'] . '
            ' . 'ORDER BY ' . $sqlOrder . ' ' . $sqlLimit, $aFilter['bind']);

        return $aData;
    }

    /**
     * Просмотр поста (frontend)
     * @param integer $nPostID ID поста
     * @return array
     */
    public function postView($nPostID)
    {
        $aData = $this->db->one_array('SELECT P.id, P.cat_id, P.created, P.keyword, P.title, P.cat_id1, P.cat_id2,
                        P.mtemplate, P.content,
                        C1.keyword AS cat1_keyword, CL1.title AS cat1_title, C2.keyword AS cat2_keyword, CL2.title AS cat2_title
                    FROM ' . TABLE_BLOG_POSTS . ' P
                             LEFT JOIN '.TABLE_BLOG_CATEGORIES.' C2 ON P.cat_id2 = C2.id
                             LEFT JOIN '.TABLE_BLOG_CATEGORIES_LANG.' CL2 ON '.$this->db->langAnd(false, 'C2', 'CL2').'
                         , '.TABLE_BLOG_CATEGORIES.' C1, '.TABLE_BLOG_CATEGORIES_LANG.' CL1
                    WHERE P.id = :id AND P.cat_id1 = C1.id '.$this->db->langAnd(true, 'C1', 'CL1').'
                    ', array(':id' => $nPostID)
        );
        if (empty($aData)) {
            return false;
        }

        return $aData;
    }

    /**
     * Получение данных поста
     * @param integer $nPostID ID поста
     * @param array $aFields необходимые поля
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function postData($nPostID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT P.*, U.email, U.blocked as user_blocked
                    FROM ' . TABLE_BLOG_POSTS . ' P, ' . TABLE_USERS . ' U
                    WHERE P.user_id = U.user_id AND P.id = :id',
                array(':id' => $nPostID)
            );
        } else {
            if (empty($aFields)) {
                $aFields = array('P.*');
            } else {
                foreach ($aFields as $k => $v) {
                    $aFields[$k] = 'P.'.$v;
                }
            }
            $aData = $this->db->one_array('SELECT '.join(', ', $aFields).'
                    FROM '.TABLE_BLOG_POSTS.' P
                    WHERE P.id = :id ',
                array(':id' => $nPostID)
            );
        }

        return $aData;
    }

    /**
     * Получение данных поста для просмотра
     * @param integer $nPostID ID поста
     * @return array
     */
    public function postDataView($nPostID)
    {
        $aData = $this->db->one_array('
            SELECT P.id, P.user_id, P.title, P.content, P.content_preview, P.keyword, P.created, P.status, P.moderated, P.blocked_reason,
                   P.imgcnt, P.cat_id, P.cat_id1, P.cat_id2, P.comments_enabled, P.views_total, P.content_search,
                   U.name, U.surname, U.avatar, U.sex, U.login, U.verified, U.pro, US.last_activity
            FROM ' . TABLE_BLOG_POSTS . ' P, ' . TABLE_USERS . ' U, ' . TABLE_USERS_STAT . ' US
            WHERE P.user_id = U.user_id AND P.id = :id
              AND U.user_id = US.user_id',
            array(':id' => $nPostID)
        );
        if (empty($aData)) {
            return array();
        }
        if ( ! empty($aData['contacts'])) {
            $aData['contacts'] = func::unserialize($aData['contacts']);
        }
        $aData['cat'] = $this->db->select_key('
            SELECT C.id, C.keyword, CL.title, C.numlevel
            FROM '.TABLE_BLOG_CATEGORIES.' C, '.TABLE_BLOG_CATEGORIES_LANG.' CL
            WHERE '.$this->db->prepareIN('C.id', array($aData['cat_id1'], $aData['cat_id2'])).
            $this->db->langAnd(true, 'C', 'CL').'
            ORDER BY C.numleft
            ', 'id');

        return $aData;
    }

    /**
     * Данные для ссылки "следующий пост"
     * @param integer $nPostID ID поста
     * @param integer $nUserID ID пользоателя
     * @param string $sCreated дата создания поста
     * @return mixed
     */
    public function postNext($nPostID, $nUserID, $sCreated)
    {
        $aFilter = array();
        $aFilter['user_id'] = $nUserID;
        $aFilter[':id'] = array('P.id != :id', ':id' => $nPostID);

        if ( ! User::isCurrent($nUserID)) {
            $aFilter['status'] = Blog::STATUS_ACTIVE;
            if (Blog::premoderation()) {
                $aFilter[':moderated'] = 'P.moderated > 0';
            }
        }
        $aFilterFirst = $aFilter;

        $aFilter[':created'] = array('P.created > :created', ':created' => $sCreated);
        $aFilter = $this->prepareFilter($aFilter, 'P');

        $aData = $this->db->one_array('SELECT P.id, P.keyword
            FROM ' . TABLE_BLOG_POSTS . ' P
            '.$aFilter['where'].'
            ORDER BY P.created ASC LIMIT 1', $aFilter['bind']
        );

        if (empty($aData)) {
            $aFilter = $this->prepareFilter($aFilterFirst, 'P');
            $aData = $this->db->one_array('SELECT P.id, P.keyword
                FROM ' . TABLE_BLOG_POSTS . ' P
                '.$aFilter['where'].'
                ORDER BY P.created ASC LIMIT 1', $aFilter['bind']
            );
        }

        return $aData;
    }

    /**
     * Данные для ссылки "предыдущий пост"
     * @param integer $nPostID ID поста
     * @param integer $nUserID ID пользоателя
     * @param string $sCreated дата создания поста
     * @return mixed
     */
    public function postPrev($nPostID, $nUserID, $sCreated)
    {
        $aFilter = array();
        $aFilter['user_id'] = $nUserID;
        $aFilter[':id'] = array('P.id != :id', ':id' => $nPostID);

        if ( ! User::isCurrent($nUserID)) {
            $aFilter['status'] = Blog::STATUS_ACTIVE;
            if (Blog::premoderation()) {
                $aFilter[':moderated'] = 'P.moderated > 0';
            }
        }
        $aFilterLast = $aFilter;

        $aFilter[':created'] = array('P.created < :created', ':created' => $sCreated);
        $aFilter = $this->prepareFilter($aFilter, 'P');

        $aData = $this->db->one_array('SELECT P.id, P.keyword
            FROM ' . TABLE_BLOG_POSTS . ' P
            '.$aFilter['where'].'
            ORDER BY P.created DESC LIMIT 1', $aFilter['bind']
        );

        if (empty($aData)) {
            $aFilter = $this->prepareFilter($aFilterLast, 'P');
            $aData = $this->db->one_array('SELECT P.id, P.keyword
                FROM ' . TABLE_BLOG_POSTS . ' P
                '.$aFilter['where'].'
                ORDER BY P.created DESC LIMIT 1', $aFilter['bind']
            );
        }

        return $aData;
    }

    /**
     * Сохранение поста
     * @param integer $nPostID ID поста
     * @param array $aData данные поста
     * @return boolean|integer
     */
    public function postSave($nPostID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }

        if ($nPostID > 0) {
            $aData['modified'] = $this->db->now(); # Дата изменения
            $res = $this->db->update(TABLE_BLOG_POSTS, $aData, array('id' => $nPostID));

            return !empty($res);
        } else {
            $aData['modified'] = $this->db->now(); # Дата изменения
            $aData['created'] = $this->db->now(); # Дата создания

            $nPostID = $this->db->insert(TABLE_BLOG_POSTS, $aData);
            if ($nPostID > 0) {

            }

            return $nPostID;
        }
    }

    /**
     * Сохранение счетчиков поста
     * @param integer $nPostID ID поста
     * @param array $aData данные поста
     * @return boolean|integer
     */
    public function postSaveCnt($nPostID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }
        return $this->db->update(TABLE_BLOG_POSTS, $aData, array('id' => $nPostID));
    }

    /**
     * Изменение порядка постов
     * @param string $sOrderField поле, по которому производится сортировка
     * @param string $aCond дополнительные условия
     * @return mixed @see rotateTablednd
     */
    public function postsRotate($sOrderField, $aCond = '')
    {
        if (!empty($aCond)) {
            $aCond = ' AND ' . (is_array($aCond) ? join(' AND ', $aCond) : $aCond);
        }

        return $this->db->rotateTablednd(TABLE_BLOG_POSTS, $aCond, 'id', $sOrderField);
    }

    /**
     * Перемещение постов из одной категории ($nCatOldID) в другую ($nCatNewID)
     * @param int $nCatNewID ID категории, в которую перемещаем посты
     * @param int $nCatOldID ID категории, из которой перемещаем посты
     * @return mixed
     */
    public function postsMoveToCategory($nCatNewID, $nCatOldID)
    {
        if (empty($nCatNewID) || empty($nCatOldID)) {
            return false;
        }

        # перемещаем
        return $this->db->update(TABLE_BLOG_POSTS, array('cat_id' => $nCatNewID), array('cat_id' => $nCatOldID));
    }

    /**
     * Удаление поста
     * @param integer $nPostID ID поста
     * @return boolean
     */
    public function postDelete($nPostID)
    {
        if (empty($nPostID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_BLOG_POSTS, array('id' => $nPostID));
        if (!empty($res)) {
            return true;
        }

        return false;
    }

    /**
     * Получаем общее кол-во постов, ожидающих модерации
     * @return integer
     */
    public function postsModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(P.id)
                FROM ' . TABLE_BLOG_POSTS . ' P
                WHERE P.moderated != 1'
        );
    }

    # --------------------------------------------------------------------
    # Категории

    /**
     * Список категорий (admin)
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = 'numleft')
    {
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter[] = 'pid != 0';
        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM ' . TABLE_BLOG_CATEGORIES . ' C, ' . TABLE_BLOG_CATEGORIES_LANG . ' CL ' . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT C.id, C.created, C.enabled, CL.title, C.pid, C.numlevel,
                    ((C.numright-C.numleft)-1) as node, COUNT(P.id) as cnt_posts
               FROM ' . TABLE_BLOG_CATEGORIES . ' C
                        LEFT JOIN '.TABLE_BLOG_POSTS.' P ON C.id = (
                            CASE C.numlevel
                                WHEN 1 THEN P.cat_id1
                                WHEN 2 THEN P.cat_id2
                                ELSE 0
                            END
                        ),
                    ' . TABLE_BLOG_CATEGORIES_LANG . ' CL
               ' . $aFilter['where']
            . ' GROUP BY C.id'
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список категорий (frontend)
     * @return mixed
     */
    public function categoriesList()
    {
        $aFilter = $this->prepareFilter(array(
                'enabled' => 1,
                'pid != 0',
                'cnt_posts > 0',
                ':lang'   => $this->db->langAnd(false, 'C', 'CL')
            ), 'C'
        );

        $aData = $this->db->select_key('SELECT C.id, C.pid, C.keyword, CL.title, C.cnt_posts, C.numlevel, 0 AS a
                                  FROM ' . TABLE_BLOG_CATEGORIES . ' C,
                                       ' . TABLE_BLOG_CATEGORIES_LANG . ' CL
                                  ' . $aFilter['where'] . '
                                  ORDER BY C.numleft', 'id', $aFilter['bind']
        );

        if (empty($aData)) {
            return array();
        }

        return $this->db->transformRowsToTree($aData, 'id', 'pid', 'sub');
    }

    /**
     * Список всех категорий с подкатегориями (frontend)
     * @param array $aFields список требуемых полей
     * @return mixed
     */
    public function categoriesListAll(array $aFields = array())
    {
        foreach (array('id','pid') as $k) {
            if (!in_array($k, $aFields)) {
                $aFields[] = $k;
            }
        }

        foreach ($aFields as $k => $v) {
            $aFields[$k] = (array_key_exists($v, $this->langCategories) ? 'CL.' : 'C.') . $v;
        }

        $aFilter = array('pid != 0', 'enabled'=>1);
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aData = $this->db->select('SELECT ' . join(',', $aFields) . ', ((C.numright-C.numleft)>1) as subs
                                  FROM ' . TABLE_BLOG_CATEGORIES . ' C, ' . TABLE_BLOG_CATEGORIES_LANG . ' CL
                                  ' . $aFilter['where'] . $this->db->langAnd(true, 'C', 'CL') . '
                                  ORDER BY C.numleft
                                  ', $aFilter['bind']
        );

        if (!empty($aData)) {
            $aData = $this->db->transformRowsToTree($aData, 'id', 'pid', 'sub');
        }

        return $aData;
    }

    /**
     * Список категорий пользователя
     * @param integer $nUserID ID пользователя
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @return mixed
     */
    public function categoriesOwnerList($nUserID, array $aFilter = array(), $bCount = false, $sqlLimit = '') // frontend
    {
        $aFilter[] = 'pid != 0';
        $aFilter[':user'] = array('P.cat_id1 = C.id AND P.user_id = :user', ':user' => $nUserID);

        if ($nUserID != User::id()) {
            $aFilter[':status'] = array('P.status = :status', ':status' => Blog::STATUS_ACTIVE);
            if (Blog::premoderation()) {
                $aFilter[':moderated'] = array('P.moderated = :moderated', ':moderated' => 1);
            }
        }

        if ( ! $bCount) {
            $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        }

        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('
                SELECT COUNT(id) FROM (
                    SELECT C.id FROM' . TABLE_BLOG_POSTS . ' P, ' . TABLE_BLOG_CATEGORIES . ' C
                    ' . $aFilter['where'].'
                    GROUP BY C.id ) Sl
                    ', $aFilter['bind']);
        }

        $sqlOrder = 'C.numleft';
        $aData = $this->db->select_key('
            SELECT C.id, CL.title
            FROM ' . TABLE_BLOG_POSTS . ' P, ' . TABLE_BLOG_CATEGORIES . ' C, ' . TABLE_BLOG_CATEGORIES_LANG . ' CL
            ' . $aFilter['where'] . '
            GROUP BY C.id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, 'id', $aFilter['bind']
        );

        if ( ! empty($aData)) {
            //
        }

        return $aData;
    }

    /**
     * Просмотр постов в категории
     * @param string $sKeyword keyword категории
     * @return array|boolean
     */
    public function categoryView($sKeyword)
    {
        return $this->db->one_array('SELECT C.id, C.pid, C.numlevel, CL.title, CL.mtitle, CL.mkeywords, CL.mdescription, C.mtemplate
                            FROM ' . TABLE_BLOG_CATEGORIES . ' C,
                                 ' . TABLE_BLOG_CATEGORIES_LANG . ' CL
                            WHERE C.keyword = :key AND C.enabled = 1
                                ' . $this->db->langAnd(true, 'C', 'CL') . '
                            ', array(':key' => $sKeyword)
        );
    }

    /**
     * Получение данных категории
     * @param integer $nCategoryID ID категории
     * @param boolean $bEdit при редактировании
     * @param array $aFields необходимые поля
     * @return array
     */
    public function categoryData($nCategoryID, $bEdit = false, array $aFields = array())
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT C.*, ((C.numright-C.numleft)-1) as node, COUNT(P.id) as posts
                    FROM ' . TABLE_BLOG_CATEGORIES . ' C
                        LEFT JOIN ' . TABLE_BLOG_POSTS . ' P ON P.cat_id = C.id
                    WHERE C.id = :id
                    GROUP BY C.id',
                array(':id' => $nCategoryID)
            );
            if (!empty($aData)) {
                $this->db->langSelect($nCategoryID, $aData, $this->langCategories, TABLE_BLOG_CATEGORIES_LANG);
            }
        } else {
            if (empty($aFields)) {
                $aFields = array('C.*', 'CL.*');
            } else {
                foreach ($aFields as $k => $v) {
                    if (array_key_exists($v, $this->langCategories)) {
                        $aFields[$k] = 'CL.'.$v;
                    } else {
                        $aFields[$k] = 'C.'.$v;
                    }
                }
            }
            $func = 'one_array';
            if(count($aFields) == 1) $func = 'one_data';
            $aData = $this->db->$func('SELECT '.join(', ', $aFields).'
                    FROM '.TABLE_BLOG_CATEGORIES.' C,
                         '.TABLE_BLOG_CATEGORIES_LANG.' CL
                    WHERE C.id = :id '.$this->db->langAnd(true, 'C', 'CL'),
                array(':id' => $nCategoryID)
            );
        }

        return $aData;
    }

    /**
     * Получение meta данных категории
     * @param mixed $mCategory ID/Keyword категории
     * @return array
     */
    public function categoryMeta($mCategory)
    {
        $aData = $this->db->one_array('SELECT
                    C.id, C.pid, C.keyword, C.numlevel, CL.title,
                    C.mtemplate, CL.mtitle, CL.mkeywords, CL.mdescription, CL.seotext
                FROM ' . TABLE_BLOG_CATEGORIES . ' C, ' . TABLE_BLOG_CATEGORIES_LANG . ' CL
                WHERE C.'.(is_int($mCategory) ? 'id' : 'keyword').' = :cat '
                    . $this->db->langAnd(true, 'C', 'CL'),
            array(':cat' => $mCategory)
        );
        if (empty($aData)) $aData = array();
        return $aData;
    }

    /**
     * Сохранение категории
     * @param integer $nCategoryID ID категории
     * @param array $aData данные категории
     * @return boolean|integer
     */
    public function categorySave($nCategoryID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }

        if ($nCategoryID > 0) {
            $aData['modified'] = $this->db->now(); # Дата изменения

            if (isset($aData['pid'])) {
                unset($aData['pid']);
            } # запрет изменения pid
            $res = $this->db->update(TABLE_BLOG_CATEGORIES, array_diff_key($aData, $this->langCategories), array('id' => $nCategoryID));

            $this->db->langUpdate($nCategoryID, $aData, $this->langCategories, TABLE_BLOG_CATEGORIES_LANG);

            return !empty($res);
        } else {

            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nCategoryID = $this->treeCategories->insertNode($aData['pid']);
            if ($nCategoryID > 0) {
                unset($aData['pid']);
                $this->db->update(TABLE_BLOG_CATEGORIES, array_diff_key($aData, $this->langCategories), 'id = :id', array(':id' => $nCategoryID));
                $this->db->langInsert($nCategoryID, $aData, $this->langCategories, TABLE_BLOG_CATEGORIES_LANG);
            }

            return $nCategoryID;
        }
    }

    /**
     * Переключатели категории
     * @param integer $nCategoryID ID категории
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function categoryToggle($nCategoryID, $sField)
    {
        switch ($sField) {
            case 'enabled':# Включен
                return $this->toggleInt(TABLE_BLOG_CATEGORIES, $nCategoryID, $sField, 'id');
                break;
        }
    }

    /**
     * Перемещение категории
     * @return mixed @see rotateTablednd
     */
    public function categoriesRotate()
    {
        return $this->treeCategories->rotateTablednd();
    }

    /**
     * Удаление категории
     * @param integer $nCategoryID ID категории
     * @return boolean
     */
    public function categoryDelete($nCategoryID)
    {
        if (empty($nCategoryID)) {
            return false;
        }

        $nPosts = $this->db->one_data('SELECT COUNT(I.id) FROM ' . TABLE_BLOG_POSTS . ' I WHERE I.cat_id = :id', array(':id' => $nCategoryID));
        if (!empty($nPosts)) {
            $this->errors->set('Невозможно выполнить удаление категории при наличии постов');

            return false;
        }

        $aDeletedID = $this->treeCategories->deleteNode($nCategoryID);
        $res = !empty($aDeletedID);
        if (!empty($res)) {
            $this->db->delete(TABLE_BLOG_CATEGORIES_LANG, array('id' => $nCategoryID));

            return true;
        }

        return false;
    }

    /**
     * Удаление всех категорий
     */
    public function categoriesDeleteAll()
    {
        # чистим таблицу категорий (+ зависимости по внешним ключам)
        $this->db->exec('DELETE FROM ' . TABLE_BLOG_CATEGORIES . ' WHERE id > 0');
        $this->db->exec('TRUNCATE TABLE ' . TABLE_BLOG_CATEGORIES_LANG);
        $this->db->exec('ALTER TABLE ' . TABLE_BLOG_CATEGORIES . ' AUTO_INCREMENT = 2');
        $this->db->update(TABLE_BLOG_POSTS, array('cat_id' => 0));

        # создаем корневую директорию
        $nRootID = self::CATS_ROOTID;
        $sRootTitle = 'Корневой раздел';
        $aData = array(
            'id'       => $nRootID,
            'pid'      => 0,
            'numleft'  => 1,
            'numright' => 2,
            'numlevel' => 0,
            'keyword'  => 'root',
            'enabled'  => 1,
            'created'  => $this->db->now(),
            'modified' => $this->db->now(),
        );
        $res = $this->db->insert(TABLE_BLOG_CATEGORIES, $aData);
        if (!empty($res)) {
            $aDataLang = array('title' => array());
            foreach ($this->locale->getLanguages() as $lng) {
                $aDataLang['title'][$lng] = $sRootTitle;
            }
            $this->db->langInsert($nRootID, $aDataLang, $this->langCategories, TABLE_BLOG_CATEGORIES_LANG);
        }

        return !empty($res);
    }

    /**
     * Формирование списка основных категорий
     * @param integer $nSelectedID ID выбранной категории
     * @param mixed $mEmptyOpt невыбранное значение
     * @param integer $nType тип списка: 0 - все(кроме корневого), 1 - список при добавлении категории, 2 - список при добавлении записи
     * @param array $aOnlyID только список определенных категорий
     * @return string <option></option>...
     */
    public function categoriesOptions($nSelectedID = 0, $mEmptyOpt = false, $nType = 0, $aOnlyID = array())
    {
        $aFilter = array();
        if ($nType == 1) {
            $aFilter[] = 'numlevel < 2';
        } else {
            $aFilter[] = 'numlevel > 0';
        }
        if (!empty($aOnlyID)) {
            $aFilter[':only'] = '(C.id IN (' . join(',', $aOnlyID) . ') OR C.pid IN(' . join(',', $aOnlyID) . '))';
        }

        // Chrome не понимает style="padding" в option
        $bUsePadding = (mb_stripos(Request::userAgent(), 'chrome') === false);
        $bJoinItems = ($nType > 0);
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aCategories = $this->db->select('SELECT C.id, CL.title, C.numlevel, ((C.numright-C.numleft)-1) as node
                    ' . ($bJoinItems ? ', COUNT(I.id) as items ' : '') . '
               FROM ' . TABLE_BLOG_CATEGORIES . ' C
                    ' . ($bJoinItems ? ' LEFT JOIN ' . TABLE_BLOG_POSTS . ' I ON C.id = I.cat_id ' : '') . '
                    , ' . TABLE_BLOG_CATEGORIES_LANG . ' CL
               ' . $aFilter['where'] . '
               GROUP BY C.id
               ORDER BY C.numleft', $aFilter['bind']
        );

        $sOptions = '';
        foreach ($aCategories as $v) {
            $nNumlevel = & $v['numlevel'];
            $bDisable = ($nType > 0 && ($nType == 2 ? $v['node'] > 0 : ($nNumlevel > 1 )));
            $sOptions .= '<option value="' . $v['id'] . '" ' .
                ($bUsePadding && $nNumlevel > 1 ? 'style="padding-left:' . ($nNumlevel * 10) . 'px;" ' : '') .
                ($v['id'] == $nSelectedID ? ' selected' : '') .
                ($bDisable ? ' disabled' : '') .
                '>' . (!$bUsePadding && $nNumlevel > 1 ? str_repeat('  ', $nNumlevel) : '') . $v['title'] . '</option>';
        }

        if ($mEmptyOpt !== false) {
            $nValue = 0;
            if (is_array($mEmptyOpt)) {
                $nValue = key($mEmptyOpt);
                $mEmptyOpt = current($mEmptyOpt);
            }
            $sOptions = '<option value="' . $nValue . '" class="bold">' . $mEmptyOpt . '</option>' . $sOptions;
        }

        return $sOptions;
    }

    /**
     * Получаем кол-во вложенных категорий
     */
    public function categorySubCount($nCategoryID)
    {
        return $this->treeCategories->getChildrenCount($nCategoryID);
    }

    /**
     * Формирование списка подкатегорий
     * @param integer $nCategoryID ID категории
     * @param mixed $mOptions формировать select-options или FALSE
     * @param array $aFilter фильтр категорий
     * @return array|string
     */
    public function categorySubOptions($nCategoryID, $mOptions = false, $aFilter = array())
    {
        $aFilter['pid'] = $nCategoryID;
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aData = $this->db->select('SELECT C.id, CL.title, C.numlevel, ((C.numright-C.numleft)-1) as node
                    FROM '.TABLE_BLOG_CATEGORIES.' C, '.TABLE_BLOG_CATEGORIES_LANG.' CL
                    ' . $aFilter['where'] . '
                    ORDER BY C.numleft', $aFilter['bind']);

        if (empty($mOptions)) return $aData;

        return HTML::selectOptions($aData, $mOptions['sel'], $mOptions['empty'], 'id', 'title');
    }

    /**
     * Формирование списков категорий (при добавлении/редактировании записи)
     * @param array $aCategoriesID ID категорий [lvl=>selectedID, ...]
     * @param mixed $mOptions формировать select-options или нет (false)
     * @return array [lvl=>[a=>selectedID, categories=>список категорий(массив или options)],...]
     */
    public function categoriesOptionsByLevel($aCategoriesID, $mOptions = false)
    {
        if (empty($aCategoriesID)) return array();

        # формируем список требуемых уровней категорий
        $aLevels = array(); $bFill = true; $parentID = 1;
        foreach ($aCategoriesID as $lvl=>$nCategoryID) {
            if ($nCategoryID || $bFill) {
                $aLevels[$lvl] = $parentID;
                if ( ! $nCategoryID) break;
                $parentID = $nCategoryID;
            } else {
                break;
            }
        }

        if (empty($aLevels)) return array();

        $aData = $this->db->select('SELECT C.id, CL.title, C.numlevel as lvl
                    FROM '.TABLE_BLOG_CATEGORIES.' C, '.TABLE_BLOG_CATEGORIES_LANG.' CL
                    WHERE C.numlevel IN ('.join(',', array_keys($aLevels)).')
                      AND C.pid IN('.join(',', $aLevels).')
                      '.$this->db->langAnd(true, 'C', 'CL').'
                    ORDER BY C.numleft');
        if (empty($aData)) return array();

        $aLevels = array();
        foreach ($aData as $v) {
            $aLevels[$v['lvl']][$v['id']] = $v;
        } unset($aData);

        foreach ($aCategoriesID as $lvl=>$nSelectedID)
        {
            if (isset($aLevels[$lvl])) {
                $aCategoriesID[$lvl] = array(
                    'a' => $nSelectedID,
                    'categories' => ( ! empty($mOptions) ?
                            HTML::selectOptions($aLevels[$lvl], $nSelectedID, $mOptions['empty'], 'id', 'title') :
                            $aLevels[$lvl] ),
                );
            } else {
                $aCategoriesID[$lvl] = array(
                    'a' => $nSelectedID,
                    'categories' => false,
                );
            }
        }

        return $aCategoriesID;
    }

    /**
     * Получаем данные parent-категорий
     * @param integer $nCategoryID ID категории
     * @param bool $bIncludingSelf включать текущую в итоговых список
     * @param bool $bExludeRoot исключить корневой раздел
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsID($nCategoryID, $bIncludingSelf = true, $bExludeRoot = true)
    {
        if (empty($nCategoryID)) return array(1=>0);
        $aData = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf, array('id','numlevel'));
        $aParentsID = array();
        if ( ! empty($aData)) {
            foreach ($aData as $v) {
                $aParentsID[ $v['numlevel'] ] = $v['id'];
            }
        }
        return $aParentsID;
    }

    /**
     * Получаем названия parent-категорий
     * @param integer $nCategoryID ID категории
     * @param boolean $bIncludingSelf включать текущую в итоговых список
     * @param boolean $bExludeRoot исключить корневой раздел
     * @param mixed $mSeparator объединить в одну строку или FALSE
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsTitle($nCategoryID, $bIncludingSelf = false, $bExludeRoot = false, $mSeparator = true, & $aParentsID = null)
    {
        $aParentsID = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf);
        if (empty($aParentsID)) return ($mSeparator !== false ? '' : array());

        $aData = $this->db->select_one_column('SELECT CL.title
                   FROM '.TABLE_BLOG_CATEGORIES.' C, '.TABLE_BLOG_CATEGORIES_LANG.' CL
                   WHERE '.$this->db->prepareIN('C.id', $aParentsID).''.$this->db->langAnd(true, 'C', 'CL').'
                   ORDER BY C.numleft');

        $aData = ( ! empty($aData) ? $aData : array());

        if ($mSeparator !== false) {
            return join('  '.($mSeparator === true ? '>' : $mSeparator).'  ', $aData);
        } else {
            return $aData;
        }
    }

    /**
     * Получим ID корневой категории
     * @return int
     */
    public function categoryRootNodeID()
    {
        return $this->treeCategories->getRootNodeID(0, $bCreated);
    }

    public function getLocaleTables()
    {
        return array(
            TABLE_BLOG_CATEGORIES => array('type' => 'table', 'fields' => $this->langCategories, 'title' => _t('blog', 'Категории')),
        );
    }

    /**
     * Статистика
     * @return array
     */
    public function statisticData()
    {
        $data = array();

        # постов
        $filter = array('status' => Blog::STATUS_ACTIVE);
        if (Blog::premoderation()) {
            $filter[':moderated'] = 'moderated > 0';
        }
        $filter[':user'] = 'P.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';
        $filter = $this->prepareFilter($filter, 'P');
        $data['posts'] = (int)$this->db->one_data(' SELECT COUNT(P.id) FROM '.TABLE_BLOG_POSTS.' P, '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);

        return $data;
    }

}