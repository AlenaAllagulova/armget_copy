<?php

abstract class BlogBase_ extends Module
{
    /** @var BlogModel */
    public $model = null;
    public $securityKey = '75409335cd1659479b0d33fd462df2d9';

    # Статус поста
    const STATUS_ACTIVE  = 1; # активен
    const STATUS_HIDDEN  = 2; # скрыт
    const STATUS_DRAFT   = 3; # черновик
    const STATUS_BLOCKED = 5; # заблокирован

    public function init()
    {
        parent::init();

        $this->module_title = _t('blog','Блог');

        bff::autoloadEx(array(
            'BlogPostImages'   => array('app', 'modules/blog/blog.post.images.php'),
            'BlogPostTags'     => array('app', 'modules/blog/blog.post.tags.php'),
            'BlogPostComments' => array('app', 'modules/blog/blog.post.comments.php'),
        ));
    }

    /**
     * Shortcut
     * @return Blog
     */
    public static function i()
    {
        return bff::module('Blog');
    }

    /**
     * Shortcut
     * @return BlogModel
     */
    public static function model()
    {
        return bff::model('Blog');
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'blog_post_blocked' => array(
                'title'       => 'Блог: Запись заблокирована модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при блокировке записи модератором',
                'vars'        => array(
                    '{fio}'            => 'ФИО пользователя',
                    '{name}'           => 'Имя пользователя',
                    '{post_id}'        => 'ID записи',
                    '{post_title}'     => 'Заголовок записи',
                    '{post_url}'       => 'Ссылка для просмотра записи',
                    '{blocked_reason}' => 'Причина блокировки',
                ),
                'impl'        => true,
                'priority'    => 70,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
            'blog_post_approved' => array(
                'title'       => 'Блог: Запись одобрена модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при одобрении записи модератором',
                'vars'        => array(
                    '{fio}'        => 'ФИО пользователя',
                    '{name}'       => 'Имя пользователя',
                    '{post_id}'    => 'ID записи',
                    '{post_title}' => 'Заголовок записи',
                    '{post_url}'   => 'Ссылка для просмотра записи',
                ),
                'impl'        => true,
                'priority'    => 71,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
        );
        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Список постов
            case 'list':
                $url .= '/blog/' . ( ! empty($opts['keyword']) ? $opts['keyword'].'/' : '' ) .
                    static::urlQuery($opts, array('keyword'));
                break;
            # Просмотр постов из общего списка
            case 'view':
                $url .= '/blog/' . $opts['id'] . '-' . $opts['keyword'] .
                    static::urlQuery($opts, array('id', 'keyword'));
                break;
            # Добавление поста
            case 'add':
                $url .= '/blog/add' . ( ! empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Инфо о статусе поста
            case 'status':
                $url .= '/blog/status' . static::urlQuery($opts);
                break;
            # Редактирование поста
            case 'edit':
                $url .= '/blog/edit' . static::urlQuery($opts);
                break;
            # Кабинет: блог
            case 'my.blog':
                $opts['login'] = User::data('login');
                $url = static::url('user.listing', $opts, $dynamic);
                break;
            # Профиль: блог
            case 'user.listing':
                $url = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'blog'), $dynamic);
                $url .= (!empty($opts) ? static::urlQuery($opts, array('login')) : '');
                break;
            # Просмотр поста в кабинете
            case 'user.view':
                $url = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'blog'), $dynamic);
                $url .= $opts['id'] . '-' . $opts['keyword'] .
                    static::urlQuery($opts, array('id','keyword','login'));
                break;
            # Поиск постов по тегу
            case 'search-tag':
                $opts['tag'] = mb_strtolower($opts['tag']).'-'.$opts['id'];
                $url .= '/blog/'.static::urlQuery($opts, array('id'));
                break;
            # Поиск постов по категории
            case 'search-cat':
                $url .= '/blog/'.$opts['cat1_keyword'].'/'.( ! empty($opts['cat2_keyword']) ? $opts['cat2_keyword'].'/' : '');
                break;
        }
        return bff::filter('blog.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        $aTemplates = array(
            'pages'  => array(
                'listing' => array(
                    't'      => 'Список (главная)',
                    'list'   => true,
                    'macros' => array(),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'listing-category' => array(
                    't'       => 'Список (категория)',
                    'list'    => true,
                    'inherit' => true,
                    'macros'  => array(
                        'category' => array('t' => 'Название категории'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-keyword' => array(
                    't'      => 'Поиск по ключевому слову',
                    'list'   => true,
                    'macros' => array(
                        'query' => array('t'=>'Строка запроса'),
                    ),
                ),
                'view' => array(
                    't'       => 'Просмотр поста',
                    'macros'  => array(
                        'title'       => array('t' => 'Заголовок (до 50 символов)'),
                        'title.full'  => array('t' => 'Заголовок (полный)'),
                        'description' => array('t' => 'Краткое описание (до 150 символов)'),
                        'tags'        => array('t' => 'Теги'),
                    ),
                    'fields' => array(
                        'share_title'       => array(
                            't'    => 'Заголовок (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                        'share_description' => array(
                            't'    => 'Описание (поделиться в соц. сетях)',
                            'type' => 'textarea',
                        ),
                        'share_sitename'    => array(
                            't'    => 'Название сайта (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                    ),
                ),
            ),
        );

        if (!static::tagsOn()) {
            unset($aTemplates['pages']['listing-tag']);
            unset($aTemplates['pages']['view']['macros']['tags']);
        }

        return $aTemplates;
    }

    /**
     * Включен ли раздел "Блоги"
     * @return bool true - включен
     */
    public static function enabled()
    {
        return config::sysAdmin('blog.enabled', true, TYPE_BOOL);
    }

    /**
     * Использовать премодерацию постов
     * @return bool
     */
    public static function premoderation()
    {
        return config::sysAdmin('blog.premoderation', false, TYPE_BOOL);
    }

    /**
     * Использовать публикатор для создания/редактирования постов
     */
    public static function publicatorEnabled()
    {
        return config::sysAdmin('blog.publicator', false, TYPE_BOOL);
    }

    /**
     * Использовать теги
     * @return bool
     */
    public static function tagsOn()
    {
        return config::sysAdmin('blog.tags.on', true, TYPE_BOOL);
    }

    /**
     * Максимально допустимое кол-во фотографий прикрепляемых к посту
     * 0 - возможность прикрепления фотографий выключена (недоступна)
     * @return integer
     */
    public static function imagesLimit()
    {
        return config::sysAdmin('blog.images.limit', 8, TYPE_UINT);
    }

    /**
     * Инициализация компонента работы с тегами
     * @return BlogPostTags
     */
    public function postTags()
    {
        static $i;
        if (!isset($i)) {
            $i = new BlogPostTags();
            if (!bff::adminPanel()) {
                $i->module_dir_tpl = $this->module_dir_tpl;
            }
        }
        return $i;
    }

    /**
     * Инициализация компонента работы с комментариями к постам
     * @return BlogPostComments component
     */
    public function postComments()
    {
        static $i;
        if (!isset($i)) {
            $i = new BlogPostComments();
        }
        return $i;
    }

    /**
     * Инициализация компонента обработки фотографий постов BlogPostImages
     * @param mixed $nPostID ID поста
     * @return BlogPostImages component
     */
    public static function postImages($nPostID = false)
    {
        static $i;
        if ( ! isset($i)) {
            $i = new BlogPostImages();
        }
        $i->setRecordID($nPostID);
        return $i;
    }

    /**
     * Является ли текущий пользователь владельцем поста
     * @param integer $nPostID ID поста
     * @param integer|bool $nPostUserID ID пользователя поста или FALSE (получаем из БД)
     * @return boolean
     */
    public function isPostOwner($nPostID, $nPostUserID = false)
    {
        $nUserID = User::id();
        if (!$nUserID) {
            return false;
        }

        if ($nPostUserID === false) {
            $aData = $this->model->postData($nPostID, array('user_id'));
            if (empty($aData)) {
                return false;
            }

            $nPostUserID = $aData['user_id'];
        }

        return ($nPostUserID > 0 && $nUserID == $nPostUserID);
    }

    /**
     * Актуализация счетчика постов ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->postsModeratingCounter();
            config::save('blog_posts_moderating', $count, true);
        } else {
            config::saveCount('blog_posts_moderating', $increment, true);
        }
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nPostID ID поста или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @param bff\db\Publicator|boolean $publicator или FALSE
     * @return array параметры
     */
    protected function validatePostData($nPostID, $bSubmit, $publicator = false)
    {
        $aData = array();
        $aParam = array(
            'cat_id'    => TYPE_UINT, # Категория
            'title'     => array(TYPE_NOTAGS, 'len' => 200, 'len.sys' => 'blog.title.limit'),  # Заголовок
            'content' => $publicator ? TYPE_ARRAY : TYPE_STR, # Описание
            'comments_enabled' => TYPE_BOOL, # Комментарии включены
        );
        if (!$nPostID) {
            if (bff::adminPanel()) {
                $aParam['user_id'] = TYPE_UNUM;
            }
        }
        $this->input->postm($aParam, $aData);

        if ($bSubmit)
        {
            # Категория
            $nCategoryID = $aData['cat_id'];
            if ( ! $nCategoryID) {
                $this->errors->set(_t('blog', 'Выберите категорию'));
            } else {
                # сохраняем ID категорий(parent и текущей), для возможности дальнейшего поиска по ним
                $nParentsID = $this->model->categoryParentsID($nCategoryID, true);
                foreach ($nParentsID as $lvl=>$id) {
                    $aData['cat_id'.$lvl] = $id;
                }
                if ( ! isset($aData['cat_id2'])) {
                    $aData['cat_id2'] = 0; # Корректировка ошибки при изменении категории c подкатегорию на основную
                }
            }

            if ( ! $nPostID) {
                if ( ! bff::adminPanel()) {
                    $aData['user_id'] = User::id();
                }
                if ( ! $aData['user_id']) {
                    $this->errors->set(_t('blog','Укажите пользователя'));
                }
            }

            if ($publicator) {
                $data = $publicator->dataPrepare($aData['content'], $nPostID);
                $aData['content'] = $data['content'];
                $aData['content_search'] = $data['content_search'][LNG];
                $aData['content_preview'] = nl2br(tpl::truncate($aData['content_search'], config::sysAdmin('blog.content_preview.truncate', 300, TYPE_UINT)));
                if (mb_strlen($aData['content_search']) < config::sysAdmin('blog.content.min', 10, TYPE_UINT)) {
                    $this->errors->set(_t('blog', 'Текст слишком короткий'));
                }
            } else {
                # Формируем краткий текст для списков
                $preview = strip_tags(preg_replace('/(\<br(\s*)?\/?\>|\<p\>|\<\/p\>)/mui', "\n", $aData['content']));
                $preview = trim(preg_replace("/\n+/u", "\n", $preview), "\n ");
                $aData['content_preview'] = nl2br(tpl::truncate($preview, config::sysAdmin('blog.content_preview.truncate', 300, TYPE_UINT)));
                if (mb_strlen($aData['content']) < config::sysAdmin('blog.content.min', 10, TYPE_UINT)) {
                    $this->errors->set(_t('blog', 'Текст слишком короткий'));
                }
            }

            if ( ! bff::adminPanel()) {
                # антиспам фильтр
                Site::i()->spamFilter(array(
                    array('text' => & $aData['title'],   'error'=>_t('', 'В указанном вами заголовке присутствует запрещенное слово "[word]"')),
                    array('text' => & $aData[($publicator ? 'content_search' : 'content')], 'error'=>_t('', 'В указанном вами тексте присутствует запрещенное слово "[word]"')),
                ));
                if ( ! $publicator) {
                    # Чистим текст
                    $parser = new \bff\utils\TextParser();
                    $aData['content'] = $parser->parseWysiwygTextFrontend($aData['content']);
                    if (mb_strlen(strip_tags($aData['content'])) < config::sysAdmin('blog.content.min', 10, TYPE_UINT)) {
                        $this->errors->set(_t('blog','Текст записи слишком короткий'), 'content');
                    }
                }
            } else {
                if ( ! $publicator) {
                    # Чистим текст
                    $parser = new \bff\utils\TextParser();
                    $parseParams = array(
                        'scripts' => true,
                        'iframes' => true,
                    );

                    $aData['content'] = $parser->parseWysiwygText($aData['content'], $parseParams);
                }
            }

            # URL-Keyword
            $aData['keyword'] = mb_strtolower(func::translit($aData['title']));
            $aData['keyword'] = preg_replace('/[^a-zA-Z0-9_\-]/', '', $aData['keyword']);
        } else {
            if (!$nPostID) {
                $aData['comments_enabled'] = 1;
            }
        }
        return $aData;
    }

    /**
     * Удаление поста
     * @param integer $nPostID ID поста
     * @return boolean
     */
    function postDelete($nPostID)
    {
        if (empty($nPostID)) return false;

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
            $data = $this->model->postData($nPostID, array('content'));
        }


        $res = $this->model->postDelete($nPostID);
        if (!empty($res)) {
            $this->postImages($nPostID)->deleteAllImages(false);
            if ($publicator) {
                $publicator->dataDelete($data['content'], $nPostID);
            }
            $this->postTags()->onItemDelete($nPostID);
            # обновляем счетчик постов "на модерации"
            $this->moderationCounterUpdate();

            return true;
        }

        return false;
    }

    /**
     * Инициализируем компонент Publicator
     * @return bff\db\Publicator
     */
    public function initPublicator()
    {
        static $i;
        if ( ! isset($i)) {
            $aSettings = array(
                'title' => false,
                'langs' => $this->locale->getLanguages(),
                'images_path'     => bff::path('blog', 'images'),
                'images_path_tmp' => bff::path('tmp', 'images'),
                'images_url'      => bff::url('blog', 'images'),
                'images_url_tmp'  => bff::url('tmp', 'images'),
                # photo
                'photo_sz_view' => array('width' => 800),
                # gallery
                'gallery_sz_view' => array(
                    'width'    => 800,
                    'height'   => false,
                    'vertical' => array('width' => false, 'height' => 400),
                    'quality'  => 95,
                    'sharp'    => array(), // no sharp
                ),
                'video_width'  => 560,
                'video_height' => 315,
                'use_wysiwyg'  => true,
                'controls' => array(
                    bff\db\Publicator::blockTypeText,
                    bff\db\Publicator::blockTypePhoto,
                    bff\db\Publicator::blockTypeGallery,
                    bff\db\Publicator::blockTypeVideo,
                ),
            );

            $aSettingsConfig = config::sys('blog.publicator.settings', array());
            if (!empty($aSettingsConfig) && is_array($aSettingsConfig)) {
                $aSettings = array_merge($aSettings, $aSettingsConfig);
            }

            # настройки водяного знака
            $watermark = Site::i()->watermarkSettings('blog');
            if (!empty($watermark)) {
                $aWatermark = array(
                    'watermark' => true,
                    'watermark_src' => $watermark['file']['path'],
                    'watermark_pos_x' => $watermark['pos_x'],
                    'watermark_pos_y' => $watermark['pos_y'],
                );
                $aSettings['gallery_sz_view'] += $aWatermark;
                $aSettings['photo_sz_view'] += $aWatermark;
            }

            $i = $this->attachComponent('publicator', new bff\db\Publicator($this->module_name, $aSettings));
        }
        return $i;
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('blog', 'images') => 'dir-split', # изображения
            bff::path('tmp', 'images')  => 'dir-only', # tmp
        ));
    }

}