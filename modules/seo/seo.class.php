<?php

class SEO_ extends SEOBase
{
    private $text = '';

    public function setText(array &$data = array(), $pageID = 0, $key = 'seotext')
    {
        if (!empty($data[$key]) && $pageID <= 1) {
            $this->text = $data[$key];
        }
    }

    public function getTextBlock()
    {
        $data = array('text'=>$this->text, 'index'=>bff::isIndex(), 'register_button'=>false);
        if ($data['index']) {
            $text = trim(strtr(strip_tags($data['text']), array('&nbsp;'=>'')));
            if (empty($text)) {
                $data['text'] = config::get('index_title_'.LNG);
                if (!empty($data['text']) && ! User::id()) {
                    $data['register_button'] = true;
                }
            } else if (!User::id()) {
                $data['register_button'] = true;
            }
        }
        $data['text_notags'] = trim(strtr(strip_tags($data['text']), array('&nbsp;'=>'')));
        return $this->viewPHP($data, 'text.block');
    }
}