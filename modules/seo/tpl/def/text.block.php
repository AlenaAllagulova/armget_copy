<?php

if (!empty($text) && !empty($text_notags)) { ?>
<article class="l-homeText visible-lg visible-md">
    <div class="container">
        <?= $text; ?>
        <? if($register_button) { ?><a href="<?= Users::url('register') ?>" class="btn btn-primary"><?= _t('users', 'Зарегистрироваться'); ?></a><? } ?>
    </div>
</article>
<? }