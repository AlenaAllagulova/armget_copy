<?php

class CatcherModel extends \Model
{
    /** @var CatcherBase */
    public $controller;

    # multi-language fields
    public $langCountries = [
        'title' => TYPE_NOTAGS, # Название
    ];

    public function init()
    {
        parent::init();
    }


    /**
     * Список слов (admin)
     * @param array $aFilter фильтр списка слов
     * @param bool $bCount только подсчет кол-ва слов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */

    public function wordsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter = $this->prepareFilter($aFilter, '');
        $aFilter['where'] .= ($aFilter['where'] === '  ') ? ' WHERE CL.lang = \'' . LNG . '\'' : ' and CL.lang = \'' . LNG . '\'';

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) 
                                               FROM 
                                                   ' . TABLE_CATCHER_WORDS . '  C 
                                                   JOIN ' . TABLE_CATCHER_WORDS_LANG . ' CL ON C.id = CL.id 
                                               ' . $aFilter['where'],
                $aFilter['bind']);

        }

        return $this->db->select('
               SELECT
                   C.id,
                   C.enabled,
                   CL.title
               FROM 
                   ' . TABLE_CATCHER_WORDS . ' C 
                   JOIN ' . TABLE_CATCHER_WORDS_LANG . ' CL ON C.id = CL.id 
               ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']);
    }

    /**
     * Получение данных слова
     * @param integer $nWordID ID слова
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function wordData($nWordID, $bEdit = false)
    {
        $aData = [];
        if ($bEdit) {
            $aSeltctData = $this->db->select(
                'SELECT 
                            С.id, 
                            С.enabled,
                            СL.title,
                            СL.lang
                        FROM 
                            ' . TABLE_CATCHER_WORDS . ' С 
                            JOIN ' . TABLE_CATCHER_WORDS_LANG . ' СL ON С.id = СL.id 
                        WHERE СL.id = :id',
                [':id' => $nWordID]
            );

            foreach ($aSeltctData as $item) {
                if (is_array($item)) {
                    $aData['id'] = $item['id'];
                    $aData['enabled'] = $item['enabled'];
                    $aData['title'][$item['lang']] = $item['title'];
                }
            }

        }
        return $aData;
    }

    /*
     * Сохранение слова
     * @param integer $nCountryID ID слова
     * @param array $aData данные слова
     * @return boolean|integer
     */
    public function wordSave($nCountryID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }

        if ($nCountryID > 0) {
            $res = $this->db->update(
                TABLE_CATCHER_WORDS,
                array_diff_key($aData, $this->langCountries),
                ['id' => $nCountryID]);

            $this->db->langUpdate($nCountryID, $aData, $this->langCountries, TABLE_CATCHER_WORDS_LANG);

            return !empty($res);
        } else {
            $nCountryID = $this->db->insert(
                TABLE_CATCHER_WORDS,
                array_diff_key($aData, $this->langCountries)
            );

            if (!empty($nCountryID)) {
                $this->db->langInsert($nCountryID, $aData, $this->langCountries, TABLE_CATCHER_WORDS_LANG);
                return $nCountryID;
            } else {
                return false;
            }
        }
    }

    /**
     * Переключатель слова
     * @param integer $nWordID ID слова
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function wordToggle($nWordID, $sField)
    {
        if ($sField) {
            return $this->toggleInt(TABLE_CATCHER_WORDS, $nWordID, $sField, 'id');
        }
        return false;
    }

    /**
     * Удаление слова
     * @param integer $nWordID ID слова
     * @return boolean
     */
    public function wordDelete($nWordID)
    {
        if (empty($nWordID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_CATCHER_WORDS, ['id' => $nWordID]);

        return $res;
    }

    /**
     * Показывать контакты / Не проводить проверку в переписке (наличие зарезервированных средств по заказу)
     * @param $authorID
     * @param $recipientID
     * @return mixed
     */
    public function enableShowContacts($authorID, $recipientID)
    {
        $bind = [
            ':author_id'    => $authorID,
            ':recipient_id' =>  $recipientID,
            ':status'       => Fairplay::STATUS_RESERVATION
        ];

        $sql = 'SELECT 1 
                FROM 
                    ' . TABLE_FAIRPLAY_WORKFLOWS . ' W
                    JOIN ' . TABLE_FAIRPLAY_WORKFLOWS_BILLS. ' WB ON (
                        W.id = WB.workflow_id AND 
                        W.client_id = WB.user_id)
                WHERE 
                    W.client_id IN (:author_id, :recipient_id) AND 
                    W.worker_id IN (:author_id, :recipient_id) AND 
                    W.status > :status';

        $res = $this->db->one_data($sql, $bind);
        return $res;

    }

    public function getAllStopWords()
    {
        $filter = $this->prepareFilter(['enabled' => true ], 'C');
        $res = $this->db->select_one_column('
               SELECT
                   CL.title
               FROM ' . TABLE_CATCHER_WORDS_LANG . ' CL 
               JOIN ' . TABLE_CATCHER_WORDS . ' C ON C.id = CL.id 
               '.$filter['where'], $filter['bind']);
        return $res ;
    }

    public function getWorkflowOrderPrepay($fairplayWorkflowID)
    {
        if (!$fairplayWorkflowID) return false;
        $sql = 'SELECT O.prepay 
                FROM '. TABLE_FAIRPLAY_WORKFLOWS .' FW 
                INNER JOIN '.DB_PREFIX.'orders'.' O ON FW.order_id = O.id 
                WHERE FW.id = :fw_id';
        $bind = [':fw_id' => $fairplayWorkflowID];
        return $this->db->one_data($sql, $bind);
    }

    /**
     * Являетя ли автор отзыва к заказу его заказчиком
     * @param $nOpinionID - ID комментария
     * @return mixed performer_id - false: заказ не закрыт, int - id исполнителя
     */
    public function  checkAuthorIsClient($nOpinionID)
    {
        return $this->db->one_data(
            'SELECT ord.performer_id 
                FROM '.TABLE_OPINIONS.' opi 
                JOIN '.TABLE_ORDERS.' ord ON opi.order_id = ord.id AND opi.author_id = ord.user_id
                              WHERE opi.id = :nOpinionID ',['nOpinionID' => $nOpinionID]);
    }

    /**
     * Кол-во закрытых заказов для исполнителя, опционально неудачно закрытых: отрицательный тип отзыва заказчика
     * @param $nPerformerId - id исполнителя
     * @param bool $bFailClosed - получить кол-во неудачно закрытых заказов
     * @return bool|mixed
     */
    public function getCntClosedOrders($nPerformerId, $bFailClosed = false)
    {
        if (!$nPerformerId){
            return false;
        }

        $sql = 'SELECT count(ord.id)
                    FROM '.TABLE_ORDERS.' ord
                    JOIN '.TABLE_OPINIONS.' opi ON opi.order_id = ord.id AND opi.author_id = ord.user_id
                    WHERE ord.performer_id = :performer_id ';
        $bind = [':performer_id' => $nPerformerId];

        if ($bFailClosed) {
            $sql .= ' AND opi.type = :type ';
            $bind = array_merge($bind, [ ':type' => Opinions::TYPE_NEGATIVE ]);
        }

        return $this->db->one_data($sql, $bind);

    }
}