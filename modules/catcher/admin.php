<?php

class Catcher extends CatcherBase
{
    const NUMBER_OF_PAGES = 20;

    public function listing()
    {
        if (!$this->haveAccessTo('Catcher')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = [];
            switch ($sAct) {
                case 'add':
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateWordData();

                    if ($bSubmit) {
                        if ($this->errors->no()) {
                            $aData['id'] = $this->model->wordSave(0, $aData);
                            $this->model->wordSave($aData['id'], $aData);
                        }

                        break;
                    }

                    $aData['id'] = 0;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.word.form');
                    break;
                case 'edit':
                    $bSubmit = $this->input->postget('save', TYPE_BOOL);
                    $nWordID = $this->input->postget('id', TYPE_UINT);
                    if (!$nWordID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        $aData = $this->validateWordData();

                        if ($this->errors->no()) {
                            $this->model->wordSave($nWordID, $aData);
                        }

                        $aData['id'] = $nWordID;
                    } else {
                        $aData = $this->model->wordData($nWordID, true);

                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }

                        $aData['act'] = $sAct;
                        $aResponse['form'] = $this->viewPHP($aData, 'admin.word.form');
                    }
                    break;
                case 'toggle':
                    $nWordID = $this->input->postget('id', TYPE_UINT);
                    if (!$nWordID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);
                    $this->model->wordToggle($nWordID, $sToggleType);

                    break;
                case 'delete':
                    $nWordID = $this->input->postget('id', TYPE_UINT);
                    if (!$nWordID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->wordData($nWordID, true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->wordDelete($nWordID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }

                    break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = [];
        $this->input->postgetm([
            'page' => TYPE_UINT, # пагинация
            'tab' => TYPE_INT,  # табы
            'title' => TYPE_NOTAGS, # поиск
        ], $f);
        # формируем фильтр списка стран
        $sql = [];
        $sqlOrder = '';
        $mPerpage = self::NUMBER_OF_PAGES;
        $aData['pgn'] = '';
        # Табы
        switch ($f['tab']) {
            case 1: # Неактивные по времени или отключенные
                $sql['enabled'] = false;
                break;
            case 0: # весь список
                break;
        }

        # Поисковая форма по "Название или ID"
        if (!empty($f['title'])) {
            $sql[':title'] = [
                '(C.id = ' . intval($f['title']) . ' OR CL.title LIKE :name)',
                ':name' => '%' . $f['title'] . '%'
            ];
        }
        if ($mPerpage !== false) {
            $nCount = $this->model->wordsListing($sql, true);
            $sqlLimit = '';
            if ($nCount > 0) {

                $oPgn = new Pagination($nCount,
                    $mPerpage,
                    $this->adminLink('listing' . '&page=' . Pagination::PAGE_ID .
                        '&tab=' . $f['tab'] . '&title=' . $f['title']));
                $sqlLimit = $oPgn->getLimitOffset();
                $aData['pgn'] = $oPgn->view();
            }

            $aData['list'] = $this->model->wordsListing($sql, false, $sqlLimit, $sqlOrder);
        } else {
            $aData['list'] = $this->model->wordsListing($sql, false, '', $sqlOrder);
        }

        $aData['list'] = $this->viewPHP($aData, 'admin.words.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm([
                'list' => $aData['list'],
                'pgn' => $aData['pgn'],
            ]);
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        return $this->viewPHP($aData, 'admin.words.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @return array параметры
     */
    protected function validateWordData()
    {
        $aData = [];
        $this->input->postm_lang($this->model->langCountries, $aData); # multi-language-fields
        $this->input->postm([
            'enabled' => TYPE_BOOL, # Включен
        ], $aData);

        foreach ($aData['title'] as $lang=>$title){ # Если не заполнено поле на др языках, будет равно локали по-умолчанию
           if(empty($title)){
               $aData['title'][$lang] = $aData['title'][LNG];
           }
        }

        return $aData;
    }
}