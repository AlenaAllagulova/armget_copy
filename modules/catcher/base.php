<?php

abstract class CatcherBase extends \Module
{
    /** @var CatcherModel */
    public $model = null;
    public $securityKey = '6c69bf39c7e976acfa8a607444552aab';

    const MIN_PREPAY = 199; # мин предоплата для резервирования средств в валюте по умолчанию

    const MIN_ORDERS_PRICE = 1000; # мин цена заказа в валюте по умолчанию

    const SEARCH_FORM_USERS = 'user_search'; # id формы поиска для модуля users

    static $articleShareData = [];
    public function init()
    {
        parent::init();

        $this->module_title = _t('catcher', 'Catcher');
    }

    /**
     * Shortcut
     * @return Catcher
     */
    public static function i()
    {
        return bff::module('Catcher');
    }

    /**
     * Shortcut
     * @return CatcherModel
     */
    public static function model()
    {
        return bff::model('Catcher');
    }


    /**
     * Использовать БС как единстванный способ оплаты  заказа
     */
    public static function onlyFairplay()
    {
        if (!bff::fairplayEnabled()){
            return false;
        }

        return config::sys('orders.fairplay.only', false, TYPE_BOOL);
    }

    /**
     * Получаем сумму предоплаты, которую указал заказчик для резервирования
     * @param $data
     * @return mixed
     */
    public static function getOrderPrepay($data)
    {
        if (!Catcher::isUseOrderPrepay()) {
            return $data;
        }
        if(empty($data['id'])){
            if(empty($data['order_id'])){
                return $data;
            }else{
                $order = Orders::model()->orderData($data['order_id'], array('prepay'));
                if (!empty($order['prepay']) ){
                    $data['prepay'] = (int)$order['prepay'];;
                }
                return $data;
            }
        }
        $prepay = self::i()->model->getWorkflowOrderPrepay($data['id']);
        if (!empty($prepay) ){
            $data['prepay'] = (int)$prepay;;
        }
        return $data;
    }

    public static function isHoldMinPrepay()
    {
        return config::sys('fairplay.hold.min.prepay', false, TYPE_BOOL);
    }

    /**
     * Заменять ли специальными символами отловленные слова и контакты в переписке
     * @return mixed
     */
    public static function isReplaceCatchedWords()
    {
        return config::sys('users.contacts.replace.catched.words', false, TYPE_BOOL);
    }

    /**
     * Настройка переключения принципа отображения контактов в проекте
     * true - обязательное наличие предоплаты по заказу, авторизированные пользователи, разные типы пользователей
     * false - стандартное поведение и настройка в админке "пользоватьли"=>"настройки"=>"контакты"
     * @return mixed
     */
    public static function isViewContactsWithPrepay()
    {
        return config::sys('users.contacts.view.prepay', false, TYPE_BOOL);
    }

    /**
     * Запрашивать слайдер для безопасной сделки
     * @return bool
     */
    public static function sliderPrice()
    {
        return config::sys('order.add.slider.price', false, TYPE_BOOL);
    }

    public function getSuggest()
    {
        $aResponse = [];
        $aData = [];

        $qStr = $this->input->getpost('q_str', TYPE_STR);
        $sSearchForm = $this->input->getpost('search_form', TYPE_STR);
        $aFilter['text'] = $qStr;

        if (!empty($qStr)) {
            # Формирование списка заказов
            $aData['orders_list'] = [];
            if($sSearchForm != self::SEARCH_FORM_USERS && User::isWorker()){ # не используем на странице исполнителей, а также не используем на главной для не авторизир пользователя и заказчика
                $nOrdersCount = Orders::model()->ordersList($aFilter, true);
                if ($nOrdersCount) {
                    $aData['orders_list'] = Orders::model()->ordersList($aFilter, false, '', 'svc_fixed DESC, O.svc_fixed_order DESC, O.svc_order DESC');
                    foreach ($aData['orders_list'] as $k => &$v) {
                        $v['url_view'] = Orders::url('list', ['q' => $v['title']]);
                        foreach (explode(' ', $qStr) as $vv) {
                            $v['title'] = str_replace($vv, '<b>' . $vv . '</b>', $v['title']);
                            $v['descr'] = str_replace($vv, '<b>' . $vv . '</b>', $v['descr']);
                        }

                    }
                    unset($v);
                }
            }


            # Формирование списка исполнителей
            $aData['users_list'] = [];
            $nUsersCount = Users::model()->searchList($aFilter, true);
            if ($nUsersCount) {
                $aData['users_list'] = Users::model()->searchList($aFilter, false, '', ' U.pro DESC ');
                foreach ($aData['users_list'] as $k => &$v) {
                    $v['user_link'] = Users::url('list', ['q' => $v['name']]);
                    if ($qStr) {
                        foreach (explode(' ', $qStr) as $vv) {
                            $v['name'] = str_replace($vv, '<b>' . $vv . '</b>', $v['name']);
                            $v['surname'] = str_replace($vv, '<b>' . $vv . '</b>', $v['surname']);
                            $v['login_title'] = str_replace($vv, '<b>' . $vv . '</b>', $v['login']);
                        }
                    }
                }
                unset($v);
            }

            # Формирование списка специализаций
            $aData['spec_list'] = $this->model->db->select('SELECT SL.title, SL.id, S.keyword
                                                              FROM ' . TABLE_SPECIALIZATIONS_LANG . '  SL
                                                              INNER JOIN '.TABLE_SPECIALIZATIONS.' S ON SL.id = S.id
                                                              WHERE SL.title LIKE :title AND SL.lang = :lng ',
                [':title' => '%' . trim($qStr) . '%',
                    ':lng' => LNG]);
            foreach ($aData['spec_list'] as $k => &$v) {

                $v['spec_link'] = Orders::url('add', ['q' => $v['title']]);

                if ($sSearchForm == self::SEARCH_FORM_USERS) { # для поиска на стр исполнителей, для авторизир пользователей
                    $nSpecUsersCnt = Users::model()->searchList(['spec_id' => $v['id']], true);
                    if ($nSpecUsersCnt || User::isWorker()) {
                        $v['spec_link'] = Users::url('list', ['keyword' => $v['keyword']]);

                    }
                } else {
                    if (User::isClient() || ! User::id()) {
                        $nSpecUsersCnt = Users::model()->searchList(['spec_id' => $v['id']], true);
                        if ($nSpecUsersCnt) {
                            $v['spec_link'] = Users::url('list', ['keyword' => $v['keyword']]);
                        }
                    }
                    if (User::isWorker()) {
                        $v['spec_link'] = Orders::url('list');
                        $nSpecOrdersCnt = Orders::model()->ordersList(['spec_id' => $v['id']], true);
                        if ($nSpecOrdersCnt) {
                            $v['spec_link'] = Orders::url('list', ['orders' => $v['keyword']]);
                        }
                    }
                }
            }
            unset($v);

            $aData['cat_list'] = [];
            if (Specializations::catsOn()) {
                # Формирование списка категорий специализаций
                $aData['cat_list'] = $this->model->db->select('SELECT SCL.title, SCL.id, SC.keyword
                                                              FROM ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' SCL
                                                              INNER JOIN '.TABLE_SPECIALIZATIONS_CATS.' SC ON SCL.id = SC.id
                                                              WHERE SCL.title LIKE :title AND SCL.lang = :lng AND SC.id != :root_spec ',
                    [
                        ':title'     => '%' . trim($qStr) . '%',
                        ':lng'       => LNG,
                        ':root_spec' => Specializations::ROOT_SPEC,
                    ]);
                foreach ($aData['cat_list'] as $k => &$v) {

                    $v['cat_link'] = Orders::url('add', ['q' => $v['title']]);

                    if ($sSearchForm == self::SEARCH_FORM_USERS) { # для поиска на стр исполнителей
                        $nSpecUsersCnt = Users::model()->searchList(['cat_id' => $v['id']], true);
                        if ($nSpecUsersCnt || User::isWorker()) {
                            $v['cat_link'] = Users::url('list', ['keyword' => $v['keyword']]);
                        }
                    } else {
                        if (User::isClient() || !User::id()) {
                            $nSpecUsersCnt = Users::model()->searchList(['cat_id' => $v['id']], true);
                            if ($nSpecUsersCnt) {
                                $v['cat_link'] = Users::url('list', ['keyword' => $v['keyword']]);
                            }
                        }

                        if (User::isWorker()) {
                            $v['cat_link'] = Orders::url('list');
                            $aSpecsInCatData = Specializations::model()->specializationsInCategory($v['id']);
                            $aFilterSpec['spec_id'] = [];
                            $aCatKeywords = [];
                            foreach ($aSpecsInCatData as $spec){
                                $aFilterSpec['spec_id'][] = $spec['id'];
                                $aCatKeywords[] = $spec['keyword'];
                            }
                            $nSpecOrdersCnt = Orders::model()->ordersList($aFilterSpec, true);
                            if ($nSpecOrdersCnt) {
                                $v['cat_link'] = Orders::url('list', ['orders' => join(',', $aCatKeywords)]);
                            }
                        }
                    }
                }
                unset($v);
            }
            $aData['tag_list'] = [];
            $aData['tag_list'] = $this->db->select('SELECT * FROM '.TABLE_TAGS.' WHERE tag LIKE :tag AND moderated = true ', array(':tag' =>  '%' . trim($qStr) . '%'));
            foreach ($aData['tag_list'] as $k => &$v){
                $v['tag_link'] = Users::url('search-tag', ['tag' => $v['tag'], 'id' => $v['id']]);
                if (User::isWorker() && $sSearchForm != self::SEARCH_FORM_USERS) {
                    $v['tag_link'] = Orders::url('search-tag', ['tag' => $v['tag'], 'id' => $v['id']]);
                }
            }

            $aData['list'] = array_merge($aData['orders_list'], $aData['users_list'], $aData['spec_list'], $aData['cat_list'], $aData['tag_list']);
            $aData['suggest_list'] = $this->viewPHP($aData, 'search.suggest.list');
        }

        $aResponse['res'] = $this->errors->no();
        $aResponse['list'] = $aData['suggest_list'];
        $this->ajaxResponse($aResponse);
    }

    public static function isUseOrderPrepay()
    {
        return config::sys('order.prepay.use',false, TYPE_BOOL);
    }

    /**
     *  Использовать пометку "договорная цена" для типа заказа постоянная работа и значения цены 0 у.e.
     *  (даже при выключенной настройке в специализации)
     */
    public static function isOrderJobUsePriceEx()
    {
        return config::sys('order.job.use.price.ex',true, TYPE_BOOL);
    }

    public static function getArticleShareData()
    {
        return self::$articleShareData;
    }

    public static function setArticleShareData($nArticleID = false)
    {
        if(!$nArticleID){
            return [];
        }

        $aArticleData = Articles::model()->articleData($nArticleID);
        $aArticleData['main_img'] = $aArticleData['img_v'];

        self::$articleShareData = $aArticleData;
        return $aArticleData;
    }

}