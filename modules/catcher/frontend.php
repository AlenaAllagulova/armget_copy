<?php

class Catcher extends CatcherBase
{
    private $patternPhone = '/
        (?<![\d-()]) # "?<!" - отрицательное назад смотрящее; [\d-()] - диапазон цифра, или - или ( или )
        [-()_\s+]* # диапазон - или ( или ) или _ или пробел или +; * 0 и более раз
        (?:[\d][-()\s]*)? # "?:" - пассивная группа; "[\d]" - число; "[-()\s]" - диапазон - или ( или ) или пробел; * - 0 и более раз; "?" - 0 или 1 раз
        (?:\d[-()_\s]*) 
        {6,14} # кол-во повторений
        (?!\d) # отрицание вперед смотрящее; "\d" - число 
    /x';

    private $patternEmail = "/[-a-z0-9!#$%&'*_`{|}~]+[-a-z0-9!#$%&'*_`{|}~\.=?]*@[a-zA-Z0-9_-]+[a-zA-Z0-9\._-]+/i";

    private $replaceSymbol = ' ***** ';

    const FROM_CHAT = 1;
    const FROM_FIREPLAY_CHAT = 2;
    const FROM_OFFER = 3;
    const FROM_OFFER_CHAT = 4;


    /**
     * Проверка сообщения
     * @param integer $authorID ID отправителя
     * @param integer $recipientID ID получателя
     * @param string $message текст сообщения
     * @param integer $place место переписки
     */
    public function checkMessage($authorID, $recipientID, &$message = '', $place = self::FROM_CHAT)
    {
        $isPayedOrderBetween = $this->model->enableShowContacts($authorID, $recipientID);

        if (!$isPayedOrderBetween) {
            if ($this->checkEmail($message) || $this->checkTelephone($message) || $this->checkWords($message)) {
                $this->notifyAdminCatchContacts($authorID, $recipientID, $message, $place);
                # замена контактов и запрещенных слов на символы
                $message = $this->getMessageWithoutCatchedWords($authorID, $recipientID, $message);
            }
        }
    }

    private function checkEmail($message)
    {
        preg_match($this->patternEmail, $message, $match);
        return !empty($match);
    }

    private function checkTelephone($message)
    {
        $pattern = $this->patternPhone;

        preg_match($pattern, $message, $match);
        return !empty($match);
    }

    private function checkWords($message)
    {
        $stopWords = $this->model->getAllStopWords();

        foreach ($stopWords as $word) {
            if(!empty($word)){
                $word = preg_quote($word, '/');
                preg_match("/{$word}/", $message, $match);

                if (!empty($match)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function notifyAdminCatchContacts($authorID, $recipientID, $message = '', $place = self::FROM_CHAT)
    {
        $authorData = Users::model()->userData($authorID, ['login', 'name', 'surname', 'email']);
        $authorName = $authorData['name'] .' '. $authorData['surname'] . ' login: ' . $authorData['login'];
        $recipientData = Users::model()->userData($recipientID, ['login', 'name','surname', 'email']);
        $sMessage = _t('', ' Пользователь [author_name], id = [author_id] 
                            отправил пользователю [recipient_name], id = [recipient_id] 
                            [place_title], содержащее контактные данные: 
                            "[msg]"', [
            'author_name'    => $authorName,
            'author_id'      => $authorID,
            'recipient_name' => $recipientData['name'] .' '. $recipientData['surname'] . ' login: ' . $recipientData['login'],
            'recipient_id'   => $recipientID,
            'msg'            => $message,
            'place_title'    => $this->getPlaceTitle($place),
        ]);

        $this->writeContacts($authorName, $authorData['email'], $sMessage, Contacts::TYPE_CATCHER);
    }

    public function writeContacts($authorName, $authorEmail, $sMessage, $cType)
    {
        if(!$authorEmail || !$sMessage || !$cType){
            return;
        }
        $p = array(
            'name'    => $authorName,  # имя
            'email'   => $authorEmail, # e-mail
            'ctype'   => $cType,       # тип уведомления
            'message' => $sMessage,    # сообщение
        );

        # проверяем на корректность "тип контакта", если некорректный берем первый из доступных
        $aContactTypes = Contacts::i()->getContactTypes(false);
        if (!isset($aContactTypes[$p['ctype']])) {
            $p['ctype'] = key($aContactTypes);
        }

        $nContactID = Contacts::model()->contactSave(0, $p);

        if ($nContactID) {
            Contacts::i()->updateCounter($p['ctype'], 1);
        }
    }

    private function replacementEmail($message)
    {
        return preg_replace($this->patternEmail, $this->replaceSymbol, $message);
    }

    private function replacementTelephone($message)
    {
        $pattern = $this->patternPhone;

        return preg_replace($pattern, $this->replaceSymbol, $message);
    }

    private function replacementWords($message)
    {
        $stopWords = $this->model->getAllStopWords();
        foreach ($stopWords as $word) {
            if(!empty($word)) {
                if(stripos($word, "/")){
                    $word = str_replace("/", "\/", $word);
                }
                $message = preg_replace("/{$word}/", $this->replaceSymbol, $message);
            }
        }

        return $message;
    }

    public function getMessageWithoutCatchedWords($authorId, $recepientId, $message)
    {
        $isPayedOrderBetween = $this->model->enableShowContacts($authorId, $recepientId);

        if (!$isPayedOrderBetween && self::isReplaceCatchedWords()) {
            $message = $this->replacementEmail($message);
            $message = $this->replacementTelephone($message);
            $message = $this->replacementWords($message);
        }

        return $message;
    }

    private function getPlaceTitle($placeId)
    {
        $placeTitle = '';
        switch ($placeId){
            case self::FROM_CHAT : {
                $placeTitle = _t('catcher_chat', ' сообщение в чате ');
                break;
            }
            case self::FROM_FIREPLAY_CHAT : {
                $placeTitle = _t('catcher_fireplay_chat', ' сообщение в ходе работ ');
                break;
            }
            case self::FROM_OFFER : {
                $placeTitle = _t('catcher_offer', ' предложение к заказу ');
                break;
            }
            case self::FROM_OFFER_CHAT : {
                $placeTitle = _t('catcher_offer', ' сообщение в чате в предложении к заказу ');
                break;
            }
            default:{
                $placeTitle = _t('catcher_chat', ' сообщение ');
                break;
            }
        }

        return $placeTitle;
    }

    /**
     * Проверка отображения контактов пользователям, условия:
     * 1) авторизированный пользователь
     * 2) пользователи разного типа
     * 3) наличие зарезервированных средств по заказу
     * @param $nCurrentUserID
     * @param $nCompanionUserID
     * @return bool
     */
    public function isShowContacts($nCurrentUserID, $nCompanionUserID)
    {
        if (!User::id() || !$nCurrentUserID || !$nCompanionUserID) {
            return false;
        }
        $aCurrentUserData = Users::model()->userData($nCurrentUserID, ['type']);
        $aCompanionUserData = Users::model()->userData($nCompanionUserID, ['type']);

        # не показывать контакты пользователям одного типа
        if( $aCurrentUserData['type'] == $aCompanionUserData['type']){
            return false;
        }
        # проверка наличия зарезервированных средств по заказу
        return (bool)$this->model->enableShowContacts($nCurrentUserID, $nCompanionUserID);
    }

    public function getIndexCatsBlock()
    {
        $aData['specs'] = Specializations::model()->specializationsInAllCategories(
            array('id', 'keyword', 'title'), array('id', 'keyword', 'title', 'icon_s'), true
        );
        if ($aData['specs']) {
            $url = Users::url('list');
            foreach ($aData['specs'] as &$v) {
                $v['url'] = $url.$v['keyword'].'/';
                if ( ! empty($v['specs'])) {
                    foreach ($v['specs'] as &$vv) {
                        $vv['url'] = $v['url'].$vv['keyword'].'/';
                    } unset($vv);
                }
                $oIcon = Specializations::categoryIcon($v['id']);
                foreach($oIcon->getVariants() as $iconField=>$icon) {
                    $oIcon->setVariant($iconField);
                    $v['icon_url_list'] = $oIcon->url($v['id'], $v[$iconField], CategoryIcon::szList);
                }
            } unset($v);
        }

        return $this->viewPHP($aData, 'index.cats.block');
    }

    public static function getIndexBtnUrl($nType)
    {
        $url = Users::url('list');

        if (!User::id() || !$nType) {
            return '';
        }

        if ($nType == Users::TYPE_CLIENT) {
            if (User::isClient()) {
                return Orders::url('add');
            } else {
                return Users::url('list');
            }
        }
        if ($nType == Users::TYPE_WORKER){
            return Orders::url('list');
        }
    }

    /**
     * Проверка статистики исполнителя по отмененным(неуспешным) заказам
     * @param $nOpinionID - отзыв к заказу
     */
    public function chekingPerformerCanсeledOrders($nOpinionID)
    {
        if (!$nOpinionID){
            return;
        }

        # проверка на закрытие заказа: наличие отзыва от заказчика, в случае  закрытого заказа получаем id исполнителя
        $nPerformerId = $this->model->checkAuthorIsClient($nOpinionID);

        if(!$nPerformerId){
            return;
        }

        $nCntClosedOrders = $this->model->getCntClosedOrders($nPerformerId);
        if($nCntClosedOrders < $this->getCntOrdersStartControl()){
            return;
        }

        $nCntFailClosedOrders = $this->model->getCntClosedOrders($nPerformerId, true);
        $nPercentFailClosedOrders = round(($nCntFailClosedOrders/$nCntClosedOrders)*100, 2);
        if(  $nPercentFailClosedOrders < $this->getPercentStartControl()){
            return;
        }
        
        $this->notifyAdminPerformerFailOrders($nPerformerId, $nPercentFailClosedOrders);
    }

    public function getCntOrdersStartControl()
    {
        return config::get('orders_fail_cnt', 5);
    }

    public function getPercentStartControl()
    {
        return config::get('orders_fail_percent', 15);
    }

    public function notifyAdminPerformerFailOrders($nPerformerId, $nPercentFailClosedOrders)
    {
        if (!$nPerformerId || !$nPercentFailClosedOrders){
            return;
        }
        $performerData = Users::model()->userData($nPerformerId, ['login', 'name', 'surname', 'email']);
        $performerName = $performerData['name'] .' '. $performerData['surname'] . ' login: ' . $performerData['login'];
        $sMessage = _t('orders_fail_percent', ' Исполнитель [performer_name], id = [performer_id] 
                             превысил пороговое значение процента отмененных сделок ([percent]%),
                             статистика отмененных сделок пользователя <b>[user_percent]%</b>
                            ', [
            'performer_name' => $performerName,
            'performer_id'   => $nPerformerId,
            'percent'        => $this->getPercentStartControl(),
            'user_percent'   => $nPercentFailClosedOrders,
        ]);

        $this->writeContacts($performerName, $performerData['email'], $sMessage, Contacts::TYPE_FAIL_CLOSED_ORDERS);
    }

    /**
     * Отображаеть все типы полей для ввода контактов в настройках личного кабинета
     * @param $contacts
     */
    public static function contactsSettingsViewPrepare(&$contacts)
    {
        foreach (Users::aContactsTypes() as $type){
            $is_filled_contact = false;
            foreach ($contacts as $contact){
                if ($contact['t'] == $type['id']){
                    $is_filled_contact = true;
                }
            }
            if(!$is_filled_contact){
                $contacts[] = ['t' => $type['id'], 'v' => ''];
            }
        }
    }
}