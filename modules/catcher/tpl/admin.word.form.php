<?php

$aData = HTML::escape($aData, 'html', ['title', 'enabled']);
$edit = !empty($id);
$aTabs = array(
    'info' => _t('', 'Основное'),
);

$sDeleteMsg = _t('', 'Удалить?');

?>
<form name="WordsForm"
      id="WordsForm"
      class="j-spec-cat-form"
      action="<?= ($edit ? $this->adminLink('listing&act=edit') : $this->adminLink('listing&act=add')) ?>"
      enctype="multipart/form-data"
>
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>"/>
    <input type="hidden" name="save" value="1"/>
    <input type="hidden" name="id" value="<?= $id ?>"/>
    <div id="WordsFormProgress" class="progress" style="display: none;"></div>
    <!-- таб: Основное -->
    <div class="j-tab j-tab-info">
        <table class="admtbl tbledit">
            <?= $this->locale->buildForm($aData, 'title', '
                <tr class="required">
                    <td class="row1">
                        <span class="field-title">
                            <?=_t(\'\',\'Название слова\');?>
                            <span class="required-mark">*</span>:
                        </span>
                    </td>
                    <td class="row2">
                        <input required type="text" id="word-title" name="title[<?= $key ?>]" value="<?= !empty($aData[\'title\'][$key]) ? HTML::escape($aData[\'title\'][$key]) : \'\' ?>" />
                    </td>
                </tr>
            '); ?>

            <tr>
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Включено:'); ?></span>
                </td>
                <td class="row2">
                    <label class="checkbox">
                        <input type="checkbox" id="promotion-enabled"
                               name="enabled"<? if (empty ($id) or $enabled): ?> checked="checked"<? endif; ?> />
                    </label>
                </td>
            </tr>
            <tr class="footer">
                <td colspan="2">

                </td>
            </tr>
        </table>
    </div>

    <div style="margin-top: 10px;">
        <input type="button"
               class="btn btn-success button submit j-btn-save"
               value="<?= _t('', 'Сохранить и вернуться'); ?>"
               data-redirect="true"
        />
        <input type="button" onclick="jWordsForm.del(); return false;"
               class="btn btn-danger button delete"
               value="<?= _t('', 'Удалить'); ?>"/>
        <input type="button"
               class="btn button cancel"
               value="<?= _t('', 'Отмена'); ?>"
               onclick="jWordsFormManager.action('cancel');"/>
    </div>
</form>

<script type="text/javascript">
    <?js::start()?>
    var jWordsForm =
        (function () {
            var $progress, $form, formChk, id = parseInt(<?= $id ?>), f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function () {
                $progress = $('#WordsFormProgress');
                $form = $('#WordsForm');
                f = $form.get(0);
                $('.j-btn-save').on('click', function (e) {
                    nothing(e);
                    if ($('#word-title').val() == '') {
                        bff.error('Заполните обязательные поля');
                        return false
                    }
                    var isRedirect = $(this).data('redirect');
                    var formData = new FormData($form.get(0));
                    $.ajax({
                        url: ajaxUrl,
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (data) {
                            if (data.errors.length > 0) {
                                bff.error(data.errors);
                            } else {
                                bff.success('Данные успешно сохранены');

                                if (isRedirect == true || !id) {
                                    window.setTimeout(
                                        function () {
                                            // window.location.href = ajaxUrl;
                                            jWordsFormManager.action('cancel');
                                            jWordsList.refresh(!id);
                                        }, 3000);
                                }
                            }

                        }
                    });
                });

                $('#j-icon-del').on('click', function () {
                    var $block = $(this).parent();
                    $block.hide().find('input.del-icon').val(1);
                    $block.prev().show();
                    return false;

                });
                // tabs
                $form.find('#WordsFormTabs .j-tab-toggler').on('click', function (e) {
                    nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-' + key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });
            });
            return {
                del: function () {
                    if (id > 0) {
                        bff.ajaxDelete('<?=$sDeleteMsg;?>', id, ajaxUrl + '&act=delete&id=' + id,
                            false, {
                                progress: $progress, repaint: false, onComplete: function () {
                                    bff.success('<?=_t('', 'Запись успешно удалена');?>');
                                    jWordsFormManager.action('cancel');
                                    jWordsList.refresh();
                                }
                            });
                    }
                },
                onShow: function () {
                    formChk = new bff.formChecker($form);
                },
            };
        }());
    <?js::stop()?>
</script>