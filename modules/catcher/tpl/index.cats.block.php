<?php
/**
 * Блок категорий на главной
 * @var $this Catcher
 */
?>

<div class="container">
    <p class="index-article text-center">
        <?= _t('','Бесплатно подберите IT-специалиста <br> в каталоге исполнителей *')?>
    </p>

    <? if (Specializations::catsOn()): # Использовать категории ?>
        <ul class="cats-list">
            <? foreach($specs as &$v): ?>
                <li class="cats-list__item">
                    <a href="<?= $v['url'] ?>" class="cats-list__link" style="background: url(<?= $v['icon_url_list']?>)">
                    <span>
                        <?= $v['title'] ?>
                    </span>
                    </a>
                </li>
            <? endforeach; unset($v); ?>
        </ul>
    <? else: ?>
        <ul class="cats-list">
            <? foreach($specs as &$v): ?>
                <li class="cats-list__item">
                    <a href="<?= $v['url'] ?>" class="cats-list__link">
                        <span>
                            <?= $v['title'] ?>
                        </span>
                    </a>
                </li>
            <? endforeach; unset($v); ?>
        </ul>
    <? endif; ?>

</div>
