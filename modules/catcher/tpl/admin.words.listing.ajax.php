<?php
/**
 * @var $this Catcher
 */
foreach ($list as $k => &$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k % 2) ?>">
        <td class="small"><?= $id ?></td>
        <td class="left"><?= $v['title'] ?></td>
        <td>
            <a class="but <?= ($v['enabled'] ? 'un' : '') ?>block word-toggle" title="<?= _t('', 'Включен'); ?>"
               href="#" data-type="enabled" data-id="<?= $id ?>"></a>
            <a class="but edit word-edit" title="<?= _t('', 'Редактировать'); ?>" href="javascript:void(0);"
               data-id="<?= $id ?>"></a>
            <a class="but del word-del"
               title="<?= _t('', 'Удалить'); ?>"
               href="#"
               data-id="<?= $id ?>"
            ></a>
        </td>
        </td>
    </tr>
<? endforeach;
unset($v);

if (empty($list) && !isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="7">
            <?= _t('', 'ничего не найдено'); ?>
        </td>
    </tr>
<? endif;