<?php
/**
 * @var $this Catcher
 */

tpl::includeJS(array('autocomplete'), true);

?>
<?= tplAdmin::blockStart(_t('','Добавление'), false, ['id' => 'WordsFormBlock', 'style' => 'display:none;']); ?>
<div id="WordsFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart('', true, ['id' => 'WordsListBlock', 'class' => (!empty($act) ? 'hidden' : '')],
    ['title' => '+ ' . _t('','добавить'), 'class' => 'ajax', 'onclick' => 'return jWordsFormManager.action(\'add\',0);'],
    []
); ?>
<?
$aTabs = [
    0 => ['t' => _t('','Слова')],
    1 => ['t' => _t('','Не активные')],
];
?>

<div class="tabsBar" id="WordsListTabs">
    <? foreach ($aTabs as $k => $v) : ?>
        <span class="tab <? if ($f['tab'] == $k): ?>tab-active<? endif; ?>">
            <a href="#" onclick="return jWordsList.onTab(<?= $k ?>,this);"><?= $v['t'] ?></a>
        </span>
    <? endforeach; ?>
</div>

<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="WordsListFilters"
          onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>"/>
        <input type="hidden" name="ev" value="<?= bff::$event ?>"/>
        <input type="hidden" name="page" value="<?= $f['page'] ?>"/>
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>"/>

        <div class="left">
            <div class="left" style="margin: 8px 0 0 8px;">
                <input style="width:284px;" type="text" maxlength="150" name="title"  placeholder="<?=_t('','Название или ID');?>"
                       value="<?= isset($f['title'])? HTML::escape($f['title']):'' ?>"/>
                <input type="button" class="btn btn-small button cancel" style="margin-left: 8px;"
                       onclick="jWordsList.submit(false);" value="<?=_t('','найти');?>"/>
            </div>
            <div class="left" style="margin: 8px 0 0 8px;">
                <a class="ajax cancel" onclick="jWordsList.submit(true); return false;"><?=_t('','сбросить');?></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="right">
            <div id="WordsProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="WordsListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">ID</th>
        <th class="left"><?=_t('','Название слова');?></th>
        <th width="135"><?=_t('','Действие');?></th>
    </tr>
    </thead>
    <tbody id="WordsList">
    <?= $list ?>
    </tbody>
</table>
<div id="WordsListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    <?js::start()?>
    var jWordsFormManager = (function () {
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function () {
            $formContainer = $('#WordsFormContainer');
            $progress = $('#WordsProgress');
            $block = $('#WordsFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if( ! empty($act)):?>action('<?= $act ?>',<?= $id ?>);<? endif; ?>
        });

        function onFormToggle(visible) {
            if (visible) {
                jWordsList.toggle(false);
                if (jWordsForm) jWordsForm.onShow();
            } else {
                jWordsList.toggle(true);
            }
        }

        function initForm(type, id, params) {
            if (process) return;
            bff.ajax(ajaxUrl, params, function (data) {
                if (data && (data.success || intval(params.save) === 1)) {
                    $blockCaption.html(type == 'add' ? '<?=_t('','Добавление')?>' : '<?=_t('','Редактирование')?>');
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo($blockCaption, {duration: 500, offset: -300});
                    onFormToggle(true);
                    if (bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act=' + type + '&id=' + id);
                    }
                } else {
                    jWordsList.toggle(true);
                }
            }, function (p) {
                process = p;
                $progress.toggle();
            });
        }

        function action(type, id, params) {
            params = $.extend(params || {}, {act: type});
            switch (type) {
                case 'add': {
                    if (id > 0) return action('edit', id, params);
                    if ($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                }
                    break;
                case 'cancel': {
                    $block.hide();
                    onFormToggle(false);
                }
                    break;
                case 'edit': {
                    if (!(id || 0)) return action('add', 0, params);
                    params.id = id;
                    initForm(type, id, params);
                }
                    break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jWordsList =
        (function () {
            var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, tab = <?= $f['tab'] ?>;
            var ajaxUrl = '<?= $this->adminLink(bff::$event . '&act='); ?>';

            $(function () {
                $progress = $('#WordsProgress');
                $block = $('#WordsListBlock');
                $list = $block.find('#WordsList');
                $listTable = $block.find('#WordsListTable');
                $listPgn = $block.find('#WordsListPgn');
                filters = $block.find('#WordsListFilters').get(0);

                $list.delegate('a.word-edit', 'click', function () {
                    var id = intval($(this).data('id'));
                    if (id > 0) jWordsFormManager.action('edit', id);
                    return false;
                });

                $list.delegate('a.word-toggle', 'click', function () {
                    var id = intval($(this).data('id'));
                    var type = $(this).data('type');
                    if (id > 0) {
                        var params = {progress: $progress, link: this};
                        bff.ajaxToggle(id, ajaxUrl + 'toggle&type=' + type + '&id=' + id, params);
                    }
                    return false;
                });

                $list.delegate('a.word-del', 'click', function () {
                    var id = intval($(this).data('id'));
                    var selected = intval($(this).data('selected'));
                    if (id > 0) del(id, selected, this);
                    return false;
                });

                $(window).bind('popstate', function (e) {
                    if ('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec(loc.search.toString());
                    if (actForm != null) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jWordsFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jWordsFormManager.action('cancel');
                        updateList(false);
                    }
                });

            });

            function isProcessing() {
                return processing;
            }

            function del(id, selected, link) {
                if(Boolean(selected)){
                    var msg = '<?=_t('','Внимание, данная страна уже выбрана у бренда(ов)! Удалить?');?>'
                }else{
                    var msg = '<?=_t('','Удалить?');?>'
                }
                bff.ajaxDelete(msg, id, ajaxUrl + 'delete&id=' + id, link, {
                    progress: $progress,
                    repaint: false
                });
                return false;
            }

            function updateList(updateUrl) {
                if (isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function (data) {
                    if (data) {
                        $list.html(data.list);
                        $listPgn.html(data.pgn);
                        if (updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                    }
                }, function (p) {
                    $progress.toggle();
                    processing = p;
                    $list.toggleClass('disabled');
                });
            }

            function setPage(id) {
                filters.page.value = intval(id);
            }

            return {
                submit: function (resetForm) {
                    if (isProcessing()) return false;
                    setPage(1);
                    if (resetForm) {
                        filters['title'].value = '';
                        //
                    }
                    updateList();
                },
                page: function (id) {
                    if (isProcessing()) return false;
                    setPage(id);
                    updateList();
                },
                onTab: function (tabNew, link) {
                    if (isProcessing() || tabNew == tab) return false;
                    setPage(1);
                    tab = filters.tab.value = tabNew;
                    updateList();
                    $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
                    return false;
                },
                refresh: function (resetPage, updateUrl) {
                    if (resetPage) setPage(0);
                    updateList(updateUrl);
                },
                toggle: function (show) {
                    if (show === true) {
                        $block.show();
                        if (bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                }
            };
        }());

    <?js::stop()?>
</script>