<? if (!empty($list)): ?>
    <? foreach ($list as $item): ?>
        <? if (isset($item['title']) && isset($item['url_view'])): # список заказов?>
            <li>
                <a href="<?= $item['url_view'] ?>">
                    <?= $item['title'] ?>
                </a>
            </li>
        <? endif; ?>
        <? if (isset($item['user_link']) && isset($item['name'])): # список исполнителей?>
            <li>
                <a href="<?= $item['user_link'] ?>">
                    <?= $item['name'] ?>
                    <?= $item['surname'] ?>
                    <?= '[' . $item['login_title'] . ']' ?>
                </a>
            </li>
        <? endif; ?>
        <? if (isset($item['spec_link']) && isset($item['title'])): # список  спец-й?>
            <li>
                <a href="<?= $item['spec_link'] ?>">
                    <?= $item['title'] ?>
                </a>
            </li>
        <? endif; ?>
        <? if (isset($item['cat_link']) && isset($item['title'])): # список кат-й?>
            <li>
                <a href="<?= $item['cat_link'] ?>">
                    <?= $item['title'] ?>
                </a>
            </li>
        <? endif; ?>
        <? if (isset($item['tag_link']) && isset($item['tag'])): # список кат-й?>
            <li>
                <a href="<?= $item['tag_link'] ?>">
                    <?= $item['tag'] ?>
                </a>
            </li>
        <? endif; ?>
    <? endforeach; ?>
<? else: ?>
    <li>
        <?= _t('', 'Результатов не обнаружено') ?>
    </li>
<? endif; ?>
