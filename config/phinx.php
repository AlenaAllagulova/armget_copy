<?php

/**
 * Конфигурация для phinx
 * @see http://docs.phinx.org/en/latest/configuration.html
 */

require_once __DIR__.'/../bff.php';

$config = config::sys([], [], 'db');
$configDB = [
    'adapter' => $config['db.type'],
    'host'    => $config['db.host'],
    'name'    => $config['db.name'],
    'user'    => $config['db.user'],
    'pass'    => $config['db.pass'],
    'port'    => $config['db.port'],
    'charset' => $config['db.charset'],
];
$paths = bff::i()->migrationsPaths();

return [
    'paths' => $paths,
    'templates' => [
        'file' => PATH_CORE.'db/migrations/migration.template.php.dist',
    ],
    'environments' => [
        'default_migration_table' => TABLE_MIGRATIONS,
        'default_database'        => $config['db.name'],
        'production'              => $configDB,
        'development'             => $configDB,
    ]
];