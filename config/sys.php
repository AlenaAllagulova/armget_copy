<?php
/**
 * Системные настройки
 */

$config = array(
    'site.host'   => 'armget.ru',
    'site.static' => '//armget.ru',
    'site.title'  => 'ArmGet-фриланс (freelance) биржа, удалённая работа, вакансии', // название сайта, для подобных случаев: "Я уже зарегистрирован на {Ru.armget.com}"
    /**
     * Доступ к базе данных
     */
    'db.type' => 'mysql', // варианты: pgsql, mysql
    'db.host' => 'localhost', // варианты: localhost, ...
    'db.port' => '3306', // варианты: pgsql - 5432, mysql - 3306
    'db.name' => 'armget',
    'db.user' => 'armget',
    'db.pass' => 'm39T3cS~P6w8iC',
    'db.charset' => 'UTF8', //utf8mb4
    'db.prefix' => 'bff_',
    /**
     * Mail
     */
    'mail.support'  => 'support@armget.ru',
    'mail.noreply'  => 'noreply@armget.ru',
    'mail.admin'    => 'admin@armget.ru',
    'mail.fromname' => 'armget.ru',
    'mail.method'   => 'mail', // варианты: mail, sendmail, smtp
    'mail.smtp' => array(
        'host'=>'localhost',
        'port'=>25,
        'user'=>'',
        'pass'=>'',
        'secure'=>'', // варианты: '', 'tls', 'ssl'
    ),
    /**
     * Локализация
     */
     'locale.available' => array( // список языков (используемых на сайте)
        // ключ языка => название языка
        'ru' => 'Русский',
        //'uk' => 'Українська',
     ),
     'locale.default' => 'ru', // язык по-умолчанию
     'locale.hidden' => array( // языки скрытые от пользователей сайта
        //'en',
     ),
    /**
     * Настройки систем оплаты (bills)
     * Полный список доступных настроек указан в BillsModuleBase::init методе
     */
    'services.enabled' => true, // платные услуги (true - включены, false - выключены)
    'bills.w1.id' => '146231943965',
    'bills.w1.currency' => 643,
	'bills.robox.test' => false, // тестовый режим (true|false)
    'bills.robox.login' => 'ArmGet.ru', // Идентификатор магазина
    'bills.robox.pass1' => 'quZqnj7SW8Gno4tTk54A', // Пароль #1
    'bills.robox.pass2' => 'a1v1H1bOtezw2vw3EMiO', // Пароль #2
    'bills.paypal.button_id' => '', // ID кнопки оплаты PayPal, например 'UMTZ9XW8PDPE8'
    'bills.liqpay.public_key' => '', // Публичный ключ, например 'i539429889'
    'bills.liqpay.private_key' => '', // Приватный ключ, например 'W2UHWJPWpOWrqUJH9TEJK31zrTxj5jzCkwuALfw9'
    'bills.liqpay.currency' => 'UAH', // Валюта платежа, возможные значения: 'UAH', 'RUB', 'USD', 'EUR'
    'bills.yandex.money.purse'  => '', // Номер кошелька, например '411015109864170'
    'bills.yandex.money.secret' => '', // Секрет, например 'Yb4tKMTfj7Qe35qPEz5E4IoH'
	'bills.wm.wmr'        => 'R410188478193', // идентификатор R кошелька получателя
    'bills.wm.wmr_secret' => '2DDE242B-C3A5-411E-9231-1AF84A1356BB', // секретный ключ R кошелька получателя
    /**
     * Sphinx (если используется)
     */
    'sphinx.enabled' => true, // варианты: true, false
    'sphinx.host'    => '127.0.0.1',
    'sphinx.port'    => 9306,
    'sphinx.version'  => '2.2.11',
    # Заказы
    'orders.premoderation'         => true,  // Использовать премодерацию заказов, false - постмодерация
    'orders.search.one.spec'       => false, // Поиск заказов по одной специализации
    'orders.map.enabled'           => true,  // Использовать привязку к карте в заказах
    'orders.use.products'          => true,  // Использовать товары из магазина
    'orders.images.limit'          => 8,     // Максимально допустимое кол-во фотографий у заказа, 0 - нет фотографий
    'orders.attachments.limit'     => 4,     // Максимально допустимое кол-во файлов у заказа, 0 - нет файлов
    'orders.offer.examples.limit'  => 6,     // Максимально допустимое кол-во примеров работ в заявке, 0 - нет примеров
    'orders.offer.spec.check'      => false, // Проверка соответствия специализации исполнителя при добавлении заявки к заказу
    'orders.delete.allow'          => true,  // Разрешать удалять заказ пользователю (true / false)
    'orders.title.limit'           => 200,   // Максимально допустимое кол-во символов в заголовке заказа (1-200)
    'orders.search.closed'         => true,  // Отображать в списках снятые с публикации (закрытые) заказы
    'orders.search.tags.limit'     => 0,     // Склейка навыков в списке заказов (0 - отображать все указанные в настройках заказа)
    'orders.rss.enabled'           => true,  // Использовать RSS-ленту заказов
    'orders.opinions'              => true,  // Привязка отзывов к заказам, расширенный порядок подтверждения сотрудничества исполнителем
    'orders.invites'               => true,  // Предложение работы исполнителю в качестве индивидуального заказа
    'orders.invites.full.form'     => true,  // Предложение заказа исполнителю в расширенном виде (true / false)
    'orders.terms'                 => true,  // Возможность указать срок приема заявок при размещении/редактировании заказа
    'orders.dynprops'              => false, // Использовать отдельный набор динамических свойств для заказов, по-умолчанию общие с исполнителями(false)
    # Безопасная сделка
    'fairplay.enabled'             => false, // Используется
    'fairplay.yandex.money.client_id' => '', // Яндекс.Деньги: ID, например: 2BCCBD2590F543160A2674D0709FA760B553ADC13BC74B269A2C543F3CFCE5A1
    'fairplay.yandex.money.client_secret' => '', // Яндекс.Деньги: Secret, например: B60F171ECA07F6B875B58A32D0D620E613EF0E59322F9E24B90C12216AD8AF38BA81E290AC0EBCAAFC54A59E117C22463FE6446BEAF4CDF109C588856F97DDB3
    'fairplay.verified.files.limit' => 5, // Максимально допустимое кол-во файлов прилагаемых к финансовым данным
    'fairplay.verified.files.expire' => 0, // Срок хранения файлов прилагаемых к финансовым данным в днях, 0 - без ограничений
    'orders.fairplay.only'          => true, // При добавлении заказов использовать только тип оплаты "Безопасная сделка" (true / false)
    'fairplay.hold.min.prepay'      => true, // При списании сдедств удерживать мин предоплату для биржи (true / false)
    'order.add.slider.price'       => false, // варинаты:true|false  - включает/выключает слайдер в форме добавления заказа https://clazion.myjetbrains.com/youtrack/issue/ACC-29
    # Пользователи
    'users.register.phone'         => false, // Запрашивать номер телефона пользователя при регистрации (варианты: true|false)
    'users.register.phone.contacts' => false, // Отображать номер телефона указанный при регистрации в контактах профиля первым (варианты: true|false)
    'users.register.social.email.activation' => true, // Отправлять письмо со ссылкой активации при авторизации через соц. сеть (варианты: true|false)
    'users.contacts.replace.catched.words' => false, // Заменять контакты и др запрещенные слова в переписке при отсутствиии зарезервированных средств по заказу (варианты: true|false)
    'users.contacts.view.prepay'   => true,  // Показывать контакты только пользователям с наличием зарезервированных средств по заказу, false - стандартное поведение (варианты: true|false)
    # Настройки SMS:
    'users.sms.provider'           => 'sms_ru', // Доступные sms провайдеры: 'sms_ru', 'atompark_com'
    'users.sms.retry.limit'        => 3,     // Кол-во допустимых повторных отправок sms
    'users.sms.retry.timeout'      => 3,     // Кол-во минут ожидания при достижении максимально допустимых повторных отправок
    # -- провайдер sms.ru:
    'users.sms.sms_ru.api_id'      => '',    // Уникальный ключ (api_id), например: 4ac0c9c0-25xx-77f4-ed29-1519e8719180
    'users.sms.sms_ru.from'        => '',    // Имя отправителя: http://sms.ru/?panel=mass&subpanel=senders, «Имя отправителя» необходимо предварительно согласовать с администрацией sms.ru
    'users.sms.sms_ru.test'        => false, // Тестовая отправка: (варианты: true|false)
    # -- провайдер atompark.com:
    'users.sms.atompark_com.username' => '', // логин пользователя в системе SMS Sender
    'users.sms.atompark_com.password' => '', // пароль пользователя в системе SMS Sender
    'users.sms.atompark_com.sender' => 'SMS', // отправитель смс, 14 цифровых символов или 11 цифробуквенных (английские буквы и цифры)

    'users.activation.days'        => 7,     // Срок действия ссылки активации аккаунта (кол-во дней, 0 - ссылка действительна 1 год)
    'users.activation.days.notify' => 3,     // Кол-во дней до окончания действия ссылки активации, за которое отправлять уведомление пользователю (0 - не отправлять уведомление)
    'users.search.tags.limit'      => 0,     // Склейка навыков в списке исполнителей (0 - отображать все указанные исполнителем)
    'users.profile.phone.required' => false, // Требовать обязательный ввод телефона
    'users.profile.map'            => true,  // Использовать привязку к карте для пользователей
    'users.profile.sex'            => true,  // Использовать поле "пол" в профиле
    'users.profile.birthdate'      => true,  // Использовать поле "дата рождения" в профиле
    'users.roles'                  => true,  // Деление на компанию/частное лицо
    'users.verified'               => true,  // Верификация пользователей
    'users.verified.files.expire'  => 0,     // Срок хранения изображений документов верификации на сервере в днях (0 - не удаляются)
    'users.trusted.pro'            => false, // Являются ли пользователи с активированным аккаунтом PRO "доверенными" (действия попадают на постмодерацию)
    'users.account.join'           => true,  // Разрешать связывание аккаунтов исполнителя и заказчика
    'opinions.premoderation'       => false, // Использовать премодерацию отзывов, false - постмодерация
    'opinions.register.timeout'    => 0,     // Задержка возможности публикации отзыва (в днях) после регистрации (0 - доступно сразу после регистрации)
    # Пользователи - рейтинг:
    'users.rating.login'           => 1,     // Рейтинг "за заход в день" (0 - заход не влияет на рейтинг)
    'users.rating.order.performer' => 0,     // Рейтинг за выбор исполнителем в заказах (0 - не влияет на рейтинг)
    'users.rating.opinions.positive' => 10,  // Рейтинг за положительный отзыв (0 - не влияет на рейтинг)
    'users.rating.opinions.negative' => -10, // Рейтинг за отрицательный отзыв (0 - не влияет на рейтинг)
    'users.rating.portfolio'       => 10,    // Рейтинг за работу в портфолио (0 - не влияет на рейтинг)
    'users.rating.portfolio.limit' => 50,    // Максимальное кол-во работ в портфолио, влияющих на рейтинг
    'users.rating.qa.best'         => 10,    // Рейтинг за лучший ответ
    'users.rating.multiplier'      => 1.2,   // Множитель рейтинга для PRO-аккаунта
    'users.rating.money'           => '0=0', // Рейтинг за потраченные деньги на платные услуги, Деньги=Рейтинг (0=0 - не влияет на рейтинг)
    # Магазин
    'shop.premoderation'           => true,  // Использовать премодерацию товаров, false - постмодерация
    'shop.images.limit'            => 8,     // Максимально допустимое кол-во фотографий, 0 - нет фотографий
    'shop.search.tags.limit'       => 0,     // Склейка тегов в списке товаров магазина (0 - отображать все указанные пользователем)
    # Новости
    'news.search.pagesize'         => 10,    // Кол-во новостей в списке на страницу
    'news.search.tags.limit'       => 0,     // Склейка тегов в списке новостей (0 - отображать все указанные)
    'news.comments'                => false, // Комментирование новостей (true / false)
    'news.comments.premoderation'  => false, // Премодерация комментриев (true - премодерация, false - постмодерация)
    # Статьи
    'articles.enabled'             => true,  // Использовать раздел "Статьи", false - скрыть
    'articles.premoderation'       => true,  // Использовать премодерацию статей, false - постмодерация
    'articles.publicator'          => true,  // Использовать публикатор для создания/редактирования статей
    'articles.tags.limit.index'    => 30,    // Максимальное кол-во отображаемых тегов в блоке слева на главной странице раздела "Статьи"
    # Блог
    'blog.enabled'                 => true,  // Использовать раздел "Блоги", false - скрыть
    'blog.premoderation'           => true,  // Использовать премодерацию постов, false - постмодерация
    'blog.publicator'              => true,  // Использовать публикатор для создания/редактирования постов
    'blog.tags.on'                 => true,  // Использовать теги
    'blog.images.limit'            => 8,     // Максимально допустимое кол-во фотографий, 0 - нет фотографий
    # Портфолио
    'portfolio.premoderation'      => false, // Использовать премодерацию работ, false - постмодерация
    'portfolio.images.limit'       => 8,     // Максимально допустимое кол-во фотографий, 0 - нет фотографий
    'portfolio.preview.pro'        => false, // Отображение превью работ доступно только для PRO
    # Вопрос-Ответ
    'qa.enabled'                   => true,  // Использовать раздел "Ответы", false - скрыть
    'qa.premoderation'             => true,  // Использовать премодерацию вопросов, false - постмодерация
    'qa.best'                      => true,  // Блок "Лидеры раздела"
    'qa.best.limit'                => 5,     // Кол-во выводимых пользователей в блоке "Лидеры раздела"
    # Контакты
    'contacts.attachments.enabled' => true,  // Разрешать прикрепление файлов
    # Geo
    'geo.default.country'          => 1000,  // Страна по-умолчанию (ID страны)
    'geo.country.select'           => true,  // Использовать страны
    'geo.maps.type'                => 'yandex', // Тип карт 'google', 'yandex'
    'geo.maps.googleKey'           => 'AIzaSyAxXricMo8gASd2QZcGxKjsv3k1eQ3tzMA', // API ключ для Google Карт
    'geo.maps.googleKey.static'    => 'AIzaSyCNBUIOoQMbA7JiBHuS9zjW1JZJlIpzOoI', // API ключ для Static Google Карт
    'geo.filter'                   => true,  // Общий фильтр по региону в шапке сайте, false - выключен
    'geo.filter.url'               => 0, // Отображение региона в URL: 0 - не отображается, 1 - поддиректория /city/, 2 - поддомен city.ru.armget.com
    'geo.ip.location'              => false, // Выполнять определение региона по IP (при включенном общем фильтре)
    'geo.ip.location.provider'     => 'ipgeobase.ru', // Провайдер базы IP адресов
    /**
     * Дополнительные настройки
     * ! Настоятельно не рекомендуется изменять после запуска проекта
     */
	'format.price'				   => '1',
    'date.timezone'                => 'Europe/Moscow', // часовой пояс
    'cookie.prefix'                => 'bff_', // cookie префикс
    'currency.default'             => 2, // основная валюта (ID)
    'users.use.client'             => true,  // Использовать тип пользователей "заказчик" (true / false)
    'specializations.cats.on'      => true,  // Использовать категории для "Специализаций" (true / false)
    'specializations.use.services' => true,  // Использовать услуги "Специализаций" (true / false)
    'specializations.search'       => true,  // Использовать поле поиска при выборе специализации
    'specializations.search.limit' => 10,    // Количество совпавших вариантов результатов для поиска специализаций
    'specializations.register'     => true,  // Выбор специализации при регистрации исполнителя
    'shop.enabled'                 => true,  // Использовать раздел "Магазин" (true / false)
    'seo.landing.pages.enabled'    => true,
    'seo.landing.pages.fields'     => array(
        'seotext' => array(
            't'=>'SEO текст',
            'type'=>'wy',
        ),
    ),
    'seo.redirects'                => true, // Задействовать редиректы (варианты: true|false)
    'config.sys.admin'             => true,  // Возможность редактирования большей части системных настроек через админ. панель в "режиме разработчика"
    'site.static.minify'           => false, // Минимизация js/css (варианты: true|false)
    /**
     * Debug
     */
    'php.errors.reporting'         => -1, // all
    'php.errors.display'           => 1, // отображать ошибки (варианты: 1|0)
    'localhost'                    => false, // localhost (варианты: true|false)
    'debug'                        => false, // варинаты:true|false  - включить debug-режим, а также разрешать FORDEV-режим
);


# Переопределение настроек для Dev-версии:
if( ! empty($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] === 'example.dev' )
{
    $config = array_merge($config, array(
        'site.host' => 'example.dev',
        'site.static' => '//example.dev',
        # db
        'db.host' => 'localhost',
        'db.port' => '3306',
        'db.name' => 'armget_com',
        'db.user' => 'armget_com',
        'db.pass' => 'm39T3cS~P6w8iC',
        # debug
        'php.errors.reporting' => -1, //all
        'php.errors.display' => 1,
        'debug' => true,
    ));
}

if (file_exists(PATH_BASE.'config'.DIRECTORY_SEPARATOR.'sys-local.php')) {
    $local = include 'sys-local.php';
    return array_merge($config, $local);
}


return $config;
