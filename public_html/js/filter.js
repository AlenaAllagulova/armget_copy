var jFilterRegion = (function(){
    var inited = false, o = {lang:{}};
    var $block, popup, cookie, cityAC = false;
    var $country, $region, $city, $cityNoReg, $cityAC, $cityVal;

    function init()
    {
        cookie = app.cookiePrefix+'geo';
        o.changeLocation = intval(o.changeLocation);
        geoIpInit();
        $block = $('#j-geo-region-dropdown');

        popup = app.popup('geo-filter', $block, '#j-geo-region-select', {onHide:function(){
            var current = bff.cookie(cookie);
            if( ! current && current !== 0){
                bff.cookie(cookie, 0, {expires: 100, path: '/', domain: '.' + app.host});
            }
            $block.fadeOut(300);
        }, onShow:function($popup){
            if(intval(o.cityNoRegion)){
                $popup.fadeIn(100, function(){
                    initCityAC();
                });
            }else{
                $popup.fadeIn(100);
            }
        }});

        $country = $block.find('.j-select-country');
        $region = $block.find('.j-select-region');
        $city = $block.find('.j-select-city');
        $cityNoReg = $block.find('.j-select-city-noregion');
        $cityAC = $cityNoReg.find('.j-city-ac');
        $cityVal = $cityNoReg.find('.j-city-value');

        $country.on('click', '.j-select', function(e){
            e.preventDefault();
            var $el = $(this);
            var reg = intval($el.data('id'));
            if( ! reg) return;
            var noreg = intval($el.data('noregions'));
            if(noreg){
                var $regs = $cityNoReg.find('.j-regions-block');
                $regs.addClass('displaynone');
                $regs = $regs.filter('[data-id="' + reg + '"]');
                if ($regs.length) {
                    $regs.removeClass('displaynone');
                    $country.addClass('displaynone');
                    $cityNoReg.removeClass('displaynone');
                    cityAC.setParam('country', reg);
                } else {
                    bff.ajax(bff.ajaxURL('geo', 'filter-city-noregions'), {id: reg}, function (data) {
                        if (data && data.html) {
                            $cityNoReg.find('.j-list').append(data.html);
                            $country.addClass('displaynone');
                            $cityNoReg.removeClass('displaynone');
                            initCityAC();
                            cityAC.setParam('country', reg);
                        }
                    });
                }
                $cityNoReg.find('.j-all').data('id', reg);
            }else {
                var $regs = $region.find('.j-regions-block');
                $regs.addClass('displaynone');
                $regs = $regs.filter('[data-id="' + reg + '"]');
                if ($regs.length) {
                    $regs.removeClass('displaynone');
                    $country.addClass('displaynone');
                    $region.removeClass('displaynone');
                } else {
                    bff.ajax(bff.ajaxURL('geo', 'filter-region'), {id: reg}, function (data) {
                        if (data && data.html) {
                            $region.find('.j-list').append(data.html);
                            $country.addClass('displaynone');
                            $region.removeClass('displaynone');
                        }
                    });
                }
                $region.find('.j-all').data('id', reg);
            }
        });

        $region.on('click', '.j-select', function(e){
            e.preventDefault();
            var $el = $(this);
            var reg = intval($el.data('id'));
            if( ! reg) return;
            var $regs = $city.find('.j-regions-block');
            $regs.addClass('displaynone');
            $regs = $regs.filter('[data-id="'+reg+'"]');
            if($regs.length){
                $regs.removeClass('displaynone');
                $region.addClass('displaynone');
                $city.removeClass('displaynone');
            }else {
                bff.ajax(bff.ajaxURL('geo', 'filter-city'), {id: reg}, function (data) {
                    if (data && data.html) {
                        $city.find('.j-list').append(data.html);
                        $region.addClass('displaynone');
                        $city.removeClass('displaynone');
                    }
                });
            }
            $city.find('.j-region-title').text($el.text());
            $city.find('.j-all').data('id', reg);
        });

        $city.on('click', '.j-select', function(e){
            e.preventDefault();
            var $el = $(this);
            var reg = intval($el.data('id'));
            if( ! reg) return;
            setRegion(reg);
            popup.hide();
        });

        $region.on('click', '.j-change', function(e){
            e.preventDefault();
            $country.removeClass('displaynone');
            $region.addClass('displaynone');
        });

        $cityNoReg.on('click', '.j-change', function(e){
            e.preventDefault();
            $country.removeClass('displaynone');
            $cityNoReg.addClass('displaynone');
        });

        $cityNoReg.on('click', '.j-select', function(e){
            e.preventDefault();
            var $el = $(this);
            var reg = intval($el.data('id'));
            if( ! reg) return;
            setRegion(reg);
            popup.hide();
        });

        $city.on('click', '.j-change', function(e){
            e.preventDefault();
            $region.removeClass('displaynone');
            $city.addClass('displaynone');
        });

        $block.on('click', '.j-all', function(e){
            e.preventDefault();
            var reg = intval($(this).data('id'));
            setRegion(reg);
            popup.hide();
        });

        var $q = $region.find('.j-search-q');
        var $search = $region.find('.j-search');
        $search.click(function(e){
            e.preventDefault();
            var q = $q.val().toLowerCase();
            var $regs = $region.find('.j-regions-block:not(.displaynone)');
            $regs.find('li').show();
            $regs.find('div > ul').parent().show();
            $regs.find('ul > li > ul > li').css('padding-top', '');
            if( q == '' ) return false;
            $regs.find('.j-select').each(function(){
                var t = $(this).text();
                if(t.toLowerCase().indexOf(q) == -1) $(this).closest('li').hide();
            });
            $regs.find('div > ul > li').each(function(){
                var $el = $(this);
                if( ! $el.find('ul > li:visible').length){
                    $el.hide();
                }else{
                    $el.find('ul > li:visible:first').css('padding-top', '0');
                }
            });
            $regs.find('div > ul').each(function(){
                var $el = $(this);
                if( ! $el.children().filter(':visible').length) {
                    $el.parent().hide();
                }
            });
        });

        $q.keyup(function(){
            $search.click();
        })
    }

    function geoIpInit()
    {
        var $geoip = $('#j-geo-region-geoip-confirm');
        if ($geoip.length) {
            var geoip = app.popup('geoip', $geoip);
            geoip.show();

            $geoip.on('click', '.j-confirm', function(e){
                e.preventDefault();
                var $el = $(this);
                var reg = intval($el.data('id'));
                if(reg){
                    setRegion(reg);
                }
                geoip.hide();
            });

            $geoip.on('click', '.j-cancel', function(e){
                nothing(e);
                geoip.hide();
                popup.show();
            });
        }
    }

    function setRegion(reg)
    {
        function set(r) 
        {
            bff.cookie(cookie, r, {expires: 100, path: '/', domain: '.' + app.host});
            if(o.changeLocation){
                bff.ajax(bff.ajaxURL('geo', 'filter-location'), {id: r, loc:location.href}, function (data) {
                    if (data && data.location) {
                        location.href = data.location;
                    }
                });
            }else{
                location.reload();
            }
        }
        reg = intval(reg);
        var current = bff.cookie(cookie);
        if( ! current && current !== 0){
            set(reg);
        }else{
            current = intval(current);
            if(current != reg) {
                set(reg);
            }
        }
    }

    function initCityAC()
    {
        if(cityAC) return;
        $cityAC.autocomplete(bff.ajaxURL('geo', 'region-suggest'), {
            valueInput: $cityVal, params: {country: intval(o.country)},
            onSelect: function (cityID, cityTitle, ex) {
                if(intval(cityID)) {
                    setRegion(cityID);
                    popup.hide();
                }
            },
            doPrepareText: function (html) {
                var regionTitlePos = html.toLowerCase().indexOf('<br');
                if (regionTitlePos != -1) {
                    html = html.substr(0, regionTitlePos);
                }
                html = html.replace(/<\/?[^>]+>/gi, '');
                return $.trim(html);
            }
        }, function () {
            cityAC = this;
        });
    }


    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());
