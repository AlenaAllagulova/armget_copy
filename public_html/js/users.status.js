var jUsersWorkerStatus = (function(){
    var inited = false, o = {lang:{}};
    var $block;

    function init()
    {
        $block = $('#j-user-status');
        var $blf = $block.find('#j-user-status-form-block');
        var $blshow = $block.find('#j-user-status-show');
        var $label = $block.find('span.label');

        var $title = $block.find('#j-user-status-title');
        $block.on('click', '.j-status', function(){
            var $el = $(this);
            $el.closest('.dropdown').removeClass('open');
            bff.ajax(bff.ajaxURL('users', 'status'), {status:$el.data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $el.parent().addClass('active').siblings().removeClass('active');
                    if(resp.title){
                        $title.text(resp.title);
                    }
                    if(resp.class){
                        $label.removeClass('label-busy label-success label-danger label-warning');
                        $label.addClass(resp.class);
                    }
                } else {
                    app.alert.error(errors);
                }
            });

            return false;
        });

        var form = app.form($blf.find('form'), function($f){
            var f = this;
            f.ajax(bff.ajaxURL('users', 'status-text'),{},function(resp, errors){
                if(resp && resp.success) {
                    if(resp.hasOwnProperty('status_text')){
                        $blshow.find('.j-text').html(resp.status_text);
                    }
                    $blf.hide();
                    $blshow.show();
                } else {
                    f.alertError(errors);
                }
            });
        });

        $blf.on('click', '.j-cancel', function(){
            $blf.hide();
            $blshow.show();
            return false;
        });

        $block.on('click', '.j-edit', function(){
            $blshow.hide();
            $blf.show();
            return false;
        });

        bff.maxlength(form.$field('status_text'), {limit:300, message:$('#j-status-message'), lang:{left: o.lang.left, symbols:o.lang.symbols}});
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());