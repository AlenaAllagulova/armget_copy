var jUserAuth = (function(){
    var inited = false, url = document.location.pathname, o = {lang:{}};

    function init(options, callback)
    {
         if(inited) return; inited = true;
         o = $.extend(o, options || {});
         $(function(){
             callback();
         });
    }

    function initLoginSocialButtons()
    {
        var url_login = o.login_social_url,
            url_return = o.login_social_return,
            popup;

        function socialPopup(o)
        {
            o = $.extend({w: 450, h: 380}, o || {});
            if(popup !== undefined) popup.close();
            var centerWidth = ($(window).width() - o.w) / 2;
            var centerHeight = ($(window).height() - o.h) / 2;
            popup = window.open(url_login+ o.provider+'?ret='+encodeURIComponent(url_return), "u_login_social_popup", "width=" + o.w + ",height=" + o.h + ",left=" + centerWidth + ",top=" + centerHeight + ",resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes");
            popup.focus();
            return false;
        }
        $('.j-u-login-social-btn').on('click', function(e){ nothing(e);
            var meta = $(this).metadata();
            if( meta && meta.hasOwnProperty('provider') ) {
                socialPopup(meta);
            }
        });
    }

    return {
        login: function(options)
        {
            o.login_social_url = '/';
            o.login_social_return = '/';
            init(options, function()
            {
                var f = app.form('#j-u-login-form', function($f){
                    if( ! f.checkRequired() ) return;
                    if( ! f.fieldStr('pass').length ) {
                        return f.fieldError('pass', o.lang.pass);
                    }
                    f.ajax(url, {}, function(data, errors){
                        if(data && data.success) {
                            bff.redirect(data.redirect);
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                });
                initLoginSocialButtons();
                app.$B.on('click', '.j-resend-activation', function(e){ nothing(e);
                    if( ! bff.isEmail( f.fieldStr('email') ) ) {
                        f.fieldError('email', o.lang.email); return;
                    }
                    f.ajax(url, {step:'resend-activation'}, function(data, errors){
                        if(data && data.success) {
                            bff.redirect(data.redirect);
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                });
            });
        },
        register: function(options)
        {
            o.embedded = false;
            o.captcha = false;
            o.agreement = false;
            o.pass_confirm = false;
            o.login_social_url = '/';
            o.login_social_return = '/';
            o.phone = false;

            init(options, function()
            {
                var $form;
                if(o.embedded){
                    $form = $(o.embedded_form);
                }else {
                    $form = $('#j-u-register-form');
                    var f = app.form($form, function ($f) {
                        if (!jUserAuth.registerValidate(f)) return;
                        f.ajax(url, {}, function (data, errors) {
                            if (data && data.success) {
                                bff.redirect(data.redirect);
                            } else {
                                if (o.captcha && data.captcha) {
                                    $f.find('.j-captcha').triggerHandler('click');
                                }
                                f.fieldsError(data.fields, errors);
                            }
                        });
                    });
                    initLoginSocialButtons();
                }
                if (o.phone) {
                    app.user.phoneInput($form.find('.j-phone-number'));
                }

                var $name = $form.find('.j-name');
                if($name.length) {
                    $form.on('change', '[name="role_id"]', function () {
                        var $el = $(this);
                        var k = 'r' + $el.val();
                        $name.text(o.lang.name[k].n);
                        $form.find('[name="name"]').attr('placeholder', o.lang.name[k].p);
                    });
                }
            });
        },
        registerValidate: function(f)
        {
            if( ! f.checkRequired() ) return false;
            if( ! bff.isEmail( f.fieldStr('email') ) ) {
                f.fieldError('email', o.lang.email);
                return false;
            }
            if(f.$field('pass').length) {
                if (!f.fieldStr('pass').length) {
                    f.fieldError('pass', o.lang.pass);
                    return false;
                }
                if (o.pass_confirm) {
                    var pass2 = f.fieldStr('pass2');
                    if (!pass2.length || pass2 != f.fieldStr('pass')) {
                        f.fieldError('pass2', o.lang.pass2);
                        return false;
                    }
                }
            }
            if(o.phone){
                if (f.fieldStr('phone').length < 6) {
                    f.fieldError('phone', o.lang.phone);
                    return false;
                }
            }else {
                if ($('#j-u-register-phone').length) {
                    if (f.fieldStr('phone').length < 6) {
                        f.fieldError('phone', o.lang.phone);
                        return false;
                    }
                }
            }
            if( o.captcha ) {
                if( ! f.fieldStr('captcha').length ) {
                    f.fieldError('captcha', o.lang.captcha);
                    return false;
                }
            }
            if(f.$field('type').length) {
                if (!f.$field('type').is(':checked')) {
                    f.alertError(o.lang.type, {title: app.lang.form_alert_errors});
                    return false;
                }
            }
            if(o.agreement) {
                if (!f.$field('agreement').is(':checked')) {
                    f.fieldError('agreement', o.lang.agreement);
                    return false;
                }
            }
            return true;
        },
        registerEmailed: function(options)
        {
            init(options, function()
            {
                var $retry = $('#j-u-register-emailed-retry'), retry_process = false;
                if( $retry.length ) {
                    $retry.on('click', 'a', function(e){ nothing(e);
                        if(retry_process) return;
                        bff.ajax(url, {step:'emailed'}, function(data, errors){
                            if(data && data.success) {
                                app.alert.success(o.lang.success);
                                $retry.fadeOut();
                            } else {
                                app.alert.error(errors);
                            }
                        }, function(p){ retry_process = p; });
                    });
                }
            });
        },
        registerSocial: function(options)
        {
            o.phone = false;
            init(options, function()
            {
                var formStatus = 'register', f, $form;
                function setFormStatus(status)
                {
                    formStatus = status;
                    $('.j-social', $form).toggle();
                    $('.j-shortpage-title').html(o.lang[status].title);
                    $('#j-u-register-social-email', $form).prop({readonly:(status=='login')});
                    app.alert.hide();
                }
                f = app.form('#j-u-register-social-form', function($f){
                    if(formStatus == 'register') {
                        if( ! bff.isEmail( f.fieldStr('email') ) ) {
                            return f.fieldError('email', o.lang.register.email);
                        }
                        if( ! f.$field('type').is(':checked')){
                            return f.alertError(o.lang.register.type, {title:app.lang.form_alert_errors});
                        }
                        if( ! f.$field('agreement').is(':checked') ) {
                            return f.fieldError('agreement', o.lang.register.agreement);
                        }
                        f.ajax(url, {step:'social'}, function(data, errors){
                            console.log(data);
                            if(data && data.success) {
                                if(data.redirect){
                                    bff.redirect(data.redirect);
                                }
                                if(data.exists) {
                                    setFormStatus('login');
                                }
                                if(data.msg){
                                    app.alert.success(data.msg);
                                }
                            } else {
                                f.fieldsError(data.fields, errors);
                            }
                            if(data.confirm){
                                $form.find('.j-step2').removeClass('hidden');
                            }
                        });
                    } else {
                        if( ! f.fieldStr('pass').length ) {
                            return f.fieldError('pass', o.lang.login.pass);
                        }
                        f.ajax(o.login_url, {social:true}, function(data, errors){
                            if(data && data.success) {
                                bff.redirect(data.redirect);
                            } else {
                                f.fieldsError(data.fields, errors);
                            }
                        });
                    }
                });
                $form = f.getForm();
                if (o.phone) {
                    app.user.phoneInput($form.find('.j-phone-number'));
                    $form.on('click', '.j-phone-repeate', function(e){
                        e.preventDefault();
                        f.$field('code').val('');
                        $form.submit();
                    });
                }
                $('#j-u-register-social-email-change', $form).on('click', function(e){ nothing(e);
                    setFormStatus('register');
                });
            });
        },
        registerPhone: function(options)
        {
            init(options, function()
            {
                var $blockCode = $('#j-u-register-phone-block-code');
                var $blockPhone = $('#j-u-register-phone-block-phone');
                var $currentNumber = $('#j-u-register-phone-current-number');
                if(options.url){
                    url = options.url;
                }
                // phone inputs:
                if ($blockCode.length) {
                    // init inputs
                    app.user.phoneInput($blockPhone.find('#j-u-register-phone-input').closest('.j-phone-number'));
                    // change phone step1:
                    $('.j-u-register-phone-change-step1-btn').on('click', function(e){ nothing(e);
                        $blockCode.toggle(); if ($blockCode.is(':visible')) $blockCode.find('.j-u-register-phone-code-input:visible').focus();
                        $blockPhone.toggle(); if ($blockPhone.is(':visible')) $blockPhone.find('.j-phone-number-input:visible').focus();
                    });
                    // change phone step2:
                    $blockPhone.on('click', '.j-u-register-phone-change-step2-btn', function(e){ nothing(e);
                        var $btn = $(this); if ($btn.is(':disabled')) return;
                        var $phone = $blockPhone.find('.j-phone-number-input:visible');
                        if (!$phone.val().length) {
                            $phone.focus(); return;
                        }
                        bff.ajax(url, {step:'phone',act:'phone-change',phone:$phone.val()}, function(data, errors){
                            if(data && data.success) {
                                app.alert.success(o.lang.change_success);
                                $currentNumber.text(data.phone);
                                $blockCode.toggle().find('.j-u-register-phone-code-input:visible').focus();
                                $blockPhone.toggle();
                            } else {
                                app.alert.error(errors);
                            }
                        }, function(p) {
                            if(p) $btn.prop('disabled','disabled');
                            else  $btn.removeProp('disabled');
                        });
                    });
                }

                // validate code:
                $blockCode.on('click', '.j-u-register-phone-code-validate-btn', function(e){ nothing(e);
                    var $btn = $(this); if ($btn.is(':disabled')) return;
                    var $code = $blockCode.find('.j-u-register-phone-code-input:visible');
                    if (!$code.val().length) {
                        $code.focus(); return;
                    }
                    bff.ajax(url, {step:'phone',act:'code-validate',code:$code.val()}, function(data, errors){
                        if(data && data.success) {
                            bff.redirect(data.redirect);
                        } else {
                            app.alert.error(errors);
                        }
                    }, function(p) {
                        if(p) $btn.prop('disabled','disabled');
                        else  $btn.removeProp('disabled');
                    });
                });

                // resend code:
                $blockCode.on('click', '.j-u-register-phone-code-resend-btn', function(e){ nothing(e);
                    var $btn = $(this); if ($btn.is(':disabled')) return;
                    bff.ajax(url, {step:'phone',act:'code-resend'}, function(data, errors){
                        if(data && data.success) {
                            app.alert.success(o.lang.resend_success);
                        } else {
                            app.alert.error(errors);
                        }
                    }, function(p) {
                        if(p) $btn.prop('disabled','disabled');
                        else  $btn.removeProp('disabled');
                    });
                });
            });
        },
        forgotStart: function(options)
        {
            init(options, function()
            {
                app.form('#j-u-forgot-start-form', function($f){
                    var f = this;
                    if( ! f.checkRequired() ) return;
                    f.ajax(url, {}, function(data, errors){
                        if(data && data.success) {
                            f.alertSuccess(o.lang.success, {hide:false,reset:true});
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                });
            });
        },
        forgotFinish: function(options)
        {
            init(options, function()
            {
                app.form('#j-u-forgot-finish-form', function($f){
                    var f = this;
                    if( ! f.fieldStr('pass').length ) {
                        f.fieldError('pass', o.lang.pass); return;
                    }
                    f.ajax(url, {}, function(data, errors){
                        if(data && data.success) {
                            f.alertSuccess(o.lang.success, {hide:false,reset:true});
                            $f.find('button[type="submit"]').prop({disabled:true});
                            if(data.redirect){
                                bff.redirect(data.redirect);
                            }
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });

                });
            });
        }
    };
}());