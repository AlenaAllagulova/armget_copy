var jBlogSearch = (function(){
    var inited = false, o = {lang:{}}, listMngr;
    var $main, $block, $form, $list, $pgn, $search;

    function init()
    {
        $main = $('#j-blog-search');
        $block = $('#j-blog-search-list');
        $search = $('#j-blog-search-keyword-form');

        $form = $('#j-blog-search-form-block').find('form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $form.on('show.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.addClass('active');
            $el.find('i.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        });

        $form.on('hide.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.removeClass('active');
            $el.find('i.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        });

        $form.on('click', '.j-cat-title', function(){
            var $el = $(this);
            if (!intval($el.data('subs'))) return true;
            var $li = $el.closest('li');
            $li.toggleClass('opened');
            $li.find('ul').toggleClass('hidden');
            $el.find('.fa').toggleClass('fa-caret-right fa-caret-down');
            return false;
        });

        var $mobileCats = $form.find('#j-mobile-cats');
        $form.on('click', '.j-mobile-cat', function(){
            var $el = $(this);
            $form.find('#j-mobile-cat-'+$el.data('id')).collapse('show');
            $mobileCats.collapse('hide');
            return false;
        });

        $form.on('click', '.j-mobile-cat-back', function(){
            $(this).closest('.j-mobile-cat-block').collapse('hide');
            $mobileCats.collapse('show');
            return false;
        });

        $list.on('click', '.j-delete', function(){
            if(confirm(o.lang.post_delete)){
                bff.ajax(bff.ajaxURL('blog', 'post-delete'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        listMngr.submit({popstate:true}, false);
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $list.on('click', '.j-hide', function(){
            bff.ajax(bff.ajaxURL('blog', 'post-hide'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    listMngr.submit({popstate:true}, false);
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    $.scrollTo($list, {offset: -150, duration:500});
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(resp.hasOwnProperty('count')){
                    $main.find('.j-posts-count').text(resp.count);
                }

            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            ajax: o.ajax
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());