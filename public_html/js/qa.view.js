var jQaView = (function(){

    var inited = false, o = {lang:{}};
    var $block, $answers;

    function init()
    {
        $block = $('#j-qa-question-view');
        $answers = $block.find('#j-answers-block');

        var $addAns = $block.find('#j-add-answer');
        if($addAns.length){
            app.form($addAns.find('form'), function($f){
                var f = this;
                if( ! f.checkRequired({focus:true}) ) return;

                f.ajax(bff.ajaxURL('qa', 'answer-add'),{},function(resp, errors){
                    if(resp && resp.success) {
                        $answers.html(resp.answers);
                    } else {
                        f.alertError(errors);
                    }
                });
            }, {noEnterSubmit: true});
        }

        $answers.on('click', '.j-vote', function(e){ nothing(e);
            var $el = $(this), $bl = $el.closest('.j-vote-block');
            if ($bl.hasClass('q-answer-rating_na')) return false;
            bff.ajax(bff.ajaxURL('qa', 'answer-vote'), {id:$el.data('id'), t:$el.data('t'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.disabled){
                        $bl.addClass('q-answer-rating_na');
                    }
                    if(resp.sum){
                        var $v = $bl.find('.j-votes');
                        $v.html(resp.sum);
                        if(intval(resp.sum) < 0){
                            $v.removeClass('q-answer-good').addClass('q-answer-bad');
                        }else{
                            $v.removeClass('q-answer-bad').addClass('q-answer-good');
                        }
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        o.id = intval(o.id);
        if(o.owner){
            $block.on('click', '.j-close', function(){
                bff.ajax(bff.ajaxURL('qa', 'question-close'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        location.reload();
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });
            $block.on('click', '.j-open', function(){
                bff.ajax(bff.ajaxURL('qa', 'question-open'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        location.reload();
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });
            $block.on('click', '.j-solved', function(){
                bff.ajax(bff.ajaxURL('qa', 'question-solved'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        location.reload();
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });

            $block.on('click', '.j-delete', function(){
                if(confirm(o.lang.delete)){
                    bff.ajax(bff.ajaxURL('qa', 'question-delete'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                        if(resp && resp.success) {
                            if(resp.redirect) {
                                bff.redirect(resp.redirect);
                            }
                        } else {
                            app.alert.error(errors);
                        }
                    });

                }
                return false;
            });

            $block.on('click', '.j-best', function(){
                var $el = $(this);
                bff.ajax(bff.ajaxURL('qa', 'answer-best'), {question_id: o.id, answer_id: $el.data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        location.reload();
                    } else {
                        app.alert.error(errors);
                    }

                });
                return false;
            });

            $block.on('click', '.j-delete-answer', function(){
                var $el = $(this);
                if(confirm(o.lang.answer_delete)) {
                    bff.ajax(bff.ajaxURL('qa', 'answer-delete'), {question_id: o.id, answer_id: $el.data('id'), hash: app.csrf_token}, function (resp, errors) {
                        if (resp && resp.success) {
                            $el.closest('li').find('.j-answer-text').after(o.delete_answer).remove();
                        } else {
                            app.alert.error(errors);
                        }
                    });
                }
                return false;
            });
        }

        $block.on('click', '.j-delete-answer-owner', function(){
            var $el = $(this);
            if(confirm(o.lang.answer_delete)){
                bff.ajax(bff.ajaxURL('qa', 'answer-delete-owner'), {id: $el.data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $el.closest('li').find('.j-answer-text').after(o.delete_answer).remove();
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $answers.on('click', '.j-comment-block', function(){
            var $bl = $(this).closest('.j-answer-text');
            var $form = $bl.find('.j-comment-form');
            if( ! $form.hasClass('i')){
                $form.addClass('i');
                app.form($form, function($f){
                    var f = this;
                    if( ! f.checkRequired({focus:true}) ) return;

                    f.ajax(bff.ajaxURL('qa', 'comment-add'),{},function(resp, errors){
                        if(resp && resp.success) {
                            f.$field('message').val('');
                            if(resp.html){
                                $form.parent().find('ul').append(resp.html);
                            }
                        } else {
                            f.alertError(errors);
                        }
                    });
                }, {noEnterSubmit: true});

            }
        });

    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());