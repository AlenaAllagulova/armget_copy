var jSpecsSelect = (function(){
    var o = {block:false, cnt:0, limit:0, onSelect:false, onDelete:false, onExists:false, catOnSelect:1, cancelIfExists:0, existsMessage:false };
    var cache = {}, searchCache = {};
    var prev = {};
    var $block;

    function init(options)
    {
        o = $.extend(o, options || {});
        $block = $(o.block);

        var catID = 0;
        $block.on('show.bs.dropdown', '.j-spec-select', function(){
            var $el = $(this);
            var d = $el.metadata();
            prev[d.count] = $el.find('.j-menu').html();
            catID = 0;
        });

        $block.on('hide.bs.dropdown', '.j-spec-select', function(){
            var $el = $(this);
            var d = $el.metadata();
            if (prev.hasOwnProperty(d.count)) {
                $el.find('.j-menu').html(prev[d.count]);
            }
            catID = 0;
        });

        $block.on('click', '.j-back', function(e){ nothing(e);
            var $el = $(this);
            var d = $el.metadata();
            if( ! catID) catID = d.id;
            menuBlock(d.pid, $el.closest('.j-menu'), function($bl){
                var $cats = $bl.find('.j-cat');
                $cats.parent().removeClass('active');
                $cats.each(function(){
                    var $el = $(this);
                    var cd = $el.metadata();
                    if(cd.id == catID){
                        $el.parent().addClass('active');
                    }
                });
            });
            return false;
        });

        $block.on('click', '.j-cat', function(e){ nothing(e);
            var $el = $(this);
            menuBlock($el.metadata().id, $el.closest('.j-menu'));
            return false;
        });

        $block.on('click', '.j-spec', function(e){ nothing(e);
            var $el = $(this);
            var d = $el.metadata();
            var $bl = $el.closest('.j-spec-select');
            var bd = $bl.metadata();

            var $existsSpecs = $block.find('input.j-spec-value[value="'+d.spec+'"]');
            if ($existsSpecs.length) {
                var $existsBlock = $existsSpecs.closest('.j-spec-select');
                if ($existsBlock.is($bl)) return; // tring to select the same
                if (isDisabledBlock($existsBlock)) {
                    $existsBlock.removeClass('hidden').attr('data-disabled', 0).find('.j-disabled').val(0);
                    $existsBlock.parent().find('.j-spec-ex').removeClass('hidden');
                    if ($bl.parent().hasClass('j-spec-main')) { // main spec: replace with disabled
                        $existsBlock.parent().addClass('j-spec-main');
                        $existsBlock.find('.j-delete').remove();
                        $bl.parent().replaceWith($existsBlock.parent());
                    } else {
                        onDeleteBlock($bl);
                    }
                    return;
                } else {
                    if (o.cancelIfExists) {
                        if (o.existsMessage) {
                            app.alert.error(o.existsMessage);
                        }
                        return;
                    }
                }
            } else {
                if (isDisabledBlock($bl)) {
                    $bl.find('.j-disabled').val(0);
                }
            }

            $bl.find('.j-cat-value').val(d.cat);

            $bl.find('.j-spec-value').val(d.spec);

            var $menu = $el.closest('.j-menu');
            $menu.find('.j-spec').parent().removeClass('active');
            $el.parent().addClass('active');
            if (prev.hasOwnProperty(bd.count)) {
                delete prev['bd.count'];
            }

            $bl.removeClass('open');
            $bl.find('.j-title-empty').addClass('hide');
            $bl.find('.j-title-selected > .j-title').html((o.catOnSelect && d.cat_title.length ? d.cat_title + ' / ' : '') + $el.text());
            $bl.find('.j-title-selected').removeClass('hide');

            if ($existsSpecs.length > 1) {
                if(o.onExists){
                    if(o.onExists($bl, d.spec)) return;
                } else {
                    var cnt = $existsSpecs.length;
                    $existsSpecs.each(function(){
                        if(cnt <= 1) return;
                        var $del = $(this).closest('.j-spec-select').find('.j-delete');
                        if($del.length){
                            $del.trigger('click');
                            cnt--;
                        }
                    });
                }
            }

            if (o.onSelect) {
                o.onSelect(d, $bl.parent());
            }
        });

        $block.on('click', '.j-delete', function(e){ nothing(e);
            var $bl = $(this).closest('.j-spec-select');
            onDeleteBlock($bl);
            return false;
        });

        $block.on('click', '.j-spec-add', function(e){ nothing(e);
            var cnt = $block.find('.j-spec-select:not(.hidden)').length;
            if (cnt >= o.limit) {
                toggleAddBlock();
                return false;
            }
            bff.ajax(bff.ajaxURL('specializations','add'), {cnt: o.cnt}, function(resp){
                if(resp && resp.success) {
                    var html = '<div>'+resp.html+'<div class="j-spec-ex hidden"></div><div class="clearfix"></div></div>';
                    $block.find('.j-specs-block').append(html);
                    toggleAddBlock();
                    if( ! $block.find('.j-spec-select.open').length){
                        $block.find('.j-spec-select:last').addClass('open');
                    }
                }
            });

            o.cnt++;
            return false;
        });

        $block.on('click', '.j-spec-autocomplete', function(e){
            nothing(e);
        });

        $block.on('input', '.j-spec-autocomplete-text', $.debounce(function(){
            var txt = $block.find('.j-spec-autocomplete-text').val();
            if(txt.length){
                if (searchCache.hasOwnProperty(txt)) {
                    $block.find('.j-autocomplete-cancel').removeClass('hidden');
                    searchResult(searchCache[txt])
                } else {
                    bff.ajax(bff.ajaxURL('specializations', 'spec-search'), {q: txt}, function (resp) {
                        if (resp && resp.success) {
                            searchCache[txt] = resp.html;
                            searchResult(searchCache[txt])
                        }
                    }, function(p){
                        $block.find('.j-autocomplete-progress').toggleClass('hidden', ! p);
                        $block.find('.j-autocomplete-cancel').toggleClass('hidden', p);
                    });
                }
            }else{
                searchReset(function() {
                    $block.find('.j-spec-autocomplete-text').focus();
                });
            }
        }, 400));

        $block.on('click', '.j-autocomplete-cancel', function(e){
            e.preventDefault();
            $block.find('.j-autocomplete-cancel').addClass('hidden');
            $block.find('.j-autocomplete-progress').removeClass('hidden');
            searchReset(function(){
                $block.find('.j-autocomplete-progress').addClass('hidden');
            });
        });

        $block.on('click', '.j-spec-search', function(e){ nothing(e);
            var $el = $(this);
            var d = $el.metadata();
            d.spec = intval(d.spec);
            var $bl = $el.closest('.j-spec-select');
            $bl.removeClass('open');
            menuBlock(d.cat, $el.closest('.j-menu'), function($specsBl){
                var $specs = $specsBl.find('.j-spec');
                $specs.each(function(){
                    var $el = $(this);
                    var sd = $el.metadata();
                    if(intval(sd.spec == d.spec)){
                        $el.trigger('click');
                    }
                });
            });
        });
    }

    function searchReset(onResult) {
        var d = $block.find('.j-spec-autocomplete').metadata();
        menuBlock(d.pid, $block.find('.j-menu'), onResult);
    }

    function searchResult(html)
    {
        $block.find('.j-menu > li:not(.j-spec-autocomplete)').remove();
        $block.find('.j-spec-autocomplete').after(html);
    }

    function isDisabledBlock($bl)
    {
        return intval($bl.data('disabled'));
    }

    function onDeleteBlock($bl)
    {
        if (isDisabledBlock($bl)) {
            $bl.addClass('hidden').find('.j-disabled').val(1);
            $bl.parent().find('.j-spec-ex').addClass('hidden');
            toggleAddBlock();
            return;
        }
        if (o.onDelete){
            o.onDelete($bl.parent());
        }

        $bl.parent().remove();
        toggleAddBlock();
    }

    function menuBlock(cat, $bl, onComplete)
    {
        if (cache.hasOwnProperty(cat)) {
            $bl.html(cache[cat]);
            if (onComplete) onComplete($bl);
        } else {
            bff.ajax(bff.ajaxURL('specializations','form-menu'), {cat:cat}, function(resp){
                if (resp && resp.success) {
                    cache[cat] = resp.menu;
                    menuBlock(cat, $bl, onComplete);
                }
            });
        }
    }

    function reset($bl)
    {
        $bl.find('.j-cat-value').val(0);
        $bl.find('.j-spec-value').val(0);
        $bl.removeClass('open');
        $bl.find('.j-title-empty').removeClass('hide');
        $bl.find('.j-title-selected').addClass('hide');
        $bl.find('.j-menu > li.active').removeClass('active');
    }

    function toggleAddBlock()
    {
        var cnt = $block.find('.j-spec-select:not(.hidden)').length;
        if (cnt >= o.limit) {
            $block.find('.j-spec-add').addClass('hide');
        } else {
            $block.find('.j-spec-add').removeClass('hide');
        }
    }

    return {
        init:init,
        onSelect:function(f){ o.onSelect = f; },
        reset:reset
    }
}());