var jQaSearch = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $main, $block, $form, $list, $pgn;
    var solved = {};

    function init()
    {
        $main = $('#j-qa-search');
        $block = $('#j-qa-search-list');

        $form = $('#j-qa-search-form-block').find('form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        for(var i in o.solved){
            if(o.solved.hasOwnProperty(i)){
                if(o.solved[i].hasOwnProperty('id') && o.solved[i].hasOwnProperty('t')){
                    solved[ o.solved[i].id ] = o.solved[i].t;
                }
            }
        }

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $form.on('click', '.j-cat-title', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            $li.toggleClass('opened');
            $li.find('ul').toggleClass('hidden');
            $el.find('.fa').toggleClass('fa-caret-right fa-caret-down');
            return false;
        });

        var $mobileCats = $form.find('#j-mobile-cats');
        $form.on('click', '.j-mobile-cat', function(){
            var $el = $(this);
            $form.find('#j-mobile-cat-'+$el.data('id')).collapse('show');
            $mobileCats.collapse('hide');
            return false;
        });

        $form.on('click', '.j-mobile-cat-back', function(){
            $(this).closest('.j-mobile-cat-block').collapse('hide');
            $mobileCats.collapse('show');
            return false;
        });

        $form.on('show.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.addClass('active');
            $el.find('i.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        });

        $form.on('hide.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.removeClass('active');
            $el.find('i.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        });

        $block.on('click', '.j-f-sl', function(){
            var $el = $(this);
            $form.find('[name="sl"]').val($el.data('id'));
            popupClose($el);
            filterRefresh();
            listMngr.submit({}, true);
            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    $.scrollTo($list, {offset: -150, duration:500});
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(resp.hasOwnProperty('count')){
                    $main.find('.j-qa-count').text(resp.count);
                }
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onAfterDeserialize: function($form, query){
                filterRefresh();
            },
            ajax: o.ajax
        });

    }

    function popupClose($el)
    {
        var $p = $el.closest('li.open');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function filterRefresh()
    {
        // solved
        var id = intval($form.find('[name="sl"]').val());
        var $t = $block.find('.j-f-sl');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == id){
                $(this).parent().addClass('active');
            }
        });
        if(solved.hasOwnProperty(id)){
            $block.find('.j-sl-title').html(solved[id]);
        }
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());