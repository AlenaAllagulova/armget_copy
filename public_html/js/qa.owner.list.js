var jQaOwnerList = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $form, $list, $pgn;

    function init()
    {
        $block = $('#j-qa-owner-list');

        $form = $('#j-qa-owner-list-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $block.on('click', '.j-f-st', function(){
            var $el = $(this);
            $form.find('[name="st"]').val($el.data('id'));
            popupClose($el);
            filterRefresh();
            listMngr.submit({}, true);
            return false;
        });

        $block.on('click', '.j-f-qa', function(){
            var $el = $(this);
            $form.find('[name="qa"]').val($el.data('id'));
            popupClose($el);
            filterRefresh();
            listMngr.submit({}, true);
            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    $.scrollTo($list, {offset: -150, duration:500});
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onAfterDeserialize: function($form, query){
                filterRefresh();
            },
            ajax: o.ajax
        });

        if(o.owner){
            $list.on('click', '.j-delete', function(){
                if(confirm(o.lang.delete)){
                    bff.ajax(bff.ajaxURL('qa', 'question-delete'), {id: $(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                        if(resp && resp.success) {
                            listMngr.submit({});
                        } else {
                            app.alert.error(errors);
                        }
                    });

                }
                return false;
            });
        }

    }

    function popupClose($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function filterRefresh()
    {
        var id = intval($form.find('[name="qa"]').val());
        var $t = $block.find('.j-f-qa');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == id){
                $(this).parent().addClass('active');
            }
        });
        if(o.qa.hasOwnProperty(id)){
            $block.find('.j-f-qa-title').html(o.qa[id].t);
        }

        id = intval($form.find('[name="st"]').val());
        $t = $block.find('.j-f-st');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == id){
                $(this).parent().addClass('active');
            }
        });
        if(o.st.hasOwnProperty(id)){
            $block.find('.j-f-st-title').html(o.st[id].t);
        }
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());