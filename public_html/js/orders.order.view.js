var jOrdersOrderView = (function(){
    var inited = false, o = {lang:{}};
    var $block;

    function init()
    {
        $block = $('#j-order-view');
        var $carousel = $block.find('#j-order-view-carousel');
        if($carousel.length){
            $carousel.owlCarousel({
                items : 5, // 10 items above 1000px browser width
                itemsDesktop : [1000,4], // 5 items between 1000px and 901px
                itemsDesktopSmall : [900,3], // betweem 900px and 601px
                itemsTablet: [600,1], // 2 items between 600 and 0
                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                navigation : true,
                navigationText : ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
                pagination : true,
                autoPlay : false
            });
        }

        $block.on('click', '.j-delete', function(){
            if(confirm(o.lang.order_delete)){
                bff.ajax(bff.ajaxURL('orders', 'order-delete'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        bff.redirect('/');
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $block.on('click', '.j-hide', function(){
            bff.ajax(bff.ajaxURL('orders', 'order-hide'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $block.find('.j-hide').addClass('hidden');
                    $block.find('.j-show').removeClass('hidden');
                    var $h = $block.find('.j-title').find('h1');
                    var $fa = $h.find('.fa-lock');
                    if($fa.length){
                        $fa.removeClass('hidden');
                    }else{
                        $h.prepend('<i class="fa fa-lock"></i> ');
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $block.on('click', '.j-show', function(){
            bff.ajax(bff.ajaxURL('orders', 'order-show'), {id: o.id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $block.find('.j-show').addClass('hidden');
                    $block.find('.j-hide').removeClass('hidden');
                    $block.find('.j-title').find('h1').find('.fa-lock').addClass('hidden');
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        var $map = $('#map-desktop');
        if($map.length){
            app.map($map.get(0), [o.lat, o.lng], function(mmap){
                if (this.isYandex()) {
                    var myPlacemark = new ymaps.Placemark(mmap.getCenter(), {
                        draggable: false,
                        balloonContent: o.balloon
                    });
                    mmap.geoObjects.add(myPlacemark);
                } else if (this.isGoogle()) {
                    var pos = new google.maps.LatLng(o.lat, o.lng);
                    var marker = new google.maps.Marker({
                        position: pos,
                        map: mmap,
                        draggable: false
                    });
                    if(o.balloon) {
                        var infowindow = new google.maps.InfoWindow({
                            position: pos,
                            content: o.balloon
                        });
                        marker.addListener('click', function(){
                            infowindow.open(mmap);
                        });
                    }
                }
            }, {zoom: 12});
        }

        $('#j-more-invites').click(function(e){
            e.preventDefault();
            $('#j-order-invites-list').find('li.hidden').removeClass('hidden');
            $(this).parent().remove();
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());