var jNewsView = (function(){
    var inited = false, o = {lang:{}};
    var $block, $comments;

    function init()
    {
        $block = $('#j-news-view');
        $comments = $('#j-comments');

        o.id = intval(o.id);

        $comments.on('click', '.j-add-comment', function(){
            var $el = $(this);
            var $form = $el.closest('.j-comment').find('.j-add-comment-form');
            $el.closest('.j-add-title').addClass('hidden');

            if( ! $form.hasClass('i')){
                $form.addClass('i');
                app.form($form, function(){
                    var f = this;
                    if( ! f.checkRequired({focus:true}) ) return;

                    f.ajax(bff.ajaxURL('news', 'comment-add'),{},function(resp, errors){
                        if(resp && resp.success) {
                            f.$field('message').val('');
                            if(resp.html){
                                var $par = $el.closest('.j-comment-block');
                                var scroll = false;
                                if( ! $par.length){
                                    $par = $comments.find('.l-comments-list');
                                    scroll = true;
                                }
                                $par.append(resp.html);
                                if(scroll){
                                    $.scrollTo($par.find('.j-comment-block:last'), {duration:300, offset:0});
                                }
                                $el.closest('.j-comment').find('.j-cancel').trigger('click');
                            }
                        } else {
                            f.alertError(errors);
                        }
                    });
                }, {noEnterSubmit: true});
            }
        });

        $comments.on('click', '.j-cancel', function(){
            var $com = $(this).closest('.j-comment');
            var $collapse = $com.find('.collapse');
            /* $collapse.one('hidden.bs.collapse', function () { }); */
            $com.find('.j-add-title').removeClass('hidden');
            $collapse.collapse('hide');
            return false;
        });

        $comments.on('click', '.j-comment-delete', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('news', 'comment-delete'),{id:$el.data('id'), post_id: o.id, hash: app.csrf_token},function(resp, errors) {
                if (resp && resp.success) {
                    var $bl = $el.closest('.j-comment-block');
                    $bl.after(resp.html);
                    $bl.remove();
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $comments.on('click', '.j-comment-restore', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('news', 'comment-restore'),{id:$el.data('id'), post_id: o.id, hash: app.csrf_token},function(resp, errors) {
                if (resp && resp.success) {
                    var $bl = $el.closest('.j-comment-block');
                    $bl.after(resp.html);
                    $bl.remove();
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());