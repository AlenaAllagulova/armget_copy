var jArticlesSearch = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $form, $list, $pgn;
    function init()
    {
        $block = $('#j-articles-search');

        $form = $('#j-articles-search-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    $.scrollTo($list, {offset: -150, duration:500});
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function() {
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $list.on('click', '.j-vote', function(e){ nothing(e);
            var $el = $(this), $bl = $el.closest('.j-vote-block');
            if ($bl.hasClass('q-answer-rating_na')) return false;
            bff.ajax(bff.ajaxURL('articles', 'vote'), {id:$el.data('id'), t:$el.data('t'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.disabled){
                        $bl.addClass('q-answer-rating_na');
                    }
                    if(resp.sum){
                        var $v = $bl.find('.j-votes');
                        $v.html(resp.sum);
                        if(intval(resp.sum) < 0){
                            $v.removeClass('q-answer-good').addClass('q-answer-bad');
                        }else{
                            $v.removeClass('q-answer-bad').addClass('q-answer-good');
                        }
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());


