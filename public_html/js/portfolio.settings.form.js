var jPortfolioSettingsForm = (function(){
    var inited = false, o = {lang:{}};
    var f, $f;

    function init() {

        $f = $('#j-portfolio-settings-form');

        f = app.form($f, function(){

            if( ! f.checkRequired()) return;

            bff.ajax(o.url_settings, $f.serialize(), function(data, errors){
                if(data && data.success) {
                    f.alertSuccess(o.lang.saved_success);
                } else {
                    f.fieldsError(data.fields, errors);
                }
            }, function(p){ f.processed(p); });
        });

    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());


