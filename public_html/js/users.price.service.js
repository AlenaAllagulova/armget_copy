var jUsersPriceService = (function(){
    var inited = false, o = {lang:{}}, listMngr;
    var $main, $block, $form, $list, $pgn;
    var geo = {$block:0, $city:0, $country:0, country:0, cityAC:0,cityID:0,cityPreSuggest:{}, cityName:{}, metro:{name:{}, data:{}}};
    var onPopstate = false;

    function init()
    {
        $main = $('#j-users-price-service');
        $block = $('#j-users-price-service-list');

        $form = $('#j-users-price-form-block').find('form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');
        if(intval(o.cat)){
            $form.append('<input type="hidden" name="cat" value="'+o.cat+'" />');
        }
        if(intval(o.spec)){
            $form.append('<input type="hidden" name="spec" value="'+o.spec+'" />');
        }

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $form.on('click', '.j-cat-title', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            $li.toggleClass('opened');
            $li.find('ul').toggleClass('hidden');
            $el.find('.fa').toggleClass('fa-caret-right fa-caret-down');
            return false;
        });

        var $mobileCats = $form.find('#j-mobile-cats');
        $form.on('click', '.j-mobile-cat', function(){
            var $el = $(this);
            $form.find('#j-mobile-cat-'+$el.data('id')).collapse('show');
            $mobileCats.collapse('hide');
            return false;
        });

        $form.on('click', '.j-mobile-cat-back', function(){
            $(this).closest('.j-mobile-cat-block').collapse('hide');
            $mobileCats.collapse('show');
            return false;
        });

        $form.on('show.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.addClass('active');
            $el.find('i.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        });

        $form.on('hide.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.removeClass('active');
            $el.find('i.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        });

        geo.fn = (function(){
            geo.$block = $('#j-left-region');
            geo.$city = geo.$block.find('#j-region-city-select');
            var $cityVal = geo.$block.find('#j-region-city-value');
            var $region = $form.find('[name="r"]');
            geo.country = intval(o.defCountry);
            geo.cityID = intval($cityVal.val());

            geo.$country = geo.$block.find('#j-left-region-country');
            if(geo.$country.length){
                geo.$country.change(function(){
                    geo.country = intval(geo.$country.val());
                    if(geo.country){
                        initCity();
                        geo.cityAC.setParam('country', geo.country);
                        geo.$city.addClass('hidden');
                        if( geo.cityPreSuggest.hasOwnProperty(geo.country) ) {
                            geo.cityAC.setSuggest(geo.cityPreSuggest[geo.country], true);
                        }else{
                            bff.ajax(bff.ajaxURL('geo', 'country-presuggest'), {country:geo.country}, function(data){
                                geo.cityAC.setSuggest[geo.country] = data;
                                geo.cityAC.setSuggest(data, true);
                            });
                        }
                        geo.$city.removeClass('hidden');
                    }else{
                        geo.$city.addClass('hidden');
                        $region.val(0);
                        $cityVal.val(0);
                        if( ! onPopstate) {
                            listMngr.submit({}, true);
                        }
                    }
                    return true;
                });
                geo.country = intval(geo.$country.val());
            }else{
                $form.on('shown.bs.collapse', '#j-left-region', function(){
                    initCity();
                });
            }

            var cityInited = false;
            function initCity(){
                if(cityInited) return;
                cityInited = true;
                var hidden = false;
                if(geo.$city.hasClass('hidden')){
                    hidden = true;
                    geo.$city.removeClass('hidden');
                }
                geo.$city.autocomplete( bff.ajaxURL('geo', 'region-suggest'),{
                    valueInput: $cityVal, params:{reg:1, country: geo.country},
                    suggest: o.preSuggest ? o.preSuggest : app.regionPreSuggest,
                    onSelect: function(cityID, cityTitle, ex){
                        geo.cityName[intval(cityID)] = cityTitle;
                        if(ex.hasOwnProperty('data') && ex.data.hasOwnProperty(3)){
                            $region.val(ex.data[3]);
                        }else{
                            $region.val(0);
                        }
                        geo.fn.onCitySelect(cityID);
                    },
                    doPrepareText: function(html){
                        var regionTitlePos = html.toLowerCase().indexOf('<br');
                        if( regionTitlePos != -1 ) {
                            html = html.substr(0, regionTitlePos);
                        }
                        html = html.replace(/<\/?[^>]+>/gi, ''); // striptags
                        return $.trim(html);
                    }
                }, function(){ geo.cityAC = this; });
                if(hidden)  geo.$city.addClass('hidden');
            }
            if(geo.$city.is(':visible')) initCity();

            return {
                onCitySelect:function(cityID){
                    geo.cityID = cityID;
                    if( ! onPopstate) {
                        listMngr.submit({}, true);
                    }
                },
                clear:function(){
                    if (geo.$country.length) {
                        geo.$country.val(0);
                        geo.$city.addClass('hidden');
                    }
                    $region.val(0);
                    $cityVal.val(0); geo.$city.val('');
                },
                onPopstate:function(){
                    if(geo.$country.length){
                        if( ! geo.$country.find(':selected').length){
                            geo.$country.find(':first').prop('selected', true);
                        }
                    }
                    var city = intval(geo.$block.find('#j-region-city-value').val());
                    if(geo.cityName.hasOwnProperty(city)){
                        geo.$city.val(geo.cityName[city]);
                    }
                }
            };
        }());


        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    $.scrollTo($list, {offset: -150, duration:500});
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function($form, query) {
            },
            ajax: o.ajax
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());
