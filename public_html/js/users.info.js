var jUsersInfo = (function(){
    var inited = false, o = {lang:{}, url_info:''};
    var $block;

    function init()
    {
        $block = $('#j-owner-info-block');

        $block.find('#j-favs-all').on('click', function(){
            var $el = $(this);
            bff.ajax(o.url_info, {id:$el.data('id'), act:'favs-all', hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $block.find('#j-favs').html(resp.html);
                    $el.remove();
                    if(o.my){
                        myInit();
                    }
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        if(o.my){

            var $my = $block.find('#j-favs-my');
            $block.find('#j-favs-my-all').on('click', function(){
                var $el = $(this);
                bff.ajax(o.url_info, {act:'favs-my-all', hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $my.html(resp.html);
                        $el.remove();
                        myInit();
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });

            $my.on('click', '.j-delete-fav', function(){
                var $el = $(this);
                bff.ajax(bff.ajaxURL('users', 'fav-del'), {id:$el.data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $el.closest('.j-my-fav').remove();
                    } else {
                        app.alert.error(errors);
                    }
                });

                return false;
            });

            myInit();

            var $resumeView = $block.find('#j-resume-view-block');
            var $resumeForm = $block.find('#j-resume-form-block');
            $resumeView.on('click', '.j-resume-edit', function(){
                if( ! $resumeForm.hasClass('i')){
                    $resumeForm.addClass('i');
                    initResume($resumeForm, $resumeView);
                }
                $resumeForm.removeClass('hidden');
                $resumeView.addClass('hidden');
                return false;
            });
            $resumeForm.on('click', '.j-cancel', function(){
                $resumeForm.addClass('hidden');
                $resumeView.removeClass('hidden');
                return false;
            });
        }
    }

    function myInit()
    {
        if(typeof(jUsersNote) == 'object'){
            jUsersNote.$init();
        }
    }

    function initResume($bl, $view)
    {
        var $f = $bl.find('form');
        var f = app.form($f, false, {noEnterSubmit: true});
        bff.iframeSubmit($f, function(resp, errors){
            if(resp && resp.success) {
                bff.ajax(o.url_info, {act: 'resume-data', hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        if(resp.hasOwnProperty('view')){
                            $view.html(resp.view);
                        }
                        if(resp.hasOwnProperty('form')){
                            $bl.removeClass('i');
                            $bl.off('click', '.j-attach');
                            $bl.html(resp.form);
                            if(resp.hasOwnProperty('js')){
                                app.$B.append(resp.js);
                            }
                        }
                        $bl.addClass('hidden');
                        $view.removeClass('hidden');
                    }else{
                        app.alert.error(errors);
                    }
                });
            } else {
                f.fieldsError(resp.fields, errors);
            }
        },{
            beforeSubmit: function(){
                return f.checkRequired({focus:true});
            },
            button: '.j-submit'
        });
        $f.on('click', '.j-submit', function(){
            $f.submit();
            return false;
        });

        var $attach = $f.find('[name="resume_file"]');
        var $del = $f.find('[name="resume_file_del"]');
        $bl.on('click', '.j-attach', function(){
            $attach.trigger('click');
            return false;
        });

        $attach.change(function(){
            $bl.find('.j-file-name').html($(this).val());
            $bl.find('.j-attach-info').removeClass('hidden');
            $del.val(0);
        });

        $bl.on('click', '.j-del-attach', function(){
            $attach.val('');
            $bl.find('.j-attach-info').addClass('hidden');
            $del.val(1);
            return false;
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());
