var jShopOwnerList = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $form, $list, $pgn;
    var $catTitle;

    function init()
    {
        $block = $('#j-shop-owner-list');

        $form = $('#j-shop-owner-list-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        $catTitle = $form.find('.j-f-cat-title');

        $form.on('click', '.j-f-cat-select', function(){
            var $el = $(this);
            $form.find('[name="cat"]').val($el.data('id'));
            closePopup($el);
            massActions();
            listMngr.submit({}, true);
            return false;
        });

        $list.on('click', '.j-delete', function(){
            if(confirm(o.lang.product_delete)){
                bff.ajax(bff.ajaxURL('shop', 'product-delete'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        listMngr.submit({popstate:true}, false);
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
            return false;
        });

        $list.on('click', '.j-cat-edit', function(){
            var $el = $(this);
            var id = intval($el.data('id'));
            var $bl = $el.closest('.j-cat');
            var $descr = $bl.find('.j-descr');
            if( ! $descr.length) return false;
            bff.ajax(bff.ajaxURL('shop', 'cat-edit-form'), {id:id, hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success){
                    $descr.addClass('hidden');
                    $bl.find('.j-edit').addClass('hidden');
                    $descr.after(resp.form);
                    var $f = $('#j-owner-cat-form-'+id);
                    var f = app.form($f, function(){
                        bff.ajax(bff.ajaxURL('shop', 'cat-edit'), $f.serialize(), function(data, errors){
                            if(data && data.success) {
                                $descr.html(data.descr);
                                f.alertSuccess(o.lang.cat_save_success);
                                if($f.find('[ name="order"]:checked').length){
                                    listMngr.submit({}, true);
                                }else{
                                    formClose();
                                }
                            } else {
                                f.fieldsError(data.fields, errors);
                            }
                        });
                    });

                    $f.on('click', '.j-cancel', function(){
                        formClose();
                        return false;
                    });

                    $f.find('.j-after-select').focus(function(){
                        $f.find('.j-after-check').prop('checked', true);
                    });

                    function formClose(){
                        $f.remove();
                        $descr.removeClass('hidden');
                        $bl.find('.j-edit').removeClass('hidden');
                    }
                }else{
                    app.alert.error(errors);
                }
            });

            return false;
        });

        $list.on('click', '.j-hide', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('shop', 'product-hide'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    var $bl = $el.closest('.j-product');
                    $bl.addClass('disabled');
                    $bl.find('.j-hide').addClass('hidden');
                    $bl.find('.j-show').removeClass('hidden');
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        $list.on('click', '.j-show', function(){
            var $el = $(this);
            bff.ajax(bff.ajaxURL('shop', 'product-show'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    var $bl = $el.closest('.j-product');
                    $bl.removeClass('disabled');
                    $bl.find('.j-show').addClass('hidden');
                    $bl.find('.j-hide').removeClass('hidden');
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onAfterDeserialize: function() {
                massActions();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });
    }

    function closePopup($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function massActions()
    {
        var cat = intval($form.find('[name="cat"]').val());
        if(o.cats.hasOwnProperty(cat)){
            $catTitle.html(o.cats[cat]['t']);
        }
    }


    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());
