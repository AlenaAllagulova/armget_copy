var jOrdersOfferAddForm = (function(){
    var inited = false, o = {itemID:0, lang:{}, examplesCnt:0, examplesLimit:0};
    var img = {uploader:0};
    var $form, $examples, $progress, $popup;

    function init()
    {
        $form = $('#j-offer-add-form');
        $examples = $('#j-offer-examples');
        $popup = $('#j-examples-popup');

        $progress = $form.find('.j-progress');

        o.url = bff.ajaxURL('orders&ev=offer_add&hash='+app.csrf_token, '');

        // submit
        app.form($form, function(){ return false;}, {
            onInit: function($f){
                var f = this;
                $f.on('click', '.j-submit', function(){
                    if( ! f.checkRequired({focus:true}) ) return false;
                    f.ajax(o.url+'add',{},function(data,errors){
                        if(data && data.success) {
                            f.alertSuccess(o.lang.saved_success);
                            if(data.reload){
                                location.reload();
                            }
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                    return false;
                });
            }
        });

        // cancel
        $form.on('click', '.j-cancel', function(){
            $('#j-collapse-add').trigger('click');
            return false;
        });
        o.examplesLimit = intval(o.examplesLimit);
        if($examples.length){
            img.url = bff.ajaxURL('orders&ev=offer_img&hash='+app.csrf_token+'&item_id='+o.itemID, '');

            img.uploader = new qq.FileUploaderBasic({
                button: $form.find('#j-upload-img').get(0),
                action: img.url+'upload',
                limit: o.examplesLimit, sizeLimit: o.imgMaxSize,
                uploaded: intval(o.imgUploaded),
                multiple: true, allowedExtensions: ['jpeg','jpg','png','gif'],
                onSubmit: function(id, fileName) {
                    return imgProgress(id, 'start', false);
                },
                onComplete: function(id, fileName, resp) {
                    if(resp && resp.success) {
                        imgProgress(id, 'preview', resp);
                        imgRotate(true);
                    } else {
                        if(resp.errors) {
                            app.alert.error(resp.errors);
                            imgProgress(id, 'remove');
                        }
                    }
                    return true;
                },
                onCancel: function(id, fileName) {
                    imgProgress(id, 'remove');
                },
                showMessage: function(message, code) {
                    app.alert.error(message);
                },
                messages: {
                    typeError: o.lang.upload_typeError,
                    sizeError: o.lang.upload_sizeError,
                    minSizeError: o.lang.upload_minSizeError,
                    emptyError: o.lang.upload_emptyError,
                    limitError: o.lang.upload_limitError,
                    onLeave: o.lang.upload_onLeave
                }
            });

            $examples.on('click', '.j-examp-del', function(e){ nothing(e);
                var $el = $(this).closest('.j-examp');
                imgRemove($el.data('id'), $el);
            });

            imgRotate();

            var unloadProcessed = false;
            app.$W.bind('beforeunload', function(){
                if( ! unloadProcessed && intval(o.itemID) === 0) {
                    unloadProcessed = true;
                    var fn = [];

                    $examples.find('.j-img').each(function(){
                        var fln = $(this).data('fn');
                        if(fln.length){
                            fn.push(fln);
                        }
                    });
                    if( fn.length ) {
                        bff.ajax(img.url+'delete-tmp', {filenames:fn}, false, false, {async:false});
                    }
                }
            });
        }

        $popup.on('click', '.j-submit', function(){
            $popup.find('.j-example-portfolio:checked').each(function(){
                var $el = $(this);
                var id = $el.val();
                if( ! $examples.find('.j-portfolio-'+id).length) {
                    if(o.examplesCnt < o.examplesLimit) {
                        o.examplesCnt++;
                        examplesAdd('portfolio', id, {preview: $el.data('preview')});
                    }
                }
            });
            $popup.find('.j-example-shop:checked').each(function(){
                var $el = $(this);
                var id = $el.val();
                if( ! $examples.find('.j-shop-'+id).length) {
                    if(o.examplesCnt < o.examplesLimit) {
                        o.examplesCnt++;
                        examplesAdd('shop', id, {preview: $el.data('preview')});
                    }
                }
            });
            $popup.modal('hide');
            return false;
        });
    }

    var progressCnt = 0;
    function imgProgress(id, step, data)
    {
        switch(step)
        {
            case 'start': {
                if(o.examplesCnt >= o.examplesLimit) return false;
                o.examplesCnt++;
                $progress.removeClass('hidden');
                progressCnt++;
            } break;
            case 'preview': {
                examplesAdd('img', id, data);
                progressCnt--;
                if(progressCnt <= 0){
                    $progress.addClass('hidden');
                    progressCnt = 0;
                }
            } break;
            case 'remove': {
                imgRemove(id);
            } break;
        }
        return true;
    }

    function examplesAdd(type, id, data)
    {
        var html;
        switch(type){
            case  'img':
                html = '<div class="o-project-thumb o-uploaded j-examp j-img j-img-'+id+'" data-id="'+id+'" data-fn="'+data.filename+'"> '+
                        '<input type="hidden" name="images[]" value="'+data.filename+'"/>'+
                        '<input type="hidden" name="sort[]" value="fn='+data.filename+'"/>'+
                        '<div class="o-inner added"> '+
                            '<img src="'+data.i+'" alt="" /> '+
                        '</div> '+
                        '<a href="#" class="o-item-remove link-delete j-examp-del"><i class="fa fa-times-circle-o"></i></a> '+
                    '</div>';
                break;
            case 'portfolio':
                html = '<div class="o-project-thumb o-uploaded j-examp j-portfolio j-portfolio-'+id+'" data-id="'+id+'"> '+
                    '<input type="hidden" name="sort[]" value="portfolio='+id+'"/>'+
                    '<div class="o-inner added"> '+
                        '<img src="'+data.preview+'" alt="" /> '+
                    '</div> '+
                    '<a href="#" class="o-item-remove link-delete j-examp-del"><i class="fa fa-times-circle-o"></i></a> '+
                '</div>';
                break;
            case 'shop':
                html = '<div class="o-project-thumb o-uploaded j-examp j-shop j-shop-'+id+'" data-id="'+id+'"> '+
                    '<input type="hidden" name="sort[]" value="shop='+id+'"/>'+
                    '<div class="o-inner added"> '+
                        '<img src="'+data.preview+'" alt="" /> '+
                    '</div> '+
                    '<a href="#" class="o-item-remove link-delete j-examp-del"><i class="fa fa-times-circle-o"></i></a> '+
                '</div>';
                break;

        }
        if(html) {
            $examples.append(html);
            imgRotate(true);
        }
    }

    function imgRotate(update)
    {
        if(update === true) {
            $examples.sortable('refresh');
        }else{
            $examples.sortable();
        }
    }

    function imgRemove(id, $bl)
    {
        o.examplesCnt--;
        if( ! $bl){
            $bl = $examples.find('.j-img-' + id);
        }
        if($bl.length){
            if($bl.hasClass('j-img')) {
                bff.ajax(img.url + 'delete', {image_id: 0, filename: $bl.data('fn')}, function (resp) {
                    if (resp && resp.success) {
                        img.uploader.decrementUploaded();
                        $bl.remove();
                        imgRotate(true);
                    } else {
                        app.alert.error(resp.errors);
                    }
                });
            }else{
                $bl.remove();
                imgRotate(true);
            }
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());