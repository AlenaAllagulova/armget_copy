var jSvcCarousel = (function(){
    var inited = false, o = {lang:{}};

    function init()
    {
        var $form = $('#j-svc-carousel'); if ( ! $form.length ) return;
        var f = app.form($form, function(){
            if( ! f.checkRequired() ) return;
            f.ajax('', {}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.redirect){
                        bff.redirect(resp.redirect);
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
})();

var jSvcPro = (function(){
    var inited = false, o = {lang:{}, balance:0, url_buy:''};

    function init()
    {
        o.balance = o.balance - 0;

        var $form = $('#j-svc-pro'); if ( ! $form.length ) return;
        var f = app.form($form, function(){
            if( ! f.checkRequired() ) return;

            var $sel = $form.find('[name="m"]:checked');
            if( ! $sel.length){
                app.alert.error(o.lang.select_month);
                return;
            }

            f.ajax('', {}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.redirect){
                        bff.redirect(resp.redirect);
                    } else {
                        app.alert.success(o.lang.success);
                        window.setTimeout(function(){ location.reload(); }, 1000);
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });

        var $submit = $form.find('.j-submit');
        $form.find('[name="m"]').change(function(){
            var $el = $(this);
            $el.closest('label').addClass('active').siblings().removeClass('active');
            var sum = $el.data('sum') - 0;
            if(o.balance >= sum){
                $submit.html(o.lang.svc);
            } else {
                $submit.html(o.lang.buy);
            }
        });

        $form.find('.j-cancel').click(function(){
            history.back();
            return false;
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
})();

var jSvcStairsSelect = (function(){
    var inited = false, o = {lang:{}, balance:0, message_length:80, descr_length:400}, $block, $specsBl, $form, $submit;

    function init()
    {
        o.balance = o.balance - 0;
        o.i = intval(o.i);
        o.price.main = o.price.main - 0;
        o.price.user = o.price.user - 0;
        o.price.spec = o.price.spec - 0;
        o.price.cat = o.price.cat - 0;
        o.spec_main = intval(o.spec_main);
        o.spec_user = intval(o.spec_user);

        $block = $('#j-stairs-block');

        $submit = $block.find('.j-submit');

        $form = $block.find('form');
        $specsBl = $('#j-stairs-specs');

        var $addF = $block.find('#j-add-form');
        var $addBl = $block.find('#j-add-block');

        var $specs = $specsBl.find('[name="spec[]"]');
        $specsBl.on('change', '[name="spec[]"]', function(){
            var $el = $(this);
            var v = $el.val();
            $specs.filter('[value="'+v+'"]').prop('checked', $el.prop('checked'));
            if($el.is(':checked')){
                var html = $addF.html();
                html = html.replace(/__counter__/g, o.i);
                $addBl.append(html);
                var $bl = $addBl.find('.j-add-'+o.i);
                $bl.find('.j-spec').val(v);
                $bl.find('.j-title').text($el.parent().text());
                bff.maxlength($bl.find('.j-message'), {limit: o.message_length, message:$bl.find('.j-message-counter'), lang:{left: o.lang.maxlength_left, symbols:o.lang.maxlength_symbols}});
                bff.maxlength($bl.find('.j-description'), {limit: o.descr_length, message:$bl.find('.j-description-counter'), lang:{left: o.lang.maxlength_left, symbols:o.lang.maxlength_symbols}});
                $bl.addClass('j-add-spec-'+v);
                calcSumBl($bl);
                o.i++;
            } else {
                $addBl.find('.j-add-spec-'+v).remove();
                calcSumBl();
            }
        });

        $specsBl.on('change', '[name="cat[]"]', function(){
            var $el = $(this);
            var v = $el.val();
            if($el.is(':checked')) {
                var html = $addF.html();
                html = html.replace(/__counter__/g, o.i);
                $addBl.append(html);
                var $bl = $addBl.find('.j-add-' + o.i);
                $bl.find('.j-cat').val(v);
                $bl.find('.j-title').text($el.parent().text());
                bff.maxlength($bl.find('.j-message'), {limit:o.message_length, message:$bl.find('.j-message-counter'), lang:{left: o.lang.maxlength_left, symbols:o.lang.maxlength_symbols}});
                bff.maxlength($bl.find('.j-description'), {limit:o.descr_length, message:$bl.find('.j-description-counter'), lang:{left: o.lang.maxlength_left, symbols:o.lang.maxlength_symbols}});
                $bl.addClass('j-add-cat-'+v);
                calcSumBl($bl);
                o.i++;
            } else {
                $addBl.find('.j-add-cat-'+v).remove();
                calcSumBl();
            }
        });

        $addBl.on('click', '.j-close', function(){
            var $el = $(this);
            var $bl = $el.closest('.j-stairs');
            var s = intval($bl.find('.j-spec').val());
            if(s){
                $specs.filter('[value="'+s+'"]').prop('checked', false);
            } else {
                s = intval($bl.find('.j-cat').val());
                if(s){
                    $specsBl.find('[name="cat[]"]').filter('[value="'+s+'"]').prop('checked', false);
                }
            }
            $bl.remove();
            calcSumBl();
            return false;
        });

        $block.find('.j-cancel').click(function(){
            history.back();
            return false;
        });

        $form.on('click', '.j-plus', function(){
            var $el = $(this);
            var $bl = $el.closest('.j-stairs');
            var $w = $bl.find('.j-weeks');
            var w = intval($w.val());
            w++;
            if(w > 9){
                w = 9;
                $el.prop('disabled', true);
            }
            $bl.find('.j-minus').prop('disabled', false);
            $w.val(w);
            calcSumBl($bl);
            return false;
        });

        $form.on('click', '.j-minus', function(){
            var $el = $(this);
            var $bl = $el.closest('.j-stairs');
            var $w = $bl.find('.j-weeks');
            var w = intval($w.val());
            w--;
            if(w < 1){
                w = 0;
                $el.prop('disabled', true);
            }
            $bl.find('.j-plus').prop('disabled', false);
            $w.val(w);
            calcSumBl($bl);
            return false;
        });

        $form.on('change', '.j-weeks', function(){
            var $el = $(this);
            var $bl = $el.closest('.j-stairs');
            var w = intval($el.val());
            if(w < 1){
                w = 0;
                $bl.find('.j-minus').prop('disabled', true);
            } else {
                $bl.find('.j-minus').prop('disabled', false);
            }
            if(w > 9){
                w = 9;
                $bl.find('.j-plus').prop('disabled', true);
            } else {
                $bl.find('.j-plus').prop('disabled', false);
            }
            $el.val(w);
            calcSumBl($bl);
            return false;
        });

        var f = app.form($form, function(){
            f.ajax('', {}, function(resp, errors){
                if( ! f.checkRequired() ) return;

                if(resp && resp.success) {
                    if(resp.redirect){
                        bff.redirect(resp.redirect);
                    } else {
                        app.alert.success(o.lang.success);
                        window.setTimeout(function(){ location.reload(); }, 1000);
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });

        o.spec_id = intval(o.spec_id);
        if(o.spec_id){
            var $sp = $specsBl.find('[name="spec[]"]').filter('[value="'+o.spec_id+'"]:first');
            if($sp.length){
                if($sp.is(':disabled')){
                    var $bl = $form.find('.j-spec').filter('[value="'+o.spec_id+'"]');
                    if($bl.length){
                        $bl.closest('.j-stairs').find('.j-plus').trigger('click');
                    }
                } else {
                    $sp.trigger('click');
                }
            }
        } else {
            o.cat_id = intval(o.cat_id);
            if(o.cat_id){
                var $ct = $specsBl.find('[name="cat[]"]').filter('[value="'+o.cat_id+'"]');
                if($ct.length){
                    if($ct.is(':disabled')){
                        var $bl = $form.find('.j-cat').filter('[value="'+o.cat_id+'"]');
                        if($bl.length){
                            $bl.closest('.j-stairs').find('.j-plus').trigger('click');
                        }
                    } else {
                        $ct.trigger('click');
                    }
                }
            }
        }
    }

    function calcSumBl($bl)
    {
        if($bl){
            var s = intval($bl.find('.j-spec').val());
            var pr = 0;
            if(s != 0){
                switch(s){
                    case o.spec_main: pr = o.price.main; break;
                    case o.spec_user: pr = o.price.user; break;
                    default: pr = o.price.spec; break;
                }
            } else {
                s = intval($bl.find('.j-cat').val());
                if(s){
                    pr = o.price.cat;
                }
            }
            var w = intval($bl.find('.j-weeks').val());
            var $sum = $bl.find('.j-sum');
            pr = w * pr;
            $bl.data('sum', pr);
            pr = pr + ' ' + o.lang.cur;
            $sum.html(pr);

        }
        var sum = 0;
        $form.find('.j-stairs').each(function(){
            var s = $(this).data('sum');
            s = s - 0;
            sum += s;
        });
        if(sum > 0){
            if(o.balance >= sum){
                sum = o.lang.publish + ' ' + sum + ' ' + o.lang.cur;
            } else {
                sum = sum - o.balance;
                sum = o.lang.buy + ' ' + sum + ' ' + o.lang.cur;
            }
            $submit.html(sum);
            $submit.removeClass('hidden');
        } else {
            $submit.addClass('hidden');
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
})();

var jSvcStairsUp = (function(){
    var inited = false, o = {lang:{}, balance:0}, $block, $submit, $list;
    var sum = 0;

    function init()
    {
        o.balance = o.balance - 0;
        o.price = o.price - 0;

        $block = $('#j-stairs-up-block');

        $submit = $block.find('.j-submit');

        $block.find('.j-cancel').click(function(){
            history.back();
            return false;
        });

        $list = $block.find('.j-list');

        $list.on('click', '.j-up', function(){
            var $bl = $list.find('.j-my');
            sum += o.price;
            var sumorig = intval($bl.filter(':first').data('sumorig'));
            $bl.data('sum', sumorig + sum);
            reorder();
            return false;
        });

        $list.on('click', '.j-down', function(){
            var $bl = $list.find('.j-my');
            sum -= o.price;
            if(sum < 0){
                sum = 0;
            }
            var sumorig = intval($bl.filter(':first').data('sumorig'));
            $bl.data('sum', sumorig + sum);
            reorder();
            return false;
        });

        $list.on('click', '.j-current', function(){
            var $bl = $(this).closest('.j-block');
            var s = intval($bl.data('sum'));
            var $my = $list.find('.j-my');
            var sumorig = intval($my.filter(':first').data('sumorig'));
            console.log(s, sumorig);
            if(s < sumorig) return false;
            sum = s - sumorig + o.price;
            $my.data('sum', sumorig + sum);
            reorder();
            return false;
        });

        $block.on('click', '.j-submit', function(){
            if(sum <= 0) return false;
            bff.ajax('', {hash:app.csrf_token, cat_id: o.cat_id, spec_id: o.spec_id, sum:sum}, function(resp, errors){
                if(resp && resp.success) {
                    app.alert.success(o.lang.success);
                    window.setTimeout(function(){ location.reload(); }, 1000);
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });
    }

    function reorder()
    {
        var $desktop = $list.find('.j-desktop');
        var $sort = $desktop.find('.j-block');
        $sort.sort(function(a,b){
            var a_am = intval( $(a).data('sum') );
            var b_am = intval( $(b).data('sum') );
            return ( ((a_am < b_am) || (b_am === a_am && intval( $(a).data('pos') ) > intval( $(b).data('pos') ))  ) ? 1 : -1 );
        });
        $.each($sort, function(index, row){
            $desktop.append(row);
        });

        var $mobile = $list.find('.j-mobile');
        $sort = $mobile.find('.j-block');
        $sort.sort(function(a,b){
            var a_am = intval( $(a).data('sum') );
            var b_am = intval( $(b).data('sum') );
            return ( ((a_am < b_am) || (b_am === a_am && intval( $(a).data('pos') ) > intval( $(b).data('pos') ))  ) ? 1 : -1 );
        });
        $.each($sort, function(index, row){
            $mobile.append(row);
        });

        var $my = $list.find('.j-my:first');
        $list.find('.j-sum').html(sum + ' ' + o.lang.cur);
        $list.find('.j-sum-sum').html($my.data('sum'));
        if(sum > 0){
            var t;
            if(o.balance >= sum){
                t = sum;
                t = o.lang.up + ' ' + t + ' ' + o.lang.cur;
            } else {
                t = sum - o.balance;
                t = o.lang.buy + ' ' + t + ' ' + o.lang.cur;
            }
            $submit.html(t);
            $submit.removeClass('hidden');
        } else {
            $submit.addClass('hidden');
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
})();

var jSvcStairsEdit = (function(){
    var inited = false, o = {lang:{}, message_length:80, descr_length:400};
    var $block;

    function init()
    {
        $block = $('#j-svc-stairs-block');
        $block.on('click', '.j-edit-stairs', function(){
            var $li = $(this).closest('li');
            $li.find('.j-stairs').addClass('hidden');
            var $f = $li.find('.j-stairs-edit-form');
            $f.removeClass('hidden');
            initForm($f);
            return false;
        });

        $block.on('click', '.j-edit-stairs-descr', function(){
            var $bl = $(this).closest('.j-stairs-descr');
            $bl.find('.j-edit-stairs-descr-bl').addClass('hidden');
            var $f = $bl.find('.j-edit-stairs-descr-form');
            $f.removeClass('hidden');
            initFormDescr($f);
            return false;
        });

    }

    function initForm($bl)
    {
        if($bl.hasClass('i')) return;
        $bl.addClass('i');
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            f.ajax(bff.ajaxURL('users', 'svc-stairs-save'), {}, function(resp, errors){
                if(resp && resp.success) {
                    hideForm($bl, resp);
                } else {
                    f.alertError(errors);
                }
            });
        });
        $bl.on('click', '.j-cancel', function(){
            hideForm($(this));
            return false;
        });
        bff.maxlength($bl.find('[name="message"]'), {limit:o.message_length, message:$bl.find('.j-message-counter'), lang:{left: o.lang.maxlength_left, symbols:o.lang.maxlength_symbols}});
    }

    function hideForm($el, o)
    {
        var $li = $el.closest('li');
        $li.find('.j-stairs').removeClass('hidden');
        $li.find('.j-stairs-edit-form').addClass('hidden');
        if(o && o.hasOwnProperty('title')){
            $li.find('.j-title').html(o.title);
        }
        if(o && o.hasOwnProperty('message')) {
            $li.find('.j-message').html(o.message);
        }
    }

    function initFormDescr($bl)
    {
        if($bl.hasClass('i')) return;
        $bl.addClass('i');
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            f.ajax(bff.ajaxURL('users', 'svc-stairs-descr'), {}, function(resp, errors){
                if(resp && resp.success) {
                    hideFormDescr($bl, resp);
                } else {
                    f.alertError(errors);
                }
            });
        });
        $bl.on('click', '.j-cancel', function(){
            hideFormDescr($(this));
            return false;
        });
        bff.maxlength($bl.find('[name="description"]'), {limit:o.descr_length, message:$bl.find('.j-description-counter'), lang:{left: o.lang.maxlength_left, symbols:o.lang.maxlength_symbols}});
    }

    function hideFormDescr($el, o)
    {
        var $bl = $el.closest('.j-stairs-descr');
        $bl.find('.j-edit-stairs-descr-bl').removeClass('hidden');
        $bl.find('.j-edit-stairs-descr-form').addClass('hidden');
        if(o && o.hasOwnProperty('description')){
            $bl.find('.j-description').html(o.description);
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }

    }
})();