var jFairplayMyFinance = (function(){
    var inited = false, o = {lang:{}, url_settings:''};
    var $block;

    function init()
    {
        $block = $('#j-fairplay-finance');

        var $form = $block.find('form');
        var f = app.form($form, false, {noEnterSubmit: true});
        bff.iframeSubmit($form, function(resp, errors){
            if(resp && resp.success) {
                f.alertSuccess(o.lang.saved_success);
                location.reload();
            } else {
                f.fieldsError(resp.fields, errors);
            }
        },{
            beforeSubmit: function(){
                if( ! f.checkRequired({focus:true}) ) return false;
                return true;
            },
            button: '.j-submit',
            url: bff.ajaxURL('fairplay', 'save&ev=my_settings_finance')
        });
        var i = 1;
        var $files = $form.find('.j-files');
        var $add = $form.find('.j-add');
        $add.click(function(){
            $files.append('<li><a href="#" class="link-red j-delete"><i class="fa fa-times"></i></a> <input type="file" name="verified_'+i+'" style="display: inline;" /> </li>');
            $files.find('input[type="file"]:last').trigger('click');
            i++;
            if($files.find('li').length >= o.verified_limit){
                $add.hide();
            }
        });
        $files.on('click', '.j-delete', function(e){
            e.preventDefault();
            var $el = $(this);
            $el.closest('li').remove();
            $add.show();
            var fn = $el.data('fn');
            if(fn && fn.length){
                $form.append('<input type="hidden" name="verified_deleted[]" value="'+$el.data('fn')+'" />');
            }
        });
    }
    
    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());
