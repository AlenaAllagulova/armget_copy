var jFairplayWorkflowsList = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $form, $list, $pgn;

    function init()
    {
        $block = $('#j-fairplay-workflows-list');

        $form = $('#j-fairplay-workflows-list-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        $form.on('click', '.j-f-status', function(){
            var $el = $(this);
            $form.find('[name="st"]').val($el.data('id'));
            closePopup($el);
            massActions();
            listMngr.submit({}, true);
            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                var st = intval($form.find('[name="st"]').val());
                if(resp.hasOwnProperty('counts')){
                    for(var t in resp.counts){
                        if(resp.counts.hasOwnProperty(t)){
                            $form.find('.j-cnt-status-' + t ).text('(' + resp.counts[t] + ')');
                            if(st == t){
                                $form.find('.j-f-status-cnt').text('(' + resp.counts[t] + ')');
                            }
                        }
                    }
                }
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function() {
                massActions();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

    }

    function closePopup($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function massActions()
    {
        var t = intval($form.find('[name="st"]').val());
        if(o.types.hasOwnProperty(t)){
            $form.find('.j-f-status-title').text(o.types[t].t);
        }
        var $t = $form.find('.j-f-status');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == t){
                $(this).parent().addClass('active');
            }
        });
    }

    function refresh()
    {
        listMngr.submit({});
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        },
        refresh:refresh
    }
}());