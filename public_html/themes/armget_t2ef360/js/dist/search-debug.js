'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Search = function () {
    function Search() {
        _classCallCheck(this, Search);
    }

    _createClass(Search, null, [{
        key: 'getSuggests',
        value: function getSuggests(search_form) {
            var form = search_form;
            var idInputSearch = $('#' + form);
            var qStr = idInputSearch.val();
            if (qStr.length == 0) {
                $('#suggest_list').html('');
            }
            if (qStr.length > 2) {
                bff.ajax(bff.ajaxURL('catcher&ev=getSuggest', ''), {
                    q_str: qStr,
                    search_form: form
                }, function (d, error) {
                    if (d.hasOwnProperty('list')) {
                        $('#suggest_list').html('');
                        $('#suggest_list').append(d.list);
                    }
                });
            }
        }
    }]);

    return Search;
}();
//# sourceMappingURL=search.js.map
