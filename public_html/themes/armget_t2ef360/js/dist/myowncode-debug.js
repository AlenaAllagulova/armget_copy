'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MyOwnCode = function () {
    function MyOwnCode() {
        _classCallCheck(this, MyOwnCode);
    }

    _createClass(MyOwnCode, [{
        key: 'UserAuth',

        // Fairplay (options)
        // {
        //     this._modalCashout = null;
        //     this._workflowId = null;
        //     this._actionBtn = null;
        //     this.options = options;
        //     this._modalLock = false;
        //     this.setModalCashout = (selector) => {
        //         this._modalCashout = $(selector);
        //         if (!Boolean(this._modalCashout.length)) {
        //             return false;
        //         }
        //         let op = this.options;
        //         this._modalCashout.on('shown.bs.modal', (e) => {
        //             let modal = $(e.currentTarget);
        //             let btn = modal.find('.j-fairplay-cashout-selected');
        //             let items = modal.find('input[name="fairplay"]');
        //             if (!Boolean(items.length)) {
        //                 btn.attr('disabled','disabled');
        //             }
        //             btn.on('click', (e) => {
        //                 btn.attr('disabled','disabled');
        //                 let item = items.filter(':checked');
        //                 if (!Boolean(item.length)) {
        //                     app.alert.error(op.lngs.err_selected_item_cashout);
        //                     btn.removeAttr('disabled');
        //                     return null;
        //                 }
        //                 $('.j-fairplay-output-money').attr("disabled", "disabled");
        //                 if(btn.attr('disabled') === 'disabled')
        //                     bff.ajax(
        //                         bff.ajaxURL(
        //                             'fairplay',
        //                             op.act.CASHOUT_HISTORY
        //                         ),
        //                         {
        //                             'title': item.val(),
        //                             'workflowId': this.getWorkflowId()
        //                         },
        //                         (data, errors) => {
        //                             if (data.success) {
        //                                 modal.modal('hide');
        //                                 app.alert.success(data.message);
        //                                 location.reload();
        //                             } else {
        //                                 app.alert.error(errors);
        //                             }
        //                         }
        //                     );
        //             });
        //         });
        //
        //         this._modalCashout.on('hidden.bs.modal', (e) => {
        //             let modal = $(e.currentTarget);
        //             modal.remove();
        //             this._modalLock = false;
        //         });
        //
        //         this._modalCashout.modal('show');
        //
        //         return this;
        //     };
        //
        //     this.isValidFairplay = () => {
        //         let op = this.options;
        //         let tabsItem = $(op.tabsItem);
        //         if ( ! Boolean(tabsItem.length)) {
        //             return true;
        //         }
        //         tabsItem.find('.form-group.has-error').removeClass('has-error');
        //
        //         let fieldsEmpty = true;
        //         let fieldsValid = false;
        //
        //         for (var i = 0; i < tabsItem.length; i++) {
        //             let itemKey = tabsItem.eq(i).data('target');
        //             let itemFields = tabsItem.eq(i).find('input[type="text"], textarea');
        //
        //             if (Boolean(itemFields.length)) {
        //                 let isValue = false;
        //                 let isValueRequired = false;
        //                 let itemsRequired = [];
        //                 itemFields.each((j, elem) => {
        //                     elem = $(elem);
        //                     switch (itemKey) {
        //                         case 'webmoney':
        //                             if (elem.hasClass(op.required)) {
        //                                 itemsRequired.push(elem);
        //                             }
        //                             if (elem.val() !== '') {
        //                                 fieldsEmpty = false;
        //                                 isValue = true;
        //                                 if ( ! (new RegExp(elem.data('pattern'))).test(elem.val())) {
        //                                     elem.closest('.form-group').addClass('has-error');
        //                                     app.alert.error(op.lngs.error_webmoney_urz);
        //                                     return false;
        //                                 }
        //                                 fieldsValid = true;
        //                             }
        //                             break;
        //                         case 'walletone':
        //                             if (elem.val() !== '') {
        //                                 fieldsEmpty = false;
        //                                 isValue = true;
        //                                 isValueRequired = true;
        //                                 if ( ! (new RegExp(elem.data('pattern'))).test(elem.val())) {
        //                                     elem.closest('.form-group').addClass('has-error');
        //                                     app.alert.error(op.lngs.error_walletone);
        //                                     return false;
        //                                 }
        //                                 fieldsValid = true;
        //                             }
        //                             break;
        //                         case 'visamastercard':
        //                             if (elem.val() !== '') {
        //                                 fieldsEmpty = false;
        //                                 isValue = true;
        //                                 isValueRequired = true;
        //                                 if ( ! (new RegExp(elem.data('pattern'))).test(elem.val())) {
        //                                     elem.closest('.form-group').addClass('has-error');
        //                                     app.alert.error(op.lngs.error_visamastercard);
        //                                     return false;
        //                                 }
        //                                 fieldsValid = true;
        //                             }
        //                             break;
        //                     }
        //                 });
        //
        //                 if (isValueRequired && !isValue) {
        //                     for (var k = 0; k < itemsRequired.length; k++) {
        //                         itemsRequired[k].closest('.form-group').addClass('has-error');
        //                     }
        //                     app.alert.error(op.lngs.error_required_not_values);
        //                 }
        //             }
        //         }
        //
        //         if (fieldsEmpty) {
        //             app.alert.error(op.lngs.error_not_fairplay);
        //             return false;
        //         }
        //
        //         return fieldsValid;
        //     };
        //
        //     this.getModalCashout = () => {
        //         return this._modalCashout;
        //     };
        //
        //     this.setWorkflowId = (workflowId) => {
        //         this._workflowId = workflowId;
        //         return this;
        //     };
        //
        //     this.getWorkflowId = () => {
        //         return this._workflowId;
        //     };
        //
        //     this.setActionBtn = (actionBtn) => {
        //         let btn = $(actionBtn);
        //         if (Boolean(btn.length)) {
        //             this._actionBtn = btn;
        //         }
        //         return this;
        //     };
        //
        //     this.getActionBtn = () => {
        //         return this._actionBtn;
        //     };
        //
        //     this.isNull = (param) => {
        //         return ((typeof param === 'function') ? param() : param) === null;
        //     };
        //
        //     return {
        //         OutputMoney: () => {
        //             let op = this.options;
        //             if (this.setActionBtn(op.btn).isNull(this.getActionBtn)) {
        //                 return false;
        //             }
        //
        //             this.getActionBtn().on('click', (e) => {
        //                 this.setWorkflowId($(e.currentTarget).data('id'));
        //                 bff.ajax(
        //                     bff.ajaxURL(
        //                         'fairplay',
        //                         op.act.MODAL_SHOW
        //                     ),
        //                     {
        //                         'workflowId': this.getWorkflowId()
        //                     },
        //                     (data, errors) => {
        //                         if (data.success) {
        //                             if(!this._modalLock) {
        //                                 this._modalLock = true;
        //                                 $('body').append(data.html);
        //                                 return this.setModalCashout('#j-workflow-cashout-modal');
        //                             }
        //                         } else {
        //                             app.alert.error(errors);
        //                         }
        //                     }
        //                 );
        //             });
        //         },
        //         PaymentRequisites: () => {
        //             let op = this.options;
        //             if (this.setActionBtn(op.btn).isNull(this.getActionBtn)) {
        //                 return false;
        //             }
        //
        //             this.getActionBtn().on('click', (e) => {
        //                 let elem = $(e.currentTarget);
        //                 let tab = $(op.tabsItem + '[data-target="' + elem.data('key') + '"]');
        //                 tab.slideToggle(300, () => {
        //                     var isVisible = tab.is(':visible');
        //                     if (isVisible) {
        //                         elem.addClass('active');
        //                     } else {
        //                         elem.removeClass('active');
        //                     }
        //                 });
        //             });
        //         }
        //     }
        // }

        value: function UserAuth(options) {
            var _this = this;

            this.options = options;
            this._types = null;
            this._catsBox = null;
            this._cats = null;

            this.setTypes = function (selector) {
                _this._types = $(selector);
                return _this;
            };

            this.getTypes = function () {
                return _this._types;
            };

            this.setCatsBox = function (selector) {
                _this._catsBox = $(selector);
                return _this;
            };

            this.getCatsBox = function () {
                return _this._catsBox;
            };

            this.setCats = function (selector) {
                _this._cats = $(selector);
                return _this;
            };

            this.getCats = function () {
                return _this._cats;
            };

            this.setTypesOnChange = function () {
                _this.getTypes().on('change', function (e) {
                    if ($(e.currentTarget).val() == _this.options.TYPE_WORKER) {
                        _this.getCatsBox().show();
                    } else {
                        _this.getCatsBox().hide();
                    }
                });
            };

            this.setSearchableOptionList = function () {
                var op = _this.options;
                _this.getCats().searchableOptionList({
                    showSelectAll: false,
                    showSelectionBelowList: true,
                    maxHeight: '300px',
                    disabled: true,
                    texts: {
                        'searchplaceholder': op.lang.searchplaceholder
                    },
                    events: {
                        onOpen: function onOpen(sol) {
                            if (sol.getSelection().length >= parseInt(op.specMaxLimit)) {
                                sol.close();
                                return false;
                            }
                        },
                        onChange: function onChange(sol, changedElements) {
                            if (sol.getSelection().length >= parseInt(op.specMaxLimit)) {
                                sol.close();
                                $('.sol-input-container').find('input').attr('disabled', 'disabled');
                            } else {
                                $('.sol-input-container').find('input').removeAttr('disabled');
                            }
                        }
                    }
                });
            };

            return {
                Register: function Register() {
                    var op = _this.options;

                    _this.setTypes(op.inputTypes);
                    _this.setCatsBox(op.boxSpecs);
                    _this.setCats(op.selectSpecs);

                    _this.setTypesOnChange();
                    _this.setSearchableOptionList();
                }
            };
        }
    }]);

    return MyOwnCode;
}();
//# sourceMappingURL=myowncode.js.map
